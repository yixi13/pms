package com.pms.mq;

import com.pms.constant.MQConstant;
import org.springframework.amqp.core.Binding;
import org.springframework.amqp.core.BindingBuilder;
import org.springframework.amqp.core.DirectExchange;
import org.springframework.amqp.core.Queue;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * Created by ljb on 2017/11/15.
 */
@Configuration
public class QueueConfiguration {
    //信道配置
    @Bean
    public DirectExchange defaultExchange() {
        return new DirectExchange(MQConstant.DEFAULT_EXCHANGE, true, false);
    }


    /*********************    注册 队列  测试    *****************/
    @Bean
    public Queue queue() {
        Queue queue = new Queue(MQConstant.QUEUE_NAME_ESTATE,true);
        return queue;
    }

    @Bean
    public Binding binding() {
        return BindingBuilder.bind(queue()).to(defaultExchange()).with(MQConstant.QUEUE_NAME_ESTATE);
    }
}
