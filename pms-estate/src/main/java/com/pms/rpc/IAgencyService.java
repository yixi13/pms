package com.pms.rpc;


import com.pms.entity.AgencyInfo;
import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;


@FeignClient("pms-system")
@RequestMapping("api")
public interface IAgencyService {
    /**
     * 根据机构id 获取机构信息
     * @param agencyId 机构id
     * @return AgencyInfo
     */
    @RequestMapping(value = "/agency/AgencyInfo/{agencyId}", method = RequestMethod.GET)
    AgencyInfo getAgencyInfoByAgencyId(@PathVariable("agencyId") String agencyId);
}
