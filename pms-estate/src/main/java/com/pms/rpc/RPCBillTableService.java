package com.pms.rpc;

import com.pms.service.IBillTableService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by Administrator on 2017/11/14 0014.
 */
@RestController
@RequestMapping("api")
public class RPCBillTableService {
    Logger logger = LoggerFactory.getLogger(getClass());
    @Autowired
    IBillTableService billTableService;
    @RequestMapping(value = "/billTable/addBillTables",method = RequestMethod.GET, produces="application/json")
    public void addBillTableByTimer(){
        billTableService.addBillTableByTimer();
    }
}
