package com.pms.rpc;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.pms.entity.EstateAgency;
import com.pms.service.IEstateAgencyService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Map;

/**
 * Created by hm on 2017/10/25.
 */
@RestController
@RequestMapping("api")
public class EstateAgencyService {
    Logger logger = LoggerFactory.getLogger(getClass());
    @Autowired
    IEstateAgencyService agencyService;

    @RequestMapping(value = "/estateAgency/getAgencyList",method = RequestMethod.GET, produces="application/json")
    public List<Map<String,Object>> getAgencyList(){
        return agencyService.selectMaps(new EntityWrapper<EstateAgency>().eq("agency_type",1));
    }
    @RequestMapping(value = "/estateAgency/getUnderCommunity/{agencyId}",method = RequestMethod.GET, produces="application/json")
    public List<Map<String,Object>> getUnderCommunity(@PathVariable("agencyId")Long agencyId){
        return agencyService.selectMaps(new EntityWrapper<EstateAgency>().eq("parent_id",agencyId));
    }
}
