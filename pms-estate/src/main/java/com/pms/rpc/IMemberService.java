package com.pms.rpc;

import com.pms.entity.MemberInfo;
import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * Created by hm on 2017/10/25.
 */
@FeignClient("pms-system")
@RequestMapping("api")
public interface IMemberService {
    /**
     * 查询会员信息
     * @param memberId 会员id
     * @return 会员昵称，id,性别，头像,手机号(不屏蔽)
     */
    @RequestMapping(value = "/member/memberId/{memberId}",method = RequestMethod.GET, produces="application/json")
    public MemberInfo queryBymember(@PathVariable("memberId")Long memberId);

}
