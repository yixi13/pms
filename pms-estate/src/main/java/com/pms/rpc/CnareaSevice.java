package com.pms.rpc;
import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

/**
 * Created by hm on 2017/10/31.
 */
@FeignClient("pms-system")
@RequestMapping("api")
public interface CnareaSevice {

    /**
     * 根据城市编码查询地址
     * @param areaCode
     * @return
     */
    @RequestMapping(value = "/cnarea/{threeAreaId}",method = RequestMethod.GET)
    Map<String,Object> selectThreeParentArea(@PathVariable("threeAreaId") Long areaCode);
}
