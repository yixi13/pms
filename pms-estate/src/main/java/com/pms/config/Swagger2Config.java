package com.pms.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.context.request.async.DeferredResult;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

/**
 * Swagger2配置
 *
 * @author zyl
 * @create 2017-06-09 16:45
 **/
@Configuration
@EnableSwagger2 // 启用swagger
public class Swagger2Config{
    @Bean
    public Docket createRestApi() {
        return new Docket(DocumentationType.SWAGGER_2)
                .groupName("groupApi")
                .apiInfo(apiInfo())
                .select()
                .apis(RequestHandlerSelectors.basePackage("com.pms.api"))
                .paths(PathSelectors.any())
                .build();
//                .pathMapping("/api/mpi/estate");
    }
    private ApiInfo apiInfo() {
        return new ApiInfoBuilder()
                .title("API接口 RESTful API")
                .description("宏铭科技：http://www.schmkj.cn")
                .termsOfServiceUrl("http://www.schmkj.cn")
                .contact("宏铭科技")
                .version("2.0")
                .build();
    }
    @Bean
    public Docket createRestApi2() {
        return new Docket(DocumentationType.SWAGGER_2)
                .groupName("groupbase")
                .apiInfo(apiInfo2())
                .select()
                .apis(RequestHandlerSelectors.basePackage("com.pms.web"))
                .paths(PathSelectors.any())
                .build();
//                .pathMapping("/base");

}
    private ApiInfo apiInfo2() {
        return new ApiInfoBuilder()
                .title("后台接口 RESTful API")
                .description("宏铭科技：http://www.schmkj.cn")
                .termsOfServiceUrl("http://www.schmkj.cn")
                .contact("宏铭科技")
                .version("2.0")
                .build();
    }
}
