package com.pms.mapper;
import com.baomidou.mybatisplus.mapper.Wrapper;
import com.pms.entity.EquipmentRepair;
import com.baomidou.mybatisplus.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.session.RowBounds;

import java.util.List;
import java.util.Map;

/**
 * 
 * 
 * @author zyl
 * @email 517770986@qq.com
 * @date 2017-11-01 14:31:47
 */
public interface EquipmentRepairMapper extends BaseMapper<EquipmentRepair> {

    List<Map<String, Object>> getequipmentRepair(RowBounds rowBounds, @Param("ew") Wrapper<EquipmentRepair> wrapper);

}
