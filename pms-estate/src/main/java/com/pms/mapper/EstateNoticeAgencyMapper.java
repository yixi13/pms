package com.pms.mapper;
import com.pms.entity.EstateNoticeAgency;
import com.baomidou.mybatisplus.mapper.BaseMapper;
/**
 * 
 * 
 * @author zyl
 * @email 517770986@qq.com
 * @date 2017-11-03 09:48:34
 */
public interface EstateNoticeAgencyMapper extends BaseMapper<EstateNoticeAgency> {
	
}
