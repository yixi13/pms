package com.pms.mapper;
import com.pms.entity.EstateNotice;
import com.baomidou.mybatisplus.mapper.BaseMapper;

import java.io.Serializable;

/**
 * 
 * 
 * @author zyl
 * @email 517770986@qq.com
 * @date 2017-11-03 09:48:33
 */
public interface EstateNoticeMapper extends BaseMapper<EstateNotice> {
    EstateNotice selectBynoticeId(Long id);


}
