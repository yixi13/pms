package com.pms.mapper;
import com.pms.entity.EstateDoorLock;
import com.baomidou.mybatisplus.mapper.BaseMapper;
/**
 * 
 * 
 * @author zyl
 * @email 517770986@qq.com
 * @date 2017-11-17 14:41:03
 */
public interface EstateDoorLockMapper extends BaseMapper<EstateDoorLock> {
	
}
