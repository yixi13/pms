package com.pms.mapper;

import com.pms.entity.OwnerMember;
import com.baomidou.mybatisplus.mapper.BaseMapper;

import java.util.List;

/**
 * <p>
  *  Mapper 接口
 * </p>
 *
 * @author ASUS_B
 * @since 2017-10-26
 */
public interface OwnerMemberMapper extends BaseMapper<OwnerMember> {
    /**
     * 查询所有物业人员 会员id
     * @param communityId
     * @return
     */
    public List<Long> readOwnerMemberIds(Long communityId);

}