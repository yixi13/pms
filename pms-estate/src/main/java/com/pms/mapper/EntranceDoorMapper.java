package com.pms.mapper;

import com.pms.entity.EntranceDoor;
import com.baomidou.mybatisplus.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;

/**
 * <p>
  *  Mapper 接口
 * </p>
 *
 * @author ASUS_B
 * @since 2017-10-24
 */
public interface EntranceDoorMapper extends BaseMapper<EntranceDoor> {
    /**
     * 修改关联表 门名称(门锁表，门卡片表)
     * @param doorId 门id
     */
  public void updateJoinTableDoorName(@Param("doorId")Long doorId);
}