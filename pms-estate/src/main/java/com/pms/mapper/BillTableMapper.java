package com.pms.mapper;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.baomidou.mybatisplus.mapper.Wrapper;
import com.pms.entity.BillTable;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * 
 * 
 * @author zyl
 * @email 517770986@qq.com
 * @date 2017-11-01 11:52:51
 */
public interface BillTableMapper extends BaseMapper<BillTable> {
    List<Map<String,Object>> selectBillTables(@Param("ew") Wrapper<BillTable> wrapper);
    List<BillTable> selectBillTableDetailsByParam(@Param("ew") Wrapper<BillTable> wrapper);
    int addBillTableList(List<BillTable> list);
}
