package com.pms.mapper;

import com.pms.entity.UnitBuilding;
import com.baomidou.mybatisplus.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;

/**
 * <p>
  *  Mapper 接口
 * </p>
 *
 * @author ASUS_B
 * @since 2017-10-24
 */
public interface UnitBuildingMapper extends BaseMapper<UnitBuilding> {

    /**
     * 查询小区最大的小区编号,用于创建单元时
     * @param communityId 小区id
     * @return 最大的小区编号
     */
    int getMaxUnitNum(Long communityId);

    /**
     * 修改单元名称 同步修改 关联表单元名称
     * @param unitName 单元名称
     * @param communityId 社区id
     * @param unitId 单元id
     */
    public void updateJoinTableUnitName(@Param("unitName")String unitName, @Param("communityId")Long communityId, @Param("unitId")Long unitId);

    /**
     * 修改 大豪卡片 的关联单元名称
     * @param unitId
     */
    public void updateCardJoinTableUnitName(@Param("unitId")Long unitId);
}