package com.pms.mapper;
import com.pms.entity.OpinionEvalua;
import com.baomidou.mybatisplus.mapper.BaseMapper;
/**
 * 
 * 
 * @author zyl
 * @email 517770986@qq.com
 * @date 2017-11-02 14:10:56
 */
public interface OpinionEvaluaMapper extends BaseMapper<OpinionEvalua> {
	
}
