package com.pms.mapper;
import com.pms.entity.RepairEvalua;
import com.baomidou.mybatisplus.mapper.BaseMapper;
/**
 * 
 * 
 * @author zyl
 * @email 517770986@qq.com
 * @date 2017-11-01 14:34:16
 */
public interface RepairEvaluaMapper extends BaseMapper<RepairEvalua> {
	
}
