package com.pms.mapper;

import com.pms.entity.EstateAgency;
import com.baomidou.mybatisplus.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;

/**
 * <p>
  *  Mapper 接口
 * </p>
 *
 * @author ASUS_B
 * @since 2017-10-26
 */
public interface EstateAgencyMapper extends BaseMapper<EstateAgency> {
  public Integer selectMaxCommunityNum();

  void updateJoinTableCommunityName(@Param("communityName")String communityName, @Param("communityId")Long communityId);

  /**
   * 修改 大豪卡片 的关联社区名称
   * @param communityId
   */
  public void updateCardJoinTableCommunityName(@Param("communityId")Long communityId);
}