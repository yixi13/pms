package com.pms.mapper;
import com.pms.entity.OpenDoorRecord;
import com.baomidou.mybatisplus.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;

import java.util.Date;

/**
 * 
 * 
 * @author zyl
 * @email 517770986@qq.com
 * @date 2017-12-12 10:42:50
 */
public interface OpenDoorRecordMapper extends BaseMapper<OpenDoorRecord> {

    /**
     * 开门次数+1
     * @param doorId 门id
     */
    public int addOpenNumByDoorId(@Param("doorId")Long doorId,@Param("openDate")Date openDate);

    /**
     * 查询总开门次数
     * @param agencyId 物业机构id
     * @param conmunityId 社区id
     * @param doorId 门
     * @return 开门次数
     */
    public Long reanOpenDoorNum(@Param("agencyId")Long agencyId,@Param("conmunityId")Long conmunityId,@Param("doorId")Long doorId);

    /**
     * 保存开门记录
     * @param eqSn 设备id
     * @param recordId 新增的id
     */
    public void saveOpenDoorNumBySqlFunction(@Param("eqSn")String eqSn,@Param("recordId")Long recordId);

}
