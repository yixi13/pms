package com.pms.mapper;
import com.baomidou.mybatisplus.mapper.Wrapper;
import com.pms.entity.EstateEqClass;
import com.baomidou.mybatisplus.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 
 * 
 * @author zyl
 * @email 517770986@qq.com
 * @date 2017-10-27 14:52:23
 */
public interface EstateEqClassMapper extends BaseMapper<EstateEqClass> {

    List<EstateEqClass> selectListByAll();
}
