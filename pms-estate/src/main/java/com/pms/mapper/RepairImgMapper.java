package com.pms.mapper;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.pms.entity.RepairEvalua;
import com.pms.entity.RepairImg;

/**
 * 
 * 
 * @author zyl
 * @email 517770986@qq.com
 * @date 2017-11-01 14:34:16
 */
public interface RepairImgMapper extends BaseMapper<RepairImg> {
	
}
