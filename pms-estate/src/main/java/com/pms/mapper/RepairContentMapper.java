package com.pms.mapper;
import com.pms.entity.RepairContent;
import com.baomidou.mybatisplus.mapper.BaseMapper;
/**
 * 
 * 
 * @author zyl
 * @email 517770986@qq.com
 * @date 2017-11-01 14:33:01
 */
public interface RepairContentMapper extends BaseMapper<RepairContent> {
	
}
