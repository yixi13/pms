package com.pms.mapper;

import com.pms.entity.ChargeValue;
import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
  *  Mapper 接口
 * </p>
 *
 * @author ASUS_B
 * @since 2017-11-07
 */
public interface ChargeValueMapper extends BaseMapper<ChargeValue> {

}