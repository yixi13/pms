package com.pms.mapper;

import com.pms.entity.UnitFloor;
import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
  *  Mapper 接口
 * </p>
 *
 * @author ASUS_B
 * @since 2017-10-24
 */
public interface UnitFloorMapper extends BaseMapper<UnitFloor> {

}