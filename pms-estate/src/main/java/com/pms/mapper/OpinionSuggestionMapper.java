package com.pms.mapper;
import com.baomidou.mybatisplus.mapper.Wrapper;
import com.pms.entity.EquipmentRepair;
import com.pms.entity.OpinionSuggestion;
import com.baomidou.mybatisplus.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.session.RowBounds;

import java.util.List;
import java.util.Map;

/**
 * 
 * 
 * @author zyl
 * @email 517770986@qq.com
 * @date 2017-11-02 14:10:56
 */
public interface OpinionSuggestionMapper extends BaseMapper<OpinionSuggestion> {

    List<Map<String, Object>> getOpinionSuggestionState(RowBounds rowBounds, @Param("ew") Wrapper<OpinionSuggestion> wrapper);

}
