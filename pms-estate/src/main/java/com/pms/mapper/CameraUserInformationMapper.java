package com.pms.mapper;
import com.pms.entity.CameraUserInformation;
import com.baomidou.mybatisplus.mapper.BaseMapper;
/**
 * 
 * 
 * @author zyl
 * @email 517770986@qq.com
 * @date 2017-11-15 11:33:11
 */
public interface CameraUserInformationMapper extends BaseMapper<CameraUserInformation> {
	
}
