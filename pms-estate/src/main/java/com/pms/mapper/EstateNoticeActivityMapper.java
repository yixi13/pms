package com.pms.mapper;
import com.pms.entity.EstateNoticeActivity;
import com.baomidou.mybatisplus.mapper.BaseMapper;
/**
 * 
 * 
 * @author zyl
 * @email 517770986@qq.com
 * @date 2017-11-03 09:48:34
 */
public interface EstateNoticeActivityMapper extends BaseMapper<EstateNoticeActivity> {
	
}
