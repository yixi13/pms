package com.pms.mapper;

import com.pms.entity.HouseMasterInfo;
import com.pms.entity.UnitHouse;
import com.baomidou.mybatisplus.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * <p>
  *  Mapper 接口
 * </p>
 *
 * @author ASUS_B
 * @since 2017-10-24
 */
public interface UnitHouseMapper extends BaseMapper<UnitHouse> {
    /**
     * excel导入户主信息，同步修改 户室有无户主状态 及户室面积
     */
    public void updateIsHaveMasterAndHouseProportion(Long communityId);

    /**
     * Api 接口查询 单元楼层户室
     * @param paramMap (communityId-社区id,unitId-单元id)
     * @return Map
     */
   public List<Map<String,Object>> selectUnitFloorHouse(Map<String,Object> paramMap);

    /**
     * Api 查询物业办公室
     * @param communityId 社区id
     * @return Map
     */
    public Map<String,Object> readEstateHouse(Long communityId);


}