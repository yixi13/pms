package com.pms.mapper;
import com.pms.entity.EstateLockCard;
import com.baomidou.mybatisplus.mapper.BaseMapper;
/**
 * 
 * 
 * @author zyl
 * @email 517770986@qq.com
 * @date 2017-11-27 17:37:54
 */
public interface EstateLockCardMapper extends BaseMapper<EstateLockCard> {
	
}
