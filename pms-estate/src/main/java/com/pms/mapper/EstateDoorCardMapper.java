package com.pms.mapper;
import com.baomidou.mybatisplus.mapper.Wrapper;
import com.pms.entity.EstateDoorCard;
import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.pms.entity.EstateUnitCard;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.session.RowBounds;

import java.util.List;
import java.util.Map;

/**
 * 
 * 
 * @author zyl
 * @email 517770986@qq.com
 * @date 2017-11-27 17:37:54
 */
public interface EstateDoorCardMapper extends BaseMapper<EstateDoorCard> {

    /**
     * 查询 未发的蓝牙卡片
     * @param rowBounds
     * @param wp
     * @return
     */
    public List<EstateUnitCard> selectWillUseCardList(RowBounds rowBounds, @Param("ew") Wrapper<EstateUnitCard> wp, @Param("leftJoinWhere") String leftJoinWhere);
    /**
     * 查询 未发的蓝牙卡片
     * @param wp
     * @return
     */
    public List<EstateUnitCard> selectWillUseCardList(@Param("ew") Wrapper<EstateUnitCard> wp,@Param("leftJoinWhere")String leftJoinWhere);

    public void saveSendCardRecordByFunction(@Param("eqSn")String eqSn, @Param("cardSixteenNum")String cardSixteenNum,@Param("lockCardIdStart")Long lockCardIdStart);
}
