package com.pms.mapper;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.pms.entity.BillTableDetail;

import java.util.List;

/**
 * 
 * 
 * @author zyl
 * @email 517770986@qq.com
 * @date 2017-11-01 11:52:52
 */
public interface BillTableDetailMapper extends BaseMapper<BillTableDetail> {
    int addBillTableDetailList(List<BillTableDetail> list);
}
