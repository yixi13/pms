package com.pms.mapper;

import com.pms.entity.HouseMasterInfo;
import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
  *  Mapper 接口
 * </p>
 *
 * @author ASUS_B
 * @since 2017-11-02
 */
public interface HouseMasterInfoMapper extends BaseMapper<HouseMasterInfo> {

}