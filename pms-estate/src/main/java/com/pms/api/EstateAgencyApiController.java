package com.pms.api;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.mapper.Wrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.pms.controller.BaseController;
import com.pms.entity.EstateAgency;
import com.pms.entity.UnitBuilding;
import com.pms.exception.R;
import com.pms.service.IEstateAgencyService;
import com.pms.service.IUnitHouseService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * <p>
 * 前端控制器
 * 物业机构
 * </p>
 *
 * @author ASUS_B
 * @since 2017-10-26
 */
@RestController
@RequestMapping("/estateAgency")
@Api(value = "物业机构查询相关接口", description = "物业机构查询相关接口")
public class EstateAgencyApiController extends BaseController {
    @Autowired
    private IEstateAgencyService estateAgencyService;

    @ApiOperation(value = "分页查询所有物业")
    @RequestMapping(value = "/readAgencyPage",method = RequestMethod.POST)
    @ApiImplicitParams({
            @ApiImplicitParam(name="areaId",value="机构所在地址id(省-市-区)",required = false,dataType = "string",paramType="form"),
            @ApiImplicitParam(name="pageSize",value="请求条数(默认10)",required = false,dataType = "int",paramType="form"),
            @ApiImplicitParam(name="pageNum",value="请求页数(默认1)",required = false,dataType = "int",paramType="form"),
            @ApiImplicitParam(name="agencyState",value="是否查询禁用机构(1-是(默认)，2-否)",required = false,dataType = "int",paramType="form"),
    })
    public R readAgencyPage(Integer pageNum,Integer pageSize,String areaId,Integer agencyState){
        if(null==pageNum||pageNum<1){pageNum=1;}
        if(null==pageSize||pageSize<1){pageSize=10;}
        Page<EstateAgency> page = new Page<EstateAgency>(pageNum,pageSize);
        Wrapper<EstateAgency> wrapper=  new EntityWrapper<EstateAgency>();
        wrapper.eq("agency_type",1);
        if(null!=agencyState){
            if(agencyState == 2){
                wrapper.eq("agency_state",1);
            }
        }
        if(StringUtils.isNotBlank(areaId)){
            wrapper.like("area_id",areaId);
        }
        Page<EstateAgency> returnPage = estateAgencyService.selectPage(page,wrapper);
        return R.ok().put("data",returnPage).putDescription(EstateAgency.class);
    }

    @ApiOperation(value = "查询所有物业集合")
    @RequestMapping(value = "/readAgencyList",method = RequestMethod.POST)
    @ApiImplicitParams({
            @ApiImplicitParam(name="areaId",value="机构所在地址id(省-市-区)",required = false,dataType = "string",paramType="form"),
            @ApiImplicitParam(name="agencyState",value="是否查询禁用机构(1-是(默认)，2-否)",required = false,dataType = "int",paramType="form"),
    })
    public R readAgencyList(String areaId,Integer agencyState){
        Wrapper<EstateAgency> wrapper=  new EntityWrapper<EstateAgency>();
        wrapper.eq("agency_type",1);
        if(null!=agencyState){
            if(agencyState == 2){
                wrapper.eq("agency_state",1);
            }
        }
        if(StringUtils.isNotBlank(areaId)){
            wrapper.like("area_id",areaId);
        }
        List<EstateAgency> returnPage = estateAgencyService.selectList(wrapper);
        return R.ok().put("data",returnPage).putDescription(EstateAgency.class);
    }

    @ApiOperation(value = "分页查询社区")
    @RequestMapping(value = "/readCommunityPage",method = RequestMethod.POST)
    @ApiImplicitParams({
            @ApiImplicitParam(name="parentId",value="上级机构id(空值查所有)",required = false,dataType = "long",paramType="form"),
            @ApiImplicitParam(name="pageSize",value="请求条数(默认10)",required = false,dataType = "int",paramType="form"),
            @ApiImplicitParam(name="pageNum",value="请求页数(默认1)",required = false,dataType = "int",paramType="form"),
            @ApiImplicitParam(name="areaId",value="社区所在地址id(省-市-区)",required = false,dataType = "string",paramType="form"),
            @ApiImplicitParam(name="agencyState",value="是否查询禁用机构(1-是(默认)，2-否)",required = false,dataType = "int",paramType="form"),
    })
    public R readCommunityPage(Integer pageNum,Integer pageSize,String areaId,Integer agencyState,Long parentId){
        if(null==pageNum||pageNum<1){pageNum=1;}
        if(null==pageSize||pageSize<1){pageSize=10;}
        Page<EstateAgency> page = new Page<EstateAgency>(pageNum,pageSize);
        Wrapper<EstateAgency> wrapper=  new EntityWrapper<EstateAgency>();
        if(parentId!=null){
            wrapper.eq("parentId",parentId);
        }
        wrapper.eq("agency_type",2);
        if(null!=agencyState){
            if(agencyState == 2){
                wrapper.eq("agency_state",1);
            }
        }
        if(StringUtils.isNotBlank(areaId)){
            wrapper.like("area_id",areaId);
        }
        Page<EstateAgency> returnPage = estateAgencyService.selectPage(page,wrapper);
        return R.ok().put("data",returnPage).putDescription(EstateAgency.class);
    }

    @ApiOperation(value = "查询社区集合")
    @RequestMapping(value = "/readCommunityList",method = RequestMethod.POST)
    @ApiImplicitParams({
            @ApiImplicitParam(name="parentId",value="上级机构id",required = false,dataType = "long",paramType="form"),
            @ApiImplicitParam(name="areaId",value="社区所在地址id(省-市-区)",required = false,dataType = "string",paramType="form"),
            @ApiImplicitParam(name="agencyState",value="是否查询禁用机构(1-是(默认)，2-否)",required = false,dataType = "int",paramType="form"),
    })
    public R readCommunityList(String areaId,Integer agencyState,Long parentId){
        Wrapper<EstateAgency> wrapper=  new EntityWrapper<EstateAgency>();
        if(null!=parentId){
            wrapper.eq("parent_id",parentId);
        }
        wrapper.eq("agency_type",2);
        if(null!=agencyState){
            if(agencyState == 2){
                wrapper.eq("agency_state",1);
            }
        }
        if(StringUtils.isNotBlank(areaId)){
            wrapper.like("area_id",areaId);
        }
        List<EstateAgency> returnPage = estateAgencyService.selectList(wrapper);
        return R.ok().put("data",returnPage).putDescription(EstateAgency.class);
    }
}
