package com.pms.api;

import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.mapper.Wrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.pms.controller.BaseController;
import com.pms.entity.AgencyInfo;
import com.pms.entity.EstateAgency;
import com.pms.entity.UnitBuilding;
import com.pms.exception.R;
import com.pms.rpc.IAgencyService;
import com.pms.service.IUnitBuildingService;
import com.pms.util.LBSGeotableColumn;
import com.pms.util.LBSUtil;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

/**
 * <p>
 *  单元管理界面接口
 * 1) 因锁设备需要绑定单元编号，所以==》单元创建后不能删除，只能屏蔽app端显示，且每个小区只能创建 250个单元。
 *
 * </p>
 *
 * @author ASUS_B
 * @since 2017-10-24
 */
@RestController
@RequestMapping("/estateAgencyLBS")
@Api(value="LBS接口",description = "LBS接口")
public class LBSController extends BaseController {
    @Autowired
    private LBSUtil lbsUtil;

    @ApiOperation(value = "创建LBS物业机构表")
    @RequestMapping(value = "/addEstateGeotable",method = RequestMethod.POST)
    @ApiImplicitParams({
            @ApiImplicitParam(name="geotableName",value="表名称(有则创建表)",required = false,dataType = "string",paramType="form"),
    })
    public R addEstateGeotable(String geotableName){
        String geotableId =null;
      if(StringUtils.isNotBlank(geotableName)){
          JSONObject json = lbsUtil.createGeotable(geotableName);
          if(null==json||null==json.getInteger("status")||json.getInteger("status")!=0){return R.error("表创建失败");}
         geotableId = json.getString("id");
      }else{
          geotableId =lbsUtil.getGeotableId();
      }
        List<LBSGeotableColumn> columnList=new ArrayList<LBSGeotableColumn>();
        LBSGeotableColumn column1 = new LBSGeotableColumn();
        column1.setColumnName("agencyName");
        column1.setColumnNameDescription("机构名称");
        column1.setGeotableId(geotableId);
        column1.setAk(lbsUtil.getAK());
        column1.setColumnType(3);
        columnList.add(column1);

        LBSGeotableColumn column2 = new LBSGeotableColumn();
        column2.setColumnName("agencyImg");
        column2.setColumnNameDescription("机构LOGOurl");
        column2.setGeotableId(geotableId);
        column2.setAk(lbsUtil.getAK());
        column2.setColumnType(4);
        columnList.add(column2);

        LBSGeotableColumn column3 = new LBSGeotableColumn();
        column3.setColumnName("agencyType");
        column3.setColumnNameDescription("机构类型");
        column3.setGeotableId(geotableId);
        column3.setAk(lbsUtil.getAK());
        column3.setColumnType(1);
        columnList.add(column3);

        LBSGeotableColumn column4 = new LBSGeotableColumn();
        column4.setColumnName("agencyState");
        column4.setColumnNameDescription("机构状态");
        column4.setGeotableId(geotableId);
        column4.setAk(lbsUtil.getAK());
        column4.setColumnType(1);
        columnList.add(column4);

        LBSGeotableColumn column5 = new LBSGeotableColumn();
        column5.setColumnName("parentId");
        column5.setColumnNameDescription("父级机构id");
        column5.setGeotableId(geotableId);
        column5.setAk(lbsUtil.getAK());
        column5.setColumnType(3);
        columnList.add(column5);

        LBSGeotableColumn column6 = new LBSGeotableColumn();
        column6.setColumnName("agencyPhone");
        column6.setColumnNameDescription("机构联系电话");
        column6.setGeotableId(geotableId);
        column6.setAk(lbsUtil.getAK());
        column6.setColumnType(3);
        column6.setIsSortfilterField(0);//不检索
        column6.setIsSearchField(0);
        columnList.add(column6);

        LBSGeotableColumn column7 = new LBSGeotableColumn();
        column7.setColumnName("agencyTelphone");
        column7.setColumnNameDescription("机构联系电话-座机");
        column7.setGeotableId(geotableId);
        column7.setAk(lbsUtil.getAK());
        column7.setColumnType(3);
        column7.setIsSortfilterField(0);//不检索
        column7.setIsSearchField(0);
        columnList.add(column7);
        List<JSONObject> returnList = lbsUtil.createColumnList(columnList);
        return R.ok().put("data",returnList).put("geotableId",geotableId);
    }

    @ApiOperation(value = "创建LBS表")
    @RequestMapping(value = "/addGeotable",method = RequestMethod.POST)
    @ApiImplicitParams({
            @ApiImplicitParam(name="geotableName",value="表名称",required = true,dataType = "string",paramType="form"),
    })
    public R addGeotable(String geotableName){
        parameterIsBlank(geotableName,"表名称不能为空");
        JSONObject json = lbsUtil.createGeotable(geotableName);
        return R.ok().put("data",json);
    }
    @ApiOperation(value = "添加一列")
    @RequestMapping(value = "/addGeotableColumn",method = RequestMethod.POST)
    @ApiImplicitParams({
            @ApiImplicitParam(name="columnNameDescription",value="列名描述",required = true,dataType = "string",paramType="form"),
            @ApiImplicitParam(name="columnName",value="列名",required = true,dataType = "string",paramType="form"),
            @ApiImplicitParam(name="columnType",value="列类型",required = true,dataType = "int",paramType="form"),
            @ApiImplicitParam(name="isSearchField",value="是否设置为 检索字段(1是(默认)|0否)",required = false,dataType = "int",paramType="form"),
            @ApiImplicitParam(name="isSortfilterField",value="是否设置为 排序字段(1是(默认)|0否)",required = false,dataType = "string",paramType="form"),
            @ApiImplicitParam(name="isIndexField",value="是否设置为 索引字段(1是|0否(默认))",required = false,dataType = "string",paramType="form"),
            @ApiImplicitParam(name="geotableId",value="表id",required = false,dataType = "string",paramType="form"),
            @ApiImplicitParam(name="ak",value="LBS_ak",required = false,dataType = "string",paramType="form"),
    })
    public R addEstateAgency(String geotableId,String columnNameDescription,String columnName,String ak
            ,Integer columnType,Integer isSearchField,Integer isIndexField,Integer isSortfilterField){
        parameterIsBlank(columnNameDescription,"列名描述不能为空");
        parameterIsBlank(columnName,"列名不能为空");
        if(null==columnType||columnType<1||columnType>4){return R.error("列类型错误");}
        if(null==isSearchField||isSearchField!=0){isSearchField=1;}
        if(null==isSortfilterField||isSortfilterField!=0){isSortfilterField=1;}
        if(null==isIndexField||isIndexField!=1){isIndexField=0;}
        LBSGeotableColumn geotableColumn = new LBSGeotableColumn();
        geotableColumn.setAk(ak);
        geotableColumn.setColumnName(columnName);
        geotableColumn.setColumnNameDescription(columnNameDescription);
        geotableColumn.setColumnType(columnType);
        geotableColumn.setGeotableId(geotableId);
        geotableColumn.setIsIndexField(isIndexField);
        geotableColumn.setIsSearchField(isSearchField);
        geotableColumn.setIsSortfilterField(isSortfilterField);
        JSONObject json = lbsUtil.createColumn(geotableColumn);
        return R.ok().put("data",json);
    }
}
