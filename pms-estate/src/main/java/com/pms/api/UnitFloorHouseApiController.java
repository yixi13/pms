package com.pms.api;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.mapper.Wrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.pms.entity.UnitBuilding;
import com.pms.entity.UnitFloor;
import com.pms.entity.UnitHouse;
import com.pms.exception.R;
import com.pms.service.IUnitBuildingService;
import com.pms.service.IUnitFloorService;
import com.pms.service.IUnitHouseService;
import com.pms.util.DescriptionUtil;
import com.pms.controller.BaseController;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author ASUS_B
 * @since 2017-10-17
 */
@RestController
@RequestMapping("/unitFloorHouse")
@Api(value="单元-楼层-户室查询相关接口",description = "单元-楼层-户室查询相关接口")
public class UnitFloorHouseApiController extends BaseController {
    Logger logger = LoggerFactory.getLogger(getClass());
    @Autowired
    private IUnitBuildingService unitBuildingService;
    @Autowired
    private IUnitFloorService unitFloorService;
    @Autowired
    private IUnitHouseService unitHouseService;

    @ApiOperation(value = "查询小区中的单元")
    @RequestMapping(value = "/readUnitList",method =RequestMethod.POST)
    @ApiImplicitParams({
            @ApiImplicitParam(name="communityId",value="小区id",required = true,dataType = "long",paramType="form"),
    })
    public Object readUnitList(Long communityId) {
        parameterIsNull(communityId,"社区id不能为空");
        Wrapper<UnitBuilding> wapper =new EntityWrapper<UnitBuilding>().eq("community_id",communityId);
        wapper.eq("is_show_app",1);
        List<UnitBuilding> unitList = unitBuildingService.selectList(wapper);
        if(null==unitList){unitList=new ArrayList<UnitBuilding>();}
        return R.ok().put("data",unitList).putDescription(UnitBuilding.class);
    }

    @ApiOperation(value = "查询单元中的楼层")
    @RequestMapping(value = "/readFloorList",method =RequestMethod.POST)
    @ApiImplicitParams({
            @ApiImplicitParam(name="communityId",value="小区id",required = true,dataType = "long",paramType="form"),
            @ApiImplicitParam(name="unitId",value="单元id",required = true,dataType = "long",paramType="form"),
    })
    public Object readFloorList(Long communityId,Long unitId) {
        parameterIsNull(communityId,"社区id不能为空");
        Wrapper<UnitFloor> wapper =  new EntityWrapper<UnitFloor>().eq("community_id",communityId);
        wapper.eq("unit_id",unitId);
        wapper.eq("is_show_app",1);
        List<UnitFloor> unitList = unitFloorService.selectList(wapper);
        if(null==unitList){unitList=new ArrayList<UnitFloor>();}
        return R.ok().put("data",unitList).putDescription(UnitFloor.class);
    }

    @ApiOperation(value = "查询楼层中的户室")
    @RequestMapping(value = "/readHouseList",method =RequestMethod.POST)
    @ApiImplicitParams({
            @ApiImplicitParam(name="communityId",value="小区id",required = true,dataType = "long",paramType="form"),
            @ApiImplicitParam(name="floorId",value="楼层id",required = true,dataType = "long",paramType="form"),
            @ApiImplicitParam(name="readEstateHouse",value="是(1)/否(2-默认)查询物业办公室",required = false,dataType = "int",paramType="form"),
    })
    public Object readHouseList(Long communityId,Long floorId,Integer readEstateHouse) {
        parameterIsNull(communityId,"社区id不能为空");
        Wrapper<UnitHouse> wapper =  new EntityWrapper<UnitHouse>().eq("community_id",communityId);
        wapper.eq("floor_id",floorId);
        wapper.eq("is_show_app",1);
        List<UnitHouse> unitList = unitHouseService.selectList(wapper);
        if(null==unitList){unitList=new ArrayList<UnitHouse>();}
        if(null!=readEstateHouse&&readEstateHouse==1){
            Wrapper<UnitHouse> estateHouseWp= new EntityWrapper<UnitHouse>()
                    .eq("community_id",communityId).eq("is_estate_house",1)
                    .orderBy("house_id",true);
            UnitHouse estateHouse =  unitHouseService.selectOne(estateHouseWp);
            if(estateHouse==null){
                return R.error(400,"未查询到该社区信息");
            }
            unitList.add(0,estateHouse);
        }
        return R.ok().put("data",unitList).putDescription(UnitHouse.class);
    }
    @ApiOperation(value = "查询社区物业办公室")
    @RequestMapping(value = "/readEstateHouse",method =RequestMethod.POST)
    @ApiImplicitParams({
            @ApiImplicitParam(name="communityId",value="小区id",required = true,dataType = "long",paramType="form"),
    })
    public Object readEstateHouse(Long communityId) {
        parameterIsNull(communityId,"社区id不能为空");
        Map<String,Object> paramMap = new HashMap<String, Object>();
        Wrapper<UnitHouse> estateHouseWp= new EntityWrapper<UnitHouse>()
                .eq("community_id",communityId).eq("is_estate_house",1)
                .orderBy("house_id",true);
        UnitHouse estateHouse =  unitHouseService.selectOne(estateHouseWp);
        if(estateHouse==null){
            return R.error(400,"未查询到该社区信息");
        }
        return R.ok().put("data",estateHouse).putDescription(UnitHouse.class);
    }

    @ApiOperation(value = "查询小区下的单元丶楼层丶户室")
    @RequestMapping(value = "/readUnitAndFloorAndHouse",method =RequestMethod.POST)
    @ApiImplicitParams({
            @ApiImplicitParam(name="communityId",value="小区id",required = true,dataType = "long",paramType="form"),
            @ApiImplicitParam(name="readEstateHouse",value="是(1)/否(2-默认)查询物业办公室",required = false,dataType = "int",paramType="form"),
    })
    public Object readUnitAndFloorAndHouse(Long communityId,Integer readEstateHouse) {
        parameterIsNull(communityId,"社区id不能为空");
        Map<String,Object> paramMap = new HashMap<String, Object>();
        paramMap.put("communityId",communityId);
        List<Map<String,Object>> mapList = unitHouseService.selectUnitFloorHouse(paramMap);
        if(mapList==null){mapList = new ArrayList<Map<String, Object>>();}
        StringBuilder description = new StringBuilder("{estateHouse:物业办公室数据,空字符串代表没有物业办公室},{");
        description.append("unitId:单元id,unitName:单元名称,communityId:社区id,unitNum:单元编号,communityNum:社区编号,agencyId:物业机构id,");
        description.append("floorId:楼层id,floorName:楼层名称,floor_sort:楼层编号,");
        description.append("houseId:户室id,houseNumber:户室名称,isEstateHouse:是(1)/否(2)为物业办公室,houseSort:户室编号,");
        description.append("}");
        if(null!=readEstateHouse&&readEstateHouse==1){
            Map<String,Object> estateHouseMap = unitHouseService.readEstateHouse(communityId);
            return R.ok().put("data",mapList).put("estateHouse",estateHouseMap).putDescription(description.toString());
        }
        return R.ok().put("data",mapList).put("estateHouse","").putDescription(description.toString());
    }
    @ApiOperation(value = "查询单元下的楼层丶户室")
    @RequestMapping(value = "/readFloorAndHouse",method =RequestMethod.POST)
    @ApiImplicitParams({
            @ApiImplicitParam(name="communityId",value="小区id",required = true,dataType = "long",paramType="form"),
            @ApiImplicitParam(name="unitId",value="单元id",required = true,dataType = "long",paramType="form"),
            @ApiImplicitParam(name="readEstateHouse",value="是(1)/否(2-默认)查询物业办公室",required = false,dataType = "int",paramType="form"),
    })
    public Object readFloorAndHouse(Long communityId,Long unitId,Integer readEstateHouse) {
        parameterIsNull(communityId,"社区id不能为空");
        parameterIsNull(unitId,"单元id不能为空");
        Map<String,Object> paramMap = new HashMap<String, Object>();
        paramMap.put("communityId",communityId);
        paramMap.put("unitId",unitId);
        List<Map<String,Object>> mapList = unitHouseService.selectUnitFloorHouse(paramMap);
        if(mapList==null||mapList.isEmpty()){
            return R.error(400,"未查询到该单元信息");
        }
        List<Map<String,Object>> floorMaps = (List<Map<String,Object>>)mapList.get(0).get("floors");
        if(floorMaps==null){
            floorMaps = new ArrayList<Map<String, Object>>();
        }
        StringBuilder description = new StringBuilder("{estateHouse:物业办公室数据,空字符串代表没有物业办公室},{");
        description.append("floorId:楼层id,floorName:楼层名称,floor_sort:楼层编号,unitId:单元id,communityId:社区id,agencyId:物业机构id,");
        description.append("houseId:户室id,houseNumber:户室名称,isEstateHouse:是(1)/否(2)为物业办公室,houseSort:户室编号,");
        description.append("}");
        if(null!=readEstateHouse&&readEstateHouse==1){
            Map<String,Object> estateHouseMap = unitHouseService.readEstateHouse(communityId);
            return R.ok().put("data",floorMaps).put("estateHouse",estateHouseMap).putDescription(description.toString());
        }
        return R.ok().put("data",floorMaps).put("estateHouse","").putDescription(description.toString());
    }

}
