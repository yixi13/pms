package com.pms.api;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.pms.cache.utils.RedisCacheUtil;
import com.pms.controller.BaseController;
import com.pms.entity.EstateLockCard;
import com.pms.exception.R;
import com.pms.service.IEstateDoorCardService;
import com.pms.service.IEstateDoorLockService;
import com.pms.service.IEstateLockCardService;
import com.pms.service.IOpenDoorRecordService;
import com.pms.util.DaHaoLockUtil;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import net.sf.json.JSONArray;
import net.sf.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.text.SimpleDateFormat;
import java.util.*;

@RestController
@Api(value = "测试/工具类接口", description = "测试/工具类接口")
@RequestMapping(value = "testUtil", method = RequestMethod.POST)
public class TestUtilApi extends BaseController {
    @Autowired
    private DaHaoLockUtil daHaoLockUtil;
    @Autowired
    private RedisCacheUtil redisCacheUtil;
    @Autowired
    private IOpenDoorRecordService openDoorRecordService;
    @Autowired
    private IEstateLockCardService estateLockCardService;
    @Autowired
    private IEstateDoorCardService iEstateDoorCardService;

    @ApiOperation(value = "根据缓存键pre清除缓存")
    @RequestMapping(value = "/clearCache",method =RequestMethod.POST)
    @ApiImplicitParams({
            @ApiImplicitParam(name="cachePre",value="缓存键pre",required = true,dataType = "string",paramType="form"),
    })
    public R clearCache(String cachePre) {
        redisCacheUtil.clearOtherCacleByPre(cachePre);
        return R.ok();
    }

    @ApiOperation(value = "测试添加开门次数")
    @RequestMapping(value = "/addOpenNum",method =RequestMethod.POST)
    @ApiImplicitParams({
            @ApiImplicitParam(name="eqSn",value="设备id",required = true,dataType = "string",paramType="form"),
    })
   public R addOpenNum(String eqSn) {
//        daHaoLockUtil.addOpenDoorRecord(eqSn,new Date());
        daHaoLockUtil.saveOpenDoorNumBySqlFunction(eqSn);
        return R.ok();
   }

//    @ApiOperation(value = "测试添加发卡记录")
//    @RequestMapping(value = "/addCardRecord",method =RequestMethod.POST)
//    @ApiImplicitParams({
//            @ApiImplicitParam(name="eqSn",value="设备id",required = true,dataType = "string",paramType="form"),
//    })
//    public R addCardRecord(String eqSn) {
////        daHaoLockUtil.addOpenDoorRecord(eqSn,new Date());
//        List<EstateLockCard> list = estateLockCardService.selectList(new EntityWrapper<EstateLockCard>().eq("eq_sn",eqSn));
//        if(list!=null){
//            List<String> strs = new ArrayList<String>();
//            int z= list.size();
////            if(z>1){z=1;}
//            for(int x=0;x<z;x++){
//                strs.add(list.get(x).getCardNumSixteen());
//            }
//            daHaoLockUtil.saveSendCardRecordByFunction(eqSn,strs);
//        }
//        return R.ok();
//    }
    @ApiOperation(value = "查询开门次数-空参查总开门次数")
    @RequestMapping(value = "/reanOpenDoorNum",method =RequestMethod.POST)
    @ApiImplicitParams({
            @ApiImplicitParam(name="conmunityId",value="社区id",required = false,dataType = "long",paramType="form"),
            @ApiImplicitParam(name="agencyId",value="物业id",required = false,dataType = "long",paramType="form"),
            @ApiImplicitParam(name="doorId",value="门id",required = false,dataType = "long",paramType="form"),
    })
    public R reanOpenDoorNum(Long conmunityId,Long agencyId,Long doorId) {
        Long openDoorNum = openDoorRecordService.reanOpenDoorNum(agencyId,conmunityId,doorId);
        if(openDoorNum==null){
            openDoorNum = 0L;
        }
        return R.ok().put("data",openDoorNum);
    }
    @RequestMapping(value = "/test",method =RequestMethod.POST)
    public R test(String A,String B){
//        iEstateDoorCardService.saveSendCardRecordByFunction(A, B);
        return R.ok();
    }

}
