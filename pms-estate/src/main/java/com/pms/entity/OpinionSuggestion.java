package com.pms.entity;
import com.baomidou.mybatisplus.activerecord.Model;
import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.persistence.*;
import com.pms.util.Description;
import com.pms.entity.BaseModel;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;


/**
 * 
 * 
 * @author zyl
 * @email 517770986@qq.com
 * @date 2017-11-02 14:10:56
 */
@TableName( "opinion_suggestion")
public class OpinionSuggestion extends BaseModel<OpinionSuggestion> {
private static final long serialVersionUID = 1L;


	    //物业投诉表
    @TableId("opinion_id")
    private Long opinionId;
	
	    //会员ID
    @TableField("member_id")
    @Description("会员ID")
    private Long memberId;
	
	    //机构ID
    @TableField("agency_id")
    @Description("机构ID")
    private Long agencyId;
	
	    //房间ID
    @TableField("house_id")
    @Description("房间ID")
    private Long houseId;
	
	    //业主反馈内容
    @TableField("content")
    @Description("业主反馈内容")
    private String content;
	
	    //标题
    @TableField("equipment_name")
    @Description("标题")
    private String equipmentName;
	
	    //建议反馈时间
    @TableField("recovery_time")
    @Description("建议反馈时间")
    private Date recoveryTime;
	
	    //1.已回复2.待回复
    @TableField("state")
    @Description("1.已回复2.待回复")
    private Integer state;
	
	    //创建时间
    @TableField("create_time")
    @Description("创建时间")
    private Date createTime;


	@TableField("house_name")
	@Description("房屋信息")
	private String houseName;

	@TableField(exist = false)
	private List<OpinionImg> opinionImg;
	@TableField(exist = false)
	private List<OpinionEvalua>	opinionEvalua;
	@TableField(exist = false)
	private List<UnitHouse> unitHouse;


	public String getHouseName() {
		return houseName;
	}

	public void setHouseName(String houseName) {
		this.houseName = houseName;
	}

	public List<OpinionImg> getOpinionImg() {
		return opinionImg;
	}

	public List<OpinionEvalua> getOpinionEvalua() {
		return opinionEvalua;
	}

	public List<UnitHouse> getUnitHouse() {
		return unitHouse;
	}

	public void setOpinionImg(List<OpinionImg> opinionImg) {
		this.opinionImg = opinionImg;
	}

	public void setOpinionEvalua(List<OpinionEvalua> opinionEvalua) {
		this.opinionEvalua = opinionEvalua;
	}

	public void setUnitHouse(List<UnitHouse> unitHouse) {
		this.unitHouse = unitHouse;
	}

	// ID赋值
public OpinionSuggestion(){
        this.opinionId= returnIdLong();
        }
@Override
protected Serializable pkVal() {
        return this.opinionId;
}
	/**
	 * 设置：物业投诉表
	 */
	public void setOpinionId(Long opinionId) {
		this.opinionId = opinionId;
	}
	/**
	 * 获取：物业投诉表
	 */
	public Long getOpinionId() {
		return opinionId;
	}
	/**
	 * 设置：会员ID
	 */
	public void setMemberId(Long memberId) {
		this.memberId = memberId;
	}
	/**
	 * 获取：会员ID
	 */
	public Long getMemberId() {
		return memberId;
	}
	/**
	 * 设置：机构ID
	 */
	public void setAgencyId(Long agencyId) {
		this.agencyId = agencyId;
	}
	/**
	 * 获取：机构ID
	 */
	public Long getAgencyId() {
		return agencyId;
	}
	/**
	 * 设置：房间ID
	 */
	public void setHouseId(Long houseId) {
		this.houseId = houseId;
	}
	/**
	 * 获取：房间ID
	 */
	public Long getHouseId() {
		return houseId;
	}
	/**
	 * 设置：业主反馈内容
	 */
	public void setContent(String content) {
		this.content = content;
	}
	/**
	 * 获取：业主反馈内容
	 */
	public String getContent() {
		return content;
	}
	/**
	 * 设置：标题
	 */
	public void setEquipmentName(String equipmentName) {
		this.equipmentName = equipmentName;
	}
	/**
	 * 获取：标题
	 */
	public String getEquipmentName() {
		return equipmentName;
	}
	/**
	 * 设置：建议反馈时间
	 */
	public void setRecoveryTime(Date recoveryTime) {
		this.recoveryTime = recoveryTime;
	}
	/**
	 * 获取：建议反馈时间
	 */
	public Date getRecoveryTime() {
		return recoveryTime;
	}
	/**
	 * 设置：1.已回复2.待回复
	 */
	public void setState(Integer state) {
		this.state = state;
	}
	/**
	 * 获取：1.已回复2.待回复
	 */
	public Integer getState() {
		return state;
	}
	/**
	 * 设置：创建时间
	 */
	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}
	/**
	 * 获取：创建时间
	 */
	public Date getCreateTime() {
		return createTime;
	}


}
