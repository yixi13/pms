package com.pms.entity;

import com.baomidou.mybatisplus.activerecord.Model;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.*;

import com.pms.util.Description;
import com.pms.entity.BaseModel;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;


/**
 * @author zyl
 * @email 517770986@qq.com
 * @date 2017-11-07 10:15:34
 */
@TableName("charge_value")
public class ChargeValue extends BaseModel<ChargeValue> {
    private static final long serialVersionUID = 1L;
    /**
     * 收费值id
     */
    @TableId("charge_value_id")
    private Long chargeValueId;

    /**
     * 费用名称id
     */
    @TableField("charge_name_id")
    @Description("费用名称id")
    private Long chargeNameId;

    /**
     * 收费名称
     */
    @TableField("charge_name")
    @Description("收费名称")
    private String chargeName;

    /**
     * 费用值
     */
    @TableField("charge_value")
    @Description("费用值")
    private Double chargeValue;

    /**
     * 社区id
     */
    @TableField("community_id")
    @Description("社区id")
    private Long communityId;

    /**
     * 费用计算方式(1-建筑面积计算,2-固定费用,3-水电气使用量计算)
     */
    @TableField("charge_way")
    @Description("费用计算方式(1-建筑面积计算,2-固定费用,3-水电气使用量计算)")
    private Integer chargeWay;

    /**
     * 费用类型(1-周期性收费-每月生成,2-临时性收费-某几个月)
     */
    @TableField("charge_type")
    @Description("费用类型(1-周期性收费-每月生成,2-临时性收费-某几个月)")
    private Integer chargeType;

    /**
     * 临时性收费起始时间
     */
    @TableField("charge_start_time")
    @Description("临时性收费起始时间")
    private Date chargeStartTime;

    /**
     * 临时性收费截止时间
     */
    @TableField("charge_end_time")
    @Description("临时性收费截止时间")
    private Date chargeEndTime;

    /**
     * 物业费模板id
     */
    @TableField("charge_template_id")
    @Description("物业费模板id")
    private Long chargeTemplateId;
    @Description("物业机构id")
    @TableField("agency_id")
    private Long agencyId;

    public Long getAgencyId() {
        return agencyId;
    }

    public void setAgencyId(Long agencyId) {
        this.agencyId = agencyId;
    }
    // ID赋值
    public ChargeValue() {
        this.chargeValueId = returnIdLong();
    }

    @Override
    protected Serializable pkVal() {
        return this.chargeValueId;
    }

    /**
     * 设置：收费值id
     */
    public ChargeValue setChargeValueId(Long chargeValueId) {
        this.chargeValueId = chargeValueId;
        return this;
    }

    /**
     * 获取：收费值id
     */
    public Long getChargeValueId() {
        return chargeValueId;
    }

    /**
     * 设置：费用名称id
     */
    public ChargeValue setChargeNameId(Long chargeNameId) {
        this.chargeNameId = chargeNameId;
        return this;
    }

    /**
     * 获取：费用名称id
     */
    public Long getChargeNameId() {
        return chargeNameId;
    }

    /**
     * 设置：收费名称
     */
    public ChargeValue setChargeName(String chargeName) {
        this.chargeName = chargeName;
        return this;
    }

    /**
     * 获取：收费名称
     */
    public String getChargeName() {
        return chargeName;
    }

    /**
     * 设置：费用值
     */
    public ChargeValue setChargeValue(Double chargeValue) {
        this.chargeValue = chargeValue;
        return this;
    }

    /**
     * 获取：费用值
     */
    public Double getChargeValue() {
        return chargeValue;
    }

    /**
     * 设置：社区id
     */
    public ChargeValue setCommunityId(Long communityId) {
        this.communityId = communityId;
        return this;
    }

    /**
     * 获取：社区id
     */
    public Long getCommunityId() {
        return communityId;
    }

    /**
     * 设置：费用计算方式(1-建筑面积计算,2-固定费用,3-水电气使用量计算)
     */
    public ChargeValue setChargeWay(Integer chargeWay) {
        this.chargeWay = chargeWay;
        return this;
    }

    /**
     * 获取：费用计算方式(1-建筑面积计算,2-固定费用,3-水电气使用量计算)
     */
    public Integer getChargeWay() {
        return chargeWay;
    }

    /**
     * 设置：费用类型(1-周期性收费-每月生成,2-临时性收费-某几个月)
     */
    public ChargeValue setChargeType(Integer chargeType) {
        this.chargeType = chargeType;
        return this;
    }

    /**
     * 获取：费用类型(1-周期性收费-每月生成,2-临时性收费-某几个月)
     */
    public Integer getChargeType() {
        return chargeType;
    }

    /**
     * 设置：临时性收费起始时间
     */
    public ChargeValue setChargeStartTime(Date chargeStartTime) {
        this.chargeStartTime = chargeStartTime;
        return this;
    }

    /**
     * 获取：临时性收费起始时间
     */
    public Date getChargeStartTime() {
        return chargeStartTime;
    }

    /**
     * 设置：临时性收费截止时间
     */
    public ChargeValue setChargeEndTime(Date chargeEndTime) {
        this.chargeEndTime = chargeEndTime;
        return this;
    }

    /**
     * 获取：临时性收费截止时间
     */
    public Date getChargeEndTime() {
        return chargeEndTime;
    }

    /**
     * 设置：物业费模板id
     */
    public ChargeValue setChargeTemplateId(Long chargeTemplateId) {
        this.chargeTemplateId = chargeTemplateId;
        return this;
    }

    /**
     * 获取：物业费模板id
     */
    public Long getChargeTemplateId() {
        return chargeTemplateId;
    }


}
