package com.pms.entity;

import com.baomidou.mybatisplus.activerecord.Model;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.*;

import com.pms.util.Description;
import com.pms.entity.BaseModel;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;


/**
 * @author zyl
 * @email 517770986@qq.com
 * @date 2017-11-09 14:43:35
 */
@TableName("entrance_door")
public class EntranceDoor extends BaseModel<EntranceDoor> {
    private static final long serialVersionUID = 1L;


    /**
     * 門id
     */
    @TableId("door_id")
    @Description("门id名称")
    private Long doorId;

    /**
     * 大门名称
     */
    @TableField("door_name")
    @Description("大门名称")
    private String doorName;

    /**
     * 门类型 1 大门 2 单元门 3 负楼层门
     */
    @TableField("door_type")
    @Description("门类型 1 大门 2 单元门 3 负楼层门")
    private Integer doorType;

    /**
     * 单元门ID
     */
    @TableField("unit_id")
    @Description("单元ID")
    private Long unitId;

    /**
     * 社区ID
     */
    @TableField("community_id")
    @Description("社区ID")
    private Long communityId;

    /**
     * 备注 描述
     */
    @TableField("door_remarks")
    @Description("备注 描述")
    private String doorRemarks;

    /**
     * 单元编号
     */
    @TableField("unit_num")
    @Description("单元编号")
    private Integer unitNum;

    /**
     * 所属社区编号
     */
    @TableField("community_num")
    @Description("所属社区编号")
    private Integer communityNum;

    /**
     * 单元名称
     */
    @TableField("unit_name")
    @Description("单元名称")
    private String unitName;

    /**
     * 物业机构id
     */
    @TableField("agency_id")
    @Description("物业机构id")
    private Long agencyId;

    @TableField("is_have_lock")
    @Description("是(1)/否(2)绑定锁")
    private Integer isHaveLock;
    // ID赋值
    public EntranceDoor() {
        this.doorId = returnIdLong();
    }

    @Override
    protected Serializable pkVal() {
        return this.doorId;
    }

    public Integer getIsHaveLock() {
        return isHaveLock;
    }

    public EntranceDoor setIsHaveLock(Integer isHaveLock) {
        this.isHaveLock = isHaveLock;
        return this;
    }

    /**
     * 设置：
     */
    public EntranceDoor setDoorId(Long doorId) {
        this.doorId = doorId;
        return this;
    }

    /**
     * 获取：
     */
    public Long getDoorId() {
        return doorId;
    }

    /**
     * 设置：大门名称
     */
    public EntranceDoor setDoorName(String doorName) {
        this.doorName = doorName;
        return this;
    }

    /**
     * 获取：大门名称
     */
    public String getDoorName() {
        return doorName;
    }

    /**
     * 设置：门类型 1 大门 2 单元门 3 负楼层门
     */
    public EntranceDoor setDoorType(Integer doorType) {
        this.doorType = doorType;
        return this;
    }

    /**
     * 获取：门类型 1 大门 2 单元门 3 负楼层门
     */
    public Integer getDoorType() {
        return doorType;
    }

    /**
     * 设置：单元门ID
     */
    public EntranceDoor setUnitId(Long unitId) {
        this.unitId = unitId;
        return this;
    }

    /**
     * 获取：单元门ID
     */
    public Long getUnitId() {
        return unitId;
    }

    /**
     * 设置：社区ID
     */
    public EntranceDoor setCommunityId(Long communityId) {
        this.communityId = communityId;
        return this;
    }

    /**
     * 获取：社区ID
     */
    public Long getCommunityId() {
        return communityId;
    }

    /**
     * 设置：备注 描述
     */
    public EntranceDoor setDoorRemarks(String doorRemarks) {
        this.doorRemarks = doorRemarks;
        return this;
    }

    /**
     * 获取：备注 描述
     */
    public String getDoorRemarks() {
        return doorRemarks;
    }

    /**
     * 设置：单元编号
     */
    public EntranceDoor setUnitNum(Integer unitNum) {
        this.unitNum = unitNum;
        return this;
    }

    /**
     * 获取：单元编号
     */
    public Integer getUnitNum() {
        return unitNum;
    }

    /**
     * 设置：所属社区编号
     */
    public EntranceDoor setCommunityNum(Integer communityNum) {
        this.communityNum = communityNum;
        return this;
    }

    /**
     * 获取：所属社区编号
     */
    public Integer getCommunityNum() {
        return communityNum;
    }

    /**
     * 设置：单元名称
     */
    public EntranceDoor setUnitName(String unitName) {
        this.unitName = unitName;
        return this;
    }

    /**
     * 获取：单元名称
     */
    public String getUnitName() {
        return unitName;
    }

    /**
     * 设置：物业机构id
     */
    public EntranceDoor setAgencyId(Long agencyId) {
        this.agencyId = agencyId;
        return this;
    }

    /**
     * 获取：物业机构id
     */
    public Long getAgencyId() {
        return agencyId;
    }


}
