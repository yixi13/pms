package com.pms.entity;

import com.baomidou.mybatisplus.activerecord.Model;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.*;

import com.pms.util.Description;
import com.pms.entity.BaseModel;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;


/**
 * 录入的卡片
 * @author zyl
 * @email 517770986@qq.com
 * @date 2017-11-27 17:37:54
 */
@TableName("estate_unit_card")
public class EstateUnitCard extends BaseModel<EstateUnitCard> {
    private static final long serialVersionUID = 1L;


    /**
     *id
     */
    @TableId("unit_card_id")
    private Long unitCardId;

    /**
     * 16进制卡号
     */
    @TableField("card_num_sixteen")
    @Description("16进制卡号")
    private String cardNumSixteen;

    /**
     * 10进制卡号
     */
    @TableField("card_num_ten")
    @Description("10进制卡号")
    private String cardNumTen;

    /**
     * 单元id
     */
    @TableField("unit_id")
    @Description("单元id")
    private Long unitId;

    /**
     *
     */
    @TableField("unit_name")
    @Description("")
    private String unitName;

    /**
     * 社区id
     */
    @TableField("community_id")
    @Description("社区id")
    private Long communityId;

    /**
     * 社区名称
     */
    @TableField("community_name")
    @Description("社区名称")
    private String communityName;

    /**
     * 厂家编号
     */
    @TableField("factory_code")
    @Description("厂家编号")
    private String factoryCode;

    /**
     * 创建时间
     */
    @TableField("create_time")
    @Description("创建时间")
    private Date createTime;

    // ID赋值
    public EstateUnitCard() {
        this.unitCardId = returnIdLong();
    }

    @Override
    protected Serializable pkVal() {
        return this.unitCardId;
    }

    /**
     * 设置：
     */
    public EstateUnitCard setUnitCardId(Long unitCardId) {
        this.unitCardId = unitCardId;
        return this;
    }

    /**
     * 获取：
     */
    public Long getUnitCardId() {
        return unitCardId;
    }

    /**
     * 设置：16进制卡号
     */
    public EstateUnitCard setCardNumSixteen(String cardNumSixteen) {
        this.cardNumSixteen = cardNumSixteen;
        return this;
    }

    /**
     * 获取：16进制卡号
     */
    public String getCardNumSixteen() {
        return cardNumSixteen;
    }

    /**
     * 设置：10进制卡号
     */
    public EstateUnitCard setCardNumTen(String cardNumTen) {
        this.cardNumTen = cardNumTen;
        return this;
    }

    /**
     * 获取：10进制卡号
     */
    public String getCardNumTen() {
        return cardNumTen;
    }

    /**
     * 设置：单元id
     */
    public EstateUnitCard setUnitId(Long unitId) {
        this.unitId = unitId;
        return this;
    }

    /**
     * 获取：单元id
     */
    public Long getUnitId() {
        return unitId;
    }

    /**
     * 设置：
     */
    public EstateUnitCard setUnitName(String unitName) {
        this.unitName = unitName;
        return this;
    }

    /**
     * 获取：
     */
    public String getUnitName() {
        return unitName;
    }

    /**
     * 设置：社区id
     */
    public EstateUnitCard setCommunityId(Long communityId) {
        this.communityId = communityId;
        return this;
    }

    /**
     * 获取：社区id
     */
    public Long getCommunityId() {
        return communityId;
    }

    /**
     * 设置：社区名称
     */
    public EstateUnitCard setCommunityName(String communityName) {
        this.communityName = communityName;
        return this;
    }

    /**
     * 获取：社区名称
     */
    public String getCommunityName() {
        return communityName;
    }

    /**
     * 设置：厂家编号
     */
    public EstateUnitCard setFactoryCode(String factoryCode) {
        this.factoryCode = factoryCode;
        return this;
    }

    /**
     * 获取：厂家编号
     */
    public String getFactoryCode() {
        return factoryCode;
    }

    /**
     * 设置：创建时间
     */
    public EstateUnitCard setCreateTime(Date createTime) {
        this.createTime = createTime;
        return this;
    }

    /**
     * 获取：创建时间
     */
    public Date getCreateTime() {
        return createTime;
    }


}
