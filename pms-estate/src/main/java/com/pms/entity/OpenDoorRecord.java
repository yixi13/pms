package com.pms.entity;

import com.baomidou.mybatisplus.activerecord.Model;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.*;

import com.pms.util.Description;
import com.pms.entity.BaseModel;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;


/**
 * @author zyl
 * @email 517770986@qq.com
 * @date 2017-12-12 10:42:50
 */
@TableName("open_door_record")
public class OpenDoorRecord extends BaseModel<OpenDoorRecord> {
    private static final long serialVersionUID = 1L;


    /**
     * 记录id
     */
    @TableId("record_id")
    @Description("记录id")
    private Long recordId;

    /**
     * 社区id
     */
    @TableField("conmunity_id")
    @Description("社区id")
    private Long conmunityId;

    /**
     * 门id
     */
    @TableField("door_id")
    @Description("门id")
    private Long doorId;

    /**
     * 物业机构id
     */
    @TableField("agency_id")
    @Description("物业机构id")
    private Long agencyId;

    /**
     * 开门次数统计
     */
    @TableField("open_num")
    @Description("开门次数统计")
    private Long openNum;

    /**
     * 最后一次开门时间
     */
    @TableField("last_open_time")
    @Description("最后一次开门时间")
    private Date lastOpenTime;

    // ID赋值
    public OpenDoorRecord() {
        this.recordId = returnIdLong();
    }

    @Override
    protected Serializable pkVal() {
        return this.recordId;
    }

    /**
     * 设置：记录id
     */
    public OpenDoorRecord setRecordId(Long recordId) {
        this.recordId = recordId;
        return this;
    }

    /**
     * 获取：记录id
     */
    public Long getRecordId() {
        return recordId;
    }

    /**
     * 设置：社区id
     */
    public OpenDoorRecord setConmunityId(Long conmunityId) {
        this.conmunityId = conmunityId;
        return this;
    }

    /**
     * 获取：社区id
     */
    public Long getConmunityId() {
        return conmunityId;
    }

    /**
     * 设置：门id
     */
    public OpenDoorRecord setDoorId(Long doorId) {
        this.doorId = doorId;
        return this;
    }

    /**
     * 获取：门id
     */
    public Long getDoorId() {
        return doorId;
    }

    /**
     * 设置：物业机构id
     */
    public OpenDoorRecord setAgencyId(Long agencyId) {
        this.agencyId = agencyId;
        return this;
    }

    /**
     * 获取：物业机构id
     */
    public Long getAgencyId() {
        return agencyId;
    }

    /**
     * 设置：开门次数统计
     */
    public OpenDoorRecord setOpenNum(Long openNum) {
        this.openNum = openNum;
        return this;
    }

    /**
     * 获取：开门次数统计
     */
    public Long getOpenNum() {
        return openNum;
    }

    /**
     * 设置：最后一次开门时间
     */
    public OpenDoorRecord setLastOpenTime(Date lastOpenTime) {
        this.lastOpenTime = lastOpenTime;
        return this;
    }

    /**
     * 获取：最后一次开门时间
     */
    public Date getLastOpenTime() {
        return lastOpenTime;
    }


}
