package com.pms.entity;

import java.util.Date;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableName;
import com.pms.util.Description;

import java.io.Serializable;

/**
 * <p>
 * 
 * </p>
 *
 * @author ASUS_B
 * @since 2017-10-26
 */
@TableName("estate_agency")
public class EstateAgency extends BaseModel<EstateAgency> {

    private static final long serialVersionUID = 1L;
	public EstateAgency(){this.agencyId = returnIdLong();}
    /**
     * 物业机构id
     */
    @TableId("agency_id")
	@Description("物业机构id")
	private Long agencyId;

	/**
	 * 机构状态(1-启用，2-禁用)
	 */
	@TableField("agency_state")
	@Description("机构状态(1-启用，2-禁用)")
	private Integer agencyState;
    /**
     * 机构名称
     */
	@TableField("agency_name")
	@Description("机构名称")
	private String agencyName;
    /**
     * 机构类型(1物业|2社区)
     */
	@TableField("agency_type")
	@Description("机构类型(1物业|2社区)")
	private Integer agencyType;
    /**
     * 父级机构id
     */
	@TableField("parent_id")
	@Description("父级机构id")
	private Long parentId;
    /**
     * 机构所在地址id(省-市-区)
     */
	@TableField("area_id")
	@Description("机构所在地址id(省-市-区)")
	private String areaId;
    /**
     * 地址经度
     */
	@TableField("area_longitude")
	@Description("地址经度")
	private Double areaLongitude;
    /**
     * 地址纬度
     */
	@TableField("area_latitude")
	@Description("地址纬度")
	private Double areaLatitude;
    /**
     * 对应LBS数据id
     */
	@TableField("lbs_poi_id")
	@Description("对应LBS数据id")
	private String lbsPoiId;
    /**
     * 机构联系电话-手机
     */
	@TableField("agency_phone")
	@Description("机构联系电话-手机")
	private String agencyPhone;
    /**
     * 机构联系电话-座机
     */
	@TableField("agency_telphone")
	@Description("机构联系电话-座机")
	private String agencyTelphone;
    /**
     * 地址编号对应的机构地址
     */
	@TableField("agency_address")
	@Description("地址编号对应的机构地址")
	private String agencyAddress;
	/**
	 * 详细地址(补充信息)
	 */
	@TableField("agency_address_detail")
	@Description("详细地址(补充信息)")
	private String agencyAddressDetail;

    /**
     * 机构主图url
     */
	@TableField("agency_img")
	@Description("机构主图url")
	private String agencyImg;
    /**
     * 社区编号(锁设备使用)
     */
	@TableField("community_num")
	@Description("社区编号(锁设备使用)")
	private Integer communityNum;
    /**
     * 社区密码
     */
	@TableField("community_password")
	@Description("社区密码")
	private String communityPassword;
    /**
     * 创建人
     */
	@TableField("create_by")
	@Description("创建人")
	private String createBy;
    /**
     * 创建时间
     */
	@TableField("create_time")
	@Description("创建时间")
	private Date createTime;
    /**
     * 修改人
     */
	@TableField("update_by")
	@Description("修改人")
	private String updateBy;
    /**
     * 修改时间
     */
	@TableField("update_time")
	@Description("修改时间")
	private Date updateTime;

	/**
	 * 社区-物业费查询方式：1.按年 2.按半年 3.按季度 4.按月
	 */
	@TableField("estate_bill_mode")
	@Description("物业费查询方式")
	private Integer estateBillMode;

	public Integer getEstateBillMode() {
		return estateBillMode;
	}

	public void setEstateBillMode(Integer estateBillMode) {
		this.estateBillMode = estateBillMode;
	}

	public String getAgencyAddressDetail() {
		return agencyAddressDetail;
	}

	public void setAgencyAddressDetail(String agencyAddressDetail) {
		this.agencyAddressDetail = agencyAddressDetail;
	}

	public Long getAgencyId() {
		return agencyId;
	}

	public void setAgencyId(Long agencyId) {
		this.agencyId = agencyId;
	}

	public String getAgencyName() {
		return agencyName;
	}

	public void setAgencyName(String agencyName) {
		this.agencyName = agencyName;
	}

	public Integer getAgencyType() {
		return agencyType;
	}

	public void setAgencyType(Integer agencyType) {
		this.agencyType = agencyType;
	}

	public Long getParentId() {
		return parentId;
	}

	public void setParentId(Long parentId) {
		this.parentId = parentId;
	}

	public String getAreaId() {
		return areaId;
	}

	public void setAreaId(String areaId) {
		this.areaId = areaId;
	}

	public Double getAreaLongitude() {
		return areaLongitude;
	}

	public void setAreaLongitude(Double areaLongitude) {
		this.areaLongitude = areaLongitude;
	}

	public Double getAreaLatitude() {
		return areaLatitude;
	}

	public void setAreaLatitude(Double areaLatitude) {
		this.areaLatitude = areaLatitude;
	}

	public String getLbsPoiId() {
		return lbsPoiId;
	}

	public Integer getAgencyState() {
		return agencyState;
	}

	public void setAgencyState(Integer agencyState) {
		this.agencyState = agencyState;
	}

	public void setLbsPoiId(String lbsPoiId) {
		this.lbsPoiId = lbsPoiId;
	}

	public String getAgencyPhone() {
		return agencyPhone;
	}

	public void setAgencyPhone(String agencyPhone) {
		this.agencyPhone = agencyPhone;
	}

	public String getAgencyTelphone() {
		return agencyTelphone;
	}

	public void setAgencyTelphone(String agencyTelphone) {
		this.agencyTelphone = agencyTelphone;
	}

	public String getAgencyAddress() {
		return agencyAddress;
	}

	public void setAgencyAddress(String agencyAddress) {
		this.agencyAddress = agencyAddress;
	}

	public String getAgencyImg() {
		return agencyImg;
	}

	public void setAgencyImg(String agencyImg) {
		this.agencyImg = agencyImg;
	}

	public Integer getCommunityNum() {
		return communityNum;
	}

	public void setCommunityNum(Integer communityNum) {
		this.communityNum = communityNum;
	}

	public String getCommunityPassword() {
		return communityPassword;
	}

	public void setCommunityPassword(String communityPassword) {
		this.communityPassword = communityPassword;
	}

	public String getCreateBy() {
		return createBy;
	}

	public void setCreateBy(String createBy) {
		this.createBy = createBy;
	}

	public Date getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	public String getUpdateBy() {
		return updateBy;
	}

	public void setUpdateBy(String updateBy) {
		this.updateBy = updateBy;
	}

	public Date getUpdateTime() {
		return updateTime;
	}

	public void setUpdateTime(Date updateTime) {
		this.updateTime = updateTime;
	}

	@Override
	protected Serializable pkVal() {
		return this.agencyId;
	}

}
