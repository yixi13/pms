package com.pms.entity;
import java.io.Serializable;
import java.util.Date;
import com.pms.util.Description;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;


/**
 * 
 * 
 * @author zyl
 * @email 517770986@qq.com
 * @date 2017-11-01 14:34:58
 */
@TableName( "repair_img")
public class RepairImg extends BaseModel<RepairImg> {
private static final long serialVersionUID = 1L;


	    //物业报修图片表
    @TableId("img_id")
    private Long imgId;
	
	    //报修ID
    @TableField("repair_id")
    @Description("报修ID")
    private Long repairId;
	
	    //图片
    @TableField("img")
    @Description("图片")
    private String img;
	
	    //
    @TableField("create_by")
    @Description("")
    private String createBy;
	
	    //
    @TableField("create_time")
    @Description("")
    private Date createTime;
	
	    //
    @TableField("update_by")
    @Description("")
    private String updateBy;
	
	    //
    @TableField("update_time")
    @Description("")
    private Date updateTime;
	
// ID赋值
public RepairImg(){
        this.imgId= returnIdLong();
        }
@Override
protected Serializable pkVal() {
        return this.imgId;
}
	/**
	 * 设置：物业报修图片表
	 */
	public void setImgId(Long imgId) {
		this.imgId = imgId;
	}
	/**
	 * 获取：物业报修图片表
	 */
	public Long getImgId() {
		return imgId;
	}
	/**
	 * 设置：报修ID
	 */
	public void setRepairId(Long repairId) {
		this.repairId = repairId;
	}
	/**
	 * 获取：报修ID
	 */
	public Long getRepairId() {
		return repairId;
	}
	/**
	 * 设置：图片
	 */
	public void setImg(String img) {
		this.img = img;
	}
	/**
	 * 获取：图片
	 */
	public String getImg() {
		return img;
	}
	/**
	 * 设置：
	 */
	public void setCreateBy(String createBy) {
		this.createBy = createBy;
	}
	/**
	 * 获取：
	 */
	public String getCreateBy() {
		return createBy;
	}
	/**
	 * 设置：
	 */
	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}
	/**
	 * 获取：
	 */
	public Date getCreateTime() {
		return createTime;
	}
	/**
	 * 设置：
	 */
	public void setUpdateBy(String updateBy) {
		this.updateBy = updateBy;
	}
	/**
	 * 获取：
	 */
	public String getUpdateBy() {
		return updateBy;
	}
	/**
	 * 设置：
	 */
	public void setUpdateTime(Date updateTime) {
		this.updateTime = updateTime;
	}
	/**
	 * 获取：
	 */
	public Date getUpdateTime() {
		return updateTime;
	}


}
