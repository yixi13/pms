package com.pms.entity;

import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;
import com.pms.util.Description;

import java.io.Serializable;


/**
 * 
 * 
 * @author zyl
 * @email 517770986@qq.com
 * @date 2017-11-01 11:52:52
 */
@TableName( "bill_table_detail")
public class BillTableDetail extends BaseModel<BillTableDetail> {
private static final long serialVersionUID = 1L;


	    //
    @TableId("bill_table_detail_id")
    private Long billTableDetailId;
	
	    //账单编号
    @TableField("bill_nu")
    @Description("账单编号")
    private String billNu;
	
	    //模板项目名称
    @TableField("Template_name")
    @Description("模板项目名称")
    private String templateName;
	
	    //单价
    @TableField("price")
    @Description("单价")
    private Double price;
	
	    //项目值
    @TableField("Template_value")
    @Description("项目值")
    private Double templateValue;
	
	    //单项总金额
    @TableField("Total_Price")
    @Description("单项总金额")
    private Double totalPrice;
	
	    //备注
    @TableField("remarks")
    @Description("备注")
    private String remarks;
	
// ID赋值
public BillTableDetail(){
        this.billTableDetailId= returnIdLong();
        }
@Override
protected Serializable pkVal() {
        return this.billTableDetailId;
}
	/**
	 * 设置：
	 */
	public void setBillTableDetailId(Long billTableDetailId) {
		this.billTableDetailId = billTableDetailId;
	}
	/**
	 * 获取：
	 */
	public Long getBillTableDetailId() {
		return billTableDetailId;
	}
	/**
	 * 设置：账单编号
	 */
	public void setBillNu(String billNu) {
		this.billNu = billNu;
	}
	/**
	 * 获取：账单编号
	 */
	public String getBillNu() {
		return billNu;
	}
	/**
	 * 设置：模板项目名称
	 */
	public void setTemplateName(String templateName) {
		this.templateName = templateName;
	}
	/**
	 * 获取：模板项目名称
	 */
	public String getTemplateName() {
		return templateName;
	}
	/**
	 * 设置：单价
	 */
	public void setPrice(Double price) {
		this.price = price;
	}
	/**
	 * 获取：单价
	 */
	public Double getPrice() {
		return price;
	}
	/**
	 * 设置：项目值
	 */
	public void setTemplateValue(Double templateValue) {
		this.templateValue = templateValue;
	}
	/**
	 * 获取：项目值
	 */
	public Double getTemplateValue() {
		return templateValue;
	}
	/**
	 * 设置：单项总金额
	 */
	public void setTotalPrice(Double totalPrice) {
		this.totalPrice = totalPrice;
	}
	/**
	 * 获取：单项总金额
	 */
	public Double getTotalPrice() {
		return totalPrice;
	}
	/**
	 * 设置：备注
	 */
	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}
	/**
	 * 获取：备注
	 */
	public String getRemarks() {
		return remarks;
	}


}
