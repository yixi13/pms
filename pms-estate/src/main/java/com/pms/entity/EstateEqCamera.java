package com.pms.entity;
import com.baomidou.mybatisplus.activerecord.Model;
import java.io.Serializable;
import java.util.Date;
import javax.persistence.*;
import com.pms.util.Description;
import com.pms.entity.BaseModel;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;


/**
 * 
 * 
 * @author zyl
 * @email 517770986@qq.com
 * @date 2017-11-15 11:33:11
 */
@TableName( "estate_eq_camera")
public class EstateEqCamera extends BaseModel<EstateEqCamera> {
private static final long serialVersionUID = 1L;


	    /**
	 *物业摄像头ID
	 */
    @TableId("camera_id")
    private Long cameraId;
	
	    /**
     *摄像头位置
     */
    @TableField("place_")
    @Description("摄像头位置")
    private String place;
	
	    /**
     *设备序列号
     */
    @TableField("key_")
    @Description("设备序列号")
    private String key;
	
	    /**
     *备注
     */
    @TableField("remack_")
    @Description("备注")
    private String remack;
	
	    /**
     *机构ID
     */
    @TableField("agency_id")
    @Description("机构ID")
    private Long agencyId;
	
	    /**
     *设备id
     */
    @TableField("equipment_id")
    @Description("设备id")
    private String equipmentId;
	
	    /**
     *验证码
     */
    @TableField("verification_code")
    @Description("验证码")
    private String verificationCode;
	
	    /**
     *token码
     */
    @TableField("token_")
    @Description("token码")
    private String token;
	
	    /**
     *token结束时间
     */
    @TableField("expire_time")
    @Description("token结束时间")
    private Date expireTime;
	
	    /**
     *当前时间
     */
    @TableField("currentdate_time")
    @Description("当前时间")
    private Date currentdateTime;
	
	    /**
     *楼栋ID
     */
    @TableField("unit_id")
    @Description("楼栋ID")
    private Long unitId;
	
	    /**
     *门ID
     */
    @TableField("door_id")
    @Description("门ID")
    private Long doorId;
	
	    /**
     *社区机构ID
     */
    @TableField("community_id")
    @Description("社区机构ID")
    private Long communityId;
	
	    /**
     *权限状态1公开、2小区所见、3私人所见
     */
    @TableField("privileged_status")
    @Description("权限状态1公开、2小区所见、3私人所见")
    private Integer privilegedStatus;
	
	    /**
     *会员ID
     */
    @TableField("member_id")
    @Description("会员ID")
    private Long memberId;
	
	    /**
     *创建时间
     */
    @TableField("create_time")
    @Description("创建时间")
    private Date createTime;
	
	    /**
     *摄像头用户配置信息ID
     */
    @TableField("camera_user_id")
    @Description("摄像头用户配置信息ID")
    private Long cameraUserId;
	
// ID赋值
public EstateEqCamera(){
        this.cameraId= returnIdLong();
        }
@Override
protected Serializable pkVal() {
        return this.cameraId;
}
	/**
	 * 设置：物业摄像头ID
	 */
	public EstateEqCamera setCameraId(Long cameraId) {
		this.cameraId = cameraId;
		return this;
	}
	/**
	 * 获取：物业摄像头ID
	 */
	public Long getCameraId() {
		return cameraId;
	}
	/**
	 * 设置：摄像头位置
	 */
	public EstateEqCamera setPlace(String place) {
		this.place = place;
		return this;
	}
	/**
	 * 获取：摄像头位置
	 */
	public String getPlace() {
		return place;
	}
	/**
	 * 设置：设备序列号
	 */
	public EstateEqCamera setKey(String key) {
		this.key = key;
		return this;
	}
	/**
	 * 获取：设备序列号
	 */
	public String getKey() {
		return key;
	}
	/**
	 * 设置：备注
	 */
	public EstateEqCamera setRemack(String remack) {
		this.remack = remack;
		return this;
	}
	/**
	 * 获取：备注
	 */
	public String getRemack() {
		return remack;
	}
	/**
	 * 设置：机构ID
	 */
	public EstateEqCamera setAgencyId(Long agencyId) {
		this.agencyId = agencyId;
		return this;
	}
	/**
	 * 获取：机构ID
	 */
	public Long getAgencyId() {
		return agencyId;
	}
	/**
	 * 设置：设备id
	 */
	public EstateEqCamera setEquipmentId(String equipmentId) {
		this.equipmentId = equipmentId;
		return this;
	}
	/**
	 * 获取：设备id
	 */
	public String getEquipmentId() {
		return equipmentId;
	}
	/**
	 * 设置：验证码
	 */
	public EstateEqCamera setVerificationCode(String verificationCode) {
		this.verificationCode = verificationCode;
		return this;
	}
	/**
	 * 获取：验证码
	 */
	public String getVerificationCode() {
		return verificationCode;
	}
	/**
	 * 设置：token码
	 */
	public EstateEqCamera setToken(String token) {
		this.token = token;
		return this;
	}
	/**
	 * 获取：token码
	 */
	public String getToken() {
		return token;
	}
	/**
	 * 设置：token结束时间
	 */
	public EstateEqCamera setExpireTime(Date expireTime) {
		this.expireTime = expireTime;
		return this;
	}
	/**
	 * 获取：token结束时间
	 */
	public Date getExpireTime() {
		return expireTime;
	}
	/**
	 * 设置：当前时间
	 */
	public EstateEqCamera setCurrentdateTime(Date currentdateTime) {
		this.currentdateTime = currentdateTime;
		return this;
	}
	/**
	 * 获取：当前时间
	 */
	public Date getCurrentdateTime() {
		return currentdateTime;
	}
	/**
	 * 设置：楼栋ID
	 */
	public EstateEqCamera setUnitId(Long unitId) {
		this.unitId = unitId;
		return this;
	}
	/**
	 * 获取：楼栋ID
	 */
	public Long getUnitId() {
		return unitId;
	}
	/**
	 * 设置：门ID
	 */
	public EstateEqCamera setDoorId(Long doorId) {
		this.doorId = doorId;
		return this;
	}
	/**
	 * 获取：门ID
	 */
	public Long getDoorId() {
		return doorId;
	}
	/**
	 * 设置：社区机构ID
	 */
	public EstateEqCamera setCommunityId(Long communityId) {
		this.communityId = communityId;
		return this;
	}
	/**
	 * 获取：社区机构ID
	 */
	public Long getCommunityId() {
		return communityId;
	}
	/**
	 * 设置：权限状态1公开、2小区所见、3私人所见
	 */
	public EstateEqCamera setPrivilegedStatus(Integer privilegedStatus) {
		this.privilegedStatus = privilegedStatus;
		return this;
	}
	/**
	 * 获取：权限状态1公开、2小区所见、3私人所见
	 */
	public Integer getPrivilegedStatus() {
		return privilegedStatus;
	}
	/**
	 * 设置：会员ID
	 */
	public EstateEqCamera setMemberId(Long memberId) {
		this.memberId = memberId;
		return this;
	}
	/**
	 * 获取：会员ID
	 */
	public Long getMemberId() {
		return memberId;
	}
	/**
	 * 设置：创建时间
	 */
	public EstateEqCamera setCreateTime(Date createTime) {
		this.createTime = createTime;
		return this;
	}
	/**
	 * 获取：创建时间
	 */
	public Date getCreateTime() {
		return createTime;
	}
	/**
	 * 设置：摄像头用户配置信息ID
	 */
	public EstateEqCamera setCameraUserId(Long cameraUserId) {
		this.cameraUserId = cameraUserId;
		return this;
	}
	/**
	 * 获取：摄像头用户配置信息ID
	 */
	public Long getCameraUserId() {
		return cameraUserId;
	}


}
