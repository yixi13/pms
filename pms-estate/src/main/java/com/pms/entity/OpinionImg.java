package com.pms.entity;
import com.baomidou.mybatisplus.activerecord.Model;
import java.io.Serializable;
import java.util.Date;
import javax.persistence.*;
import com.pms.util.Description;
import com.pms.entity.BaseModel;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;


/**
 * 
 * 
 * @author zyl
 * @email 517770986@qq.com
 * @date 2017-11-02 14:10:56
 */
@TableName( "opinion_img")
public class OpinionImg extends BaseModel<OpinionImg> {
private static final long serialVersionUID = 1L;


	    //物业投诉图片表
    @TableId("img_id")
    private Long imgId;
	
	    //投诉表ID
    @TableField("opinion_id")
    @Description("投诉表ID")
    private Long opinionId;
	
	    //图片
    @TableField("img")
    @Description("图片")
    private String img;
	
	    //创建人
    @TableField("create_by")
    @Description("创建人")
    private String createBy;
	
	    //创建时间
    @TableField("create_time")
    @Description("创建时间")
    private Date createTime;
	
	    //修改人
    @TableField("update_by")
    @Description("修改人")
    private String updateBy;
	
	    //修改时间
    @TableField("update_time")
    @Description("修改时间")
    private Date updateTime;
	
// ID赋值
public OpinionImg(){
        this.imgId= returnIdLong();
        }
@Override
protected Serializable pkVal() {
        return this.imgId;
}
	/**
	 * 设置：物业投诉图片表
	 */
	public void setImgId(Long imgId) {
		this.imgId = imgId;
	}
	/**
	 * 获取：物业投诉图片表
	 */
	public Long getImgId() {
		return imgId;
	}
	/**
	 * 设置：投诉表ID
	 */
	public void setOpinionId(Long opinionId) {
		this.opinionId = opinionId;
	}
	/**
	 * 获取：投诉表ID
	 */
	public Long getOpinionId() {
		return opinionId;
	}
	/**
	 * 设置：图片
	 */
	public void setImg(String img) {
		this.img = img;
	}
	/**
	 * 获取：图片
	 */
	public String getImg() {
		return img;
	}
	/**
	 * 设置：创建人
	 */
	public void setCreateBy(String createBy) {
		this.createBy = createBy;
	}
	/**
	 * 获取：创建人
	 */
	public String getCreateBy() {
		return createBy;
	}
	/**
	 * 设置：创建时间
	 */
	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}
	/**
	 * 获取：创建时间
	 */
	public Date getCreateTime() {
		return createTime;
	}
	/**
	 * 设置：修改人
	 */
	public void setUpdateBy(String updateBy) {
		this.updateBy = updateBy;
	}
	/**
	 * 获取：修改人
	 */
	public String getUpdateBy() {
		return updateBy;
	}
	/**
	 * 设置：修改时间
	 */
	public void setUpdateTime(Date updateTime) {
		this.updateTime = updateTime;
	}
	/**
	 * 获取：修改时间
	 */
	public Date getUpdateTime() {
		return updateTime;
	}


}
