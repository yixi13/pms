package com.pms.entity;

import com.baomidou.mybatisplus.activerecord.Model;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.*;

import com.pms.util.Description;
import com.pms.entity.BaseModel;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;


/**
 * @author zyl
 * @email 517770986@qq.com
 * @date 2017-11-09 09:44:04
 */
@TableName("owner_member")
public class OwnerMember extends BaseModel<OwnerMember> {
    private static final long serialVersionUID = 1L;


    /**
     * 业主认证id
     */
    @TableId("owner_id")
    private Long ownerId;

    /**
     * 会员id
     */
    @TableField("member_id")
    @Description("会员id")
    private Long memberId;

    /**
     * 户室id
     */
    @TableField("house_id")
    @Description("户室id")
    private Long houseId;

    /**
     * 单元id
     */
    @TableField("unit_id")
    @Description("单元id")
    private Long unitId;

    /**
     * 户室名称
     */
    @TableField("house_name")
    @Description("户室名称")
    private String houseName;

    /**
     * 社区Id
     */
    @TableField("community_id")
    @Description("社区Id")
    private Long communityId;

    /**
     * 验证类型  1-业主  2-管理员（待审核） 3-管理员（审核通过） 4-家庭成员  5-物业人员
     */
    @TableField("type_")
    @Description("验证类型  1-业主  2-管理员（待审核） 3-管理员（审核通过） 4-家庭成员  5-物业人员")
    private Integer type;

    /**
     * 管理员/会员的手机号
     */
    @TableField("owner_phone")
    @Description("管理员/会员手机号")
    private String ownerPhone;

    /**
     * 管理员/会员的呢称
     */
    @TableField("owner_name")
    @Description("管理员/会员的呢称")
    private String ownerName;

    /**
     * 邀请人(会员）id
     */
    @TableField("referrer_id")
    @Description("邀请人(会员）id")
    private Long referrerId;
    /**
     * 邀请人(会员）昵称
     */
    @TableField("referrer_nike_name")
    @Description("邀请人(会员）昵称")
    private String referrerNikeName;
    /**
     * 认证时间
     */
    @TableField("create_time")
    @Description("认证时间")
    private Date createTime;

    /**
     * 2-当前房屋为默认房屋
     */
    @TableField("is_default")
    @Description("2-当前房屋为默认房屋")
    private Integer isDefault;

    /**
     * 楼层id
     */
    @TableField("floor_id")
    @Description("楼层id")
    private Long floorId;

    /**
     * 单元名称
     */
    @TableField("unit_name")
    @Description("单元名称")
    private String unitName;

    /**
     * 小区名称
     */
    @TableField("community_name")
    @Description("小区名称")
    private String communityName;

    @Description("物业机构id")
    @TableField("agency_id")
    private Long agencyId;

    public Long getAgencyId() {
        return agencyId;
    }

    public void setAgencyId(Long agencyId) {
        this.agencyId = agencyId;
    }
    // ID赋值
    public OwnerMember() {
        this.ownerId = returnIdLong();
    }

    @Override
    protected Serializable pkVal() {
        return this.ownerId;
    }

    /**
     * 设置：业主认证id
     */
    public OwnerMember setOwnerId(Long ownerId) {
        this.ownerId = ownerId;
        return this;
    }

    /**
     * 获取：业主认证id
     */
    public Long getOwnerId() {
        return ownerId;
    }

    /**
     * 设置：会员id
     */
    public OwnerMember setMemberId(Long memberId) {
        this.memberId = memberId;
        return this;
    }

    /**
     * 获取：会员id
     */
    public Long getMemberId() {
        return memberId;
    }

    /**
     * 设置：户室id
     */
    public OwnerMember setHouseId(Long houseId) {
        this.houseId = houseId;
        return this;
    }

    /**
     * 获取：户室id
     */
    public Long getHouseId() {
        return houseId;
    }

    /**
     * 设置：单元id
     */
    public OwnerMember setUnitId(Long unitId) {
        this.unitId = unitId;
        return this;
    }

    /**
     * 获取：单元id
     */
    public Long getUnitId() {
        return unitId;
    }

    /**
     * 设置：户室名称
     */
    public OwnerMember setHouseName(String houseName) {
        this.houseName = houseName;
        return this;
    }

    /**
     * 获取：户室名称
     */
    public String getHouseName() {
        return houseName;
    }

    /**
     * 设置：社区Id
     */
    public OwnerMember setCommunityId(Long communityId) {
        this.communityId = communityId;
        return this;
    }

    /**
     * 获取：社区Id
     */
    public Long getCommunityId() {
        return communityId;
    }

    /**
     * 设置：验证类型  1-业主  2-管理员（待审核） 3-管理员（审核通过） 4-家庭成员  5-物业人员
     */
    public OwnerMember setType(Integer type) {
        this.type = type;
        return this;
    }

    /**
     * 获取：验证类型  1-业主  2-管理员（待审核） 3-管理员（审核通过） 4-家庭成员  5-物业人员
     */
    public Integer getType() {
        return type;
    }

    /**
     * 设置：验证管理员的手机号
     */
    public OwnerMember setOwnerPhone(String ownerPhone) {
        this.ownerPhone = ownerPhone;
        return this;
    }

    /**
     * 获取：验证管理员的手机号
     */
    public String getOwnerPhone() {
        return ownerPhone;
    }

    /**
     * 设置：验证管理员的名称
     */
    public OwnerMember setOwnerName(String ownerName) {
        this.ownerName = ownerName;
        return this;
    }

    /**
     * 获取：验证管理员的名称
     */
    public String getOwnerName() {
        return ownerName;
    }

    /**
     * 设置：邀请人(会员）id
     */
    public OwnerMember setReferrerId(Long referrerId) {
        this.referrerId = referrerId;
        return this;
    }

    /**
     * 获取：邀请人(会员）id
     */
    public Long getReferrerId() {
        return referrerId;
    }

    /**
     * 设置：认证时间
     */
    public OwnerMember setCreateTime(Date createTime) {
        this.createTime = createTime;
        return this;
    }

    /**
     * 获取：认证时间
     */
    public Date getCreateTime() {
        return createTime;
    }

    /**
     * 设置：2-默认为当前房屋
     */
    public OwnerMember setIsDefault(Integer isDefault) {
        this.isDefault = isDefault;
        return this;
    }

    /**
     * 获取：2-默认为当前房屋
     */
    public Integer getIsDefault() {
        return isDefault;
    }

    /**
     * 设置：楼层id
     */
    public OwnerMember setFloorId(Long floorId) {
        this.floorId = floorId;
        return this;
    }

    /**
     * 获取：楼层id
     */
    public Long getFloorId() {
        return floorId;
    }

    /**
     * 设置：单元名称
     */
    public OwnerMember setUnitName(String unitName) {
        this.unitName = unitName;
        return this;
    }

    /**
     * 获取：单元名称
     */
    public String getUnitName() {
        return unitName;
    }

    /**
     * 设置：小区名称
     */
    public OwnerMember setCommunityName(String communityName) {
        this.communityName = communityName;
        return this;
    }

    /**
     * 获取：小区名称
     */
    public String getCommunityName() {
        return communityName;
    }

    public String getReferrerNikeName() {
        return referrerNikeName;
    }

    public void setReferrerNikeName(String referrerNikeName) {
        this.referrerNikeName = referrerNikeName;
    }
}
