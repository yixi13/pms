package com.pms.entity;
import com.baomidou.mybatisplus.activerecord.Model;
import java.io.Serializable;
import java.util.Date;
import javax.persistence.*;
import com.pms.util.Description;
import com.pms.entity.BaseModel;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;


/**
 * 
 * 
 * @author zyl
 * @email 517770986@qq.com
 * @date 2017-11-03 09:48:34
 */
@TableName( "estate_notice_activity")
public class EstateNoticeActivity extends BaseModel<EstateNoticeActivity> {
private static final long serialVersionUID = 1L;


	    /**
	 *物业公告活动表
	 */
    @TableId("no_ac_id")
    private Long noAcId;
	
	    /**
     *公告ID
     */
    @TableField("notice_id")
    @Description("公告ID")
    private Long noticeId;
	
	    /**
     *报名电话
     */
    @TableField("phone_")
    @Description("报名电话")
    private String phone;
	
	    /**
     *报名姓名
     */
    @TableField("name_")
    @Description("报名姓名")
    private String name;
	
	    /**
     *人数
     */
    @TableField("number_")
    @Description("人数")
		private Integer number;
	
	    /**
     *机构ID
     */
    @TableField("agency_id")
    @Description("机构ID")
    private Long agencyId;
	
// ID赋值
public EstateNoticeActivity(){
        this.noAcId= returnIdLong();
        }
@Override
protected Serializable pkVal() {
        return this.noAcId;
}
	/**
	 * 设置：物业公告活动表
	 */
	public void setNoAcId(Long noAcId) {
		this.noAcId = noAcId;
	}
	/**
	 * 获取：物业公告活动表
	 */
	public Long getNoAcId() {
		return noAcId;
	}
	/**
	 * 设置：公告ID
	 */
	public void setNoticeId(Long noticeId) {
		this.noticeId = noticeId;
	}
	/**
	 * 获取：公告ID
	 */
	public Long getNoticeId() {
		return noticeId;
	}
	/**
	 * 设置：报名电话
	 */
	public void setPhone(String phone) {
		this.phone = phone;
	}
	/**
	 * 获取：报名电话
	 */
	public String getPhone() {
		return phone;
	}
	/**
	 * 设置：报名姓名
	 */
	public void setName(String name) {
		this.name = name;
	}
	/**
	 * 获取：报名姓名
	 */
	public String getName() {
		return name;
	}

	public Integer getNumber() {
		return number;
	}

	public void setNumber(Integer number) {
		this.number = number;
	}

	/**
	 * 设置：机构ID
	 */
	public void setAgencyId(Long agencyId) {
		this.agencyId = agencyId;
	}
	/**
	 * 获取：机构ID
	 */
	public Long getAgencyId() {
		return agencyId;
	}


}
