package com.pms.entity;

import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;
import com.pms.util.Description;

import java.io.Serializable;
import java.util.Date;
import java.util.List;


/**
 * 
 * 
 * @author zyl
 * @email 517770986@qq.com
 * @date 2017-11-01 11:52:51
 */
@TableName( "bill_table")
public class BillTable extends BaseModel<BillTable> {
private static final long serialVersionUID = 1L;


	//账单主键
    @TableId("bill_id")
    private Long billId;
	
	//账单名称
    @TableField("bill_name")
    @Description("账单名称")
    private String billName;
	
	//账单编号
    @TableField("bill_nu")
    @Description("账单编号")
    private String billNu;
	
	//总金额
    @TableField("total_Price")
    @Description("总金额")
    private Double totalPrice;
	
	//是否缴纳 1 未缴清2 已缴清
    @TableField("is_payment")
    @Description("是否缴纳 1 未缴清2 已缴清")
    private Integer isPayment;
	
	//1 银联 2 支付宝 3 微信
    @TableField("pay_type")
    @Description("1 银联 2 支付宝 3 微信")
    private Integer payType;
	

    @TableField("bill_year")
    @Description("年份")
    private Integer billYear;
	
	//月份
    @TableField("bill_month")
    @Description("月份")
    private Integer billMonth;
	
	//所在年份上下半年:1.上半年 2.下半年
    @TableField("bill_half_year")
    @Description("所在年份上下半年:1.上半年 2.下半年")
    private Integer billHalfYear;
	
	//所在年份季节:1.等一季度 2.第二季度 3.第三季度 4.第四季度
    @TableField("bill_season")
    @Description("所在年份季节:1.等一季度 2.第二季度 3.第三季度 4.第四季度")
    private Integer billSeason;
	
	//户室ID
    @TableField("house_id")
    @Description("户室ID")
    private Long houseId;
	
	//机构ID
    @TableField("sp_id")
    @Description("机构ID")
    private Long spId;
	
	//创建时间
    @TableField("create_time")
    @Description("创建时间")
    private Date createTime;

	@TableField(exist = false)
    private List<BillTableDetail> billTableDetails;

	public List<BillTableDetail> getBillTableDetails() {
		return billTableDetails;
	}

	public void setBillTableDetails(List<BillTableDetail> billTableDetails) {
		this.billTableDetails = billTableDetails;
	}

	// ID赋值
public BillTable(){
        this.billId= returnIdLong();
        }
@Override
protected Serializable pkVal() {
        return this.billId;
}
	/**
	 * 设置：
	 */
	public void setBillId(Long billId) {
		this.billId = billId;
	}
	/**
	 * 获取：
	 */
	public Long getBillId() {
		return billId;
	}
	/**
	 * 设置：账单名称
	 */
	public void setBillName(String billName) {
		this.billName = billName;
	}
	/**
	 * 获取：账单名称
	 */
	public String getBillName() {
		return billName;
	}
	/**
	 * 设置：账单编号
	 */
	public void setBillNu(String billNu) {
		this.billNu = billNu;
	}
	/**
	 * 获取：账单编号
	 */
	public String getBillNu() {
		return billNu;
	}
	/**
	 * 设置：总金额
	 */
	public void setTotalPrice(Double totalPrice) {
		this.totalPrice = totalPrice;
	}
	/**
	 * 获取：总金额
	 */
	public Double getTotalPrice() {
		return totalPrice;
	}
	/**
	 * 设置：是否缴纳 1 未缴清2 已缴清
	 */
	public void setIsPayment(Integer isPayment) {
		this.isPayment = isPayment;
	}
	/**
	 * 获取：是否缴纳 1 未缴清2 已缴清
	 */
	public Integer getIsPayment() {
		return isPayment;
	}
	/**
	 * 设置：1 银联 2 支付宝 3 微信
	 */
	public void setPayType(Integer payType) {
		this.payType = payType;
	}
	/**
	 * 获取：1 银联 2 支付宝 3 微信
	 */
	public Integer getPayType() {
		return payType;
	}
	/**
	 * 设置：
	 */
	public void setBillYear(Integer billYear) {
		this.billYear = billYear;
	}
	/**
	 * 获取：
	 */
	public Integer getBillYear() {
		return billYear;
	}
	/**
	 * 设置：月份
	 */
	public void setBillMonth(Integer billMonth) {
		this.billMonth = billMonth;
	}
	/**
	 * 获取：月份
	 */
	public Integer getBillMonth() {
		return billMonth;
	}
	/**
	 * 设置：所在年份上下半年:1.上半年 2.下半年
	 */
	public void setBillHalfYear(Integer billHalfYear) {
		this.billHalfYear = billHalfYear;
	}
	/**
	 * 获取：所在年份上下半年:1.上半年 2.下半年
	 */
	public Integer getBillHalfYear() {
		return billHalfYear;
	}
	/**
	 * 设置：所在年份季节:1.等一季度 2.第二季度 3.第三季度 4.第四季度
	 */
	public void setBillSeason(Integer billSeason) {
		this.billSeason = billSeason;
	}
	/**
	 * 获取：所在年份季节:1.等一季度 2.第二季度 3.第三季度 4.第四季度
	 */
	public Integer getBillSeason() {
		return billSeason;
	}
	/**
	 * 设置：户室ID
	 */
	public void setHouseId(Long houseId) {
		this.houseId = houseId;
	}
	/**
	 * 获取：户室ID
	 */
	public Long getHouseId() {
		return houseId;
	}
	/**
	 * 设置：机构ID
	 */
	public void setSpId(Long spId) {
		this.spId = spId;
	}
	/**
	 * 获取：机构ID
	 */
	public Long getSpId() {
		return spId;
	}
	/**
	 * 设置：创建时间
	 */
	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}
	/**
	 * 获取：创建时间
	 */
	public Date getCreateTime() {
		return createTime;
	}


}
