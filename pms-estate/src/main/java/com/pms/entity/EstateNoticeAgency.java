package com.pms.entity;
import com.baomidou.mybatisplus.activerecord.Model;
import java.io.Serializable;
import java.util.Date;
import javax.persistence.*;
import com.pms.util.Description;
import com.pms.entity.BaseModel;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;


/**
 * 
 * 
 * @author zyl
 * @email 517770986@qq.com
 * @date 2017-11-03 09:48:34
 */
@TableName( "estate_notice_agency")
public class EstateNoticeAgency extends BaseModel<EstateNoticeAgency> {
private static final long serialVersionUID = 1L;


	    /**
	 *公告ID
	 */
    @TableId("notice_id")
    private Long noticeId;
	
	    /**
     *机构ID
     */
    @TableField("agency_id")
    @Description("机构ID")
    private Long agencyId;
	
	    /**
     *创建时间
     */
    @TableField("create_time")
    @Description("创建时间")
    private Date createTime;


	
// ID赋值
public EstateNoticeAgency(){
        this.noticeId= returnIdLong();
        }
@Override
protected Serializable pkVal() {
        return this.noticeId;
}
	/**
	 * 设置：公告ID
	 */
	public void setNoticeId(Long noticeId) {
		this.noticeId = noticeId;
	}
	/**
	 * 获取：公告ID
	 */
	public Long getNoticeId() {
		return noticeId;
	}
	/**
	 * 设置：机构ID
	 */
	public void setAgencyId(Long agencyId) {
		this.agencyId = agencyId;
	}
	/**
	 * 获取：机构ID
	 */
	public Long getAgencyId() {
		return agencyId;
	}
	/**
	 * 设置：创建时间
	 */
	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}
	/**
	 * 获取：创建时间
	 */
	public Date getCreateTime() {
		return createTime;
	}

}
