package com.pms.entity;

import com.baomidou.mybatisplus.activerecord.Model;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.*;

import com.pms.util.Description;
import com.pms.entity.BaseModel;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;


/**
 * 在排队进行远程发卡的卡片
 * @author zyl
 * @email 517770986@qq.com
 * @date 2017-11-28 16:15:55
 */
@TableName("estate_lock_card")
public class EstateLockCard extends BaseModel<EstateLockCard> {
    private static final long serialVersionUID = 1L;


    /**
     * id
     */
    @TableId("lock_card_id")
    private Long lockCardId;

    /**
     * 锁设备id
     */
    @TableField("eq_sn")
    @Description("锁设备id")
    private String eqSn;

    /**
     * 锁设备密码
     */
    @TableField("eq_pswd")
    @Description("锁设备密码")
    private String eqPswd;

    /**
     * 门id
     */
    @TableField("door_id")
    @Description("门id")
    private Long doorId;

    /**
     * 门名称
     */
    @TableField("door_name")
    @Description("门名称")
    private String doorName;

    /**
     * 社区id
     */
    @TableField("community_id")
    @Description("社区id")
    private Long communityId;

    /**
     * 社区名称
     */
    @TableField("community_name")
    @Description("社区名称")
    private String communityName;

    /**
     * 10进制卡号
     */
    @TableField("card_num_ten")
    @Description("10进制卡号")
    private String cardNumTen;

    /**
     * 16进制卡号
     */
    @TableField("card_num_sixteen")
    @Description("16进制卡号")
    private String cardNumSixteen;

    /**
     * 有效期时间
     */
    @TableField("valid_time")
    @Description("有效期时间")
    private Date validTime;

    /**
     * 单元id
     */
    @TableField("unit_id")
    @Description("单元id")
    private Long unitId;

    /**
     * 单元名称
     */
    @TableField("unit_name")
    @Description("单元名称")
    private String unitName;

    // ID赋值
    public EstateLockCard() {
        this.lockCardId = returnIdLong();
    }

    @Override
    protected Serializable pkVal() {
        return this.lockCardId;
    }

    public String getUnitName() {
        return unitName;
    }

    public void setUnitName(String unitName) {
        this.unitName = unitName;
    }

    /**
     * 设置：id
     */
    public EstateLockCard setLockCardId(Long lockCardId) {
        this.lockCardId = lockCardId;
        return this;
    }

    /**
     * 获取：id
     */
    public Long getLockCardId() {
        return lockCardId;
    }

    /**
     * 设置：锁设备id
     */
    public EstateLockCard setEqSn(String eqSn) {
        this.eqSn = eqSn;
        return this;
    }

    /**
     * 获取：锁设备id
     */
    public String getEqSn() {
        return eqSn;
    }

    /**
     * 设置：锁设备密码
     */
    public EstateLockCard setEqPswd(String eqPswd) {
        this.eqPswd = eqPswd;
        return this;
    }

    /**
     * 获取：锁设备密码
     */
    public String getEqPswd() {
        return eqPswd;
    }

    /**
     * 设置：
     */
    public EstateLockCard setDoorId(Long doorId) {
        this.doorId = doorId;
        return this;
    }

    /**
     * 获取：
     */
    public Long getDoorId() {
        return doorId;
    }

    /**
     * 设置：门名称
     */
    public EstateLockCard setDoorName(String doorName) {
        this.doorName = doorName;
        return this;
    }

    /**
     * 获取：门名称
     */
    public String getDoorName() {
        return doorName;
    }

    /**
     * 设置：社区id
     */
    public EstateLockCard setCommunityId(Long communityId) {
        this.communityId = communityId;
        return this;
    }

    /**
     * 获取：社区id
     */
    public Long getCommunityId() {
        return communityId;
    }

    /**
     * 设置：社区名称
     */
    public EstateLockCard setCommunityName(String communityName) {
        this.communityName = communityName;
        return this;
    }

    /**
     * 获取：社区名称
     */
    public String getCommunityName() {
        return communityName;
    }

    /**
     * 设置：10进制卡号
     */
    public EstateLockCard setCardNumTen(String cardNumTen) {
        this.cardNumTen = cardNumTen;
        return this;
    }

    /**
     * 获取：10进制卡号
     */
    public String getCardNumTen() {
        return cardNumTen;
    }

    /**
     * 设置：16进制卡号
     */
    public EstateLockCard setCardNumSixteen(String cardNumSixteen) {
        this.cardNumSixteen = cardNumSixteen;
        return this;
    }

    /**
     * 获取：16进制卡号
     */
    public String getCardNumSixteen() {
        return cardNumSixteen;
    }

    /**
     * 设置：有效期时间
     */
    public EstateLockCard setValidTime(Date validTime) {
        this.validTime = validTime;
        return this;
    }

    /**
     * 获取：有效期时间
     */
    public Date getValidTime() {
        return validTime;
    }

    /**
     * 设置：单元id
     */
    public EstateLockCard setUnitId(Long unitId) {
        this.unitId = unitId;
        return this;
    }

    /**
     * 获取：单元id
     */
    public Long getUnitId() {
        return unitId;
    }


}
