package com.pms.entity;

import com.baomidou.mybatisplus.activerecord.Model;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.*;

import com.pms.util.Description;
import com.pms.entity.BaseModel;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;


/**
 * @author zyl
 * @email 517770986@qq.com
 * @date 2017-11-07 10:15:34
 */
@TableName("charge_name")
public class ChargeName extends BaseModel<ChargeName> {
    private static final long serialVersionUID = 1L;


    /**
     * 费用名称id
     */
    @TableId("charge_name_id")
    private Long chargeNameId;

    /**
     * 费用名称
     */
    @TableField("charge_name")
    @Description("费用名称")
    private String chargeName;
    /**
     * 社区id(公共收费可为空)
     */
    @TableField("community_id")
    @Description("社区id(公共收费可为空)")
    private Long communityId;
    /**
     * 是否是 公共收费(1-不是，2-是)
     */
    @TableField("charge_is_common")
    @Description("是否是 公共收费(1-不是，2-是)")
    private Integer chargeIsCommon;
    /**
     * 费用计算方式(1-建筑面积计算,2-固定费用,3/4/5-水/电/气使用量计算
     */
    @TableField("charge_way")
    @Description("费用计算方式(1-建筑面积计算,2-固定费用,3/4/5-水/电/气使用量计算")
    private Integer chargeWay;
    /**
     * 费用类型(1-周期性收费-每月生成,2-临时性收费-某几个月)
     */
    @TableField("charge_type")
    @Description("费用类型(1-周期性收费-每月生成,2-临时性收费-某几个月)")
    private Integer chargeType;
    /**
     * 收费描述
     */
    @TableField("charge_remarks")
    @Description("收费描述")
    private String chargeRemarks;
    /**
     * 临时性收费起始时间
     */
    @TableField("charge_start_time")
    @Description("临时性收费起始时间")
    private Date chargeStartTime;
    /**
     * 临时性收费截止时间
     */
    @TableField("charge_end_time")
    @Description("临时性收费截止时间")
    private Date chargeEndTime;

    /**
     * 创建时间
     */
    @TableField("create_time")
    @Description("创建时间")
    private Date createTime;
    @Description("物业机构id")
    @TableField("agency_id")
    private Long agencyId;

    public Long getAgencyId() {
        return agencyId;
    }

    public void setAgencyId(Long agencyId) {
        this.agencyId = agencyId;
    }
    // ID赋值
    public ChargeName() {
        this.chargeNameId = returnIdLong();
    }

    @Override
    protected Serializable pkVal() {
        return this.chargeNameId;
    }

    /**
     * 设置：费用名称id
     */
    public ChargeName setChargeNameId(Long chargeNameId) {
        this.chargeNameId = chargeNameId;
        return this;
    }

    /**
     * 获取：费用名称id
     */
    public Long getChargeNameId() {
        return chargeNameId;
    }

    /**
     * 设置：费用名称
     */
    public ChargeName setChargeName(String chargeName) {
        this.chargeName = chargeName;
        return this;
    }

    /**
     * 获取：费用名称
     */
    public String getChargeName() {
        return chargeName;
    }

    /**
     * 设置：社区id(公共收费可为空)
     */
    public ChargeName setCommunityId(Long communityId) {
        this.communityId = communityId;
        return this;
    }

    /**
     * 获取：社区id(公共收费可为空)
     */
    public Long getCommunityId() {
        return communityId;
    }

    /**
     * 设置：是否是 公共收费(1-不是，2-是)
     */
    public ChargeName setChargeIsCommon(Integer chargeIsCommon) {
        this.chargeIsCommon = chargeIsCommon;
        return this;
    }

    /**
     * 获取：是否是 公共收费(1-不是，2-是)
     */
    public Integer getChargeIsCommon() {
        return chargeIsCommon;
    }

    /**
     * 设置：费用计算方式(1-建筑面积计算,2-固定费用,3/4/5-水/电/气使用量计算
     */
    public ChargeName setChargeWay(Integer chargeWay) {
        this.chargeWay = chargeWay;
        return this;
    }

    /**
     * 获取：费用计算方式(1-建筑面积计算,2-固定费用,3/4/5-水/电/气使用量计算
     */
    public Integer getChargeWay() {
        return chargeWay;
    }

    /**
     * 设置：费用类型(1-周期性收费-每月生成,2-临时性收费-某几个月)
     */
    public ChargeName setChargeType(Integer chargeType) {
        this.chargeType = chargeType;
        return this;
    }

    /**
     * 获取：费用类型(1-周期性收费-每月生成,2-临时性收费-某几个月)
     */
    public Integer getChargeType() {
        return chargeType;
    }

    /**
     * 设置：收费描述
     */
    public ChargeName setChargeRemarks(String chargeRemarks) {
        this.chargeRemarks = chargeRemarks;
        return this;
    }

    /**
     * 获取：收费描述
     */
    public String getChargeRemarks() {
        return chargeRemarks;
    }

    /**
     * 设置：临时性收费起始时间
     */
    public ChargeName setChargeStartTime(Date chargeStartTime) {
        this.chargeStartTime = chargeStartTime;
        return this;
    }

    /**
     * 获取：临时性收费起始时间
     */
    public Date getChargeStartTime() {
        return chargeStartTime;
    }

    /**
     * 设置：临时性收费截止时间
     */
    public ChargeName setChargeEndTime(Date chargeEndTime) {
        this.chargeEndTime = chargeEndTime;
        return this;
    }

    /**
     * 获取：临时性收费截止时间
     */
    public Date getChargeEndTime() {
        return chargeEndTime;
    }

    /**
     * 设置：创建时间
     */
    public ChargeName setCreateTime(Date createTime) {
        this.createTime = createTime;
        return this;
    }

    /**
     * 获取：创建时间
     */
    public Date getCreateTime() {
        return createTime;
    }


}
