package com.pms.entity;

import com.baomidou.mybatisplus.activerecord.Model;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.*;

import com.pms.util.Description;
import com.pms.entity.BaseModel;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;


/**
 * 下发的卡片
 * @author zyl
 * @email 517770986@qq.com
 * @date 2017-11-28 16:15:55
 */
@TableName("estate_door_card")
public class EstateDoorCard extends BaseModel<EstateDoorCard> {
    private static final long serialVersionUID = 1L;


    /**
     * 卡片及门关联id
     */
    @TableId("door_card_id")
    private Long doorCardId;

    /**
     * 门id
     */
    @TableField("door_id")
    @Description("门id")
    private Long doorId;

    /**
     * 门名称
     */
    @TableField("door_name")
    @Description("门名称")
    private String doorName;

    /**
     * 16进制卡号
     */
    @TableField("card_num_sixteen")
    @Description("16进制卡号")
    private String cardNumSixteen;

    /**
     * 10进制卡号
     */
    @TableField("card_num_ten")
    @Description("10进制卡号")
    private String cardNumTen;

    /**
     * 社区id
     */
    @TableField("community_id")
    @Description("社区id")
    private Long communityId;

    /**
     * 社区名称
     */
    @TableField("community_name")
    @Description("社区名称")
    private String communityName;

    /**
     * 创建时间
     */
    @TableField("create_time")
    @Description("创建时间")
    private Date createTime;

    /**
     * 有效期时间
     */
    @TableField("valid_time")
    @Description("有效期时间")
    private Date validTime;

    /**
     * 单元id
     */
    @TableField("unit_id")
    @Description("单元id")
    private Long unitId;

    /**
     * 单元名称
     */
    @TableField("unit_name")
    @Description("单元名称")
    private String unitName;

    // ID赋值
    public EstateDoorCard() {
        this.doorCardId = returnIdLong();
    }

    @Override
    protected Serializable pkVal() {
        return this.doorCardId;
    }

    /**
     * 设置：卡片及门关联id
     */
    public EstateDoorCard setDoorCardId(Long doorCardId) {
        this.doorCardId = doorCardId;
        return this;
    }

    /**
     * 获取：卡片及门关联id
     */
    public Long getDoorCardId() {
        return doorCardId;
    }

    /**
     * 设置：门id
     */
    public EstateDoorCard setDoorId(Long doorId) {
        this.doorId = doorId;
        return this;
    }

    /**
     * 获取：门id
     */
    public Long getDoorId() {
        return doorId;
    }

    /**
     * 设置：门名称
     */
    public EstateDoorCard setDoorName(String doorName) {
        this.doorName = doorName;
        return this;
    }

    /**
     * 获取：门名称
     */
    public String getDoorName() {
        return doorName;
    }

    /**
     * 设置：16进制卡号
     */
    public EstateDoorCard setCardNumSixteen(String cardNumSixteen) {
        this.cardNumSixteen = cardNumSixteen;
        return this;
    }

    /**
     * 获取：16进制卡号
     */
    public String getCardNumSixteen() {
        return cardNumSixteen;
    }

    /**
     * 设置：10进制卡号
     */
    public EstateDoorCard setCardNumTen(String cardNumTen) {
        this.cardNumTen = cardNumTen;
        return this;
    }

    /**
     * 获取：10进制卡号
     */
    public String getCardNumTen() {
        return cardNumTen;
    }

    /**
     * 设置：社区id
     */
    public EstateDoorCard setCommunityId(Long communityId) {
        this.communityId = communityId;
        return this;
    }

    /**
     * 获取：社区id
     */
    public Long getCommunityId() {
        return communityId;
    }

    /**
     * 设置：社区名称
     */
    public EstateDoorCard setCommunityName(String communityName) {
        this.communityName = communityName;
        return this;
    }

    /**
     * 获取：社区名称
     */
    public String getCommunityName() {
        return communityName;
    }

    /**
     * 设置：创建时间
     */
    public EstateDoorCard setCreateTime(Date createTime) {
        this.createTime = createTime;
        return this;
    }

    /**
     * 获取：创建时间
     */
    public Date getCreateTime() {
        return createTime;
    }

    /**
     * 设置：有效期时间
     */
    public EstateDoorCard setValidTime(Date validTime) {
        this.validTime = validTime;
        return this;
    }

    /**
     * 获取：有效期时间
     */
    public Date getValidTime() {
        return validTime;
    }

    /**
     * 设置：单元id
     */
    public EstateDoorCard setUnitId(Long unitId) {
        this.unitId = unitId;
        return this;
    }

    /**
     * 获取：单元id
     */
    public Long getUnitId() {
        return unitId;
    }

    /**
     * 设置：单元名称
     */
    public EstateDoorCard setUnitName(String unitName) {
        this.unitName = unitName;
        return this;
    }

    /**
     * 获取：单元名称
     */
    public String getUnitName() {
        return unitName;
    }


}
