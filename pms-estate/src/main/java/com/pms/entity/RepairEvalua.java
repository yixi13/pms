package com.pms.entity;
import java.io.Serializable;
import java.util.Date;
import com.pms.util.Description;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;


/**
 * 
 * 
 * @author zyl
 * @email 517770986@qq.com
 * @date 2017-11-01 14:34:16
 */
@TableName( "repair_evalua")
public class RepairEvalua extends BaseModel<RepairEvalua> {
private static final long serialVersionUID = 1L;


	    //物业表修评论表
    @TableId("evalua_id")
    private Long evaluaId;
	
	    //保修ID
    @TableField("repair_id")
    @Description("保修ID")
    private Long repairId;
	
	    //评论详情
    @TableField("evalua_content")
    @Description("评论详情")
    private String evaluaContent;
	
	    //
    @TableField("create_by")
    @Description("")
    private String createBy;
	
	    //
    @TableField("create_time")
    @Description("")
    private Date createTime;
	
	    //
    @TableField("update_by")
    @Description("")
    private String updateBy;
	
	    //
    @TableField("update_time")
    @Description("")
    private Date updateTime;
	
// ID赋值
public RepairEvalua(){
        this.evaluaId= returnIdLong();
        }
@Override
protected Serializable pkVal() {
        return this.evaluaId;
}
	/**
	 * 设置：物业表修评论表
	 */
	public void setEvaluaId(Long evaluaId) {
		this.evaluaId = evaluaId;
	}
	/**
	 * 获取：物业表修评论表
	 */
	public Long getEvaluaId() {
		return evaluaId;
	}
	/**
	 * 设置：保修ID
	 */
	public void setRepairId(Long repairId) {
		this.repairId = repairId;
	}
	/**
	 * 获取：保修ID
	 */
	public Long getRepairId() {
		return repairId;
	}
	/**
	 * 设置：评论详情
	 */
	public void setEvaluaContent(String evaluaContent) {
		this.evaluaContent = evaluaContent;
	}
	/**
	 * 获取：评论详情
	 */
	public String getEvaluaContent() {
		return evaluaContent;
	}
	/**
	 * 设置：
	 */
	public void setCreateBy(String createBy) {
		this.createBy = createBy;
	}
	/**
	 * 获取：
	 */
	public String getCreateBy() {
		return createBy;
	}
	/**
	 * 设置：
	 */
	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}
	/**
	 * 获取：
	 */
	public Date getCreateTime() {
		return createTime;
	}
	/**
	 * 设置：
	 */
	public void setUpdateBy(String updateBy) {
		this.updateBy = updateBy;
	}
	/**
	 * 获取：
	 */
	public String getUpdateBy() {
		return updateBy;
	}
	/**
	 * 设置：
	 */
	public void setUpdateTime(Date updateTime) {
		this.updateTime = updateTime;
	}
	/**
	 * 获取：
	 */
	public Date getUpdateTime() {
		return updateTime;
	}


}
