package com.pms.entity;
import com.baomidou.mybatisplus.activerecord.Model;
import java.io.Serializable;
import java.util.Date;
import javax.persistence.*;
import com.pms.util.Description;
import com.pms.entity.BaseModel;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;


/**
 * <p>
 *
 * </p>
 *
 * @author ASUS_B
 * @since 2017-11-02
 */
@TableName( "house_master_info")
public class HouseMasterInfo extends BaseModel<HouseMasterInfo> {
	private static final long serialVersionUID = 1L;

 	/**
	 *户主信息id
	 */
    @TableId("house_master_id")
	@Description("户主信息id")
    private Long houseMasterId;
	 /**
     *户主姓名
     */
    @TableField("house_master_name")
    @Description("户主姓名")
    private String houseMasterName;
	/**
     *户主性别(1-男，2-女，3-未知)
     */
    @TableField("house_master_sex")
    @Description("户主性别(1-男，2-女，3-保密(默认))")
    private Integer houseMasterSex;
	 /**
     *户主联系电话
     */
    @TableField("house_master_phone")
    @Description("户主联系电话")
    private String houseMasterPhone;
	/**
     *户主座机号码
     */
    @TableField("house_master_telphone")
    @Description("户主座机号码")
    private String houseMasterTelphone;
	/**
     *社区id
     */
    @TableField("community_id")
    @Description("社区id")
    private Long communityId;
	/**
     *单元名称
     */
    @TableField("unit_name")
    @Description("单元名称")
    private String unitName;
	/**
     *房号
     */
    @TableField("house_number")
    @Description("房号")
    private String houseNumber;
	 /**
     *户室面积
     */
    @TableField("house_proportion")
    @Description("户室面积")
    private Double houseProportion;
	/**
     *楼层序号(从房号截取)
     */
    @TableField("floor_sort")
    @Description("楼层序号(从房号截取)")
    private Integer floorSort;

	@TableField("house_id")
	@Description("户室id")
	private Long houseId;
	@TableField("floor_id")
	@Description("楼层id")
	private Long floorId;
	@TableField("unit_id")
	@Description("单元id")
	private Long unitId;
	@TableField(exist = false)
	@Description("excel导入错误信息描述")
	private String excelErrRemarks;
	@Description("物业机构id")
	@TableField("agency_id")
	private Long agencyId;

	public Long getAgencyId() {
		return agencyId;
	}

	public void setAgencyId(Long agencyId) {
		this.agencyId = agencyId;
	}
// ID赋值
public HouseMasterInfo(){
        this.houseMasterId= returnIdLong();
        }
@Override
protected Serializable pkVal() {
        return this.houseMasterId;
}

	public Long getFloorId() {
		return floorId;
	}

	public void setFloorId(Long floorId) {
		this.floorId = floorId;
	}

	public Long getUnitId() {
		return unitId;
	}

	public void setUnitId(Long unitId) {
		this.unitId = unitId;
	}

	public String getExcelErrRemarks() {
		return excelErrRemarks;
	}

	public void setExcelErrRemarks(String excelErrRemarks) {
		this.excelErrRemarks = excelErrRemarks;
	}

	public Long getHouseId() {
		return houseId;
	}

	public void setHouseId(Long houseId) {
		this.houseId = houseId;
	}

	/**
	 * 设置：户主信息id
	 */
	public void setHouseMasterId(Long houseMasterId) {
		this.houseMasterId = houseMasterId;
	}
	/**
	 * 获取：户主信息id
	 */
	public Long getHouseMasterId() {
		return houseMasterId;
	}
	/**
	 * 设置：户主姓名
	 */
	public void setHouseMasterName(String houseMasterName) {
		this.houseMasterName = houseMasterName;
	}
	/**
	 * 获取：户主姓名
	 */
	public String getHouseMasterName() {
		return houseMasterName;
	}
	/**
	 * 设置：户主性别(1-男，2-女，3-未知)
	 */
	public void setHouseMasterSex(Integer houseMasterSex) {
		this.houseMasterSex = houseMasterSex;
	}
	/**
	 * 获取：户主性别(1-男，2-女，3-未知)
	 */
	public Integer getHouseMasterSex() {
		return houseMasterSex;
	}
	/**
	 * 设置：户主联系电话
	 */
	public void setHouseMasterPhone(String houseMasterPhone) {
		this.houseMasterPhone = houseMasterPhone;
	}
	/**
	 * 获取：户主联系电话
	 */
	public String getHouseMasterPhone() {
		return houseMasterPhone;
	}
	/**
	 * 设置：户主座机号码
	 */
	public void setHouseMasterTelphone(String houseMasterTelphone) {
		this.houseMasterTelphone = houseMasterTelphone;
	}
	/**
	 * 获取：户主座机号码
	 */
	public String getHouseMasterTelphone() {
		return houseMasterTelphone;
	}
	/**
	 * 设置：社区id
	 */
	public void setCommunityId(Long communityId) {
		this.communityId = communityId;
	}
	/**
	 * 获取：社区id
	 */
	public Long getCommunityId() {
		return communityId;
	}
	/**
	 * 设置：单元名称
	 */
	public void setUnitName(String unitName) {
		this.unitName = unitName;
	}
	/**
	 * 获取：单元名称
	 */
	public String getUnitName() {
		return unitName;
	}
	/**
	 * 设置：
	 */
	public void setHouseNumber(String houseNumber) {
		this.houseNumber = houseNumber;
	}
	/**
	 * 获取：
	 */
	public String getHouseNumber() {
		return houseNumber;
	}
	/**
	 * 设置：户室面积
	 */
	public void setHouseProportion(Double houseProportion) {
		this.houseProportion = houseProportion;
	}
	/**
	 * 获取：户室面积
	 */
	public Double getHouseProportion() {
		return houseProportion;
	}
	/**
	 * 设置：楼层序号(从房号截取)
	 */
	public void setFloorSort(Integer floorSort) {
		this.floorSort = floorSort;
	}
	/**
	 * 获取：楼层序号(从房号截取)
	 */
	public Integer getFloorSort() {
		return floorSort;
	}


}
