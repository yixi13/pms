package com.pms.entity;

import com.baomidou.mybatisplus.activerecord.Model;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.persistence.*;

import com.pms.util.Description;
import com.pms.entity.BaseModel;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;


/**
 * @author zyl
 * @email 517770986@qq.com
 * @date 2017-11-07 10:15:34
 */
@TableName("charge_template")
public class ChargeTemplate extends BaseModel<ChargeTemplate> {
    private static final long serialVersionUID = 1L;
    /**
     * 费用模板id
     */
    @TableId("charge_template_id")
    private Long chargeTemplateId;
    /**
     * 收费模板名称
     */
    @TableField("charge_template_name")
    @Description("收费模板名称")
    private String chargeTemplateName;
    /**
     * 创建时间
     */
    @TableField("create_time")
    @Description("创建时间")
    private Date createTime;
    /**
     * 备注信息
     */
    @TableField("charge_template_remark")
    @Description("备注信息")
    private String chargeTemplateRemark;
    /**
     * 修改时间
     */
    @TableField("update_time")
    @Description("修改时间")
    private Date updateTime;
    /**
     * 创建人
     */
    @TableField("create_by")
    @Description("创建人")
    private Long createBy;
    /**
     * 修改人
     */
    @TableField("update_by")
    @Description("修改人")
    private Long updateBy;

    /**
     * 社区id
     */
    @TableField("community_id")
    @Description("社区id")
    private Long communityId;

    @TableField(exist=false)
    @Description("模板收费详情")
    private List<ChargeValue> templateDetail;
    @Description("物业机构id")
    @TableField("agency_id")
    private Long agencyId;

    public Long getAgencyId() {
        return agencyId;
    }

    public void setAgencyId(Long agencyId) {
        this.agencyId = agencyId;
    }
    // ID赋值
    public ChargeTemplate() {
        this.chargeTemplateId = returnIdLong();
    }

    @Override
    protected Serializable pkVal() {
        return this.chargeTemplateId;
    }

    public List<ChargeValue> getTemplateDetail() {
        return templateDetail;
    }

    public void setTemplateDetail(List<ChargeValue> templateDetail) {
        this.templateDetail = templateDetail;
    }

    /**
     * 设置：费用模板id
     */
    public ChargeTemplate setChargeTemplateId(Long chargeTemplateId) {
        this.chargeTemplateId = chargeTemplateId;
        return this;
    }

    /**
     * 获取：费用模板id
     */
    public Long getChargeTemplateId() {
        return chargeTemplateId;
    }

    /**
     * 设置：收费模板名称
     */
    public ChargeTemplate setChargeTemplateName(String chargeTemplateName) {
        this.chargeTemplateName = chargeTemplateName;
        return this;
    }

    /**
     * 获取：收费模板名称
     */
    public String getChargeTemplateName() {
        return chargeTemplateName;
    }

    /**
     * 设置：创建时间
     */
    public ChargeTemplate setCreateTime(Date createTime) {
        this.createTime = createTime;
        return this;
    }

    /**
     * 获取：创建时间
     */
    public Date getCreateTime() {
        return createTime;
    }

    /**
     * 设置：备注信息
     */
    public ChargeTemplate setChargeTemplateRemark(String chargeTemplateRemark) {
        this.chargeTemplateRemark = chargeTemplateRemark;
        return this;
    }

    /**
     * 获取：备注信息
     */
    public String getChargeTemplateRemark() {
        return chargeTemplateRemark;
    }

    /**
     * 设置：修改时间
     */
    public ChargeTemplate setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
        return this;
    }

    /**
     * 获取：修改时间
     */
    public Date getUpdateTime() {
        return updateTime;
    }

    /**
     * 设置：创建人
     */
    public ChargeTemplate setCreateBy(Long createBy) {
        this.createBy = createBy;
        return this;
    }

    /**
     * 获取：创建人
     */
    public Long getCreateBy() {
        return createBy;
    }

    /**
     * 设置：修改人
     */
    public ChargeTemplate setUpdateBy(Long updateBy) {
        this.updateBy = updateBy;
        return this;
    }

    /**
     * 获取：修改人
     */
    public Long getUpdateBy() {
        return updateBy;
    }

    /**
     * 设置：社区id
     */
    public ChargeTemplate setCommunityId(Long communityId) {
        this.communityId = communityId;
        return this;
    }

    /**
     * 获取：社区id
     */
    public Long getCommunityId() {
        return communityId;
    }


}
