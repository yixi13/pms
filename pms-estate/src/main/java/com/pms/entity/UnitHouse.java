package com.pms.entity;

import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableName;
import com.pms.util.Description;
import com.pms.util.idutil.SnowFlake;

import java.io.Serializable;

/**
 * <p>
 * 
 * </p>
 *
 * @author ASUS_B
 * @since 2017-10-24
 */
@TableName("unit_house")
public class UnitHouse extends BaseModel<UnitHouse> {

    private static final long serialVersionUID = 1L;
	public UnitHouse(){
		this.houseId = returnIdLong();
	}

    @TableId("house_id")
	@Description("户室id")
	private Long houseId;
    /**
     * 房屋号  (楼层号 + 房号  eg: 07+05  705)
     */
	@Description("房屋号")
	@TableField("house_number")
	private String houseNumber;
    /**
     * 楼层Id
     */
	@Description("楼层Id")
	@TableField("floor_id")
	private Long floorId;
    /**
     * 单元ID
     */
	@Description("单元ID")
	@TableField("unit_id")
	private Long unitId;
    /**
     * 社区id
     */
	@Description("社区id")
	@TableField("community_id")
	private Long communityId;
    /**
     * 户室面积(m²)
     */
	@Description("户室面积(m²)")
	@TableField("house_proportion")
	private Double houseProportion;
    /**
     * 物业费模板Id
     */
	@Description("物业费模板Id")
	@TableField("charge_template_id")
	private Long chargeTemplateId;
    /**
     * 是否有会员认证(1-有，2-无)
     */
	@Description("是否有会员认证(1-有，2-无)")
	@TableField("is_have_member")
	private Integer isHaveMember;
	/**
	 * 是否 导入户主信息(1-是，2-否)
	 */
	@Description("是否 导入户主信息(1-是，2-否)")
	@TableField("is_have_master")
	private Integer isHaveMaster;

	@Description("户室排序")
	@TableField("house_sort")
	private Integer houseSort;

	@Description("楼层排序序号")
	@TableField("floor_sort")
	private Integer floorSort;

    /**
     * 单元名称
     */
	@Description("单元名称")
	@TableField("unit_name")
	private String unitName;
    /**
     * 1-显示，2-不显示,只用于App查询
     */
	@Description("App是否显示(1-显示，2-不显示)")
	@TableField("is_show_app")
	private Integer isShowApp;
    /**
     * 备注
     */
	@Description("备注信息")
	@TableField("house_remarks")
	private String houseRemarks;
	/**
	 * 是否是物业办公室（1-是，2-不是）
	 */
	@Description("是否是物业办公室(1-是,2-不是)")
	@TableField("is_estate_house")
	private Integer isEstateHouse;
	@Description("是否配置物业费模板(2-否，1-是)")
	@TableField("is_hava_charge_template")
	private Integer isHavaChargeTemplate;
	@Description("物业费模板名称")
	@TableField("charge_template_name")
	private String chargeTemplateName;
	@Description("物业机构id")
	@TableField("agency_id")
	private Long agencyId;

	public Long getAgencyId() {
		return agencyId;
	}

	public void setAgencyId(Long agencyId) {
		this.agencyId = agencyId;
	}
	public String getChargeTemplateName() {
		return chargeTemplateName;
	}

	public void setChargeTemplateName(String chargeTemplateName) {
		this.chargeTemplateName = chargeTemplateName;
	}

	public Integer getFloorSort() {
		return floorSort;
	}

	public void setFloorSort(Integer floorSort) {
		this.floorSort = floorSort;
	}

	public Long getChargeTemplateId() {
		return chargeTemplateId;
	}

	public void setChargeTemplateId(Long chargeTemplateId) {
		this.chargeTemplateId = chargeTemplateId;
	}

	public Integer getIsHavaChargeTemplate() {
		return isHavaChargeTemplate;
	}

	public void setIsHavaChargeTemplate(Integer isHavaTemplate) {
		this.isHavaChargeTemplate = isHavaTemplate;
	}

	public Integer getIsEstateHouse() {
		return isEstateHouse;
	}

	public void setIsEstateHouse(Integer isEstateHouse) {
		this.isEstateHouse = isEstateHouse;
	}

	public Long getHouseId() {
		return houseId;
	}

	public void setHouseId(Long houseId) {
		this.houseId = houseId;
	}

	public String getHouseNumber() {
		return houseNumber;
	}

	public void setHouseNumber(String houseNumber) {
		this.houseNumber = houseNumber;
	}

	public Long getFloorId() {
		return floorId;
	}

	public void setFloorId(Long floorId) {
		this.floorId = floorId;
	}

	public Long getUnitId() {
		return unitId;
	}

	public void setUnitId(Long unitId) {
		this.unitId = unitId;
	}

	public Long getCommunityId() {
		return communityId;
	}

	public void setCommunityId(Long communityId) {
		this.communityId = communityId;
	}

	public Double getHouseProportion() {
		return houseProportion;
	}

	public void setHouseProportion(Double houseProportion) {
		this.houseProportion = houseProportion;
	}

	public Integer getIsHaveMember() {
		return isHaveMember;
	}

	public void setIsHaveMember(Integer isHaveMember) {
		this.isHaveMember = isHaveMember;
	}

	public Integer getIsHaveMaster() {
		return isHaveMaster;
	}

	public void setIsHaveMaster(Integer isHaveMaster) {
		this.isHaveMaster = isHaveMaster;
	}

	public Integer getHouseSort() {
		return houseSort;
	}

	public void setHouseSort(Integer houseSort) {
		this.houseSort = houseSort;
	}

	public String getUnitName() {
		return unitName;
	}

	public void setUnitName(String unitName) {
		this.unitName = unitName;
	}

	public Integer getIsShowApp() {
		return isShowApp;
	}

	public void setIsShowApp(Integer isShowApp) {
		this.isShowApp = isShowApp;
	}

	public String getHouseRemarks() {
		return houseRemarks;
	}

	public void setHouseRemarks(String houseRemarks) {
		this.houseRemarks = houseRemarks;
	}

	@Override
	protected Serializable pkVal() {
		return this.houseId;
	}

}
