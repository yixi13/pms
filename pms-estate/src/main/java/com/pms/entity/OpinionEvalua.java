package com.pms.entity;
import com.baomidou.mybatisplus.activerecord.Model;
import java.io.Serializable;
import java.util.Date;
import javax.persistence.*;
import com.pms.util.Description;
import com.pms.entity.BaseModel;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;


/**
 * 
 * 
 * @author zyl
 * @email 517770986@qq.com
 * @date 2017-11-02 14:10:56
 */
@TableName( "opinion_evalua")
public class OpinionEvalua extends BaseModel<OpinionEvalua> {
private static final long serialVersionUID = 1L;


	    //物业投诉回复表
    @TableId("evalua_id")
    private Long evaluaId;
	
	    //投诉表ID
    @TableField("opinion_id")
    @Description("投诉表ID")
    private Long opinionId;
	
	    //物业回复内容
    @TableField("reply_details")
    @Description("物业回复内容")
    private String replyDetails;
	
	    //
    @TableField("create_by")
    @Description("")
    private String createBy;
	
	    //
    @TableField("create_time")
    @Description("")
    private Date createTime;
	
	    //
    @TableField("update_by")
    @Description("")
    private String updateBy;
	
	    //
    @TableField("update_time")
    @Description("")
    private Date updateTime;
	
// ID赋值
public OpinionEvalua(){
        this.evaluaId= returnIdLong();
        }
@Override
protected Serializable pkVal() {
        return this.evaluaId;
}
	/**
	 * 设置：物业投诉回复表
	 */
	public void setEvaluaId(Long evaluaId) {
		this.evaluaId = evaluaId;
	}
	/**
	 * 获取：物业投诉回复表
	 */
	public Long getEvaluaId() {
		return evaluaId;
	}
	/**
	 * 设置：投诉表ID
	 */
	public void setOpinionId(Long opinionId) {
		this.opinionId = opinionId;
	}
	/**
	 * 获取：投诉表ID
	 */
	public Long getOpinionId() {
		return opinionId;
	}
	/**
	 * 设置：物业回复内容
	 */
	public void setReplyDetails(String replyDetails) {
		this.replyDetails = replyDetails;
	}
	/**
	 * 获取：物业回复内容
	 */
	public String getReplyDetails() {
		return replyDetails;
	}
	/**
	 * 设置：
	 */
	public void setCreateBy(String createBy) {
		this.createBy = createBy;
	}
	/**
	 * 获取：
	 */
	public String getCreateBy() {
		return createBy;
	}
	/**
	 * 设置：
	 */
	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}
	/**
	 * 获取：
	 */
	public Date getCreateTime() {
		return createTime;
	}
	/**
	 * 设置：
	 */
	public void setUpdateBy(String updateBy) {
		this.updateBy = updateBy;
	}
	/**
	 * 获取：
	 */
	public String getUpdateBy() {
		return updateBy;
	}
	/**
	 * 设置：
	 */
	public void setUpdateTime(Date updateTime) {
		this.updateTime = updateTime;
	}
	/**
	 * 获取：
	 */
	public Date getUpdateTime() {
		return updateTime;
	}


}
