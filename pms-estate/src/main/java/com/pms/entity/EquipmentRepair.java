package com.pms.entity;
import com.baomidou.mybatisplus.activerecord.Model;
import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.persistence.*;
import com.pms.util.Description;
import com.pms.entity.BaseModel;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;


/**
 * 
 * 
 * @author zyl
 * @email 517770986@qq.com
 * @date 2017-11-01 14:31:47
 */
@TableName( "equipment_repair")
public class EquipmentRepair extends BaseModel<EquipmentRepair> {
private static final long serialVersionUID = 1L;


	    //物业报修表
    @TableId("repair_id")
    private Long repairId;
	
	    //会员ID
    @TableField("member_id")
    @Description("会员ID")
    private Long memberId;
	
	    //机构ID
    @TableField("agency_id")
    @Description("机构ID")
    private Long agencyId;
	
	    //标题
    @TableField("equipment_name")
    @Description("标题")
    private String equipmentName;
	
	    //报修详情说明
    @TableField("description")
    @Description("报修详情说明")
    private String description;
	
	    //实际处理时间
    @TableField("processing_time")
    @Description("实际处理时间")
    private Date processingTime;
	
	    //物业维修人员
    @TableField("name_")
    @Description("物业维修人员")
    private String name;
	
	    //物业维修人员手机号
    @TableField("phone_")
    @Description("物业维修人员手机号")
    private String phone;
	
	    //状态1.已报修 2.处理中3.已处理(待评价)4.已评价
    @TableField("state_")
    @Description("状态1.已报修 2.处理中3.已处理(待评价)4.已评价")
    private Integer state;
	
	    //房间ID
    @TableField("house_id")
    @Description("房间ID")
    private Long houseId;
	
	    //期望处理日期
    @TableField("expectation_time")
    @Description("期望处理日期")
    private Date expectationTime;
	
	    //1.室内 2.公共区域
    @TableField("choice_")
    @Description("1.室内 2.公共区域")
    private Integer choice;
	
	    //报修选项ID
    @TableField("content_id")
    @Description("报修选项ID")
    private Long contentId;
	
	    //创建时间
    @TableField("create_time")
    @Description("创建时间")
    private Date createTime;

	@TableField(exist = false)
	private List<RepairImg> repairImg;
	@TableField(exist = false)
	private List<RepairEvalua> repairEvalua;
	@TableField(exist = false)
	private List<RepairContent>repairContent;
	@TableField(exist = false)
	private List<UnitHouse> unitHouse;

	@TableField("house_name")
	@Description("房屋信息")
	private String houseName;

	public String getHouseName() {
		return houseName;
	}

	public void setHouseName(String houseName) {
		this.houseName = houseName;
	}

	public List<RepairImg> getRepairImg() {
		return repairImg;
	}

	public List<RepairEvalua> getRepairEvalua() {
		return repairEvalua;
	}

	public List<RepairContent> getRepairContent() {
		return repairContent;
	}

	public List<UnitHouse> getUnitHouse() {
		return unitHouse;
	}

	public void setRepairImg(List<RepairImg> repairImg) {
		this.repairImg = repairImg;
	}

	public void setRepairEvalua(List<RepairEvalua> repairEvalua) {
		this.repairEvalua = repairEvalua;
	}

	public void setRepairContent(List<RepairContent> repairContent) {
		this.repairContent = repairContent;
	}

	public void setUnitHouse(List<UnitHouse> unitHouse) {
		this.unitHouse = unitHouse;
	}

	// ID赋值
public EquipmentRepair(){
        this.repairId= returnIdLong();
        }
@Override
protected Serializable pkVal() {
        return this.repairId;
}
	/**
	 * 设置：物业报修表
	 */
	public void setRepairId(Long repairId) {
		this.repairId = repairId;
	}
	/**
	 * 获取：物业报修表
	 */
	public Long getRepairId() {
		return repairId;
	}
	/**
	 * 设置：会员ID
	 */
	public void setMemberId(Long memberId) {
		this.memberId = memberId;
	}
	/**
	 * 获取：会员ID
	 */
	public Long getMemberId() {
		return memberId;
	}
	/**
	 * 设置：机构ID
	 */
	public void setAgencyId(Long agencyId) {
		this.agencyId = agencyId;
	}
	/**
	 * 获取：机构ID
	 */
	public Long getAgencyId() {
		return agencyId;
	}
	/**
	 * 设置：标题
	 */
	public void setEquipmentName(String equipmentName) {
		this.equipmentName = equipmentName;
	}
	/**
	 * 获取：标题
	 */
	public String getEquipmentName() {
		return equipmentName;
	}
	/**
	 * 设置：报修详情说明
	 */
	public void setDescription(String description) {
		this.description = description;
	}
	/**
	 * 获取：报修详情说明
	 */
	public String getDescription() {
		return description;
	}
	/**
	 * 设置：实际处理时间
	 */
	public void setProcessingTime(Date processingTime) {
		this.processingTime = processingTime;
	}
	/**
	 * 获取：实际处理时间
	 */
	public Date getProcessingTime() {
		return processingTime;
	}
	/**
	 * 设置：物业维修人员
	 */
	public void setName(String name) {
		this.name = name;
	}
	/**
	 * 获取：物业维修人员
	 */
	public String getName() {
		return name;
	}
	/**
	 * 设置：物业维修人员手机号
	 */
	public void setPhone(String phone) {
		this.phone = phone;
	}
	/**
	 * 获取：物业维修人员手机号
	 */
	public String getPhone() {
		return phone;
	}
	/**
	 * 设置：状态1.已报修 2.处理中3.已处理(待评价)4.已评价
	 */
	public void setState(Integer state) {
		this.state = state;
	}
	/**
	 * 获取：状态1.已报修 2.处理中3.已处理(待评价)4.已评价
	 */
	public Integer getState() {
		return state;
	}
	/**
	 * 设置：房间ID
	 */
	public void setHouseId(Long houseId) {
		this.houseId = houseId;
	}
	/**
	 * 获取：房间ID
	 */
	public Long getHouseId() {
		return houseId;
	}
	/**
	 * 设置：期望处理日期
	 */
	public void setExpectationTime(Date expectationTime) {
		this.expectationTime = expectationTime;
	}
	/**
	 * 获取：期望处理日期
	 */
	public Date getExpectationTime() {
		return expectationTime;
	}
	/**
	 * 设置：1.室内 2.公共区域
	 */
	public void setChoice(Integer choice) {
		this.choice = choice;
	}
	/**
	 * 获取：1.室内 2.公共区域
	 */
	public Integer getChoice() {
		return choice;
	}
	/**
	 * 设置：报修选项ID
	 */
	public void setContentId(Long contentId) {
		this.contentId = contentId;
	}
	/**
	 * 获取：报修选项ID
	 */
	public Long getContentId() {
		return contentId;
	}
	/**
	 * 设置：创建时间
	 */
	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}
	/**
	 * 获取：创建时间
	 */
	public Date getCreateTime() {
		return createTime;
	}


}
