package com.pms.entity;
import com.baomidou.mybatisplus.activerecord.Model;
import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.persistence.*;
import com.pms.util.Description;
import com.pms.entity.BaseModel;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;


/**
 * 
 * 
 * @author zyl
 * @email 517770986@qq.com
 * @date 2017-11-03 09:48:33
 */
@TableName( "estate_notice")
public class EstateNotice extends BaseModel<EstateNotice> {
private static final long serialVersionUID = 1L;


	    /**
	 *物业公告信息表
	 */
    @TableId("notice_id")
    private Long noticeId;
	
	    /**
     *名称
     */
    @TableField("name_")
    @Description("名称")
    private String name;
	
	    /**
     *类型 1 消息新闻 2 物业公告 3 活动预告
     */
    @TableField("type_")
    @Description("类型 1 消息新闻 2 物业公告 3 活动预告")
    private Integer type;
	
	    /**
     *是否置顶 1  置顶 2 否
     */
    @TableField("is_top")
    @Description("是否置顶 1  置顶 2 否")
    private Integer isTop;
	
	    /**
     *公告内容
     */
    @TableField("estate_descr")
    @Description("公告内容")
    private String estateDescr;
	
	    /**
     *开始时间
     */
    @TableField("strat_time")
    @Description("开始时间")
    private Date stratTime;
	
	    /**
     *结束时间
     */
    @TableField("end_time")
    @Description("结束时间")
    private Date endTime;
	
	    /**
     *创建时间
     */
    @TableField("create_time")
    @Description("创建时间")
    private Date createTime;


	    /**
     *总人数
     */
    @TableField("number_")
    @Description("总人数")
    private Integer number;
	
	    /**
     *标题图片
     */
    @TableField("title_img")
    @Description("标题图片")
    private String titleImg;

	@TableField(exist = false)
	private List<EstateNoticeAgency> estateNoticeAgency;

	public List<EstateNoticeAgency> getEstateNoticeAgency() {
		return estateNoticeAgency;
	}

	public void setEstateNoticeAgency(List<EstateNoticeAgency> estateNoticeAgency) {
		this.estateNoticeAgency = estateNoticeAgency;
	}

	// ID赋值
public EstateNotice(){
        this.noticeId= returnIdLong();
        }
@Override
protected Serializable pkVal() {
        return this.noticeId;
}
	/**
	 * 设置：物业公告信息表
	 */
	public void setNoticeId(Long noticeId) {
		this.noticeId = noticeId;
	}
	/**
	 * 获取：物业公告信息表
	 */
	public Long getNoticeId() {
		return noticeId;
	}
	/**
	 * 设置：名称
	 */
	public void setName(String name) {
		this.name = name;
	}
	/**
	 * 获取：名称
	 */
	public String getName() {
		return name;
	}
	/**
	 * 设置：类型 1 消息新闻 2 物业公告 3 活动预告
	 */
	public void setType(Integer type) {
		this.type = type;
	}
	/**
	 * 获取：类型 1 消息新闻 2 物业公告 3 活动预告
	 */
	public Integer getType() {
		return type;
	}
	/**
	 * 设置：是否置顶 1  置顶 2 否
	 */
	public void setIsTop(Integer isTop) {
		this.isTop = isTop;
	}
	/**
	 * 获取：是否置顶 1  置顶 2 否
	 */
	public Integer getIsTop() {
		return isTop;
	}
	/**
	 * 设置：公告内容
	 */
	public void setEstateDescr(String estateDescr) {
		this.estateDescr = estateDescr;
	}
	/**
	 * 获取：公告内容
	 */
	public String getEstateDescr() {
		return estateDescr;
	}
	/**
	 * 设置：开始时间
	 */
	public void setStratTime(Date stratTime) {
		this.stratTime = stratTime;
	}
	/**
	 * 获取：开始时间
	 */
	public Date getStratTime() {
		return stratTime;
	}
	/**
	 * 设置：结束时间
	 */
	public void setEndTime(Date endTime) {
		this.endTime = endTime;
	}
	/**
	 * 获取：结束时间
	 */
	public Date getEndTime() {
		return endTime;
	}
	/**
	 * 设置：创建时间
	 */
	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}
	/**
	 * 获取：创建时间
	 */
	public Date getCreateTime() {
		return createTime;
	}

	public Integer getNumber() {
		return number;
	}

	public void setNumber(Integer number) {
		this.number = number;
	}

	/**
	 * 设置：标题图片
	 */
	public void setTitleImg(String titleImg) {
		this.titleImg = titleImg;
	}
	/**
	 * 获取：标题图片
	 */
	public String getTitleImg() {
		return titleImg;
	}


}
