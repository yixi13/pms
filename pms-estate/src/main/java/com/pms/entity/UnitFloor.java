package com.pms.entity;

import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableName;
import com.pms.util.Description;
import com.pms.util.idutil.SnowFlake;

import java.io.Serializable;

/**
 * <p>
 * 
 * </p>
 *
 * @author ASUS_B
 * @since 2017-10-24
 */
@TableName("unit_floor")
public class UnitFloor extends BaseModel<UnitFloor> {

    private static final long serialVersionUID = 1L;
	public UnitFloor(){
		this.floorId = returnIdLong();;
	}

    @TableId("floor_id")
	@Description("楼层id")
	private Long floorId;
    /**
     * 楼层
     */
	@Description("单元名称")
	@TableField("floor_name")
	private String floorName;
    /**
     * 单元ID
     */
	@Description("单元id")
	@TableField("unit_id")
	private Long unitId;

	@Description("小区id")
	@TableField("community_id")
	private Long communityId;
    /**
     * 楼层排序
     */
	@Description("楼层排序")
	@TableField("floor_sort")
	private Integer floorSort;
    /**
     * 1-显示，2-不显示,只用于App查询
     */
	@Description("App是否显示(1-显示|2-不显示)")
	@TableField("is_show_app")
	private Integer isShowApp;
	@Description("物业机构id")
	@TableField("agency_id")
	private Long agencyId;

	public Long getAgencyId() {
		return agencyId;
	}

	public void setAgencyId(Long agencyId) {
		this.agencyId = agencyId;
	}

	public Long getFloorId() {
		return floorId;
	}

	public void setFloorId(Long floorId) {
		this.floorId = floorId;
	}

	public String getFloorName() {
		return floorName;
	}

	public void setFloorName(String floorName) {
		this.floorName = floorName;
	}

	public Long getUnitId() {
		return unitId;
	}

	public void setUnitId(Long unitId) {
		this.unitId = unitId;
	}

	public Long getCommunityId() {
		return communityId;
	}

	public void setCommunityId(Long communityId) {
		this.communityId = communityId;
	}

	public Integer getFloorSort() {
		return floorSort;
	}

	public void setFloorSort(Integer floorSort) {
		this.floorSort = floorSort;
	}

	public Integer getIsShowApp() {
		return isShowApp;
	}

	public void setIsShowApp(Integer isShowApp) {
		this.isShowApp = isShowApp;
	}

	@Override
	protected Serializable pkVal() {
		return this.floorId;
	}

}
