package com.pms.entity;

import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableName;
import com.pms.util.Description;
import com.pms.util.idutil.SnowFlake;

import java.io.Serializable;

/**
 * <p>
 * 
 * </p>
 *
 * @author ASUS_B
 * @since 2017-10-24
 */
@TableName("unit_building")
public class UnitBuilding extends BaseModel<UnitBuilding> {

    private static final long serialVersionUID = 1L;
	public UnitBuilding(){
		this.unitId = returnIdLong();
	}
    /**
     * 单元id
     */
    @Description("单元id")
    @TableId("unit_id")
	private Long unitId;
    /**
     * 单元名称
     */
	@Description("单元名称")
	@TableField("unit_name")
	private String unitName;
    /**
     * 单元备注  描述
     */
	@Description("单元备注信息")
	@TableField("unit_remarks")
	private String unitRemarks;
    /**
     * 社区id
     */
	@Description("社区id")
	@TableField("community_id")
	private Long communityId;
    /**
     * 单元编号
     */
	@Description("单元编号")
	@TableField("unit_num")
	private Integer unitNum;
    /**
     * 所属社区编号
     */
	@Description("社区编号")
	@TableField("community_num")
	private Integer communityNum;
    /**
     * 用于app是否显示
     */
	@Description("App是否显示(1-显示|2-不显示)")
	@TableField("is_show_app")
	private Integer isShowApp;
	@Description("物业机构id")
	@TableField("agency_id")
	private Long agencyId;

	public Long getAgencyId() {
		return agencyId;
	}

	public void setAgencyId(Long agencyId) {
		this.agencyId = agencyId;
	}

	public Long getUnitId() {
		return unitId;
	}

	public void setUnitId(Long unitId) {
		this.unitId = unitId;
	}

	public String getUnitName() {
		return unitName;
	}

	public void setUnitName(String unitName) {
		this.unitName = unitName;
	}

	public String getUnitRemarks() {
		return unitRemarks;
	}

	public void setUnitRemarks(String unitRemarks) {
		this.unitRemarks = unitRemarks;
	}

	public Long getCommunityId() {
		return communityId;
	}

	public void setCommunityId(Long communityId) {
		this.communityId = communityId;
	}

	public Integer getUnitNum() {
		return unitNum;
	}

	public void setUnitNum(Integer unitNum) {
		this.unitNum = unitNum;
	}

	public Integer getCommunityNum() {
		return communityNum;
	}

	public void setCommunityNum(Integer communityNum) {
		this.communityNum = communityNum;
	}

	public Integer getIsShowApp() {
		return isShowApp;
	}

	public void setIsShowApp(Integer isShowApp) {
		this.isShowApp = isShowApp;
	}

	@Override
	protected Serializable pkVal() {
		return this.unitId;
	}

}
