package com.pms.entity;
import java.io.Serializable;
import com.pms.util.Description;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;


/**
 * 
 * 
 * @author zyl
 * @email 517770986@qq.com
 * @date 2017-11-01 14:33:01
 */
@TableName( "repair_content")
public class RepairContent extends BaseModel<RepairContent> {
private static final long serialVersionUID = 1L;


	    //物业报修内容表
    @TableId("content_id")
    private Long contentId;
	
	    //1.室内 2.公共区域
    @TableField("state_")
    @Description("1.室内 2.公共区域")
    private Integer state;
	
	    //报修选项
    @TableField("content_")
    @Description("报修选项")
    private String content;

	
// ID赋值
public RepairContent(){
        this.contentId= returnIdLong();
        }
@Override
protected Serializable pkVal() {
        return this.contentId;
}
	/**
	 * 设置：物业报修内容表
	 */
	public void setContentId(Long contentId) {
		this.contentId = contentId;
	}
	/**
	 * 获取：物业报修内容表
	 */
	public Long getContentId() {
		return contentId;
	}
	/**
	 * 设置：1.室内 2.公共区域
	 */
	public void setState(Integer state) {
		this.state = state;
	}
	/**
	 * 获取：1.室内 2.公共区域
	 */
	public Integer getState() {
		return state;
	}
	/**
	 * 设置：报修选项
	 */
	public void setContent(String content) {
		this.content = content;
	}
	/**
	 * 获取：报修选项
	 */
	public String getContent() {
		return content;
	}



}
