package com.pms.entity;

import com.baomidou.mybatisplus.activerecord.Model;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.*;

import com.pms.util.Description;
import com.pms.entity.BaseModel;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;


/**
 * @author zyl
 * @email 517770986@qq.com
 * @date 2017-11-20 17:18:36
 */
@TableName("estate_door_lock")
public class EstateDoorLock extends BaseModel<EstateDoorLock> {
    private static final long serialVersionUID = 1L;


    /**
     *
     */
    @TableId("lock_id")
    private Long lockId;

    /**
     * 设备序列号/id
     */
    @TableField("eq_sn")
    @Description("设备序列号/id")
    private String eqSn;

    /**
     * 设备mac地址
     */
    @TableField("eq_mac")
    @Description("设备mac地址")
    private String eqMac;

    /**
     * 设备密钥
     */
    @TableField("eq_key")
    @Description("设备密钥")
    private String eqKey;

    /**
     * 设备名称
     */
    @TableField("eq_name")
    @Description("设备名称")
    private String eqName;

    /**
     * 设备厂商提供的参数(智果-devType)
     */
    @TableField("eq_type_factory")
    @Description("设备厂商提供的参数(智果-devType)")
    private Integer eqTypeFactory;

    /**
     * 设备密码
     */
    @TableField("eq_pswd")
    @Description("设备密码")
    private String eqPswd;

    /**
     * 关联门id
     */
    @TableField("door_id")
    @Description("关联门id")
    private Long doorId;

    /**
     * 关联门名称
     */
    @TableField("door_name")
    @Description("关联门名称")
    private String doorName;

    /**
     * 社区id
     */
    @TableField("conmunity_id")
    @Description("社区id")
    private Long conmunityId;

    /**
     * 社区名称
     */
    @TableField("conmunity_name")
    @Description("社区名称")
    private String conmunityName;

    /**
     * 物业机构id
     */
    @TableField("agency_id")
    @Description("物业机构id")
    private Long agencyId;

    /**
     * 创建时间
     */
    @TableField("create_time")
    @Description("创建时间")
    private Date createTime;

    /**
     * 设备开门密码有效时间(空值永久有效)
     */
    @TableField("pswd_valid_time")
    @Description("设备开门密码有效时间(空值永久有效)")
    private Date pswdValidTime;

    /**
     * 厂家编号
     */
    @TableField("factory_code")
    @Description("厂家编号")
    private String factoryCode;

    /**
     * 厂家服务地址
     */
    @TableField("factory_service_url")
    @Description("厂家服务地址")
    private String factoryServiceUrl;

    /**
     * 按键开门密码
     */
    @TableField("eq_input_pswd")
    @Description("按键开门密码")
    private String eqInputPswd;

    /**
     * 单元id
     */
    @TableField("unit_id")
    @Description("单元id")
    private Long unitId;

    /**
     * 门类型(1-大门,2单元门,3车库门)
     */
    @TableField("door_type")
    @Description("门类型(1-大门,2单元门,3车库门)")
    private Integer doorType;

    // ID赋值
    public EstateDoorLock() {
        this.lockId = returnIdLong();
    }

    @Override
    protected Serializable pkVal() {
        return this.lockId;
    }

    /**
     * 设置：
     */
    public EstateDoorLock setLockId(Long lockId) {
        this.lockId = lockId;
        return this;
    }

    /**
     * 获取：
     */
    public Long getLockId() {
        return lockId;
    }

    /**
     * 设置：设备序列号/id
     */
    public EstateDoorLock setEqSn(String eqSn) {
        this.eqSn = eqSn;
        return this;
    }

    /**
     * 获取：设备序列号/id
     */
    public String getEqSn() {
        return eqSn;
    }

    /**
     * 设置：设备mac地址
     */
    public EstateDoorLock setEqMac(String eqMac) {
        this.eqMac = eqMac;
        return this;
    }

    /**
     * 获取：设备mac地址
     */
    public String getEqMac() {
        return eqMac;
    }

    /**
     * 设置：设备密钥
     */
    public EstateDoorLock setEqKey(String eqKey) {
        this.eqKey = eqKey;
        return this;
    }

    /**
     * 获取：设备密钥
     */
    public String getEqKey() {
        return eqKey;
    }

    /**
     * 设置：设备名称
     */
    public EstateDoorLock setEqName(String eqName) {
        this.eqName = eqName;
        return this;
    }

    /**
     * 获取：设备名称
     */
    public String getEqName() {
        return eqName;
    }

    /**
     * 设置：设备厂商提供的参数(智果-devType)
     */
    public EstateDoorLock setEqTypeFactory(Integer eqTypeFactory) {
        this.eqTypeFactory = eqTypeFactory;
        return this;
    }

    /**
     * 获取：设备厂商提供的参数(智果-devType)
     */
    public Integer getEqTypeFactory() {
        return eqTypeFactory;
    }

    /**
     * 设置：设备密码
     */
    public EstateDoorLock setEqPswd(String eqPswd) {
        this.eqPswd = eqPswd;
        return this;
    }

    /**
     * 获取：设备密码
     */
    public String getEqPswd() {
        return eqPswd;
    }

    /**
     * 设置：关联门id
     */
    public EstateDoorLock setDoorId(Long doorId) {
        this.doorId = doorId;
        return this;
    }

    /**
     * 获取：关联门id
     */
    public Long getDoorId() {
        return doorId;
    }

    /**
     * 设置：关联门名称
     */
    public EstateDoorLock setDoorName(String doorName) {
        this.doorName = doorName;
        return this;
    }

    /**
     * 获取：关联门名称
     */
    public String getDoorName() {
        return doorName;
    }

    /**
     * 设置：社区id
     */
    public EstateDoorLock setConmunityId(Long conmunityId) {
        this.conmunityId = conmunityId;
        return this;
    }

    /**
     * 获取：社区id
     */
    public Long getConmunityId() {
        return conmunityId;
    }

    /**
     * 设置：社区名称
     */
    public EstateDoorLock setConmunityName(String conmunityName) {
        this.conmunityName = conmunityName;
        return this;
    }

    /**
     * 获取：社区名称
     */
    public String getConmunityName() {
        return conmunityName;
    }

    /**
     * 设置：物业机构id
     */
    public EstateDoorLock setAgencyId(Long agencyId) {
        this.agencyId = agencyId;
        return this;
    }

    /**
     * 获取：物业机构id
     */
    public Long getAgencyId() {
        return agencyId;
    }

    /**
     * 设置：创建时间
     */
    public EstateDoorLock setCreateTime(Date createTime) {
        this.createTime = createTime;
        return this;
    }

    /**
     * 获取：创建时间
     */
    public Date getCreateTime() {
        return createTime;
    }

    /**
     * 设置：设备开门密码有效时间(空值永久有效)
     */
    public EstateDoorLock setPswdValidTime(Date pswdValidTime) {
        this.pswdValidTime = pswdValidTime;
        return this;
    }

    /**
     * 获取：设备开门密码有效时间(空值永久有效)
     */
    public Date getPswdValidTime() {
        return pswdValidTime;
    }

    /**
     * 设置：厂家id
     */
    public EstateDoorLock setFactoryCode(String factoryId) {
        this.factoryCode = factoryId;
        return this;
    }

    /**
     * 获取：厂家id
     */
    public String getFactoryCode() {
        return factoryCode;
    }

    /**
     * 设置：厂家服务地址
     */
    public EstateDoorLock setFactoryServiceUrl(String factoryServiceUrl) {
        this.factoryServiceUrl = factoryServiceUrl;
        return this;
    }

    /**
     * 获取：厂家服务地址
     */
    public String getFactoryServiceUrl() {
        return factoryServiceUrl;
    }

    /**
     * 设置：按键开门密码
     */
    public EstateDoorLock setEqInputPswd(String eqInputPswd) {
        this.eqInputPswd = eqInputPswd;
        return this;
    }

    /**
     * 获取：按键开门密码
     */
    public String getEqInputPswd() {
        return eqInputPswd;
    }

    /**
     * 设置：单元id
     */
    public EstateDoorLock setUnitId(Long unitId) {
        this.unitId = unitId;
        return this;
    }

    /**
     * 获取：单元id
     */
    public Long getUnitId() {
        return unitId;
    }

    /**
     * 设置：门类型(1-大门,2单元门,3车库门)
     */
    public EstateDoorLock setDoorType(Integer doorType) {
        this.doorType = doorType;
        return this;
    }

    /**
     * 获取：门类型(1-大门,2单元门,3车库门)
     */
    public Integer getDoorType() {
        return doorType;
    }


}
