package com.pms.entity;
import com.baomidou.mybatisplus.activerecord.Model;
import java.io.Serializable;
import java.util.Date;
import javax.persistence.*;
import com.pms.util.Description;
import com.pms.entity.BaseModel;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;


/**
 * 
 * 
 * @author zyl
 * @email 517770986@qq.com
 * @date 2017-10-27 14:52:23
 */
@TableName( "estate_eq_class")
public class EstateEqClass extends BaseModel<EstateEqClass> {
private static final long serialVersionUID = 1L;


	    //物业设备分类ID
    @TableId("eqClass_id")
    private Long eqclassId;
	
	    //物业设备分类名称
    @TableField("eqClass_name")
    @Description("物业设备分类名称")
    private String eqclassName;
	
	    //设备分类1.物业锁2.摄像头
    @TableField("type_")
    @Description("设备分类1.物业锁2.摄像头")
    private Integer type;
	
	    //父级ID
    @TableField("p_id")
    @Description("父级ID")
    private Long pId;
	
	    //级别
    @TableField("level_")
    @Description("级别")
    private Integer level;
	
	    //设备图片
    @TableField("eq_img")
    @Description("设备图片")
    private String eqImg;
	
	    //创建人
    @TableField("create_by")
    @Description("创建人")
    private String createBy;
	
	    //创建时间
    @TableField("create_time")
    @Description("创建时间")
    private Date createTime;
	
	    //修改人
    @TableField("update_by")
    @Description("修改人")
    private String updateBy;
	
	    //修改时间
    @TableField("update_time")
    @Description("修改时间")
    private Date updateTime;
	
// ID赋值
public EstateEqClass(){
        this.eqclassId= returnIdLong();
        }
@Override
protected Serializable pkVal() {
        return this.eqclassId;
}
	/**
	 * 设置：物业设备分类ID
	 */
	public void setEqclassId(Long eqclassId) {
		this.eqclassId = eqclassId;
	}
	/**
	 * 获取：物业设备分类ID
	 */
	public Long getEqclassId() {
		return eqclassId;
	}
	/**
	 * 设置：物业设备分类名称
	 */
	public void setEqclassName(String eqclassName) {
		this.eqclassName = eqclassName;
	}
	/**
	 * 获取：物业设备分类名称
	 */
	public String getEqclassName() {
		return eqclassName;
	}
	/**
	 * 设置：设备分类1.物业锁2.摄像头
	 */
	public void setType(Integer type) {
		this.type = type;
	}
	/**
	 * 获取：设备分类1.物业锁2.摄像头
	 */
	public Integer getType() {
		return type;
	}
	/**
	 * 设置：父级ID
	 */
	public void setPId(Long pId) {
		this.pId = pId;
	}
	/**
	 * 获取：父级ID
	 */
	public Long getPId() {
		return pId;
	}
	/**
	 * 设置：级别
	 */
	public void setLevel(Integer level) {
		this.level = level;
	}
	/**
	 * 获取：级别
	 */
	public Integer getLevel() {
		return level;
	}
	/**
	 * 设置：设备图片
	 */
	public void setEqImg(String eqImg) {
		this.eqImg = eqImg;
	}
	/**
	 * 获取：设备图片
	 */
	public String getEqImg() {
		return eqImg;
	}
	/**
	 * 设置：创建人
	 */
	public void setCreateBy(String createBy) {
		this.createBy = createBy;
	}
	/**
	 * 获取：创建人
	 */
	public String getCreateBy() {
		return createBy;
	}
	/**
	 * 设置：创建时间
	 */
	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}
	/**
	 * 获取：创建时间
	 */
	public Date getCreateTime() {
		return createTime;
	}
	/**
	 * 设置：修改人
	 */
	public void setUpdateBy(String updateBy) {
		this.updateBy = updateBy;
	}
	/**
	 * 获取：修改人
	 */
	public String getUpdateBy() {
		return updateBy;
	}
	/**
	 * 设置：修改时间
	 */
	public void setUpdateTime(Date updateTime) {
		this.updateTime = updateTime;
	}
	/**
	 * 获取：修改时间
	 */
	public Date getUpdateTime() {
		return updateTime;
	}


}
