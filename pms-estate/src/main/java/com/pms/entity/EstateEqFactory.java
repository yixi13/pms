package com.pms.entity;

import com.baomidou.mybatisplus.activerecord.Model;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.*;

import com.pms.util.Description;
import com.pms.entity.BaseModel;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;


/**
 * @author zyl
 * @email 517770986@qq.com
 * @date 2017-11-17 14:41:03
 */
@TableName("estate_eq_factory")
public class EstateEqFactory extends BaseModel<EstateEqFactory> {
    private static final long serialVersionUID = 1L;


    /**
     * 厂商id
     */
    @TableId("factory_id")
    @Description("厂商id")
    private Long factoryId;

    /**
     * 厂商名称
     */
    @TableField("factory_name")
    @Description("厂商名称")
    private String factoryName;

    /**
     * 厂商 服务url
     */
    @TableField("factory_service_url")
    @Description("厂商 服务url")
    private String factoryServiceUrl;
    /**
     * 厂商编号
     */
    @TableField("factory_code")
    @Description("厂商编号")
    private String factoryCode;

    /**
     * 创建时间
     */
    @TableField("create_time")
    @Description("创建时间")
    private Date createTime;

    /**
     * 厂家设备类型（1-锁，2-摄像头）
     */
    @TableField("factory_type")
    @Description("厂家设备类型（1-锁，2-摄像头）")
    private Integer factoryType;

    // ID赋值
    public EstateEqFactory() {
        this.factoryId = returnIdLong();
    }

    @Override
    protected Serializable pkVal() {
        return this.factoryId;
    }

    public String getFactoryCode() {
        return factoryCode;
    }

    public void setFactoryCode(String factoryCode) {
        this.factoryCode = factoryCode;
    }

    /**
     * 设置：厂商id
     */
    public EstateEqFactory setFactoryId(Long factoryId) {
        this.factoryId = factoryId;
        return this;
    }

    /**
     * 获取：厂商id
     */
    public Long getFactoryId() {
        return factoryId;
    }

    /**
     * 设置：厂商名称
     */
    public EstateEqFactory setFactoryName(String factoryName) {
        this.factoryName = factoryName;
        return this;
    }

    /**
     * 获取：厂商名称
     */
    public String getFactoryName() {
        return factoryName;
    }

    /**
     * 设置：厂商 服务url
     */
    public EstateEqFactory setFactoryServiceUrl(String factoryServiceUrl) {
        this.factoryServiceUrl = factoryServiceUrl;
        return this;
    }

    /**
     * 获取：厂商 服务url
     */
    public String getFactoryServiceUrl() {
        return factoryServiceUrl;
    }

    /**
     * 设置：创建时间
     */
    public EstateEqFactory setCreateTime(Date createTime) {
        this.createTime = createTime;
        return this;
    }

    /**
     * 获取：创建时间
     */
    public Date getCreateTime() {
        return createTime;
    }

    /**
     * 设置：厂家设备类型（1-锁，2-摄像头）
     */
    public EstateEqFactory setFactoryType(Integer factoryType) {
        this.factoryType = factoryType;
        return this;
    }

    /**
     * 获取：厂家设备类型（1-锁，2-摄像头）
     */
    public Integer getFactoryType() {
        return factoryType;
    }


}
