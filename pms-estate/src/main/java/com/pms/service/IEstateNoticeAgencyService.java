package com.pms.service;
import com.baomidou.mybatisplus.service.IService;
import com.pms.entity.EstateNoticeAgency;
/**
 * 
 *
 * @author zyl
 * @email 517770986@qq.com
 * @date 2017-11-03 09:48:34
 */

public interface IEstateNoticeAgencyService extends  IBaseService<EstateNoticeAgency> {

}