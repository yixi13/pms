package com.pms.service;

import com.pms.entity.OwnerMember;
import com.baomidou.mybatisplus.service.IService;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author ASUS_B
 * @since 2017-10-26
 */
public interface IOwnerMemberService extends IBaseService<OwnerMember> {
    /**
     * 查询所有物业人员 会员id
     * @param communityId
     * @return
     */
    public List<Long> readOwnerMemberIds(Long communityId);
	
}
