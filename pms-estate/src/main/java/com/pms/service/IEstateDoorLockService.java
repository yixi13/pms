package com.pms.service;
import com.baomidou.mybatisplus.service.IService;
import com.pms.entity.EstateDoorLock;
/**
 * 
 *
 * @author zyl
 * @email 517770986@qq.com
 * @date 2017-11-17 14:41:03
 */

public interface IEstateDoorLockService extends  IBaseService<EstateDoorLock> {

}