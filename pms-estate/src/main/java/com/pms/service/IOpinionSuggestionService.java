package com.pms.service;
import com.baomidou.mybatisplus.mapper.Wrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.service.IService;
import com.pms.entity.EquipmentRepair;
import com.pms.entity.OpinionSuggestion;

import java.util.Map;

/**
 * 
 *
 * @author zyl
 * @email 517770986@qq.com
 * @date 2017-11-02 14:10:56
 */

public interface IOpinionSuggestionService extends  IBaseService<OpinionSuggestion> {

    Page<Map<String, Object>> getOpinionSuggestionState(Page page, Wrapper<OpinionSuggestion> wrapper);
}