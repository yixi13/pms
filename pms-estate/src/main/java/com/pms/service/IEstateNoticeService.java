package com.pms.service;
import com.pms.entity.EstateNotice;

/**
 * 
 *
 * @author zyl
 * @email 517770986@qq.com
 * @date 2017-11-03 09:48:33
 */

public interface IEstateNoticeService extends  IBaseService<EstateNotice> {

    EstateNotice selectBynoticeId(Long id);
}