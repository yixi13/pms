package com.pms.service;
import com.baomidou.mybatisplus.service.IService;
import com.pms.entity.OpinionImg;
/**
 * 
 *
 * @author zyl
 * @email 517770986@qq.com
 * @date 2017-11-02 14:10:56
 */

public interface IOpinionImgService extends  IBaseService<OpinionImg> {

}