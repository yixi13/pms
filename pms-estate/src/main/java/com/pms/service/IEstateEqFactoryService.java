package com.pms.service;
import com.baomidou.mybatisplus.service.IService;
import com.pms.entity.EstateEqFactory;
/**
 * 
 *
 * @author zyl
 * @email 517770986@qq.com
 * @date 2017-11-17 14:41:03
 */

public interface IEstateEqFactoryService extends  IService<EstateEqFactory> {

}