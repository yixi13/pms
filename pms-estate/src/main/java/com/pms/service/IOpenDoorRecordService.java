package com.pms.service;
import com.baomidou.mybatisplus.service.IService;
import com.pms.entity.OpenDoorRecord;
import org.apache.ibatis.annotations.Param;

import java.util.Date;

/**
 * 
 *
 * @author zyl
 * @email 517770986@qq.com
 * @date 2017-12-12 10:42:50
 */

public interface IOpenDoorRecordService extends  IService<OpenDoorRecord> {

    /**
     * 判断该门是否存在记录
     * @param doorId
     * @return true 存在记录，false 不存在
     */
    public boolean judgeIsExistRecord(Long doorId);

    /**
     * 开门次数+1
     * @param doorId 门id
     */
    public int addOpenNumByDoorId(Long doorId, Date openDate);

    /**
     * 查询总开门次数
     * @param agencyId 物业机构id
     * @param conmunityId 社区id
     * @param doorId 门
     * @return 开门次数
     */
    public Long reanOpenDoorNum(Long agencyId,Long conmunityId,Long doorId);

    /**
     * 保存开门记录
     * @param eqSn 设备id
     * @param recordId 新增的id
     */
    public void saveOpenDoorNumBySqlFunction(@Param("eqSn")String eqSn, @Param("recordId")Long recordId);
}