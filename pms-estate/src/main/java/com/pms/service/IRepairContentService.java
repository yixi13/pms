package com.pms.service;
import com.baomidou.mybatisplus.service.IService;
import com.pms.entity.RepairContent;
/**
 * 
 *
 * @author zyl
 * @email 517770986@qq.com
 * @date 2017-11-01 14:33:01
 */

public interface IRepairContentService extends  IBaseService<RepairContent> {

}