package com.pms.service;
import com.baomidou.mybatisplus.service.IService;
import com.pms.entity.EstateLockCard;

import java.util.List;

/**
 * 
 *
 * @author zyl
 * @email 517770986@qq.com
 * @date 2017-11-27 17:37:54
 */

public interface IEstateLockCardService extends  IBaseService<EstateLockCard> {

    public List<EstateLockCard> selectListByEqSn(String eqId);
}