package com.pms.service;
import com.baomidou.mybatisplus.service.IService;
import com.pms.entity.EstateNoticeActivity;
/**
 * 
 *
 * @author zyl
 * @email 517770986@qq.com
 * @date 2017-11-03 09:48:34
 */

public interface IEstateNoticeActivityService extends  IBaseService<EstateNoticeActivity> {

}