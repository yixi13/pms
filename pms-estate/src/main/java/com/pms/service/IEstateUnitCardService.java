package com.pms.service;
import com.baomidou.mybatisplus.service.IService;
import com.pms.entity.EstateUnitCard;
/**
 * 
 *
 * @author zyl
 * @email 517770986@qq.com
 * @date 2017-11-27 17:37:54
 */

public interface IEstateUnitCardService extends  IBaseService<EstateUnitCard> {

}