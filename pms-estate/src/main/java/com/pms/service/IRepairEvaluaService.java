package com.pms.service;
import com.baomidou.mybatisplus.service.IService;
import com.pms.entity.RepairEvalua;
/**
 * 
 *
 * @author zyl
 * @email 517770986@qq.com
 * @date 2017-11-01 14:34:16
 */

public interface IRepairEvaluaService extends  IBaseService<RepairEvalua> {

}