package com.pms.service.impl;

import org.springframework.stereotype.Service;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.pms.entity.CameraUserInformation;
import com.pms.mapper.CameraUserInformationMapper;
import com.pms.service.ICameraUserInformationService;
/**
 * 
 *
 * @author zyl
 * @email 517770986@qq.com
 * @date 2017-11-15 11:33:11
 */
@Service
public class CameraUserInformationServiceImpl extends BaseServiceImpl<CameraUserInformationMapper,CameraUserInformation> implements ICameraUserInformationService {

}