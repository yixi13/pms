package com.pms.service.impl;

import com.pms.cache.utils.RedisCacheUtil;
import com.pms.entity.EntranceDoor;
import com.pms.mapper.EntranceDoorMapper;
import com.pms.service.IEntranceDoorService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author ASUS_B
 * @since 2017-10-24
 */
@Service
public class EntranceDoorServiceImpl extends BaseServiceImpl<EntranceDoorMapper, EntranceDoor> implements IEntranceDoorService {
    @Autowired
    RedisCacheUtil redisCacheUtil;
    /**
     * 修改关联表 门名称(门锁表，门卡片表)
     * @param doorId 门id
     */
    public void updateJoinTableDoorName(Long doorId){
        baseMapper.updateJoinTableDoorName(doorId);
        redisCacheUtil.clearOtherCacleByPre("estateDoorCard");
        redisCacheUtil.clearOtherCacleByPre("estateLockCard");
        redisCacheUtil.clearOtherCacleByPre("estateDoorLock");
    }
}
