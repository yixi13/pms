package com.pms.service.impl;

import com.pms.entity.HouseMasterInfo;
import com.pms.mapper.HouseMasterInfoMapper;
import com.pms.service.IHouseMasterInfoService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author ASUS_B
 * @since 2017-11-02
 */
@Service
public class HouseMasterInfoServiceImpl extends BaseServiceImpl<HouseMasterInfoMapper, HouseMasterInfo> implements IHouseMasterInfoService {
	
}
