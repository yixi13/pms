package com.pms.service.impl;

import org.springframework.stereotype.Service;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.pms.entity.RepairEvalua;
import com.pms.mapper.RepairEvaluaMapper;
import com.pms.service.IRepairEvaluaService;
/**
 * 
 *
 * @author zyl
 * @email 517770986@qq.com
 * @date 2017-11-01 14:34:16
 */
@Service
public class RepairEvaluaServiceImpl extends BaseServiceImpl<RepairEvaluaMapper,RepairEvalua> implements IRepairEvaluaService {

}