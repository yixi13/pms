package com.pms.service.impl;

import org.springframework.stereotype.Service;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.pms.entity.EstateNoticeAgency;
import com.pms.mapper.EstateNoticeAgencyMapper;
import com.pms.service.IEstateNoticeAgencyService;
/**
 * 
 *
 * @author zyl
 * @email 517770986@qq.com
 * @date 2017-11-03 09:48:34
 */
@Service
public class EstateNoticeAgencyServiceImpl extends BaseServiceImpl<EstateNoticeAgencyMapper,EstateNoticeAgency> implements IEstateNoticeAgencyService {

}