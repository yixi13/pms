package com.pms.service.impl;

import com.baomidou.mybatisplus.mapper.Wrapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.pms.entity.EstateEqClass;
import com.pms.mapper.EstateEqClassMapper;
import com.pms.service.IEstateEqClassService;

import java.util.List;

/**
 * 
 *
 * @author zyl
 * @email 517770986@qq.com
 * @date 2017-10-27 14:52:23
 */
@Service
public class EstateEqClassServiceImpl extends BaseServiceImpl<EstateEqClassMapper,EstateEqClass> implements IEstateEqClassService {

    public List<EstateEqClass> selectList(Wrapper<EstateEqClass> wrapper) {
        return baseMapper.selectListByAll();
    }

}