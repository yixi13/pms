package com.pms.service.impl;

import com.pms.entity.UnitFloor;
import com.pms.mapper.UnitFloorMapper;
import com.pms.service.IUnitFloorService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author ASUS_B
 * @since 2017-10-24
 */
@Service
public class UnitFloorServiceImpl extends BaseServiceImpl<UnitFloorMapper, UnitFloor> implements IUnitFloorService {
	
}
