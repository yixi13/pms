package com.pms.service.impl;

import com.pms.entity.RepairEvalua;
import com.pms.mapper.RepairEvaluaMapper;
import com.pms.service.IRepairEvaluaService;
import org.springframework.stereotype.Service;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.pms.entity.RepairImg;
import com.pms.mapper.RepairImgMapper;
import com.pms.service.IRepairImgService;
/**
 * 
 *
 * @author zyl
 * @email 517770986@qq.com
 * @date 2017-11-01 14:34:58
 */
@Service
public class RepairImgServiceImpl extends BaseServiceImpl<RepairImgMapper,RepairImg> implements IRepairImgService {

}