package com.pms.service.impl;

import org.springframework.stereotype.Service;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.pms.entity.EstateEqCamera;
import com.pms.mapper.EstateEqCameraMapper;
import com.pms.service.IEstateEqCameraService;
/**
 * 
 *
 * @author zyl
 * @email 517770986@qq.com
 * @date 2017-11-15 11:33:11
 */
@Service
public class EstateEqCameraServiceImpl extends BaseServiceImpl<EstateEqCameraMapper,EstateEqCamera> implements IEstateEqCameraService {

}