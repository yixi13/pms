package com.pms.service.impl;

import org.springframework.stereotype.Service;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.pms.entity.EstateNoticeActivity;
import com.pms.mapper.EstateNoticeActivityMapper;
import com.pms.service.IEstateNoticeActivityService;
/**
 * 
 *
 * @author zyl
 * @email 517770986@qq.com
 * @date 2017-11-03 09:48:34
 */
@Service
public class EstateNoticeActivityServiceImpl extends BaseServiceImpl<EstateNoticeActivityMapper,EstateNoticeActivity> implements IEstateNoticeActivityService {

}