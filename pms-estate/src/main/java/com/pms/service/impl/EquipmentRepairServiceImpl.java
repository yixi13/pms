package com.pms.service.impl;

import com.baomidou.mybatisplus.mapper.SqlHelper;
import com.baomidou.mybatisplus.mapper.Wrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.pms.cache.annotation.BaseCache;
import com.pms.cache.annotation.Cache;
import org.springframework.stereotype.Service;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.pms.entity.EquipmentRepair;
import com.pms.mapper.EquipmentRepairMapper;
import com.pms.service.IEquipmentRepairService;

import java.util.Map;

/**
 * 
 *
 * @author zyl
 * @email 517770986@qq.com
 * @date 2017-11-01 14:31:47
 */
@Service
public class EquipmentRepairServiceImpl extends BaseServiceImpl<EquipmentRepairMapper,EquipmentRepair> implements IEquipmentRepairService {


    @Cache(key = "equipmentRepair:getequipmentRepair{1}{2.paramNameValuePairs}")
    public Page<Map<String, Object>> getequipmentRepair(Page page, Wrapper<EquipmentRepair> wrapper) {
        SqlHelper.fillWrapper(page, wrapper);
        page.setRecords(baseMapper.getequipmentRepair(page, wrapper));
        return page;
    }

}