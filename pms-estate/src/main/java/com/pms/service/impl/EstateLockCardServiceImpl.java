package com.pms.service.impl;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.pms.cache.annotation.Cache;
import org.springframework.stereotype.Service;
import com.pms.entity.EstateLockCard;
import com.pms.mapper.EstateLockCardMapper;
import com.pms.service.IEstateLockCardService;

import java.sql.Wrapper;
import java.util.List;

/**
 * 
 *
 * @author zyl
 * @email 517770986@qq.com
 * @date 2017-11-27 17:37:54
 */
@Service
public class EstateLockCardServiceImpl extends BaseServiceImpl<EstateLockCardMapper,EstateLockCard> implements IEstateLockCardService {

    /**
     * 只查询前500条 待发卡片
     * @param eqId 设备id
     * @return
     */
    public List<EstateLockCard> selectListByEqSn(String eqId){
       return selectList(new EntityWrapper<EstateLockCard>().eq("eq_sn", eqId).orderBy("lock_card_id", true));
    }
}