package com.pms.service.impl;

import org.springframework.stereotype.Service;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.pms.entity.OpinionImg;
import com.pms.mapper.OpinionImgMapper;
import com.pms.service.IOpinionImgService;
/**
 * 
 *
 * @author zyl
 * @email 517770986@qq.com
 * @date 2017-11-02 14:10:56
 */
@Service
public class OpinionImgServiceImpl extends BaseServiceImpl<OpinionImgMapper,OpinionImg> implements IOpinionImgService {

}