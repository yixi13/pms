package com.pms.service.impl;

import com.pms.entity.ChargeValue;
import com.pms.mapper.ChargeValueMapper;
import com.pms.service.IChargeValueService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author ASUS_B
 * @since 2017-11-07
 */
@Service
public class ChargeValueServiceImpl extends ServiceImpl<ChargeValueMapper, ChargeValue> implements IChargeValueService {
	
}
