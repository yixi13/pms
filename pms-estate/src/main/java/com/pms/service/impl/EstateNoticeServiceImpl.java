package com.pms.service.impl;

import com.pms.entity.EstateNotice;
import com.pms.entity.EstateNoticeAgency;
import com.pms.mapper.EstateNoticeAgencyMapper;
import com.pms.mapper.EstateNoticeMapper;
import com.pms.service.IEstateNoticeAgencyService;
import com.pms.service.IEstateNoticeService;
import org.springframework.stereotype.Service;

/**
 * 
 *
 * @author zyl
 * @email 517770986@qq.com
 * @date 2017-11-03 09:48:34
 */
@Service
public class EstateNoticeServiceImpl extends BaseServiceImpl<EstateNoticeMapper,EstateNotice> implements IEstateNoticeService {

   public EstateNotice selectBynoticeId(Long id){
        return  baseMapper.selectBynoticeId(id);
    }
}