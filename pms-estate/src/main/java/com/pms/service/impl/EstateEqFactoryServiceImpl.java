package com.pms.service.impl;

import org.springframework.stereotype.Service;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.pms.entity.EstateEqFactory;
import com.pms.mapper.EstateEqFactoryMapper;
import com.pms.service.IEstateEqFactoryService;
/**
 * 
 *
 * @author zyl
 * @email 517770986@qq.com
 * @date 2017-11-17 14:41:03
 */
@Service
public class EstateEqFactoryServiceImpl extends BaseServiceImpl<EstateEqFactoryMapper,EstateEqFactory> implements IEstateEqFactoryService {

}