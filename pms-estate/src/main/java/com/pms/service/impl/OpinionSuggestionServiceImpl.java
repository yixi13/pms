package com.pms.service.impl;

import com.baomidou.mybatisplus.mapper.SqlHelper;
import com.baomidou.mybatisplus.mapper.Wrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.pms.cache.annotation.BaseCache;
import com.pms.cache.annotation.Cache;
import com.pms.entity.EquipmentRepair;
import org.springframework.stereotype.Service;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.pms.entity.OpinionSuggestion;
import com.pms.mapper.OpinionSuggestionMapper;
import com.pms.service.IOpinionSuggestionService;

import java.util.Map;

/**
 * 
 *
 * @author zyl
 * @email 517770986@qq.com
 * @date 2017-11-02 14:10:56
 */
@Service
public class OpinionSuggestionServiceImpl extends BaseServiceImpl<OpinionSuggestionMapper,OpinionSuggestion> implements IOpinionSuggestionService {


    @Cache(key ="opinionSuggestion:getOpinionSuggestionState{1}{2.paramNameValuePairs}")
    public Page<Map<String, Object>> getOpinionSuggestionState(Page page, Wrapper<OpinionSuggestion> wrapper) {
        SqlHelper.fillWrapper(page, wrapper);
        page.setRecords(baseMapper.getOpinionSuggestionState(page, wrapper));
        return page;
    }
}