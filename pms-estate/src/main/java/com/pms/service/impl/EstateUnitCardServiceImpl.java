package com.pms.service.impl;

import org.springframework.stereotype.Service;
import com.pms.entity.EstateUnitCard;
import com.pms.mapper.EstateUnitCardMapper;
import com.pms.service.IEstateUnitCardService;
/**
 * 
 *
 * @author zyl
 * @email 517770986@qq.com
 * @date 2017-11-27 17:37:54
 */
@Service
public class EstateUnitCardServiceImpl extends BaseServiceImpl<EstateUnitCardMapper,EstateUnitCard> implements IEstateUnitCardService {

}