package com.pms.service.impl;

import com.pms.cache.utils.RedisCacheUtil;
import com.pms.entity.UnitBuilding;
import com.pms.mapper.UnitBuildingMapper;
import com.pms.service.IUnitBuildingService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author ASUS_B
 * @since 2017-10-24
 */
@Service
public class UnitBuildingServiceImpl extends BaseServiceImpl<UnitBuildingMapper, UnitBuilding> implements IUnitBuildingService {
    @Autowired
    UnitBuildingMapper unitBuildingMapper;
    @Autowired
    RedisCacheUtil redisCacheUtil;
    /**
     * 查询小区最大的小区编号,用于创建单元时
     * @param communityId 小区id
     * @return 最大的小区编号
     */
   public int getMaxUnitNum(Long communityId){
        return unitBuildingMapper.getMaxUnitNum(communityId);
    }

    /**
     * 修改单元名称 同步修改 关联表单元名称
     * @param unitName 单元名称
     * @param communityId 社区id
     * @param unitId 单元id
     */
    public void updateJoinTableUnitName(String unitName,Long communityId,Long unitId){
        unitBuildingMapper.updateJoinTableUnitName(unitName,communityId,unitId);
        redisCacheUtil.clearOtherCacleByPre("unitHouse");
        redisCacheUtil.clearOtherCacleByPre("ownerMember");
        redisCacheUtil.clearOtherCacleByPre("houseMasterInfo");
        redisCacheUtil.clearOtherCacleByPre("entranceDoor");
    }

    /**
     * 修改 大豪卡片 的关联单元名称
     * @param unitId
     */
    public void updateCardJoinTableUnitName(Long unitId){
        unitBuildingMapper.updateCardJoinTableUnitName(unitId);
        redisCacheUtil.clearOtherCacleByPre("estateUnitCard");
        redisCacheUtil.clearOtherCacleByPre("estateDoorCard");
        redisCacheUtil.clearOtherCacleByPre("estateLockCard");
    }
}
