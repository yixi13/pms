package com.pms.service.impl;

import org.springframework.stereotype.Service;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.pms.entity.RepairContent;
import com.pms.mapper.RepairContentMapper;
import com.pms.service.IRepairContentService;
/**
 * 
 *
 * @author zyl
 * @email 517770986@qq.com
 * @date 2017-11-01 14:33:01
 */
@Service
public class RepairContentServiceImpl extends BaseServiceImpl<RepairContentMapper,RepairContent> implements IRepairContentService {

}