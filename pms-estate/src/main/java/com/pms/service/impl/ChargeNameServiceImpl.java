package com.pms.service.impl;

import com.pms.entity.ChargeName;
import com.pms.mapper.ChargeNameMapper;
import com.pms.service.IChargeNameService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author ASUS_B
 * @since 2017-11-07
 */
@Service
public class ChargeNameServiceImpl extends BaseServiceImpl<ChargeNameMapper, ChargeName> implements IChargeNameService {
	
}
