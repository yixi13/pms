package com.pms.service.impl;

import com.pms.cache.utils.RedisCacheUtil;
import com.pms.entity.EstateAgency;
import com.pms.mapper.EstateAgencyMapper;
import com.pms.service.IEstateAgencyService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Random;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author ASUS_B
 * @since 2017-10-26
 */
@Service
public class EstateAgencyServiceImpl extends BaseServiceImpl<EstateAgencyMapper, EstateAgency> implements IEstateAgencyService {
    @Autowired
    RedisCacheUtil redisCacheUtil;

    public int selectMaxCommunityNum(){
       Integer maxCommunityNumm = baseMapper.selectMaxCommunityNum();
       if(maxCommunityNumm==null){maxCommunityNumm=0;}
        return maxCommunityNumm+1;
    }

    /**
     * 获取16进制随机数
     * @param len 想要的16进制随机数的 长度
     * @return
     */
    public String randomHexString(int len)  {
        try {
            StringBuffer result = new StringBuffer();
            for(int i=0;i<len;i++) {
                result.append(Integer.toHexString(new Random().nextInt(16)));
            }
            return result.toString().toUpperCase();

        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;

    }

    /**
     * 修改关联表 社区名称
     */
    public void updateJoinTableCommunityName(String communityName,Long communityId){
        baseMapper.updateJoinTableCommunityName(communityName,communityId);
        redisCacheUtil.clearOtherCacleByPre("estateDoorLock");
        redisCacheUtil.clearOtherCacleByPre("ownerMember");
    }

    public void updateCardJoinTableCommunityName(Long communityId){
        baseMapper.updateCardJoinTableCommunityName(communityId);
        redisCacheUtil.clearOtherCacleByPre("estateUnitCard");
        redisCacheUtil.clearOtherCacleByPre("estateDoorCard");
        redisCacheUtil.clearOtherCacleByPre("estateLockCard");
    }
}
