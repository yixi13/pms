package com.pms.service.impl;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.mapper.SqlHelper;
import com.baomidou.mybatisplus.mapper.Wrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.pms.cache.annotation.Cache;
import com.pms.cache.annotation.CacheClear;
import com.pms.entity.EstateUnitCard;
import com.pms.util.DateUtil;
import org.springframework.stereotype.Service;
import com.pms.entity.EstateDoorCard;
import com.pms.mapper.EstateDoorCardMapper;
import com.pms.service.IEstateDoorCardService;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * 
 *
 * @author zyl
 * @email 517770986@qq.com
 * @date 2017-11-27 17:37:54
 */
@Service
public class EstateDoorCardServiceImpl extends BaseServiceImpl<EstateDoorCardMapper,EstateDoorCard> implements IEstateDoorCardService {

    /**
     * 查询 未发的蓝牙卡片
     * @param page
     * @param wp
     * @return
     */
    @Cache(key="estateDoorCard:selectWillUseCardPage{1}{2}{3.paramNameValuePairs}")
    public Page<EstateUnitCard> selectWillUseCardPage(String leftJoinWhere,Page<EstateUnitCard> page, Wrapper<EstateUnitCard> wp){
        SqlHelper.fillWrapper(page, wp);
        page.setRecords(baseMapper.selectWillUseCardList(page, wp,leftJoinWhere));
        return page;
    }
    @Cache(key="estateDoorCard:selectWillUseCardList{1}{2.paramNameValuePairs}")
    public List<EstateUnitCard> selectWillUseCardList(String leftJoinWhere,Wrapper<EstateUnitCard> wp){
        return baseMapper.selectWillUseCardList(wp,leftJoinWhere);
    }
    /**
     * 删除  有效期 过时的卡片
     */
    @CacheClear(pre="estateDoorCard")
    @Transactional
    public void delOverValidTimeCard(){
        Date validTime = DateUtil.addDate(new Date(),5,1);//当前时间加1天
        baseMapper.delete(new EntityWrapper<EstateDoorCard>().lt("valid_time",validTime));
        clearPreKeyCache("estateUnitCard");
    }

    /**
     * 保存发卡记录
     * @param eqSn 设备id
     * @param cardSixteenNum 卡号字符串
     */
    @Transactional
    public void saveSendCardRecordByFunction(String eqSn, String cardSixteenNum,Long lockCardIdStart){
        baseMapper.saveSendCardRecordByFunction(eqSn,cardSixteenNum,lockCardIdStart);
        clearPreKeyCache("estateDoorCard"); clearPreKeyCache("estateUnitCard");
        clearPreKeyCache("estateLockCard");

    }
}