package com.pms.service.impl;

import com.pms.cache.annotation.Cache;
import org.springframework.stereotype.Service;
import com.pms.entity.OwnerMember;
import com.pms.mapper.OwnerMemberMapper;
import com.pms.service.IOwnerMemberService;

import java.util.List;

/**
 * 
 *
 * @author zyl
 * @email 517770986@qq.com
 * @date 2017-11-22 14:32:37
 */
@Service
public class OwnerMemberServiceImpl extends BaseServiceImpl<OwnerMemberMapper,OwnerMember> implements IOwnerMemberService {
    /**
     * 查询所有物业人员 会员id
     * @param communityId
     * @return
     */
    @Cache(key="ownerMember:readOwnerMemberIds{1}")
    public List<Long> readOwnerMemberIds(Long communityId){
        return baseMapper.readOwnerMemberIds(communityId);
    }
}