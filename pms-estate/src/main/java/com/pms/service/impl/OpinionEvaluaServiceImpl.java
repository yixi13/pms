package com.pms.service.impl;

import org.springframework.stereotype.Service;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.pms.entity.OpinionEvalua;
import com.pms.mapper.OpinionEvaluaMapper;
import com.pms.service.IOpinionEvaluaService;
/**
 * 
 *
 * @author zyl
 * @email 517770986@qq.com
 * @date 2017-11-02 14:10:56
 */
@Service
public class OpinionEvaluaServiceImpl extends BaseServiceImpl<OpinionEvaluaMapper,OpinionEvalua> implements IOpinionEvaluaService {

}