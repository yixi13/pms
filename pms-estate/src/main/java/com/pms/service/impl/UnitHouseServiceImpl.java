package com.pms.service.impl;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.pms.cache.annotation.Cache;
import com.pms.entity.HouseMasterInfo;
import com.pms.entity.UnitHouse;
import com.pms.mapper.UnitHouseMapper;
import com.pms.service.IUnitHouseService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author ASUS_B
 * @since 2017-10-24
 */
@Service
public class UnitHouseServiceImpl extends BaseServiceImpl<UnitHouseMapper, UnitHouse> implements IUnitHouseService {

    /**
     * excel导入户主信息，同步修改 户室有无户主状态 及户室面积
     */
    public void updateIsHaveMasterAndHouseProportion(Long communityId){
        baseMapper.updateIsHaveMasterAndHouseProportion(communityId);
    }

    public int countNotHaveChargeTemplateHouseNum(Long communityId){
        return selectCount(new EntityWrapper<UnitHouse>().eq("community_id",communityId).eq("is_hava_charge_template",2).eq("is_estate_house",2));
    }
    public int countNotHaveProportionHouseNum(Long communityId){
        return selectCount(new EntityWrapper<UnitHouse>().eq("community_id",communityId).eq("is_have_master",2).eq("is_estate_house",2));
    }
    /**
     * Api 接口查询 单元楼层户室
     * @param paramMap (communityId-社区id,unitId-单元id)
     * @return Map
     */
    @Cache(key="unitHouse:selectUnitFloorHouse{1}")
    public  List<Map<String,Object>> selectUnitFloorHouse(Map<String,Object> paramMap){
        return baseMapper.selectUnitFloorHouse(paramMap);
    }
    /**
     * Api 查询物业办公室
     * @param communityId 社区id
     * @return Map
     */
    @Cache(key="unitHouse:readEstateHouse{1}")
    public Map<String,Object> readEstateHouse(Long communityId){
        return baseMapper.readEstateHouse(communityId);
    }
}
