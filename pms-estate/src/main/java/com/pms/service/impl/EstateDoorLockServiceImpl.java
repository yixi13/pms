package com.pms.service.impl;

import org.springframework.stereotype.Service;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.pms.entity.EstateDoorLock;
import com.pms.mapper.EstateDoorLockMapper;
import com.pms.service.IEstateDoorLockService;
/**
 * 
 *
 * @author zyl
 * @email 517770986@qq.com
 * @date 2017-11-17 14:41:03
 */
@Service
public class EstateDoorLockServiceImpl extends BaseServiceImpl<EstateDoorLockMapper,EstateDoorLock> implements IEstateDoorLockService {

}