package com.pms.service.impl;

import com.alipay.api.AlipayApiException;
import com.alipay.api.AlipayClient;
import com.alipay.api.DefaultAlipayClient;
import com.alipay.api.request.AlipayEcoCplifeCommunityCreateRequest;
import com.alipay.api.response.AlipayEcoCplifeCommunityCreateResponse;
import com.pms.service.IEstateAgencyService;
import com.pms.service.IEstateAliService;
import com.pms.util.AliConfig;
import org.springframework.stereotype.Service;

/**
 * Created by ljb on 2018/1/3.
 */
@Service
public class EstateAliServiceImpl implements IEstateAliService {

    @Override
    public void communityCreate(String communityName, String communityAddress, String districtCode, String cityCode,
                                String provinceCode, String communityLocations, String associatedPois, String hotline) {
        AlipayClient alipayClient = new DefaultAlipayClient("https://openapi.alipay.com/gateway.do",
                AliConfig.app_id,AliConfig.private_key,"json",AliConfig.input_charset,
                AliConfig.ali_public_key,AliConfig.sign_type);
        AlipayEcoCplifeCommunityCreateRequest request = new AlipayEcoCplifeCommunityCreateRequest();
        request.setBizContent("{" +
                "\"community_name\":\"" + communityName +"\"," +
                "\"community_address\":\""+ communityAddress +"\"," +
                "\"district_code\":\""+ districtCode +"\"," +
                "\"city_code\":\""+ cityCode +"\"," +
                "\"province_code\":\""+ provinceCode +"\"," +
                "      \"community_locations\":[" +
                "        \"114.032395|22.519725\",\"114.032469|22.519336\"" +
                "      ]," +
                "      \"associated_pois\":[" +
                "        \"B02F37VVFP\",\"B0FFFQB4Y4\"" +
                "      ]," +
                "\"hotline\":\""+ hotline +"\"," +
                "\"out_community_id\":\"12345\"" +
                "  }");
        AlipayEcoCplifeCommunityCreateResponse response = null;
        try {
            response = alipayClient.execute(request);
        } catch (AlipayApiException e) {
            e.printStackTrace();
        }
        if(response.isSuccess()){
            System.out.println("调用成功");
        } else {
            System.out.println("调用失败");
        }
    }

}
