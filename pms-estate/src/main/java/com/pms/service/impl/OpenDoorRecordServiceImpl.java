package com.pms.service.impl;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.pms.cache.annotation.Cache;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Service;
import com.pms.entity.OpenDoorRecord;
import com.pms.mapper.OpenDoorRecordMapper;
import com.pms.service.IOpenDoorRecordService;

import java.util.Date;

/**
 * 
 *
 * @author zyl
 * @email 517770986@qq.com
 * @date 2017-12-12 10:42:50
 */
@Service
public class OpenDoorRecordServiceImpl extends ServiceImpl<OpenDoorRecordMapper,OpenDoorRecord> implements IOpenDoorRecordService {

    public boolean judgeIsExistRecord(Long doorId){
        int count = selectCount(new EntityWrapper<OpenDoorRecord>().eq("door_id",doorId));
        if(count>0){
            return true;
        }
        return false;
    }

    /**
     * 开门次数+1
     * @param doorId 门id
     */
    public int addOpenNumByDoorId(Long doorId, Date openDate){
        return baseMapper.addOpenNumByDoorId(doorId,openDate);
    }

    /**
     * 查询总开门次数
     * @param agencyId 物业机构id
     * @param conmunityId 社区id
     * @param doorId 门
     * @return 开门次数
     */
    public Long reanOpenDoorNum(Long agencyId,Long conmunityId,Long doorId){
        return baseMapper.reanOpenDoorNum(agencyId,conmunityId,doorId);
    }

    /**
     * 保存开门记录
     * @param eqSn 设备id
     * @param recordId 新增的id
     */
    public void saveOpenDoorNumBySqlFunction(@Param("eqSn")String eqSn, @Param("recordId")Long recordId){
        baseMapper.saveOpenDoorNumBySqlFunction(eqSn,recordId);
    }
}