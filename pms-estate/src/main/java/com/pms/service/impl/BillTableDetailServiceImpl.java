package com.pms.service.impl;

import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.pms.entity.BillTableDetail;
import com.pms.mapper.BillTableDetailMapper;
import com.pms.service.IBillTableDetailService;
import org.springframework.stereotype.Service;

/**
 * 
 *
 * @author zyl
 * @email 517770986@qq.com
 * @date 2017-11-01 11:52:52
 */
@Service
public class BillTableDetailServiceImpl extends ServiceImpl<BillTableDetailMapper,BillTableDetail> implements IBillTableDetailService {

}