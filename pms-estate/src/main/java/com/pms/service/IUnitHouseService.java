package com.pms.service;

import com.pms.entity.HouseMasterInfo;
import com.pms.entity.UnitHouse;
import com.baomidou.mybatisplus.service.IService;

import java.util.List;
import java.util.Map;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author ASUS_B
 * @since 2017-10-24
 */
public interface IUnitHouseService extends IBaseService<UnitHouse> {
    /**
     * excel导入户主信息，同步修改 户室有无户主状态 及户室面积
     */
    public void updateIsHaveMasterAndHouseProportion(Long communityId);

    /**
     * 查询小区 未配置 物业费模板的户室数量
     * @param communityId
     * @return
     */
    public int countNotHaveChargeTemplateHouseNum(Long communityId);

    /**
     * 查询小区 未导入户主信息 的户室数量
     * @param communityId
     * @return
     */
    public int countNotHaveProportionHouseNum(Long communityId);

    /**
     * Api 接口查询 单元楼层户室
     * @param paramMap (communityId-社区id,unitId-单元id)
     * @return Map
     */
    public  List<Map<String,Object>> selectUnitFloorHouse(Map<String,Object> paramMap);
    /**
     * Api 查询物业办公室
     * @param communityId 社区id
     * @return Map
     */
    public Map<String,Object> readEstateHouse(Long communityId);
}
