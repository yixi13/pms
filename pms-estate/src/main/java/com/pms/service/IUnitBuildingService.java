package com.pms.service;

        import com.pms.entity.UnitBuilding;
        import com.baomidou.mybatisplus.service.IService;
        import org.apache.ibatis.annotations.Param;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author ASUS_B
 * @since 2017-10-24
 */
public interface IUnitBuildingService extends IBaseService<UnitBuilding> {

    /**
     * 查询小区最大的小区编号,用于创建单元时
     * @param communityId 小区id
     * @return 最大的小区编号
     */
    public int getMaxUnitNum(Long communityId);

    /**
     * 修改单元名称 同步修改 关联表单元名称
     *  户室，户主信息，业主认证，门信息 表
     * @param unitName 单元名称
     * @param communityId 社区id
     * @param unitId 单元id
     */
    public void updateJoinTableUnitName(String unitName,Long communityId,Long unitId);

    /**
     * 修改 大豪卡片 的关联单元名称
     * @param unitId
     */
    public void updateCardJoinTableUnitName(Long unitId);
}
