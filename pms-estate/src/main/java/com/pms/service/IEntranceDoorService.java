package com.pms.service;

import com.pms.entity.EntranceDoor;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author ASUS_B
 * @since 2017-10-24
 */
public interface IEntranceDoorService extends IBaseService<EntranceDoor> {
    /**
     * 修改关联表 门名称(门锁表，门卡片表)
     * @param doorId 门id
     */
    public void updateJoinTableDoorName(Long doorId);
}
