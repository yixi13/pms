package com.pms.service;
import com.baomidou.mybatisplus.service.IService;
import com.pms.entity.EstateEqCamera;
/**
 * 
 *
 * @author zyl
 * @email 517770986@qq.com
 * @date 2017-11-15 11:33:11
 */

public interface IEstateEqCameraService extends  IBaseService<EstateEqCamera> {

}