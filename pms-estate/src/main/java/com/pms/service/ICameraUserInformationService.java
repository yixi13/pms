package com.pms.service;
import com.baomidou.mybatisplus.service.IService;
import com.pms.entity.CameraUserInformation;
/**
 * 
 *
 * @author zyl
 * @email 517770986@qq.com
 * @date 2017-11-15 11:33:11
 */

public interface ICameraUserInformationService extends  IBaseService<CameraUserInformation> {

}