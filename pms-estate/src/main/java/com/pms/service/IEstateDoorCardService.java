package com.pms.service;
import com.baomidou.mybatisplus.mapper.Wrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.service.IService;
import com.pms.cache.annotation.Cache;
import com.pms.cache.annotation.CacheClear;
import com.pms.entity.EstateDoorCard;
import com.pms.entity.EstateUnitCard;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;


/**
 * 
 *
 * @author zyl
 * @email 517770986@qq.com
 * @date 2017-11-27 17:37:54
 */

public interface IEstateDoorCardService extends  IBaseService<EstateDoorCard> {
    /**
     * 查询 未发的蓝牙卡片
     * @param page
     * @param wp
     * @return
     */
    public Page<EstateUnitCard> selectWillUseCardPage(String leftJoinWhere,Page<EstateUnitCard> page, Wrapper<EstateUnitCard> wp);

    public List<EstateUnitCard> selectWillUseCardList(String leftJoinWhere,Wrapper<EstateUnitCard> wp);
    /**
     * 删除  有效期 过时的卡片
     */
    public void delOverValidTimeCard();

    /**
     * 保存发卡记录
     * @param eqSn 设备id
     * @param cardSixteenNum 卡号字符串
     */
    public void saveSendCardRecordByFunction(String eqSn, String cardSixteenNum,Long lockCardIdStart);
}