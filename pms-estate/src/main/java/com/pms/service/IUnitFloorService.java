package com.pms.service;

import com.pms.entity.UnitFloor;
import com.baomidou.mybatisplus.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author ASUS_B
 * @since 2017-10-24
 */
public interface IUnitFloorService extends IBaseService<UnitFloor> {
	
}
