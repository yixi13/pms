package com.pms.service;

import com.pms.entity.EstateAgency;
import com.baomidou.mybatisplus.service.IService;
import org.apache.ibatis.annotations.Param;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author ASUS_B
 * @since 2017-10-26
 */
public interface IEstateAgencyService extends IBaseService<EstateAgency> {
    /**
     * 获取小区编号
     * @return
     */
    int selectMaxCommunityNum();

    /**
     * 获取16进制随机数
     */
    public String randomHexString(int len);
    /**
     * 修改关联表 社区名称
     */
     public void updateJoinTableCommunityName(String communityName,Long communityId);

    /**
     * 修改 大豪卡片 的关联社区名称
     * @param communityId
     */
    public void updateCardJoinTableCommunityName(Long communityId);
}
