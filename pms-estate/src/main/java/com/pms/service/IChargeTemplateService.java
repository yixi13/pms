package com.pms.service;

import com.pms.entity.ChargeTemplate;
import com.baomidou.mybatisplus.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author ASUS_B
 * @since 2017-11-07
 */
public interface IChargeTemplateService extends IBaseService<ChargeTemplate> {

   public void updateChargeTemplate(ChargeTemplate template);

    public void addChargeTemplate(ChargeTemplate template);

    public ChargeTemplate selectChargeTemplateInfo(Long id);
}
