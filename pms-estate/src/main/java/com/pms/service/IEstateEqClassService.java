package com.pms.service;
import com.baomidou.mybatisplus.service.IService;
import com.pms.entity.EstateEqClass;
/**
 * 
 *
 * @author zyl
 * @email 517770986@qq.com
 * @date 2017-10-27 14:52:23
 */

public interface IEstateEqClassService extends  IBaseService<EstateEqClass> {

}