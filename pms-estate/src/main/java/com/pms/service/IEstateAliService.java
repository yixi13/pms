package com.pms.service;

/**
 * Created by ljb on 2018/1/3.
 */
public interface IEstateAliService {

    /**
     *
     * @param communityName         社区名字
     * @param communityAddress      社区地址
     * @param districtCode          地区code
     * @param cityCode               城市code
     * @param provinceCode          省code
     * @param communityLocations    经纬度(114.032395|22.519725,114.032469|22.519336 )
     * @param associatedPois        POI ID  选填
     * @param hotline                热线或联系电话
     */
    public void communityCreate(String communityName,String communityAddress,
                                String districtCode,String cityCode,String provinceCode,
                                String communityLocations,String associatedPois,String hotline );
}
