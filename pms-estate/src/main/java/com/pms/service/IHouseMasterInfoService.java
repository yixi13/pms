package com.pms.service;

import com.pms.entity.HouseMasterInfo;
import com.baomidou.mybatisplus.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author ASUS_B
 * @since 2017-11-02
 */
public interface IHouseMasterInfoService extends IBaseService<HouseMasterInfo> {
	
}
