package com.pms.service;

import com.pms.entity.ChargeValue;
import com.baomidou.mybatisplus.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author ASUS_B
 * @since 2017-11-07
 */
public interface IChargeValueService extends IService<ChargeValue> {
	
}
