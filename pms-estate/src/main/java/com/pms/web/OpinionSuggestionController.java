package com.pms.web;
import com.pms.entity.*;
import com.pms.exception.R;
import com.pms.service.IEstateAgencyService;
import com.pms.service.IOpinionEvaluaService;
import com.pms.service.IOpinionImgService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.plugins.Page;
import org.springframework.web.bind.annotation.*;
import com.pms.controller.BaseController;
import com.pms.service.IOpinionSuggestionService;
import com.pms.result.ObjectRestResponse;
import com.pms.result.TableResultResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("opinionSuggestion")
@Api(value = "物业投诉后台接口",description = "物业投诉后台接口")
public class OpinionSuggestionController extends BaseController {
    Logger logger = LoggerFactory.getLogger(getClass());
    @Autowired
    IOpinionSuggestionService opinionSuggestionService;
    @Autowired
    IOpinionImgService opinionImgService;
    @Autowired
    IOpinionEvaluaService opinionEvaluaService;
    @Autowired
    IEstateAgencyService estateAgencyService;
    /**
     *分页查询物业投诉
     * @param limit
     * @param offset
     * @return
     */
    @RequestMapping(value = "/page",method = RequestMethod.POST)
    public R page(@RequestParam(defaultValue = "10") int limit, @RequestParam(defaultValue = "1")int offset){
        Page<OpinionSuggestion> page = new Page<OpinionSuggestion>(offset,limit);
        page = opinionSuggestionService.selectPage(page,new EntityWrapper<OpinionSuggestion>());
        return R.ok().put("data",page).putDescription(OpinionSuggestion.class);
    }

    /**
     * 删除物业投诉
     * @param opinionId
     * @return
     */
    @ApiOperation(value = "删除物业投诉")
    @RequestMapping(value = "/delete/{id}",method = RequestMethod.POST)
    @ApiImplicitParams({
            @ApiImplicitParam(name="opinionId",value="物业投诉ID",required = true,dataType = "Long",paramType="form"),
    })
    public R deleteById(Long opinionId){
        parameterIsNull(opinionId,"物业投诉ID不能为空");
        OpinionSuggestion opinionSuggestion=opinionSuggestionService.selectById(opinionId);
        parameterIsNull(opinionSuggestion,"该物业投诉信息不存在");
            opinionSuggestionService.deleteById (opinionId);
            opinionImgService.delete(new EntityWrapper<OpinionImg>().eq("opinion_id",opinionSuggestion.getOpinionId()));
            opinionEvaluaService.delete(new EntityWrapper<OpinionEvalua>().eq("opinion_id",opinionSuggestion.getOpinionId()));
            return R.ok();
     }

    /**
     * 查询投诉建议回复内容
     * @param opinionId
     * @return
     */
    @ApiOperation(value ="查询投诉建议回复内容")
    @RequestMapping(value = "/get/repairEvaluaRecord",method = RequestMethod.POST)
    @ApiImplicitParams({
            @ApiImplicitParam(name="opinionId",value="物业投诉ID",required = true,dataType = "Long",paramType="form"),
    })
    public R getRepairEvaluaRecord(Long opinionId){
        parameterIsNull(opinionId,"物业投诉ID不能为空");
        List<OpinionEvalua> opinionSuggestion=opinionEvaluaService.selectList(new EntityWrapper<OpinionEvalua>().eq("opinion_id",opinionId));
        return R.ok().put("data",opinionSuggestion).putDescription(OpinionEvalua.class);
    }


    /**
     *状态查询物业投诉建议状态内容
     * @param size
     * @param page
     * @param agencyId
     * @param state
     * @return
     */
    @ApiOperation(value ="状态查询物业投诉建议状态内容")
    @RequestMapping(value = "/get/getOpinionSuggestionState",method = RequestMethod.POST)
    @ApiImplicitParams({
            @ApiImplicitParam(name="state",value="状态1.已回复2.待回复",required = true,dataType = "Integer",paramType="form"),
            @ApiImplicitParam(name="agencyId",value="机构ID",required = true,dataType = "Long",paramType="form"),
            @ApiImplicitParam(name = "size", value = "条数（默认为10）", required = false, dataType = "Integer",paramType = "form"),
            @ApiImplicitParam(name = "page", value = "页数（默认为1）", required = false, dataType = "Integer",paramType = "form"),
    })
    public R getOpinionSuggestionState(Integer size,Integer page,Long agencyId,Integer state){
        parameterIsNull(agencyId,"机构ID不能为空");
        parameterIsNull(state,"状态不能为空");
        EstateAgency sysAgency=estateAgencyService.selectById(agencyId);
        parameterIsNull(sysAgency,"该机构信息不存在");
        if (null == size || size < 1){ size=10;}
        if (null== page ||  page<1 ){page=1;}
        Page<OpinionSuggestion> pages = new Page<OpinionSuggestion>(page,size);
        Page<Map<String, Object>> eq= new Page<Map<String, Object>>();
        eq=opinionSuggestionService.getOpinionSuggestionState(pages,new EntityWrapper<OpinionSuggestion>().eq("os.state",state));
        return R.ok().put("data",eq).putDescription(OpinionSuggestion.class).putDescription(OpinionEvalua.class).putDescription(OpinionImg.class);
    }


}