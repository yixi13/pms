package com.pms.web;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.mapper.Wrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.pms.controller.BaseController;
import com.pms.entity.EstateAgency;
import com.pms.entity.OwnerMember;
import com.pms.entity.UnitHouse;
import com.pms.exception.R;
import com.pms.rpc.IMemberService;
import com.pms.service.IEstateAgencyService;
import com.pms.service.IOwnerMemberService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author ASUS_B
 * @since 2017-10-26
 */
@RestController
@RequestMapping("/ownerMemberBase")
@Api(value="物业人员管理界面接口",description = "物业人员管理界面接口")
public class OwnerMemberController extends BaseController {
    @Autowired
    private IMemberService memberService;
    @Autowired
    private IOwnerMemberService ownerMemberService;
    @Autowired
    private IEstateAgencyService agencyService;

    @ApiOperation(value = "分页 查询物业人员")
    @RequestMapping(value = "/readePage",method = RequestMethod.POST)
    @ApiImplicitParams({
            @ApiImplicitParam(name="pageSize",value="请求条数",required = false,dataType = "int",paramType="form"),
            @ApiImplicitParam(name="pageNum",value="请求页数",required = false,dataType = "int",paramType="form"),
            @ApiImplicitParam(name="agencyId",value="物业机构id",required = true,dataType = "long",paramType="form"),
            @ApiImplicitParam(name="communityId",value="社区id",required = false,dataType = "long",paramType="form"),
            @ApiImplicitParam(name="unitId",value="单元id",required = false,dataType = "long",paramType="form"),
            @ApiImplicitParam(name="floorId",value="楼层id",required = false,dataType = "long",paramType="form"),
            @ApiImplicitParam(name="houseId",value="户室id",required = false,dataType = "long",paramType="form"),
            @ApiImplicitParam(name="type",value="类型(6-所有,1-业主,2-申请管理员(默认),3-管理员,5-物业人员,4-家庭成员)",required = false,dataType = "int",paramType="form"),
    })
    public R readePage(Integer pageSize,Integer pageNum,Long agencyId,Long communityId,Integer type
            ,Long unitId,Long floorId,Long houseId){
        parameterIsNull(agencyId,"物业机构id不能为空");
        if(null!=communityId){
            EstateAgency community = agencyService.selectById(communityId);
            parameterIsNull(community,"未查询到社区信息");
            if(!agencyId.equals(community.getParentId())||community.getAgencyType()!=2){
                return R.error(400,"社区信息错误");
            }
        }
        if(null==communityId){
            EstateAgency agency = agencyService.selectById(agencyId);
            parameterIsNull(agency,"未查询到物业信息");
            if(agency.getAgencyType()!=1){
                return R.error(400,"物业信息错误");
            }
        }
        if(type==null||type<1||type>6){
            type = 2;//默认查 申请管理员
        }
        if(null==pageNum||pageNum<1){pageNum=1;}
        if(null==pageSize||pageSize<1){pageSize=10;}
        Page<OwnerMember> page = new Page<OwnerMember>(pageNum,pageSize);
        Wrapper<OwnerMember> omWp = new EntityWrapper<OwnerMember>();
        omWp.eq("agency_id",agencyId);
        if(null!=communityId){
            omWp.eq("community_id",communityId);
        }
        if(type!=6){
            omWp.eq("type_",type);
        }
        if(type==1||type==4||type==6){
            if(null!=unitId){
                omWp.eq("unit_id",unitId);
            }
            if(null!=floorId){
                omWp.eq("floor_id",floorId);
            }
            if(null!=houseId){
                omWp.eq("house_id",houseId);
            }
        }
        Page<OwnerMember> returnPage = ownerMemberService.selectPage(page,omWp);
        return R.ok().put("data",returnPage).putMpPageDescription().putDescription(OwnerMember.class);
    }
    @ApiOperation(value = "审核 管理员申请")
    @RequestMapping(value = "/checkApply",method = RequestMethod.POST)
    @ApiImplicitParams({
            @ApiImplicitParam(name="ownerId",value="业主认证id",required = true,dataType = "long",paramType="form"),
            @ApiImplicitParam(name="checkResult",value="审核结果(1-通过,2-拒绝)",required = true,dataType = "int",paramType="form"),
    })
    public R checkApply(Long ownerId,Integer checkResult){
        parameterIsNull(checkResult,"请告知审核结果");
        parameterIsNull(ownerId,"请选择审核对象");
        OwnerMember om = ownerMemberService.selectById(ownerId);
        parameterIsNull(om,"未查询到相关信息");
        if(om.getType()!=2){
            return R.error(400,"你没有权限");
        }
        if(checkResult>2||checkResult<1){
            return R.error(400,"请告知审核结果");
        }
        if(checkResult==1){
            om.setType(3);
            ownerMemberService.updateById(om);
        }
        if(checkResult==2){
            ownerMemberService.deleteById(ownerId);
        }
        return R.ok();
    }

    @ApiOperation(value = "删除户室认证，同时删除家庭成员(谨慎(不推荐)使用)")
    @RequestMapping(value = "/delHouseOwner",method = RequestMethod.POST)
    @ApiImplicitParams({
            @ApiImplicitParam(name="ownerId",value="业主认证id",required = true,dataType = "long",paramType="form"),
    })
    public R delHouseOwner(Long ownerId){
        parameterIsNull(ownerId,"请选择删除对象");
        OwnerMember om = ownerMemberService.selectById(ownerId);
        parameterIsNull(om,"未查询到相关信息");
        if(om.getType()==2){
            return R.error(400,"无效操作");
        }
        if(om.getType()==1||om.getType()==3){//业主和物业管理员 同步删除 家庭成员
            ownerMemberService.delete(new EntityWrapper<OwnerMember>().eq("house_id",om.getHouseId()).eq("referrer_id",om.getMemberId()));
        }
        ownerMemberService.deleteById(ownerId);
        return R.ok();
    }
}
