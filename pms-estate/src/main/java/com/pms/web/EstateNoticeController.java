package com.pms.web;
import com.alibaba.druid.util.StringUtils;
import com.pms.entity.EstateNoticeAgency;
import com.pms.exception.R;
import com.pms.service.IEstateAgencyService;
import com.pms.service.IEstateNoticeActivityService;
import com.pms.service.IEstateNoticeAgencyService;
import com.pms.service.IEstateNoticeService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.plugins.Page;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import com.pms.controller.BaseController;
import com.pms.entity.EstateNotice;
import org.springframework.web.bind.annotation.RequestMapping;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

@RestController
@RequestMapping("estateNotice")
@Api(value = "物业公告信息表",description = "物业公告信息表")
public class EstateNoticeController extends BaseController {
    Logger logger = LoggerFactory.getLogger(getClass());
    @Autowired
    IEstateNoticeService estateNoticeService;
    @Autowired
    IEstateNoticeAgencyService estateNoticeAgencyService;
    @Autowired
    IEstateNoticeActivityService estateNoticeActivityService;
    @Autowired
    IEstateAgencyService estateAgencyService;

    /**
     * 分页查询物业公告
     * @param size
     * @param page
     * @param name
     * @return
     */
    @ApiOperation("分页查询物业公告")
    @RequestMapping(value = "/page",method = RequestMethod.POST)
    @ApiImplicitParams({
            @ApiImplicitParam(name = "size", value = "条数（默认为10）", required = false, dataType = "Integer",paramType = "form"),
            @ApiImplicitParam(name = "page", value = "页数（默认为1）", required = false, dataType = "Integer",paramType = "form"),
            @ApiImplicitParam(name = "name", value = "公告名称", required = false, dataType = "String",paramType = "form")
    })
    public R page(Integer size, Integer page, String name){
        if (null == size || size < 1){ size=10;}
        if (null== page ||  page<1 ){page=1;}
        Page<EstateNotice> pages = new Page<EstateNotice>(page,size);
        pages = estateNoticeService.selectPage(pages,new EntityWrapper<EstateNotice>().like("name_",name));
        return R.ok().put("data",pages).putDescription(EstateNotice.class);
    }

    /**
     * 添加物业公告
     * @param type
     * @param isTop
     * @param name
     * @param estateDescr
     * @param stratTime
     * @param endTime
     * @param agencyId
     * @param titleImg
     * @return
     */
    @ApiOperation("添加物业公告")
    @RequestMapping(value = "/add",method = RequestMethod.POST)
    @ApiImplicitParams({
            @ApiImplicitParam(name = "type", value = "类型 1 消息新闻 2 物业公告 3 活动预告4.健康课堂", required = true, dataType = "Integer",paramType = "form"),
            @ApiImplicitParam(name = "isTop", value = "是否置顶 1  置顶 2 否", required = true, dataType = "Integer",paramType = "form"),
            @ApiImplicitParam(name = "estateDescr", value = "公告内容", required = true, dataType = "String",paramType = "form"),
            @ApiImplicitParam(name = "name", value = "公告名称", required = true, dataType = "String",paramType = "form"),
            @ApiImplicitParam(name = "stratTime", value = "开始时间", required = true, dataType = "String",paramType = "form"),
            @ApiImplicitParam(name = "endTime", value = "结束时间", required = true, dataType = "String",paramType = "form"),
            @ApiImplicitParam(name = "agencyId", value = "数组机构ID,用逗号隔开", required = true, dataType = "String",paramType = "form"),
            @ApiImplicitParam(name = "titleImg", value = "标题图片", required = true, dataType = "String",paramType = "form"),
    })
    public R add(Integer type, Integer isTop,String name,String estateDescr,
                 String stratTime, String endTime, String titleImg,String  agencyId){
        parameterIsBlank(name,"公告标题名称不能为空");
        parameterIsBlank(estateDescr,"公告内容名称不能为空");
        parameterIsBlank(stratTime,"开始时间名称不能为空");
        parameterIsBlank(endTime,"结束时间名称不能为空");
        parameterIsBlank(titleImg,"标题图片名称不能为空");
        parameterIsNull(agencyId,"机构ID名称不能为空");
        parameterIsNull(type,"公告类型名称不能为空");
        parameterIsNull(isTop,"是否置顶不能为空");
        EstateNotice estateNotice =new EstateNotice();
        estateNotice.setType(type);//公告类型
        estateNotice.setIsTop(isTop);
        estateNotice.setTitleImg(titleImg);//标题图片
        estateNotice.setName(name);//公告名称
        estateNotice.setNumber(0);
        estateNotice.setEstateDescr(estateDescr);//公告内容
        SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date stratTime1= null;
        Date endTime1 =null;
        try {
            stratTime1 = sdf.parse(stratTime);
            endTime1=sdf.parse(endTime);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        if (endTime1.getTime() <= stratTime1.getTime()){
            return  R.error(400,"结束时间必须要大于开始时间");
        }
        estateNotice.setStratTime(stratTime1);//开始时间
        estateNotice.setEndTime(endTime1);//结束时间
        estateNotice.setCreateTime(new Date());
        estateNoticeService.insert(estateNotice);
        String[] agencys =agencyId.split(",");
        for (int i = 0; i < agencys.length; i++) {
            String id = agencys[i];
            EstateNoticeAgency enc=new EstateNoticeAgency();
            enc.setNoticeId(estateNotice.getNoticeId());
            enc.setAgencyId(Long.parseLong(id));
            enc.setCreateTime(new Date());
            estateNoticeAgencyService.insert(enc);
        }
        return R.ok();
    }

    /**
     * 删除物业公告
     * @param noticeId
     * @return
     */
    @ApiOperation("删除物业公告")
    @RequestMapping(value = "/delete/{noticeId}",method = RequestMethod.POST)
    @ApiImplicitParams({
            @ApiImplicitParam(name = "noticeId", value = "物业公告ID", required = true, dataType = "Long",paramType = "form"),
            })
    public R delete(Long noticeId){
        parameterIsNull(noticeId,"是否置顶不能为空");
        EstateNotice estateNotice= estateNoticeService.selectById(noticeId);
        parameterIsNull(estateNotice,"该公告信息不存在");
        estateNoticeService.deleteById(noticeId);
        List<EstateNoticeAgency> ena= estateNoticeAgencyService.selectList(new EntityWrapper<EstateNoticeAgency>().eq("notice_id",noticeId));
        if (ena.size()>=1){
            for (int i=0;i<ena.size();i++){
                estateNoticeAgencyService.delete(new EntityWrapper<EstateNoticeAgency>().eq("notice_id",ena.get(i).getNoticeId()));
            }
        }
        return R.ok();
    }

    /**
     * 修改物业公告
     * @param type
     * @param isTop
     * @param name
     * @param estateDescr
     * @param stratTime
     * @param endTime
     * @param agencyId
     * @param titleImg
     * @return
     */
    @ApiOperation("修改物业公告")
    @RequestMapping(value = "/update",method = RequestMethod.POST)
    @ApiImplicitParams({
            @ApiImplicitParam(name = "noticeId", value = "公告ID", required = true, dataType = "Long",paramType = "form"),
            @ApiImplicitParam(name = "type", value = "类型 1 消息新闻 2 物业公告 3 活动预告4.健康课堂", required = false, dataType = "Integer",paramType = "form"),
            @ApiImplicitParam(name = "isTop", value = "是否置顶 1  置顶 2 否", required = false, dataType = "Integer",paramType = "form"),
            @ApiImplicitParam(name = "estateDescr", value = "公告内容", required = false, dataType = "String",paramType = "form"),
            @ApiImplicitParam(name = "name", value = "公告名称", required = false, dataType = "String",paramType = "form"),
            @ApiImplicitParam(name = "stratTime", value = "开始时间", required = false, dataType = "String",paramType = "form"),
            @ApiImplicitParam(name = "endTime", value = "结束时间", required = false, dataType = "String",paramType = "form"),
            @ApiImplicitParam(name = "agencyId", value = "数组机构ID,用逗号隔开", required = false, dataType = "String",paramType = "form"),
            @ApiImplicitParam(name = "titleImg", value = "标题图片", required = false, dataType = "String",paramType = "form"),
    })
    public R update(Long noticeId,Integer type, Integer isTop,String name,String estateDescr,
                 String stratTime, String endTime, String titleImg,String  agencyId){
        parameterIsNull(noticeId,"公告ID不能为空");
        EstateNotice  estateNotice= estateNoticeService.selectById(noticeId);
        parameterIsNull(estateNotice,"该公告信息不存在");
        if (!StringUtils.isEmpty(name)){
            estateNotice.setName(name);//公告名称
        }
        if (!StringUtils.isEmpty(titleImg)){
            estateNotice.setTitleImg(titleImg);//标题图片
        }
        if (!StringUtils.isEmpty(estateDescr)){
            estateNotice.setEstateDescr(estateDescr);//公告内容
        }
        if (type!=null){
            estateNotice.setType(type);//公告类型
        }
        if (isTop !=null){
            estateNotice.setIsTop(isTop);
        }
        SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date stratTime1= null;
        Date endTime1 =null;
        try {
            if (!StringUtils.isEmpty(stratTime)){
                stratTime1 = sdf.parse(stratTime);
                estateNotice.setStratTime(stratTime1);//开始时间
            }
            if (!StringUtils.isEmpty(endTime)){
                endTime1=sdf.parse(endTime);
                if (endTime1.getTime() <= stratTime1.getTime()){
                    return  R.error(400,"结束时间必须要大于开始时间");
                }
                estateNotice.setEndTime(endTime1);//结束时间
            }
        } catch (ParseException e) {
            e.printStackTrace();
        }
        estateNotice.setCreateTime(new Date());
        estateNoticeService.updateAllColumnById(estateNotice);
        if (agencyId!=null && !"".equals(agencyId)){
            estateNoticeAgencyService.delete(new EntityWrapper<EstateNoticeAgency>().eq("notice_id",noticeId));
            String[] agencys =agencyId.split(",");
        for (int i = 0; i < agencys.length; i++) {
            String id = agencys[i];
            EstateNoticeAgency enc=new EstateNoticeAgency();
            enc.setNoticeId(estateNotice.getNoticeId());
            enc.setAgencyId(Long.parseLong(id));
            enc.setCreateTime(new Date());
            estateNoticeAgencyService.insert(enc);
        }
      }
        return R.ok();
    }


    /**
     * 查询物业公告
     * @param noticeId
     * @return
     */
    @ApiOperation("查询物业公告")
    @RequestMapping(value = "/fianfById",method = RequestMethod.POST)
    @ApiImplicitParams({
            @ApiImplicitParam(name = "noticeId", value = "公告ID", required = true, dataType = "Long",paramType = "form"),
            })
    public R fianfById(Long noticeId){
        parameterIsNull(noticeId,"公告ID不能为空");
        EstateNotice  estateNotice= estateNoticeService.selectBynoticeId(noticeId);
        parameterIsNull(estateNotice,"该公告信息不存在");
        return R.ok().put("data",estateNotice).putDescription(EstateNotice.class);
    }


}