package com.pms.web;

import com.pms.exception.R;
import com.pms.service.IEstateAliService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by ljb on 2018/1/4.
 */
@RestController
@RequestMapping("test")
public class testController {

    @Autowired
    private IEstateAliService iEstateAliService;
    @PostMapping("/create")
    public R testCreate(){
        iEstateAliService.communityCreate("人和逸景","成都市金牛区营门口188号",
                "510102","510100","510000","11.212|12.123",
                "","028-5602354");
        return R.ok();
    }

}
