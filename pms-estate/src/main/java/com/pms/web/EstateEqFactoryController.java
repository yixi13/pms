package com.pms.web;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.mapper.Wrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.pms.controller.BaseController;
import com.pms.entity.EstateDoorLock;
import com.pms.entity.EstateEqFactory;
import com.pms.exception.R;
import com.pms.service.*;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 *1)厂商信息创建后，厂商类型 及 厂商编号 不能修改。
 */
@RestController
@RequestMapping("estateEqFactoryBase")
@Api(value="设备厂商信息管理",description = "设备厂商信息管理界面接口")
public class EstateEqFactoryController extends BaseController {
    Logger logger = LoggerFactory.getLogger(getClass());
    @Autowired
    private IEstateDoorLockService estateDoorLockService;
    @Autowired
    private IEstateEqFactoryService estateEqFactoryService;

    @ApiOperation(value = "分页查询厂商")
    @RequestMapping(value = "/readPage",method =RequestMethod.POST)
    @ApiImplicitParams({
            @ApiImplicitParam(name="pageSize",value="请求条数",required = false,dataType = "int",paramType="form"),
            @ApiImplicitParam(name="pageNum",value="请求页数",required = false,dataType = "int",paramType="form"),
            @ApiImplicitParam(name="factoryType",value="厂商类型(0-所有,1-锁,2-摄像头)",required = false,dataType = "int",paramType="form"),
    })
    public  R readPage(Integer factoryType,Integer pageSize,Integer pageNum){
        if(null==pageNum||pageNum<1){pageNum=1;}
        if(null==pageSize||pageSize<1){pageSize=10;}
        if(factoryType==null){
            factoryType=0;
        }
        Page<EstateEqFactory> page = new Page<EstateEqFactory>(pageNum,pageSize);
        Wrapper<EstateEqFactory> wp = new EntityWrapper<EstateEqFactory>();
        if(factoryType!=0){
            wp.eq("factory_type",factoryType);
        }
        Page<EstateEqFactory> dataPage = estateEqFactoryService.selectPage(page,wp);
        return R.ok().put("data",dataPage).putMpPageDescription().putDescription(EstateEqFactory .class);
    }
    @ApiOperation(value = "分页查询厂商")
    @RequestMapping(value = "/readList",method =RequestMethod.POST)
    @ApiImplicitParams({
            @ApiImplicitParam(name="factoryType",value="厂商类型(0-所有,1-锁,2-摄像头)",required = false,dataType = "int",paramType="form"),
    })
    public  R readList(Integer factoryType){
        if(factoryType==null){
            factoryType=0;
        }
        Wrapper<EstateEqFactory> wp = new EntityWrapper<EstateEqFactory>();
        if(factoryType!=0){
            wp.eq("factory_type",factoryType);
        }
        List<EstateEqFactory> dataPage = estateEqFactoryService.selectList(wp);
        if(dataPage==null){
            dataPage = new ArrayList<EstateEqFactory>();
        }
        return R.ok().put("data",dataPage).putDescription(EstateEqFactory .class);
    }

    @ApiOperation(value = "添加厂商")
    @RequestMapping(value = "/addEqFactory", method = RequestMethod.POST)
    @ApiImplicitParams({
            @ApiImplicitParam(name = "factoryName", value = "厂商名称", required = true, dataType = "string", paramType = "form"),
            @ApiImplicitParam(name = "factoryServiceUrl", value = "厂商服务地址(ip:port)", required = false, dataType = "string", paramType = "form"),
            @ApiImplicitParam(name = "factoryType", value = "厂商类型(1-锁,2-摄像头)", required = true, dataType = "int", paramType = "form"),
            @ApiImplicitParam(name = "factoryCode", value = "厂商编号", required = true, dataType = "string", paramType = "form"),
    })
    public R addEqFactory(String factoryName, Integer factoryType,String factoryServiceUrl,String factoryCode) {
        parameterIsBlank(factoryName,"厂商名称不能为空");
        parameterIsBlank(factoryCode,"厂商编号不能为空");
        parameterIsNull(factoryType,"请选择厂商类型");
        int countNum = estateEqFactoryService.selectCount(new EntityWrapper<EstateEqFactory>().eq("factory_code",factoryCode));
        if(countNum>0){
            return R.error(400,"厂商编号已存在");
        }
        if(StringUtils.isBlank(factoryServiceUrl)){
            factoryServiceUrl = "";
        }
        EstateEqFactory eqFactory = new EstateEqFactory();
        eqFactory.setFactoryServiceUrl(factoryServiceUrl);
        eqFactory.setFactoryName(factoryName);
        eqFactory.setFactoryType(factoryType);
        eqFactory.setCreateTime(new Date());
        eqFactory.setFactoryCode(factoryCode);
        estateEqFactoryService.insert(eqFactory);
        return R.ok();
    }
    @ApiOperation(value = "修改厂商")
    @RequestMapping(value = "/editEqFactory", method = RequestMethod.POST)
    @ApiImplicitParams({
            @ApiImplicitParam(name = "factoryId", value = "厂商id", required = true, dataType = "long", paramType = "form"),
            @ApiImplicitParam(name = "factoryName", value = "厂商名称", required = false, dataType = "string", paramType = "form"),
            @ApiImplicitParam(name = "factoryServiceUrl", value = "厂商服务地址(ip:port)", required = false, dataType = "string", paramType = "form"),
    })
    public R editEqFactory(Long factoryId,String factoryName,String factoryServiceUrl) {
        parameterIsNull(factoryId,"厂商id不能为空");
        EstateEqFactory eqFactory =estateEqFactoryService.selectById(factoryId);
        parameterIsNull(eqFactory,"未查询到厂商信息");
        boolean editTag = false;
        boolean editLockTag = false;
        if(StringUtils.isNotBlank(factoryName)){
            if(!eqFactory.getFactoryName().equals(factoryName)){
                eqFactory.setFactoryName(factoryName);
                editTag = true;
            }
        }
        if(StringUtils.isNotBlank(factoryServiceUrl)){
            if(!eqFactory.getFactoryServiceUrl().equals(factoryServiceUrl)){
                eqFactory.setFactoryServiceUrl(factoryServiceUrl);
                editTag = true;
                if(eqFactory.getFactoryType()==1){
                    editLockTag = true;
                }
            }
        }
        if(editTag){
            estateEqFactoryService.updateById(eqFactory);
            if(editLockTag){
                EstateDoorLock estateDoorLock = new EstateDoorLock();
                estateDoorLock.setFactoryServiceUrl(eqFactory.getFactoryServiceUrl());
                estateDoorLockService.update(estateDoorLock,new EntityWrapper<EstateDoorLock>().eq("factory_code",eqFactory.getFactoryCode()));
            }
        }
        return R.ok();
    }

    @ApiOperation(value = "删除厂商")
    @RequestMapping(value = "/delEqFactory", method = RequestMethod.POST)
    @ApiImplicitParams({
            @ApiImplicitParam(name = "factoryId", value = "厂商id", required = true, dataType = "long", paramType = "form"),
    })
    public R delEqFactory(Long factoryId) {
        parameterIsNull(factoryId,"厂商id不能为空");
        EstateEqFactory estateEqFactory= estateEqFactoryService.selectById(factoryId);
        parameterIsNull(estateEqFactory,"未查询到厂商信息");
        boolean delTag = false;
        if(estateEqFactory.getFactoryType().equals(1)){//锁
            int countNum = estateDoorLockService.selectCount(new EntityWrapper<EstateDoorLock>().eq("factory_code",estateEqFactory.getFactoryCode()));
            if(countNum==0){
                delTag = true;
            }
        }
        if(estateEqFactory.getFactoryType().equals(2)){//摄像头
            delTag = true;
        }
        if(!delTag){
            return R.error(400,"该厂商下已有设备在使用中，无法删除");
        }
        if(delTag){
            estateEqFactoryService.deleteById(factoryId);
        }
        return R.ok();
    }
}