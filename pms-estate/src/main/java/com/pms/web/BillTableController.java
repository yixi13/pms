package com.pms.web;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.pms.controller.BaseController;
import com.pms.entity.BillTable;
import com.pms.result.ObjectRestResponse;
import com.pms.result.TableResultResponse;
import com.pms.service.IBillTableService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("billTable")
public class BillTableController extends BaseController {
    Logger logger = LoggerFactory.getLogger(getClass());
    @Autowired
    IBillTableService billTableService;

    /**
         * 新增
         * @param entity
         * @return
         */
    @RequestMapping(value = "",method = RequestMethod.POST)
    @ResponseBody
    public ObjectRestResponse<BillTable> add(@RequestBody BillTable entity){
            billTableService.insert(entity);
        return new ObjectRestResponse<BillTable>().rel(true);
    }
    /**
     * 修改
     * @param entity
     * @return
     */
    @RequestMapping(value = "/{id}",method = RequestMethod.POST)
    @ResponseBody
    public ObjectRestResponse<BillTable> update(@RequestBody BillTable entity){
            billTableService.updateById(entity);
        return new ObjectRestResponse<BillTable>().rel(true);
    }

    /**
 * 删除
 * @param id
 * @return
 */
    @RequestMapping(value = "/{id}",method = RequestMethod.DELETE)
    @ResponseBody
    public ObjectRestResponse<BillTable> deleteById(@PathVariable int id){
            billTableService.deleteById (id);
        return new ObjectRestResponse<BillTable>().rel(true);
    }


    /**
     * findByID
     * @param id
     * @return
     */
    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    @ResponseBody
    public ObjectRestResponse<BillTable> findById(@PathVariable int id){
        return new ObjectRestResponse<BillTable>().rel(true).data( billTableService.selectById(id));
    }


    @RequestMapping(value = "/page",method = RequestMethod.GET)
    @ResponseBody
    public TableResultResponse<BillTable> page(@RequestParam(defaultValue = "10") int limit, @RequestParam(defaultValue = "1")int offset){
        Page<BillTable> page = new Page<BillTable>(offset,limit);
        page = billTableService.selectPage(page,new EntityWrapper<BillTable>());
        return new TableResultResponse<BillTable>(page.getTotal(),page.getRecords());
    }

    @RequestMapping(value = "/all",method = RequestMethod.GET)
    @ResponseBody
    public List<BillTable> all(){
        return billTableService.selectList(null);
    }


}