package com.pms.web;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.mapper.Wrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.pms.controller.BaseController;
import com.pms.entity.ChargeName;
import com.pms.entity.EstateAgency;
import com.pms.exception.R;
import com.pms.service.IChargeNameService;
import com.pms.service.IEstateAgencyService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

/**
 * <p>
 *  物业收费项 控制器
 *  1)不设修改功能，错误了=》删除,重新添加
 * </p>
 *
 * @author ASUS_B
 * @since 2017-11-07
 */
@RestController
@RequestMapping("/chargeNameBase")
@Api(value="收费项管理",description = "收费项管理相关接口")
public class ChargeNameController extends BaseController {
    @Autowired
    IChargeNameService chargeNameService;
    @Autowired
    private IEstateAgencyService estateAgencyService;

    @ApiOperation(value = "添加收费项")
    @RequestMapping(value = "/addChargeName",method = RequestMethod.POST)
    @ApiImplicitParams({
            @ApiImplicitParam(name="chargeName",value="费用名称",required = true,dataType = "string",paramType="form"),
            @ApiImplicitParam(name="chargeIsCommon",value="是否是公共收费(1-不是(默认),2-是)",required = true,dataType = "int",paramType="form"),
            @ApiImplicitParam(name="communityId",value="社区id(公共收费可为空)",required = false,dataType = "long",paramType="form"),
            @ApiImplicitParam(name="chargeWay",value="费用计算方式(1-建筑面积计算,2-固定费用,3/4/5-水/电/气使用量计算",required = true,dataType = "int",paramType="form"),
            @ApiImplicitParam(name="chargeType",value="费用类型(1-周期性收费-每月生成,2-临时性收费-某几个月)",required = true,dataType = "int",paramType="form"),
            @ApiImplicitParam(name="chargeRemarks",value="费用描述",required = false,dataType = "int",paramType="form"),
            @ApiImplicitParam(name="chargeStartTime",value="临时性收费起始时间(yyyyMMddHHmmss),周期性收费可为空",required = false,dataType = "string",paramType="form"),
            @ApiImplicitParam(name="chargeEndTime",value="临时性收费截止时间(yyyyMMddHHmmss),周期性收费可为空",required = false,dataType = "string",paramType="form"),

    })
    public R addUnitBuilding(String chargeName,Integer chargeIsCommon, Long communityId,Integer chargeWay,Integer chargeType
    ,String chargeRemarks ,String chargeStartTime ,String chargeEndTime){
        parameterIsBlank(chargeName,"请填写费用名称");
        if(chargeName.length()>40){ return R.error(400,"费用名称内容过长");}
        if(chargeRemarks.length()>220){return R.error(400,"费用描述内容过长");}
        parameterIsNull(chargeWay,"请选择费用计算方式");
        parameterIsNull(chargeType,"请选择费用类型");
        if(chargeType>2||chargeType<1){return R.error(400,"没有该费用类型");}
        if(null==chargeIsCommon||chargeIsCommon!=2){chargeIsCommon = 1;}//默认设置为公共收费
        EstateAgency community = null;
        if(chargeIsCommon==1){
            if(null == communityId){ return R.error(400,"非公共收费社区id不能为空");}
            community =estateAgencyService.selectById(communityId);
            if(null == community){ return R.error(400,"没有该社区信息");}
            if(null == community.getAgencyType()||community.getAgencyType()!=2){
                return R.error(400,"该机构不是社区类型");
            }
        }
        Date startTime= null;Date endTime= null;
        if(chargeType==2){
            SimpleDateFormat sdf = new SimpleDateFormat( "yyyyMMddHHmmss" );
            parameterIsBlank(chargeStartTime,"请选择临时性收费起始时间");
            parameterIsBlank(chargeEndTime,"请选择临时性收费截止时间");
            try{
                startTime = sdf.parse(chargeStartTime);
                endTime = sdf.parse(chargeEndTime);
            }catch(ParseException ex){
                return R.error(400,"时间格式错误");
            }
        }
        ChargeName chargeNameObj = new ChargeName();
        chargeNameObj.setChargeName(chargeName);
        chargeNameObj.setChargeIsCommon(chargeIsCommon);
        chargeNameObj.setCommunityId(communityId);
        chargeNameObj.setChargeWay(chargeWay);
        chargeNameObj.setChargeType(chargeType);
        chargeNameObj.setChargeEndTime(endTime);
        chargeNameObj.setChargeStartTime(startTime);
        chargeNameObj.setChargeRemarks(chargeRemarks);
        chargeNameObj.setCreateTime(new Date());
        chargeNameObj.setAgencyId(community.getParentId());
        chargeNameService.insert(chargeNameObj);
        return R.ok();
    }
    @ApiOperation(value = "删除收费项")
    @RequestMapping(value = "/delChargeName",method = RequestMethod.POST)
    @ApiImplicitParams({
            @ApiImplicitParam(name="chargeNameId",value="费用名称id",required = true,dataType = "long",paramType="form"),
    })
    public R delUnitBuilding(Long chargeNameId){
        parameterIsNull(chargeNameId,"请选择要删除的收费项");
        ChargeName chargeNameObj =chargeNameService.selectById(chargeNameId);
        if(null==chargeNameObj){return R.error(400,"没有该收费项信息");}
        if(chargeNameObj.getChargeIsCommon()==2){ return R.error(400,"公共收费项不能删除");}
        chargeNameService.deleteById(chargeNameId);
        return R.ok();
    }
    @ApiOperation(value = "查询收费项详情")
    @RequestMapping(value = "/readInfo",method = RequestMethod.POST)
    @ApiImplicitParams({
            @ApiImplicitParam(name="chargeNameId",value="费用名称id",required = true,dataType = "long",paramType="form"),
    })
    public R readInfo(Long chargeNameId){
        parameterIsNull(chargeNameId,"请选择收费项");
        ChargeName chargeNameObj =chargeNameService.selectById(chargeNameId);
        if(null==chargeNameObj){return R.error(400,"没有该收费项信息");}
        return R.ok().put("data",chargeNameObj).putDescription(ChargeName.class);
    }

    @ApiOperation(value = "分页查询收费项")
    @RequestMapping(value = "/readPage",method = RequestMethod.POST)
    @ApiImplicitParams({
            @ApiImplicitParam(name="pageSize",value="请求条数",required = false,dataType = "int",paramType="form"),
            @ApiImplicitParam(name="pageNum",value="请求页数",required = false,dataType = "int",paramType="form"),
            @ApiImplicitParam(name="communityId",value="社区id",required = false,dataType = "long",paramType="form"),
            @ApiImplicitParam(name="agencyId",value="物业机构id",required = true,dataType = "long",paramType="form"),
            @ApiImplicitParam(name="chargeType",value="费用类型(1-周期性收费-每月生成,2-临时性收费-某几个月)",required = true,dataType = "int",paramType="form"),
    })
    public R readPage(Integer pageSize,Integer pageNum,Long communityId,Integer chargeType,Long agencyId){
        if(null==pageNum||pageNum<1){pageNum=1;}
        if(null==pageSize||pageSize<1){pageSize=10;}
        Page<ChargeName> page = new Page<ChargeName>(pageNum,pageSize);
        Wrapper<ChargeName> wrapper=  new EntityWrapper<ChargeName>();
        if(null!=agencyId){
            wrapper.eq("agency_id",agencyId);
        }
        if(null!=communityId){
            wrapper .eq("community_id",communityId);
        }
        if(null!=chargeType){ wrapper.eq("charge_type",chargeType);}
        Page<ChargeName> returnPage = chargeNameService.selectPage(page,wrapper);
        return R.ok().put("data",returnPage).putMpPageDescription().putDescription(ChargeName.class);
    }

    @ApiOperation(value = "查询收费项")
    @RequestMapping(value = "/readList",method = RequestMethod.POST)
    @ApiImplicitParams({
            @ApiImplicitParam(name="communityId",value="社区id",required = false,dataType = "long",paramType="form"),
            @ApiImplicitParam(name="agencyId",value="物业机构id",required = true,dataType = "long",paramType="form"),
            @ApiImplicitParam(name="chargeType",value="费用类型(1-周期性收费-每月生成,2-临时性收费-某几个月)",required = true,dataType = "int",paramType="form"),
    })
    public R readList(Long communityId,Integer chargeType,Long agencyId){
        Wrapper<ChargeName> wrapper=  new EntityWrapper<ChargeName>();
        if(null!=agencyId){
            wrapper.eq("agency_id",agencyId);
        }
        if(null!=communityId){
            wrapper .eq("community_id",communityId);
        }
        if(null!=chargeType){ wrapper.eq("charge_type",chargeType);}
        List<ChargeName> returnPage = chargeNameService.selectList(wrapper);
        return R.ok().put("data",returnPage).putDescription(ChargeName.class);
    }
}
