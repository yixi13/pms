package com.pms.web;
import com.pms.entity.EstateAgency;
import com.pms.entity.RepairEvalua;
import com.pms.entity.RepairImg;
import com.pms.exception.R;
import com.pms.service.IEstateAgencyService;
import com.pms.service.IRepairEvaluaService;
import com.pms.service.IRepairImgService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.plugins.Page;
import org.springframework.web.bind.annotation.*;
import com.pms.controller.BaseController;
import com.pms.service.IEquipmentRepairService;
import com.pms.entity.EquipmentRepair;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * 物业报修表
 * zpf
 *
 */
@RestController
@RequestMapping("equipmentRepair")
@Api(value="物业报修表",description = "物业报修表")
public class EquipmentRepairController extends BaseController {
    Logger logger = LoggerFactory.getLogger(getClass());
    @Autowired
    IEquipmentRepairService equipmentRepairService;
    @Autowired
    IRepairImgService repairImgService;
    @Autowired
    IRepairEvaluaService repairEvaluaService;
    @Autowired
    IEstateAgencyService estateAgencyService;
    /**
     *分页查询 物业报修表
     * @param limit
     * @param offset
     * @return
     */
    @RequestMapping(value = "/page",method = RequestMethod.POST)
    public R page(@RequestParam(defaultValue = "10") int limit, @RequestParam(defaultValue = "1")int offset){
        Page<EquipmentRepair> page = new Page<EquipmentRepair>(offset,limit);
        page = equipmentRepairService.selectPage(page,new EntityWrapper<EquipmentRepair>());
        return R.ok().put("data",page).putDescription(EquipmentRepair.class);
    }

    /**
     * 删除物业报修
     * @return
     */
    @ApiOperation(value = "删除物业报修")
    @RequestMapping(value = "/delete/{id}",method = RequestMethod.POST)
    @ApiImplicitParams({
            @ApiImplicitParam(name="repairId",value="物业报修ID",required = true,dataType = "Long",paramType="form"),
    })
    public R deleteEquipmentRepair(Long repairId){
        parameterIsNull(repairId,"物业报修ID不能为空");
        EquipmentRepair equipmentRepair=equipmentRepairService.selectById(repairId);
        parameterIsNull(equipmentRepair,"该物业报修信息不存在");
            //删除这条物业报修
            equipmentRepairService.deleteById(repairId);
            //删除该条物业报修图片
            repairImgService.delete(new EntityWrapper<RepairImg>().eq("repair_id",equipmentRepair.getRepairId()));
            //删除该条物业报修回复内容
            repairEvaluaService.delete(new EntityWrapper<RepairEvalua>().eq("repair_id",equipmentRepair.getRepairId()));
            return R.ok();
    }

    /**
     * 查询物业报修回复内容
     * @param repairId
     * @return
     */
    @ApiOperation(value ="查询物业报修回复内容")
    @RequestMapping(value = "/get/repairEvaluaRecord",method = RequestMethod.POST)
    @ApiImplicitParams({
            @ApiImplicitParam(name="repairId",value="物业报修ID",required = true,dataType = "Long",paramType="form"),
    })
    public R getRepairEvaluaRecord(Long repairId){
        parameterIsNull(repairId,"物业报修ID不能为空");
        List<RepairEvalua> repairEvalua=repairEvaluaService.selectList(new EntityWrapper<RepairEvalua>().eq("repair_id",repairId));
        return R.ok().put("data",repairEvalua).putDescription(RepairEvalua.class);
    }

    /**
     *状态查询物业报修状态内容
     * @param size
     * @param page
     * @param agencyId
     * @param state
     * @return
     */
    @ApiOperation(value ="状态查询物业报修状态内容")
    @RequestMapping(value = "/get/getEquipmentRepairRecord",method = RequestMethod.POST)
    @ApiImplicitParams({
            @ApiImplicitParam(name="state",value="状态1.已报修 2.处理中3.已处理(待评价)4.已评价",required = true,dataType = "Integer",paramType="form"),
            @ApiImplicitParam(name="agencyId",value="机构ID",required = true,dataType = "Long",paramType="form"),
            @ApiImplicitParam(name = "size", value = "条数（默认为10）", required = false, dataType = "Integer",paramType = "form"),
            @ApiImplicitParam(name = "page", value = "页数（默认为1）", required = false, dataType = "Integer",paramType = "form"),
            })
    public R getEquipmentRepairRecord(Integer size,Integer page,Long agencyId,Integer state){
        parameterIsNull(agencyId,"机构ID不能为空");
        parameterIsNull(state,"状态不能为空");
        EstateAgency sysAgency=estateAgencyService.selectById(agencyId);
        parameterIsNull(sysAgency,"该机构信息不存在");
        if (null == size || size < 1){ size=10;}
        if (null== page ||  page<1 ){page=1;}
        Page<EquipmentRepair> pages = new Page<EquipmentRepair>(page,size);
        Page<Map<String, Object>> eq= new Page<Map<String, Object>>();
        if (state == -1){
             eq=equipmentRepairService.getequipmentRepair(pages,null);
        }else{
            eq=equipmentRepairService.getequipmentRepair(pages,new EntityWrapper<EquipmentRepair>().eq("re.state_",state));
        }
        return R.ok().put("data",eq).putDescription(EquipmentRepair.class);
    }

    /**
     * 指派物业维修人员
     * 状态1.已报修 2.处理中3.已处理(待评价)4.已评价
     * @return
     */
    @ApiOperation(value ="指派物业维修人员")
    @RequestMapping(value = "/add/addEquipmentRepairPersonnel",method = RequestMethod.POST)
    @ApiImplicitParams({
            @ApiImplicitParam(name="name",value="物业维修人员",required = true,dataType = "String",paramType="form"),
            @ApiImplicitParam(name = "phone", value = "物业维修人员手机号", required = true, dataType = "String",paramType = "form"),
            @ApiImplicitParam(name="repairId",value="物业报修ID",required = true,dataType = "Long",paramType="form"),
    })
    public R addEquipmentRepairPersonnel(String phone,String name,Long repairId){
        parameterIsNull(repairId,"物业报修ID不能为空");
        parameterIsBlank(phone,"物业维修人员手机号不能为空");
        parameterIsBlank(name,"物业维修人员不能为空");
        EquipmentRepair eq= equipmentRepairService.selectById(repairId);
        parameterIsNull(eq,"物业报修信息不存在");
        if(null==eq.getState()){
            return R.error("该报修单状态出错");
        }else if(eq.getState()==2){
            return R.error("该报修单正在处理中");
        }else if(eq.getState()>2){
            return R.error("该报修单已处理");
        }else {
            eq.setPhone(phone);
            eq.setProcessingTime(new Date());
            eq.setName(name);
            eq.setState(2);//指派维修人员 状态改为2  处理中
        }
        equipmentRepairService.updateAllColumnById(eq);
        return R.ok();
    }



 }