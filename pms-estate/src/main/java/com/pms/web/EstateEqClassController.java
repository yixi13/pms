package com.pms.web;

import com.alibaba.druid.util.StringUtils;
import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.pms.controller.BaseController;
import com.pms.entity.*;
import com.pms.exception.R;
import com.pms.service.*;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import java.util.Date;
import java.util.List;

/**
 * 物业设备摄像头
 *
 */
@RestController
@RequestMapping("estateEqClass")
@Api(value="物业设备分类",description = "物业设备分类")
public class EstateEqClassController extends BaseController {
    Logger logger = LoggerFactory.getLogger(getClass());
    @Autowired
    IEstateEqClassService estateEqClassService;


    /**
     * 新增
     * @param
     * @return
     */
    @ApiOperation(value = "添加设备分类")
    @RequestMapping(value = "/add",method = RequestMethod.POST)
    @ApiImplicitParams({
            @ApiImplicitParam(name="eqclassName",value="物业设备分类名称",required = true,dataType = "String",paramType="form"),
            @ApiImplicitParam(name="type",value="设备分类1.物业锁2.摄像头",required = true,dataType = "Integer",paramType="form"),
            @ApiImplicitParam(name="pId",value="父级ID",required = true,dataType = "Long",paramType="form"),
            @ApiImplicitParam(name="level",value="级别",required = true,dataType = "Integer",paramType="form"),
            @ApiImplicitParam(name="eqImg",value="设备图片",required = true,dataType = "String",paramType="form"),
            })
    public R add(String eqclassName,Integer type,Long pId,Integer level,String eqImg){
        parameterIsBlank(eqclassName,"物业设备分类名称不能为空");
        parameterIsNull(type,"设备分类不能为空");
        parameterIsNull(pId,"父级ID不能为空");
        parameterIsNull(level,"级别不能为空");
        parameterIsNull(eqImg,"设备图片不能为空");
        EstateEqClass entity=new EstateEqClass();
        entity.setEqclassName(eqclassName);
        entity.setType(type);
        entity.setLevel(level);
        entity.setEqImg(eqImg);
        entity.setPId(pId);
        entity.setCreateTime(new Date());
        estateEqClassService.insert(entity);
        return  R.ok();
    }
    /**
     * 更改
     * @param entity
     * @return
     */
    /**
     * 新增
     * @param
     * @return
     */
    @ApiOperation(value = "添加设备分类")
    @RequestMapping(value = "/update/{id}",method = RequestMethod.POST)
    @ApiImplicitParams({
            @ApiImplicitParam(name="eqclassName",value="物业设备分类名称",required = true,dataType = "String",paramType="form"),
            @ApiImplicitParam(name="eqclassName",value="物业设备分类名称",required = true,dataType = "String",paramType="form"),
            @ApiImplicitParam(name="type",value="设备分类1.物业锁2.摄像头",required = true,dataType = "Integer",paramType="form"),
            @ApiImplicitParam(name="pId",value="父级ID",required = true,dataType = "Long",paramType="form"),
            @ApiImplicitParam(name="level",value="级别",required = true,dataType = "Integer",paramType="form"),
            @ApiImplicitParam(name="eqImg",value="设备图片",required = true,dataType = "String",paramType="form"),
    })
    public R update(Long eqclassId,String eqclassName,Integer type,Long pId,Integer level,String eqImg){
        parameterIsBlank(eqclassId.toString(),"设备分类ID不能为空");
        EstateEqClass eq= estateEqClassService.selectById(eqclassId);
        parameterIsNull(eq,"该设备分类信息不存在");
        if (!StringUtils.isEmpty(eqclassName)){
            eq.setEqclassName(eqclassName);
        }
        if (!StringUtils.isEmpty(pId.toString())){
            eq.setPId(pId);
        }
        if (!StringUtils.isEmpty(eqImg)){
            eq.setEqImg(eqImg);
        }
        if (!StringUtils.isEmpty(type.toString())){
            eq.setType(type);
        }
        if (!StringUtils.isEmpty(level.toString())){
            eq.setLevel(level);
        }
        eq.setUpdateTime(new Date());
        estateEqClassService.updateById(eq);
        return R.ok();
    }



    /**
     * 删除
     * @param
     * @return
     */
    @RequestMapping(value = "/delete/{eqclassId}",method = RequestMethod.POST)
    @ApiOperation(value = "删除设备分类")
    @ApiImplicitParams({
            @ApiImplicitParam(name="eqclassId",value="设备分类ID",required = true,dataType = "Long",paramType="form")
    })
    public R deleteById(@RequestParam  Long eqclassId){
        parameterIsNull(eqclassId,"设备分类ID不能为空");
        EstateEqClass eq=estateEqClassService.selectById(eqclassId);
        parameterIsNull(eq,"该设备分类不存在，请从新输入");
        estateEqClassService.deleteById (eqclassId);
        return R.ok();
    }


    /**
     * 查询设备分类详情
     * @param eqclassId
     * @return
     */
    @RequestMapping(value = "/findById/{eqclassId}", method = RequestMethod.POST)
    @ApiOperation(value = "查询设备分类详情")
    @ApiImplicitParams({
            @ApiImplicitParam(name="eqclassId",value="设备分类ID",required = true,dataType = "Long",paramType="form")
    })
    public R findById(Long eqclassId){
        parameterIsNull(eqclassId,"设备分类ID不能为空");
        EstateEqClass eq=estateEqClassService.selectById(eqclassId);//判断摄像头是否存在
        parameterIsNull(eq,"该设备分类不存在，请从新输入");
        return R.ok().put("data",eq).putDescription(EstateEqClass.class);
    }

    /**
     * 分页查询设备分类
     * @param limit
     * @param page
     * @return
     */
    @RequestMapping(value = "/page",method = RequestMethod.GET)
    @ResponseBody
    public R page(@RequestParam int limit, @RequestParam int page){
        if(limit<1){limit=10;}
        if(page<1){page=1;}
        Page<EstateEqClass> pages = new Page<EstateEqClass>(page,limit);
        pages = estateEqClassService.selectPage(pages,new EntityWrapper<EstateEqClass>());
        return R.ok().put("data",pages).putDescription(EstateEqClass.class);
    }

    /**
     * 查询所有设备分类 数组
     * @return
     */
    @RequestMapping(value = "/all",method = RequestMethod.GET)
    @ResponseBody
    public List<EstateEqClass> all(){
        return estateEqClassService.selectList(null);
    }


}