package com.pms.web;

import com.alibaba.druid.util.StringUtils;
import com.pms.exception.R;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.plugins.Page;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import com.pms.controller.BaseController;
import com.pms.service.ICameraUserInformationService;
import com.pms.entity.CameraUserInformation;
import org.springframework.web.bind.annotation.RequestMapping;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
@RestController
@RequestMapping("cameraUserInformation")
@Api(value="摄像头用户信息",description = "摄像头用户信息")
public class CameraUserInformationController extends BaseController {
    Logger logger = LoggerFactory.getLogger(getClass());
    @Autowired
    ICameraUserInformationService cameraUserInformationService;

    /**
     * 添加摄像头用户信息
     * @param appKey
     * @param appSecret
     * @param userName
     * @return
     */
    @ApiOperation(value = "添加摄像头用户信息")
    @RequestMapping(value = "/add",method = RequestMethod.POST)
    @ApiImplicitParams({
            @ApiImplicitParam(name="appKey",value="账号",required = true,dataType = "String",paramType="form"),
            @ApiImplicitParam(name="appSecret",value="密码",required = true,dataType = "String",paramType="form"),
            @ApiImplicitParam(name="userName",value="用户别名",required = true,dataType = "String",paramType="form"),
            })
    public R add(String appKey, String appSecret, String userName){
        parameterIsBlank(appKey,"用户信息账号不能为空");
        parameterIsBlank(appSecret,"用户信息密码不能为空");
        parameterIsBlank(userName,"用户信息别名不能为空");
        CameraUserInformation user= cameraUserInformationService.selectOne(new EntityWrapper<CameraUserInformation>().eq("appKey",appKey));
        if (user==null){
            user.setAppkey(appKey);
            user.setAppsecret(appSecret);
            user.setSum(0);
            cameraUserInformationService.insert(user);
            return  R.ok();
        }else{
            return  R.error(400,"改账号已被添加！");
        }
    }

    /**
     * 修改摄像头用户信息
     * @param id
     * @param appKey
     * @param appSecret
     * @param userName
     * @return
     */
    @ApiOperation(value = "修改摄像头用户信息")
    @RequestMapping(value = "/update",method = RequestMethod.POST)
    @ApiImplicitParams({
            @ApiImplicitParam(name="id",value="用户信息ID",required = true,dataType = "Long",paramType="form"),
            @ApiImplicitParam(name="appKey",value="账号",required = false,dataType = "String",paramType="form"),
            @ApiImplicitParam(name="appSecret",value="密码",required = false,dataType = "String",paramType="form"),
            @ApiImplicitParam(name="userName",value="用户别名",required = false,dataType = "String",paramType="form"),
    })
    public R update(Long id,String appKey, String appSecret, String userName){
        parameterIsNull(id,"用户信息ID不能为空");
        CameraUserInformation user= cameraUserInformationService.selectById(id);
        parameterIsNull(user,"该用户信息不存在");
        if (!StringUtils.isEmpty(appKey)){
            user.setAppkey(appKey);
        }
        if (!StringUtils.isEmpty(appSecret)){
            user.setAppsecret(appSecret);
        }
        if (!StringUtils.isEmpty(userName)){
            user.setUserName(userName);
        }
            cameraUserInformationService.updateAllColumnById(user);
            return  R.ok();
    }

    /**
     * 查询摄像头用户信息详情
     * @param id
     * @return
     */
    @RequestMapping(value = "/findById/{id}", method = RequestMethod.POST)
    @ApiOperation(value = "查询摄像头用户信息详情")
    @ApiImplicitParams({
            @ApiImplicitParam(name="id",value="摄像头用户信息ID",required = true,dataType = "Long",paramType="form")
    })
    public R findById(Long id){
        parameterIsNull(id,"摄像头用户信息ID不能为空");
        CameraUserInformation user= cameraUserInformationService.selectById(id);
        parameterIsNull(user,"该用户信息不存在");
        return R.ok().put("data",user).putDescription(CameraUserInformation.class);
    }

    /**
     * 分页查询摄像头
     * @param limit
     * @param page
     * @return
     */
    @ApiOperation(value = "分页查询摄像头用户信息")
    @RequestMapping(value = "/page",method = RequestMethod.POST)
    @ApiImplicitParams({
            @ApiImplicitParam(name="limit",value="条数",required = false,dataType = "int",paramType="form"),
            @ApiImplicitParam(name="page",value="页数",required = false,dataType = "int",paramType="form"),
            @ApiImplicitParam(name="userName",value="用户别名",required = false,dataType = "String",paramType="form")
    })
    public R page(int limit, int page,String userName){
        if(limit<1){limit=10;}
        if(page<1){page=1;}
        Page<CameraUserInformation> pages = new Page<CameraUserInformation>(page,limit);
        pages = cameraUserInformationService.selectPage(pages,new EntityWrapper<CameraUserInformation>().like("user_name",userName));
        return R.ok().put("data",pages).putDescription(CameraUserInformation.class);
    }


}