package com.pms.web;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.pms.controller.BaseController;
import com.pms.entity.BillTableDetail;
import com.pms.result.ObjectRestResponse;
import com.pms.result.TableResultResponse;
import com.pms.service.IBillTableDetailService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("billTableDetail")
public class BillTableDetailController extends BaseController {
    Logger logger = LoggerFactory.getLogger(getClass());
    @Autowired
    IBillTableDetailService billTableDetailService;

    /**
         * 新增
         * @param entity
         * @return
         */
    @RequestMapping(value = "",method = RequestMethod.POST)
    @ResponseBody
    public ObjectRestResponse<BillTableDetail> add(@RequestBody BillTableDetail entity){
            billTableDetailService.insert(entity);
        return new ObjectRestResponse<BillTableDetail>().rel(true);
    }
    /**
     * 修改
     * @param entity
     * @return
     */
    @RequestMapping(value = "/{id}",method = RequestMethod.POST)
    @ResponseBody
    public ObjectRestResponse<BillTableDetail> update(@RequestBody BillTableDetail entity){
            billTableDetailService.updateById(entity);
        return new ObjectRestResponse<BillTableDetail>().rel(true);
    }

    /**
 * 删除
 * @param id
 * @return
 */
    @RequestMapping(value = "/{id}",method = RequestMethod.DELETE)
    @ResponseBody
    public ObjectRestResponse<BillTableDetail> deleteById(@PathVariable int id){
            billTableDetailService.deleteById (id);
        return new ObjectRestResponse<BillTableDetail>().rel(true);
    }


    /**
     * findByID
     * @param id
     * @return
     */
    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    @ResponseBody
    public ObjectRestResponse<BillTableDetail> findById(@PathVariable int id){
        return new ObjectRestResponse<BillTableDetail>().rel(true).data( billTableDetailService.selectById(id));
    }


    @RequestMapping(value = "/page",method = RequestMethod.GET)
    @ResponseBody
    public TableResultResponse<BillTableDetail> page(@RequestParam(defaultValue = "10") int limit, @RequestParam(defaultValue = "1")int offset){
        Page<BillTableDetail> page = new Page<BillTableDetail>(offset,limit);
        page = billTableDetailService.selectPage(page,new EntityWrapper<BillTableDetail>());
        return new TableResultResponse<BillTableDetail>(page.getTotal(),page.getRecords());
    }

    @RequestMapping(value = "/all",method = RequestMethod.GET)
    @ResponseBody
    public List<BillTableDetail> all(){
        return billTableDetailService.selectList(null);
    }


}