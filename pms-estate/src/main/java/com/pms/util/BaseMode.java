package com.pms.util;

import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableField;
import com.pms.util.idutil.SnowFlake;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.lang.reflect.Field;
import java.lang.reflect.ParameterizedType;

/**
 * Created by ASUS_B on 2017/10/23.
 */
public abstract class BaseMode<T extends BaseMode> extends Model<BaseMode>{

    private Logger logger = LoggerFactory.getLogger(getClass());

    private static final Long snowFlakeId = new SnowFlake(0,0).nextId();

    /**
     * 获取Id值
     * @return
     */
    protected String snowFlakeNextId(){
        return snowFlakeId.toString();
    }

    /**
     * 描述
     */
    @TableField(exist = false)
    protected String description;

    public String getDescription() {
        if(StringUtils.isBlank(description)){
            setDescription(returnDescription());
        }
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String returnDescription(){
        StringBuffer str = new StringBuffer("");
        // 获取泛型T 的类型对象
        Class <T>  entityClass  =  (Class <T>)((ParameterizedType) getClass().getGenericSuperclass()).getActualTypeArguments()[ 0 ];
        Field[] fields = entityClass.getDeclaredFields();
        for(Field f : fields){
            Description desc = f.getAnnotation(Description.class);
            if(null!=desc){
                str.append(f.getName()).append(":").append(desc.value()).append(",");
            }
        }
        return str.toString();
    }
}
