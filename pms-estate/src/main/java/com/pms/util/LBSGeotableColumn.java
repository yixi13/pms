package com.pms.util;

import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;
import com.pms.entity.BaseModel;
import org.apache.commons.lang.StringUtils;

import java.io.Serializable;
import java.util.Date;

/**
 * <p>
 * lbs 添加列  云存储V3
 * </p>
 *
 * @author ASUS_B
 * @since 2017-10-26
 */
public class LBSGeotableColumn{
	/**
	 * 验证  数据是否完整
	 * @return
	 */
	public boolean validateLBSGeotableColumn(){
		if(StringUtils.isBlank(getColumnName())){return false;}//判断列名
		if(StringUtils.isBlank(getColumnNameDescription())){return false;}//判断列名描述
		if(getColumnType()<1||getColumnType()>4){return false;}//判断 列类型
		if(getColumnType()==3){
			if(getMaxLength()<1||getMaxLength()>2048){setMaxLength(2047);}//判断 字符串列 最大长度
			if(getIsSearchField()!=0 && getIsSearchField()!=1){return false;}//判断 字符串列 是否检索
			if(getIsSearchField()==1){setMaxLength(512);}//检索字段只能用于字符串类型的列且最大长度不能超过512个字节
		}
		if(getColumnType()<3){
			if(getIsSortfilterField()!=0 && getIsSortfilterField()!=1){return false;}//判断 数字列 是否排序
		}
		if(getIsIndexField()!=0 && getIsIndexField()!=1){return false;}//判断 是否设置 存储索引
		return true;
	}
	/**
	 * 列名描述
	 * 必填字段
	 */
	private String columnNameDescription;
	/**
	 * 列名
	 * 必填字段
	 */
	private String columnName;
	/**
	 * 列类型(1-int, 2-double, 3-string, 4-在线图片url)
	 * 必填字段
	 */
	private int columnType =3;
	/**
	 * 最大长度(最大值2048，最小值为1)
	 * columnType为3时必填
	 */
	private int maxLength = 2000;
	/**
	 * columnType为 3 时 否设置为 检索字段(1是|0否)(最多设置4个)
	 * columnType为3时必填
	 */
	private int isSearchField=1;
	/**
	 * 是否设置为排序筛选字段 (1是|0否) ，最多设置15个
	 * columnType 为1,2时 必填
	 */
	private int isSortfilterField=1;
	/**
	 * 是否将字段设置为云存储的索引字段(1是，0否)
	 * 必填字段
	 */
	private int isIndexField=0;
	/**
	 * 表id (geotable_id)
	 * 必填字段
	 */
	private String  geotableId;
	/**
	 * 用户的访问权限key
	 * 必填字段
	 */
	private String ak;

	public String getColumnNameDescription() {
		return columnNameDescription;
	}

	public void setColumnNameDescription(String columnNameDescription) {
		this.columnNameDescription = columnNameDescription;
	}

	public String getColumnName() {
		return columnName;
	}

	public void setColumnName(String columnName) {
		this.columnName = columnName;
	}

	public int getColumnType() {
		return columnType;
	}

	public void setColumnType(int columnType) {
		this.columnType = columnType;
	}

	public int getMaxLength() {
		return maxLength;
	}

	public void setMaxLength(int maxLength) {
		this.maxLength = maxLength;
	}

	public int getIsSearchField() {
		return isSearchField;
	}

	public void setIsSearchField(int isSearchField) {
		this.isSearchField = isSearchField;
	}

	public int getIsSortfilterField() {
		return isSortfilterField;
	}

	public void setIsSortfilterField(int isSortfilterField) {
		this.isSortfilterField = isSortfilterField;
	}

	public int getIsIndexField() {
		return isIndexField;
	}

	public void setIsIndexField(int isIndexField) {
		this.isIndexField = isIndexField;
	}

	public String getGeotableId() {
		return geotableId;
	}

	public void setGeotableId(String geotableId) {
		this.geotableId = geotableId;
	}

	public String getAk() {
		return ak;
	}

	public void setAk(String ak) {
		this.ak = ak;
	}

}
