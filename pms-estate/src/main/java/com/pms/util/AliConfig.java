package com.pms.util;

/**
 * Created by ljb on 2018/1/3.
 */
public class AliConfig {
    //↓↓↓↓↓↓↓↓↓↓请在这里配置您的基本信息↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓
    // 合作身份者ID，以2088开头由16位纯数字组成的字符串
    public static String partner = "2088421360041050";
    //不能用众农汇的 app_id
    public static String app_id = "2016062901564625";
    // 商户的私钥
    public static String private_key = "MIICdQIBADANBgkqhkiG9w0BAQEFAASCAl8wggJbAgEAAoGBAJ/OLMN9xAZ8yENARGSu7neIDlDHdjFXe1/P6Nd0zvI+68oq8f1/lsaeozFhZrl/1KyIoHOUJS50cL3qYB+mPWe6qGMWCZwNYl9BgTz4tvi6Yx892jOY7zcwFiwOh6v2qgt1VXAdfNL4fJlkU3gMHW/QigrGwfLfNoL2OwtC3NyFAgMBAAECgYBVlTtP7ti+iLFPoul96ll31nukM05PGoTOSTBvwvTZs2zftqQmmOAMuwS2+2ziR12EkR8tBtfqyx1d+FlGWedj/zVJ6ArFNgt3fFvvgHcvMsR1xF7u5QwD25TfAzXlznuhIMNr74SqeeMyRQd5Ubd9th7fLWGZdOJNv633QhVHwQJBANK9DyHTPIXg8AMvWZmrGeArwYifFVNNXARwWvYANTtPisyl7I/+E7PVVwWQWC4mU6VuXk2w75UJ7t1a4CMf7SkCQQDCIK6E+d5kkCR0FHIYO4f0JCKxzDaPiqexf9cxYUfBsE93XEkqcKfK4J33AGeselDzGgBSQeegvUx4uBbRSAP9AkB0RlxTDXoXZEUJfNazMvTT+1CNUTky/TwINvX5RaQZVYn/4Izl5gp47v1U1I/S5tLANGVT+Iw4b4KcH8gLmVIBAkBKsgqUzhQ9e50bMTbJjjZ8wV68LQidaMunp7oknPrgUzfwhIj+lIRrssv1W6mImzQEpm+TNxqqJcyD8smM03n9AkALB8mqFF9xTufxEWjq5qMpxwqdwNv/1o5j2NUSv0vmE2zARwh4BD8pldG41f7svGpYzPrhsybEpIo0emdL19MW";

    // 支付宝的公钥，无需修改该值
    public static String ali_public_key = "MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQDDI6d306Q8fIfCOaTXyiUeJHkrIvYISRcc73s3vF1ZT7XN8RNPwJxo8pWaJMmvyTn9N4HQ632qJBVHf8sxHi/fEsraprwCtzvzQETrNRwVxLO5jVmRGi60j8Ue1efIlzPXV9je9mkjzOmdssymZkh2QhUrCmZYI/FCEa3/cNMW0QIDAQAB";

   //↑↑↑↑↑↑↑↑↑↑请在这里配置您的基本信息↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑
    public static String seller_id = "hongming-2005@163.com";

    // 调试用，创建TXT日志文件夹路径
    public static String log_path = "D:\\";

    // 字符编码格式 目前支持 gbk 或 utf-8
    public static String input_charset = "utf-8";
    //md5加密的key
    public static String key = "mlztshb7p4eidk9eznynb18wj1go0nm4";
    // 签名方式 不需修改
    public static String sign_type = "RSA";


    public static String BACK_URL = "120.26.235.96/appthirdpay/alipay/notify";
//	public static String BACK_URL="120.26.246.86/appthirdpay/alipay/notify";
}
