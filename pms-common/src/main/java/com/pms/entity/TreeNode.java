package com.pms.entity;

import java.util.ArrayList;
import java.util.List;

/**
 * @author zyl
 * @create 2017-07-06 15:11
 **/
public class TreeNode {
    protected int id;
    protected int parentId;
    protected String permissionsId;
    protected Integer  menuType;
    protected int baseElementId;
    protected String title;

    public Integer getMenuType() {
        return menuType;
    }

    public void setMenuType(Integer menuType) {
        this.menuType = menuType;
    }

    public int getBaseElementId() {
        return baseElementId;
    }



    public void setBaseElementId(int baseElementId) {
        this.baseElementId = baseElementId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getPermissionsId() {
        return permissionsId;
    }

    public void setPermissionsId(String permissionsId) {
        this.permissionsId = permissionsId;
    }

    public List<TreeNode> getChildren() {
        return children;
    }

    public void setChildren(List<TreeNode> children) {
        this.children = children;
    }

    List<TreeNode> children = new ArrayList<TreeNode>();

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getParentId() {
        return parentId;
    }

    public void setParentId(int parentId) {
        this.parentId = parentId;
    }

    public void add(TreeNode node){
        children.add(node);
    }
}
