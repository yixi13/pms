package com.pms.entity;

import com.baomidou.mybatisplus.activerecord.Model;
import com.pms.util.idutil.SnowFlake;

/**
 * Created by ASUS_B on 2017/10/23.
 */
public abstract class BaseModel<T extends BaseModel> extends Model<BaseModel>{

    private static final SnowFlake snowFlake = new SnowFlake(0,0);

    public String returnIdValue(){
        return snowFlake.nextId()+"";
    }

    public Long returnIdLong(){
        return snowFlake.nextId();
    }

    public static Long returnStaticIdLong(){
        return snowFlake.nextId();
    }
    public static String returnStaticIdString(){
        return snowFlake.nextId()+"";
    }
}
