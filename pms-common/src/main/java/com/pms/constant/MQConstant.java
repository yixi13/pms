package com.pms.constant;

/**
 * Created by ljb on 2017/11/15.
 * Rabbit消息队列相关常量
 * 每个消息队列一个模块调用，
 * 不然mq会自动进行负载处理
 */
public class MQConstant {
    private MQConstant(){
    }
    //exchange name
    public static final String DEFAULT_EXCHANGE = "PMS";

    //DLX QUEUE
    public static final String DEFAULT_DEAD_LETTER_QUEUE_NAME = "pms.dead.letter.queue";

    //DLX repeat QUEUE 死信转发队列
    public static final String DEFAULT_REPEAT_TRADE_QUEUE_NAME = "pms.repeat.trade.queue";


    //Hello 测试消息队列名称
    public static final String HELLO_QUEUE_NAME = "HELLO2";
    // 物业消息队列名称
    public static final String QUEUE_NAME_ESTATE = "ESTATE";
    // 支付消息队列名称
    public static final String QUEUE_NAME_PAY = "PAY";
    // IOT设备信息 从mqtt中转发rabbitMq中
    public static final String QUEUE_NAME_IOT_MQTT_STORAGE= "IOT_MQTT_STORAGE";
    // IOT设备故障信息 websocket推送 routingkey
    public static final String QUEUE_NAME_IOT_FAULT_SOCKET_PUSH= "IOT_FAULT_SOCKET_PUSH";
    // IOT设备故障信息 短信 推送 routingkey
    public static final String QUEUE_NAME_IOT_FAULT_SMS_PUSH= "IOT_FAULT_SMS_PUSH";

    // IOT设备信息 websocket推送 routingkey( 将设备信息推送前往折线图)
    public static final String QUEUE_NAME_IOT_EVERYDAY_SOCKET_PUSH= "IOT_EVERYDAY_SOCKET_PUSH";
}
