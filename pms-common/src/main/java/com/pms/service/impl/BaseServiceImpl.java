package com.pms.service.impl;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.baomidou.mybatisplus.mapper.Condition;
import com.baomidou.mybatisplus.mapper.SqlHelper;
import com.baomidou.mybatisplus.mapper.Wrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.pms.cache.annotation.BaseCache;
import com.pms.cache.annotation.BaseCacheClear;
import com.pms.cache.annotation.CacheClear;
import com.pms.service.IBaseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.io.Serializable;
import java.lang.reflect.ParameterizedType;
import java.util.List;
import java.util.Random;

/**
 * <p>
 * 基础服务实现类 - 基于缓存
 * T-基类，T：缓存前缀，基于基类
 * 默认 当前类  的服务，除查询外，默认清楚 T：缓存
 * </p>
 *
 * @author ASUS_B
 * @since 2017-10-26
 */
public class BaseServiceImpl<M extends BaseMapper<T>, T> extends ServiceImpl<M, T> implements IBaseService<T> {

    /*===============查询========================*/
    @BaseCache(key ="selectPage{1}{2.paramNameValuePairs}")
    public Page<T> selectPage(Page<T> page, Wrapper<T> wrapper) {
        SqlHelper.fillWrapper(page, wrapper);
        page.setRecords(baseMapper.selectPage(page, wrapper));
        return page;
    }
    @Override
    @BaseCache(key ="selectById{1}")
    public T selectById(Serializable id) {
        return baseMapper.selectById(id);
    }

    @Override
    @BaseCache(key ="selectList{1.paramNameValuePairs}")
    public List<T> selectList(Wrapper<T> wrapper) {
        return baseMapper.selectList(wrapper);
    }

    @Override
    @BaseCache(key ="selectOne{1.paramNameValuePairs}")
    public T selectOne(Wrapper<T> wrapper) {
        return SqlHelper.getObject(baseMapper.selectList(wrapper));
    }

    @Override
    @BaseCache(key ="selectPage{1}")
    public Page<T> selectPage(Page<T> page) {
        return selectPage(page, Condition.EMPTY);
    }
    /*===============插入========================*/
    @Override
    @Transactional
    @BaseCacheClear(pre=":")
    public boolean insert(T entity) {
        return retBool(baseMapper.insert(entity));
    }

    @Transactional
    @BaseCacheClear(pre=":")
    public boolean insertBatch(List<T> entityList) {
        return insertBatch(entityList, 30);
    }

    /*===============修改========================*/
    @Override
    @Transactional
    @BaseCacheClear(pre=":")
    public boolean updateById(T entity) {
        return retBool(baseMapper.updateById(entity));
    }

    @Override
    @Transactional
    @BaseCacheClear(pre=":")
    public boolean updateAllColumnById(T entity) {
        return retBool(baseMapper.updateAllColumnById(entity));
    }


    @Override
    @Transactional
    @BaseCacheClear(pre=":")
    public boolean update(T entity, Wrapper<T> wrapper) {
        return retBool(baseMapper.update(entity, wrapper));
    }

    @Override
    @Transactional
    @BaseCacheClear(pre=":")
    public boolean updateBatchById(List<T> entityList) {
        return updateBatchById(entityList, 30);
    }
    /*===============删除========================*/
    @Override
    @Transactional
    @BaseCacheClear(pre=":")
    public boolean deleteBatchIds(List<? extends Serializable> idList) {
        return retBool(baseMapper.deleteBatchIds(idList));
    }

    @Override
    @Transactional
    @BaseCacheClear(pre=":")
    public boolean deleteById(Serializable id) {
        return retBool(baseMapper.deleteById(id));
    }

    @Override
    @Transactional
    @BaseCacheClear(pre=":")
    public boolean delete(Wrapper<T> wrapper) {
        return retBool(baseMapper.delete(wrapper));
    }

    /**
     * 清除 基类T 实现类的 所有redis 缓存
     */
    @BaseCacheClear(pre=":")
    public void clearTCache(){}
    /**
     * 根据方法名称清除缓存,清楚的缓存键规则为：tName:methodName
     * tName=》基类名称(T.getClasssName)  methodName=》方法名称
     * eg：methodName=selectPage；pre= unitBuilding:selectPage;
     */
    @BaseCacheClear(pre="{1}")
    public void clearPreKeyBaseCache(String methodName){}

    /**
     *
     * @param preKey 缓存前缀，不支持 {1} 格式
     */
    @CacheClear(pre="{1}")
    public void clearPreKeyCache(String preKey){}
}
