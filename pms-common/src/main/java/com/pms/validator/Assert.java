package com.pms.validator;


import com.pms.exception.RRException;
import org.apache.commons.lang.StringUtils;

import java.util.Collections;
import java.util.List;

/**
 * 数据校验
 */
public abstract class Assert {

    public static void isBlank(String str, String message) {
        if (StringUtils.isBlank(str)) {
            throw new RRException(message);
        }
    }

    public static void isNull(Object object, String message) {
        if (object == null) {
            throw new RRException(message);
        }
    }

    public static void isEmpty(String str, String message) {
        if (StringUtils.isEmpty(str)) {
            throw new RRException(message);
        }
    }
    public static void parameterIsBlank(String str, String message) {
        if (StringUtils.isBlank(str)) {
            throw new RRException(message,400);
        }
    }

    public static void parameterIsNull(Object object, String message) {
        if (object == null) {
            throw new RRException(message,400);
        }
    }

    public static void parameterIsEmpty(String str, String message) {
        if (StringUtils.isEmpty(str)) {
            throw new RRException(message,400);
        }
    }
    public static void parameterIsNull(List<?> list, String message) {
        if (list==null) {
            throw new RRException(message,400);
        }
    }
    public static void parameterIsEmpty(List<?> list, String message) {
        if (list==null||list.isEmpty()) {
            throw new RRException(message,400);
        }
    }

    public static void isExcessLength(String str,int length ,String message) {
        if (str.length() >length){
            throw new RRException(message);
        }
    }
}
