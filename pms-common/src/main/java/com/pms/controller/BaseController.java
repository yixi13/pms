package com.pms.controller;

import com.pms.exception.RRException;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.Base64Utils;

import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.Map;

/**
 * @author zyl
 * @create 2017-10-24 14:50
 **/
public abstract class BaseController {
    @Autowired
    protected HttpServletRequest request;

    protected Logger logger = LoggerFactory.getLogger(getClass());

    /**
     * 得到用户名
     * @return
     */
    public String getCurrentUserName(){
//        String authorization = request.getHeader("Authorization");
//        return new String(Base64Utils.decodeFromString(authorization));
        String X_Name = request.getHeader("X-Name");
        return X_Name ;
    }
    /**
     * 得到用户token
     * @return
     */
    public String getCurrentToken(){
        String X_Token = request.getHeader("X-Token");
        return X_Token ;
    }
    public String getCurrentCompanyCode(){
        String X_Code = request.getHeader("X-Code");
        return X_Code ;
    }
    public Long getCurrentRoleId(){
        String roleId =  request.getHeader("X-groupId");
        return Long.valueOf(roleId);
    }
    public Integer getCurrentRoleType(){
        String roleType =  request.getHeader("X-groupType");
        return Integer.valueOf(roleType);
    }
    /**
     * 得到用户token
     * @return
     */
    public Map<String,String> getCurrentUserMap(){
        Map<String,String> currentUserMap = new HashMap<String,String>();
        String X_Token = request.getHeader("X-Token");
        currentUserMap.put("token",X_Token);
        String X_Name = request.getHeader("X-Name");
        currentUserMap.put("userName",X_Name);
        String X_Code = request.getHeader("X-Code");
        currentUserMap.put("companyCode",X_Code);
        String role_id = request.getHeader("X-groupId");
        currentUserMap.put("roleId",role_id);
        String role_type = request.getHeader("X-groupType");
        currentUserMap.put("roleType",role_type);
        return currentUserMap ;
    }
    public static void parameterIsBlank(String str, String message) {
        if (StringUtils.isBlank(str)) {
            throw new RRException(message,400);
        }
    }

    public static void parameterIsNull(Object object, String message) {
        if (object == null) {
            throw new RRException(message,400);
        }
    }

    public static void parameterIsEmpty(String str, String message) {
        if (StringUtils.isEmpty(str)) {
            throw new RRException(message,400);
        }
    }


}
