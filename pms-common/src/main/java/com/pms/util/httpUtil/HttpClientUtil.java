package com.pms.util.httpUtil;

import com.alibaba.fastjson.JSONObject;
import org.apache.commons.lang.StringUtils;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;
import java.util.List;
import java.util.Map;

/**
 * 请求工具类
 * com.alibaba.fastjson.JSONObject.parseObject 将null 装换为 null对象
 * net.sf.json.JSONObject.fromObject 将null装换为 "null"字符串
 */
public class HttpClientUtil {

  public static void main(String arg[]) throws Exception {

  }

  /**
   * http-->>post请求
   * @param url
   * @param json
   * @return
   */
  public static JSONObject doPost(String url, String json){
    DefaultHttpClient client = new DefaultHttpClient();
    HttpPost post = new HttpPost(url);
    JSONObject response = null;
    try {
      StringEntity s = new StringEntity(json.toString(),"utf-8");
      s.setContentType("application/json");//发送json数据需要设置contentType
      s.setContentType("application/x-www-form-urlencoded");
      post.setEntity(s);
      Long startTime =System.currentTimeMillis();
      HttpResponse res = client.execute(post);
      if(res.getStatusLine().getStatusCode() == HttpStatus.SC_OK){
        HttpEntity entity = res.getEntity();
        String result = EntityUtils.toString(res.getEntity());// 返回json格式：
//          response = JSONObject.fromObject(result);
        response = JSONObject.parseObject(result);
      }
    } catch (Exception e) {
      throw new RuntimeException(e);
    }
    return response;
  }
    /**
     * https -->> post请求
     * @param url
     * @param json
     * @return
     */
    @SuppressWarnings({ "unused", "resource" })
    public static JSONObject httpsDoPost(String url,String json){
        HttpClient client = null;
        JSONObject response = null;
        try {
            client= new SSLClient();
            HttpPost post = new HttpPost(url);
            post.setHeader("Content-Type","application/json");
            StringEntity s = new StringEntity(json.toString());
            s.setContentEncoding("UTF-8");
            s.setContentType("application/json");//发送json数据需要设置contentType
            post.setEntity(s);
            Long startTime =System.currentTimeMillis();
            HttpResponse res = client.execute(post);
            if(res.getStatusLine().getStatusCode() == HttpStatus.SC_OK){
                HttpEntity entity = res.getEntity();
                String result = EntityUtils.toString(res.getEntity());// 返回json格式：
                response = JSONObject.parseObject(result);
            }
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
        return response;
    }

    /**
     * 向指定URL发送GET方法的请求
     *
     * @param url
     * 发送请求的URL
     * @param param
     * 请求参数，请求参数应该是 name1=value1&name2=value2 的形式。
     * @return URL 所代表远程资源的响应结果
     */
    public static String doGet(String url, String param) {
        String result = "";
        BufferedReader in = null;
        try {
            String urlNameString =null;
            if(StringUtils.isBlank(param)){
                urlNameString = url;
            }else{
                urlNameString = url + "?" + param;
            }
            URL realUrl = new URL(urlNameString);
// 打开和URL之间的连接
            URLConnection connection = realUrl.openConnection();
// 设置通用的请求属性
            connection.setRequestProperty("accept", "*/*");
            connection.setRequestProperty("connection", "Keep-Alive");
            connection.setRequestProperty("user-agent",
                    "Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1;SV1)");
// 建立实际的连接
            connection.connect();
// 获取所有响应头字段
            Map<String, List<String>> map = connection.getHeaderFields();
// 遍历所有的响应头字段
// 定义 BufferedReader输入流来读取URL的响应
            in = new BufferedReader(new InputStreamReader(
                    connection.getInputStream()));
            String line;
            while ((line = in.readLine()) != null) {
                result += line;
            }
        } catch (Exception e) {
            System.out.println("发送GET请求出现异常！" + e);
            e.printStackTrace();
        }
// 使用finally块来关闭输入流
        finally {
            try {
                if (in != null) {
                    in.close();
                }
            } catch (Exception e2) {
                e2.printStackTrace();
            }
        }
        return result;
    }


}