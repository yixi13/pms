package com.pms.util;

import cn.jiguang.common.ClientConfig;
import cn.jiguang.common.resp.APIConnectionException;
import cn.jiguang.common.resp.APIRequestException;
import cn.jpush.api.JPushClient;
import cn.jpush.api.push.PushResult;
import cn.jpush.api.push.model.Options;
import cn.jpush.api.push.model.Platform;
import cn.jpush.api.push.model.PushPayload;
import cn.jpush.api.push.model.audience.Audience;
import cn.jpush.api.push.model.notification.AndroidNotification;
import cn.jpush.api.push.model.notification.IosNotification;
import cn.jpush.api.push.model.notification.Notification;

/**
 * 推送 极光
 */
public class JpushClientUtil {
	
	public static void main(String[] args) {
		String alias = "A000005691D15A";//声明别名
        System.out.println("对别名" + alias + "的用户推送信息");
        PushResult result = pushByAlias("1c5e2e3617e77a2bcdbc1a2b","67899054e0b97284c1ca0428",
        		String.valueOf(alias),"test",11);

        if(result != null && result.isResultOK()){
            System.out.println("针对别名" + alias + "的信息推送成功！");
        }else{
            System.out.println("针对别名" + alias + "的信息推送失败！");
        }
	}
    
    /**
     * 生成极光推送对象PushPayload（采用java SDK）
     * @param alias
     * @param alert
     * @return PushPayload
     */
    public static PushPayload buildPushObject_android_ios_alias_alert(String alias,String alert,Integer type){
        return PushPayload.newBuilder()
                .setPlatform(Platform.android_ios())
                .setAudience(Audience.alias(alias))
                .setNotification(Notification.newBuilder()
                        .addPlatformNotification(AndroidNotification.newBuilder()
                                .addExtra("type", "infomation")
                                .setAlert(alert)
                                .addExtra("msgType", type)
                                .build())
                        .addPlatformNotification(IosNotification.newBuilder()
                                .addExtra("type", "infomation")
                                .setAlert(alert)
                                .addExtra("msgType", type)
                                .build())
                        .build())
                .setOptions(Options.newBuilder()
                        .setApnsProduction(true)//true-推送生产环境 false-推送开发环境（测试使用参数）
                        .setTimeToLive(90)//消息在JPush服务器的失效时间（测试使用参数）
                        .build())
                .build();
    }
    
    /**
     * 生成极光推送对象PushPayload
     * @param
     * @param alert
     * @return PushPayload
     */
    public static PushPayload buildPushObject_android_ios_tag_alert(String[] tags, String alert, Integer type){
        return PushPayload.newBuilder()
                .setPlatform(Platform.android_ios())
                .setAudience(Audience.tag(tags))
                .setNotification(Notification.newBuilder()
                        .addPlatformNotification(AndroidNotification.newBuilder()
                                .addExtra("type", "infomation")
                                .setAlert(alert)
                                .addExtra("msgType", type)
                                .build())
                        .addPlatformNotification(IosNotification.newBuilder()
                                .addExtra("type", "infomation")
                                .setAlert(alert)
                                .addExtra("msgType", type)
                                .build())
                        .build())
                .setOptions(Options.newBuilder()
                        .setApnsProduction(true)//true-推送生产环境 false-推送开发环境（测试使用参数）
                        .setTimeToLive(90)//消息在JPush服务器的失效时间（测试使用参数）
                        .build())
                .build();
    }
    
    /**
     * 
     * @param appKey
     * @param masterSecret
     * @param alias
     * @param alert
     * @param msgType 		消息类别
     * @return
     */
    public static PushResult pushByAlias(String appKey,String masterSecret,String alias,String alert,Integer msgType){
        ClientConfig clientConfig = ClientConfig.getInstance();
        JPushClient jpushClient = new JPushClient(masterSecret, appKey, null, clientConfig);
        PushPayload payload = buildPushObject_android_ios_alias_alert(alias,alert,msgType);
        try {
        	PushResult result = jpushClient.sendPush(payload);
        	System.out.println(result);
            return result;
        } catch (APIConnectionException e) {
            System.out.println("Connection error. Should retry later. ");
            return null;
        } catch (APIRequestException e) {
            System.out.println("Error response from JPush server. Should review and fix it. ");
            System.out.println("HTTP Status: " + e.getStatus());
            System.out.println("Error Code: " + e.getErrorCode());
            System.out.println("Error Message: " + e.getErrorMessage());
            System.out.println("Msg ID: " + e.getMsgId());
            return null;
        }    
    }
    
    /**
     * 根据tag 推送信息
     * @param appKey
     * @param masterSecret
     * @param
     * @param alert
     * @param msgType
     * @return
     */
    public static PushResult pushByTag(String appKey,String masterSecret,String[] Tags,String alert,Integer msgType){
        ClientConfig clientConfig = ClientConfig.getInstance();
        JPushClient jpushClient = new JPushClient(masterSecret, appKey, null, clientConfig);
        PushPayload payload = buildPushObject_android_ios_tag_alert(Tags,alert,msgType);
        try {
        	PushResult result = jpushClient.sendPush(payload);
        	System.out.println(result);
            return result;
        } catch (APIConnectionException e) {
            System.out.println("Connection error. Should retry later. ");
            return null;
        } catch (APIRequestException e) {
            System.out.println("Error response from JPush server. Should review and fix it. ");
            System.out.println("HTTP Status: " + e.getStatus());
            System.out.println("Error Code: " + e.getErrorCode());
            System.out.println("Error Message: " + e.getErrorMessage());
            System.out.println("Msg ID: " + e.getMsgId());
            return null;
        }    
    }
    
    
}
