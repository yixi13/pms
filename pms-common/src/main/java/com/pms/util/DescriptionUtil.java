package com.pms.util;

import java.lang.reflect.Field;
import java.lang.reflect.ParameterizedType;

/**
 * Created by Administrator on 2017/10/23.
 */
public class DescriptionUtil {
    /**
     * 获取 cl 的注解注释内容
     * @param cl
     * @return
     */
    public static String getDescreptionValue(Class<?> cl){
        StringBuffer str = new StringBuffer("}");
        Field[] fields = cl.getDeclaredFields();
        for(Field f : fields){
            Description desc = f.getAnnotation(Description.class);
            if(null!=desc){
                str.append(f.getName()).append(":").append(desc.value()).append(",");

            }
        }
        str.append("}");
        return str.toString();
    }
}
