package com.pms.util;
/**
 *  编号设定的生成类型
 * @author xzq
 *
 */
public enum NumberManagerType {
	/**
	 * 序列号类型
	 */
	serialNumber,
	/**
	 * 时间随机数类型
	 */
	TimeNumber,
	/**
	 * 自动增长类型
	 */
	autoGrowNumber,
}
