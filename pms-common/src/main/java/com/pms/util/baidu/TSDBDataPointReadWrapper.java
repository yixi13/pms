package com.pms.util.baidu;

import com.pms.util.DateUtil;
import java.util.*;

/**
 * tsdb 查询数据点 工具类
 */
public abstract class TSDBDataPointReadWrapper<T> {

    // 度量-数据库名
    private String metric ;
    // 起始时间-时间戳
    private Long start;
    // 截止时间-时间戳
    private Long end;
    // 查询数量  默认 1000000
    private Integer limit = 1000000;
    // 设置要查询的字段
    private final List<String> readFieldList = new ArrayList<String>();
    // 设置要查询的 标签
    private final List<String> readTagList = new ArrayList<String>();
    // 设置 是否查询标签返回，默认查询true
    private boolean isReadTag = true;
    // 设置 是否查询字段返回，默认查询true
    private boolean isReadField = true;
    // 字段 过滤条件
    private final List<Map<String, String>> fieldList = new ArrayList<Map<String, String>>();
    // 标签 过滤条件
    private final List<Map<String, Object>> tagList = new ArrayList<Map<String, Object>>();
    // 标签 分组
    private final List<Map<String, Object>> groupByTagList = new ArrayList<Map<String, Object>>();
    // 聚合函数计算
    private final List<Map<String, Object>> aggregatorList = new ArrayList<Map<String, Object>>();
    // null数据补全
    private final List<Map<String, Object>> fillList = new ArrayList<Map<String, Object>>();
    /**
     * 用于分页查询，从marker开始返回，应使用上一次查询返回的nextMarker值
     */
    private String marker;


/*==============================设置 字段(field)过滤条件  end===========================*/
    /**
     * 基于 T 自动验证 fieldName 是否存在
     *  等于 自动拼接 =
     * @param fieldName
     * @param params
     * @return
     */
    public TSDBDataPointReadWrapper fieldEq(String fieldName, Object params) {
        if(fieldName == null||params==null){
            return this;
        }
        if (params instanceof String) {
            params = "'"+((String) params).trim()+"'";
        }
        Map<String,String> fieldMap = new HashMap<String,String>();
        fieldMap.put(TSDBConstant.FIELD,fieldName);
        fieldMap.put(TSDBConstant.VALUE,"= "+params);
        this.fieldList.add(fieldMap);
        return this;
    }

    /**
     * 基于 T 自动验证 fieldName 是否存在
     *  不等于
     * @param fieldName
     * @param params
     * @return
     */
    public TSDBDataPointReadWrapper fieldEqNon(String fieldName, Object params) {
        if(fieldName == null||params==null){
            return this;
        }
        if (params instanceof String) {
            params = "'"+((String) params).trim()+"'";
        }
        Map<String,String> fieldMap = new HashMap<String,String>();
        fieldMap.put(TSDBConstant.FIELD,fieldName);
        fieldMap.put(TSDBConstant.VALUE,"!= "+params);
        this.fieldList.add(fieldMap);
        return this;
    }

    /**
     * 基于 T 自动验证 fieldName 是否存在
     *  大于
     * @param fieldName
     * @param params
     * @return
     */
    public TSDBDataPointReadWrapper fieldGt(String fieldName, Object params) {
        if(fieldName == null||params==null){
            return this;
        }
        if (params instanceof String) {
            params = "'"+((String) params).trim()+"'";
        }
        Map<String,String> fieldMap = new HashMap<String,String>();
        fieldMap.put(TSDBConstant.FIELD,fieldName);
        fieldMap.put(TSDBConstant.VALUE,"> "+params);
        this.fieldList.add(fieldMap);
        return this;
    }
    /**
     * 基于 T 自动验证 fieldName 是否存在
     *  大于等于
     * @param fieldName
     * @param params
     * @return
     */
    public TSDBDataPointReadWrapper fieldGtEq(String fieldName, Object params) {
        if(fieldName == null||params==null){
            return this;
        }
        if (params instanceof String) {
            params = "'"+((String) params).trim()+"'";
        }
        Map<String,String> fieldMap = new HashMap<String,String>();
        fieldMap.put(TSDBConstant.FIELD,fieldName);
        fieldMap.put(TSDBConstant.VALUE,">= "+params);
        this.fieldList.add(fieldMap);
        return this;
    }

    /**
     * 基于 T 自动验证 fieldName 是否存在
     *  小于
     * @param fieldName
     * @param params
     * @return
     */
    public TSDBDataPointReadWrapper fieldLt(String fieldName, Object params) {
        if(fieldName == null||params==null){
            return this;
        }
        if (params instanceof String) {
            params = "'"+((String) params).trim()+"'";
        }
        Map<String,String> fieldMap = new HashMap<String,String>();
        fieldMap.put(TSDBConstant.FIELD,fieldName);
        fieldMap.put(TSDBConstant.VALUE,"< "+params);
        this.fieldList.add(fieldMap);
        return this;
    }
    /**
     * 基于 T 自动验证 fieldName 是否存在
     *  小于
     * @param fieldName
     * @param params
     * @return
     */
    public TSDBDataPointReadWrapper fieldLtEq(String fieldName, Object params) {
        if(fieldName == null||params==null){
            return this;
        }
        if (params instanceof String) {
            params = "'"+((String) params).trim()+"'";
        }
        Map<String,String> fieldMap = new HashMap<String,String>();
        fieldMap.put(TSDBConstant.FIELD,fieldName);
        fieldMap.put(TSDBConstant.VALUE,"<= "+params);
        this.fieldList.add(fieldMap);
        return this;
    }

  /*==============================设置 字段(field)过滤条件  end===========================*/
    /*==============================设置标签过滤条件  start===========================*/

    /**
     * 基于 T 自动验证 tagName 是否存在
     *  标签-模糊查询
     * @param tagName 标签名称
     * @param value 标签值
     * @return
     */
    public TSDBDataPointReadWrapper tagLike(String tagName, String value) {
        if(tagName == null||value==null){
            return this;
        }
        Map<String, Object> tagMap = new HashMap<String, Object>();
        tagMap.put(TSDBConstant.TAG,tagName);
        tagMap.put(TSDBConstant.LIKE,"%"+value+"%");
        this.tagList.add(tagMap);
        return this;
    }
    /**
     * 基于 T 自动验证 tagName 是否存在
     *  标签-in条件查询
     * @param tagName 标签名称
     * @param value 标签值,多个以,分开
     * @return
     */
    public TSDBDataPointReadWrapper tagIn(String tagName, String value) {
        if(tagName == null||value==null){
            return this;
        }
        StringTokenizer stringTokenizer = new StringTokenizer(value, ",");
        List<String> valueList = new ArrayList<String>();
        while (stringTokenizer.hasMoreTokens()) {
            valueList.add(stringTokenizer.nextToken());
        }
        return tagIn(tagName,valueList);
    }

    /**
     * 基于 T 自动验证 tagName 是否存在
     *  标签-in条件查询
     * @param tagName 标签名称
     * @param valueList 标签值集合
     * @return
     */
    public TSDBDataPointReadWrapper tagIn(String tagName, List<String> valueList) {
        if(tagName == null||valueList==null||valueList.isEmpty()){
            return this;
        }
        Map<String, Object> tagMap = new HashMap<String, Object>();
        tagMap.put(TSDBConstant.TAG,tagName);
        tagMap.put(TSDBConstant.IN,valueList);
        this.tagList.add(tagMap);
        return this;
    }

    /**
     * 基于 T 自动验证 tagName 是否存在
     *  标签-in条件查询
     * @param tagName 标签名称
     * @param value 标签值,多个以,分开
     * @return
     */
    public TSDBDataPointReadWrapper tagNotIn(String tagName, String value) {
        if(tagName == null||value==null){
            return this;
        }
        StringTokenizer stringTokenizer = new StringTokenizer(value, ",");
        List<String> valueList = new ArrayList<String>();
        while (stringTokenizer.hasMoreTokens()) {
            valueList.add(stringTokenizer.nextToken());
        }
        return tagNotIn(tagName,valueList);
    }

    /**
     * 基于 T 自动验证 tagName 是否存在
     *  标签-in条件查询
     * @param tagName 标签名称
     * @param valueList 标签值集合
     * @return
     */
    public TSDBDataPointReadWrapper tagNotIn(String tagName, List<String> valueList) {
        if(tagName == null||valueList==null||valueList.isEmpty()){
            return this;
        }
        Map<String, Object> tagMap = new HashMap<String, Object>();
        tagMap.put(TSDBConstant.TAG,tagName);
        tagMap.put(TSDBConstant.NOTIN,valueList);
        this.tagList.add(tagMap);
        return this;
    }

    /*==============================设置标签过滤条件  end===========================*/

/*==============================设置查询字段  start===========================*/
    /**
     * 基于 T 自动验证 field 是否存在
     * 设置要查询 字段
     * @param fieldNames 多个字段以,分割
     * @return
     */
    public TSDBDataPointReadWrapper setReadFieldList(String fieldNames) {
        if(fieldNames==null){
            return this;
        }
        StringTokenizer stringTokenizer = new StringTokenizer(fieldNames, ",");
        List<String> valueList = new ArrayList<String>();
        while (stringTokenizer.hasMoreTokens()) {
            valueList.add(stringTokenizer.nextToken());
        }
        return setReadFieldList(valueList);
    }

    /**
     * 基于 T 自动验证 field 是否存在
     * 设置要查询的字段
     * @param fieldNames
     * @return
     */
    public TSDBDataPointReadWrapper setReadFieldList(List<String> fieldNames) {
        if(fieldNames==null){
            return this;
        }
        this.readFieldList.addAll(fieldNames);
        return this;
    }
/*==============================设置查询字段  end===========================*/

/*==============================设置查询标签  start===========================*/
    /**
     * 基于 T 自动验证 标签 是否存在
     * 设置要查询 标签
     * @param tagNames 标签名称,多个标签以,分割
     * @return
     */
    public TSDBDataPointReadWrapper setReadTagList(String tagNames) {
        if(tagNames==null){
            return this;
        }
        StringTokenizer stringTokenizer = new StringTokenizer(tagNames, ",");
        List<String> valueList = new ArrayList<String>();
        while (stringTokenizer.hasMoreTokens()) {
            valueList.add(stringTokenizer.nextToken());
        }
        return setReadTagList(valueList);
    }

    /**
     * 基于 T 自动验证 标签 是否存在
     * 设置要查询的标签
     * @param tagNames
     * @return
     */
    public TSDBDataPointReadWrapper setReadTagList(List<String> tagNames) {
        if(tagNames==null){
            return this;
        }
        this.readTagList.addAll(tagNames);
        return this;
    }
/*==============================设置查询标签  end===========================*/


/*==============================根据标签分组  start===========================*/
    /**
     * 基于 T 自动验证 tag 是否存在
     *  根据 标签 tag 分组,
     * @param tags  多个以,分割
     * @return
     */
    public TSDBDataPointReadWrapper groupByTag(String tags) {
        if(tags == null){
            return this;
        }
        StringTokenizer stringTokenizer = new StringTokenizer(tags, ",");
        List<String> valueList = new ArrayList<String>();
        while (stringTokenizer.hasMoreTokens()) {
            valueList.add(stringTokenizer.nextToken());
        }
        return groupByTag(valueList);
    }
    /**
     * 基于 T 自动验证 tag 是否存在
     *  根据 标签 tag 分组,
     * @param tags
     * @return
     */
    public TSDBDataPointReadWrapper groupByTag(List<String> tags) {
        if(tags == null){
            return this;
        }
        List<String> T_tagList = getTTagNameList();
        tags.retainAll(T_tagList);// 取交集
        if(tags.isEmpty()){
            return this;
        }
        Map<String,Object> fieldMap = new HashMap<String,Object>();
        fieldMap.put(TSDBConstant.NAME,TSDBConstant.T_AG);
        fieldMap.put(TSDBConstant.TAG_S,tags);
        this.groupByTagList.add(fieldMap);
        return this;
    }
/*==============================根据标签分组  end===========================*/
 /*==============================聚合函数  start===========================*/


    /**
     * 聚合函数 采样时间
     * @param type 类型(1-年,2-月,3-日,4-时,5-分,6-秒)
     * @param num  值
     * @return
     */
    public String aggregator_sampling(Integer type,Integer num) {
        if(type==null||type<1||type>6){
            return null;
        }
        if(num==null||num<1){
            return null;
        }
        return num+" "+getTimeUnit(type);
    }

    public String getTimeUnit(int type){
        switch (type){
            case 1: return TSDBConstant.time_unit_year;
            case 2: return TSDBConstant.time_unit_month;
            case 3: return TSDBConstant.time_unit_day;
            case 4: return TSDBConstant.time_unit_hour;
            case 5: return TSDBConstant.time_unit_minute;
            case 6: return TSDBConstant.time_unit_second;
            default: return TSDBConstant.time_unit_day;
        }
    }
    /**
     * 聚合函数 求平均数
     * eg： type=1,num=7  表示 求每周(7天)为分割 的平均数
     * @param type 采样时间类型,为null 则根据全时间
     * @param num  数字
     * @return
     */
    public TSDBDataPointReadWrapper aggregator_Avg(Integer type,Integer num) {
        Map<String,Object> map = new HashMap<String,Object>();
        map.put(TSDBConstant.NAME,TSDBConstant.aggregators_AVG);
        String sampling = aggregator_sampling(type,num);
        if(sampling!=null){
            map.put(TSDBConstant.aggregators_Sampling,sampling);
        }
        this.aggregatorList.add(map);
        return this;
    }
    public TSDBDataPointReadWrapper aggregator_Avg() {
        return aggregator_Avg(null,null);
    }
    /**
     * 聚合函数 统计采样时间类 数目
     * eg： type=1,num=7  表示 求每周(7天)为分割 的平均数
     * @param type 采样时间类型,为null 则根据全时间
     * @param num  数字
     * @return
     */
    public TSDBDataPointReadWrapper aggregator_Count(Integer type,Integer num) {
        Map<String,Object> map = new HashMap<String,Object>();
        map.put(TSDBConstant.NAME,TSDBConstant.aggregators_COUNT);
        String sampling = aggregator_sampling(type,num);
        if(sampling!=null){
            map.put(TSDBConstant.aggregators_Sampling,sampling);
        }
        this.aggregatorList.add(map);
        return this;
    }
    public TSDBDataPointReadWrapper aggregator_Count() {
        return aggregator_Count(null,null);
    }

    /**
     * 获取第一个值
     * eg： type=1,num=7  表示 求每周(7天)为分割 的平均数
     * @param type 采样时间类型,为null ,则根据全时间
     * @param num  数字
     * @return
     */
    public TSDBDataPointReadWrapper aggregator_First(Integer type,Integer num) {
        Map<String,Object> map = new HashMap<String,Object>();
        map.put(TSDBConstant.NAME,TSDBConstant.aggregators_FIRST);
        String sampling = aggregator_sampling(type,num);
        if(sampling!=null){
            map.put(TSDBConstant.aggregators_Sampling,sampling);
        }
        this.aggregatorList.add(map);
        return this;
    }
    public TSDBDataPointReadWrapper aggregator_First() {
        return aggregator_First(null,null);
    }
    /**
     * 获取最后一个值
     * eg： type=1,num=7  表示 求每周(7天)为分割 的平均数
     * @param type 采样时间类型,为null ,则根据全时间
     * @param num  数字
     * @return
     */
    public TSDBDataPointReadWrapper aggregator_Last(Integer type,Integer num) {
        Map<String,Object> map = new HashMap<String,Object>();
        map.put(TSDBConstant.NAME,TSDBConstant.aggregators_LAST);
        String sampling = aggregator_sampling(type,num);
        if(sampling!=null){
            map.put(TSDBConstant.aggregators_Sampling,sampling);
        }
        this.aggregatorList.add(map);
        return this;
    }
    public TSDBDataPointReadWrapper aggregator_Last() {
        return aggregator_Last(null,null);
    }

    /**
     * 获取最大值
     * eg： type=1,num=7  表示 求每周(7天)为分割 的平均数
     * @param type 采样时间类型,为null ,则根据全时间
     * @param num  数字
     * @return
     */
    public TSDBDataPointReadWrapper aggregator_Max(Integer type,Integer num) {
        Map<String,Object> map = new HashMap<String,Object>();
        map.put(TSDBConstant.NAME,TSDBConstant.aggregators_MAX);
        String sampling = aggregator_sampling(type,num);
        if(sampling!=null){
            map.put(TSDBConstant.aggregators_Sampling,sampling);
        }
        this.aggregatorList.add(map);
        return this;
    }
    public TSDBDataPointReadWrapper aggregator_Max() {
        return aggregator_Max(null,null);
    }
    /**
     * 获取最小值
     * eg： type=1,num=7  表示 求每周(7天)为分割 的平均数
     * @param type 采样时间类型,为null ,则根据全时间
     * @param num  数字
     * @return
     */
    public TSDBDataPointReadWrapper aggregator_Min(Integer type,Integer num) {
        Map<String,Object> map = new HashMap<String,Object>();
        map.put(TSDBConstant.NAME,TSDBConstant.aggregators_MIN);
        String sampling = aggregator_sampling(type,num);
        if(sampling!=null){
            map.put(TSDBConstant.aggregators_Sampling,sampling);
        }
        this.aggregatorList.add(map);
        return this;
    }
    public TSDBDataPointReadWrapper aggregator_Min() {
        return aggregator_Min(null,null);
    }
    /**
     * 求和
     * eg： type=1,num=7  表示 求每周(7天)为分割 的平均数
     * @param type 采样时间类型,为null ,则根据全时间
     * @param num  数字
     * @return
     */
    public TSDBDataPointReadWrapper aggregator_Sum(Integer type,Integer num) {
        Map<String,Object> map = new HashMap<String,Object>();
        map.put(TSDBConstant.NAME,TSDBConstant.aggregators_SUM);
        String sampling = aggregator_sampling(type,num);
        if(sampling!=null){
            map.put(TSDBConstant.aggregators_Sampling,sampling);
        }
        this.aggregatorList.add(map);
        return this;
    }
    public TSDBDataPointReadWrapper aggregator_Sum() {
        return aggregator_Sum(null,null);
    }

    /**
     * 求差
     * eg： type=1,num=7  表示 求每周(7天)为分割 的平均数
     * @param type 采样时间类型,为null ,则根据全时间
     * @param num  数字
     * @return
     */
    public TSDBDataPointReadWrapper aggregator_Diff(Integer type,Integer num) {
        Map<String,Object> map = new HashMap<String,Object>();
        map.put(TSDBConstant.NAME,TSDBConstant.aggregators_DIFF);
        String sampling = aggregator_sampling(type,num);
        if(sampling!=null){
            map.put(TSDBConstant.aggregators_Sampling,sampling);
        }
        this.aggregatorList.add(map);
        return this;
    }
    public TSDBDataPointReadWrapper aggregator_Diff() {
        return aggregator_Diff(null,null);
    }

    /**
     * 求倍数
     * 以每个value乘以一个倍数作为结果
     * @param factor 倍数
     * @return
     */
    public TSDBDataPointReadWrapper aggregator_Scale(double factor) {
        Map<String,Object> map = new HashMap<String,Object>();
        map.put(TSDBConstant.NAME,TSDBConstant.aggregators_Scale);
        map.put(TSDBConstant.aggregators_factor,factor);
        this.aggregatorList.add(map);
        return this;
    }

    /**
     * 相邻值去重
     * 相邻值去重，相同的值只保留第一个，非相邻的值不去重。譬如“1, 1, 2, 2, 2, 1, 1, 3”，相邻值去重后的结果为“1, 2, 1, 3”
     * @return
     */
    public TSDBDataPointReadWrapper aggregator_AdjacentUnique() {
        Map<String,Object> map = new HashMap<String,Object>();
        map.put(TSDBConstant.NAME,TSDBConstant.aggregators_AdjacentUnique);
        this.aggregatorList.add(map);
        return this;
    }
    /**
     *  变化率，以每两个相邻的value在单位时间（见参数timeUnit）的变化率作为结果
     * @return
     */
    public TSDBDataPointReadWrapper aggregator_Rate() {
        Map<String,Object> map = new HashMap<String,Object>();
        map.put(TSDBConstant.NAME,TSDBConstant.aggregators_Rate);
        this.aggregatorList.add(map);
        return this;
    }

    /**
     * 补充 聚合函数
     * @return
     */
    public TSDBDataPointReadWrapper add_aggregator(Map<String,Object> aggregator) {
        this.aggregatorList.add(aggregator);
        return this;
    }

    /*==============================聚合函数  end===========================*/

/* ======= =====================null数据补值 start ================================*/

    /**
     * 线性补值
     * @param field 需要补值的字段，null表示全字段补值
     * @return
     */
    public TSDBDataPointReadWrapper fill_Linear(String field) {
        Map<String,Object> fillMap = new HashMap<String,Object>();
        fillMap.put("type","Linear");
        fillMap.put("interval","1 " +TSDBConstant.time_unit_hour);// 默认1小时间隔时间
        if(field!=null){
            fillMap.put("field",field);
        }
        this.fillList.add(fillMap);
        return this;
    }

    /**
     * 按前一个值 补值
     * @param field 需要补值的字段，null表示全字段补值
     * @return
     */
    public TSDBDataPointReadWrapper fill_Previous(String field) {
        Map<String,Object> fillMap = new HashMap<String,Object>();
        fillMap.put("type","Previous");
        fillMap.put("interval","1 " +TSDBConstant.time_unit_hour);// 默认1小时间隔时间
        if(field!=null){
            fillMap.put("field",field);
        }
        this.fillList.add(fillMap);
        return this;
    }

    /**
     * 固定值 补值
     * @param field 需要补值的字段，null表示全字段补值
     * @param value 固定值
     * @return
     */
    public TSDBDataPointReadWrapper fill_Fixed(Object value,String field) {
        if(value==null){
            return this;
        }
        Map<String,Object> fillMap = new HashMap<String,Object>();
        fillMap.put("type","Fixed");
        fillMap.put("interval","1 " +TSDBConstant.time_unit_hour);// 默认1小时间隔时间
        fillMap.put("value",value);
        if(field!=null){
            fillMap.put("field",field);
        }
        this.fillList.add(fillMap);
        return this;
    }
/* ======= =====================null数据补值 end ================================*/
    public String getMetric() {
        if(this.metric==null){
            this.metric = getTMetric();
        }
        return metric;
    }

    public List<Map<String, Object>> getGroupByTagList() {
        return groupByTagList;
    }

    public List<Map<String, Object>> getFillList() {
        return fillList;
    }

    public void setMetric(String metric) {
        this.metric = metric;
    }

    public Long getStart() {
        return start;
    }

    public void setStart(Long start) {
        this.start = start;
    }
    public void setStartDate(Date startDate) {
        this.start = startDate.getTime();
    }

    public Long getEnd() {
        return end;
    }

    public void setEnd(Long end) {
        this.end = end;
    }

    public List<Map<String, Object>> getAggregatorList() {
        return aggregatorList;
    }

    public List<Map<String, String>> getFieldList() {
        Map<String,Integer> fieldNameMap = getTFieldNameMap();
        for(int x=0,len=fieldList.size();x<len;x++){
            if( fieldNameMap.get(fieldList.get(x).get(TSDBConstant.FIELD)) ==null ){
                fieldList.remove(x);--x;--len;
            }
        }
        return fieldList;
    }

    public List<Map<String, Object>> getTagList() {
        Map<String,Integer> tagNameMap = getTTagNameMap();
        for(int x=0,len=tagList.size();x<len;x++){
            if( tagNameMap.get(tagList.get(x).get(TSDBConstant.TAG)) ==null ){
                tagList.remove(x);--x;--len;
            }
        }
        return tagList;
    }

    public List<String> getReadFieldList() {
        List<String> allField = getTFieldNameList();
        if(this.readFieldList.isEmpty()){
            return allField;
        }
        this.readFieldList.retainAll(allField);
        return this.readFieldList;
    }
    public List<String> getReadTagList() {
        List<String> allTag = getTTagNameList();
        if(this.readTagList.isEmpty()){
            return allTag;
        }
        this.readTagList.retainAll(allTag);
        return this.readTagList;
    }
    public String getMarker() {
        return marker;
    }

    public void setMarker(String marker) {
        this.marker = marker;
    }

    public void setIsReadTag(boolean readTag) {
        this.isReadTag =readTag;
    }
    public boolean getIsReadTag() {
        return isReadTag;
    }

    public Integer getLimit() {
        return limit;
    }

    public boolean getIsReadField() {
        return isReadField;
    }

    public void setIsReadField(boolean readField) {
        isReadField = readField;
    }

    public void setLimit(Integer limit) {
        if(limit==null||limit<1||limit>1000000){
            limit = 1000000;
        }
        this.limit = limit;
    }

    /**
     * 获取 单个查询对象
     * @return
     */
    public abstract Map<String,Object> getQueryJson();

    /**
     * 获取完整的 查询数据点参数，单个查询对象
     * @return
     */
    public abstract Map<String,Object> getParamMap();

    /**
     * 获取泛型T实例
     * @return
     */
    public abstract  Class<T> getEntityClass();
    /**
     * 获取泛型T实例 中 度量名称
     * @return
     */
    public abstract String getTMetric();

    /**
     * 获取泛型T实例 中 Field字段集合
     * @return
     */
    public abstract List<String> getTFieldNameList();
    /**
     * 获取泛型T实例 中 Field字段集合
     * @return
     */
    public abstract Map<String,Integer> getTFieldNameMap();
    /**
     * 获取泛型T实例 中 tag标签名称集合
     * @return
     */
    public abstract List<String> getTTagNameList();
    /**
     * 获取泛型T实例 中 tag标签名称集合
     * @return
     */
    public abstract Map<String,Integer>  getTTagNameMap();
}
