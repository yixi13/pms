package com.pms.util.baidu;

import java.util.List;

/**
 * Created by Administrator on 2018/10/24.
 */
public class TSDBConstant {
    /**
     * 度量
     */
    public static final String METRIC ="metric";

    /**
     * 单域
     */
    public static final String FIELD ="field";

    /**
     * 多域
     */
    public static final String FIELD_S ="fields";

    /**
     * 过滤器参数
     */
    public static final String FILTER_S ="filters";

    /**
     * 截止时间
     */
    public static final String END ="end";

    /**
     * 起始时间
     */
    public static final String START ="start";

    /**
     * 值
     */
    public static final String VALUE ="value";

    /**
     * 标签
     */
    public static final String TAG ="tag";

    /**
     * 标签
     */
    public static final String T_AG ="Tag";

    public static final String TAG_S ="tags";

    /**
     * IN
     */
    public static final String IN ="in";
    /**
     * NOTIN
     */
    public static final String NOTIN ="notin";
    /**
     * LIKE
     */
    public static final String LIKE ="like";

    /**
     * queries
     */
    public static final String QUERIES ="queries";

    /**
     * 分组参数
     */
    public static final String GROUPBY ="groupBy";

    /**
     * 分组参数 (名称)
     */
    public static final String NAME ="name";

    /**
     * 聚合函数
     */
    public static final String aggregators ="aggregators";

    /**
     * 聚合函数-求平均数
     */
    public static final String aggregators_AVG ="Avg";
    /**
     * 聚合函数-求统计条数
     */
    public static final String aggregators_COUNT ="Count";
    /**
     * 聚合函数-求第一条数据
     */
    public static final String aggregators_FIRST ="First";

    /**
     * 聚合函数-求最后一条数据
     */
    public static final String aggregators_LAST="Last";
    /**
     * 聚合函数-查询最大值
     */
    public static final String aggregators_MAX="Max";

    /**
     * 聚合函数-查询最小值
     */
    public static final String aggregators_MIN="Min";
    /**
     * 聚合函数-求和
     */
    public static final String aggregators_SUM="Sum";
    /**
     * 聚合函数-求差
     */
    public static final String aggregators_DIFF="Diff";
    /**
     * 聚合函数-求平均数
     */
    public static final String  aggregators_Sampling ="sampling";

    /**
     * 聚合函数-相邻值去重
     */
    public static final String  aggregators_AdjacentUnique="AdjacentUnique";
    /**
     * 聚合函数-变化率
     */
    public static final String  aggregators_Rate="Rate";
    /**
     * 聚合函数-求倍数
     */
    public static final String  aggregators_Scale="Scale";
    /**
     * 聚合函数参数-倍数
     */
    public static final String  aggregators_factor="factor";

    /**
     * 秒
     */
    public static final String time_unit_second="sc";

    /**
     * 分
     */
    public static final String time_unit_minute="mc";

    /**
     * 时
     */
    public static final String time_unit_hour="hc";
    /**
     * 日
     */
    public static final String time_unit_day="dc";

    /**
     * 月
     */
    public static final String time_unit_month="nc";

    /**
     * 年
     */
    public static final String time_unit_year="yc";
}
