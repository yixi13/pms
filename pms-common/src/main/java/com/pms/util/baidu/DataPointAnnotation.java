package com.pms.util.baidu;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * TSDB 写入数据点 注解
 */
@Target(value = {ElementType.FIELD,ElementType.TYPE}) //该注解可以用于 字段或类 上
@Retention(RetentionPolicy.RUNTIME)//运行时 保留注解，可以通过反射机制读取注解的信息
public  @interface DataPointAnnotation {

    /**
     * 名称
     * @return
     */
    public String name() default "";
    /**
     * 类型(1-表(度量)名,2-字段(filed)名,3-标签(tag),4-时间戳)
     * @return
     */
    public int type() default 1;
}
