package com.pms.util.baidu;

import org.apache.commons.lang3.StringUtils;

import java.lang.reflect.Field;
import java.util.*;

/**
 * Created by Administrator on 2018/10/24.
 */
public class TSDBDataPointReadWrapperImpl<T> extends TSDBDataPointReadWrapper<T> {

    protected Class<T> entityClass;

    /**
     * @param tclass
     */
    public TSDBDataPointReadWrapperImpl(Class<T> tclass) {
        entityClass = tclass;
    }
    /**
     * @param tclass
     */
    public TSDBDataPointReadWrapperImpl(Class<T> tclass,Date startDate) {
        entityClass = tclass;
        super.setStartDate(startDate);
    }
    @Override
    public Class<T> getEntityClass() {
        return  entityClass;
    }

    /**
     * 获取泛型T实例 中 度量名称
     * @return
     */
    public String getTMetric(){
        String metricName = null;
        Class<?> tClass =getEntityClass();
        DataPointAnnotation metric = tClass.getAnnotation(DataPointAnnotation.class);
        if(metric==null){
            metricName = tClass.getName();
        }
        if(metric!=null){
            metricName = metric.name();
            if(metricName==null||metricName==""){
                metricName = tClass.getName();
            }
        }
        return  metricName;
    }
    /**
     * 获取泛型T实例 中 Field字段集合
     * @return
     */
    public List<String> getTFieldNameList(){
        List<String> fieldList = new ArrayList<String>();
        Class<?> tClass =getEntityClass();
        Field[] fields = tClass.getDeclaredFields();
        for (Field f : fields) {
            // 获取字段的注解
            DataPointAnnotation annotation = f.getAnnotation(DataPointAnnotation.class);
            if(null!=annotation){
                if(annotation.type()==2){// 2-字段(filed)名
                    String fieldName = annotation.name();
                    if(StringUtils.isBlank(fieldName)){
                        fieldName = f.getName();
                    }
                    fieldList.add(fieldName);
                }
            }
        }
        return  fieldList;
    }
    /**
     * 获取泛型T实例 中 Field字段集合
     * @return
     */
    public Map<String,Integer> getTFieldNameMap(){
        Map<String,Integer> fieldMap = new HashMap<String,Integer>();
        Class<?> tClass =getEntityClass();
        Field[] fields = tClass.getDeclaredFields();
        for (Field f : fields) {
            // 获取字段的注解
            DataPointAnnotation annotation = f.getAnnotation(DataPointAnnotation.class);
            if(null!=annotation){
                if(annotation.type()==2){// 2-字段(filed)名
                    String fieldName = annotation.name();
                    if(StringUtils.isBlank(fieldName)){
                        fieldName = f.getName();
                    }
                    fieldMap.put(fieldName,0);
                }
            }
        }
        return  fieldMap;
    }
    /**
     * 获取泛型T实例 中 tag标签名称集合
     * @return
     */
    public List<String> getTTagNameList(){
        List<String> tagNameList = new ArrayList<String>();
        Class<?> tClass =getEntityClass();
        Field[] fields = tClass.getDeclaredFields();
        for (Field f : fields) {
            // 获取字段的注解
            DataPointAnnotation annotation = f.getAnnotation(DataPointAnnotation.class);
            if(null!=annotation){
                if(annotation.type()==3){// 2-字段(filed)名
                    String tagName = annotation.name();
                    if(StringUtils.isBlank(tagName)){
                        tagName = f.getName();
                    }
                    tagNameList.add(tagName);
                }
            }
        }
        return  tagNameList;
    }
    /**
     * 获取泛型T实例 中 tag标签名称集合
     * @return
     */
    public Map<String,Integer>  getTTagNameMap(){
        Map<String,Integer>  tagNameMap = new HashMap<String,Integer>();
        Class<?> tClass =getEntityClass();
        Field[] fields = tClass.getDeclaredFields();
        for (Field f : fields) {
            // 获取字段的注解
            DataPointAnnotation annotation = f.getAnnotation(DataPointAnnotation.class);
            if(null!=annotation){
                if(annotation.type()==3){// 2-字段(filed)名
                    String tagName = annotation.name();
                    if(StringUtils.isBlank(tagName)){
                        tagName = f.getName();
                    }
                    tagNameMap.put(tagName,0);
                }
            }
        }
        return tagNameMap;
    }
    public Map<String,Object> getQueryJson(){
        Map<String,Object> query = new HashMap<String,Object>();
        Map<String,Object> filterMap = new HashMap<String,Object>();
        filterMap.put(TSDBConstant.START,getStart());// 起始时间
        filterMap.put(TSDBConstant.END,this.getEnd());// 截止时间
        filterMap.put(TSDBConstant.FIELD_S,this.getFieldList()); // 字段条件过滤
        filterMap.put(TSDBConstant.TAG_S,this.getTagList());// 标签条件过滤
        query.put(TSDBConstant.FILTER_S,filterMap);
        // 度量
        query.put(TSDBConstant.METRIC,this.getMetric());
        // 查询字段
        if(getIsReadField()) {
            query.put(TSDBConstant.FIELD_S, this.getReadFieldList());
        }
        // 查询标签
        if(getIsReadTag()){
            query.put(TSDBConstant.TAG_S,this.getReadTagList());
        }
        // 聚合函数
        query.put(TSDBConstant.aggregators,getAggregatorList());
        // null值补值
        query.put("fills",getFillList());
        //分组
        query.put(TSDBConstant.GROUPBY,this.getGroupByTagList());
        return query;
    }

    public Map<String,Object> getParamMap(){
        Map<String,Object> paramMap = new HashMap<String,Object>();
        List<Map<String,Object>> queries = new ArrayList<Map<String,Object>>();
        queries.add(getQueryJson());
        paramMap.put(TSDBConstant.QUERIES,queries);
        return paramMap;
    }
}
