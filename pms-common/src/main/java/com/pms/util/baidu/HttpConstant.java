package com.pms.util.baidu;

/**
 * tsdb  请求路径
 */
public class HttpConstant {

    /**
     * Tsdb写入数据点
     */
    public static final String writeDataPoint_TSDB ="/v1/datapoint";

    /**
     * Tsdb 查询数据点
     */
    public static final String readDataPoint_TSDB ="/v1/datapoint?query";
    /**
     * Tsdb 查询数据点
     */
    public static final String readDataPoint_TSDB_sql ="/v1/row";

}
