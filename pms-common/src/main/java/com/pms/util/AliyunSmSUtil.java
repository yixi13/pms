package com.pms.util;

import com.alibaba.fastjson.JSONObject;
import com.aliyuncs.DefaultAcsClient;
import com.aliyuncs.IAcsClient;
import com.aliyuncs.dysmsapi.model.v20170525.SendSmsRequest;
import com.aliyuncs.dysmsapi.model.v20170525.SendSmsResponse;
import com.aliyuncs.exceptions.ClientException;
import com.aliyuncs.profile.DefaultProfile;
import com.aliyuncs.profile.IClientProfile;

import java.util.Random;

/**
 * zpf
 * Created by hm on 2017/11/29.
 */
public class AliyunSmSUtil {

//    private static final String appkey  = "LTAIveMr8nLjAomt";
//    private static final String secret  = "p4MfZNsxyeFbEqBcdHYwkANDBM99Pf";
//    private static final String url  = "http://gw.api.taobao.com/router/rest";
//
//
//    public static void taobaoSendMoblieMessage(String userId, String tel, String vcode, String type) {
//
//        TaobaoClient client = new DefaultTaobaoClient(url, appkey, secret);
//
//        AlibabaAliqinFcSmsNumSendRequest req = new AlibabaAliqinFcSmsNumSendRequest();
//
////        String json = "{\"code\":\"" + vcode + "\",\"product\":\"我的平台名称\"}";
//
//        req.setExtend(userId); // 公共回传参数
//        req.setSmsType("normal"); // 短信类型
//        req.setSmsFreeSignName("注册测试"); // 短信签名
////        req.setSmsParamString(json); // 短信模板变量，传参规则{"key":"value"}，key的名字须和申请模板中的变量名一致，多个变量之间以逗号隔开。示例：针对模板“验证码${code}，您正在进行${product}身份验证，打死不要告诉别人哦！”，传参时需传入{"code":"1234","product":"alidayu"}
//        req.setRecNum(tel); // 短信接收号码，群发短信需传入多个号码，以英文逗号分隔 如:1390000000,1380000000
//        req.setSmsTemplateCode("SMS_114605028"); // 短信模板ID，传入的模板必须是在阿里大于“管理中心-短信模板管理”中的可用模板
//
//        AlibabaAliqinFcSmsNumSendResponse rsp;
//        try {
//            rsp = client.execute(req);
//            System.out.println(rsp.getBody());
//        } catch (ApiException e) {
//            e.printStackTrace();
//        }
//    }
//
//public static  void  main(String[] args){
//
//        taobaoSendMoblieMessage( "1","17323086205","123456","1");
//}



    //产品名称:云通信短信API产品,开发者无需替换
    static final String product = "Dysmsapi";
    //产品域名,开发者无需替换
    static final String domain = "dysmsapi.aliyuncs.com";
    static final String accessKeyId = "LTAIveMr8nLjAomt";
    static final String accessKeySecret = "p4MfZNsxyeFbEqBcdHYwkANDBM99Pf";


    /**
     * 短信信息发送
     * @param phone
     * @param type
     * @return
     */
    public static SendSmsResponse sendSms(String phone,String type){
        String code=getRandomNumCode();
        String content = getContent(Integer.parseInt(type));
        //可自助调整超时时间
        System.setProperty("sun.net.client.defaultConnectTimeout", "10000");
        System.setProperty("sun.net.client.defaultReadTimeout", "10000");
        //初始化acsClient,暂不支持region化
        IClientProfile profile = DefaultProfile.getProfile("cn-hangzhou", accessKeyId, accessKeySecret);
        try {
            DefaultProfile.addEndpoint("cn-hangzhou", "cn-hangzhou", product, domain);
        } catch (ClientException e) {
            e.printStackTrace();
        }
        IAcsClient acsClient = new DefaultAcsClient(profile);
        //组装请求对象-具体描述见控制台-文档部分内容
        SendSmsRequest request = new SendSmsRequest();
        //必填:待发送手机号
        request.setPhoneNumbers(phone);
        //必填:短信签名-可在短信控制台中找到
        request.setSignName("阿里云短信测试专用");
        //必填:短信模板-可在短信控制台中找到
        request.setTemplateCode(content);
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("code", code);
        request.setTemplateParam(jsonObject.toJSONString());
        //可选:outId为提供给业务方扩展字段,最终在短信回执消息中将此值带回给调用者
        request.setOutId("yourOutId");
        //hint 此处可能会抛出异常，注意catch
        SendSmsResponse sendSmsResponse = null;
        try {
            sendSmsResponse = acsClient.getAcsResponse(request);
        } catch (ClientException e) {
            e.printStackTrace();
        }
        return sendSmsResponse;
    }


    public static void main(String[] args) throws ClientException, InterruptedException {
        //发短信
        SendSmsResponse response =sendSms("13340745296","6");
        System.out.println("短信接口返回的数据----------------");
        System.out.println("Code=" + response.getCode());
        System.out.println("Message=" + response.getMessage());
        System.out.println("RequestId=" + response.getRequestId());
        System.out.println("BizId=" + response.getBizId());
    }


    /**
     * 得到验证码消息文本
     * @param type 1-注册 2-身份验证 3-修改密码 4-修改预留手机号码
     * @param
     * @return
     */
    public static String getContent(int type){
        String zhuc="SMS_114605024";//注册1
        String sf="SMS_114605028";//身份认证2
        String updatePswd="SMS_114605023";//修改密码3
        String updatePhone="SMS_114605022";//修改重要信息4
        String login= "SMS_114605026";//登录短信5
        String loginError= "SMS_114605025";//登录异常信息配置6
        String reString="";
        switch (type) {
            case 1:
                reString=zhuc;
                break;
            case 2:
                reString=sf;
                break;
            case 3:
                reString=updatePswd;
                break;
            case 4:
                reString=updatePhone;
                break;
            case 5:
                reString=login;
                break;
            case 6:
                reString=loginError;
                break;
            default: reString=sf;
                break;
        }
        return reString;
    }

    /**
     * 获取指定长度随机数字串
     * @param lens 长度，默认6
     * @return 指定长度随机数字串
     * @date 2016年1月27日 下午2:18:09
     */

    /**
     * 获取指定长度随机数字串
     * @param
     * @return
     */
    public static String getRandomNumCode() {
        String r = "123456789";
        StringBuffer sb=new StringBuffer();
        Random random=new Random();
        int len = 6;
        for (int i = 0; i < len; i++) {
            int cd = random.nextInt(9);
            sb.append(r.charAt(cd));
        }
        return sb.toString();
    }

}
