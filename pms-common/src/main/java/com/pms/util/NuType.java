package com.pms.util;

/**
 * 对象
 * 
 * @author zyl
 *
 */
public enum NuType  {
	newOrder,//订单
	product,//产品编号
	member, //会员号
	merchants,//商户账号
	payNo,//订单支付流水
	payUserNo,//支付系统用户编码
	payAccountNo,//支付系统用户钱包编码
	refundNu,//退款流水
	returnGoods, //退货
	img,//图片
	billNu,//账单编号
	mp4;//视频
}
