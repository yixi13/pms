package com.pms.util;
import javax.servlet.http.HttpServletRequest;

/**
 * @author zyl
 * @create 2017-07-12 18:18
 **/
public class IPUtils {
    /**
     * 获取客户端真实ip
     * @param request
     * @return
     */
    public static String getIp(HttpServletRequest request){
        String ip = request.getHeader("x-forwarded-for");
        if (ip==null||ip.length()==0||"unknown".equalsIgnoreCase(ip))
            ip = request.getHeader("Proxy-Client-IP");
        if (ip==null||ip.length()==0||"unknown".equalsIgnoreCase(ip))
            ip = request.getHeader("WL-Proxy-Client-IP");
        if (ip==null||ip.length()==0||"unknown".equalsIgnoreCase(ip))
            ip = request.getRemoteAddr();
        return ip;
    }
}
