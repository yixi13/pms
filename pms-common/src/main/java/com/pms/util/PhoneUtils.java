package com.pms.util;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * 电话号码验证
 */
public  class PhoneUtils {
    private static Logger logger = LoggerFactory.getLogger(PhoneUtils.class);

    /**
     * 验证号码是否为手机号码格式
     */
    public static Boolean isPhone(String phone) {
        Pattern parttert = Pattern.compile("^[1][3-9]\\d{9}$|^([6|9])\\d{7}$|^[0][9]\\d{8}$|^[6]([8|6])\\d{5}$|^(\\+?00886\\-?|0)?9\\d{8}$|^(\\+?00852\\-?)?[569]\\d{3}\\-?\\d{4}$|^(\\+?00853\\-?)\\d{8}$");//中国大陆港澳台
        Matcher matcher = parttert.matcher(phone.trim());
        if (matcher.matches()) {
            return true;
        }
        return false;
    }

    /**
     * 验证号码是否为手机号码格式
     */
    public static Boolean isTelPhone(String telPhone) {
        Pattern telephones = Pattern.compile("^(0\\d{2,3}-\\d{7,8}(-\\d{3,5}){0,1})$");
        Matcher matcher = telephones.matcher(telPhone.trim());
        if (matcher.matches()) {
            return true;
        }
        return false;
    }
    /**
     * 验证号码是否为手机号码格式
     */
    public static Boolean isTelPhone_(String telPhone) {
        Pattern telephones = Pattern.compile("^(0\\\\d{2,3}(-)?\\\\d{7,8}((-)?\\\\d{3,5}){0,1})$");
        Matcher matcher = telephones.matcher(telPhone.trim());
        if (matcher.matches()) {
            return true;
        }
        return false;
    }

    /**
     * 屏蔽手机号显示
     * 截取字符串前四为和后四位，其余部分用*显示 */
    public static String coverPhoneDisplay(String str){
        StringBuffer strsBuffer = new StringBuffer("");
//        String strs ="";
        if(StringUtils.isNotBlank(str)){
            String varStr = StringUtils.trim(str);
            for(int x=0;x<varStr.length();x++){
                if(varStr.length()==11){//手机号
                    if(x<3 || x>varStr.length()-5){
//                        strs +=varStr.substring(x,x+1);
                        strsBuffer.append(varStr.substring(x,x+1));
                    }else {
//                        strs +="*";
                        strsBuffer.append("*");
                    }
                }else{//身份证号或者银行卡号
                    if(x<4 || x>varStr.length()-5){
//                        strs +=varStr.substring(x,x+1);
                        strsBuffer.append(varStr.substring(x,x+1));
                    }else {
//                        strs +="*";
                        strsBuffer.append("*");
                    }
                }
            }
        }
//        return strs;
        return strsBuffer.toString();
    }

//    public static void main(String[] args) {
//        String phoneString = "13127941993\u202C";
//        // 提取数字
//        // 1
//        Pattern pattern = Pattern.compile("[^0-9]");
//        Matcher matcher = pattern.matcher(phoneString);
//        String all = matcher.replaceAll("");
//        System.out.println("phone:" + all);
//        // 2
//      String as=  Pattern.compile("[^0-9]").matcher(phoneString).replaceAll("");
//        System.out.println(as+"----------");
//    }
}