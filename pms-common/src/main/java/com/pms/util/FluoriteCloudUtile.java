package com.pms.util;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.NameValuePair;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicHeader;
import org.apache.http.util.EntityUtils;

import java.util.ArrayList;
import java.util.List;

public class FluoriteCloudUtile {

   /**请求路径*/
    private static  String url = "https://open.ys7.com/api/lapp/token/get";
   /**添加萤石云请求路径*/
    private static  String addurl = "https://open.ys7.com/api/lapp/device/add";
    
    /**删除萤石云请求路径*/
    private static  String deleteurl = "https://open.ys7.com/api/lapp/device/delete";
    /**萤石云设备抓拍图片请求路径*/
    private static  String captureurl ="https://open.ys7.com/api/lapp/device/capture";

    /**萤石云获取单个设备信息*/
    private static  String deviceInfo ="https://open.ys7.com/api/lapp/device/info";
    
    /**生成token*/
    public static JSONObject fluoriteCloud1(String appKey,String appSecret)throws Exception{
        StringBuffer fluoriteCloud=new StringBuffer("");
        fluoriteCloud.append("appKey=")
                .append(appKey)
                .append("&appSecret=")
                .append(appSecret);
        String ret = doPost(url,fluoriteCloud.toString()).toString();
        System.out.println(ret);
        JSONObject json = JSON.parseObject(ret);
        return  json;
    }

    /**
     * 注册到萤石云
     * @param deviceSerial  设备序列号
     * @param validateCode  验证码
     * @param accessToken  授权过程获取的access_token
     * @return
     * @throws Exception
     */
    public static JSONObject addfluoriteCloud(String  deviceSerial,String validateCode,String accessToken) throws Exception{
        StringBuffer addfluoriteCloud= new StringBuffer().append("");
        addfluoriteCloud.append("deviceSerial=")
                .append(deviceSerial)
                .append("&validateCode=")
                .append(validateCode)
                .append("&accessToken=")
                .append(accessToken);
    	String add = doPost(addurl,addfluoriteCloud.toString()).toString();
    	System.out.println(add);
    	JSONObject fluoriteCloud = JSON.parseObject(add);
    	return  fluoriteCloud;
    }


    /**
     * 删除萤石云
     * @param deviceSerial  设备序列号
     * @param accessToken 授权过程获取的access_token
     * @return
     */
    public static JSONObject deletefluoriteCloud(String  deviceSerial,String accessToken){
        StringBuffer deletefluoriteCloud= new StringBuffer().append("");
        deletefluoriteCloud
                .append("deviceSerial=")
                .append(deviceSerial)
                .append("&accessToken=")
                .append(accessToken );
    	String delete = doPost(deleteurl,deletefluoriteCloud.toString()).toString();
    	System.out.println(delete);
    	JSONObject fluoriteCloud = JSON.parseObject(delete);
    	return  fluoriteCloud;
    }

    /**
     * 萤石云获取单个设备信息
     * @param deviceSerial  设备序列号
     * @param accessToken   授权过程获取的access_token
     * @return
     */
    public static JSONObject deviceInfo(String  deviceSerial,String accessToken){
        StringBuffer capturefluoriteCloud= new StringBuffer().append("");
        capturefluoriteCloud.append("deviceSerial=")
                .append(deviceSerial)
                .append("&accessToken=")
                .append(accessToken);
       String capture = doPost(deviceInfo,capturefluoriteCloud.toString()).toString();
    	System.out.println(capture);
    	JSONObject fluoriteCloud = JSON.parseObject(capture);
    	return  fluoriteCloud;
    }


    /**
     * 萤石云设备抓拍图片
     * @param deviceSerial  设备序列号
     * @param channelNo     通道号，IPC设备填写1
     * @param accessToken   授权过程获取的access_token
     * @return
     */
    public static JSONObject capture(String  deviceSerial,String channelNo,String accessToken){
        StringBuffer capturefluoriteCloud= new StringBuffer().append("");
        capturefluoriteCloud.append("deviceSerial=")
                .append(deviceSerial)
                .append("&channelNo=")
                .append(channelNo)
                .append("&accessToken=")
                .append(accessToken);
        String capture = doPost(captureurl,capturefluoriteCloud.toString()).toString();
        System.out.println(capture);
        JSONObject fluoriteCloud = JSON.parseObject(capture);
        return  fluoriteCloud;
    }


    /**
     * post请求
     * @param url
     * @param json
     * @return
     */
    public static JSONObject doPost(String url,String json){
      DefaultHttpClient client = new DefaultHttpClient();
      List<NameValuePair> params = new ArrayList<NameValuePair>();
      HttpPost post = new HttpPost(url);
      post.addHeader(new BasicHeader("version","ytgo_2.2.0"));
      JSONObject response = null;
      try {
        StringEntity s = new StringEntity(json.toString());
        System.out.println(json.toString());
        s.setContentEncoding("UTF-8");
        s.setContentType("application/x-www-form-urlencoded");
        post.setEntity(s);
        System.out.println(post.getEntity().toString());
        Long startTime =System.currentTimeMillis();
        HttpResponse res = client.execute(post);
        if(res.getStatusLine().getStatusCode() == HttpStatus.SC_OK){
          HttpEntity entity = res.getEntity();
          String result = EntityUtils.toString(res.getEntity());// 返回json格式：
          response = JSONObject.parseObject(result);
          System.out.println("请求耗时："+(System.currentTimeMillis()-startTime) +" 毫秒！");
        }
      } catch (Exception e) {
        throw new RuntimeException(e);
      }
      return response;
    }

    /**
     * 删除萤石云
     * @param args
     */
//    public  static   void  main(String[] args) throws Exception {
////       deletefluoriteCloud("574652219","at.1q6dmmbfdkncvlrw6g8ejuzk4trafgkk-3glo7v6l5c-1hlucie-71m4cp9y3");
////        fluoriteCloud1("dc0792044f8c4464a78e75e898f07d6d","f0b375a877d0c71be31fc82d19ca125f");
//        capture("574652219","1","at.77ixmyds2oucbx4714pra9dz3ityzl9i-39ij4kkfcg-0v9m0s1-uweyurcru");
//    }


}
