package com.pms.util;

import org.apache.commons.lang.StringUtils;

import java.lang.reflect.Field;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by ASUS_B on 2017/10/26.
 * 数据验证工具类
 */
public class ValidateUtil {
    /**
     * 手机号码正则
     */
    private static String phonePatternStr = "^[1][3,4,5,7,8][0-9]{9}$";
    private static Pattern phonePattern = Pattern.compile(phonePatternStr);
    /**
     * 座机号码正则 (区号间可不加 - eg:028xxxxx)
     */
    private static String telphonePatternStr = "^(0\\d{2,3}(-)?\\d{7,8}((-)?\\d{3,5}){0,1})|(((13[0-9])|(15([0-3]|[5-9]))|(18[0,5-9]))\\d{8})$";
    private static Pattern telphonePattern = Pattern.compile(telphonePatternStr);
    /**
     * 座机号码正则 (区号间必须加 - eg:028-xxxx)
     */
    private static String telphonePatternStr_ = "^(0\\d{2,3}\\d{7,8}(\\d{3,5}){0,1})|(((13[0-9])|(15([0-3]|[5-9]))|(18[0,5-9]))\\d{8})$";
    private static Pattern telphonePattern_ = Pattern.compile(telphonePatternStr_);


    /**
     * 验证手机号 格式是否正确
     * @return
     */
    public static boolean validatePhone(String phone){
        if(StringUtils.isBlank(phone)){return false;}
        if(phonePattern.matcher(phone).matches()){ return true;}
        return false;
    }
    /**
     * 验证座机号 格式是否正确 (区号间必须加 - eg:028-xxxx)
     * @return
     */
    public static boolean validateTelphone(String telphone){
        if(StringUtils.isBlank(telphone)){return false;}
        if(telphonePattern_.matcher(telphone).matches()){ return true;}
        return false;
    }
    /**
     * 验证座机号 格式是否正确(区号间可不加 - eg:028xxxxx)
     * @return
     */
    public static boolean validateTelphone_(String telphone){
        if(StringUtils.isBlank(telphone)){return false;}
        if(telphonePattern.matcher(telphone).matches()){ return true;}
        return false;
    }
}
