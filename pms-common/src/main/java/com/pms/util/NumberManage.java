package com.pms.util;



public class NumberManage {
	private static NumberManage insobj;

	public static NumberManage getInstance() {
		if (insobj == null)
			insobj = new NumberManage();
		return insobj;

	}

	/**
	 * 根据生成策略返回编号串
	 * 
	 * @param type
	 * @return
	 */
	public String getNumberStrategies(NumberManagerType type) {
		switch (type) {
		case serialNumber:
			return UniqId.getInstance().getSerialNumber();
		case TimeNumber:
			return UniqId.getInstance().getTimeNumber();
		case autoGrowNumber:
			return UniqId.getInstance().getTimeNumber();
		default:
			return UniqId.getInstance().getUniqTimeString();
		}

	}

	/**
	 * 限制形的时间序列号编号生成
	 * 
	 * @param type
	 * @return
	 */
	public String getNumberStrategies(NumberManagerType type,
			Integer numberLength) {
		if (type.equals(NumberManagerType.TimeNumber)) {
			return UniqId.getInstance().getTimeNumber(numberLength);
		} else {
			return getNumberStrategies(type);
		}
	}
}
