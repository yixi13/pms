package com.pms.util;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;
import java.util.ResourceBundle;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang3.StringUtils;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.session.Session;
import org.apache.shiro.subject.Subject;
import org.springframework.web.util.WebUtils;


/**
 * Web层辅助类
 * 
 * @version 2016年4月2日 下午4:19:28
 */
public final class WebUtil {
	private WebUtil() {
	}

	/**
	 * 获取指定Cookie的值
	 * 
	 * @param
	 *
	 * @param cookieName
	 *            cookie名字
	 * @param defaultValue
	 *            缺省值
	 * @return
	 */
	public static final String getCookieValue(HttpServletRequest request, String cookieName, String defaultValue) {
		Cookie cookie = WebUtils.getCookie(request, cookieName);
		if (cookie == null) {
			return defaultValue;
		}
		return cookie.getValue();
	}

	


	/**
	 * 将一些数据放到ShiroSession中,以便于其它地方使用
	 * 
	 * @see
	 */
	public static final void setSession(Object key, Object value) {
		Subject currentUser = SecurityUtils.getSubject();
		if (null != currentUser) {
			Session session = currentUser.getSession();
			if (null != session) {
				session.setAttribute(key, value);
			}
		}
	}


	/**
	 * 得打session
	 * 
	 */
	public static Session getSession(){
		Subject currentUser = SecurityUtils.getSubject();
		if (null != currentUser) {
			Session session = currentUser.getSession();
			if (null != session) {
				return  session;
			}
		}
		return null;
	}


//	/**
//	 * 创建会员登录Sn
//	 *
//	 * @param username
//	 *            会员账号
//	 * @param pwd
//	 *            会员密码
//	 * @return
//	 * @throws Exception
//	 */
//	public static String createSn(String username,String pwd,String mac) throws Exception{
//		String sn=MD5Util.getMD5String(username.toLowerCase()+pwd+getDatestr()+new Date());
//		//setSession("SN"+sn, sn);
//
//		return sn;
//	}
//
//	/**
//	 * 创建会员登录Sn
//	 *
//	 * @param
//	 *            会员账号
//	 * @param
//	 *            会员密码
//	 * @return
//	 * @throws Exception
//	 */
//	public static String establishSn(String phone,String spId,String mac) throws Exception{
//		String sn=MD5Util.getMD5String(phone+spId+mac);
//		return sn;
//	}

	
	/**
	 * 获得国际化信息
	 * 
	 * @param key
	 *            键
	 * @param request
	 * @return
	 */
	public static final String getApplicationResource(String key, HttpServletRequest request) {
		ResourceBundle resourceBundle = ResourceBundle.getBundle("ApplicationResources", request.getLocale());
		return resourceBundle.getString(key);
	}

	/**
	 * 获得参数Map
	 * 
	 * @param request
	 * @return
	 */
	public static final Map<String, Object> getParameterMap(HttpServletRequest request) {
		return WebUtils.getParametersStartingWith(request, null);
	}

	/** 获取客户端IP */
	public static final String getHost(HttpServletRequest request) {
		String ip = request.getHeader("X-Forwarded-For");
		if (StringUtils.isBlank(ip) || "unknown".equalsIgnoreCase(ip)) {
			ip = request.getHeader("Proxy-Client-IP");
		}
		if (StringUtils.isBlank(ip) || "unknown".equalsIgnoreCase(ip)) {
			ip = request.getHeader("WL-Proxy-Client-IP");
		}
		if (StringUtils.isBlank(ip) || "unknown".equalsIgnoreCase(ip)) {
			ip = request.getHeader("X-Real-IP");
		}
		if (StringUtils.isBlank(ip) || "unknown".equalsIgnoreCase(ip)) {
			ip = request.getRemoteAddr();
		}
		if ("127.0.0.1".equals(ip)) {
			InetAddress inet = null;
			try { // 根据网卡取本机配置的IP
				inet = InetAddress.getLocalHost();
			} catch (UnknownHostException e) {
				e.printStackTrace();
			}
			ip = inet.getHostAddress();
		}
		// 对于通过多个代理的情况，第一个IP为客户端真实IP,多个IP按照','分割
		if (ip != null && ip.length() > 15) {
			if (ip.indexOf(",") > 0) {
				ip = ip.substring(0, ip.indexOf(","));
			}
		}
		return ip;
	}


	
	
	/** 截取字符串前四为和后四位，其余部分用*显示 */
 	public static String subStr(String str){
 		String strs ="";
 		String varStr =StringUtils.trim(str);
 		if(StringUtils.isNotBlank(varStr)){
 			for(int x=0;x<varStr.length();x++){
 				if(varStr.length()==11){//手机号
 					if(x<3 || x>varStr.length()-5){
 	 	 				strs +=varStr.substring(x,x+1);
 	 	 			}else {
 	 	 				strs +="*";
 	 	 			}
 				}else{//身份证号或者银行卡号
 					if(x<4 || x>varStr.length()-5){
 	 	 				strs +=varStr.substring(x,x+1);
 	 	 			}else {
 	 	 				strs +="*";
 	 	 			}
 				}
 	 		}
 		}
 		return strs;
 	}


	/**
	 * 正则手机号验证
	 * @param mobiles
	 * @return
	 */
	public static boolean isMobileNO(String mobiles) {
		if (mobiles.length()!= 11){
			return false;
		}else{
			Pattern p = Pattern.compile("^((13[0-9])|(14[0-9])|(17[0-9])|(19[0-9])|(15[^4,\\D])|(18[0-9]))\\d{8}$");
			Matcher m = p.matcher(mobiles);
			return m.matches();
		}
	}
}
