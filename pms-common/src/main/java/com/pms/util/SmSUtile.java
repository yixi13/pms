package com.pms.util;

import com.aliyuncs.dysmsapi.model.v20170525.SendSmsResponse;

import java.util.Random;

public class SmSUtile {

	/**
	 * 发送验证码
	 * @param phone 电话号
	 * @param type 1 注册验证码，
	 * @return
	 */
	public static String sendSms(String phone,int type,String password){
		String code=getNewCode();
		String content =getContent(type);
		SendSmsResponse state=null;
		if (type==6){//快速注册
			  state=OSSSmsUtils.sendSms(phone,content,password);
		}else{
			  state=OSSSmsUtils.sendSms(phone,content,code);
		}
		if (state.getCode().equals("OK")){
			return  code;
		}else{
			System.out.print(state.getCode()+"----------------------");
			String codeError="error";
			return codeError;
		}
	}


	/**
	 * 获取指定长度随机数字串
	 * @param
	 * @return
	 */
	public static String getNewCode() {
		String r = "123456789";
		StringBuffer sb=new StringBuffer();
		Random random=new Random();
		int len = 6;
		for (int i = 0; i < len; i++) {
			int cd = random.nextInt(9);
			sb.append(r.charAt(cd));
		}
		return sb.toString();
	}

    /**
     * 得到验证码消息文本
     * @param type 1-用户注册 2-身份验证3-修改密码  4-身份变更 5-登录异常6-快速注册
     * @return
     */
    public static String getContent(int type){
    	String register="SMS_114605024";//用户注册
		String authentication="SMS_114605028";//身份验证
		String updatePassword="SMS_114605023";//修改密码
		String updateIdentity="SMS_114605022";//身份变更
		String LoginException="SMS_114605025";//登录异常
		String fastRegistration="SMS_122288586";//快速注册
		String updatePaymentPassword="SMS_123795490";//修改支付密码
		String reString="";
    	switch (type) {
		case 1:
			reString=register;
			break;
		case 2:
			reString=authentication;
			break;
		case 3:
			reString=updatePassword;
			break;
		case 4:
			reString=updateIdentity;
			break;
		case 5:
			reString = LoginException;
			break;
		case 6:
			reString=fastRegistration;
			break;
		case 7:
				reString=updatePaymentPassword;
				break;
		default: reString=authentication;
			break;
		}
    	return reString;
    }

}
