package com.pms.util;

import java.util.ArrayList;
import java.util.List;

/**
 * 字符串处理工具类
 * Created by Administrator on 2017/10/23.
 */
public class StringFormatUtil {
    //首字母转小写
    public static String toLowerCaseFirstOne(String s){
        if(Character.isLowerCase(s.charAt(0)))
            return s;
        else
            return (new StringBuilder()).append(Character.toLowerCase(s.charAt(0))).append(s.substring(1)).toString();
    }
    //首字母转大写
    public static String toUpperCaseFirstOne(String s){
        if(Character.isUpperCase(s.charAt(0)))
            return s;
        else
            return (new StringBuilder()).append(Character.toUpperCase(s.charAt(0))).append(s.substring(1)).toString();
    }

    /**
     * 获取字符串中某字符的下标集合
     * @param str 字符串
     * @param charStr 字符
     * @return
     */
    public static List<Integer> getCharIndexs( String str,String charStr){
        List<Integer> indexlist = new ArrayList<Integer>();
        int lastIndex= str.lastIndexOf(charStr);
        int index= str.indexOf(charStr);
        indexlist.add(index);
        while (index<lastIndex){
            index = str.indexOf(charStr,index+1);
            indexlist.add(index);
        }
        return indexlist;
    }

}
