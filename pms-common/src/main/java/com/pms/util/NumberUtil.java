package com.pms.util;


public class NumberUtil {

	public static String getNu(NuType d) {
		switch (d) {
		case newOrder:
			return "O"+NumberManage.getInstance().getNumberStrategies(NumberManagerType.autoGrowNumber, 8);
		case product:
			return "HM"+NumberManage.getInstance().getNumberStrategies(NumberManagerType.TimeNumber, 8);
		case member:
			return "M"+NumberManage.getInstance().getNumberStrategies(NumberManagerType.serialNumber, 8);
		case merchants:
			return "MC"+NumberManage.getInstance().getNumberStrategies(NumberManagerType.serialNumber, 8);
		case payNo:
			return "P"+NumberManage.getInstance().getNumberStrategies(NumberManagerType.autoGrowNumber, 8);
		case payUserNo:
			return "PU"+NumberManage.getInstance().getNumberStrategies(NumberManagerType.autoGrowNumber, 8);
		case payAccountNo:
			return "PA"+NumberManage.getInstance().getNumberStrategies(NumberManagerType.autoGrowNumber, 8);
		case refundNu:
			return "RF"+NumberManage.getInstance().getNumberStrategies(NumberManagerType.autoGrowNumber, 8);
		case returnGoods:
			return "T"+NumberManage.getInstance().getNumberStrategies(NumberManagerType.TimeNumber, 8);
		case img:
			return "img"+NumberManage.getInstance().getNumberStrategies(NumberManagerType.TimeNumber, 8);
		case billNu:
			return "BN"+NumberManage.getInstance().getNumberStrategies(NumberManagerType.TimeNumber, 8);
		default:
			return UniqId.getInstance().getUniqTimeString();
		}
	}
	
	public static String getNickName(String phone){

		return "TZF"+phone.substring(0,6)+UniqId.getRandomCode(4);
	}

	public static void main(String[] args){
		//for(int i=0;i<10000;i++)
		System.out.println(UniqId.getRandomCode(4));
		System.out.println(UniqId.getRandomCodeNum());
		System.out.println(NumberUtil.getNu(NuType.img));
		System.out.println("PO"+NumberManage.getInstance().getNumberStrategies(NumberManagerType.autoGrowNumber, 8));
		System.out.println("PO"+NumberManage.getInstance().getNumberStrategies(NumberManagerType.autoGrowNumber, 6));
		System.out.println("PO"+NumberManage.getInstance().getNumberStrategies(NumberManagerType.autoGrowNumber));
		System.out.println(NumberManage.getInstance().getNumberStrategies(NumberManagerType.TimeNumber));
		System.out.println(NumberManage.getInstance().getNumberStrategies(NumberManagerType.TimeNumber,8));
		System.out.println(NumberManage.getInstance().getNumberStrategies(NumberManagerType.TimeNumber,15));
	}


}
