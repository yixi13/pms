package com.pms.util.idutil;


public class CodeGenerate {
    /**
     *
     * 随机生成验证码（数字+字母）
     *
     * @param len 邀请码长度
     * @return
     */
    public static String generateRandomStr(int len) {
        //字符源，可以根据需要删减
        String generateSource = "23456789abcdefghgklmnpqrstuvwxyz";//去掉1和i ，0和o
        String rtnStr = "";
        for (int i = 0; i < len; i++) {
            //循环随机获得当次字符，并移走选出的字符
            String nowStr = String.valueOf(generateSource.charAt((int) Math.floor(Math.random() * generateSource.length())));
            rtnStr += nowStr;
            generateSource = generateSource.replaceAll(nowStr, "");
        }
        return rtnStr.toUpperCase();
    }

    public static void main(String[] args){
        for (int i = 0; i < 20; i++) {
            System.out.println(generateRandomStr(5));
        }

    }
}
