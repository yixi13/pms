package com.pms.util;

import javax.servlet.http.HttpServletRequest;
import java.io.File;


/**
 * 上传辅助类 与Spring.multipartResolver冲突
 * 
s */
public final class UploadUtil {
    private UploadUtil() {
    }

    /** 上传文件缓存大小限制 */
    private static int fileSizeThreshold = 1024 * 1024 * 1;
    /** 上传文件临时目录 */
    private static final String uploadFileDir = "/WEB-INF/upload/";



    /** 获取上传文件临时目录 */
    public static String getUploadDir(HttpServletRequest request) {
        return request.getServletContext().getRealPath(uploadFileDir) + File.separator;
    }


}
