package com.pms.result;

public enum ResultCode {
    /** 成功 */
    SUCCESS(200, "成功"),

    /** 没有登录 */
    NOT_LOGIN(400, "没有登录"),

    /** 发生异常 */
    EXCEPTION(401, "发生异常"),

    /** 系统错误 */
    SYS_ERROR(402, "系统错误"),

    /** 参数错误 */
    PARAMS_ERROR(201, "参数错误 "),
    /** 403没有权限 */
    FORBIDDEN(403,"没有权限"),
    /** 不支持或已经废弃 */
    NOT_SUPPORTED(410, "不支持或已经废弃"),

    /** AuthCode错误 */
    INVALID_AUTHCODE(444, "无效的AuthCode"),
    /** 500服务器出错 */
    INTERNAL_SERVER_ERROR(500,"服务器错误"),
    /** 太频繁的调用 */
    MULTI_STATUS(207, "操作频繁"),
    /** 408请求超时 */
    REQUEST_TIMEOUT(408,"请求超时"),
    /** 未知的错误 */
    UNKNOWN_ERROR(499, "未知错误");

    private ResultCode(Integer value, String msg){
        this.val = value;
        this.msg = msg;
    }

    public Integer val() {
        return val;
    }

    public String msg() {
        return msg;
    }

    private Integer val;
    private String msg;
}
