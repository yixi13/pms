package com.pms.exception;

import com.pms.util.Description;

import java.lang.reflect.Field;
import java.util.HashMap;
import java.util.Map;

/**
 * 返回数据
 * 
 */
public class R extends HashMap<String, Object> {
	private static final long serialVersionUID = 1L;
	
	public R() {
		put("code", 200);
	}
	
	public static R error() {
		return error(500, "未知异常，请联系管理员");
	}
	
	public static R error(String msg) {
		return error(500, msg);
	}
	
	public static R error(int code, String msg) {
		R r = new R();
		r.put("code", code);
		r.put("msg", msg);
		return r;
	}

	public static R ok(String msg) {
		R r = new R();
		r.put("msg", msg);
		return r;
	}
	
	public static R ok(Map<String, Object> map) {
		R r = new R();
		r.putAll(map);
		return r;
	}
	
	public static R ok() {
		return new R().put("msg", "ok");
	}

	public R put(String key, Object value) {
		super.put(key, value);
		return this;
	}

	public R putDescription(Class<?> cl){
		Object description = super.get("description");
		if(null==description){description="";}
		super.put("description", description +getDescriptionValue(cl));
		return this;
	}
	public R putDescription(String descriptionStr){
		Object description = super.get("description");
		if(null==description){description="";}
		super.put("description", description +descriptionStr);
		return this;
	}
	public R putMpPageDescription(){
		Object description = super.get("description");
		if(null==description){description="";}
		super.put("description", description +"{Page==>records:数据列表,current:当前页数,pages:总页数,total:总记录数,size:查询条数},");
		return this;
	}
	private String getDescriptionValue(Class<?> cl){
		StringBuffer str = new StringBuffer("{");
		Field[] fields = cl.getDeclaredFields();
		for(Field f : fields){
			Description desc = f.getAnnotation(Description.class);
			if(null!=desc){
				str.append(f.getName()).append(":").append(desc.value()).append(",");
			}
		}
		str.append("},");
		return str.toString();
	}
}
