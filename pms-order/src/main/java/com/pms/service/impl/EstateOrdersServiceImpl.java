package com.pms.service.impl;

import org.springframework.stereotype.Service;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.pms.entity.EstateOrders;
import com.pms.mapper.EstateOrdersMapper;
import com.pms.service.IEstateOrdersService;
/**
 * 
 *
 * @author zyl
 * @email 517770986@qq.com
 * @date 2017-11-01 11:52:52
 */
@Service
public class EstateOrdersServiceImpl extends ServiceImpl<EstateOrdersMapper,EstateOrders> implements IEstateOrdersService {

}