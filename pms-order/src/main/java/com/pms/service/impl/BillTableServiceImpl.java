package com.pms.service.impl;

import com.baomidou.mybatisplus.mapper.Wrapper;
import com.pms.entity.MemberInfo;
import com.pms.rpc.IMemberService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.pms.entity.BillTable;
import com.pms.mapper.BillTableMapper;
import com.pms.service.IBillTableService;

import java.util.List;
import java.util.Map;

/**
 * 
 *
 * @author zyl
 * @email 517770986@qq.com
 * @date 2017-11-01 11:52:51
 */
@Service
public class BillTableServiceImpl extends ServiceImpl<BillTableMapper,BillTable> implements IBillTableService {
    private IMemberService memberService;
    @Autowired
    public BillTableServiceImpl(
            IMemberService memberService) {
        this.memberService = memberService;
    }
    @Autowired
    private BillTableMapper billTableMapper;
    @Override
    public MemberInfo queryBymember(Long memberId) {
        MemberInfo member = memberService.queryBymember(memberId);
        return member;
    }

    @Override
    public List<Map<String, Object>> selectBillTables(Wrapper<BillTable> wrapper) {
        return billTableMapper.selectBillTables(wrapper);
    }

    @Override
    public List<BillTable> selectBillTableDetailsByParam(Wrapper<BillTable> wrapper) {
        return billTableMapper.selectBillTableDetailsByParam(wrapper);
    }
}