package com.pms.service;
import com.baomidou.mybatisplus.mapper.Wrapper;
import com.baomidou.mybatisplus.service.IService;
import com.pms.entity.BillTable;
import com.pms.entity.MemberInfo;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * 
 *
 * @author zyl
 * @email 517770986@qq.com
 * @date 2017-11-01 11:52:51
 */

public interface IBillTableService extends  IService<BillTable> {
    MemberInfo queryBymember(Long memberId);
    List<Map<String,Object>> selectBillTables(@Param("ew") Wrapper<BillTable> wrapper);
    List<BillTable> selectBillTableDetailsByParam(@Param("ew") Wrapper<BillTable> wrapper);
}