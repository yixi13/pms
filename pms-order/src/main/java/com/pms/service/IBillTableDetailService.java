package com.pms.service;
import com.baomidou.mybatisplus.service.IService;
import com.pms.entity.BillTableDetail;
/**
 * 
 *
 * @author zyl
 * @email 517770986@qq.com
 * @date 2017-11-01 11:52:52
 */

public interface IBillTableDetailService extends  IService<BillTableDetail> {

}