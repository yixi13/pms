package com.pms.web;

import org.springframework.beans.factory.annotation.Autowired;
import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.plugins.Page;
import org.springframework.web.bind.annotation.*;
import com.pms.controller.BaseController;
import com.pms.service.IEstateOrdersService;
import com.pms.entity.EstateOrders;
import com.pms.result.JsonResult;
import com.pms.result.ObjectRestResponse;
import com.pms.result.TableResultResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;

@RestController
@RequestMapping("estateOrders")
public class EstateOrdersController extends BaseController {
    Logger logger = LoggerFactory.getLogger(getClass());
    @Autowired
    IEstateOrdersService estateOrdersService;

    /**
         * 新增
         * @param entity
         * @return
         */
    @RequestMapping(value = "",method = RequestMethod.POST)
    @ResponseBody
    public ObjectRestResponse<EstateOrders> add(@RequestBody EstateOrders entity){
            estateOrdersService.insert(entity);
        return new ObjectRestResponse<EstateOrders>().rel(true);
    }
    /**
     * 修改
     * @param entity
     * @return
     */
    @RequestMapping(value = "/{id}",method = RequestMethod.POST)
    @ResponseBody
    public ObjectRestResponse<EstateOrders> update(@RequestBody EstateOrders entity){
            estateOrdersService.updateById(entity);
        return new ObjectRestResponse<EstateOrders>().rel(true);
    }

    /**
 * 删除
 * @param id
 * @return
 */
    @RequestMapping(value = "/{id}",method = RequestMethod.DELETE)
    @ResponseBody
    public ObjectRestResponse<EstateOrders> deleteById(@PathVariable int id){
            estateOrdersService.deleteById (id);
        return new ObjectRestResponse<EstateOrders>().rel(true);
    }


    /**
     * findByID
     * @param id
     * @return
     */
    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    @ResponseBody
    public ObjectRestResponse<EstateOrders> findById(@PathVariable int id){
        return new ObjectRestResponse<EstateOrders>().rel(true).data( estateOrdersService.selectById(id));
    }


    @RequestMapping(value = "/page",method = RequestMethod.GET)
    @ResponseBody
    public TableResultResponse<EstateOrders> page(@RequestParam(defaultValue = "10") int limit, @RequestParam(defaultValue = "1")int offset){
        Page<EstateOrders> page = new Page<EstateOrders>(offset,limit);
        page = estateOrdersService.selectPage(page,new EntityWrapper<EstateOrders>());
        return new TableResultResponse<EstateOrders>(page.getTotal(),page.getRecords());
    }

    @RequestMapping(value = "/all",method = RequestMethod.GET)
    @ResponseBody
    public List<EstateOrders> all(){
        return estateOrdersService.selectList(null);
    }


}