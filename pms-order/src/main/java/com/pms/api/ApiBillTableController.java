package com.pms.api;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.mapper.Wrapper;
import com.pms.controller.BaseController;
import com.pms.entity.BillTable;
import com.pms.entity.MemberInfo;
import com.pms.exception.R;
import com.pms.rpc.IMemberService;
import com.pms.service.IBillTableDetailService;
import com.pms.service.IBillTableService;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.apache.shiro.util.Assert;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Administrator on 2017/11/2 0002.
 */
@RestController
@RequestMapping("apiBillTable")
public class ApiBillTableController extends BaseController {
    @Autowired
    IBillTableService billTableService;
    @Autowired
    IBillTableDetailService billTableDetailService;
    @Autowired
    IMemberService memberService;

    @ApiOperation(value = "会员查询房屋账单")
    @GetMapping(value = "/getBillTables")
    @ApiImplicitParams({
            @ApiImplicitParam(name="memberId",value="会员id",required = true,dataType = "Long"),
            @ApiImplicitParam(name="houseId",value="房屋id",required = true,dataType = "Long")
    })
    public ResponseEntity<?> getBillTables(Long memberId, Long houseId){
        parameterIsNull(memberId,"会员id不能为空");
        parameterIsNull(houseId,"房屋id不能为空");
        MemberInfo member = memberService.queryBymember(memberId);
        Assert.notNull(member,"没有此会员");
        //获取当前的年月份
        Calendar rightNow = Calendar.getInstance();
        int year = rightNow.get(Calendar.YEAR);
        int month = rightNow.get(Calendar.MONTH)+1;
        Wrapper<BillTable> wp = new EntityWrapper<BillTable>();
        List<Map<String,Object>> noPayList = null;
        List<Map<String,Object>> payList = null;
        if(memberId==1){
            //按月生成的查询
            wp.eq("is_payment",1);
            wp.orderBy("create_time",false);
            wp.groupBy("bill_year");
            wp.groupBy("bill_month");
            System.out.print(wp.getParamNameValuePairs());
            noPayList=  billTableService.selectBillTables(wp);
            wp.getParamNameValuePairs().remove("MPGENVAL1");
            System.out.print(wp.getParamNameValuePairs());
            System.out.print(wp.getSqlSegment());
            wp.eq("is_payment",2);
            System.out.print(wp.getParamNameValuePairs());
            payList =  billTableService.selectBillTables(wp);
        }else if(memberId==2){
            //按季度生成
            wp.eq("is_payment",1);
            wp.orderBy("create_time",false);
            wp.groupBy("bill_year");
            wp.groupBy("bill_season");
            noPayList=  billTableService.selectBillTables(wp);
            wp.eq("is_payment",2);
            payList=  billTableService.selectBillTables(wp);
        }else if(memberId==3){
            //按半年生成
            wp.eq("is_payment",1);
            wp.orderBy("create_time",false);
            wp.groupBy("bill_year");
            wp.groupBy("bill_half_year");
            noPayList=  billTableService.selectBillTables(wp);
            wp.eq("is_payment",2);
            payList=  billTableService.selectBillTables(wp);
        }else if(memberId==4){
            //按年生成
            wp.eq("is_payment",1);
            wp.orderBy("create_time",false);
            wp.groupBy("bill_year");
            noPayList=  billTableService.selectBillTables(wp);
            wp.eq("is_payment",2);
            payList=  billTableService.selectBillTables(wp);
        }
        Map<String,Object> map = new HashMap<String,Object>();
        map.put("noPayList",noPayList);
        map.put("payList",payList);
        return ResponseEntity.ok().body(map);
    }

    @ApiOperation(value = "会员查询房屋账单详情")
    @GetMapping(value = "/getBillTableDetail")
    @ApiImplicitParams({
            @ApiImplicitParam(name="houseId",value="房屋id",required = true,dataType = "Long"),
            @ApiImplicitParam(name="estateBillMode",value="类型：1.按年 2.按半年 3.按季度 4.按月",required = true,dataType = "Integer"),
            @ApiImplicitParam(name="num",value="对应类型下时间：当estateBillMode为1时，num可为1或者null;" +
                    "当estateBillMode为2时，num=1表示上半年,num=2表示下半年" +
                    "当estateBillMode为3时,num=1,2,3,4表示对应的每个季度" +
                    "当estateBillMode为4时,num=1,2,3,4,5,6,7,8,9,10,11,12表示对应的每个月份",
                    required = true,dataType = "Integer")
    })
    public R getBillTableDetail(Long houseId,Integer year,Integer estateBillMode,Integer num){
        parameterIsNull(houseId,"房屋id不能为空");
        parameterIsNull(year,"对应的年份不能为空");
        parameterIsNull(estateBillMode,"对应的类型不能为空");
        if(estateBillMode!=1){
            parameterIsNull(num,"对应类型下时间不能为空");
        }
        Wrapper<BillTable> wp = new EntityWrapper<BillTable>();
        wp.eq("bt.bill_year",year);
        if(estateBillMode==1){

        }else if(estateBillMode==2){
            if(num!=1&&num!=2){
                return R.error(500,"对应类型时间传值有误");
            }
            wp.eq("bt.bill_half_year",num);
        }else if(estateBillMode==3){
            if(num<1||num>4){
                return R.error(500,"对应类型时间传值有误");
            }
            wp.eq("bt.bill_season",num);
        }else if(estateBillMode==4){
            if(num<1||num>12){
                return R.error(500,"对应类型时间传值有误");
            }
            wp.eq("bt.bill_month",num);
        }
        List<BillTable> list = billTableService.selectBillTableDetailsByParam(wp);
        return R.ok().put("list",list);
    }
}
