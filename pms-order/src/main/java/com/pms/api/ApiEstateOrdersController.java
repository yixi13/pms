package com.pms.api;

import com.pms.controller.BaseController;
import com.pms.service.IEstateOrdersService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by Administrator on 2017/11/2 0002.
 */
@RestController
@RequestMapping("apiEstateOrders")
public class ApiEstateOrdersController extends BaseController {
    @Autowired
    IEstateOrdersService estateOrdersService;
}
