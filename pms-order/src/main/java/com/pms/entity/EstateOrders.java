package com.pms.entity;
import com.baomidou.mybatisplus.activerecord.Model;
import java.io.Serializable;
import java.util.Date;
import javax.persistence.*;
import com.pms.util.Description;
import com.pms.entity.BaseModel;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;


/**
 * 
 * 
 * @author zyl
 * @email 517770986@qq.com
 * @date 2017-11-01 11:52:52
 */
@TableName( "estate_orders")
public class EstateOrders extends BaseModel<EstateOrders> {
private static final long serialVersionUID = 1L;


	    //
    @TableId("order_id")
    private Long orderId;
	
	    //订单编号
    @TableField("order_no")
    @Description("订单编号")
    private String orderNo;
	
	    //订单状态:1.待付款 2.已付款 3.已删除
    @TableField("order_status")
    @Description("订单状态:1.待付款 2.已付款 3.已删除")
    private Integer orderStatus;
	
	    //
    @TableField("member_id")
    @Description("")
    private Long memberId;
	
	    //会员名称
    @TableField("member_name")
    @Description("会员名称")
    private String memberName;
	
	    //会员电话
    @TableField("member_phone")
    @Description("会员电话")
    private String memberPhone;
	
	    //物业机构id
    @TableField("agency_id")
    @Description("物业机构id")
    private Long agencyId;
	
	    //机构名称
    @TableField("agency_name")
    @Description("机构名称")
    private String agencyName;
	
	    //1.微信 2.支付宝  3.网银
    @TableField("pay_type")
    @Description("1.微信 2.支付宝  3.网银")
    private Integer payType;
	
	    //下单机构(在哪个app买的)
    @TableField("order_source")
    @Description("下单机构(在哪个app买的)")
    private Long orderSource;
	
	    //总金额
    @TableField("total_price")
    @Description("总金额")
    private Double totalPrice;
	
	    //创建时间
    @TableField("create_time")
    @Description("创建时间")
    private Date createTime;
	
// ID赋值
public EstateOrders(){
        this.orderId= returnIdLong();
        }
@Override
protected Serializable pkVal() {
        return this.orderId;
}
	/**
	 * 设置：
	 */
	public void setOrderId(Long orderId) {
		this.orderId = orderId;
	}
	/**
	 * 获取：
	 */
	public Long getOrderId() {
		return orderId;
	}
	/**
	 * 设置：订单编号
	 */
	public void setOrderNo(String orderNo) {
		this.orderNo = orderNo;
	}
	/**
	 * 获取：订单编号
	 */
	public String getOrderNo() {
		return orderNo;
	}
	/**
	 * 设置：订单状态:1.待付款 2.已付款 3.已删除
	 */
	public void setOrderStatus(Integer orderStatus) {
		this.orderStatus = orderStatus;
	}
	/**
	 * 获取：订单状态:1.待付款 2.已付款 3.已删除
	 */
	public Integer getOrderStatus() {
		return orderStatus;
	}
	/**
	 * 设置：
	 */
	public void setMemberId(Long memberId) {
		this.memberId = memberId;
	}
	/**
	 * 获取：
	 */
	public Long getMemberId() {
		return memberId;
	}
	/**
	 * 设置：会员名称
	 */
	public void setMemberName(String memberName) {
		this.memberName = memberName;
	}
	/**
	 * 获取：会员名称
	 */
	public String getMemberName() {
		return memberName;
	}
	/**
	 * 设置：会员电话
	 */
	public void setMemberPhone(String memberPhone) {
		this.memberPhone = memberPhone;
	}
	/**
	 * 获取：会员电话
	 */
	public String getMemberPhone() {
		return memberPhone;
	}
	/**
	 * 设置：物业机构id
	 */
	public void setAgencyId(Long agencyId) {
		this.agencyId = agencyId;
	}
	/**
	 * 获取：物业机构id
	 */
	public Long getAgencyId() {
		return agencyId;
	}
	/**
	 * 设置：机构名称
	 */
	public void setAgencyName(String agencyName) {
		this.agencyName = agencyName;
	}
	/**
	 * 获取：机构名称
	 */
	public String getAgencyName() {
		return agencyName;
	}
	/**
	 * 设置：1.微信
  2.支付宝  3.网银
	 */
	public void setPayType(Integer payType) {
		this.payType = payType;
	}
	/**
	 * 获取：1.微信
  2.支付宝  3.网银
	 */
	public Integer getPayType() {
		return payType;
	}
	/**
	 * 设置：下单机构(在哪个app买的)
	 */
	public void setOrderSource(Long orderSource) {
		this.orderSource = orderSource;
	}
	/**
	 * 获取：下单机构(在哪个app买的)
	 */
	public Long getOrderSource() {
		return orderSource;
	}
	/**
	 * 设置：总金额
	 */
	public void setTotalPrice(Double totalPrice) {
		this.totalPrice = totalPrice;
	}
	/**
	 * 获取：总金额
	 */
	public Double getTotalPrice() {
		return totalPrice;
	}
	/**
	 * 设置：创建时间
	 */
	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}
	/**
	 * 获取：创建时间
	 */
	public Date getCreateTime() {
		return createTime;
	}


}
