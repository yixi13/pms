package com.pms.mapper;
import com.pms.entity.EstateOrders;
import com.baomidou.mybatisplus.mapper.BaseMapper;
/**
 * 
 * 
 * @author zyl
 * @email 517770986@qq.com
 * @date 2017-11-01 11:52:52
 */
public interface EstateOrdersMapper extends BaseMapper<EstateOrders> {
	
}
