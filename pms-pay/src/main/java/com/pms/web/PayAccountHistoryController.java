package com.pms.web;

import org.springframework.beans.factory.annotation.Autowired;
import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.plugins.Page;
import org.springframework.web.bind.annotation.*;
import com.pms.controller.BaseController;
import com.pms.service.IPayAccountHistoryService;
import com.pms.entity.PayAccountHistory;
import com.pms.result.JsonResult;
import com.pms.result.ObjectRestResponse;
import com.pms.result.TableResultResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;

@RestController
@RequestMapping("payAccountHistory")
public class PayAccountHistoryController extends BaseController {
    Logger logger = LoggerFactory.getLogger(getClass());
    @Autowired
    IPayAccountHistoryService payAccountHistoryService;

    /**
         * 新增
         * @param entity
         * @return
         */
    @RequestMapping(value = "",method = RequestMethod.POST)
    @ResponseBody
    public ObjectRestResponse<PayAccountHistory> add(@RequestBody PayAccountHistory entity){
            payAccountHistoryService.insert(entity);
        return new ObjectRestResponse<PayAccountHistory>().rel(true);
    }
    /**
     * 修改
     * @param entity
     * @return
     */
    @RequestMapping(value = "/{id}",method = RequestMethod.POST)
    @ResponseBody
    public ObjectRestResponse<PayAccountHistory> update(@RequestBody PayAccountHistory entity){
            payAccountHistoryService.updateById(entity);
        return new ObjectRestResponse<PayAccountHistory>().rel(true);
    }

    /**
 * 删除
 * @param id
 * @return
 */
    @RequestMapping(value = "/{id}",method = RequestMethod.DELETE)
    @ResponseBody
    public ObjectRestResponse<PayAccountHistory> deleteById(@PathVariable int id){
            payAccountHistoryService.deleteById (id);
        return new ObjectRestResponse<PayAccountHistory>().rel(true);
    }


    /**
     * findByID
     * @param id
     * @return
     */
    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    @ResponseBody
    public ObjectRestResponse<PayAccountHistory> findById(@PathVariable int id){
        return new ObjectRestResponse<PayAccountHistory>().rel(true).data( payAccountHistoryService.selectById(id));
    }


    @RequestMapping(value = "/page",method = RequestMethod.GET)
    @ResponseBody
    public TableResultResponse<PayAccountHistory> page(@RequestParam(defaultValue = "10") int limit, @RequestParam(defaultValue = "1")int offset){
        Page<PayAccountHistory> page = new Page<PayAccountHistory>(offset,limit);
        page = payAccountHistoryService.selectPage(page,new EntityWrapper<PayAccountHistory>());
        return new TableResultResponse<PayAccountHistory>(page.getTotal(),page.getRecords());
    }

    @RequestMapping(value = "/all",method = RequestMethod.GET)
    @ResponseBody
    public List<PayAccountHistory> all(){
        return payAccountHistoryService.selectList(null);
    }


}