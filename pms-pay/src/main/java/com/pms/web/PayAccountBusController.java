package com.pms.web;

import org.springframework.beans.factory.annotation.Autowired;
import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.plugins.Page;
import org.springframework.web.bind.annotation.*;
import com.pms.controller.BaseController;
import com.pms.service.IPayAccountBusService;
import com.pms.entity.PayAccountBus;
import com.pms.result.JsonResult;
import com.pms.result.ObjectRestResponse;
import com.pms.result.TableResultResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;

@RestController
@RequestMapping("payAccountBus")
public class PayAccountBusController extends BaseController {
    Logger logger = LoggerFactory.getLogger(getClass());
    @Autowired
    IPayAccountBusService payAccountBusService;

    /**
         * 新增
         * @param entity
         * @return
         */
    @RequestMapping(value = "",method = RequestMethod.POST)
    @ResponseBody
    public ObjectRestResponse<PayAccountBus> add(@RequestBody PayAccountBus entity){
            payAccountBusService.insert(entity);
        return new ObjectRestResponse<PayAccountBus>().rel(true);
    }
    /**
     * 修改
     * @param entity
     * @return
     */
    @RequestMapping(value = "/{id}",method = RequestMethod.POST)
    @ResponseBody
    public ObjectRestResponse<PayAccountBus> update(@RequestBody PayAccountBus entity){
            payAccountBusService.updateById(entity);
        return new ObjectRestResponse<PayAccountBus>().rel(true);
    }

    /**
 * 删除
 * @param id
 * @return
 */
    @RequestMapping(value = "/{id}",method = RequestMethod.DELETE)
    @ResponseBody
    public ObjectRestResponse<PayAccountBus> deleteById(@PathVariable int id){
            payAccountBusService.deleteById (id);
        return new ObjectRestResponse<PayAccountBus>().rel(true);
    }


    /**
     * findByID
     * @param id
     * @return
     */
    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    @ResponseBody
    public ObjectRestResponse<PayAccountBus> findById(@PathVariable int id){
        return new ObjectRestResponse<PayAccountBus>().rel(true).data( payAccountBusService.selectById(id));
    }


    @RequestMapping(value = "/page",method = RequestMethod.GET)
    @ResponseBody
    public TableResultResponse<PayAccountBus> page(@RequestParam(defaultValue = "10") int limit, @RequestParam(defaultValue = "1")int offset){
        Page<PayAccountBus> page = new Page<PayAccountBus>(offset,limit);
        page = payAccountBusService.selectPage(page,new EntityWrapper<PayAccountBus>());
        return new TableResultResponse<PayAccountBus>(page.getTotal(),page.getRecords());
    }

    @RequestMapping(value = "/all",method = RequestMethod.GET)
    @ResponseBody
    public List<PayAccountBus> all(){
        return payAccountBusService.selectList(null);
    }


}