package com.pms.web;

import org.springframework.beans.factory.annotation.Autowired;
import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.plugins.Page;
import org.springframework.web.bind.annotation.*;
import com.pms.controller.BaseController;
import com.pms.service.IPayTradeRecordService;
import com.pms.entity.PayTradeRecord;
import com.pms.result.JsonResult;
import com.pms.result.ObjectRestResponse;
import com.pms.result.TableResultResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;

@RestController
@RequestMapping("payTradeRecord")
public class PayTradeRecordController extends BaseController {
    Logger logger = LoggerFactory.getLogger(getClass());
    @Autowired
    IPayTradeRecordService payTradeRecordService;

    /**
         * 新增
         * @param entity
         * @return
         */
    @RequestMapping(value = "",method = RequestMethod.POST)
    @ResponseBody
    public ObjectRestResponse<PayTradeRecord> add(@RequestBody PayTradeRecord entity){
            payTradeRecordService.insert(entity);
        return new ObjectRestResponse<PayTradeRecord>().rel(true);
    }
    /**
     * 修改
     * @param entity
     * @return
     */
    @RequestMapping(value = "/{id}",method = RequestMethod.POST)
    @ResponseBody
    public ObjectRestResponse<PayTradeRecord> update(@RequestBody PayTradeRecord entity){
            payTradeRecordService.updateById(entity);
        return new ObjectRestResponse<PayTradeRecord>().rel(true);
    }

    /**
 * 删除
 * @param id
 * @return
 */
    @RequestMapping(value = "/{id}",method = RequestMethod.DELETE)
    @ResponseBody
    public ObjectRestResponse<PayTradeRecord> deleteById(@PathVariable int id){
            payTradeRecordService.deleteById (id);
        return new ObjectRestResponse<PayTradeRecord>().rel(true);
    }


    /**
     * findByID
     * @param id
     * @return
     */
    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    @ResponseBody
    public ObjectRestResponse<PayTradeRecord> findById(@PathVariable int id){
        return new ObjectRestResponse<PayTradeRecord>().rel(true).data( payTradeRecordService.selectById(id));
    }


    @RequestMapping(value = "/page",method = RequestMethod.GET)
    @ResponseBody
    public TableResultResponse<PayTradeRecord> page(@RequestParam(defaultValue = "10") int limit, @RequestParam(defaultValue = "1")int offset){
        Page<PayTradeRecord> page = new Page<PayTradeRecord>(offset,limit);
        page = payTradeRecordService.selectPage(page,new EntityWrapper<PayTradeRecord>());
        return new TableResultResponse<PayTradeRecord>(page.getTotal(),page.getRecords());
    }

    @RequestMapping(value = "/all",method = RequestMethod.GET)
    @ResponseBody
    public List<PayTradeRecord> all(){
        return payTradeRecordService.selectList(null);
    }


}