package com.pms.web;

import org.springframework.beans.factory.annotation.Autowired;
import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.plugins.Page;
import org.springframework.web.bind.annotation.*;
import com.pms.controller.BaseController;
import com.pms.service.IPayAccountService;
import com.pms.entity.PayAccount;
import com.pms.result.JsonResult;
import com.pms.result.ObjectRestResponse;
import com.pms.result.TableResultResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;

@RestController
@RequestMapping("payAccount")
public class PayAccountController extends BaseController {
    Logger logger = LoggerFactory.getLogger(getClass());
    @Autowired
    IPayAccountService payAccountService;

    /**
         * 新增
         * @param entity
         * @return
         */
    @RequestMapping(value = "",method = RequestMethod.POST)
    @ResponseBody
    public ObjectRestResponse<PayAccount> add(@RequestBody PayAccount entity){
            payAccountService.insert(entity);
        return new ObjectRestResponse<PayAccount>().rel(true);
    }
    /**
     * 修改
     * @param entity
     * @return
     */
    @RequestMapping(value = "/{id}",method = RequestMethod.POST)
    @ResponseBody
    public ObjectRestResponse<PayAccount> update(@RequestBody PayAccount entity){
            payAccountService.updateById(entity);
        return new ObjectRestResponse<PayAccount>().rel(true);
    }

    /**
 * 删除
 * @param id
 * @return
 */
    @RequestMapping(value = "/{id}",method = RequestMethod.DELETE)
    @ResponseBody
    public ObjectRestResponse<PayAccount> deleteById(@PathVariable int id){
            payAccountService.deleteById (id);
        return new ObjectRestResponse<PayAccount>().rel(true);
    }


    /**
     * findByID
     * @param id
     * @return
     */
    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    @ResponseBody
    public ObjectRestResponse<PayAccount> findById(@PathVariable int id){
        return new ObjectRestResponse<PayAccount>().rel(true).data( payAccountService.selectById(id));
    }


    @RequestMapping(value = "/page",method = RequestMethod.GET)
    @ResponseBody
    public TableResultResponse<PayAccount> page(@RequestParam(defaultValue = "10") int limit, @RequestParam(defaultValue = "1")int offset){
        Page<PayAccount> page = new Page<PayAccount>(offset,limit);
        page = payAccountService.selectPage(page,new EntityWrapper<PayAccount>());
        return new TableResultResponse<PayAccount>(page.getTotal(),page.getRecords());
    }

    @RequestMapping(value = "/all",method = RequestMethod.GET)
    @ResponseBody
    public List<PayAccount> all(){
        return payAccountService.selectList(null);
    }


}