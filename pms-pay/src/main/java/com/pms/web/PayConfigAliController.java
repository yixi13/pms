package com.pms.web;

import org.springframework.beans.factory.annotation.Autowired;
import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.plugins.Page;
import org.springframework.web.bind.annotation.*;
import com.pms.controller.BaseController;
import com.pms.service.IPayConfigAliService;
import com.pms.entity.PayConfigAli;
import com.pms.result.JsonResult;
import com.pms.result.ObjectRestResponse;
import com.pms.result.TableResultResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import java.util.List;

@RestController
@RequestMapping("payConfigAli")
public class PayConfigAliController extends BaseController {
    Logger logger = LoggerFactory.getLogger(getClass());
    @Autowired
    IPayConfigAliService payConfigAliService;

    /**
         * 新增
         * @param entity
         * @return
         */
    @RequestMapping(value = "",method = RequestMethod.POST)
    @ResponseBody
    public ObjectRestResponse<PayConfigAli> add(@RequestBody PayConfigAli entity){
            payConfigAliService.insert(entity);
        return new ObjectRestResponse<PayConfigAli>().rel(true);
    }
    /**
     * 修改
     * @param entity
     * @return
     */
    @RequestMapping(value = "/{id}",method = RequestMethod.POST)
    @ResponseBody
    public ObjectRestResponse<PayConfigAli> update(@RequestBody PayConfigAli entity){
            payConfigAliService.updateById(entity);
        return new ObjectRestResponse<PayConfigAli>().rel(true);
    }

    /**
 * 删除
 * @param id
 * @return
 */
    @RequestMapping(value = "/{id}",method = RequestMethod.DELETE)
    @ResponseBody
    public ObjectRestResponse<PayConfigAli> deleteById(@PathVariable int id){
            payConfigAliService.deleteById (id);
        return new ObjectRestResponse<PayConfigAli>().rel(true);
    }


    /**
     * findByID
     * @param id
     * @return
     */
    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    @ResponseBody
    public ObjectRestResponse<PayConfigAli> findById(@PathVariable int id){
        return new ObjectRestResponse<PayConfigAli>().rel(true).data( payConfigAliService.selectById(id));
    }


    @RequestMapping(value = "/page",method = RequestMethod.GET)
    @ResponseBody
    public TableResultResponse<PayConfigAli> page(@RequestParam(defaultValue = "10") int limit, @RequestParam(defaultValue = "1")int offset){
        Page<PayConfigAli> page = new Page<PayConfigAli>(offset,limit);
        page = payConfigAliService.selectPage(page,new EntityWrapper<PayConfigAli>());
        return new TableResultResponse<PayConfigAli>(page.getTotal(),page.getRecords());
    }

    @RequestMapping(value = "/all",method = RequestMethod.GET)
    @ResponseBody
    public List<PayConfigAli> all(){
        return payConfigAliService.selectList(null);
    }


}