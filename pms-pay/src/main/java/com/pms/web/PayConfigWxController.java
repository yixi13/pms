package com.pms.web;

import org.springframework.beans.factory.annotation.Autowired;
import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.plugins.Page;
import org.springframework.web.bind.annotation.*;
import com.pms.controller.BaseController;
import com.pms.service.IPayConfigWxService;
import com.pms.entity.PayConfigWx;
import com.pms.result.JsonResult;
import com.pms.result.ObjectRestResponse;
import com.pms.result.TableResultResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;

@RestController
@RequestMapping("payConfigWx")
public class PayConfigWxController extends BaseController {
    Logger logger = LoggerFactory.getLogger(getClass());
    @Autowired
    IPayConfigWxService payConfigWxService;

    /**
         * 新增
         * @param entity
         * @return
         */
    @RequestMapping(value = "",method = RequestMethod.POST)
    @ResponseBody
    public ObjectRestResponse<PayConfigWx> add(@RequestBody PayConfigWx entity){
            payConfigWxService.insert(entity);
        return new ObjectRestResponse<PayConfigWx>().rel(true);
    }
    /**
     * 修改
     * @param entity
     * @return
     */
    @RequestMapping(value = "/{id}",method = RequestMethod.POST)
    @ResponseBody
    public ObjectRestResponse<PayConfigWx> update(@RequestBody PayConfigWx entity){
            payConfigWxService.updateById(entity);
        return new ObjectRestResponse<PayConfigWx>().rel(true);
    }

    /**
 * 删除
 * @param id
 * @return
 */
    @RequestMapping(value = "/{id}",method = RequestMethod.DELETE)
    @ResponseBody
    public ObjectRestResponse<PayConfigWx> deleteById(@PathVariable int id){
            payConfigWxService.deleteById (id);
        return new ObjectRestResponse<PayConfigWx>().rel(true);
    }


    /**
     * findByID
     * @param id
     * @return
     */
    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    @ResponseBody
    public ObjectRestResponse<PayConfigWx> findById(@PathVariable int id){
        return new ObjectRestResponse<PayConfigWx>().rel(true).data( payConfigWxService.selectById(id));
    }


    @RequestMapping(value = "/page",method = RequestMethod.GET)
    @ResponseBody
    public TableResultResponse<PayConfigWx> page(@RequestParam(defaultValue = "10") int limit, @RequestParam(defaultValue = "1")int offset){
        Page<PayConfigWx> page = new Page<PayConfigWx>(offset,limit);
        page = payConfigWxService.selectPage(page,new EntityWrapper<PayConfigWx>());
        return new TableResultResponse<PayConfigWx>(page.getTotal(),page.getRecords());
    }

    @RequestMapping(value = "/all",method = RequestMethod.GET)
    @ResponseBody
    public List<PayConfigWx> all(){
        return payConfigWxService.selectList(null);
    }


}