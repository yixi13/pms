package com.pms.web;

import org.springframework.beans.factory.annotation.Autowired;
import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.plugins.Page;
import org.springframework.web.bind.annotation.*;
import com.pms.controller.BaseController;
import com.pms.service.IPayUserInfoService;
import com.pms.entity.PayUserInfo;
import com.pms.result.JsonResult;
import com.pms.result.ObjectRestResponse;
import com.pms.result.TableResultResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;

@RestController
@RequestMapping("payUserInfo")
public class PayUserInfoController extends BaseController {
    Logger logger = LoggerFactory.getLogger(getClass());
    @Autowired
    IPayUserInfoService payUserInfoService;

    /**
         * 新增
         * @param entity
         * @return
         */
    @RequestMapping(value = "",method = RequestMethod.POST)
    @ResponseBody
    public ObjectRestResponse<PayUserInfo> add(@RequestBody PayUserInfo entity){
            payUserInfoService.insert(entity);
        return new ObjectRestResponse<PayUserInfo>().rel(true);
    }
    /**
     * 修改
     * @param entity
     * @return
     */
    @RequestMapping(value = "/{id}",method = RequestMethod.POST)
    @ResponseBody
    public ObjectRestResponse<PayUserInfo> update(@RequestBody PayUserInfo entity){
            payUserInfoService.updateById(entity);
        return new ObjectRestResponse<PayUserInfo>().rel(true);
    }

    /**
 * 删除
 * @param id
 * @return
 */
    @RequestMapping(value = "/{id}",method = RequestMethod.DELETE)
    @ResponseBody
    public ObjectRestResponse<PayUserInfo> deleteById(@PathVariable int id){
            payUserInfoService.deleteById (id);
        return new ObjectRestResponse<PayUserInfo>().rel(true);
    }


    /**
     * findByID
     * @param id
     * @return
     */
    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    @ResponseBody
    public ObjectRestResponse<PayUserInfo> findById(@PathVariable int id){
        return new ObjectRestResponse<PayUserInfo>().rel(true).data( payUserInfoService.selectById(id));
    }


    @RequestMapping(value = "/page",method = RequestMethod.GET)
    @ResponseBody
    public TableResultResponse<PayUserInfo> page(@RequestParam(defaultValue = "10") int limit, @RequestParam(defaultValue = "1")int offset){
        Page<PayUserInfo> page = new Page<PayUserInfo>(offset,limit);
        page = payUserInfoService.selectPage(page,new EntityWrapper<PayUserInfo>());
        return new TableResultResponse<PayUserInfo>(page.getTotal(),page.getRecords());
    }

    @RequestMapping(value = "/all",method = RequestMethod.GET)
    @ResponseBody
    public List<PayUserInfo> all(){
        return payUserInfoService.selectList(null);
    }


}