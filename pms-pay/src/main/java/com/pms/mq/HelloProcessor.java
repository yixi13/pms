package com.pms.mq;

import com.pms.constant.MQConstant;
import org.springframework.amqp.rabbit.annotation.RabbitHandler;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Component;

/**
 * Created by ljb on 2017/11/15.
 * 消息队列消费者
 */
@Component
@RabbitListener(queues = MQConstant.QUEUE_NAME_PAY)
public class HelloProcessor {
    @RabbitHandler
    public void process(String content) {
        System.out.println("支付模块消息:" + content);
    }
}
