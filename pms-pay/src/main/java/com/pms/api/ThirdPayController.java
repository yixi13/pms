package com.pms.api;

import com.pms.controller.BaseController;
import com.pms.entity.Product;
import com.pms.exception.R;
import com.pms.constant.MQConstant;
import com.pms.mq.jms.MqPayNotify;
import com.pms.service.IThirdPayService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.log4j.Log4j;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * Created by ljb on 2017/11/3.
 */
@RestController
@RequestMapping("/thirdPay")
@Api(value="门查询相关接口",description = "门查询相关接口")
@Log4j
public class ThirdPayController  extends BaseController {

    @Autowired
    private IThirdPayService iThirdPayService;
    @Autowired
    private RabbitTemplate rabbitTemplate;

    @ApiOperation(value = "测试方法")
    @RequestMapping(value = "/test",method = RequestMethod.POST
//            ,produces={"application/json;charset=UTF-8"}
            )
//    @ApiImplicitParams({
//            @ApiImplicitParam(name="pageSize",value="请求条数",required = false,dataType = "int",paramType="form"),
//            @ApiImplicitParam(name="pageNum",value="请求页数",required = false,dataType = "int",paramType="form")
//    })
    public Object test(
            @RequestBody Product product
    ) {
        logger.info("123");
//        PayWayEnum payWay = PayWayEnum.getEnumByName("WX_JSAPI");
//        iThirdPayService.initPay(product);
        MqPayNotify mq = new MqPayNotify();

//        rabbitTemplate.convertAndSend(MQConstant.HELLO_QUEUE_NAME,"111");
        rabbitTemplate.convertAndSend(MQConstant.DEFAULT_EXCHANGE,MQConstant.HELLO_QUEUE_NAME,"222");
        rabbitTemplate.convertAndSend(MQConstant.DEFAULT_EXCHANGE,MQConstant.QUEUE_NAME_ESTATE,"物业来了");
        rabbitTemplate.convertAndSend(MQConstant.DEFAULT_EXCHANGE,MQConstant.QUEUE_NAME_PAY,"支付来了");
//        mq.send("22222 test");
        logger.info("345");
        return R.ok();
    }


}
