package com.pms.core;

import org.springframework.util.ClassUtils;

/**
 *  支付常量类
 * @author zyl
 * @create 2017-10-27 15:58
 **/
public class PayConstant {
    public final static String PAY_CHANNEL_WX_JSAPI = "WX_JSAPI"; 				// 微信公众号支付
    public final static String PAY_CHANNEL_WX_NATIVE = "WX_NATIVE";				// 微信原生扫码支付
    public final static String PAY_CHANNEL_WX_APP = "WX_APP";					// 微信APP支付
    public final static String PAY_CHANNEL_WX_MWEB = "WX_MWEB";					// 微信H5支付

    public final static String TRANS_CHANNEL_WX_APP = "TRANS_WX_APP"; 			// 微信APP转账
    public final static String TRANS_CHANNEL_WX_JSAPI = "TRANS_WX_JSAPI"; 		// 微信公众号转账

    public final static String PAY_CHANNEL_IAP = "IAP";							// 苹果应用内支付

    public final static String PAY_CHANNEL_ALIPAY_MOBILE = "ALIPAY_MOBILE";		// 支付宝移动支付
    public final static String PAY_CHANNEL_ALIPAY_PC = "ALIPAY_PC";	    		// 支付宝PC支付
    public final static String PAY_CHANNEL_ALIPAY_WAP = "ALIPAY_WAP";	    	// 支付宝WAP支付
    public final static String PAY_CHANNEL_ALIPAY_QR = "ALIPAY_QR";	    	// 支付宝当面付之扫码支付


    public static final String SF_FILE_SEPARATOR = System.getProperty("file.separator");//文件分隔符
    public static final String SF_LINE_SEPARATOR = System.getProperty("line.separator");//行分隔符
    public static final String SF_PATH_SEPARATOR = System.getProperty("path.separator");//路径分隔符

    public static final String QRCODE_PATH = ClassUtils.getDefaultClassLoader().getResource("static").getPath()+SF_FILE_SEPARATOR+"qrcode";

    //微信账单 相关字段 用于load文本到数据库
    public static final String WEIXIN_BILL = "tradetime, ghid, mchid, submch, deviceid, wxorder, bzorder, openid, tradetype, tradestatus, bank, currency, totalmoney, redpacketmoney, wxrefund, bzrefund, refundmoney, redpacketrefund, refundtype, refundstatus, productname, bzdatapacket, fee, rate";

    public static final String PATH_BASE_INFO_XML = SF_FILE_SEPARATOR+"WEB-INF"+SF_FILE_SEPARATOR+"xmlConfig"+SF_FILE_SEPARATOR;

    public static final String CURRENT_USER = "UserInfo";

    public static final String SUCCESS = "success";

    public static final String FAIL = "fail";
}
