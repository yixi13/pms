package com.pms.core;

import io.swagger.models.auth.In;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by ljb on 2017/11/8.
 */
public enum PayWayEnum {
    WX_JSAPI(11,"微信公众号支付"),
    WX_NATIVE(12,"微信原生扫码支付"),
    WX_APP(13,"微信APP支付"),
    WX_MWEB(14,"微信H5支付"),
    WX_TRANS_APP(15,"微信APP转账"),
    WX_TRANS_JSAPI(16,"微信公众号转账"),

    ALIPAY_MOBILE(21,"支付宝APP支付"),
    ALIPAY_PC(22,"支付宝PC支付"),
    ALIPAY_WAP(23,"支付宝WAP支付"),
    ALIPAY_QR(24,"支付宝扫码支付"),

    IAP(31,"苹果支付");
    /**
     *  编码
     */
    private Integer code;
    /** 描述 */
    private String desc;

    private PayWayEnum(Integer code,String desc) {
        this.code = code;
        this.desc = desc;
    }

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }


    public static PayWayEnum getEnumByName(String name) {
        PayWayEnum[] arry = PayWayEnum.values();
        for (int i = 0; i < arry.length; i++) {
            if (arry[i].name().equalsIgnoreCase(name)) {
                return arry[i];
            }
        }
        return null;
    }
    public static PayWayEnum getEnumByCode(Integer code) {
        PayWayEnum[] arry = PayWayEnum.values();
        for (int i = 0; i < arry.length; i++) {
            if (arry[i].code == code) {
                return arry[i];
            }
        }
        return null;
    }

}
