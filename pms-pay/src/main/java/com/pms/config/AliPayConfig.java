package com.pms.config;

import com.alipay.api.AlipayClient;
import com.alipay.api.DefaultAlipayClient;
import com.alipay.demo.trade.config.Configs;
import com.alipay.demo.trade.service.AlipayTradeService;
import com.alipay.demo.trade.service.impl.AlipayTradeServiceImpl;
import com.pms.entity.PayConfigAli;

/**
 * @author zyl
 * @create 2017-11-01 14:10
 **/
public class AliPayConfig {

    /**
     * 参数类型
     */
    public static String PARAM_TYPE = "json";
    /**
     * 编码
     */
    public static String CHARSET = "UTF-8";

    public  static String alUrl="https://openapi.alipay.com/gateway.do";

    public AlipayClient getAlipayClient(PayConfigAli payConfigAli){
        AlipayClient alipayClient = new DefaultAlipayClient(
                alUrl, payConfigAli.getAppId(),
                payConfigAli.getPrivateKey(), PARAM_TYPE, CHARSET,
                payConfigAli.getAliPublicKey(),payConfigAli.getSignType());

        return alipayClient;
    }

    /**
     * 类级的内部类，也就是静态的成员式内部类，该内部类的实例与外部类的实例
     * 没有绑定关系，而且只有被调用到才会装载，从而实现了延迟加载
     */
//    private static class SingletonHolder{
//        /**
//         * 静态初始化器，由JVM来保证线程安全
//         */
//        private  static AlipayClient alipayClient = new DefaultAlipayClient(
//                alUrl, payConfigAli),
//                Configs.getPrivateKey(), PARAM_TYPE, CHARSET,
//                Configs.getAlipayPublicKey(),"RSA2");
//
//        private  static AlipayTradeService tradeService = new AlipayTradeServiceImpl.ClientBuilder().build();
//    }
    /**
     * 支付宝APP请求客户端实例
     * @return  AlipayClient
     *
     */
//    public static AlipayClient getAlipayClient(){
//        return SingletonHolder.alipayClient;
//    }
    /**
     * 电脑端预下单
     * @return  AlipayTradeService
     *
     */
    public static AlipayTradeService getAlipayTradeService(){
         AlipayTradeService tradeService = new AlipayTradeServiceImpl.ClientBuilder().build();
        return tradeService;
}
}
