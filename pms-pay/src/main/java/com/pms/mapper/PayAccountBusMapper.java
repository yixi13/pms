package com.pms.mapper;
import com.pms.entity.PayAccountBus;
import com.baomidou.mybatisplus.mapper.BaseMapper;
/**
 * 
 * 
 * @author ljb
 * @email 517770986@qq.com
 * @date 2017-11-03 10:48:47
 */
public interface PayAccountBusMapper extends BaseMapper<PayAccountBus> {
	
}
