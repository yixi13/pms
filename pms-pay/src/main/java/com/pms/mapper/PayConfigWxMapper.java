package com.pms.mapper;
import com.pms.entity.PayConfigWx;
import com.baomidou.mybatisplus.mapper.BaseMapper;
/**
 * 
 * 
 * @author ljb
 * @email 517770986@qq.com
 * @date 2017-11-03 10:48:48
 */
public interface PayConfigWxMapper extends BaseMapper<PayConfigWx> {
	
}
