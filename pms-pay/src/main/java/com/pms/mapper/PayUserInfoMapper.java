package com.pms.mapper;
import com.pms.entity.PayUserInfo;
import com.baomidou.mybatisplus.mapper.BaseMapper;
/**
 * 支付用户基本信息表
 * 
 * @author zyl
 * @email 517770986@qq.com
 * @date 2017-10-30 14:13:53
 */
public interface PayUserInfoMapper extends BaseMapper<PayUserInfo> {
	
}
