package com.pms.rpc.client;

import com.pms.entity.AgencyInfo;
import com.pms.entity.UserInfo;
import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.util.Map;

/**
 * Created by ljb on 2017/11/3.
 */
@FeignClient("pms-system")
@RequestMapping("api")
public interface RpcTestService {
    @RequestMapping(value = "/sys/rpcTest", method = RequestMethod.GET)
    public String rpcTest();

    @RequestMapping(value = "/user/username/{username}", method = RequestMethod.GET)
    UserInfo getUserByUsername(@PathVariable("username") String username);
}
