package com.pms.rpc.client;

import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * Created by ljb on 2017/11/7.
 */
@FeignClient("pms-pay")
@RequestMapping("api")
public interface RpcClientTest {

}
