package com.pms.rpc.client;

import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * Created by ljb on 2017/11/14.
 */
@FeignClient("pms-estate")
@RequestMapping("api")
public interface EstateOrdersRpcClient {
    /**订单金额验证方法*/
    @PostMapping(value = "/estate/checkOrders")
    public String checkOrders(String innerNo,String totalFee);
    /**支付完成回调修改订单状态*/
    @PostMapping(value = "/estate/updateOrderState")
    public String updateOrdersState(String innerNo,String state);
}
