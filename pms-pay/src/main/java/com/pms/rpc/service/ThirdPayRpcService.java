package com.pms.rpc.service;

import com.pms.controller.BaseController;
import com.pms.exception.R;
import com.pms.exception.RRException;
import com.pms.validator.Assert;
import com.xiaoleilu.hutool.util.NumberUtil;
import com.xiaoleilu.hutool.util.StrUtil;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.math.BigDecimal;
import java.util.SortedMap;

/**
 * Created by ljb on 2017/11/7.
 */
@RestController
@Api(value = "RPC内部支付接口", tags = {"RPC内部支付接口"})
@RequestMapping("api")
public class ThirdPayRpcService extends BaseController{

    @PostMapping("/initPay")
    @ApiOperation(value = "APP支付方法")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "memberId", value = "用户id", required = true, dataType = "Long", paramType = "query"),
            @ApiImplicitParam(name = "shopId", value = "店铺id", required = true, dataType = "Long", paramType = "query"),
            @ApiImplicitParam(name = "orderNo", value = "订单编号", required = true, dataType = "String", paramType = "query"),
            @ApiImplicitParam(name = "amount", value = "金额", required = true, dataType = "String", paramType = "query"),
            @ApiImplicitParam(name = "productInfo", value = "支付备注信息", required = true, dataType = "Integer", paramType = "query"),
            @ApiImplicitParam(name = "payWay", value = "支付方式:WXPAY/ALIPAY/DJPAY（代金券）/XJPAY(钱包现金)/SCPAY（商超券）/其它待定", required = true, dataType = "String", paramType = "query"),
            @ApiImplicitParam(name = "payType", value = "支付类型:APP/SHOP", required = true, dataType = "String", paramType = "query"),
            @ApiImplicitParam(name = "pass", value = "支付密码:系统内部支付时使用", required = false, dataType = "String", paramType = "query")
    })
    public R initPay(Long memberId, String orderNo, String amount, String productInfo,
                     String payWay ,String payType,String pass,String shopId) {

        return R.ok();
    }
}
