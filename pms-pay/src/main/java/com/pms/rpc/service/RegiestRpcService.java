package com.pms.rpc.service;

import com.pms.entity.PayAccount;
import com.pms.entity.PayUserInfo;
import com.pms.exception.R;
import com.pms.service.IPayAccountService;
import com.pms.service.IPayUserInfoService;
import com.pms.util.NuType;
import com.pms.util.NumberUtil;
import com.pms.validator.Assert;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.math.BigDecimal;
import java.util.Date;

/**
 * Created by ljb on 2017/11/14.
 */
@RestController
@Api(value = "支付系统注册rpc接口", tags = {"支付系统注册rpc接口"})
@RequestMapping("rpc")
public class RegiestRpcService {
    @Autowired
    private IPayUserInfoService iPayUserInfoService;
    @Autowired
    private IPayAccountService iPayAccountService;

    @PostMapping("/pay/regiest")
    @ApiOperation(value = "支付系统注册方法")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "userName", value = "用户名", required = true, dataType = "Long", paramType = "query"),
            @ApiImplicitParam(name = "pass", value = "支付密码:系统内部支付时使用", required = false, dataType = "String", paramType = "query")
    })
    public R payRegiest(String userName){
        Assert.isNull(userName, "userName 不能为空");

        String accountNo = NumberUtil.getNu(NuType.payAccountNo);
        String userNo = NumberUtil.getNu(NuType.payUserNo);
        PayUserInfo userInfo = new PayUserInfo()
                .setStatus(1)
                .setAccountNo(accountNo)
                .setCreateTime(new Date())
                .setUserName(userName)
                .setUserNo(userNo);
        iPayUserInfoService.insert(userInfo);
        PayAccount payAccount = new PayAccount()
                .setAccountNo(accountNo)
                .setUserNo(userNo)
                .setBalance(new BigDecimal(0))
                .setUnbalance(new BigDecimal(0))
                .setSecurityMoney(new BigDecimal(0))
                .setTodayExpend(new BigDecimal(0))
                .setTodayIncome(new BigDecimal(0))
                .setTotalExpend(new BigDecimal(0))
                .setTotalIncome(new BigDecimal(0))
                .setAccountType(1)
                .setCreateTime(new Date());
        iPayAccountService.insert(payAccount);
        return R.ok();
    }
}
