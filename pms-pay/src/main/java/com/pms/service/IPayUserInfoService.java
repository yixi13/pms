package com.pms.service;
import com.baomidou.mybatisplus.service.IService;
import com.pms.entity.PayUserInfo;
/**
 * 支付用户基本信息表
 *
 * @author zyl
 * @email 517770986@qq.com
 * @date 2017-10-31 10:09:45
 */

public interface IPayUserInfoService extends  IService<PayUserInfo> {

}