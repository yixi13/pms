package com.pms.service;
import com.baomidou.mybatisplus.service.IService;
import com.pms.entity.PayAccountBus;
import com.pms.entity.PayConfigAli;
import com.pms.entity.PayConfigWx;

/**
 * 
 *
 * @author ljb
 * @email 517770986@qq.com
 * @date 2017-11-03 10:48:47
 */

public interface IPayAccountBusService extends  IService<PayAccountBus> {
    /**
     * 更具用户账号查询对应的支付宝配置
     * @param accountNo
     * @return
     */
    public PayConfigAli getAliConfigByNo(String accountNo);

    /**
     *  更具用户账号查询对应的微信配置
     * @param accountNo
     * @return
     */
    public PayConfigWx getWxConfigByNo(String accountNo);
}