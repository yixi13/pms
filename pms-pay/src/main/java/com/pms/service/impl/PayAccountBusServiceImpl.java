package com.pms.service.impl;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.pms.entity.PayConfigAli;
import com.pms.entity.PayConfigWx;
import com.pms.service.IAliPayService;
import com.pms.service.IPayConfigAliService;
import com.pms.service.IPayConfigWxService;
import com.pms.validator.Assert;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.pms.entity.PayAccountBus;
import com.pms.mapper.PayAccountBusMapper;
import com.pms.service.IPayAccountBusService;
/**
 * 
 *
 * @author ljb
 * @email 517770986@qq.com
 * @date 2017-11-03 10:48:47
 */
@Service
public class PayAccountBusServiceImpl extends ServiceImpl<PayAccountBusMapper,PayAccountBus> implements IPayAccountBusService {

    @Autowired
    private IPayConfigAliService iPayConfigAliService;
    @Autowired
    private IPayConfigWxService iPayConfigWxService;

    /**
     * 更具用户账号查询对应的支付宝配置
     *
     * @param accountNo
     * @return
     */
    @Override
    public PayConfigAli getAliConfigByNo(String accountNo) {
        Assert.isNull(accountNo, "accountNo 不能为空");

        PayAccountBus payAccountBus = this.selectOne(new EntityWrapper<PayAccountBus>().eq("account_no",accountNo));
        PayConfigAli payConfigAli = null;
        if (payAccountBus != null){
            if (payAccountBus.getPayType() == 2) {
                payConfigAli = iPayConfigAliService.selectOne(
                        new EntityWrapper<PayConfigAli>().eq("account_no", accountNo));
            }
        }
        if (payConfigAli == null){
            payConfigAli = iPayConfigAliService.selectById(1);
        }
        return payConfigAli;
    }

    /**
     * 更具用户账号查询对应的微信配置
     *
     * @param accountNo
     * @return
     */
    @Override
    public PayConfigWx getWxConfigByNo(String accountNo) {
        Assert.isNull(accountNo, "accountNo 不能为空");

        PayAccountBus payAccountBus = this.selectOne(new EntityWrapper<PayAccountBus>().eq("account_no",accountNo));
        PayConfigWx payConfigWx = null;
        if (payAccountBus != null){
            if (payAccountBus.getPayType() == 2) {
                payConfigWx = iPayConfigWxService.selectOne(
                        new EntityWrapper<PayConfigWx>().eq("account_no", accountNo));
            }
        }
        if (payConfigWx == null){
            payConfigWx = iPayConfigWxService.selectById(1);
        }
        return payConfigWx;
    }
}