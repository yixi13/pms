package com.pms.service.impl;


import com.alipay.api.AlipayApiException;
import com.alipay.api.AlipayClient;
import com.alipay.api.AlipayResponse;
import com.alipay.api.domain.AlipayTradeAppPayModel;
import com.alipay.api.internal.util.AlipaySignature;
import com.alipay.api.request.*;
import com.alipay.api.response.AlipayDataDataserviceBillDownloadurlQueryResponse;
import com.alipay.api.response.AlipayTradeAppPayResponse;
import com.alipay.api.response.AlipayTradeCloseResponse;
import com.alipay.api.response.AlipayTradePrecreateResponse;
import com.alipay.demo.trade.config.Configs;
import com.alipay.demo.trade.config.Constants;
import com.alipay.demo.trade.model.ExtendParams;
import com.alipay.demo.trade.model.builder.AlipayTradePrecreateRequestBuilder;
import com.alipay.demo.trade.model.builder.AlipayTradeRefundRequestBuilder;
import com.alipay.demo.trade.model.result.AlipayF2FPrecreateResult;
import com.alipay.demo.trade.model.result.AlipayF2FRefundResult;
import com.alipay.demo.trade.utils.ZxingUtils;
import com.pms.config.AliPayConfig;
import com.pms.core.PayConstant;
import com.pms.entity.PayConfigAli;
import com.pms.entity.Product;
import com.pms.service.IAliPayService;
import com.pms.util.BigdecimalUtil;
import net.sf.json.JSONObject;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.io.File;
import java.util.Map;

/**
 * @author zyl
 * @create 2017-11-01 10:58
 **/
@Service
public class AliPayServiceImpl implements IAliPayService {
    @Value("${alipay.notify.url}")
    private String notify_url;

    Logger logger = LoggerFactory.getLogger(getClass());
    @Override
    public String aliPay(Product product) {
        logger.info("订单号：{}生成支付宝支付码",product.getOutTradeNo());
        String  message = PayConstant.SUCCESS;
        //二维码存放路径
        System.out.println(PayConstant.QRCODE_PATH);
        String imgPath= PayConstant.QRCODE_PATH+PayConstant.SF_FILE_SEPARATOR+product.getOutTradeNo()+".png";
        String outTradeNo = product.getOutTradeNo();
        String subject = product.getSubject();
        String totalAmount =  BigdecimalUtil.divide(product.getTotalFee(), "100").toString();
        // 如果该字段为空，则默认为与支付宝签约的商户的PID，也就是appid对应的PID
        String sellerId = "";
        // (必填) 商户门店编号，通过门店号和商家后台可以配置精准到门店的折扣信息，详询支付宝技术支持
        String storeId = "test_store_id";
        // 业务扩展参数，目前可添加由支付宝分配的系统商编号(通过setSysServiceProviderId方法)，详情请咨询支付宝技术支持
        ExtendParams extendParams = new ExtendParams();
        extendParams.setSysServiceProviderId("2088100200300400500");
        // 订单描述，可以对交易或商品进行一个详细地描述，比如填写"购买商品2件共15.00元"
        String body = product.getBody();
        // 支付超时，定义为120分钟
        String timeoutExpress = "120m";
        // 创建扫码支付请求builder，设置请求参数
        AlipayTradePrecreateRequestBuilder builder = new AlipayTradePrecreateRequestBuilder()
                .setSubject(subject)
                .setTotalAmount(totalAmount)
                .setOutTradeNo(outTradeNo)
                .setSellerId(sellerId)
                .setBody(body)//128长度 --附加信息
                .setStoreId(storeId)
                .setExtendParams(extendParams)
                .setTimeoutExpress(timeoutExpress)
                .setNotifyUrl(notify_url);//支付宝服务器主动通知商户服务器里指定的页面http路径,根据需要设置

        AlipayF2FPrecreateResult result = AliPayConfig.getAlipayTradeService().tradePrecreate(builder);
        switch (result.getTradeStatus()) {
            case SUCCESS:
                logger.info("支付宝预下单成功: )");

                AlipayTradePrecreateResponse response = result.getResponse();
                dumpResponse(response);
                ZxingUtils.getQRCodeImge(response.getQrCode(), 256, imgPath);
                break;

            case FAILED:
                logger.info("支付宝预下单失败!!!");
                message = PayConstant.FAIL;
                break;

            case UNKNOWN:
                logger.info("系统异常，预下单状态未知!!!");
                message = PayConstant.FAIL;
                break;

            default:
                logger.info("不支持的交易状态，交易返回异常!!!");
                message = PayConstant.FAIL;
                break;
        }
        return message;
    }
    // 简单打印应答
    private void dumpResponse(AlipayResponse response) {
        if (response != null) {
            logger.info(String.format("code:%s, msg:%s", response.getCode(), response.getMsg()));
            if (StringUtils.isNotEmpty(response.getSubCode())) {
                logger.info(String.format("subCode:%s, subMsg:%s", response.getSubCode(), response.getSubMsg()));
            }
            logger.info("body:" + response.getBody());
        }
    }
    @Override
    public String aliRefund(Product product) {
        logger.info("订单号："+product.getOutTradeNo()+"支付宝退款");
        String  message = Constants.SUCCESS;
        // (必填) 外部订单号，需要退款交易的商户外部订单号
        String outTradeNo = product.getOutTradeNo();
        // (必填) 退款金额，该金额必须小于等于订单的支付金额，单位为元
        String refundAmount = BigdecimalUtil.divide(product.getTotalFee(), "100").toString();

        // (必填) 退款原因，可以说明用户退款原因，方便为商家后台提供统计
        String refundReason = "正常退款，用户买多了";

        // (必填) 商户门店编号，退款情况下可以为商家后台提供退款权限判定和统计等作用，详询支付宝技术支持
        String storeId = "test_store_id";

        // 创建退款请求builder，设置请求参数
        AlipayTradeRefundRequestBuilder builder = new AlipayTradeRefundRequestBuilder()
                .setOutTradeNo(outTradeNo)
                .setRefundAmount(refundAmount)
                .setRefundReason(refundReason)
                //.setOutRequestNo(outRequestNo)
                .setStoreId(storeId);

        AlipayF2FRefundResult result = AliPayConfig.getAlipayTradeService().tradeRefund(builder);
        switch (result.getTradeStatus()) {
            case SUCCESS:
                logger.info("支付宝退款成功: )");
                break;

            case FAILED:
                logger.info("支付宝退款失败!!!");
                message = PayConstant.FAIL;
                break;

            case UNKNOWN:
                logger.info("系统异常，订单退款状态未知!!!");
                message = PayConstant.FAIL;
                break;

            default:
                logger.info("不支持的交易状态，交易返回异常!!!");
                message = PayConstant.FAIL;
                break;
        }
        return message;
    }

    @Override
    public String aliCloseOrder(Product product,PayConfigAli payConfigAli) {
        logger.info("订单号："+product.getOutTradeNo()+"支付宝关闭订单");
        String  message = Constants.SUCCESS;
        try {
            String imgPath= PayConstant.QRCODE_PATH+PayConstant.SF_FILE_SEPARATOR+"alipay_"+product.getOutTradeNo()+".png";
            File file = new File(imgPath);
            if(file.exists()){
                AliPayConfig aliPayConfig = new AliPayConfig();
                AlipayClient alipayClient=  aliPayConfig.getAlipayClient(payConfigAli);
                AlipayTradeCloseRequest request = new AlipayTradeCloseRequest();
                request.setBizContent("{" +
                        "    \"out_trade_no\":\""+product.getOutTradeNo()+"\"" +
                        "  }");
                AlipayTradeCloseResponse response = alipayClient.execute(request);
                if(response.isSuccess()){//扫码未支付的情况
                    logger.info("订单号："+product.getOutTradeNo()+"支付宝关闭订单成功并删除支付二维码");
                    file.delete();
                }else{
                    if("ACQ.TRADE_NOT_EXIST".equals(response.getSubCode())){
                        logger.info("订单号："+product.getOutTradeNo()+response.getSubMsg()+"(预下单 未扫码的情况)");
                    }else if("ACQ.TRADE_STATUS_ERROR".equals(response.getSubCode())){
                        logger.info("订单号："+product.getOutTradeNo()+response.getSubMsg());
                    }else{
                        logger.info("订单号："+product.getOutTradeNo()+"支付宝关闭订单失败"+response.getSubCode()+response.getSubMsg());
                        message = PayConstant.FAIL;
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            message = PayConstant.FAIL;
            logger.info("订单号："+product.getOutTradeNo()+"支付宝关闭订单异常");
        }
        return message;
    }

    @Override
    public String downloadBillUrl(String billDate, String billType,PayConfigAli payConfigAli) {
        logger.info("获取支付宝订单地址:"+billDate);
        String downloadBillUrl = "";
        try {
            AlipayDataDataserviceBillDownloadurlQueryRequest request = new AlipayDataDataserviceBillDownloadurlQueryRequest();
            request.setBizContent("{" + "    \"bill_type\":\"trade\","
                    + "    \"bill_date\":\"2016-12-26\"" + "  }");
            AliPayConfig aliPayConfig = new AliPayConfig();
            AlipayClient alipayClient=  aliPayConfig.getAlipayClient(payConfigAli);

            AlipayDataDataserviceBillDownloadurlQueryResponse response
                    = alipayClient.execute(request);
            if (response.isSuccess()) {
                logger.info("获取支付宝订单地址成功:"+billDate);
                downloadBillUrl = response.getBillDownloadUrl();//获取下载地
            } else {
                logger.info("获取支付宝订单地址失败"+response.getSubMsg()+":"+billDate);
            }
        } catch (Exception e) {
            e.printStackTrace();
            logger.info("获取支付宝订单地址异常:"+billDate);
        }
        return downloadBillUrl;
    }

    @Override
    public String aliPayMobile(Product product, PayConfigAli payConfigAli) {
        logger.info("支付宝手机支付下单");
        AlipayTradeWapPayRequest alipayRequest = new AlipayTradeWapPayRequest();
        String returnUrl = "http://1d8d863589.51mypc.cn/api/mpi/pay/qrpay/alinote";
        alipayRequest.setReturnUrl(returnUrl);//前台通知
        alipayRequest.setNotifyUrl(notify_url);//后台回调
        JSONObject bizContent = new JSONObject();
        bizContent.put("out_trade_no", product.getOutTradeNo());
        bizContent.put("total_amount", product.getTotalFee());//订单金额:元
        bizContent.put("subject",product.getSubject());//订单标题
        bizContent.put("seller_id", payConfigAli.getPartner());//实际收款账号，一般填写商户PID即可
        bizContent.put("product_code", "QUICK_WAP_PAY");//手机网页支付
        bizContent.put("body", "两个苹果五毛钱");
        alipayRequest.setBizContent("{" +
                " \"out_trade_no\":\""+product.getOutTradeNo()+"\"," +
                " \"total_amount\":\""+product.getTotalFee()+"\"," +
                " \"subject\":\""+product.getSubject()+"\"," +
                " \"product_code\":\"QUICK_WAP_PAY\"" +
                " }");


//        String biz = bizContent.toString().replaceAll("\"", "'");
//        alipayRequest.setBizContent(biz);
        logger.info("业务参数:"+alipayRequest.getBizContent());
        String form = PayConstant.FAIL;
        try {
          AliPayConfig aliPayConfig = new AliPayConfig();
          AlipayClient alipayClient=  aliPayConfig.getAlipayClient(payConfigAli);
            form = alipayClient.pageExecute(alipayRequest).getBody();
        } catch (AlipayApiException e) {
            logger.error("支付宝构造表单失败",e);
        }
        return form;
    }

    @Override
    public String aliPayPc(Product product,PayConfigAli payConfigAli) {
        logger.info("支付宝PC支付下单");
        AlipayTradePagePayRequest alipayRequest = new AlipayTradePagePayRequest();
        String returnUrl = "前台回调地址 http 自定义";
        alipayRequest.setReturnUrl(returnUrl);//前台通知
        alipayRequest.setNotifyUrl(notify_url);//后台回调
        JSONObject bizContent = new JSONObject();
        bizContent.put("out_trade_no", product.getOutTradeNo());
        bizContent.put("total_amount", product.getTotalFee());//订单金额:元
        bizContent.put("subject",product.getSubject());//订单标题
        bizContent.put("seller_id", Configs.getPid());//实际收款账号，一般填写商户PID即可
        bizContent.put("product_code", "FAST_INSTANT_TRADE_PAY");//电脑网站支付
        bizContent.put("body", "两个苹果五毛钱");
        String biz = bizContent.toString().replaceAll("\"", "'");
        alipayRequest.setBizContent(biz);
        logger.info("业务参数:"+alipayRequest.getBizContent());
        String form = PayConstant.FAIL;
        AliPayConfig aliPayConfig = new AliPayConfig();
        AlipayClient alipayClient=  aliPayConfig.getAlipayClient(payConfigAli);
        try {
            form = alipayClient.pageExecute(alipayRequest).getBody();
        } catch (AlipayApiException e) {
            logger.error("支付宝构造表单失败",e);
        }
        return form;
    }

    @Override
    public String appPay(Product product,PayConfigAli payConfigAli) {
        String orderString = PayConstant.FAIL;
        // 实例化客户端
        AliPayConfig aliPayConfig = new AliPayConfig();
        AlipayClient alipayClient=  aliPayConfig.getAlipayClient(payConfigAli);
        // 实例化具体API对应的request类,类名称和接口名称对应,当前调用接口名称：alipay.trade.app.pay
        AlipayTradeAppPayRequest request = new AlipayTradeAppPayRequest();
        // SDK已经封装掉了公共参数，这里只需要传入业务参数。以下方法为sdk的model入参方式(model和biz_content同时存在的情况下取biz_content)。
        AlipayTradeAppPayModel model = new AlipayTradeAppPayModel();
        model.setBody(product.getBody());
        model.setSubject(product.getSubject());
        model.setOutTradeNo(product.getOutTradeNo());
        model.setTimeoutExpress("30m");
        model.setTotalAmount(product.getTotalFee());
        model.setProductCode("QUICK_MSECURITY_PAY");
        request.setBizModel(model);
        request.setNotifyUrl("商户外网可以访问的异步地址");
        try {
            // 这里和普通的接口调用不同，使用的是sdkExecute
            AlipayTradeAppPayResponse response = alipayClient
                    .sdkExecute(request);
            orderString  = response.getBody();//就是orderString 可以直接给客户端请求，无需再做处理。
            //System.out.println(response.getBody());
        } catch (AlipayApiException e) {
            e.printStackTrace();
        }
        return orderString ;
    }

    @Override
    public boolean verifyByAli(Map<String, String> underScoreKeyMap,String ali_public_key) {
        boolean flag = false;
        try {
            flag = AlipaySignature.rsaCheckV1(underScoreKeyMap,ali_public_key, "utf-8", "RSA2");
        } catch (AlipayApiException e) {
            e.printStackTrace();
        }
        return flag;
    }
}
