package com.pms.service.impl;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.pms.exception.RRException;
import org.springframework.stereotype.Service;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.pms.entity.PayAccount;
import com.pms.mapper.PayAccountMapper;
import com.pms.service.IPayAccountService;
/**
 * 
 *
 * @author ljb
 * @email 517770986@qq.com
 * @date 2017-11-03 10:48:48
 */
@Service
public class PayAccountServiceImpl extends ServiceImpl<PayAccountMapper,PayAccount> implements IPayAccountService {


}