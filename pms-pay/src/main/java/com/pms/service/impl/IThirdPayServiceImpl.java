package com.pms.service.impl;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.pms.core.PayConstant;
import com.pms.core.PayWayEnum;
import com.pms.entity.*;
import com.pms.exception.R;
import com.pms.exception.RRException;
import com.pms.service.*;
import com.pms.validator.Assert;
import lombok.extern.java.Log;
import lombok.extern.log4j.Log4j;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.Date;
import java.util.SortedMap;

import static com.pms.core.PayWayEnum.ALIPAY_MOBILE;
import static com.pms.core.PayWayEnum.ALIPAY_PC;

/**
 * Created by ljb on 2017/11/7.
 */
@Service
@Log4j
public class IThirdPayServiceImpl implements IThirdPayService {

    protected Logger logger = LoggerFactory.getLogger(getClass());

    @Autowired
    private IAliPayService iAliPayService;
    @Autowired
    private IWxPayService iWxPayService;
    @Autowired
    private IPayAccountService iPayAccountService;
    @Autowired
    private IPayUserInfoService iPayUserInfoService;
    @Autowired
    private IPayAccountBusService iPayAccountBusService;
    @Autowired
    private IPayTradeRecordService iPayTradeRecordService;
    @Override
    public String initPay(Product product) {
        //参数验证
        Assert.isNull(product.getAccountNo(), "accountNo 不能为空");
        Assert.isNull(product.getTotalFee(), "totalFee 不能为空");
        Assert.isNull(product.getOutTradeNo(), "outTradeNo 不能为空");
        Assert.isNull(product.getSubject(), "productInfo 不能为空");//描述  验证不
        Assert.isNull(product.getPayWay(), "payWay 不能为空");

        this.checkAccountByNo(product.getAccountNo());

        // 订单验证方法
        checkOrders(product.getInnerNo(), product.getTotalFee(), product.getPayType());

        String infor = "";
        PayWayEnum payWay = PayWayEnum.getEnumByName(product.getPayWay());
        PayConfigAli payConfigAli = iPayAccountBusService.getAliConfigByNo(product.getAccountNo());
        PayConfigWx payConfigWx = iPayAccountBusService.getWxConfigByNo(product.getAccountNo());
        switch (payWay){
            case ALIPAY_MOBILE:
                infor = iAliPayService.aliPayMobile(product,payConfigAli);
                break;
            case ALIPAY_PC:
                infor = iAliPayService.aliPayPc(product,payConfigAli);
                break;
            case ALIPAY_QR:
                infor = iAliPayService.aliPay(product);
                break;
            case WX_JSAPI:
                iWxPayService.wxPay1(product,payConfigWx);
                break;
            default:
                throw new RRException("支付方式不存在!");
        }
        // 插入支付记录
        addPaymentRecord(product);
        return infor;
    }

    /**
     * 根据用户编码，验证用户合法性
     * @param accountNo
     */
    public void checkAccountByNo(String accountNo){
        PayUserInfo userInfo = iPayUserInfoService.selectOne(
                new EntityWrapper<PayUserInfo>().eq("account_no", accountNo));

        if (userInfo == null) {
            throw new RRException("用户账号不存在!");
        }
        if (userInfo.getStatus() != 1){
            throw new RRException("该用户已被停用!");
        }
        PayAccount account = iPayAccountService.selectOne(
                new EntityWrapper<PayAccount>().eq("account_no", accountNo));
        if (account == null) {
            throw new RRException("该用户钱包不存在！");
        }
    }

    /**
     *
     * @param innerNo       系统内部订单号
     * @param amount        总金额 分
     * @param payType       类别 区分哪个系统来的订单 1 商城系统 2 物业系统
     */
    public void checkOrders(String innerNo ,String amount,Integer payType){
        switch (payType) {
            case 1:
                break;
            case 2:
                break;
        }
    }

    /**
     * 支付成功，添加支付记录
     * @param product
     */
    public void addPaymentRecord(Product product) {
        PayTradeRecord trade = iPayTradeRecordService.selectOne(
                new EntityWrapper<PayTradeRecord>().eq("inner_no",product.getInnerNo()));
        if (trade == null) {

            trade = new PayTradeRecord();
            trade.setTradeNo(product.getOutTradeNo());                          // 支付记录流水
            trade.setAccountNo(product.getAccountNo());                         // 支付系统内部编码
            trade.setInnerNo(product.getInnerNo());                             // 订单编号
            trade.setAmount(new BigDecimal(product.getTotalFee()));             // 支付金额
            trade.setPayInfo(product.getBody());                                // 订单描述
            trade.setCreateTime(new Date());                                    // 支付时间
            trade.setCreateTime(new Date());                                    // 创建时间
            trade.setPayType(product.getPayType());                             // 支付方式
            trade.setPayWay(product.getPayWay());                               // 支付类型

            iPayTradeRecordService.insert(trade);    // 添加支付记录(然后添加明细)
        } else {// 如果有支付记录，更新支付记录的流水号
            trade.setPayWay(product.getPayWay());                               // 支付类型
            trade.setTradeNo(product.getOutTradeNo());                          // 支付记录流水
            trade.setCreateTime(new Date());                                    // 支付时间
            iPayTradeRecordService.updateById(trade);
        }
    }
}
