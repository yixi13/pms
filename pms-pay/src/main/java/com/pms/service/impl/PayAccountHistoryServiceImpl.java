package com.pms.service.impl;

import org.springframework.stereotype.Service;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.pms.entity.PayAccountHistory;
import com.pms.mapper.PayAccountHistoryMapper;
import com.pms.service.IPayAccountHistoryService;
/**
 * 
 *
 * @author ljb
 * @email 517770986@qq.com
 * @date 2017-11-03 10:48:48
 */
@Service
public class PayAccountHistoryServiceImpl extends ServiceImpl<PayAccountHistoryMapper,PayAccountHistory> implements IPayAccountHistoryService {

}