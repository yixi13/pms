package com.pms.service.impl;

import org.springframework.stereotype.Service;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.pms.entity.PayUserInfo;
import com.pms.mapper.PayUserInfoMapper;
import com.pms.service.IPayUserInfoService;
/**
 * 支付用户基本信息表
 *
 * @author zyl
 * @email 517770986@qq.com
 * @date 2017-10-30 14:13:53
 */
@Service
public class PayUserInfoServiceImpl extends ServiceImpl<PayUserInfoMapper,PayUserInfo> implements IPayUserInfoService {

}