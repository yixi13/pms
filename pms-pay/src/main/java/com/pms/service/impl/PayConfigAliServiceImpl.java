package com.pms.service.impl;

import org.springframework.stereotype.Service;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.pms.entity.PayConfigAli;
import com.pms.mapper.PayConfigAliMapper;
import com.pms.service.IPayConfigAliService;
/**
 * 
 *
 * @author ljb
 * @email 517770986@qq.com
 * @date 2017-11-03 10:48:48
 */
@Service
public class PayConfigAliServiceImpl extends ServiceImpl<PayConfigAliMapper,PayConfigAli> implements IPayConfigAliService {

}