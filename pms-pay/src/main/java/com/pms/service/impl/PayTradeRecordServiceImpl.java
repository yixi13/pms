package com.pms.service.impl;

import org.springframework.stereotype.Service;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.pms.entity.PayTradeRecord;
import com.pms.mapper.PayTradeRecordMapper;
import com.pms.service.IPayTradeRecordService;
/**
 * 
 *
 * @author ljb
 * @email 517770986@qq.com
 * @date 2017-11-03 10:48:48
 */
@Service
public class PayTradeRecordServiceImpl extends ServiceImpl<PayTradeRecordMapper,PayTradeRecord> implements IPayTradeRecordService {

}