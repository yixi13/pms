package com.pms.service.impl;

import org.springframework.stereotype.Service;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.pms.entity.PayConfigWx;
import com.pms.mapper.PayConfigWxMapper;
import com.pms.service.IPayConfigWxService;
/**
 * 
 *
 * @author ljb
 * @email 517770986@qq.com
 * @date 2017-11-03 10:48:48
 */
@Service
public class PayConfigWxServiceImpl extends ServiceImpl<PayConfigWxMapper,PayConfigWx> implements IPayConfigWxService {

}