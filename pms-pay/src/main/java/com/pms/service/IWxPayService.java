package com.pms.service;

import com.pms.entity.PayConfigWx;
import com.pms.entity.Product;

public interface IWxPayService {
    /**
     * 微信支付下单(模式二)
     * 扫码支付 还有模式一 适合固定商品ID
     * @param product
     *
     */
    String wxPay2(Product product,PayConfigWx payConfigWx);
    /**
     * 微信支付下单(模式一)
     * @param product  void
     *
     */
    void wxPay1(Product product,PayConfigWx payConfigWx);
    /**
     * 微信支付退款
     * @param product
     * @return  String
     *
     */
    String wxRefund(Product product,PayConfigWx payConfigWx);
    /**
     * 关闭订单
     * @param product
     * @return  String
     *
     */
    String wxCloseOrder(Product product,PayConfigWx payConfigWx);
    /**
     * 下载微信账单
     *
     */
    void saveBill(PayConfigWx payConfigWx);
    /**
     * 微信公众号支付返回一个url地址
     * @param product
     * @return  String
     *
     */
    String wxPayMobile(Product product,PayConfigWx payConfigWx);
    /**
     * H5支付 唤醒 微信APP 进行支付
     * 申请入口：登录商户平台-->产品中心-->我的产品-->支付产品-->H5支付
     * @param product
     * @return  String
     *
     */
    String wxPayH5(Product product,PayConfigWx payConfigWx);
}
