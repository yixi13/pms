package com.pms.service;
import com.baomidou.mybatisplus.service.IService;
import com.pms.entity.PayAccountHistory;
/**
 * 
 *
 * @author ljb
 * @email 517770986@qq.com
 * @date 2017-11-03 10:48:48
 */

public interface IPayAccountHistoryService extends  IService<PayAccountHistory> {

}