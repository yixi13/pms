package com.pms;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.netflix.feign.EnableFeignClients;

/**
 * @author zyl
 * @create 2017-10-25 14:07
 **/
@SpringBootApplication
@EnableFeignClients
@EnableDiscoveryClient
public class PayApplication {
    public static void main(String args[]){
        SpringApplication.run(PayApplication.class, args);
    }
}
