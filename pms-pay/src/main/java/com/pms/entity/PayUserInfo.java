package com.pms.entity;
import com.baomidou.mybatisplus.activerecord.Model;
import java.io.Serializable;
import java.util.Date;
import javax.persistence.*;
import com.pms.util.Description;
import com.pms.entity.BaseModel;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;


/**
 * 支付用户基本信息表
 * 
 * @author ljb
 * @email 517770986@qq.com
 * @date 2017-11-03 11:33:36
 */
@TableName( "pay_user_info")
public class PayUserInfo extends BaseModel<PayUserInfo> {
private static final long serialVersionUID = 1L;


	    /**
	 *支付用户ID
	 */
    @TableId("pay_user_id")
    private Long payUserId;
	
	    /**
     *用户编号
     */
    @TableField("user_no")
    @Description("用户编号")
    private String userNo;
	
	    /**
     *名称
     */
    @TableField("user_name")
    @Description("名称")
    private String userName;
	
	    /**
     *账号编号
     */
    @TableField("account_no")
    @Description("账号编号")
    private String accountNo;
	
	    /**
     *状态，1启用2，停用
     */
    @TableField("status")
    @Description("状态，1启用2，停用")
    private Integer status;
	
	    /**
     *创建时间
     */
    @TableField("create_time")
    @Description("创建时间")
    private Date createTime;
	
	    /**
     *
     */
    @TableField("create_by")
    @Description("")
    private String createBy;
	
	    /**
     *
     */
    @TableField("update_time")
    @Description("")
    private Date updateTime;
	
	    /**
     *
     */
    @TableField("update_by")
    @Description("")
    private String updateBy;
	
// ID赋值
public PayUserInfo(){
        this.payUserId= returnIdLong();
        }
@Override
protected Serializable pkVal() {
        return this.payUserId;
}
	/**
	 * 设置：支付用户ID
	 */
	public PayUserInfo setPayUserId(Long payUserId) {
		this.payUserId = payUserId;
		return this;
	}
	/**
	 * 获取：支付用户ID
	 */
	public Long getPayUserId() {
		return payUserId;
	}
	/**
	 * 设置：用户编号
	 */
	public PayUserInfo setUserNo(String userNo) {
		this.userNo = userNo;
		return this;
	}
	/**
	 * 获取：用户编号
	 */
	public String getUserNo() {
		return userNo;
	}
	/**
	 * 设置：名称
	 */
	public PayUserInfo setUserName(String userName) {
		this.userName = userName;
		return this;
	}
	/**
	 * 获取：名称
	 */
	public String getUserName() {
		return userName;
	}
	/**
	 * 设置：账号编号
	 */
	public PayUserInfo setAccountNo(String accountNo) {
		this.accountNo = accountNo;
		return this;
	}
	/**
	 * 获取：账号编号
	 */
	public String getAccountNo() {
		return accountNo;
	}
	/**
	 * 设置：状态，1启用2，停用
	 */
	public PayUserInfo setStatus(Integer status) {
		this.status = status;
		return this;
	}
	/**
	 * 获取：状态，1启用2，停用
	 */
	public Integer getStatus() {
		return status;
	}
	/**
	 * 设置：创建时间
	 */
	public PayUserInfo setCreateTime(Date createTime) {
		this.createTime = createTime;
		return this;
	}
	/**
	 * 获取：创建时间
	 */
	public Date getCreateTime() {
		return createTime;
	}
	/**
	 * 设置：
	 */
	public PayUserInfo setCreateBy(String createBy) {
		this.createBy = createBy;
		return this;
	}
	/**
	 * 获取：
	 */
	public String getCreateBy() {
		return createBy;
	}
	/**
	 * 设置：
	 */
	public PayUserInfo setUpdateTime(Date updateTime) {
		this.updateTime = updateTime;
		return this;
	}
	/**
	 * 获取：
	 */
	public Date getUpdateTime() {
		return updateTime;
	}
	/**
	 * 设置：
	 */
	public PayUserInfo setUpdateBy(String updateBy) {
		this.updateBy = updateBy;
		return this;
	}
	/**
	 * 获取：
	 */
	public String getUpdateBy() {
		return updateBy;
	}


}
