package com.pms.entity;
import com.baomidou.mybatisplus.activerecord.Model;
import java.io.Serializable;
import java.util.Date;
import javax.persistence.*;
import com.pms.util.Description;
import com.pms.entity.BaseModel;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;


/**
 * 
 * 
 * @author ljb
 * @email 517770986@qq.com
 * @date 2017-11-03 11:33:36
 */
@TableName( "pay_config_wx")
public class PayConfigWx extends BaseModel<PayConfigWx> {
private static final long serialVersionUID = 1L;


	    /**
	 *主键id
	 */
    @TableId("pay_config_wx_id")
    private Long payConfigWxId;
	
	    /**
     *账户编号
     */
    @TableField("account_no")
    @Description("账户编号")
    private String accountNo;
	
	    /**
     *微信支付APP_ID
     */
    @TableField("app_id")
    @Description("微信支付APP_ID")
    private String appId;
	
	    /**
     *微信商户号
     */
    @TableField("mch_id")
    @Description("微信商户号")
    private String mchId;
	
	    /**
     *微信秘钥
     */
    @TableField("pr_key")
    @Description("微信秘钥")
    private String prKey;
	
	    /**
     *回调路径
     */
    @TableField("notify_url")
    @Description("回调路径")
    private String notifyUrl;
	
	    /**
     *
     */
    @TableField("info")
    @Description("")
    private String info;
	
// ID赋值
public PayConfigWx(){
        this.payConfigWxId= returnIdLong();
        }
@Override
protected Serializable pkVal() {
        return this.payConfigWxId;
}
	/**
	 * 设置：主键id
	 */
	public PayConfigWx setPayConfigWxId(Long payConfigWxId) {
		this.payConfigWxId = payConfigWxId;
		return this;
	}
	/**
	 * 获取：主键id
	 */
	public Long getPayConfigWxId() {
		return payConfigWxId;
	}
	/**
	 * 设置：账户编号
	 */
	public PayConfigWx setAccountNo(String accountNo) {
		this.accountNo = accountNo;
		return this;
	}
	/**
	 * 获取：账户编号
	 */
	public String getAccountNo() {
		return accountNo;
	}
	/**
	 * 设置：微信支付APP_ID
	 */
	public PayConfigWx setAppId(String appId) {
		this.appId = appId;
		return this;
	}
	/**
	 * 获取：微信支付APP_ID
	 */
	public String getAppId() {
		return appId;
	}
	/**
	 * 设置：微信商户号
	 */
	public PayConfigWx setMchId(String mchId) {
		this.mchId = mchId;
		return this;
	}
	/**
	 * 获取：微信商户号
	 */
	public String getMchId() {
		return mchId;
	}
	/**
	 * 设置：微信秘钥
	 */
	public PayConfigWx setPrKey(String prKey) {
		this.prKey = prKey;
		return this;
	}
	/**
	 * 获取：微信秘钥
	 */
	public String getPrKey() {
		return prKey;
	}
	/**
	 * 设置：回调路径
	 */
	public PayConfigWx setNotifyUrl(String notifyUrl) {
		this.notifyUrl = notifyUrl;
		return this;
	}
	/**
	 * 获取：回调路径
	 */
	public String getNotifyUrl() {
		return notifyUrl;
	}
	/**
	 * 设置：
	 */
	public PayConfigWx setInfo(String info) {
		this.info = info;
		return this;
	}
	/**
	 * 获取：
	 */
	public String getInfo() {
		return info;
	}


}
