package com.pms.entity;
import com.baomidou.mybatisplus.activerecord.Model;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import javax.persistence.*;
import com.pms.util.Description;
import com.pms.entity.BaseModel;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;


/**
 * 
 * 
 * @author ljb
 * @email 517770986@qq.com
 * @date 2017-11-03 11:33:36
 */
@TableName( "pay_account_history")
public class PayAccountHistory extends BaseModel<PayAccountHistory> {
private static final long serialVersionUID = 1L;


	    /**
	 *历史记录ID
	 */
    @TableId("account_history_id")
    private Long accountHistoryId;
	
	    /**
     *账户编号
     */
    @TableField("account_no")
    @Description("账户编号")
    private String accountNo;
	
	    /**
     *金额
     */
    @TableField("amount")
    @Description("金额")
    private BigDecimal amount;
	
	    /**
     *账户余额
     */
    @TableField("balance")
    @Description("账户余额")
    private BigDecimal balance;
	
	    /**
     *资金变动方向
     */
    @TableField("fund_direction")
    @Description("资金变动方向")
    private String fundDirection;
	
	    /**
     *是否允许结算
     */
    @TableField("is_allowsett")
    @Description("是否允许结算")
    private Integer isAllowsett;
	
	    /**
     *是否完成结算
     */
    @TableField("is_completesett")
    @Description("是否完成结算")
    private Integer isCompletesett;
	
	    /**
     *银行流水号
     */
    @TableField("banktrx_no")
    @Description("银行流水号")
    private String banktrxNo;
	
	    /**
     *业务类型
     */
    @TableField("trx_type")
    @Description("业务类型")
    private Integer trxType;
	
	    /**
     *风险预存期
     */
    @TableField("risk_day")
    @Description("风险预存期")
    private Integer riskDay;
	
	    /**
     *用户编号
     */
    @TableField("user_no")
    @Description("用户编号")
    private String userNo;
	
	    /**
     *
     */
    @TableField("create_time")
    @Description("")
    private Date createTime;
	
	    /**
     *
     */
    @TableField("create_by")
    @Description("")
    private String createBy;
	
	    /**
     *
     */
    @TableField("update_time")
    @Description("")
    private Date updateTime;
	
	    /**
     *
     */
    @TableField("update_by")
    @Description("")
    private String updateBy;
	
// ID赋值
public PayAccountHistory(){
        this.accountHistoryId= returnIdLong();
        }
@Override
protected Serializable pkVal() {
        return this.accountHistoryId;
}
	/**
	 * 设置：历史记录ID
	 */
	public PayAccountHistory setAccountHistoryId(Long accountHistoryId) {
		this.accountHistoryId = accountHistoryId;
		return this;
	}
	/**
	 * 获取：历史记录ID
	 */
	public Long getAccountHistoryId() {
		return accountHistoryId;
	}
	/**
	 * 设置：账户编号
	 */
	public PayAccountHistory setAccountNo(String accountNo) {
		this.accountNo = accountNo;
		return this;
	}
	/**
	 * 获取：账户编号
	 */
	public String getAccountNo() {
		return accountNo;
	}
	/**
	 * 设置：金额
	 */
	public PayAccountHistory setAmount(BigDecimal amount) {
		this.amount = amount;
		return this;
	}
	/**
	 * 获取：金额
	 */
	public BigDecimal getAmount() {
		return amount;
	}
	/**
	 * 设置：账户余额
	 */
	public PayAccountHistory setBalance(BigDecimal balance) {
		this.balance = balance;
		return this;
	}
	/**
	 * 获取：账户余额
	 */
	public BigDecimal getBalance() {
		return balance;
	}
	/**
	 * 设置：资金变动方向
	 */
	public PayAccountHistory setFundDirection(String fundDirection) {
		this.fundDirection = fundDirection;
		return this;
	}
	/**
	 * 获取：资金变动方向
	 */
	public String getFundDirection() {
		return fundDirection;
	}
	/**
	 * 设置：是否允许结算
	 */
	public PayAccountHistory setIsAllowsett(Integer isAllowsett) {
		this.isAllowsett = isAllowsett;
		return this;
	}
	/**
	 * 获取：是否允许结算
	 */
	public Integer getIsAllowsett() {
		return isAllowsett;
	}
	/**
	 * 设置：是否完成结算
	 */
	public PayAccountHistory setIsCompletesett(Integer isCompletesett) {
		this.isCompletesett = isCompletesett;
		return this;
	}
	/**
	 * 获取：是否完成结算
	 */
	public Integer getIsCompletesett() {
		return isCompletesett;
	}
	/**
	 * 设置：银行流水号
	 */
	public PayAccountHistory setBanktrxNo(String banktrxNo) {
		this.banktrxNo = banktrxNo;
		return this;
	}
	/**
	 * 获取：银行流水号
	 */
	public String getBanktrxNo() {
		return banktrxNo;
	}
	/**
	 * 设置：业务类型
	 */
	public PayAccountHistory setTrxType(Integer trxType) {
		this.trxType = trxType;
		return this;
	}
	/**
	 * 获取：业务类型
	 */
	public Integer getTrxType() {
		return trxType;
	}
	/**
	 * 设置：风险预存期
	 */
	public PayAccountHistory setRiskDay(Integer riskDay) {
		this.riskDay = riskDay;
		return this;
	}
	/**
	 * 获取：风险预存期
	 */
	public Integer getRiskDay() {
		return riskDay;
	}
	/**
	 * 设置：用户编号
	 */
	public PayAccountHistory setUserNo(String userNo) {
		this.userNo = userNo;
		return this;
	}
	/**
	 * 获取：用户编号
	 */
	public String getUserNo() {
		return userNo;
	}
	/**
	 * 设置：
	 */
	public PayAccountHistory setCreateTime(Date createTime) {
		this.createTime = createTime;
		return this;
	}
	/**
	 * 获取：
	 */
	public Date getCreateTime() {
		return createTime;
	}
	/**
	 * 设置：
	 */
	public PayAccountHistory setCreateBy(String createBy) {
		this.createBy = createBy;
		return this;
	}
	/**
	 * 获取：
	 */
	public String getCreateBy() {
		return createBy;
	}
	/**
	 * 设置：
	 */
	public PayAccountHistory setUpdateTime(Date updateTime) {
		this.updateTime = updateTime;
		return this;
	}
	/**
	 * 获取：
	 */
	public Date getUpdateTime() {
		return updateTime;
	}
	/**
	 * 设置：
	 */
	public PayAccountHistory setUpdateBy(String updateBy) {
		this.updateBy = updateBy;
		return this;
	}
	/**
	 * 获取：
	 */
	public String getUpdateBy() {
		return updateBy;
	}


}
