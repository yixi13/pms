package com.pms.entity;
import com.baomidou.mybatisplus.activerecord.Model;
import java.io.Serializable;
import java.util.Date;
import javax.persistence.*;
import com.pms.util.Description;
import com.pms.entity.BaseModel;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;


/**
 * 
 * 
 * @author ljb
 * @email 517770986@qq.com
 * @date 2017-11-03 11:33:36
 */
@TableName( "pay_config_ali")
public class PayConfigAli extends BaseModel<PayConfigAli> {
private static final long serialVersionUID = 1L;


	    /**
	 *主键id
	 */
    @TableId("pay_config_ali_id")
    private Long payConfigAliId;
	
	    /**
     *账户编号
     */
    @TableField("account_no")
    @Description("账户编号")
    private String accountNo;
	
	    /**
     *合作身份者ID
     */
    @TableField("partner")
    @Description("合作身份者ID")
    private String partner;
	
	    /**
     *支付宝APP_ID
     */
    @TableField("app_id")
    @Description("支付宝APP_ID")
    private String appId;
	
	    /**
     *商户私钥
     */
    @TableField("private_key")
    @Description("商户私钥")
    private String privateKey;
	
	    /**
     *支付宝私钥
     */
    @TableField("ali_private_key")
    @Description("支付宝私钥")
    private String aliPrivateKey;
	
	    /**
     *支付宝公钥
     */
    @TableField("ali_public_key")
    @Description("支付宝公钥")
    private String aliPublicKey;
	
	    /**
     *签名方式默认RSA/RSA2
     */
    @TableField("sign_type")
    @Description("签名方式默认RSA/RSA2")
    private String signType;
	
	    /**
     *字符编码
     */
    @TableField("input_charset")
    @Description("字符编码")
    private String inputCharset;
	
	    /**
     *md5加密key
     */
    @TableField("md5_key")
    @Description("md5加密key")
    private String md5Key;
	
	    /**
     *回调地址
     */
    @TableField("back_url")
    @Description("回调地址")
    private String backUrl;
	
// ID赋值
public PayConfigAli(){
        this.payConfigAliId= returnIdLong();
        }
@Override
protected Serializable pkVal() {
        return this.payConfigAliId;
}
	/**
	 * 设置：主键id
	 */
	public PayConfigAli setPayConfigAliId(Long payConfigAliId) {
		this.payConfigAliId = payConfigAliId;
		return this;
	}
	/**
	 * 获取：主键id
	 */
	public Long getPayConfigAliId() {
		return payConfigAliId;
	}
	/**
	 * 设置：账户编号
	 */
	public PayConfigAli setAccountNo(String accountNo) {
		this.accountNo = accountNo;
		return this;
	}
	/**
	 * 获取：账户编号
	 */
	public String getAccountNo() {
		return accountNo;
	}
	/**
	 * 设置：合作身份者ID
	 */
	public PayConfigAli setPartner(String partner) {
		this.partner = partner;
		return this;
	}
	/**
	 * 获取：合作身份者ID
	 */
	public String getPartner() {
		return partner;
	}
	/**
	 * 设置：支付宝APP_ID
	 */
	public PayConfigAli setAppId(String appId) {
		this.appId = appId;
		return this;
	}
	/**
	 * 获取：支付宝APP_ID
	 */
	public String getAppId() {
		return appId;
	}
	/**
	 * 设置：商户私钥
	 */
	public PayConfigAli setPrivateKey(String privateKey) {
		this.privateKey = privateKey;
		return this;
	}
	/**
	 * 获取：商户私钥
	 */
	public String getPrivateKey() {
		return privateKey;
	}
	/**
	 * 设置：支付宝私钥
	 */
	public PayConfigAli setAliPrivateKey(String aliPrivateKey) {
		this.aliPrivateKey = aliPrivateKey;
		return this;
	}
	/**
	 * 获取：支付宝私钥
	 */
	public String getAliPrivateKey() {
		return aliPrivateKey;
	}
	/**
	 * 设置：支付宝公钥
	 */
	public PayConfigAli setAliPublicKey(String aliPublicKey) {
		this.aliPublicKey = aliPublicKey;
		return this;
	}
	/**
	 * 获取：支付宝公钥
	 */
	public String getAliPublicKey() {
		return aliPublicKey;
	}
	/**
	 * 设置：签名方式默认RSA/RSA2
	 */
	public PayConfigAli setSignType(String signType) {
		this.signType = signType;
		return this;
	}
	/**
	 * 获取：签名方式默认RSA/RSA2
	 */
	public String getSignType() {
		return signType;
	}
	/**
	 * 设置：字符编码
	 */
	public PayConfigAli setInputCharset(String inputCharset) {
		this.inputCharset = inputCharset;
		return this;
	}
	/**
	 * 获取：字符编码
	 */
	public String getInputCharset() {
		return inputCharset;
	}
	/**
	 * 设置：md5加密key
	 */
	public PayConfigAli setMd5Key(String md5Key) {
		this.md5Key = md5Key;
		return this;
	}
	/**
	 * 获取：md5加密key
	 */
	public String getMd5Key() {
		return md5Key;
	}
	/**
	 * 设置：回调地址
	 */
	public PayConfigAli setBackUrl(String backUrl) {
		this.backUrl = backUrl;
		return this;
	}
	/**
	 * 获取：回调地址
	 */
	public String getBackUrl() {
		return backUrl;
	}


}
