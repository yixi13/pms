package com.pms.entity;
import com.baomidou.mybatisplus.activerecord.Model;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import javax.persistence.*;
import com.pms.util.Description;
import com.pms.entity.BaseModel;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;


/**
 * 
 * 
 * @author ljb
 * @email 517770986@qq.com
 * @date 2017-11-03 11:33:36
 */
@TableName( "pay_trade_record")
public class PayTradeRecord extends BaseModel<PayTradeRecord> {
private static final long serialVersionUID = 1L;


	    /**
	 *支付记录表
	 */
    @TableId("pay_trade_record_id")
    private Long payTradeRecordId;
	
	    /**
     *交易编码(支付系统编码)
     */
    @TableField("trade_no")
    @Description("交易编码(支付系统编码)")
    private String tradeNo;
	
	    /**
     *支付宝/微信交易码
     */
    @TableField("third_no")
    @Description("支付宝/微信交易码")
    private String thirdNo;
	
	    /**
     *内部系统交易码
     */
    @TableField("inner_no")
    @Description("内部系统交易码")
    private String innerNo;
	
	    /**
     *支付账户
     */
    @TableField("account_no")
    @Description("支付账户")
    private String accountNo;
	
	    /**
     *支付商品描述
     */
    @TableField("pay_info")
    @Description("支付商品描述")
    private String payInfo;
	
	    /**
     *支付金额
     */
    @TableField("amount")
    @Description("支付金额")
    private BigDecimal amount;
	
	    /**
     *支付状态成功:SUCCESS,回调时候修改
     */
    @TableField("pay_state")
    @Description("支付状态成功:SUCCESS,回调时候修改")
    private String payState;
	
	    /**
     *支付时间
     */
    @TableField("create_time")
    @Description("支付时间")
    private Date createTime;
	
	    /**
     *支付成功时间
     */
    @TableField("pay_success_time")
    @Description("支付成功时间")
    private Date paySuccessTime;
	
	    /**
     *支付方式：ALIPAY/CHINAPAY/WEIXIN
     */
    @TableField("pay_way")
    @Description("支付方式：ALIPAY/CHINAPAY/WEIXIN")
    private String payWay;
	
	    /**
     *支付业务类型：1订单支付/2物业费.等
     */
    @TableField("pay_type")
    @Description("支付业务类型：1订单支付/2物业费.等")
    private Integer payType;
	
	    /**
     *通知内部系统状态:SUCCESS/FALSE
     */
    @TableField("notify_state")
    @Description("通知内部系统状态:SUCCESS/FALSE")
    private String notifyState;
	
	    /**
     *通知次数
     */
    @TableField("notify_number")
    @Description("通知次数")
    private Integer notifyNumber;
	
	    /**
     *通知成功时间
     */
    @TableField("notify_success_time")
    @Description("通知成功时间")
    private Date notifySuccessTime;
	
// ID赋值
public PayTradeRecord(){
        this.payTradeRecordId= returnIdLong();
        }
@Override
protected Serializable pkVal() {
        return this.payTradeRecordId;
}
	/**
	 * 设置：支付记录表
	 */
	public PayTradeRecord setPayTradeRecordId(Long payTradeRecordId) {
		this.payTradeRecordId = payTradeRecordId;
		return this;
	}
	/**
	 * 获取：支付记录表
	 */
	public Long getPayTradeRecordId() {
		return payTradeRecordId;
	}
	/**
	 * 设置：交易编码(支付系统编码)
	 */
	public PayTradeRecord setTradeNo(String tradeNo) {
		this.tradeNo = tradeNo;
		return this;
	}
	/**
	 * 获取：交易编码(支付系统编码)
	 */
	public String getTradeNo() {
		return tradeNo;
	}
	/**
	 * 设置：支付宝/微信交易码
	 */
	public PayTradeRecord setThirdNo(String thirdNo) {
		this.thirdNo = thirdNo;
		return this;
	}
	/**
	 * 获取：支付宝/微信交易码
	 */
	public String getThirdNo() {
		return thirdNo;
	}
	/**
	 * 设置：内部系统交易码
	 */
	public PayTradeRecord setInnerNo(String innerNo) {
		this.innerNo = innerNo;
		return this;
	}
	/**
	 * 获取：内部系统交易码
	 */
	public String getInnerNo() {
		return innerNo;
	}
	/**
	 * 设置：支付账户
	 */
	public PayTradeRecord setAccountNo(String accountNo) {
		this.accountNo = accountNo;
		return this;
	}
	/**
	 * 获取：支付账户
	 */
	public String getAccountNo() {
		return accountNo;
	}
	/**
	 * 设置：支付商品描述
	 */
	public PayTradeRecord setPayInfo(String payInfo) {
		this.payInfo = payInfo;
		return this;
	}
	/**
	 * 获取：支付商品描述
	 */
	public String getPayInfo() {
		return payInfo;
	}
	/**
	 * 设置：支付金额
	 */
	public PayTradeRecord setAmount(BigDecimal amount) {
		this.amount = amount;
		return this;
	}
	/**
	 * 获取：支付金额
	 */
	public BigDecimal getAmount() {
		return amount;
	}
	/**
	 * 设置：支付状态成功:SUCCESS,回调时候修改
	 */
	public PayTradeRecord setPayState(String payState) {
		this.payState = payState;
		return this;
	}
	/**
	 * 获取：支付状态成功:SUCCESS,回调时候修改
	 */
	public String getPayState() {
		return payState;
	}
	/**
	 * 设置：支付时间
	 */
	public PayTradeRecord setCreateTime(Date createTime) {
		this.createTime = createTime;
		return this;
	}
	/**
	 * 获取：支付时间
	 */
	public Date getCreateTime() {
		return createTime;
	}
	/**
	 * 设置：支付成功时间
	 */
	public PayTradeRecord setPaySuccessTime(Date paySuccessTime) {
		this.paySuccessTime = paySuccessTime;
		return this;
	}
	/**
	 * 获取：支付成功时间
	 */
	public Date getPaySuccessTime() {
		return paySuccessTime;
	}
	/**
	 * 设置：支付方式：ALIPAY/CHINAPAY/WEIXIN
	 */
	public PayTradeRecord setPayWay(String payWay) {
		this.payWay = payWay;
		return this;
	}
	/**
	 * 获取：支付方式：ALIPAY/CHINAPAY/WEIXIN
	 */
	public String getPayWay() {
		return payWay;
	}
	/**
	 * 设置：支付业务类型：1订单支付/2物业费.等
	 */
	public PayTradeRecord setPayType(Integer payType) {
		this.payType = payType;
		return this;
	}
	/**
	 * 获取：支付业务类型：1订单支付/2物业费.等
	 */
	public Integer getPayType() {
		return payType;
	}
	/**
	 * 设置：通知内部系统状态:SUCCESS/FALSE
	 */
	public PayTradeRecord setNotifyState(String notifyState) {
		this.notifyState = notifyState;
		return this;
	}
	/**
	 * 获取：通知内部系统状态:SUCCESS/FALSE
	 */
	public String getNotifyState() {
		return notifyState;
	}
	/**
	 * 设置：通知次数
	 */
	public PayTradeRecord setNotifyNumber(Integer notifyNumber) {
		this.notifyNumber = notifyNumber;
		return this;
	}
	/**
	 * 获取：通知次数
	 */
	public Integer getNotifyNumber() {
		return notifyNumber;
	}
	/**
	 * 设置：通知成功时间
	 */
	public PayTradeRecord setNotifySuccessTime(Date notifySuccessTime) {
		this.notifySuccessTime = notifySuccessTime;
		return this;
	}
	/**
	 * 获取：通知成功时间
	 */
	public Date getNotifySuccessTime() {
		return notifySuccessTime;
	}


}
