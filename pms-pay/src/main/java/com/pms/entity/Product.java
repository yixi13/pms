package com.pms.entity;

import com.pms.core.PayWayEnum;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * @author zyl
 * @create 2017-11-01 11:45
 **/
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Product implements Serializable {
    private static final long serialVersionUID = 1L;

    private String accountNo;                   // 系统内部用户编码
    private String productId;                   // 商品ID
    private String subject;                     // 订单标题
    private String body;                         // 商品描述
    private String totalFee;                    // 总金额(单位是分)
    private String innerNo;                     // 系统内部订单号
    private String outTradeNo;                  // 支付系统内部流水号
    private String spbillCreateIp;              // 发起人IP地址
    private String attach;                       // 附件数据主要用于商户携带订单的自定义数据
    private String payWay;                       // 支付方式
    private Integer payType;                      // 系统来源:1 物业系统，2 商城系统
    private String frontUrl;                     // 前台回调地址  非扫码支付使用
}
