package com.pms.entity;
import com.baomidou.mybatisplus.activerecord.Model;
import java.io.Serializable;
import java.util.Date;
import javax.persistence.*;
import com.pms.util.Description;
import com.pms.entity.BaseModel;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;


/**
 * 
 * 
 * @author ljb
 * @email 517770986@qq.com
 * @date 2017-11-03 11:33:36
 */
@TableName( "pay_account_bus")
public class PayAccountBus extends BaseModel<PayAccountBus> {
private static final long serialVersionUID = 1L;


	    /**
	 *用户业务支付配置
	 */
    @TableId("pay_account_bus_id")
    private Long payAccountBusId;
	
	    /**
     *用户账号
     */
    @TableField("account_no")
    @Description("用户账号")
    private String accountNo;
	
	    /**
     *业务编码:1:订单，2物业费
     */
    @TableField("bus_code")
    @Description("业务编码:1:订单，2物业费")
    private Integer busCode;
	
	    /**
     *调用支付配置1：平台，2：自己
     */
    @TableField("pay_type")
    @Description("调用支付配置1：平台，2：自己")
    private Integer payType;
	
// ID赋值
public PayAccountBus(){
        this.payAccountBusId= returnIdLong();
        }
@Override
protected Serializable pkVal() {
        return this.payAccountBusId;
}
	/**
	 * 设置：用户业务支付配置
	 */
	public PayAccountBus setPayAccountBusId(Long payAccountBusId) {
		this.payAccountBusId = payAccountBusId;
		return this;
	}
	/**
	 * 获取：用户业务支付配置
	 */
	public Long getPayAccountBusId() {
		return payAccountBusId;
	}
	/**
	 * 设置：用户账号
	 */
	public PayAccountBus setAccountNo(String accountNo) {
		this.accountNo = accountNo;
		return this;
	}
	/**
	 * 获取：用户账号
	 */
	public String getAccountNo() {
		return accountNo;
	}
	/**
	 * 设置：业务编码:1:订单，2物业费
	 */
	public PayAccountBus setBusCode(Integer busCode) {
		this.busCode = busCode;
		return this;
	}
	/**
	 * 获取：业务编码:1:订单，2物业费
	 */
	public Integer getBusCode() {
		return busCode;
	}
	/**
	 * 设置：调用支付配置1：平台，2：自己
	 */
	public PayAccountBus setPayType(Integer payType) {
		this.payType = payType;
		return this;
	}
	/**
	 * 获取：调用支付配置1：平台，2：自己
	 */
	public Integer getPayType() {
		return payType;
	}


}
