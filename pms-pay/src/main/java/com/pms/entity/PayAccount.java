package com.pms.entity;
import com.baomidou.mybatisplus.activerecord.Model;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import javax.persistence.*;
import com.pms.util.Description;
import com.pms.entity.BaseModel;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;


/**
 * 
 * 
 * @author ljb
 * @email 517770986@qq.com
 * @date 2017-11-03 11:33:36
 */
@TableName( "pay_account")
public class PayAccount extends BaseModel<PayAccount> {
private static final long serialVersionUID = 1L;


	    /**
	 *账户ID
	 */
    @TableId("account_id")
    private Long accountId;
	
	    /**
     *账户编号
     */
    @TableField("account_no")
    @Description("账户编号")
    private String accountNo;
	
	    /**
     *账户余额
     */
    @TableField("balance")
    @Description("账户余额")
    private BigDecimal balance;
	
	    /**
     *不可用余额
     */
    @TableField("unbalance")
    @Description("不可用余额")
    private BigDecimal unbalance;
	
	    /**
     *保证金
     */
    @TableField("security_money")
    @Description("保证金")
    private BigDecimal securityMoney;
	
	    /**
     *总收益
     */
    @TableField("total_Income")
    @Description("总收益")
    private BigDecimal totalIncome;
	
	    /**
     *总支出
     */
    @TableField("total_expend")
    @Description("总支出")
    private BigDecimal totalExpend;
	
	    /**
     *今日收益
     */
    @TableField("today_Income")
    @Description("今日收益")
    private BigDecimal todayIncome;
	
	    /**
     *今日支出
     */
    @TableField("today_expend")
    @Description("今日支出")
    private BigDecimal todayExpend;
	
	    /**
     *账户类型
     */
    @TableField("account_type")
    @Description("账户类型")
    private Integer accountType;
	
	    /**
     *用户编号
     */
    @TableField("user_no")
    @Description("用户编号")
    private String userNo;
	
	    /**
     *创建时间
     */
    @TableField("create_time")
    @Description("创建时间")
    private Date createTime;
	
	    /**
     *创建人
     */
    @TableField("create_by")
    @Description("创建人")
    private String createBy;
	
	    /**
     *修改时间
     */
    @TableField("update_time")
    @Description("修改时间")
    private Date updateTime;
	
	    /**
     *修改人
     */
    @TableField("update_by")
    @Description("修改人")
    private String updateBy;
	
// ID赋值
public PayAccount(){
        this.accountId= returnIdLong();
        }
@Override
protected Serializable pkVal() {
        return this.accountId;
}
	/**
	 * 设置：账户ID
	 */
	public PayAccount setAccountId(Long accountId) {
		this.accountId = accountId;
		return this;
	}
	/**
	 * 获取：账户ID
	 */
	public Long getAccountId() {
		return accountId;
	}
	/**
	 * 设置：账户编号
	 */
	public PayAccount setAccountNo(String accountNo) {
		this.accountNo = accountNo;
		return this;
	}
	/**
	 * 获取：账户编号
	 */
	public String getAccountNo() {
		return accountNo;
	}
	/**
	 * 设置：账户余额
	 */
	public PayAccount setBalance(BigDecimal balance) {
		this.balance = balance;
		return this;
	}
	/**
	 * 获取：账户余额
	 */
	public BigDecimal getBalance() {
		return balance;
	}
	/**
	 * 设置：不可用余额
	 */
	public PayAccount setUnbalance(BigDecimal unbalance) {
		this.unbalance = unbalance;
		return this;
	}
	/**
	 * 获取：不可用余额
	 */
	public BigDecimal getUnbalance() {
		return unbalance;
	}
	/**
	 * 设置：保证金
	 */
	public PayAccount setSecurityMoney(BigDecimal securityMoney) {
		this.securityMoney = securityMoney;
		return this;
	}
	/**
	 * 获取：保证金
	 */
	public BigDecimal getSecurityMoney() {
		return securityMoney;
	}
	/**
	 * 设置：总收益
	 */
	public PayAccount setTotalIncome(BigDecimal totalIncome) {
		this.totalIncome = totalIncome;
		return this;
	}
	/**
	 * 获取：总收益
	 */
	public BigDecimal getTotalIncome() {
		return totalIncome;
	}
	/**
	 * 设置：总支出
	 */
	public PayAccount setTotalExpend(BigDecimal totalExpend) {
		this.totalExpend = totalExpend;
		return this;
	}
	/**
	 * 获取：总支出
	 */
	public BigDecimal getTotalExpend() {
		return totalExpend;
	}
	/**
	 * 设置：今日收益
	 */
	public PayAccount setTodayIncome(BigDecimal todayIncome) {
		this.todayIncome = todayIncome;
		return this;
	}
	/**
	 * 获取：今日收益
	 */
	public BigDecimal getTodayIncome() {
		return todayIncome;
	}
	/**
	 * 设置：今日支出
	 */
	public PayAccount setTodayExpend(BigDecimal todayExpend) {
		this.todayExpend = todayExpend;
		return this;
	}
	/**
	 * 获取：今日支出
	 */
	public BigDecimal getTodayExpend() {
		return todayExpend;
	}
	/**
	 * 设置：账户类型
	 */
	public PayAccount setAccountType(Integer accountType) {
		this.accountType = accountType;
		return this;
	}
	/**
	 * 获取：账户类型
	 */
	public Integer getAccountType() {
		return accountType;
	}
	/**
	 * 设置：用户编号
	 */
	public PayAccount setUserNo(String userNo) {
		this.userNo = userNo;
		return this;
	}
	/**
	 * 获取：用户编号
	 */
	public String getUserNo() {
		return userNo;
	}
	/**
	 * 设置：创建时间
	 */
	public PayAccount setCreateTime(Date createTime) {
		this.createTime = createTime;
		return this;
	}
	/**
	 * 获取：创建时间
	 */
	public Date getCreateTime() {
		return createTime;
	}
	/**
	 * 设置：创建人
	 */
	public PayAccount setCreateBy(String createBy) {
		this.createBy = createBy;
		return this;
	}
	/**
	 * 获取：创建人
	 */
	public String getCreateBy() {
		return createBy;
	}
	/**
	 * 设置：修改时间
	 */
	public PayAccount setUpdateTime(Date updateTime) {
		this.updateTime = updateTime;
		return this;
	}
	/**
	 * 获取：修改时间
	 */
	public Date getUpdateTime() {
		return updateTime;
	}
	/**
	 * 设置：修改人
	 */
	public PayAccount setUpdateBy(String updateBy) {
		this.updateBy = updateBy;
		return this;
	}
	/**
	 * 获取：修改人
	 */
	public String getUpdateBy() {
		return updateBy;
	}


}
