package com.pms.entity;

import java.io.Serializable;
import java.util.Date;

/**
 * Created by Administrator on 2018/1/4 0004.
 */
public class UserAgencyInfo implements Serializable {

    private Long userId;
    private Long agencyId;
    private Integer state;
    private Integer type;
    private Date creationTime;

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public Long getAgencyId() {
        return agencyId;
    }

    public void setAgencyId(Long agencyId) {
        this.agencyId = agencyId;
    }

    public Integer getState() {
        return state;
    }

    public void setState(Integer state) {
        this.state = state;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public Date getCreationTime() {
        return creationTime;
    }

    public void setCreationTime(Date creationTime) {
        this.creationTime = creationTime;
    }
}
