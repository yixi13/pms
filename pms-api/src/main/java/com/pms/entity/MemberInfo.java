package com.pms.entity;

import java.io.Serializable;
import java.util.Date;

/**
 * Created by hm on 2017/10/25.
 */
public class MemberInfo implements Serializable {
    private Long memberId;
    private String userAccount;
    private String passWord;
    private String nickName;
    private String memberSign;
    private String realName;
    private String phone;
    private String areaId;
    private String address;
    private String img;
    private String email;
    private Integer sex;
    private Integer age;
    private String idCard;
    private String spId;
    private Integer memType;
    private Integer isDisable;
    private Integer disableType;
    private Date disableTime;
    private String myRecommend;
    private String gradeId;
    private String createBy;
    private Date createTime;
    private String updateBy;
    private Date updateTime;
    private String referrerId;
    private String houseId;
    private String houseName;
    private String unitId;
    private String macAddress;
    private Integer accountType;
    private Integer integral;
    private String weixinOpenid;

    public void setMemberId(Long memberId) {
        this.memberId = memberId;
    }

    public void setUserAccount(String userAccount) {
        this.userAccount = userAccount;
    }

    public void setPassWord(String passWord) {
        this.passWord = passWord;
    }

    public void setNickName(String nickName) {
        this.nickName = nickName;
    }

    public void setMemberSign(String memberSign) {
        this.memberSign = memberSign;
    }

    public void setRealName(String realName) {
        this.realName = realName;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public void setAreaId(String areaId) {
        this.areaId = areaId;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public void setImg(String img) {
        this.img = img;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void setSex(Integer sex) {
        this.sex = sex;
    }

    public void setAge(Integer age) {
        this.age = age;
    }

    public void setIdCard(String idCard) {
        this.idCard = idCard;
    }

    public void setSpId(String spId) {
        this.spId = spId;
    }

    public void setMemType(Integer memType) {
        this.memType = memType;
    }

    public void setIsDisable(Integer isDisable) {
        this.isDisable = isDisable;
    }

    public void setDisableType(Integer disableType) {
        this.disableType = disableType;
    }

    public void setDisableTime(Date disableTime) {
        this.disableTime = disableTime;
    }

    public void setMyRecommend(String myRecommend) {
        this.myRecommend = myRecommend;
    }

    public void setGradeId(String gradeId) {
        this.gradeId = gradeId;
    }

    public void setCreateBy(String createBy) {
        this.createBy = createBy;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public void setUpdateBy(String updateBy) {
        this.updateBy = updateBy;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    public void setReferrerId(String referrerId) {
        this.referrerId = referrerId;
    }

    public void setHouseId(String houseId) {
        this.houseId = houseId;
    }

    public void setHouseName(String houseName) {
        this.houseName = houseName;
    }

    public void setUnitId(String unitId) {
        this.unitId = unitId;
    }

    public void setMacAddress(String macAddress) {
        this.macAddress = macAddress;
    }

    public void setAccountType(Integer accountType) {
        this.accountType = accountType;
    }

    public void setIntegral(Integer integral) {
        this.integral = integral;
    }

    public void setWeixinOpenid(String weixinOpenid) {
        this.weixinOpenid = weixinOpenid;
    }

    public Long getMemberId() {
        return memberId;
    }

    public String getUserAccount() {
        return userAccount;
    }

    public String getPassWord() {
        return passWord;
    }

    public String getNickName() {
        return nickName;
    }

    public String getMemberSign() {
        return memberSign;
    }

    public String getRealName() {
        return realName;
    }

    public String getPhone() {
        return phone;
    }

    public String getAreaId() {
        return areaId;
    }

    public String getAddress() {
        return address;
    }

    public String getImg() {
        return img;
    }

    public String getEmail() {
        return email;
    }

    public Integer getSex() {
        return sex;
    }

    public Integer getAge() {
        return age;
    }

    public String getIdCard() {
        return idCard;
    }

    public String getSpId() {
        return spId;
    }

    public Integer getMemType() {
        return memType;
    }

    public Integer getIsDisable() {
        return isDisable;
    }

    public Integer getDisableType() {
        return disableType;
    }

    public Date getDisableTime() {
        return disableTime;
    }

    public String getMyRecommend() {
        return myRecommend;
    }

    public String getGradeId() {
        return gradeId;
    }

    public String getCreateBy() {
        return createBy;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public String getUpdateBy() {
        return updateBy;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public String getReferrerId() {
        return referrerId;
    }

    public String getHouseId() {
        return houseId;
    }

    public String getHouseName() {
        return houseName;
    }

    public String getUnitId() {
        return unitId;
    }

    public String getMacAddress() {
        return macAddress;
    }

    public Integer getAccountType() {
        return accountType;
    }

    public Integer getIntegral() {
        return integral;
    }

    public String getWeixinOpenid() {
        return weixinOpenid;
    }
}
