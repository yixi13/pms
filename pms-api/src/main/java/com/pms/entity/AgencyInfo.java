package com.pms.entity;

import java.io.Serializable;
import java.util.Date;

/**
 * 内部提供的 机构信息
 * @author ASUS_B
 * @create 2017-10-25 10:58
 **/
public class AgencyInfo implements Serializable {
    //机构id
    private String agencyId;

    //机构编号
    private String agencyNo;

    //机构名称
    private String agencyName;

    //机构地址id(省-市-区)
    private String areaId;

    //机构地址
    private String agencyAddress;

    //机构状态(1-启用|0-禁用)
    private Integer state;

    //机构电话
    private String phone;

    //机构座机电话
    private String telPhone;

    //机构主图
    private String img;

    //上级机构id
    private String parantId;

    //机构类型(0平台|1厂家|2物业|3城市运营商|4平台直属|21小区)
    private Integer type;

    //利润比列
    private String profitScale;

    //小区门的通用密码
    private String eqPswd;

    //机构分润百分比
    private Double profitPer;

    //社区编号
    private Integer communityNum;

    /**
     * 设置：
     */
    public void setAgencyId(String agencyId) {
        this.agencyId = agencyId;
    }
    /**
     * 获取：
     */
    public String getAgencyId() {
        return agencyId;
    }
    /**
     * 设置：
     */
    public void setAgencyNo(String agencyNo) {
        this.agencyNo = agencyNo;
    }
    /**
     * 获取：
     */
    public String getAgencyNo() {
        return agencyNo;
    }
    /**
     * 设置：
     */
    public void setAgencyName(String agencyName) {
        this.agencyName = agencyName;
    }
    /**
     * 获取：
     */
    public String getAgencyName() {
        return agencyName;
    }
    /**
     * 设置：
     */
    public void setAreaId(String areaId) {
        this.areaId = areaId;
    }
    /**
     * 获取：
     */
    public String getAreaId() {
        return areaId;
    }
    /**
     * 设置：
     */
    public void setAgencyAddress(String agencyAddress) {
        this.agencyAddress = agencyAddress;
    }
    /**
     * 获取：
     */
    public String getAgencyAddress() {
        return agencyAddress;
    }
    /**
     * 设置：
     */
    public void setState(Integer state) {
        this.state = state;
    }
    /**
     * 获取：
     */
    public Integer getState() {
        return state;
    }
    /**
     * 设置：
     */
    public void setPhone(String phone) {
        this.phone = phone;
    }
    /**
     * 获取：
     */
    public String getPhone() {
        return phone;
    }
    /**
     * 设置：
     */
    public void setTelPhone(String telPhone) {
        this.telPhone = telPhone;
    }
    /**
     * 获取：
     */
    public String getTelPhone() {
        return telPhone;
    }
    /**
     * 设置：
     */
    public void setImg(String img) {
        this.img = img;
    }
    /**
     * 获取：
     */
    public String getImg() {
        return img;
    }
    /**
     * 设置：
     */
    public void setParantId(String parantId) {
        this.parantId = parantId;
    }
    /**
     * 获取：
     */
    public String getParantId() {
        return parantId;
    }
    /**
     * 设置：
     */
    public void setType(Integer type) {
        this.type = type;
    }
    /**
     * 获取：
     */
    public Integer getType() {
        return type;
    }
    /**
     * 设置：利润比列
     */
    public void setProfitScale(String profitScale) {
        this.profitScale = profitScale;
    }
    /**
     * 获取：利润比列
     */
    public String getProfitScale() {
        return profitScale;
    }
    /**
     * 设置：小区门的通用密码
     */
    public void setEqPswd(String eqPswd) {
        this.eqPswd = eqPswd;
    }
    /**
     * 获取：小区门的通用密码
     */
    public String getEqPswd() {
        return eqPswd;
    }
    /**
     * 设置：机构分润百分比
     */
    public void setProfitPer(Double profitPer) {
        this.profitPer = profitPer;
    }
    /**
     * 获取：机构分润百分比
     */
    public Double getProfitPer() {
        return profitPer;
    }
    /**
     * 设置：社区编号
     */
    public void setCommunityNum(Integer communityNum) {
        this.communityNum = communityNum;
    }
    /**
     * 获取：社区编号
     */
    public Integer getCommunityNum() {
        return communityNum;
    }

}
