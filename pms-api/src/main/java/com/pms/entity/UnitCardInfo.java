package com.pms.entity;

/**
 * Created by Administrator on 2017/11/28.
 * 物业单元 蓝牙卡片信息 错误类
 */
public class UnitCardInfo {

    private  String cradNumTen;
    private  String unitName;
    private  String errInfo;

    public String getCradNumTen() {
        return cradNumTen;
    }

    public void setCradNumTen(String cradNumTen) {
        this.cradNumTen = cradNumTen;
    }

    public String getUnitName() {
        return unitName;
    }

    public void setUnitName(String unitName) {
        this.unitName = unitName;
    }

    public String getErrInfo() {
        return errInfo;
    }

    public void setErrInfo(String errInfo) {
        this.errInfo = errInfo;
    }
}
