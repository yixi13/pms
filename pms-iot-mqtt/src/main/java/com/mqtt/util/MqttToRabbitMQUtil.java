package com.mqtt.util;

import com.pms.constant.MQConstant;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;
import org.springframework.messaging.MessageHandler;
import org.springframework.messaging.MessagingException;
import org.springframework.messaging.Message;
import org.springframework.integration.annotation.ServiceActivator;


/**
 * Created by Administrator on 2018/3/22.
 */
//@Component
public class MqttToRabbitMQUtil {

    @Autowired
    private RabbitTemplate rabbitTemplate;
    @Bean
    @ServiceActivator(inputChannel = "mqttInputChannel")
    public MessageHandler handler() {
        return new MessageHandler() {
            @Override
            public void handleMessage(Message<?> message) throws MessagingException {
//                System.out.println(message.getPayload().toString());
                rabbitTemplate.convertAndSend(MQConstant.QUEUE_NAME_IOT_MQTT_STORAGE,message.getPayload());
            }
        };
    }
}
