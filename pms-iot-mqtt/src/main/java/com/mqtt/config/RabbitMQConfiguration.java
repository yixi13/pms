package com.mqtt.config;

import com.pms.constant.MQConstant;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.core.Binding;
import org.springframework.amqp.core.BindingBuilder;
import org.springframework.amqp.core.DirectExchange;
import org.springframework.amqp.core.Queue;
import org.springframework.amqp.rabbit.connection.CachingConnectionFactory;
import org.springframework.amqp.rabbit.connection.ConnectionFactory;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Scope;

/**
 * Created by ljb on 2017/11/15.
 */
@Configuration
public class RabbitMQConfiguration {
    private static Logger logger = LoggerFactory.getLogger(RabbitMQConfiguration.class);
    @Value("${spring.rabbitmq.host}")
    private String host;
    @Value("${spring.rabbitmq.port}")
    private int port;
    @Value("${spring.rabbitmq.username}")
    private String username;
    @Value("${spring.rabbitmq.password}")
    private String password;
    // 注册链接信息
    @Bean
    public ConnectionFactory connectionFactory() {
        logger.info("============== MQtest =============== port = " + port +"user =" + username);
        CachingConnectionFactory connectionFactory = new CachingConnectionFactory(host, port);
        connectionFactory.setUsername(username);
        connectionFactory.setPassword(password);
        connectionFactory.setVirtualHost("/");
        connectionFactory.setPublisherConfirms(true);//必须要设置，用于进行消息的回调
        logger.info("Create ConnectionFactory bean ..");
        return connectionFactory;
    }

    /**
     * 注册RabbitTemplate用于提供API操作
     * @return RabbitTemplate
     */
    @Bean
    @Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)//此处必须是prototype类型
    public RabbitTemplate rabbitTemplate() {
        RabbitTemplate template = new RabbitTemplate(connectionFactory());
        return template;
    }


    /**
     * 针对消费者配置交换机
     FanoutExchange:  将消息分发到所有的绑定队列，无routingkey的概念
     HeadersExchange：通过添加属性key-value匹配
     DirectExchange: 按照routingkey分发到指定队列
     TopicExchange:多关键字匹配
     */
    @Bean
    public DirectExchange defaultExchange() {
        return new DirectExchange(MQConstant.DEFAULT_EXCHANGE);
    }
    /**
     * 注册消息队列
     * @return
     */
    @Bean
    public Queue iotMqttStorageQueue() {
        return new Queue(MQConstant.QUEUE_NAME_IOT_MQTT_STORAGE, true); //默认durable=true,让队列持久
    }

    /**
     * 将交换机和消息队列绑定
     * DirectExchangeRoutingKeyConfigurer.with(关联routingKey)====》DirectExchange需要按照routingkey分发到指定队列
     * @return
     */
    @Bean
    public Binding iotMqttStorageQueueBinding() {
        return BindingBuilder.bind(iotMqttStorageQueue()).to(defaultExchange()).with(MQConstant.QUEUE_NAME_IOT_MQTT_STORAGE);
    }
}
