package com.mqtt;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

@EnableDiscoveryClient //注解开启服务发现
@SpringBootApplication
public class PmsIotMqttApplication {

	public static void main(String[] args) {
		SpringApplication.run(PmsIotMqttApplication.class, args);
	}
}
