package com.pms.partitionPoints.mapper;
import com.pms.partitionPoints.entity.KyEquipmentInfo;
import com.baomidou.mybatisplus.mapper.BaseMapper;
/**
 * 设备信息表
 * 
 * @author lk
 * @email 307169223@qq.com
 * @date 2018-07-20 11:02:02
 */
public interface KyEquipmentInfoMapper extends BaseMapper<KyEquipmentInfo> {
	
}
