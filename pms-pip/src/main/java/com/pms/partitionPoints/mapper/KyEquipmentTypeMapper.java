package com.pms.partitionPoints.mapper;
import com.pms.partitionPoints.entity.KyEquipmentType;
import com.baomidou.mybatisplus.mapper.BaseMapper;
/**
 * 
 * 
 * @author lk
 * @email 307169223@qq.com
 * @date 2018-07-18 17:45:51
 */
public interface KyEquipmentTypeMapper extends BaseMapper<KyEquipmentType> {
	
}
