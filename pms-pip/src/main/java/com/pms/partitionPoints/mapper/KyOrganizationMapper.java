package com.pms.partitionPoints.mapper;
import com.baomidou.mybatisplus.mapper.Wrapper;
import com.pms.partitionPoints.bean.OrgBean;
import com.pms.partitionPoints.entity.KyOrganization;
import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.pms.partitionPoints.entity.KyOrganizationEquipment;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.session.RowBounds;

import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * 机构
 * 
 * @author lk
 * @email 307169223@qq.com
 * @date 2018-07-18 17:45:06
 */
public interface KyOrganizationMapper extends BaseMapper<KyOrganization> {
    /**
     * 根据一级分区id查询 下级  二/三级分区id
     * @param orgId
     * @return
     */
    List<Integer> getSubordinateOrgIds(@Param("orgId") Integer orgId);

    String getGroupIds(@Param("orgIds") String orgIds);
    String getGroupIdsByEq(@Param("equipmentIds") String equipmentIds);

    List<OrgBean> findByGroupIdAndCode(@Param("groupId")Long groupId,@Param("code")String code);
    List<OrgBean> findEquipmentsByOrgId(Integer orgId);
    //根据当前分区id查询所有下属分区id
    String  findOrgIdsBycCurrentId(Integer orgId);
    //根据分区id集合删除对应的分区
    int deleteOrgByIdList(List<String> idList);
    //根据分区id集合删除对应的分区与测点关联数据
    int deleteOrgEquipmentByIdList(List<String> idList);

    List<Map<String,Object>> selectEquipmentsByOrgId(@Param("ew") Wrapper<KyOrganizationEquipment> wrapper);

    List<Map<String,Object>> selectEquipmentsByOrgIds(@Param("orgIds") String orgIds);

    List<Map<String,Object>> findEquipmentListByOrgId(RowBounds rowBounds, @Param("ew") Wrapper<KyOrganizationEquipment> wrapper, @Param("orgIds")String orgIds,@Param("strName")String strName);

    List<Map<String,Object>> selectOrgPageByOrgIds(RowBounds rowBounds, @Param("ew") Wrapper<KyOrganization> wrapper, @Param("orgIds")String orgIds,@Param("strName")String strName);
    //根据分区查询下一级分区没有的测点
    List<OrgBean> getEquipmentsByOrgId(Integer orgId);
    List<KyOrganization> getOrganizationByRoleId(long roleId);
    //根据系统编码查询所有的分区集合
    List<Integer> getOrgIdListByCode(@Param("code")String code);
    //根据顶级分区id查询所有测点id
    List<Integer> getEquipmentIdListByOrgMaxId(@Param("orgMaxId")Integer orgMaxId);
    //删除对应的系统数据上报数据表
    void dropUploadData(@Param("orgMaxId")Integer orgMaxId);
    //根据顶级分区id和测点id查询测点最新上报数据
    Date selectUploadDateByEquipmentId(@Param("orgMaxId")Integer orgMaxId, @Param("equipmentId")Integer equipmentId);
    //创建对应的数据上报表
    void createUploadTable(@Param("orgMaxId")Integer orgMaxId);
}
