package com.pms.partitionPoints.mapper;
import com.pms.partitionPoints.entity.KyOrganizationGrade;
import com.baomidou.mybatisplus.mapper.BaseMapper;
/**
 * 机构级别
 * 
 * @author lk
 * @email 307169223@qq.com
 * @date 2018-07-18 17:45:51
 */
public interface KyOrganizationGradeMapper extends BaseMapper<KyOrganizationGrade> {
	
}
