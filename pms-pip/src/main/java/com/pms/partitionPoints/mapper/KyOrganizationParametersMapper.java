package com.pms.partitionPoints.mapper;
import com.pms.partitionPoints.entity.KyOrganizationParameters;
import com.baomidou.mybatisplus.mapper.BaseMapper;
/**
 * 
 * 
 * @author lk
 * @email 307169223@qq.com
 * @date 2018-07-27 15:31:33
 */
public interface KyOrganizationParametersMapper extends BaseMapper<KyOrganizationParameters> {
	
}
