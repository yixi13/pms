package com.pms.partitionPoints.mapper;
import com.pms.partitionPoints.entity.KyEquipmentTypeCareCol;
import com.baomidou.mybatisplus.mapper.BaseMapper;
/**
 * 特定设备类型的设备 所关心的上传数据列
 * 
 * @author lk
 * @email 307169223@qq.com
 * @date 2018-07-18 17:45:51
 */
public interface KyEquipmentTypeCareColMapper extends BaseMapper<KyEquipmentTypeCareCol> {
	
}
