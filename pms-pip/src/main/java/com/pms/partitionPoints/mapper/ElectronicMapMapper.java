package com.pms.partitionPoints.mapper;

import com.pms.partitionPoints.entity.KyEquipmentUploadData;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * 电子地图
 *
 * @author ASUS_
 */
public interface ElectronicMapMapper {
    /**
     * 查询分区信息
     * @param paramMap (companyCode-公司编码,level-分区级别,roleOrgIds-角色对应机构id,parentId-上级分区id,id-当前分区id)
     * @return Map (id,name,parentId,level,monitoringsites)
     */
    List<Map<String,Object>> getOrgDmaInfoByLevel(Map<String, Object> paramMap);
    /**
     * 查询不在分区范围内的  测点
     * @param paramMap (companyCode-公司编码,level-分区级别,roleOrgIds-角色对应机构id,parentId-查询分区的上级id,oneLevelOrgId-查询分区对应的一级分区Id)
     * @return
     */
    List<Map<String,Object>> getDmaDorderEquipment(Map<String, Object> paramMap);

    /**
     * 查询分区下的测点
     * @param orgDmaIds 分区id字符串
     * @return Map(measurepointId,organizationGroupId,measurepointName,measurepointStatus,measurepointDirection,longitude,latitude,orgDmaId)
     */
    List<Map<String,Object>> getDmaMeasurepointInfo(@Param("orgDmaIds") String orgDmaIds);

    /**
     * 查询测点 瞬时流量
     * @param paramMap (tableSuffix-表名后缀,measurepointIds-测点字符串)
     * @return Map (equipmentId-测点id,instantaneousFlow-瞬时流量)
     */
    List<Map<String,Object>> getMeasurepointInstantaneousFlow( List<Map<String,Object>> paramMap);

    /**
     * 查询一级分区的id
     * @param id 二级分区id
     * @return
     */
    Integer getOneLevelOrgId(@Param("id")Integer id);
    /**
     * 查询测点 上报数据
     * @param paramMap (tableSuffix-表名后缀,measurepointIds-测点字符串)
     * @return Map (equipmentId-测点id,equipmentStatus-设备状态,communicationStatus-设备通讯状态
     * ,pressure-压力,waterLevel-水位,netCumulativeFlow-净累计流量,positiveCumulativeFlow-正累计流量
     * ,negativeAccumulatedFlow-负累计流量,instantaneousFlow-瞬时流量)
     */
    List<Map<String,Object>> getMeasurepointDataInfo( List<Map<String,Object>> paramMap);

    /**
     * 查询测点 集合
     * @param paramMap (parentId-三级分区id,id-测点id,roleOrgIds-角色拥有的权限机构id,companyCode-公司编码,startRow-分页起始条数,pageSize-每页查询数量)
     * @return Map (id-测点id,name-测点名称,organizationGroupId-数据表名,longitude-经度
     * ,latitude-纬度,partitionName-所属分区名称,partitionId-所属分区id)
     */
    List<Map<String,Object>> getMeasurepointList(Map<String,Object> paramMap);

    /**
     * 查询 测点数量
     * @param paramMap (parentId-三级分区id,id-测点id,roleOrgIds-角色拥有的权限机构id,companyCode-公司编码)
     * @return measurepointNum
     */
    Integer getCountMeasurepointNum(Map<String,Object> paramMap);

    /**
     * 查询 用户所有测点
     * @param paramMap (parentId-三级分区id,id-测点id,roleOrgIds-角色拥有的权限机构id,companyCode-公司编码)
     * @return  Map (id-测点id,name-测点名称,longitude-经度,latitude-纬度)
     */
    List<Map<String,Object>> getUserAllMeasurepoint(Map<String,Object> paramMap);

    /**
     * 查询 用户所有分区
     * @param paramMap (roleId-角色id,companyCode-公司编码)
     * @return
     */
    List<Map<String,Object>> getUserOrgList(Map<String,Object> paramMap);

    List<KyEquipmentUploadData> readAllData(@Param("pageNum")Integer pageNum,@Param("pageSize")Integer pageSize);

}
