package com.pms.partitionPoints.mapper;
import com.baomidou.mybatisplus.mapper.Wrapper;
import com.pms.partitionPoints.bean.EquipmentBean;
import com.pms.partitionPoints.bean.EquipmentDataBean;
import com.pms.partitionPoints.entity.KyEquipmentUploadData;
import com.pms.partitionPoints.entity.KyOrganizationEquipment;
import com.baomidou.mybatisplus.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.session.RowBounds;

import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * 
 * @author lk
 * @email 307169223@qq.com
 * @date 2018-07-24 11:38:28
 */
public interface KyOrganizationEquipmentMapper extends BaseMapper<KyOrganizationEquipment> {
    int insertOrgList(List<KyOrganizationEquipment> orgList);
    //旧测点报表方法
    List<EquipmentDataBean> getEquipmentsByParam(RowBounds rowBounds, @Param("ew") Wrapper<KyOrganizationEquipment> wrapper,@Param("orgMaxId")Integer orgMaxId);
    List<KyEquipmentUploadData> getEquipmentDateListById(RowBounds rowBounds,@Param("ew") Wrapper<KyOrganizationEquipment> wrapper,@Param("orgId")Integer orgId);
    List<KyEquipmentUploadData> selectEquipmentDateByIdAndTime(@Param("orgId") Integer orgId, @Param("equipmentId") Integer equipmentId, @Param("startTime") Date startTime, @Param("endTime") Date endTime);
    List<Map<String,Object>> selectOrgWaterUsageByOrgId(@Param("orgMaxId") Integer orgMaxId, @Param("list")List<Integer> list,@Param("startTime") Date startTime, @Param("endTime") Date endTime,@Param("orgId") Integer orgId);
    List<Map<String,Object>> selectNewestFlowByOrgIdAndequIdList(@Param("orgMaxId")Integer orgMaxId,@Param("list")List<Integer> list,@Param("startTime") Date startTime);
    List<Integer> selectEquListByOrgId(Integer orgId);
    //根据测点和时间按天查询测点的流量统计
    List<Map<String,Object>> selectOrgWaterDayByEquIdsAndTime(Map<String,Object> map);
    //测点报表方法查询修改
    List<EquipmentDataBean> getEquipmentsByParamNew(RowBounds rowBounds,@Param("ew") Wrapper<KyOrganizationEquipment> wrapper,@Param("orgMaxId") Integer orgMaxId, @Param("list")List<Integer> list,@Param("startTime") Date startTime,@Param("endTime") Date endTime,@Param("name") String name);
    //根据系统id查询当前系统下所有有效测点
    List<Map<String,Object>> getEquipmentByOrgMaxId(int orgMax);
    public List<Integer> getEquipmentsByOrganizationList(@Param("organizationIdList") String  organizationIdList);
    List<Map<String,Object>> selectEquipmentDateListByIdAndTime(@Param("orgId") Integer orgId, @Param("equipmentId") Integer equipmentId, @Param("startTime") Date startTime, @Param("endTime") Date endTime,@Param("param") String param);
    List<EquipmentBean> getEquipmentDateByIdAndTime(@Param("orgId") Integer orgId, @Param("list") List<String> list, @Param("startTime") Date startTime, @Param("endTime") Date endTime);
    Integer getEquipmentNumByOrgMaxIdAndStatus(@Param("orgIds")List<String> orgIds,@Param("equipmentStatus")Integer equipmentStatus);
}
