package com.pms.partitionPoints.mapper;
import com.pms.partitionPoints.entity.KyGroupOrganization;
import com.baomidou.mybatisplus.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;

/**
 * 
 * 
 * @author lk
 * @email 307169223@qq.com
 * @date 2018-07-19 15:41:26
 */
public interface KyGroupOrganizationMapper extends BaseMapper<KyGroupOrganization> {
    /**
     * 查询角色权限范围的  分区id字符串
     * @param groupId 角色id
     * @return OrgIds
     */
    String getOrgIdsStrByGroupId(@Param("groupId") String groupId);
}
