package com.pms.partitionPoints.service;
import com.baomidou.mybatisplus.service.IService;
import com.pms.partitionPoints.entity.KyEquipmentTypeCareCol;
/**
 * 特定设备类型的设备 所关心的上传数据列
 *
 * @author lk
 * @email 307169223@qq.com
 * @date 2018-07-18 17:45:51
 */

public interface IKyEquipmentTypeCareColService extends  IService<KyEquipmentTypeCareCol> {

}