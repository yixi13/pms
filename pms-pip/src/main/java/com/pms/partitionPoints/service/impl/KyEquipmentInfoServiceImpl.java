package com.pms.partitionPoints.service.impl;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.mapper.SqlHelper;
import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.pms.partitionPoints.entity.KyOrganizationEquipment;
import com.pms.partitionPoints.mapper.KyOrganizationEquipmentMapper;
import com.pms.partitionPoints.mapper.KyOrganizationMapper;
import com.pms.repair.entity.KyCardRecharge;
import com.pms.repair.mapper.KyCardRechargeMapper;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.pms.partitionPoints.entity.KyEquipmentInfo;
import com.pms.partitionPoints.mapper.KyEquipmentInfoMapper;
import com.pms.partitionPoints.service.IKyEquipmentInfoService;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * 设备信息表
 *
 * @author lk
 * @email 307169223@qq.com
 * @date 2018-07-20 11:02:02
 */
@Service
public class KyEquipmentInfoServiceImpl extends ServiceImpl<KyEquipmentInfoMapper,KyEquipmentInfo> implements IKyEquipmentInfoService {
    @Autowired
    KyEquipmentInfoMapper kyEquipmentInfoMapper;
    @Autowired
    KyOrganizationEquipmentMapper kyOrganizationEquipmentMapper;
    @Autowired
    KyCardRechargeMapper kyCardRechargeMapper;
    @Autowired
    KyOrganizationMapper kyOrganizationMapper;
    /**
     *  //测点和对应的分区插入还有数据库充值
     * @param info
     * @param ids
     * @param cardRecharge
     * @return
     */
    @Transactional(rollbackFor = Exception.class)
    public boolean insertEquipmentInfoAndOrgs(KyEquipmentInfo info,String ids,KyCardRecharge cardRecharge){
        Integer equId = kyEquipmentInfoMapper.insert(info);
        if(StringUtils.isNotEmpty(ids)){
            List<KyOrganizationEquipment> koeList = new ArrayList<>();
            String[] strId = ids.split(",");
            for(int i=0;i<strId.length;i++){
                String id = strId[i];
                int orgId = Integer.parseInt(id);
                KyOrganizationEquipment keo = new KyOrganizationEquipment();
                keo.setEquipmentid(info.getId());
                keo.setOrganizationid(orgId);
                keo.setDirection(5);
                koeList.add(keo);
            }
            kyOrganizationEquipmentMapper.insertOrgList(koeList);
            cardRecharge.setEquipmentInfoId(info.getId().longValue());
            kyCardRechargeMapper.insert(cardRecharge);

        }
        return true;
    }

    @Transactional(rollbackFor = Exception.class)
    public void updateEquipmentInfo(KyEquipmentInfo info){
        kyEquipmentInfoMapper.updateById(info);
        kyOrganizationEquipmentMapper.delete(new EntityWrapper<KyOrganizationEquipment>().eq("equipmentId",info.getId()));
        String ids = info.getDmaid();
        if(StringUtils.isNotEmpty(ids)) {
            List<KyOrganizationEquipment> koeList = new ArrayList<>();
            String[] strId = ids.split(",");
            for (int i = 0; i < strId.length; i++) {
                String id = strId[i];
                int orgId = Integer.parseInt(id);
                KyOrganizationEquipment keo = new KyOrganizationEquipment();
                keo.setDirection(5);
                keo.setEquipmentid(info.getId());
                keo.setOrganizationid(orgId);
                koeList.add(keo);
            }
            kyOrganizationEquipmentMapper.insertOrgList(koeList);
        }
    }

    public Page<Map<String,Object>> queryEquipmentListByOrgId(Page<Map<String,Object>> page, EntityWrapper ew, String orgIds, String strName){
        SqlHelper.fillWrapper(page,ew);
        page.setRecords(kyOrganizationMapper.findEquipmentListByOrgId(page,ew,orgIds,strName));
        return page;
    }
}