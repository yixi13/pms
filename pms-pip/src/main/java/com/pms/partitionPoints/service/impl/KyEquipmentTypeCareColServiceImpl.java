package com.pms.partitionPoints.service.impl;

import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;
import com.pms.partitionPoints.entity.KyEquipmentTypeCareCol;
import com.pms.partitionPoints.mapper.KyEquipmentTypeCareColMapper;
import com.pms.partitionPoints.service.IKyEquipmentTypeCareColService;
/**
 * 特定设备类型的设备 所关心的上传数据列
 *
 * @author lk
 * @email 307169223@qq.com
 * @date 2018-07-18 17:45:51
 */
@Service
public class KyEquipmentTypeCareColServiceImpl extends ServiceImpl<KyEquipmentTypeCareColMapper,KyEquipmentTypeCareCol> implements IKyEquipmentTypeCareColService {

}