package com.pms.partitionPoints.service.impl;

import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;
import com.pms.partitionPoints.entity.KyOrganizationParameters;
import com.pms.partitionPoints.mapper.KyOrganizationParametersMapper;
import com.pms.partitionPoints.service.IKyOrganizationParametersService;
/**
 * 
 *
 * @author lk
 * @email 307169223@qq.com
 * @date 2018-07-27 15:31:33
 */
@Service
public class KyOrganizationParametersServiceImpl extends ServiceImpl<KyOrganizationParametersMapper,KyOrganizationParameters> implements IKyOrganizationParametersService {

}