package com.pms.partitionPoints.service.impl;

import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;
import com.pms.partitionPoints.entity.KyOrganizationGrade;
import com.pms.partitionPoints.mapper.KyOrganizationGradeMapper;
import com.pms.partitionPoints.service.IKyOrganizationGradeService;
/**
 * 机构级别
 *
 * @author lk
 * @email 307169223@qq.com
 * @date 2018-07-18 17:45:51
 */
@Service
public class KyOrganizationGradeServiceImpl extends ServiceImpl<KyOrganizationGradeMapper,KyOrganizationGrade> implements IKyOrganizationGradeService {

}