package com.pms.partitionPoints.service;
import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.service.IService;
import com.pms.partitionPoints.entity.KyEquipmentInfo;
import com.pms.repair.entity.KyCardRecharge;

import java.util.List;
import java.util.Map;

/**
 * 设备信息表
 *
 * @author lk
 * @email 307169223@qq.com
 * @date 2018-07-20 11:02:02
 */

public interface IKyEquipmentInfoService extends  IService<KyEquipmentInfo> {
    //测点和对应的分区插入还有数据库充值
    boolean insertEquipmentInfoAndOrgs(KyEquipmentInfo info,String ids,KyCardRecharge cardRecharge);
    void updateEquipmentInfo(KyEquipmentInfo info);
    Page<Map<String,Object>> queryEquipmentListByOrgId(Page<Map<String,Object>> page, EntityWrapper ew, String orgIds,String strNmae);
}