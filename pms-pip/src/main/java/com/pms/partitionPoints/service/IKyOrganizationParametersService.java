package com.pms.partitionPoints.service;
import com.baomidou.mybatisplus.service.IService;
import com.pms.partitionPoints.entity.KyOrganizationParameters;
/**
 * 
 *
 * @author lk
 * @email 307169223@qq.com
 * @date 2018-07-27 15:31:33
 */

public interface IKyOrganizationParametersService extends  IService<KyOrganizationParameters> {

}