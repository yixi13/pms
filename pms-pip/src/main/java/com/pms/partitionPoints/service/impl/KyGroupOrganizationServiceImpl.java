package com.pms.partitionPoints.service.impl;

import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;
import com.pms.partitionPoints.entity.KyGroupOrganization;
import com.pms.partitionPoints.mapper.KyGroupOrganizationMapper;
import com.pms.partitionPoints.service.IKyGroupOrganizationService;
/**
 * 
 *
 * @author lk
 * @email 307169223@qq.com
 * @date 2018-07-19 15:41:26
 */
@Service
public class KyGroupOrganizationServiceImpl extends ServiceImpl<KyGroupOrganizationMapper,KyGroupOrganization> implements IKyGroupOrganizationService {

    /**
     * 查询角色权限范围的  分区id字符串
     * @param groupId 角色id
     * @return OrgIds
     */
    public String getOrgIdsStrByGroupId(String groupId){
        return baseMapper.getOrgIdsStrByGroupId(groupId);
    }
}