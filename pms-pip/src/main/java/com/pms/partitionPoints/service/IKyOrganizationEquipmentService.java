package com.pms.partitionPoints.service;
import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.service.IService;
import com.pms.partitionPoints.bean.EquipmentBean;
import com.pms.partitionPoints.bean.EquipmentDataBean;
import com.pms.partitionPoints.entity.KyEquipmentUploadData;
import com.pms.partitionPoints.entity.KyOrganizationEquipment;
import org.apache.ibatis.annotations.Param;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * 
 *
 * @author lk
 * @email 307169223@qq.com
 * @date 2018-07-24 11:38:28
 */

public interface IKyOrganizationEquipmentService extends  IService<KyOrganizationEquipment> {
    Page<EquipmentDataBean>  queryEquipmentPages(Page<EquipmentDataBean> page,EntityWrapper ew,Integer orgMaxId);
    Page<KyEquipmentUploadData> queryEquipmentDateListById(Page<KyEquipmentUploadData> page, EntityWrapper ew,Integer orgId);
    List<KyEquipmentUploadData> getEquipmentDateByIdsAndTime(Integer orgId, Integer equipmentId, Date startTime, Date endTime);
    BigDecimal selectOrgWaterUsageByOrgIdAndTime(Integer orgMaxId,Integer orgId,Date startTime,Date endTime);
    BigDecimal getNewestFlowByOrgId(Integer orgMaxId, Integer orgId,Date startTime);
    List<Map<String,Object>> getOrgWaterDayByEquIdsAdTime(Integer orgMaxId,Integer orgId,Date startTime,Date endTime);
    Page<EquipmentDataBean> queryEquipmentPages(Page<EquipmentDataBean> page, EntityWrapper ew,Integer orgMaxId,List<Integer> list,Date startTime,Date endTime,String name);
    List<Map<String,Object>> getEquipmentByOrgMaxId(int orgMaxId);
    List<Integer> getEquipmentsByOrganizationList(String  organizationIdList );
    Set<Map<String,Object>> getEquipmentDateByIdListAndTime(Integer orgId, List<Integer> equList, Date startTime, Date endTime,String param);
    List<EquipmentBean> getEquipmentDateByIdAndTime(Integer orgId, List<String> list, Date startTime, Date endTime);
    int getEquipmentNumByOrgMaxIdAndStatus(int orgMaxId,int equipmentStatus);
}