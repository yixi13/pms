package com.pms.partitionPoints.service;
import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.mapper.Wrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.service.IService;
import com.pms.partitionPoints.bean.OrgBean;
import com.pms.partitionPoints.entity.KyOrganization;
import com.pms.partitionPoints.entity.KyOrganizationEquipment;
import org.apache.ibatis.annotations.Param;

import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * 机构
 *
 * @author lk
 * @email 307169223@qq.com
 * @date 2018-07-18 17:45:06
 */

public interface IKyOrganizationService extends  IService<KyOrganization> {
    /**
     * 根据一级分区id查询 下级  二/三级分区id
     * @param orgId
     * @return
     */
    List<Integer> getSubordinateOrgIds(Integer orgId);

    /**
     * 更具机构id获取groupId
     * @param orgIds
     * @return
     */
    String getGroupIds(String orgIds);
    String getGroupIdsByEq(String equipmentIds);

    List<OrgBean> selectByGroupIdAndCode(Long groupId,String code);
    OrgBean sortList(Integer id,List<OrgBean> list,OrgBean orgBean,Set<Integer> idList);
    //返回当前分区和所有下属分区的集合
    List<String> getOrgIdsByCurrentId(Integer orgId);
    //根据id集合删除对应的分区及分区和测点关联数据
    boolean deleteOrgByIdList(List<String> idList);
    List<Map<String,Object>> getEquipmentsByOrgId(Wrapper<KyOrganizationEquipment> wrapper);

    List<Map<String,Object>> getEquipmentsByOrgIds(String orgId);
    OrgBean sortOrgList(Integer id,List<OrgBean> list,OrgBean orgBean,Set<Integer> parentIdList);
    Page<Map<String,Object>> queryOrgPage(Page<Map<String,Object>> page, EntityWrapper ew, String orgIds,String strName);
    List<OrgBean> getEquipmentsByOrgId(Integer id);

    /**
     * 根据权限和公司编码查询分区
     * @param roleType
     * @param roleId
     * @param code
     * @return
     */
    List<KyOrganization> getOrganizationByRoleTypeAndRoleIdAndCompanyCode(int roleType,long roleId,String code);
    List<Integer> getOrgIdListByCode(String code);
    List<Integer> getEquipmentIdListByOrgMaxId(Integer orgMaxId);
    void deleteSystemByCode(String code);
    Date getUploadDateByEquipmentId(Integer orgMaxId, Integer equipmentId);
    void insertOrgMax(KyOrganization ko);

}