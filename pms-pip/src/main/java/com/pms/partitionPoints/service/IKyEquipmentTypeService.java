package com.pms.partitionPoints.service;
import com.baomidou.mybatisplus.service.IService;
import com.pms.partitionPoints.entity.KyEquipmentType;
/**
 * 
 *
 * @author lk
 * @email 307169223@qq.com
 * @date 2018-07-18 17:45:51
 */

public interface IKyEquipmentTypeService extends  IService<KyEquipmentType> {

}