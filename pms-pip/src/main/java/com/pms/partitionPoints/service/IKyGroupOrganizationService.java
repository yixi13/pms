package com.pms.partitionPoints.service;
import com.baomidou.mybatisplus.service.IService;
import com.pms.partitionPoints.entity.KyGroupOrganization;
/**
 * 
 *
 * @author lk
 * @email 307169223@qq.com
 * @date 2018-07-19 15:41:26
 */

public interface IKyGroupOrganizationService extends  IService<KyGroupOrganization> {

    /**
     * 查询角色权限范围的  分区id字符串
     * @param groupId 角色id
     * @return OrgIds
     */
    String getOrgIdsStrByGroupId(String groupId);
}