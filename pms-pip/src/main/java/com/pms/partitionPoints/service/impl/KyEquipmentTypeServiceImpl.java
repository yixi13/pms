package com.pms.partitionPoints.service.impl;

import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;
import com.pms.partitionPoints.entity.KyEquipmentType;
import com.pms.partitionPoints.mapper.KyEquipmentTypeMapper;
import com.pms.partitionPoints.service.IKyEquipmentTypeService;
/**
 * 
 *
 * @author lk
 * @email 307169223@qq.com
 * @date 2018-07-18 17:45:51
 */
@Service
public class KyEquipmentTypeServiceImpl extends ServiceImpl<KyEquipmentTypeMapper,KyEquipmentType> implements IKyEquipmentTypeService {

}