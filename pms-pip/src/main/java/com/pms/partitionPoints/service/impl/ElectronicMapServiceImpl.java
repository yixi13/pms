package com.pms.partitionPoints.service.impl;

import com.pms.partitionPoints.entity.KyEquipmentUploadData;
import com.pms.partitionPoints.mapper.ElectronicMapMapper;
import com.pms.partitionPoints.service.ElectronicMapService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;

/**
 * 电子地图
 *
 * @author ASUS_
 */
@Service
public class ElectronicMapServiceImpl implements ElectronicMapService {

    @Autowired
    ElectronicMapMapper baseMapper;
    /**
     * 查询分区信息
     * @param paramMap (companyCode-公司编码,level-分区级别,roleOrgIds-角色对应机构id,parentId-上级分区id,id-当前分区id)
     * @return Map (id,name,parentId,level,monitoringsites)
     */
    public List<Map<String,Object>> getOrgDmaInfoByLevel(Map<String,Object> paramMap){
        List<Map<String,Object>> dataList = baseMapper.getOrgDmaInfoByLevel(paramMap);
        return dataList==null?new ArrayList<Map<String,Object>>():dataList;
    }
    /**
     * 查询不在分区范围内的  测点
     * @param paramMap (companyCode-公司编码,level-分区级别,roleOrgIds-角色对应机构id,parentId-查询分区的上级id,oneLevelOrgId-查询分区对应的一级分区Id)
     * @return
     */
    public List<Map<String,Object>> getDmaDorderEquipment(Map<String,Object> paramMap){
        List<Map<String,Object>> dataList = baseMapper.getDmaDorderEquipment(paramMap);
        return dataList==null?new ArrayList<Map<String,Object>>():dataList;
    }

    /**
     * 查询分区下的测点
     * @param orgDmaIds 分区id字符串
     * @return Map(measurepointId,organizationGroupId,measurepointName,measurepointStatus,measurepointDirection,longitude,latitude,orgDmaId)
     */
    public List<Map<String,Object>> getDmaMeasurepointInfo( String orgDmaIds){
        List<Map<String,Object>> dataList = baseMapper.getDmaMeasurepointInfo(orgDmaIds);
        return dataList==null?new ArrayList<Map<String,Object>>():dataList;
    }

    /**
     * 查询测点 瞬时流量
     * @param paramMap (tableSuffix-表名后缀,measurepointIds-测点字符串)
     * @return Map (equipmentId-测点id,instantaneousFlow-瞬时流量)
     */
    public List<Map<String,Object>> getMeasurepointInstantaneousFlow( List<Map<String,Object>> paramMap){
        List<Map<String,Object>> dataList = baseMapper.getMeasurepointInstantaneousFlow(paramMap);
        return dataList==null?new ArrayList<Map<String,Object>>():dataList;
    }
    /**
     * 查询一级分区的id
     * @param id 二级分区id
     * @return
     */
    public  Integer getOneLevelOrgId(Integer id){
        return baseMapper.getOneLevelOrgId(id);
    }

    /**
     * 查询测点 上报数据
     * @param paramMap (tableSuffix-表名后缀,measurepointIds-测点字符串)
     * @return Map (equipmentId-测点id,equipmentStatus-设备状态,communicationStatus-设备通讯状态
     * ,pressure-压力,waterLevel-水位,netCumulativeFlow-净累计流量,positiveCumulativeFlow-正累计流量
     * ,negativeAccumulatedFlow-负累计流量,instantaneousFlow-瞬时流量)
     */
    public List<Map<String,Object>> getMeasurepointDataInfo( List<Map<String,Object>> paramMap){
        List<Map<String,Object>> dataList = baseMapper.getMeasurepointDataInfo(paramMap);
        return dataList==null?new ArrayList<Map<String,Object>>():dataList;
    }

    /**
     * 查询测点 集合
     * @param paramMap (parentId-三级分区id,id-测点id,roleOrgIds-角色拥有的权限机构id,companyCode-公司编码,startRow-分页起始条数,pageSize-每页查询数量)
     * @return Map (id-测点id,name-测点名称,organizationGroupId-数据表名,longitude-经度
     * ,latitude-纬度,partitionName-所属分区名称,partitionId-所属分区id)
     */
   public List<Map<String,Object>> getMeasurepointList(Map<String,Object> paramMap){
       List<Map<String,Object>> dataList = baseMapper.getMeasurepointList(paramMap);
       return dataList==null?new ArrayList<Map<String,Object>>():dataList;
    }

    /**
     * 查询 测点数量
     * @param paramMap (parentId-三级分区id,id-测点id,roleOrgIds-角色拥有的权限机构id,companyCode-公司编码)
     * @return measurepointNum
     */
   public Integer getCountMeasurepointNum(Map<String,Object> paramMap){
       return baseMapper.getCountMeasurepointNum(paramMap);
    }

    /**
     * 查询 用户所有测点
     * @param paramMap (parentId-三级分区id,id-测点id,roleOrgIds-角色拥有的权限机构id,companyCode-公司编码)
     * @return  Map (id-测点id,name-测点名称,longitude-经度,latitude-纬度)
     */
    public List<Map<String,Object>> getUserAllMeasurepoint(Map<String,Object> paramMap){
        return baseMapper.getUserAllMeasurepoint(paramMap);
    }

    /**
     * 查询 用户所有分区
     * @param paramMap (roleId-角色id,companyCode-公司编码)
     * @return
     */
    public List<Map<String,Object>> getUserOrgList(Map<String,Object> paramMap){
        return baseMapper.getUserOrgList(paramMap);
    }

    public List<KyEquipmentUploadData> readAllData(Integer pageNum,Integer pageSize){
        return baseMapper.readAllData(pageNum,pageSize);
    }
}