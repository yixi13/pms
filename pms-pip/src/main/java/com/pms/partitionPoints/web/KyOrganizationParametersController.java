package com.pms.partitionPoints.web;

import com.baomidou.mybatisplus.mapper.Wrapper;
import com.pms.exception.R;
import com.pms.partitionPoints.entity.KyOrganization;
import com.pms.partitionPoints.service.IKyOrganizationService;
import com.pms.validator.Assert;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.plugins.Page;
import org.springframework.web.bind.annotation.*;
import com.pms.controller.BaseController;
import com.pms.partitionPoints.service.IKyOrganizationParametersService;
import com.pms.partitionPoints.entity.KyOrganizationParameters;
import com.pms.result.JsonResult;
import com.pms.result.ObjectRestResponse;
import com.pms.result.TableResultResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;

@RestController
@RequestMapping("kyOrganizationParameters")
@Api(value="计量分区参数管理",description = "计量分区参数管理")
public class KyOrganizationParametersController extends BaseController {
    Logger logger = LoggerFactory.getLogger(getClass());
    @Autowired
    IKyOrganizationParametersService kyOrganizationParametersService;
    @Autowired
    IKyOrganizationService kyOrganizationService;

    /**
         * 新增
         * @param entity
         * @return
         */
    @RequestMapping(value = "add",method = RequestMethod.POST)
    @ResponseBody
    @ApiOperation(value = "计量分区参数添加")
    @ApiImplicitParams({
            @ApiImplicitParam(name="organizationId",value="分区id",required = true,dataType = "Integer",paramType="application/json"),
            @ApiImplicitParam(name="pipeLength",value="供水管长",required = true,dataType = "BigDecimal",paramType="application/json"),
            @ApiImplicitParam(name="freeWaterConsumption",value="免费用水量",required = true,dataType = "BigDecimal",paramType="application/json"),
            @ApiImplicitParam(name="noWaterConsumption",value="未计量用水量估值",required = true,dataType = "BigDecimal",paramType="application/json"),
            @ApiImplicitParam(name="feeWaterConsumption",value="计费用水量",required = true,dataType = "BigDecimal",paramType="application/json"),
            @ApiImplicitParam(name="backgroundLeakage",value="背景漏失",required = true,dataType = "BigDecimal",paramType="application/json"),
            @ApiImplicitParam(name="measureErrer",value="计量误差",required = true,dataType = "BigDecimal",paramType="application/json"),
            @ApiImplicitParam(name="otherLossWater",value="其他损失量",required = true,dataType = "BigDecimal",paramType="application/json"),
            @ApiImplicitParam(name="linkman",value="联系人",required = false,dataType = "String",paramType="application/json"),
            @ApiImplicitParam(name="phone",value="联系电话",required = false,dataType = "String",paramType="application/json"),
            @ApiImplicitParam(name="year",value="对应的年份",required = true,dataType = "Integer",paramType="application/json"),
            @ApiImplicitParam(name="month",value="对应的月份",required = true,dataType = "Integer",paramType="application/json"),
            @ApiImplicitParam(name="remark",value="备注",required = false,dataType = "String",paramType="application/json")
    })
    public R add(@RequestBody KyOrganizationParameters entity){
        Assert.isNull(entity.getOrganizationId(),"分区id不能为空");
        Assert.isNull(entity.getBackgroundLeakage(),"背景漏失量不能为空");
        Assert.isNull(entity.getPipeLength(),"区域供水管长度不能为空");
        Assert.isNull(entity.getNoWaterConsumption(),"未计量用水量不能为空");
        Assert.isNull(entity.getFeeWaterConsumption(),"计费用水量不能为空");
        Assert.isNull(entity.getFeeWaterConsumption(),"免费用水量不能为空");
        Assert.isNull(entity.getMeasureErrer(),"计量误差不能为空");
        Assert.isNull(entity.getOtherLossWater(),"其他损失水量不能为空");
        Assert.isNull(entity.getYear(),"年份不能为空");
        Assert.isNull(entity.getMonth(),"月份不能为空");
        Wrapper<KyOrganizationParameters> wp = new EntityWrapper<>();
        wp.eq("organization_id",entity.getOrganizationId());
        wp.eq("year",entity.getYear());
        wp.eq("month",entity.getMonth());
        int count = kyOrganizationParametersService.selectCount(wp);
        if(count>0){
            return  R.error("此分区在这个年月的参数不能重复添加");
        }
        kyOrganizationParametersService.insert(entity);
        return R.ok();
    }
    /**
     * 修改
     * @param entity
     * @return
     */
    @RequestMapping(value = "/{id}",method = RequestMethod.POST)
    @ResponseBody
    @ApiOperation(value = "计量分区参数修改")
    @ApiImplicitParams({
            @ApiImplicitParam(name="id",value="主键id",required = true,dataType = "Integer",paramType="application/json"),
            @ApiImplicitParam(name="organizationId",value="分区id",required = true,dataType = "Integer",paramType="application/json"),
            @ApiImplicitParam(name="pipeLength",value="供水管长",required = true,dataType = "BigDecimal",paramType="application/json"),
            @ApiImplicitParam(name="freeWaterConsumption",value="免费用水量",required = true,dataType = "BigDecimal",paramType="application/json"),
            @ApiImplicitParam(name="noWaterConsumption",value="未计量用水量估值",required = true,dataType = "BigDecimal",paramType="application/json"),
            @ApiImplicitParam(name="feeWaterConsumption",value="计费用水量",required = true,dataType = "BigDecimal",paramType="application/json"),
            @ApiImplicitParam(name="backgroundLeakage",value="背景漏失",required = true,dataType = "BigDecimal",paramType="application/json"),
            @ApiImplicitParam(name="measureErrer",value="计量误差",required = true,dataType = "BigDecimal",paramType="application/json"),
            @ApiImplicitParam(name="otherLossWater",value="其他损失量",required = true,dataType = "BigDecimal",paramType="application/json"),
            @ApiImplicitParam(name="linkman",value="联系人",required = false,dataType = "String",paramType="application/json"),
            @ApiImplicitParam(name="phone",value="联系电话",required = false,dataType = "String",paramType="application/json"),
            @ApiImplicitParam(name="year",value="对应的年份",required = true,dataType = "Integer",paramType="application/json"),
            @ApiImplicitParam(name="month",value="对应的月份",required = true,dataType = "Integer",paramType="application/json"),
            @ApiImplicitParam(name="remark",value="备注",required = false,dataType = "String",paramType="application/json")
    })
    public R update(@RequestBody KyOrganizationParameters entity){
        Assert.isNull(entity.getId(),"主键id不能为空");
        Assert.isNull(entity.getOrganizationId(),"分区id不能为空");
        Assert.isNull(entity.getBackgroundLeakage(),"背景漏失量不能为空");
        Assert.isNull(entity.getPipeLength(),"区域供水管长度不能为空");
        Assert.isNull(entity.getNoWaterConsumption(),"未计量用水量不能为空");
        Assert.isNull(entity.getFeeWaterConsumption(),"计费用水量不能为空");
        Assert.isNull(entity.getFeeWaterConsumption(),"免费用水量不能为空");
        Assert.isNull(entity.getMeasureErrer(),"计量误差不能为空");
        Assert.isNull(entity.getOtherLossWater(),"其他损失水量不能为空");
        Assert.isNull(entity.getYear(),"年份不能为空");
        Assert.isNull(entity.getMonth(),"月份不能为空");
        kyOrganizationParametersService.updateById(entity);
        return R.ok();
    }

    /**
 * 删除
 * @param id
 * @return
 */
    @RequestMapping(value = "/{id}",method = RequestMethod.DELETE)
    @ResponseBody
    @ApiOperation(value = "计量分区参数删除")
    @ApiImplicitParam(name="id",value="计量分区参数id",required = true,dataType = "Integer",paramType="form")
    public R deleteById(@PathVariable int id){
        Assert.isNull(id,"计量分区参数id不能为空");
        kyOrganizationParametersService.deleteById (id);
        return R.ok();
    }


    /**
     * findByOrgId
     * @param orgId
     * @return
     */
    @RequestMapping(value = "/findByOrgId", method = RequestMethod.GET)
    @ResponseBody
    @ApiImplicitParams({
            @ApiImplicitParam(name="orgId",value="计量分区参数详情",required = true,dataType = "int",paramType="form"),
            @ApiImplicitParam(name="year",value="年份",required = true,dataType = "int",paramType="form"),
            @ApiImplicitParam(name="month",value="月份",required = true,dataType = "int",paramType="form")
    })
    public R findByOrgId(int orgId,int year,int month){
        Assert.isNull(orgId,"计量分区id不能为空");
        Assert.isNull(year,"年份不能为空");
        Assert.isNull(month,"月份不能为空");
        KyOrganization ko = kyOrganizationService.selectById(orgId);
        Assert.isNull(ko,"没有此分区");
        KyOrganizationParameters kp = kyOrganizationParametersService.selectOne(new EntityWrapper<KyOrganizationParameters>().eq("organization_id",orgId).eq("year",year).eq("month",month));
        return R.ok().put("data",kp);
    }


    @RequestMapping(value = "/page",method = RequestMethod.GET)
    @ResponseBody
    public TableResultResponse<KyOrganizationParameters> page(@RequestParam(defaultValue = "10") int limit, @RequestParam(defaultValue = "1")int offset){
        Page<KyOrganizationParameters> page = new Page<KyOrganizationParameters>(offset,limit);
        page = kyOrganizationParametersService.selectPage(page,new EntityWrapper<KyOrganizationParameters>());
        return new TableResultResponse<KyOrganizationParameters>(page.getTotal(),page.getRecords());
    }

    @RequestMapping(value = "/all",method = RequestMethod.GET)
    @ResponseBody
    public List<KyOrganizationParameters> all(){
        return kyOrganizationParametersService.selectList(null);
    }


}