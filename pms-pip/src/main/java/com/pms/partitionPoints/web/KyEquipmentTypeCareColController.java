package com.pms.partitionPoints.web;

import org.springframework.beans.factory.annotation.Autowired;
import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.plugins.Page;
import org.springframework.web.bind.annotation.*;
import com.pms.controller.BaseController;
import com.pms.partitionPoints.service.IKyEquipmentTypeCareColService;
import com.pms.partitionPoints.entity.KyEquipmentTypeCareCol;
import com.pms.result.JsonResult;
import com.pms.result.ObjectRestResponse;
import com.pms.result.TableResultResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;

@RestController
@RequestMapping("kyEquipmentTypeCareCol")
public class KyEquipmentTypeCareColController extends BaseController {
    Logger logger = LoggerFactory.getLogger(getClass());
    @Autowired
    IKyEquipmentTypeCareColService kyEquipmentTypeCareColService;

    /**
         * 新增
         * @param entity
         * @return
         */
    @RequestMapping(value = "",method = RequestMethod.POST)
    @ResponseBody
    public ObjectRestResponse<KyEquipmentTypeCareCol> add(@RequestBody KyEquipmentTypeCareCol entity){
            kyEquipmentTypeCareColService.insert(entity);
        return new ObjectRestResponse<KyEquipmentTypeCareCol>().rel(true);
    }
    /**
     * 修改
     * @param entity
     * @return
     */
    @RequestMapping(value = "/{id}",method = RequestMethod.POST)
    @ResponseBody
    public ObjectRestResponse<KyEquipmentTypeCareCol> update(@RequestBody KyEquipmentTypeCareCol entity){
            kyEquipmentTypeCareColService.updateById(entity);
        return new ObjectRestResponse<KyEquipmentTypeCareCol>().rel(true);
    }

    /**
 * 删除
 * @param id
 * @return
 */
    @RequestMapping(value = "/{id}",method = RequestMethod.DELETE)
    @ResponseBody
    public ObjectRestResponse<KyEquipmentTypeCareCol> deleteById(@PathVariable int id){
            kyEquipmentTypeCareColService.deleteById (id);
        return new ObjectRestResponse<KyEquipmentTypeCareCol>().rel(true);
    }


    /**
     * findByID
     * @param id
     * @return
     */
    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    @ResponseBody
    public ObjectRestResponse<KyEquipmentTypeCareCol> findById(@PathVariable int id){
        return new ObjectRestResponse<KyEquipmentTypeCareCol>().rel(true).data( kyEquipmentTypeCareColService.selectById(id));
    }


    @RequestMapping(value = "/page",method = RequestMethod.GET)
    @ResponseBody
    public TableResultResponse<KyEquipmentTypeCareCol> page(@RequestParam(defaultValue = "10") int limit, @RequestParam(defaultValue = "1")int offset){
        Page<KyEquipmentTypeCareCol> page = new Page<KyEquipmentTypeCareCol>(offset,limit);
        page = kyEquipmentTypeCareColService.selectPage(page,new EntityWrapper<KyEquipmentTypeCareCol>());
        return new TableResultResponse<KyEquipmentTypeCareCol>(page.getTotal(),page.getRecords());
    }

    @RequestMapping(value = "/all",method = RequestMethod.GET)
    @ResponseBody
    public List<KyEquipmentTypeCareCol> all(){
        return kyEquipmentTypeCareColService.selectList(null);
    }


}