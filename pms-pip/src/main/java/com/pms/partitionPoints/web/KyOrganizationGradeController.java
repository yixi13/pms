package com.pms.partitionPoints.web;

import org.springframework.beans.factory.annotation.Autowired;
import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.plugins.Page;
import org.springframework.web.bind.annotation.*;
import com.pms.controller.BaseController;
import com.pms.partitionPoints.service.IKyOrganizationGradeService;
import com.pms.partitionPoints.entity.KyOrganizationGrade;
import com.pms.result.JsonResult;
import com.pms.result.ObjectRestResponse;
import com.pms.result.TableResultResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;

@RestController
@RequestMapping("kyOrganizationGrade")
public class KyOrganizationGradeController extends BaseController {
    Logger logger = LoggerFactory.getLogger(getClass());
    @Autowired
    IKyOrganizationGradeService kyOrganizationGradeService;

    /**
         * 新增
         * @param entity
         * @return
         */
    @RequestMapping(value = "",method = RequestMethod.POST)
    @ResponseBody
    public ObjectRestResponse<KyOrganizationGrade> add(@RequestBody KyOrganizationGrade entity){
            kyOrganizationGradeService.insert(entity);
        return new ObjectRestResponse<KyOrganizationGrade>().rel(true);
    }
    /**
     * 修改
     * @param entity
     * @return
     */
    @RequestMapping(value = "/{id}",method = RequestMethod.POST)
    @ResponseBody
    public ObjectRestResponse<KyOrganizationGrade> update(@RequestBody KyOrganizationGrade entity){
            kyOrganizationGradeService.updateById(entity);
        return new ObjectRestResponse<KyOrganizationGrade>().rel(true);
    }

    /**
 * 删除
 * @param id
 * @return
 */
    @RequestMapping(value = "/{id}",method = RequestMethod.DELETE)
    @ResponseBody
    public ObjectRestResponse<KyOrganizationGrade> deleteById(@PathVariable int id){
            kyOrganizationGradeService.deleteById (id);
        return new ObjectRestResponse<KyOrganizationGrade>().rel(true);
    }


    /**
     * findByID
     * @param id
     * @return
     */
    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    @ResponseBody
    public ObjectRestResponse<KyOrganizationGrade> findById(@PathVariable int id){
        return new ObjectRestResponse<KyOrganizationGrade>().rel(true).data( kyOrganizationGradeService.selectById(id));
    }


    @RequestMapping(value = "/page",method = RequestMethod.GET)
    @ResponseBody
    public TableResultResponse<KyOrganizationGrade> page(@RequestParam(defaultValue = "10") int limit, @RequestParam(defaultValue = "1")int offset){
        Page<KyOrganizationGrade> page = new Page<KyOrganizationGrade>(offset,limit);
        page = kyOrganizationGradeService.selectPage(page,new EntityWrapper<KyOrganizationGrade>());
        return new TableResultResponse<KyOrganizationGrade>(page.getTotal(),page.getRecords());
    }

    @RequestMapping(value = "/all",method = RequestMethod.GET)
    @ResponseBody
    public List<KyOrganizationGrade> all(){
        return kyOrganizationGradeService.selectList(null);
    }


}