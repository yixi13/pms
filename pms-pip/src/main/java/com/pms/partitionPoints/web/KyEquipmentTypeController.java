package com.pms.partitionPoints.web;

import org.springframework.beans.factory.annotation.Autowired;
import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.plugins.Page;
import org.springframework.web.bind.annotation.*;
import com.pms.controller.BaseController;
import com.pms.partitionPoints.service.IKyEquipmentTypeService;
import com.pms.partitionPoints.entity.KyEquipmentType;
import com.pms.result.JsonResult;
import com.pms.result.ObjectRestResponse;
import com.pms.result.TableResultResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;

@RestController
@RequestMapping("kyEquipmentType")
public class KyEquipmentTypeController extends BaseController {
    Logger logger = LoggerFactory.getLogger(getClass());
    @Autowired
    IKyEquipmentTypeService kyEquipmentTypeService;

    /**
         * 新增
         * @param entity
         * @return
         */
    @RequestMapping(value = "",method = RequestMethod.POST)
    @ResponseBody
    public ObjectRestResponse<KyEquipmentType> add(@RequestBody KyEquipmentType entity){
            kyEquipmentTypeService.insert(entity);
        return new ObjectRestResponse<KyEquipmentType>().rel(true);
    }
    /**
     * 修改
     * @param entity
     * @return
     */
    @RequestMapping(value = "/{id}",method = RequestMethod.POST)
    @ResponseBody
    public ObjectRestResponse<KyEquipmentType> update(@RequestBody KyEquipmentType entity){
            kyEquipmentTypeService.updateById(entity);
        return new ObjectRestResponse<KyEquipmentType>().rel(true);
    }

    /**
 * 删除
 * @param id
 * @return
 */
    @RequestMapping(value = "/{id}",method = RequestMethod.DELETE)
    @ResponseBody
    public ObjectRestResponse<KyEquipmentType> deleteById(@PathVariable int id){
            kyEquipmentTypeService.deleteById (id);
        return new ObjectRestResponse<KyEquipmentType>().rel(true);
    }


    /**
     * findByID
     * @param id
     * @return
     */
    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    @ResponseBody
    public ObjectRestResponse<KyEquipmentType> findById(@PathVariable int id){
        return new ObjectRestResponse<KyEquipmentType>().rel(true).data( kyEquipmentTypeService.selectById(id));
    }


    @RequestMapping(value = "/page",method = RequestMethod.GET)
    @ResponseBody
    public TableResultResponse<KyEquipmentType> page(@RequestParam(defaultValue = "10") int limit, @RequestParam(defaultValue = "1")int offset){
        Page<KyEquipmentType> page = new Page<KyEquipmentType>(offset,limit);
        page = kyEquipmentTypeService.selectPage(page,new EntityWrapper<KyEquipmentType>());
        return new TableResultResponse<KyEquipmentType>(page.getTotal(),page.getRecords());
    }

    @RequestMapping(value = "/all",method = RequestMethod.GET)
    @ResponseBody
    public List<KyEquipmentType> all(){
        return kyEquipmentTypeService.selectList(null);
    }


}