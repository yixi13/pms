package com.pms.partitionPoints.web;
import com.pms.exception.R;
import com.pms.util.idutil.SnowFlake;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import com.baomidou.mybatisplus.mapper.EntityWrapper;
import org.springframework.web.bind.annotation.*;
import com.pms.controller.BaseController;
import com.pms.partitionPoints.service.IKyGroupOrganizationService;
import com.pms.partitionPoints.entity.KyGroupOrganization;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import java.util.ArrayList;
import java.util.List;
/**
 *角色管网分区关联表
 */
@RestController
@RequestMapping("groupOrganization")
@Api(value = "角色管网分区关联表",description = "角色管网分区关联表")
public class KyGroupOrganizationController extends BaseController {
    Logger logger = LoggerFactory.getLogger(getClass());
    @Autowired
    IKyGroupOrganizationService groupOrganizationService;
  /*  *//**
     * 添加角色管网分区关联信息
     * @param groupId
     * @param organizationList
     * @return
     *//*
    @PostMapping("/add")
    @ApiOperation("添加角色管网分区关联信息")
    @ApiImplicitParams({
            @ApiImplicitParam(name="groupId",value="角色ID",required = true,dataType = "Long",paramType="form"),
            @ApiImplicitParam(name="organizationList",value="分区ID(数组)",required = true,dataType = "Long[]",paramType="form"),
     })
    public R add(Long groupId,Long[] organizationList){
        parameterIsNull(groupId,"角色ID不能为空");
        parameterIsNull(organizationList,"分区ID不能为空");
         List<KyGroupOrganization> list=new ArrayList<>();
        for(Long i : organizationList) {
            KyGroupOrganization  groupOrganization=new KyGroupOrganization();
            groupOrganization.setId(new SnowFlake(0,0).nextId());
            groupOrganization.setGroupId(groupId);
            groupOrganization.setOrganizationId(i);
            list.add(groupOrganization);
        }
        groupOrganizationService.insertBatch(list);
        return R.ok();
    }*/

    /**
     * 修改角色管网分区关联信息
     * @param groupId
     * @param organizationList
     * @return
     */
    @PostMapping("/updata")
    @ApiOperation("修改角色管网分区关联信息")
    @ApiImplicitParams({
            @ApiImplicitParam(name="groupId",value="角色ID",required = true,dataType = "Long",paramType="form"),
            @ApiImplicitParam(name="organizationList",value="分区ID",required = true,dataType = "String",paramType="form"),
    })
    public R update(Long groupId,String organizationList){
        parameterIsNull(groupId,"角色ID不能为空");
        parameterIsNull(organizationList,"分区ID不能为空");
        if (StringUtils.isNotBlank(organizationList.toString())){
            groupOrganizationService.delete(new EntityWrapper<KyGroupOrganization>().eq("group_id",groupId));
            String[] grouplist=organizationList.split(",");
            for(int x=0;x<grouplist.length;x++){
                String id = grouplist[x];
                KyGroupOrganization  groupOrganization=new KyGroupOrganization();
                groupOrganization.setGroupId(groupId);
                groupOrganization.setOrganizationId(Long.parseLong(id));
                groupOrganizationService.insert(groupOrganization);
            }
        }
        return R.ok();
    }

    @PostMapping("/selectList")
    @ApiOperation("查询角色管网分区关联信息")
    @ApiImplicitParams({
            @ApiImplicitParam(name="groupId",value="角色ID",required = true,dataType = "Long",paramType="form"),
    })
    public R selectList(Long groupId){
        parameterIsNull(groupId,"角色ID不能为空");
        List<KyGroupOrganization> list=groupOrganizationService.selectList(new EntityWrapper<KyGroupOrganization>().eq("group_id",groupId));
        return R.ok().put("data",list);
    }

}