package com.pms.partitionPoints.entity;
import com.baomidou.mybatisplus.activerecord.Model;
import java.io.Serializable;
import java.util.Date;
import javax.persistence.*;

import com.baomidou.mybatisplus.enums.IdType;
import com.pms.util.Description;
import com.pms.entity.BaseModel;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;


/**
 * 机构
 * 
 * @author lk
 * @email 307169223@qq.com
 * @date 2018-07-18 17:45:06
 */
@TableName( "ky_organization")
public class KyOrganization extends BaseModel<KyOrganization> {
private static final long serialVersionUID = 1L;


	/**
	 *主键
	 */
    @TableId(value="id",type= IdType.AUTO)
    private Integer id;
	
	    /**
     *级别id(2-一级分区、3-二级分区/4-三级分区)
     */
    @TableField("organizationGradeId")
    @Description("级别id(2-一级分区、3-二级分区/4-三级分区)")
    private Integer organizationgradeid;
	
	/**
     *父级组织机构id
     */
    @TableField("parentId")
    @Description("父级组织机构id")
    private Integer parentid;
	
	    /**
     *状态，1：正常；2，禁用；3：删除
     */
    @TableField("status")
    @Description("状态，1：正常；2，禁用；3：删除")
    private Integer status;
	
	    /**
     *名称
     */
    @TableField("name")
    @Description("名称")
    private String name;
	
	    /**
     *备注
     */
    @TableField("description")
    @Description("备注")
    private String description;
	
	    /**
     *创建人id
     */
    @TableField("creatorId")
    @Description("创建人id")
    private Integer creatorid;
	
	    /**
     *创建时间
     */
    @TableField("createTime")
    @Description("创建时间")
    private Date createtime;
	
	    /**
     *编码
     */
    @TableField("code")
    @Description("编码")
    private String code;
	
	    /**
     *排序权重
     */
    @TableField("weightOrder")
    @Description("排序权重")
    private Integer weightorder;
	
	    /**
     *营业执照
     */
    @TableField("businessLicense")
    @Description("营业执照")
    private String businesslicense;
	
	    /**
     *经营地址
     */
    @TableField("businessAddress")
    @Description("经营地址")
    private String businessaddress;
	
	    /**
     *联系人
     */
    @TableField("linkMan")
    @Description("联系人")
    private String linkman;
	
	    /**
     *联系电话
     */
    @TableField("linkPhone")
    @Description("联系电话")
    private String linkphone;
	
	    /**
     *机构简介
     */
    @TableField("organizationProfile")
    @Description("机构简介")
    private String organizationprofile;
	
	    /**
     *LOGO文件
     */
    @TableField("logoFile")
    @Description("LOGO文件")
    private String logofile;
	
	    /**
     *系统简称
     */
    @TableField("systemAbbreviation")
    @Description("系统简称")
    private String systemabbreviation;
	
	    /**
     *更新日期
     */
    @TableField("updateTime")
    @Description("更新日期")
    private Date updatetime;
	
	    /**
     *更新操作员
     */
    @TableField("updateId")
    @Description("更新操作员")
    private Integer updateid;
	
	    /**
     *日志地址
     */
    @TableField("logourl")
    @Description("日志地址")
    private String logourl;
	
	    /**
     *机构类型（1：正常机构；2：默认机构，新增机构时用的关于权限和角色de 模版）
     */
    @TableField("type")
    @Description("机构类型（1：正常机构；2：默认机构，新增机构时用的关于权限和角色de 模版）")
    private Integer type;

	/**
	 * 分区中的边界点
	 */
	@TableField("monitoringsites")
	@Description("分区中的边界点")
	private String monitoringsites;
	

@Override
protected Serializable pkVal() {
        return this.id;
}
	/**
	 * 设置：主键
	 */
	public KyOrganization setId(Integer id) {
		this.id = id;
		return this;
	}
	/**
	 * 获取：主键
	 */
	public Integer getId() {
		return id;
	}
	/**
	 * 设置：级别id(2-一级分区、3-二级分区/4-三级分区)
	 */
	public KyOrganization setOrganizationgradeid(Integer organizationgradeid) {
		this.organizationgradeid = organizationgradeid;
		return this;
	}
	/**
	 * 获取：级别id(2-一级分区、3-二级分区/4-三级分区)
	 */
	public Integer getOrganizationgradeid() {
		return organizationgradeid;
	}
	/**
	 * 设置：父级组织机构id
	 */
	public KyOrganization setParentid(Integer parentid) {
		this.parentid = parentid;
		return this;
	}
	/**
	 * 获取：父级组织机构id
	 */
	public Integer getParentid() {
		return parentid;
	}
	/**
	 * 设置：状态，1：正常；2，禁用；3：删除
	 */
	public KyOrganization setStatus(Integer status) {
		this.status = status;
		return this;
	}
	/**
	 * 获取：状态，1：正常；2，禁用；3：删除
	 */
	public Integer getStatus() {
		return status;
	}
	/**
	 * 设置：名称
	 */
	public KyOrganization setName(String name) {
		this.name = name;
		return this;
	}
	/**
	 * 获取：名称
	 */
	public String getName() {
		return name;
	}
	/**
	 * 设置：备注
	 */
	public KyOrganization setDescription(String description) {
		this.description = description;
		return this;
	}
	/**
	 * 获取：备注
	 */
	public String getDescription() {
		return description;
	}
	/**
	 * 设置：创建人id
	 */
	public KyOrganization setCreatorid(Integer creatorid) {
		this.creatorid = creatorid;
		return this;
	}
	/**
	 * 获取：创建人id
	 */
	public Integer getCreatorid() {
		return creatorid;
	}
	/**
	 * 设置：创建时间
	 */
	public KyOrganization setCreatetime(Date createtime) {
		this.createtime = createtime;
		return this;
	}
	/**
	 * 获取：创建时间
	 */
	public Date getCreatetime() {
		return createtime;
	}
	/**
	 * 设置：编码
	 */
	public KyOrganization setCode(String code) {
		this.code = code;
		return this;
	}
	/**
	 * 获取：编码
	 */
	public String getCode() {
		return code;
	}
	/**
	 * 设置：排序权重
	 */
	public KyOrganization setWeightorder(Integer weightorder) {
		this.weightorder = weightorder;
		return this;
	}
	/**
	 * 获取：排序权重
	 */
	public Integer getWeightorder() {
		return weightorder;
	}
	/**
	 * 设置：营业执照
	 */
	public KyOrganization setBusinesslicense(String businesslicense) {
		this.businesslicense = businesslicense;
		return this;
	}
	/**
	 * 获取：营业执照
	 */
	public String getBusinesslicense() {
		return businesslicense;
	}
	/**
	 * 设置：经营地址
	 */
	public KyOrganization setBusinessaddress(String businessaddress) {
		this.businessaddress = businessaddress;
		return this;
	}
	/**
	 * 获取：经营地址
	 */
	public String getBusinessaddress() {
		return businessaddress;
	}
	/**
	 * 设置：联系人
	 */
	public KyOrganization setLinkman(String linkman) {
		this.linkman = linkman;
		return this;
	}
	/**
	 * 获取：联系人
	 */
	public String getLinkman() {
		return linkman;
	}
	/**
	 * 设置：联系电话
	 */
	public KyOrganization setLinkphone(String linkphone) {
		this.linkphone = linkphone;
		return this;
	}
	/**
	 * 获取：联系电话
	 */
	public String getLinkphone() {
		return linkphone;
	}
	/**
	 * 设置：机构简介
	 */
	public KyOrganization setOrganizationprofile(String organizationprofile) {
		this.organizationprofile = organizationprofile;
		return this;
	}
	/**
	 * 获取：机构简介
	 */
	public String getOrganizationprofile() {
		return organizationprofile;
	}
	/**
	 * 设置：LOGO文件
	 */
	public KyOrganization setLogofile(String logofile) {
		this.logofile = logofile;
		return this;
	}
	/**
	 * 获取：LOGO文件
	 */
	public String getLogofile() {
		return logofile;
	}
	/**
	 * 设置：系统简称
	 */
	public KyOrganization setSystemabbreviation(String systemabbreviation) {
		this.systemabbreviation = systemabbreviation;
		return this;
	}
	/**
	 * 获取：系统简称
	 */
	public String getSystemabbreviation() {
		return systemabbreviation;
	}
	/**
	 * 设置：更新日期
	 */
	public KyOrganization setUpdatetime(Date updatetime) {
		this.updatetime = updatetime;
		return this;
	}
	/**
	 * 获取：更新日期
	 */
	public Date getUpdatetime() {
		return updatetime;
	}
	/**
	 * 设置：更新操作员
	 */
	public KyOrganization setUpdateid(Integer updateid) {
		this.updateid = updateid;
		return this;
	}
	/**
	 * 获取：更新操作员
	 */
	public Integer getUpdateid() {
		return updateid;
	}
	/**
	 * 设置：日志地址
	 */
	public KyOrganization setLogourl(String logourl) {
		this.logourl = logourl;
		return this;
	}
	/**
	 * 获取：日志地址
	 */
	public String getLogourl() {
		return logourl;
	}
	/**
	 * 设置：机构类型（1：正常机构；2：默认机构，新增机构时用的关于权限和角色de 模版）
	 */
	public KyOrganization setType(Integer type) {
		this.type = type;
		return this;
	}
	/**
	 * 获取：机构类型（1：正常机构；2：默认机构，新增机构时用的关于权限和角色de 模版）
	 */
	public Integer getType() {
		return type;
	}

	public String getMonitoringsites() {
		return monitoringsites;
	}

	public void setMonitoringsites(String monitoringsites) {
		this.monitoringsites = monitoringsites;
	}
}
