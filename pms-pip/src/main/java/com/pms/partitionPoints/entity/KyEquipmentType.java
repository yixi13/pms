package com.pms.partitionPoints.entity;
import com.baomidou.mybatisplus.activerecord.Model;
import java.io.Serializable;
import java.util.Date;
import javax.persistence.*;

import com.baomidou.mybatisplus.enums.IdType;
import com.pms.util.Description;
import com.pms.entity.BaseModel;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;


/**
 * 
 * 
 * @author lk
 * @email 307169223@qq.com
 * @date 2018-07-18 17:45:51
 */
@TableName( "ky_equipment_type")
public class KyEquipmentType extends BaseModel<KyEquipmentType> {
private static final long serialVersionUID = 1L;


	    /**
	 *主键
	 */
    @TableId(value="id",type= IdType.AUTO)
    private Integer id;
	
	    /**
     *名称
     */
    @TableField("name")
    @Description("名称")
    private String name;
	
	    /**
     *协议版本
     */
    @TableField("protocolVersion")
    @Description("协议版本")
    private String protocolversion;
	
	    /**
     *设备地址
     */
    @TableField("equipmentAddress")
    @Description("设备地址")
    private String equipmentaddress;
	
	    /**
     *是否轮询（1：是；0：否）
     */
    @TableField("isPolling")
    @Description("是否轮询（1：是；0：否）")
    private Integer ispolling;
	
	    /**
     *轮询间隔:秒数
     */
    @TableField("pollingInterval")
    @Description("轮询间隔:秒数")
    private Integer pollinginterval;
	
	    /**
     *状态(1：正常；2：禁用；3：删除)
     */
    @TableField("status")
    @Description("状态(1：正常；2：禁用；3：删除)")
    private Integer status;
	
	    /**
     *通道类型
     */
    @TableField("channelType")
    @Description("通道类型")
    private String channeltype;
	
	    /**
     *备注
     */
    @TableField("description")
    @Description("备注")
    private String description;
	
	    /**
     *排序
     */
    @TableField("sort")
    @Description("排序")
    private Integer sort;
	

@Override
protected Serializable pkVal() {
        return this.id;
}
	/**
	 * 设置：主键
	 */
	public KyEquipmentType setId(Integer id) {
		this.id = id;
		return this;
	}
	/**
	 * 获取：主键
	 */
	public Integer getId() {
		return id;
	}
	/**
	 * 设置：名称
	 */
	public KyEquipmentType setName(String name) {
		this.name = name;
		return this;
	}
	/**
	 * 获取：名称
	 */
	public String getName() {
		return name;
	}
	/**
	 * 设置：协议版本
	 */
	public KyEquipmentType setProtocolversion(String protocolversion) {
		this.protocolversion = protocolversion;
		return this;
	}
	/**
	 * 获取：协议版本
	 */
	public String getProtocolversion() {
		return protocolversion;
	}
	/**
	 * 设置：设备地址
	 */
	public KyEquipmentType setEquipmentaddress(String equipmentaddress) {
		this.equipmentaddress = equipmentaddress;
		return this;
	}
	/**
	 * 获取：设备地址
	 */
	public String getEquipmentaddress() {
		return equipmentaddress;
	}
	/**
	 * 设置：是否轮询（1：是；0：否）
	 */
	public KyEquipmentType setIspolling(Integer ispolling) {
		this.ispolling = ispolling;
		return this;
	}
	/**
	 * 获取：是否轮询（1：是；0：否）
	 */
	public Integer getIspolling() {
		return ispolling;
	}
	/**
	 * 设置：轮询间隔:秒数
	 */
	public KyEquipmentType setPollinginterval(Integer pollinginterval) {
		this.pollinginterval = pollinginterval;
		return this;
	}
	/**
	 * 获取：轮询间隔:秒数
	 */
	public Integer getPollinginterval() {
		return pollinginterval;
	}
	/**
	 * 设置：状态(1：正常；2：禁用；3：删除)
	 */
	public KyEquipmentType setStatus(Integer status) {
		this.status = status;
		return this;
	}
	/**
	 * 获取：状态(1：正常；2：禁用；3：删除)
	 */
	public Integer getStatus() {
		return status;
	}
	/**
	 * 设置：通道类型
	 */
	public KyEquipmentType setChanneltype(String channeltype) {
		this.channeltype = channeltype;
		return this;
	}
	/**
	 * 获取：通道类型
	 */
	public String getChanneltype() {
		return channeltype;
	}
	/**
	 * 设置：备注
	 */
	public KyEquipmentType setDescription(String description) {
		this.description = description;
		return this;
	}
	/**
	 * 获取：备注
	 */
	public String getDescription() {
		return description;
	}
	/**
	 * 设置：排序
	 */
	public KyEquipmentType setSort(Integer sort) {
		this.sort = sort;
		return this;
	}
	/**
	 * 获取：排序
	 */
	public Integer getSort() {
		return sort;
	}


}
