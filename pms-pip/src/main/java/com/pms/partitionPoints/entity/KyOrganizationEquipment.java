package com.pms.partitionPoints.entity;
import com.baomidou.mybatisplus.activerecord.Model;
import java.io.Serializable;
import java.util.Date;
import javax.persistence.*;

import com.baomidou.mybatisplus.enums.IdType;
import com.pms.util.Description;
import com.pms.entity.BaseModel;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;


/**
 * 
 * 
 * @author lk
 * @email 307169223@qq.com
 * @date 2018-07-24 11:38:28
 */
@TableName( "ky_organization_equipment")
public class KyOrganizationEquipment extends BaseModel<KyOrganizationEquipment> {
private static final long serialVersionUID = 1L;


	    /**
	 *DMA关联测点表
	 */
    @TableId(value="id",type= IdType.AUTO)
    private Integer id;
	
	    /**
     *dmaId
     */
    @TableField("organizationId")
    @Description("dmaId")
    private Integer organizationid;
	
	    /**
     *测点id
     */
    @TableField("equipmentId")
    @Description("测点id")
    private Integer equipmentid;
	
	    /**
     *流动方向(1表示流入 0 表示流出) 5.表示普通测点
     */
    @TableField("direction")
    @Description("流动方向(1表示流入 0 表示流出)")
    private Integer direction;

@Override
protected Serializable pkVal() {
        return this.id;
}
	/**
	 * 设置：DMA关联测点表
	 */
	public KyOrganizationEquipment setId(Integer id) {
		this.id = id;
		return this;
	}
	/**
	 * 获取：DMA关联测点表
	 */
	public Integer getId() {
		return id;
	}
	/**
	 * 设置：dmaId
	 */
	public KyOrganizationEquipment setOrganizationid(Integer organizationid) {
		this.organizationid = organizationid;
		return this;
	}
	/**
	 * 获取：dmaId
	 */
	public Integer getOrganizationid() {
		return organizationid;
	}
	/**
	 * 设置：测点id
	 */
	public KyOrganizationEquipment setEquipmentid(Integer equipmentid) {
		this.equipmentid = equipmentid;
		return this;
	}
	/**
	 * 获取：测点id
	 */
	public Integer getEquipmentid() {
		return equipmentid;
	}
	/**
	 * 设置：流动方向(1表示流入 0 表示流出)
	 */
	public KyOrganizationEquipment setDirection(Integer direction) {
		this.direction = direction;
		return this;
	}
	/**
	 * 获取：流动方向(1表示流入 0 表示流出)
	 */
	public Integer getDirection() {
		return direction;
	}


}
