package com.pms.partitionPoints.entity;
import com.baomidou.mybatisplus.activerecord.Model;
import java.io.Serializable;
import java.util.Date;
import javax.persistence.*;

import com.baomidou.mybatisplus.enums.IdType;
import com.pms.util.Description;
import com.pms.entity.BaseModel;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;


/**
 * 机构级别
 * 
 * @author lk
 * @email 307169223@qq.com
 * @date 2018-07-18 17:45:51
 */
@TableName( "ky_organization_grade")
public class KyOrganizationGrade extends BaseModel<KyOrganizationGrade> {
private static final long serialVersionUID = 1L;


	    /**
	 *主键
	 */
    @TableId(value="id",type= IdType.AUTO)
    private Integer id;
	
	    /**
     *父级机构id
     */
    @TableField("parentId")
    @Description("父级机构id")
    private Integer parentid;
	
	    /**
     *名称
     */
    @TableField("name")
    @Description("名称")
    private String name;
	
	    /**
     *级别代码
     */
    @TableField("code")
    @Description("级别代码")
    private String code;
	
	    /**
     *描述
     */
    @TableField("description")
    @Description("描述")
    private String description;
	
	    /**
     *状态，1：正常；2：废除
     */
    @TableField("status")
    @Description("状态，1：正常；2：废除")
    private Integer status;
	
	    /**
     *图标
     */
    @TableField("icon")
    @Description("图标")
    private String icon;
	
	    /**
     *权重
     */
    @TableField("weight")
    @Description("权重")
    private Integer weight;

@Override
protected Serializable pkVal() {
        return this.id;
}
	/**
	 * 设置：主键
	 */
	public KyOrganizationGrade setId(Integer id) {
		this.id = id;
		return this;
	}
	/**
	 * 获取：主键
	 */
	public Integer getId() {
		return id;
	}
	/**
	 * 设置：父级机构id
	 */
	public KyOrganizationGrade setParentid(Integer parentid) {
		this.parentid = parentid;
		return this;
	}
	/**
	 * 获取：父级机构id
	 */
	public Integer getParentid() {
		return parentid;
	}
	/**
	 * 设置：名称
	 */
	public KyOrganizationGrade setName(String name) {
		this.name = name;
		return this;
	}
	/**
	 * 获取：名称
	 */
	public String getName() {
		return name;
	}
	/**
	 * 设置：级别代码
	 */
	public KyOrganizationGrade setCode(String code) {
		this.code = code;
		return this;
	}
	/**
	 * 获取：级别代码
	 */
	public String getCode() {
		return code;
	}
	/**
	 * 设置：描述
	 */
	public KyOrganizationGrade setDescription(String description) {
		this.description = description;
		return this;
	}
	/**
	 * 获取：描述
	 */
	public String getDescription() {
		return description;
	}
	/**
	 * 设置：状态，1：正常；2：废除
	 */
	public KyOrganizationGrade setStatus(Integer status) {
		this.status = status;
		return this;
	}
	/**
	 * 获取：状态，1：正常；2：废除
	 */
	public Integer getStatus() {
		return status;
	}
	/**
	 * 设置：图标
	 */
	public KyOrganizationGrade setIcon(String icon) {
		this.icon = icon;
		return this;
	}
	/**
	 * 获取：图标
	 */
	public String getIcon() {
		return icon;
	}
	/**
	 * 设置：权重
	 */
	public KyOrganizationGrade setWeight(Integer weight) {
		this.weight = weight;
		return this;
	}
	/**
	 * 获取：权重
	 */
	public Integer getWeight() {
		return weight;
	}


}
