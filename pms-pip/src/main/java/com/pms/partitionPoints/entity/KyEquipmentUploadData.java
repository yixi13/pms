package com.pms.partitionPoints.entity;
import com.baomidou.mybatisplus.activerecord.Model;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import javax.persistence.*;
import com.pms.util.Description;
import com.pms.entity.BaseModel;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;
import com.pms.util.baidu.DataPointAnnotation;


/**
 * 设备上传数据
 * 
 * @author lk
 * @email 307169223@qq.com
 * @date 2018-08-06 16:30:52
 */
@TableName( "ky_equipment_upload_data")
@DataPointAnnotation(name="equipment_data",type = 1)
public class KyEquipmentUploadData extends BaseModel<KyEquipmentUploadData> {
private static final long serialVersionUID = 1L;


	    /**
	 *主键
	 */
    @TableId("id")
    private Integer id;
	
	    /**
     *设备id
     */
    @TableField("equipmentId")
    @Description("设备id")
	@DataPointAnnotation(name="serial_number",type = 3)
    private Integer equipmentid;
	
	    /**
     *记录时间
     */
    @TableField("creatTime")
    @Description("记录时间")
    private Date creattime;
	
	    /**
     *采集时间
     */
    @TableField("acquisitionTime")
    @Description("采集时间")
	@DataPointAnnotation(type = 4)
    private Date acquisitiontime;
	
	    /**
     *设备状态
     */
    @TableField("equipmentStatus")
    @Description("设备状态")
    private String equipmentstatus;
	/**
	 *设备状态
	 */
	@TableField(exist = false)
	@Description("设备状态")
	@DataPointAnnotation(type = 2)
	private Integer deviceStatus;
	    /**
     *通讯状态
     */
    @TableField("communicationStatus")
    @Description("通讯状态")
    private String communicationstatus;
	
	    /**
     *数据来源
     */
    @TableField("dataSources")
    @Description("数据来源")
    private String datasources;
	
	    /**
     *净累计流量
     */
    @TableField("netCumulativeFlow")
    @Description("净累计流量")
	@DataPointAnnotation(type = 2)
    private BigDecimal netcumulativeflow;
	
	    /**
     *正累计流量
     */
    @TableField("positiveCumulativeFlow")
    @Description("正累计流量")
	@DataPointAnnotation(type = 2)
    private BigDecimal positivecumulativeflow;
	
	    /**
     *负累计流量
     */
    @TableField("negativeAccumulatedFlow")
    @Description("负累计流量")
	@DataPointAnnotation(type = 2)
    private BigDecimal negativeaccumulatedflow;
	
	    /**
     *瞬时流量
     */
    @TableField("instantaneousFlow")
    @Description("瞬时流量")
	@DataPointAnnotation(type = 2)
    private BigDecimal instantaneousflow;
	
	    /**
     *电源电压
     */
    @TableField("supplyVoltage")
    @Description("电源电压")
	@DataPointAnnotation(type = 2)
    private BigDecimal supplyvoltage;
	
	    /**
     *GPRS信号强度
     */
    @TableField("gprsSignalIntensity")
    @Description("GPRS信号强度")
    private BigDecimal gprssignalintensity;
	
	    /**
     *压力
     */
    @TableField("pressure")
    @Description("压力")
	@DataPointAnnotation(type = 2)
    private BigDecimal pressure;
	
	    /**
     *水位
     */
    @TableField("waterLevel")
    @Description("水位")
    private BigDecimal waterlevel;
	
	    /**
     *电池电压报警
     */
    @TableField("batteryVoltageAlarm")
    @Description("电池电压报警")
    private String batteryvoltagealarm;
	
	    /**
     *串口通讯状态
     */
    @TableField("serialCommunicationState")
    @Description("串口通讯状态")
    private String serialcommunicationstate;
	
	    /**
     *箱门开关
     */
    @TableField("doorSwitch")
    @Description("箱门开关")
    private String doorswitch;
	
	    /**
     *压力故障
     */
    @TableField("pressureFault")
    @Description("压力故障")
    private String pressurefault;
	
	    /**
     *压力上限报警
     */
    @TableField("higherPressureAlarm")
    @Description("压力上限报警")
    private String higherpressurealarm;
	
	    /**
     *压力下限报警
     */
    @TableField("lowerPressureAlarm")
    @Description("压力下限报警")
    private String lowerpressurealarm;
	
	    /**
     *电池电压低报警
     */
    @TableField("batteryVoltageLowAlarm")
    @Description("电池电压低报警")
    private String batteryvoltagelowalarm;
	
	    /**
     *电池电压过低报警
     */
    @TableField("batteryVoltageTooLowAlarm")
    @Description("电池电压过低报警")
    private String batteryvoltagetoolowalarm;
	
	    /**
     *压力状态
     */
    @TableField("pressureStatus")
    @Description("压力状态")
    private String pressurestatus;
	
	    /**
     *电源电压状态
     */
    @TableField("powerSupplyVoltage")
    @Description("电源电压状态")
    private String powersupplyvoltage;
	
	    /**
     *水位故障
     */
    @TableField("waterLevelFault")
    @Description("水位故障")
    private String waterlevelfault;
	
	    /**
     *水位上限报警
     */
    @TableField("waterUpLevelAlarm")
    @Description("水位上限报警")
    private String wateruplevelalarm;
	
	    /**
     *水位下限报警
     */
    @TableField("waterDownLevelAlarm")
    @Description("水位下限报警")
    private String waterdownlevelalarm;
	
	    /**
     *水位状态
     */
    @TableField("waterLevelStatus")
    @Description("水位状态")
    private String waterlevelstatus;
	

@Override
protected Serializable pkVal() {
        return this.id;
}
	/**
	 * 设置：主键
	 */
	public KyEquipmentUploadData setId(Integer id) {
		this.id = id;
		return this;
	}
	/**
	 * 获取：主键
	 */
	public Integer getId() {
		return id;
	}
	/**
	 * 设置：设备id
	 */
	public KyEquipmentUploadData setEquipmentid(Integer equipmentid) {
		this.equipmentid = equipmentid;
		return this;
	}
	/**
	 * 获取：设备id
	 */
	public Integer getEquipmentid() {
		return equipmentid;
	}
	/**
	 * 设置：记录时间
	 */
	public KyEquipmentUploadData setCreattime(Date creattime) {
		this.creattime = creattime;
		return this;
	}
	/**
	 * 获取：记录时间
	 */
	public Date getCreattime() {
		return creattime;
	}
	/**
	 * 设置：采集时间
	 */
	public KyEquipmentUploadData setAcquisitiontime(Date acquisitiontime) {
		this.acquisitiontime = acquisitiontime;
		return this;
	}
	/**
	 * 获取：采集时间
	 */
	public Date getAcquisitiontime() {
		return acquisitiontime;
	}
	/**
	 * 设置：设备状态
	 */
	public KyEquipmentUploadData setEquipmentstatus(String equipmentstatus) {
		this.equipmentstatus = equipmentstatus;
		return this;
	}
	/**
	 * 获取：设备状态
	 */
	public String getEquipmentstatus() {
		return equipmentstatus;
	}
	/**
	 * 设置：通讯状态
	 */
	public KyEquipmentUploadData setCommunicationstatus(String communicationstatus) {
		this.communicationstatus = communicationstatus;
		return this;
	}
	/**
	 * 获取：通讯状态
	 */
	public String getCommunicationstatus() {
		return communicationstatus;
	}
	/**
	 * 设置：数据来源
	 */
	public KyEquipmentUploadData setDatasources(String datasources) {
		this.datasources = datasources;
		return this;
	}
	/**
	 * 获取：数据来源
	 */
	public String getDatasources() {
		return datasources;
	}
	/**
	 * 设置：净累计流量
	 */
	public KyEquipmentUploadData setNetcumulativeflow(BigDecimal netcumulativeflow) {
		this.netcumulativeflow = netcumulativeflow;
		return this;
	}
	/**
	 * 获取：净累计流量
	 */
	public BigDecimal getNetcumulativeflow() {
		return netcumulativeflow;
	}
	/**
	 * 设置：正累计流量
	 */
	public KyEquipmentUploadData setPositivecumulativeflow(BigDecimal positivecumulativeflow) {
		this.positivecumulativeflow = positivecumulativeflow;
		return this;
	}
	/**
	 * 获取：正累计流量
	 */
	public BigDecimal getPositivecumulativeflow() {
		return positivecumulativeflow;
	}
	/**
	 * 设置：负累计流量
	 */
	public KyEquipmentUploadData setNegativeaccumulatedflow(BigDecimal negativeaccumulatedflow) {
		this.negativeaccumulatedflow = negativeaccumulatedflow;
		return this;
	}
	/**
	 * 获取：负累计流量
	 */
	public BigDecimal getNegativeaccumulatedflow() {
		return negativeaccumulatedflow;
	}
	/**
	 * 设置：瞬时流量
	 */
	public KyEquipmentUploadData setInstantaneousflow(BigDecimal instantaneousflow) {
		this.instantaneousflow = instantaneousflow;
		return this;
	}
	/**
	 * 获取：瞬时流量
	 */
	public BigDecimal getInstantaneousflow() {
		return instantaneousflow;
	}
	/**
	 * 设置：电源电压
	 */
	public KyEquipmentUploadData setSupplyvoltage(BigDecimal supplyvoltage) {
		this.supplyvoltage = supplyvoltage;
		return this;
	}
	/**
	 * 获取：电源电压
	 */
	public BigDecimal getSupplyvoltage() {
		return supplyvoltage;
	}
	/**
	 * 设置：GPRS信号强度
	 */
	public KyEquipmentUploadData setGprssignalintensity(BigDecimal gprssignalintensity) {
		this.gprssignalintensity = gprssignalintensity;
		return this;
	}
	/**
	 * 获取：GPRS信号强度
	 */
	public BigDecimal getGprssignalintensity() {
		return gprssignalintensity;
	}
	/**
	 * 设置：压力
	 */
	public KyEquipmentUploadData setPressure(BigDecimal pressure) {
		this.pressure = pressure;
		return this;
	}
	/**
	 * 获取：压力
	 */
	public BigDecimal getPressure() {
		return pressure;
	}
	/**
	 * 设置：水位
	 */
	public KyEquipmentUploadData setWaterlevel(BigDecimal waterlevel) {
		this.waterlevel = waterlevel;
		return this;
	}
	/**
	 * 获取：水位
	 */
	public BigDecimal getWaterlevel() {
		return waterlevel;
	}
	/**
	 * 设置：电池电压报警
	 */
	public KyEquipmentUploadData setBatteryvoltagealarm(String batteryvoltagealarm) {
		this.batteryvoltagealarm = batteryvoltagealarm;
		return this;
	}
	/**
	 * 获取：电池电压报警
	 */
	public String getBatteryvoltagealarm() {
		return batteryvoltagealarm;
	}
	/**
	 * 设置：串口通讯状态
	 */
	public KyEquipmentUploadData setSerialcommunicationstate(String serialcommunicationstate) {
		this.serialcommunicationstate = serialcommunicationstate;
		return this;
	}
	/**
	 * 获取：串口通讯状态
	 */
	public String getSerialcommunicationstate() {
		return serialcommunicationstate;
	}
	/**
	 * 设置：箱门开关
	 */
	public KyEquipmentUploadData setDoorswitch(String doorswitch) {
		this.doorswitch = doorswitch;
		return this;
	}
	/**
	 * 获取：箱门开关
	 */
	public String getDoorswitch() {
		return doorswitch;
	}
	/**
	 * 设置：压力故障
	 */
	public KyEquipmentUploadData setPressurefault(String pressurefault) {
		this.pressurefault = pressurefault;
		return this;
	}
	/**
	 * 获取：压力故障
	 */
	public String getPressurefault() {
		return pressurefault;
	}
	/**
	 * 设置：压力上限报警
	 */
	public KyEquipmentUploadData setHigherpressurealarm(String higherpressurealarm) {
		this.higherpressurealarm = higherpressurealarm;
		return this;
	}
	/**
	 * 获取：压力上限报警
	 */
	public String getHigherpressurealarm() {
		return higherpressurealarm;
	}
	/**
	 * 设置：压力下限报警
	 */
	public KyEquipmentUploadData setLowerpressurealarm(String lowerpressurealarm) {
		this.lowerpressurealarm = lowerpressurealarm;
		return this;
	}
	/**
	 * 获取：压力下限报警
	 */
	public String getLowerpressurealarm() {
		return lowerpressurealarm;
	}
	/**
	 * 设置：电池电压低报警
	 */
	public KyEquipmentUploadData setBatteryvoltagelowalarm(String batteryvoltagelowalarm) {
		this.batteryvoltagelowalarm = batteryvoltagelowalarm;
		return this;
	}
	/**
	 * 获取：电池电压低报警
	 */
	public String getBatteryvoltagelowalarm() {
		return batteryvoltagelowalarm;
	}
	/**
	 * 设置：电池电压过低报警
	 */
	public KyEquipmentUploadData setBatteryvoltagetoolowalarm(String batteryvoltagetoolowalarm) {
		this.batteryvoltagetoolowalarm = batteryvoltagetoolowalarm;
		return this;
	}
	/**
	 * 获取：电池电压过低报警
	 */
	public String getBatteryvoltagetoolowalarm() {
		return batteryvoltagetoolowalarm;
	}
	/**
	 * 设置：压力状态
	 */
	public KyEquipmentUploadData setPressurestatus(String pressurestatus) {
		this.pressurestatus = pressurestatus;
		return this;
	}
	/**
	 * 获取：压力状态
	 */
	public String getPressurestatus() {
		return pressurestatus;
	}
	/**
	 * 设置：电源电压状态
	 */
	public KyEquipmentUploadData setPowersupplyvoltage(String powersupplyvoltage) {
		this.powersupplyvoltage = powersupplyvoltage;
		return this;
	}
	/**
	 * 获取：电源电压状态
	 */
	public String getPowersupplyvoltage() {
		return powersupplyvoltage;
	}
	/**
	 * 设置：水位故障
	 */
	public KyEquipmentUploadData setWaterlevelfault(String waterlevelfault) {
		this.waterlevelfault = waterlevelfault;
		return this;
	}
	/**
	 * 获取：水位故障
	 */
	public String getWaterlevelfault() {
		return waterlevelfault;
	}
	/**
	 * 设置：水位上限报警
	 */
	public KyEquipmentUploadData setWateruplevelalarm(String wateruplevelalarm) {
		this.wateruplevelalarm = wateruplevelalarm;
		return this;
	}
	/**
	 * 获取：水位上限报警
	 */
	public String getWateruplevelalarm() {
		return wateruplevelalarm;
	}
	/**
	 * 设置：水位下限报警
	 */
	public KyEquipmentUploadData setWaterdownlevelalarm(String waterdownlevelalarm) {
		this.waterdownlevelalarm = waterdownlevelalarm;
		return this;
	}

	public Integer getDeviceStatus() {
		return deviceStatus;
	}

	public void setDeviceStatus(Integer deviceStatus) {
		this.deviceStatus = deviceStatus;
	}

	/**
	 * 获取：水位下限报警
	 */
	public String getWaterdownlevelalarm() {
		return waterdownlevelalarm;
	}
	/**
	 * 设置：水位状态
	 */
	public KyEquipmentUploadData setWaterlevelstatus(String waterlevelstatus) {
		this.waterlevelstatus = waterlevelstatus;
		return this;
	}
	/**
	 * 获取：水位状态
	 */
	public String getWaterlevelstatus() {
		return waterlevelstatus;
	}


}
