package com.pms.partitionPoints.entity;
import java.io.Serializable;

import com.baomidou.mybatisplus.enums.IdType;
import com.pms.util.Description;
import com.pms.entity.BaseModel;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;


/**
 * 
 * 
 * @author lk
 * @email 307169223@qq.com
 * @date 2018-07-19 15:41:26
 */
@TableName( "ky_group_organization")
public class KyGroupOrganization extends BaseModel<KyGroupOrganization> {
private static final long serialVersionUID = 1L;


	/**
	 *角色分区（机构）id
	 */
    @TableId(value="id",type= IdType.AUTO)
    private Long id;
	
	    /**
     *
     */
    @TableField("group_id")
    @Description("")
    private Long groupId;
	
	    /**
     *分区机构id
     */
    @TableField("organization_id")
    @Description("分区机构id")
    private Long organizationId;
	

@Override
protected Serializable pkVal() {
        return this.id;
}
	// ID赋值
	public KyGroupOrganization(){
		this.id= returnIdLong();
	}
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	/*
             * 设置：
             */
	public KyGroupOrganization setGroupId(Long groupId) {
		this.groupId = groupId;
		return this;
	}
	/**
	 * 获取：
	 */
	public Long getGroupId() {
		return groupId;
	}
	/**
	 * 设置：分区机构id
	 */
	public KyGroupOrganization setOrganizationId(Long organizationId) {
		this.organizationId = organizationId;
		return this;
	}
	/**
	 * 获取：分区机构id
	 */
	public Long getOrganizationId() {
		return organizationId;
	}


}
