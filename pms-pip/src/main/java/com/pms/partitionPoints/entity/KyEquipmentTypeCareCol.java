package com.pms.partitionPoints.entity;
import com.baomidou.mybatisplus.activerecord.Model;
import java.io.Serializable;
import java.util.Date;
import javax.persistence.*;

import com.baomidou.mybatisplus.enums.IdType;
import com.pms.util.Description;
import com.pms.entity.BaseModel;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;


/**
 * 特定设备类型的设备 所关心的上传数据列
 * 
 * @author lk
 * @email 307169223@qq.com
 * @date 2018-07-18 17:45:51
 */
@TableName( "ky_equipment_type_care_col")
public class KyEquipmentTypeCareCol extends BaseModel<KyEquipmentTypeCareCol> {
private static final long serialVersionUID = 1L;


	    /**
	 *
	 */
    @TableId(value="id",type= IdType.AUTO)
    private Integer id;
	
	    /**
     *
     */
    @TableField("equipment_type_id")
    @Description("")
    private Integer equipmentTypeId;
	
	    /**
     *关心的列名(列名来至上传数据表)
     */
    @TableField("col_name")
    @Description("关心的列名(列名来至上传数据表)")
    private String colName;
	
	    /**
     *实时数据业务是否关心
     */
    @TableField("b_realtimedata_care")
    @Description("实时数据业务是否关心")
    private Integer bRealtimedataCare;
	

@Override
protected Serializable pkVal() {
        return this.id;
}
	/**
	 * 设置：
	 */
	public KyEquipmentTypeCareCol setId(Integer id) {
		this.id = id;
		return this;
	}
	/**
	 * 获取：
	 */
	public Integer getId() {
		return id;
	}
	/**
	 * 设置：
	 */
	public KyEquipmentTypeCareCol setEquipmentTypeId(Integer equipmentTypeId) {
		this.equipmentTypeId = equipmentTypeId;
		return this;
	}
	/**
	 * 获取：
	 */
	public Integer getEquipmentTypeId() {
		return equipmentTypeId;
	}
	/**
	 * 设置：关心的列名(列名来至上传数据表)
	 */
	public KyEquipmentTypeCareCol setColName(String colName) {
		this.colName = colName;
		return this;
	}
	/**
	 * 获取：关心的列名(列名来至上传数据表)
	 */
	public String getColName() {
		return colName;
	}
	/**
	 * 设置：实时数据业务是否关心
	 */
	public KyEquipmentTypeCareCol setBRealtimedataCare(Integer bRealtimedataCare) {
		this.bRealtimedataCare = bRealtimedataCare;
		return this;
	}
	/**
	 * 获取：实时数据业务是否关心
	 */
	public Integer getBRealtimedataCare() {
		return bRealtimedataCare;
	}


}
