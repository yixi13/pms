package com.pms.partitionPoints.entity;
import com.baomidou.mybatisplus.activerecord.Model;
import java.io.Serializable;
import java.util.Date;
import javax.persistence.*;

import com.baomidou.mybatisplus.enums.IdType;
import com.pms.util.Description;
import com.pms.entity.BaseModel;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;


/**
 * 设备信息表
 * 
 * @author lk
 * @email 307169223@qq.com
 * @date 2018-07-20 11:02:02
 */
@TableName( "ky_equipment_info")
public class KyEquipmentInfo extends BaseModel<KyEquipmentInfo> {
private static final long serialVersionUID = 1L;


	    /**
	 *主键
	 */
    @TableId(value="id",type= IdType.AUTO)
    private Integer id;
	
	    /**
     *名称
     */
    @TableField("name")
    @Description("名称")
    private String name;
	
	    /**
     *对应的机构集团id
     */
    @TableField("organizationGroupId")
    @Description("对应的机构集团id")
    private Integer organizationgroupid;
	
	    /**
     *机构id
     */
    @TableField("organizationId")
    @Description("机构id")
    private Integer organizationid;
	
	    /**
     *设备类型Id
     */
    @TableField("equipmentTypeId")
    @Description("设备类型Id")
    private Integer equipmenttypeid;
	
	    /**
     *数据采集方式
     */
    @TableField("dataAcquisitionMode")
    @Description("数据采集方式")
    private Integer dataacquisitionmode;
	
	    /**
     *状态(1：正常；2：禁用；3：删除)
     */
    @TableField("status")
    @Description("状态(1：正常；2：禁用；3：删除)")
    private Integer status;
	
	    /**
     *设备地址
     */
    @TableField("equipmentAddress")
    @Description("设备地址")
    private String equipmentaddress;
	
	    /**
     *设备标识
     */
    @TableField("equipmentIdentification")
    @Description("设备标识")
    private String equipmentidentification;
	
	    /**
     *安装时间
     */
    @TableField("installationTime")
    @Description("安装时间")
    private Date installationtime;
	
	    /**
     *备注
     */
    @TableField("description")
    @Description("备注")
    private String description;
	
	    /**
     *纬度
     */
    @TableField("longitude")
    @Description("纬度")
    private String longitude;
	
	    /**
     *经度
     */
    @TableField("latitude")
    @Description("经度")
    private String latitude;
	
	    /**
     *电话
     */
    @TableField("phoneNo")
    @Description("电话")
    private String phoneno;
	
	    /**
     *创建人id
     */
    @TableField("creatorId")
    @Description("创建人id")
    private Integer creatorid;
	
	    /**
     *创建时间
     */
    @TableField("createTime")
    @Description("创建时间")
    private Date createtime;
	
	    /**
     *负责人id
     */
    @TableField("managerName")
    @Description("负责人id")
    private String managername;
	
	    /**
     *通讯状态（1：在线，2：离线）
     */
    @TableField("communicationStatus")
    @Description("通讯状态（1：在线，2：离线）")
    private Integer communicationstatus;
	
	    /**
     *设备状态（1：在线，2：离线）
     */
    @TableField("equipmentStatus")
    @Description("设备状态（1：在线，2：离线）")
    private Integer equipmentstatus;
	
	    /**
     *流动方向(1表示流入 0 表示流出)
     */
    @TableField("direction")
    @Description("流动方向(1表示流入 0 表示流出)")
    private Integer direction;
	
	    /**
     *串口通讯状态（0：正常，1：警告）
     */
    @TableField("serialCommunicationState")
    @Description("串口通讯状态（0：正常，1：警告）")
    private Integer serialcommunicationstate;
	
	    /**
     *箱门开关（0：正常，1：警告）
     */
    @TableField("doorSwitch")
    @Description("箱门开关（0：正常，1：警告）")
    private Integer doorswitch;
	
	    /**
     *压力故障（0：正常，1：警告）
     */
    @TableField("pressureFault")
    @Description("压力故障（0：正常，1：警告）")
    private Integer pressurefault;
	
	    /**
     *电池电压报警（0：正常，1：警告）
     */
    @TableField("batteryVoltageAlarm")
    @Description("电池电压报警（0：正常，1：警告）")
    private Integer batteryvoltagealarm;
	
	    /**
     *水位故障（0：正常，1：警告）
     */
    @TableField("waterLevelFault")
    @Description("水位故障（0：正常，1：警告）")
    private Integer waterlevelfault;
	
	    /**
     *分区ID-新增字段。集合字符串以,分开
     */
    @TableField("dmaId")
    @Description("分区ID-新增字段。集合字符串以,分开")
    private String dmaid;

	/**
	 *测点卡电话号码
	 */
	@TableField("cardPhone")
	@Description("测点卡电话号码")
	private String cardphone;

	/**
	 *轮询间隔:秒数
	 */
	@TableField("pollInterval")
	@Description("轮询间隔:秒数")
	private Integer pollinterval;
	

@Override
protected Serializable pkVal() {
        return this.id;
}
	/**
	 * 设置：主键
	 */
	public KyEquipmentInfo setId(Integer id) {
		this.id = id;
		return this;
	}
	/**
	 * 获取：主键
	 */
	public Integer getId() {
		return id;
	}
	/**
	 * 设置：名称
	 */
	public KyEquipmentInfo setName(String name) {
		this.name = name;
		return this;
	}
	/**
	 * 获取：名称
	 */
	public String getName() {
		return name;
	}
	/**
	 * 设置：对应的机构集团id
	 */
	public KyEquipmentInfo setOrganizationgroupid(Integer organizationgroupid) {
		this.organizationgroupid = organizationgroupid;
		return this;
	}
	/**
	 * 获取：对应的机构集团id
	 */
	public Integer getOrganizationgroupid() {
		return organizationgroupid;
	}
	/**
	 * 设置：机构id
	 */
	public KyEquipmentInfo setOrganizationid(Integer organizationid) {
		this.organizationid = organizationid;
		return this;
	}
	/**
	 * 获取：机构id
	 */
	public Integer getOrganizationid() {
		return organizationid;
	}
	/**
	 * 设置：设备类型Id
	 */
	public KyEquipmentInfo setEquipmenttypeid(Integer equipmenttypeid) {
		this.equipmenttypeid = equipmenttypeid;
		return this;
	}
	/**
	 * 获取：设备类型Id
	 */
	public Integer getEquipmenttypeid() {
		return equipmenttypeid;
	}
	/**
	 * 设置：数据采集方式
	 */
	public KyEquipmentInfo setDataacquisitionmode(Integer dataacquisitionmode) {
		this.dataacquisitionmode = dataacquisitionmode;
		return this;
	}
	/**
	 * 获取：数据采集方式
	 */
	public Integer getDataacquisitionmode() {
		return dataacquisitionmode;
	}
	/**
	 * 设置：状态(1：正常；2：禁用；3：删除)
	 */
	public KyEquipmentInfo setStatus(Integer status) {
		this.status = status;
		return this;
	}
	/**
	 * 获取：状态(1：正常；2：禁用；3：删除)
	 */
	public Integer getStatus() {
		return status;
	}
	/**
	 * 设置：设备地址
	 */
	public KyEquipmentInfo setEquipmentaddress(String equipmentaddress) {
		this.equipmentaddress = equipmentaddress;
		return this;
	}
	/**
	 * 获取：设备地址
	 */
	public String getEquipmentaddress() {
		return equipmentaddress;
	}
	/**
	 * 设置：设备标识
	 */
	public KyEquipmentInfo setEquipmentidentification(String equipmentidentification) {
		this.equipmentidentification = equipmentidentification;
		return this;
	}
	/**
	 * 获取：设备标识
	 */
	public String getEquipmentidentification() {
		return equipmentidentification;
	}
	/**
	 * 设置：安装时间
	 */
	public KyEquipmentInfo setInstallationtime(Date installationtime) {
		this.installationtime = installationtime;
		return this;
	}
	/**
	 * 获取：安装时间
	 */
	public Date getInstallationtime() {
		return installationtime;
	}
	/**
	 * 设置：备注
	 */
	public KyEquipmentInfo setDescription(String description) {
		this.description = description;
		return this;
	}
	/**
	 * 获取：备注
	 */
	public String getDescription() {
		return description;
	}
	/**
	 * 设置：纬度
	 */
	public KyEquipmentInfo setLongitude(String longitude) {
		this.longitude = longitude;
		return this;
	}
	/**
	 * 获取：纬度
	 */
	public String getLongitude() {
		return longitude;
	}
	/**
	 * 设置：经度
	 */
	public KyEquipmentInfo setLatitude(String latitude) {
		this.latitude = latitude;
		return this;
	}
	/**
	 * 获取：经度
	 */
	public String getLatitude() {
		return latitude;
	}
	/**
	 * 设置：电话
	 */
	public KyEquipmentInfo setPhoneno(String phoneno) {
		this.phoneno = phoneno;
		return this;
	}
	/**
	 * 获取：电话
	 */
	public String getPhoneno() {
		return phoneno;
	}
	/**
	 * 设置：创建人id
	 */
	public KyEquipmentInfo setCreatorid(Integer creatorid) {
		this.creatorid = creatorid;
		return this;
	}
	/**
	 * 获取：创建人id
	 */
	public Integer getCreatorid() {
		return creatorid;
	}
	/**
	 * 设置：创建时间
	 */
	public KyEquipmentInfo setCreatetime(Date createtime) {
		this.createtime = createtime;
		return this;
	}
	/**
	 * 获取：创建时间
	 */
	public Date getCreatetime() {
		return createtime;
	}
	/**
	 * 设置：负责人id
	 */
	public KyEquipmentInfo setManagername(String managername) {
		this.managername = managername;
		return this;
	}
	/**
	 * 获取：负责人id
	 */
	public String getManagername() {
		return managername;
	}
	/**
	 * 设置：通讯状态（1：在线，2：离线）
	 */
	public KyEquipmentInfo setCommunicationstatus(Integer communicationstatus) {
		this.communicationstatus = communicationstatus;
		return this;
	}
	/**
	 * 获取：通讯状态（1：在线，2：离线）
	 */
	public Integer getCommunicationstatus() {
		return communicationstatus;
	}
	/**
	 * 设置：设备状态（1：在线，2：离线）
	 */
	public KyEquipmentInfo setEquipmentstatus(Integer equipmentstatus) {
		this.equipmentstatus = equipmentstatus;
		return this;
	}
	/**
	 * 获取：设备状态（1：在线，2：离线）
	 */
	public Integer getEquipmentstatus() {
		return equipmentstatus;
	}
	/**
	 * 设置：流动方向(1表示流入 0 表示流出)
	 */
	public KyEquipmentInfo setDirection(Integer direction) {
		this.direction = direction;
		return this;
	}
	/**
	 * 获取：流动方向(1表示流入 0 表示流出)
	 */
	public Integer getDirection() {
		return direction;
	}
	/**
	 * 设置：串口通讯状态（0：正常，1：警告）
	 */
	public KyEquipmentInfo setSerialcommunicationstate(Integer serialcommunicationstate) {
		this.serialcommunicationstate = serialcommunicationstate;
		return this;
	}
	/**
	 * 获取：串口通讯状态（0：正常，1：警告）
	 */
	public Integer getSerialcommunicationstate() {
		return serialcommunicationstate;
	}
	/**
	 * 设置：箱门开关（0：正常，1：警告）
	 */
	public KyEquipmentInfo setDoorswitch(Integer doorswitch) {
		this.doorswitch = doorswitch;
		return this;
	}
	/**
	 * 获取：箱门开关（0：正常，1：警告）
	 */
	public Integer getDoorswitch() {
		return doorswitch;
	}
	/**
	 * 设置：压力故障（0：正常，1：警告）
	 */
	public KyEquipmentInfo setPressurefault(Integer pressurefault) {
		this.pressurefault = pressurefault;
		return this;
	}
	/**
	 * 获取：压力故障（0：正常，1：警告）
	 */
	public Integer getPressurefault() {
		return pressurefault;
	}
	/**
	 * 设置：电池电压报警（0：正常，1：警告）
	 */
	public KyEquipmentInfo setBatteryvoltagealarm(Integer batteryvoltagealarm) {
		this.batteryvoltagealarm = batteryvoltagealarm;
		return this;
	}
	/**
	 * 获取：电池电压报警（0：正常，1：警告）
	 */
	public Integer getBatteryvoltagealarm() {
		return batteryvoltagealarm;
	}
	/**
	 * 设置：水位故障（0：正常，1：警告）
	 */
	public KyEquipmentInfo setWaterlevelfault(Integer waterlevelfault) {
		this.waterlevelfault = waterlevelfault;
		return this;
	}
	/**
	 * 获取：水位故障（0：正常，1：警告）
	 */
	public Integer getWaterlevelfault() {
		return waterlevelfault;
	}
	/**
	 * 设置：分区ID-新增字段。集合字符串以,分开
	 */
	public KyEquipmentInfo setDmaid(String dmaid) {
		this.dmaid = dmaid;
		return this;
	}
	/**
	 * 获取：分区ID-新增字段。集合字符串以,分开
	 */
	public String getDmaid() {
		return dmaid;
	}

	public String getCardphone() {
		return cardphone;
	}

	public void setCardphone(String cardphone) {
		this.cardphone = cardphone;
	}

	public Integer getPollinterval() {
		return pollinterval;
	}

	public void setPollinterval(Integer pollinterval) {
		this.pollinterval = pollinterval;
	}
}
