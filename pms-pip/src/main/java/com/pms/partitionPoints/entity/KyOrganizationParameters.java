package com.pms.partitionPoints.entity;
import com.baomidou.mybatisplus.activerecord.Model;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import javax.persistence.*;

import com.baomidou.mybatisplus.enums.IdType;
import com.pms.util.Description;
import com.pms.entity.BaseModel;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;


/**
 * 
 * 
 * @author lk
 * @email 307169223@qq.com
 * @date 2018-07-27 15:31:33
 */
@TableName( "ky_organization_parameters")
public class KyOrganizationParameters extends BaseModel<KyOrganizationParameters> {
private static final long serialVersionUID = 1L;


	    /**
	 *
	 */
    @TableId(value="id",type= IdType.AUTO)
    private Integer id;
	
	    /**
     *分区id
     */
    @TableField("organization_id")
    @Description("分区id")
    private Integer organizationId;
	
	    /**
     *供水管长
     */
    @TableField("pipe_length")
    @Description("供水管长")
    private BigDecimal pipeLength;
	
	    /**
     *免费用水量(单位：m³）
     */
    @TableField("free_water_consumption")
    @Description("免费用水量(单位：m³）")
    private BigDecimal freeWaterConsumption;
	
	    /**
     *未计量用水量估计值(单位：m³)
     */
    @TableField("no_water_consumption")
    @Description("未计量用水量估计值(单位：m³)")
    private BigDecimal noWaterConsumption;
	
	    /**
     *计费用水量
     */
    @TableField("fee_water_consumption")
    @Description("计费用水量")
    private BigDecimal feeWaterConsumption;
	
	    /**
     *背景漏失（单位m³）
     */
    @TableField("background_leakage")
    @Description("背景漏失（单位m³）")
    private BigDecimal backgroundLeakage;
	
	    /**
     *计量误差（单位m³）
     */
    @TableField(" measure_errer")
    @Description("计量误差（单位m³）")
    private BigDecimal  measureErrer;
	
	    /**
     *其他损失水量
     */
    @TableField("other_loss_water")
    @Description("其他损失水量")
    private BigDecimal otherLossWater;
	
	    /**
     *
     */
    @TableField("linkman")
    @Description("")
    private String linkman;
	
	    /**
     *
     */
    @TableField("phone")
    @Description("")
    private String phone;
	
	    /**
     *对应的年份
     */
    @TableField("year")
    @Description("对应的年份")
    private Integer year;
	
	    /**
     *对应的月份
     */
    @TableField("month")
    @Description("对应的月份")
    private Integer month;

	/**
	 *备注
	 */
	@TableField("remark")
	@Description("备注")
	private String remark;

@Override
protected Serializable pkVal() {
        return this.id;
}
	/**
	 * 设置：
	 */
	public KyOrganizationParameters setId(Integer id) {
		this.id = id;
		return this;
	}
	/**
	 * 获取：
	 */
	public Integer getId() {
		return id;
	}
	/**
	 * 设置：分区id
	 */
	public KyOrganizationParameters setOrganizationId(Integer organizationId) {
		this.organizationId = organizationId;
		return this;
	}
	/**
	 * 获取：分区id
	 */
	public Integer getOrganizationId() {
		return organizationId;
	}
	/**
	 * 设置：供水管长
	 */
	public KyOrganizationParameters setPipeLength(BigDecimal pipeLength) {
		this.pipeLength = pipeLength;
		return this;
	}
	/**
	 * 获取：供水管长
	 */
	public BigDecimal getPipeLength() {
		return pipeLength;
	}
	/**
	 * 设置：免费用水量(单位：m³）
	 */
	public KyOrganizationParameters setFreeWaterConsumption(BigDecimal freeWaterConsumption) {
		this.freeWaterConsumption = freeWaterConsumption;
		return this;
	}
	/**
	 * 获取：免费用水量(单位：m³）
	 */
	public BigDecimal getFreeWaterConsumption() {
		return freeWaterConsumption;
	}
	/**
	 * 设置：未计量用水量估计值(单位：m³)
	 */
	public KyOrganizationParameters setNoWaterConsumption(BigDecimal noWaterConsumption) {
		this.noWaterConsumption = noWaterConsumption;
		return this;
	}
	/**
	 * 获取：未计量用水量估计值(单位：m³)
	 */
	public BigDecimal getNoWaterConsumption() {
		return noWaterConsumption;
	}
	/**
	 * 设置：计费用水量
	 */
	public KyOrganizationParameters setFeeWaterConsumption(BigDecimal feeWaterConsumption) {
		this.feeWaterConsumption = feeWaterConsumption;
		return this;
	}
	/**
	 * 获取：计费用水量
	 */
	public BigDecimal getFeeWaterConsumption() {
		return feeWaterConsumption;
	}
	/**
	 * 设置：背景漏失（单位m³）
	 */
	public KyOrganizationParameters setBackgroundLeakage(BigDecimal backgroundLeakage) {
		this.backgroundLeakage = backgroundLeakage;
		return this;
	}
	/**
	 * 获取：背景漏失（单位m³）
	 */
	public BigDecimal getBackgroundLeakage() {
		return backgroundLeakage;
	}
	/**
	 * 设置：计量误差（单位m³）
	 */
	public KyOrganizationParameters setMeasureErrer(BigDecimal  measureErrer) {
		this. measureErrer =  measureErrer;
		return this;
	}
	/**
	 * 获取：计量误差（单位m³）
	 */
	public BigDecimal getMeasureErrer() {
		return  measureErrer;
	}
	/**
	 * 设置：其他损失水量
	 */
	public KyOrganizationParameters setOtherLossWater(BigDecimal otherLossWater) {
		this.otherLossWater = otherLossWater;
		return this;
	}
	/**
	 * 获取：其他损失水量
	 */
	public BigDecimal getOtherLossWater() {
		return otherLossWater;
	}
	/**
	 * 设置：
	 */
	public KyOrganizationParameters setLinkman(String linkman) {
		this.linkman = linkman;
		return this;
	}
	/**
	 * 获取：
	 */
	public String getLinkman() {
		return linkman;
	}
	/**
	 * 设置：
	 */
	public KyOrganizationParameters setPhone(String phone) {
		this.phone = phone;
		return this;
	}
	/**
	 * 获取：
	 */
	public String getPhone() {
		return phone;
	}
	/**
	 * 设置：对应的年份
	 */
	public KyOrganizationParameters setYear(Integer year) {
		this.year = year;
		return this;
	}
	/**
	 * 获取：对应的年份
	 */
	public Integer getYear() {
		return year;
	}
	/**
	 * 设置：对应的月份
	 */
	public KyOrganizationParameters setMonth(Integer month) {
		this.month = month;
		return this;
	}
	/**
	 * 获取：对应的月份
	 */
	public Integer getMonth() {
		return month;
	}

	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}
}
