package com.pms.partitionPoints.bean;

/**
 * Created by lk on 2018/8/1 0001.
 */
public class EquipmentDataBean {
    private Integer id;//测点id
    private String name;//测点名称
    private Double netCumulativeFlowStart;//净累计流量初始值
    private Double netCumulativeFlowEnd;//净累计流量终止值
    private Double netCumulativeFlowAdd;//净累计流量增量
    private Double positiveCumulativeFlowStart;//正累计流量初始值
    private Double positiveCumulativeFlowEnd;//正累计流量终止值
    private Double positiveCumulativeFlowAdd;//正累计流量增量
    private Double negativeAccumulatedFlowStart;//负累计流量初始值
    private Double negativeAccumulatedFlowEnd;//负累计流量终止值
    private Double negativeAccumulatedFlowAdd;//负累计流量终增量
    private Double instantaneousFlowMax;//瞬时流量最大值
    private Double instantaneousFlowMin;//瞬时流量最小值
    private Double instantaneousFlowAvg;//瞬时流量平均值
    private Double supplyVoltageMax;//电压最大值
    private Double supplyVoltageMin;//电压最小值
    private Double supplyVoltageAvg;//电压最平均值
    private Double pressureMax;//压力最大值
    private Double pressureMin;//压力最小值
    private Double pressureAvg;//压力平均值
    private Double waterLevelMax;//水位最大值
    private Double waterLevelMin;//水位最小值
    private Double waterLevelAvg;//水位平均值
    private Double gprsSignalIntensityMax;//GPRS信号最大值
    private Double gprsSignalIntensityMin;//GPRS信号最小值
    private Double gprsSignalIntensityAvg;//GPRS信号平均值

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Double getNetCumulativeFlowStart() {
        return netCumulativeFlowStart;
    }

    public void setNetCumulativeFlowStart(Double netCumulativeFlowStart) {
        this.netCumulativeFlowStart = netCumulativeFlowStart;
    }

    public Double getNetCumulativeFlowEnd() {
        return netCumulativeFlowEnd;
    }

    public void setNetCumulativeFlowEnd(Double netCumulativeFlowEnd) {
        this.netCumulativeFlowEnd = netCumulativeFlowEnd;
    }

    public Double getNetCumulativeFlowAdd() {
        return netCumulativeFlowAdd;
    }

    public void setNetCumulativeFlowAdd(Double netCumulativeFlowAdd) {
        this.netCumulativeFlowAdd = netCumulativeFlowAdd;
    }

    public Double getPositiveCumulativeFlowStart() {
        return positiveCumulativeFlowStart;
    }

    public void setPositiveCumulativeFlowStart(Double positiveCumulativeFlowStart) {
        this.positiveCumulativeFlowStart = positiveCumulativeFlowStart;
    }

    public Double getPositiveCumulativeFlowEnd() {
        return positiveCumulativeFlowEnd;
    }

    public void setPositiveCumulativeFlowEnd(Double positiveCumulativeFlowEnd) {
        this.positiveCumulativeFlowEnd = positiveCumulativeFlowEnd;
    }

    public Double getPositiveCumulativeFlowAdd() {
        return positiveCumulativeFlowAdd;
    }

    public void setPositiveCumulativeFlowAdd(Double positiveCumulativeFlowAdd) {
        this.positiveCumulativeFlowAdd = positiveCumulativeFlowAdd;
    }

    public Double getNegativeAccumulatedFlowStart() {
        return negativeAccumulatedFlowStart;
    }

    public void setNegativeAccumulatedFlowStart(Double negativeAccumulatedFlowStart) {
        this.negativeAccumulatedFlowStart = negativeAccumulatedFlowStart;
    }

    public Double getNegativeAccumulatedFlowEnd() {
        return negativeAccumulatedFlowEnd;
    }

    public void setNegativeAccumulatedFlowEnd(Double negativeAccumulatedFlowEnd) {
        this.negativeAccumulatedFlowEnd = negativeAccumulatedFlowEnd;
    }

    public Double getNegativeAccumulatedFlowAdd() {
        return negativeAccumulatedFlowAdd;
    }

    public void setNegativeAccumulatedFlowAdd(Double negativeAccumulatedFlowAdd) {
        this.negativeAccumulatedFlowAdd = negativeAccumulatedFlowAdd;
    }

    public Double getInstantaneousFlowMax() {
        return instantaneousFlowMax;
    }

    public void setInstantaneousFlowMax(Double instantaneousFlowMax) {
        this.instantaneousFlowMax = instantaneousFlowMax;
    }

    public Double getInstantaneousFlowMin() {
        return instantaneousFlowMin;
    }

    public void setInstantaneousFlowMin(Double instantaneousFlowMin) {
        this.instantaneousFlowMin = instantaneousFlowMin;
    }

    public Double getInstantaneousFlowAvg() {
        return instantaneousFlowAvg;
    }

    public void setInstantaneousFlowAvg(Double instantaneousFlowAvg) {
        this.instantaneousFlowAvg = instantaneousFlowAvg;
    }

    public Double getSupplyVoltageMax() {
        return supplyVoltageMax;
    }

    public void setSupplyVoltageMax(Double supplyVoltageMax) {
        this.supplyVoltageMax = supplyVoltageMax;
    }

    public Double getSupplyVoltageMin() {
        return supplyVoltageMin;
    }

    public void setSupplyVoltageMin(Double supplyVoltageMin) {
        this.supplyVoltageMin = supplyVoltageMin;
    }

    public Double getSupplyVoltageAvg() {
        return supplyVoltageAvg;
    }

    public void setSupplyVoltageAvg(Double supplyVoltageAvg) {
        this.supplyVoltageAvg = supplyVoltageAvg;
    }

    public Double getPressureMax() {
        return pressureMax;
    }

    public void setPressureMax(Double pressureMax) {
        this.pressureMax = pressureMax;
    }

    public Double getPressureMin() {
        return pressureMin;
    }

    public void setPressureMin(Double pressureMin) {
        this.pressureMin = pressureMin;
    }

    public Double getPressureAvg() {
        return pressureAvg;
    }

    public void setPressureAvg(Double pressureAvg) {
        this.pressureAvg = pressureAvg;
    }

    public Double getWaterLevelMax() {
        return waterLevelMax;
    }

    public void setWaterLevelMax(Double waterLevelMax) {
        this.waterLevelMax = waterLevelMax;
    }

    public Double getWaterLevelMin() {
        return waterLevelMin;
    }

    public void setWaterLevelMin(Double waterLevelMin) {
        this.waterLevelMin = waterLevelMin;
    }

    public Double getWaterLevelAvg() {
        return waterLevelAvg;
    }

    public void setWaterLevelAvg(Double waterLevelAvg) {
        this.waterLevelAvg = waterLevelAvg;
    }

    public Double getGprsSignalIntensityMax() {
        return gprsSignalIntensityMax;
    }

    public void setGprsSignalIntensityMax(Double gprsSignalIntensityMax) {
        this.gprsSignalIntensityMax = gprsSignalIntensityMax;
    }

    public Double getGprsSignalIntensityMin() {
        return gprsSignalIntensityMin;
    }

    public void setGprsSignalIntensityMin(Double gprsSignalIntensityMin) {
        this.gprsSignalIntensityMin = gprsSignalIntensityMin;
    }

    public Double getGprsSignalIntensityAvg() {
        return gprsSignalIntensityAvg;
    }

    public void setGprsSignalIntensityAvg(Double gprsSignalIntensityAvg) {
        this.gprsSignalIntensityAvg = gprsSignalIntensityAvg;
    }
}
