package com.pms.partitionPoints.bean;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Administrator on 2018/7/20 0020.
 */
public class OrgBean {
    private Integer id;
    private String name;
    private Integer organizationGradeId;
    private  String code;
    private  Integer parentId;
    private  boolean disabled;

    private Integer ascriptionId;

    private List<OrgBean> children;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getOrganizationGradeId() {
        return organizationGradeId;
    }

    public void setOrganizationGradeId(Integer organizationGradeId) {
        this.organizationGradeId = organizationGradeId;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public Integer getParentId() {
        return parentId;
    }

    public void setParentId(Integer parentId) {
        this.parentId = parentId;
    }

    public Integer getAscriptionId() {
        return ascriptionId;
    }

    public void setAscriptionId(Integer ascriptionId) {
        this.ascriptionId = ascriptionId;
    }

    public List<OrgBean> getChildren() {
        return children;
    }

    public void setChildren(List<OrgBean> children) {
        this.children = children;
    }

    public boolean isDisabled() {
        return disabled;
    }

    public void setDisabled(boolean disabled) {
        this.disabled = disabled;
    }

    public OrgBean() {
        this.children = new ArrayList<OrgBean>();
        this.disabled = false;
    }
}
