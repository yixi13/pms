package com.pms.partitionPoints.bean;
import com.pms.partitionPoints.entity.KyEquipmentUploadData;

import java.util.List;

/**
 * Created by Administrator on 2018/7/20 0020.
 */
public class EquipmentBean {
    private Integer id;
    private String name;
    private List<UploadBean> data;

    public EquipmentBean() {
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<UploadBean> getData() {
        return data;
    }

    public void setData(List<UploadBean> data) {
        this.data = data;
    }
}
