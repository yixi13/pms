package com.pms.partitionPoints.bean;
import com.pms.partitionPoints.entity.KyEquipmentUploadData;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

/**
 * Created by Administrator on 2018/7/20 0020.
 */
public class UploadBean {
    private BigDecimal instantaneousflow;
    private BigDecimal supplyvoltage;
    private BigDecimal gprssignalintensity;
    private BigDecimal pressure;
    private BigDecimal waterlevel;
    private Date creattime;

    public BigDecimal getInstantaneousflow() {
        return instantaneousflow;
    }

    public void setInstantaneousflow(BigDecimal instantaneousflow) {
        this.instantaneousflow = instantaneousflow;
    }

    public BigDecimal getSupplyvoltage() {
        return supplyvoltage;
    }

    public void setSupplyvoltage(BigDecimal supplyvoltage) {
        this.supplyvoltage = supplyvoltage;
    }

    public BigDecimal getGprssignalintensity() {
        return gprssignalintensity;
    }

    public void setGprssignalintensity(BigDecimal gprssignalintensity) {
        this.gprssignalintensity = gprssignalintensity;
    }

    public BigDecimal getPressure() {
        return pressure;
    }

    public void setPressure(BigDecimal pressure) {
        this.pressure = pressure;
    }

    public BigDecimal getWaterlevel() {
        return waterlevel;
    }

    public void setWaterlevel(BigDecimal waterlevel) {
        this.waterlevel = waterlevel;
    }

    public Date getCreattime() {
        return creattime;
    }

    public void setCreattime(Date creattime) {
        this.creattime = creattime;
    }
}
