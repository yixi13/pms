package com.pms.repair.service.impl;

import com.pms.repair.entity.KyRechargeHistoricalRecords;
import com.pms.repair.mapper.KyRechargeHistoricalRecordsMapper;
import com.pms.repair.service.IKyRechargeHistoricalRecordsService;
import com.pms.service.impl.BaseServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.annotation.Version;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 
 *
 * @author ljb
 * @email 517770986@qq.com
 * @date 2018-08-06 14:37:25
 */
@Service
public class KyRechargeHistoricalRecordsServiceImpl extends BaseServiceImpl<KyRechargeHistoricalRecordsMapper,KyRechargeHistoricalRecords> implements IKyRechargeHistoricalRecordsService {
   @Autowired
   public  KyRechargeHistoricalRecordsMapper kyRechargeHistoricalRecordsMapper;

   @Override
   public List<KyRechargeHistoricalRecords> selectListByData(Long equipmentInfoId){
       List<KyRechargeHistoricalRecords> list= kyRechargeHistoricalRecordsMapper.getListByData(equipmentInfoId);
       return list;
    }
}