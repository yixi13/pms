package com.pms.repair.service.impl;

import com.baomidou.mybatisplus.mapper.SqlHelper;
import com.baomidou.mybatisplus.mapper.Wrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.pms.repair.entity.KyCardRecharge;
import com.pms.service.impl.BaseServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.pms.repair.mapper.KyCardRechargeMapper;
import com.pms.repair.service.IKyCardRechargeService;
/**
 * 
 *
 * @author ljb
 * @email 517770986@qq.com
 * @date 2018-07-18 16:40:45
 */
@Service
public class KyCardRechargeServiceImpl extends BaseServiceImpl<KyCardRechargeMapper,KyCardRecharge> implements IKyCardRechargeService {

    @Autowired
    KyCardRechargeMapper cardRechargeMapper;

   public  KyCardRecharge findById(Long cardId){
        return cardRechargeMapper.findById(cardId);
    }

    public Page<KyCardRecharge> selectByKyCardRechargePage(Page<KyCardRecharge> page, Wrapper<KyCardRecharge> wrapper){
        SqlHelper.fillWrapper(page, wrapper);
        page.setRecords(cardRechargeMapper.selectByKyCardRechargePage(page, wrapper));
        return page;
    }

}