package com.pms.repair.service.impl;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.mapper.SqlHelper;
import com.baomidou.mybatisplus.mapper.Wrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.pms.repair.entity.KyRepairMaintenance;
import com.pms.service.impl.BaseServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.pms.repair.entity.KyRepairDeclare;
import com.pms.repair.mapper.KyRepairDeclareMapper;
import com.pms.repair.service.IKyRepairDeclareService;

import java.util.List;
import java.util.Map;

/**
 * 
 *
 * @author ljb
 * @email 517770986@qq.com
 * @date 2018-07-18 16:40:45
 */
@Service
public class KyRepairDeclareServiceImpl extends BaseServiceImpl<KyRepairDeclareMapper,KyRepairDeclare> implements IKyRepairDeclareService {
    @Autowired
    KyRepairDeclareMapper repairDeclareMapper;

    public Page<Map<String, Object>> getByPipRepairDeclarePage(Page<Map<String, Object>>  page,Map<String,Object> map){
        Integer count=  repairDeclareMapper.countApiPipRepairDeclarePage(map);
        List<Map<String,Object>> list= repairDeclareMapper.getByPipRepairDeclarePage(map);
        page.setRecords(list);
        page.setSize(Integer.parseInt(map.get("limit").toString()));
        page.setTotal(count);
        return page;
    }

    public Map<String,Object> getByPipRepairDeclare(Long repairMaintenanceId){
        return  repairDeclareMapper.getByPipRepairDeclare(repairMaintenanceId);
    }

    public Page<KyRepairDeclare> selectByPageRepairDeclare(Page<KyRepairDeclare> page, Map<String, Object> map){
        Integer list= repairDeclareMapper.countRepairDeclare(map);
        List<KyRepairDeclare> pageList =  repairDeclareMapper.selectByPageRepairDeclare(map);
        page.setRecords(pageList);
        page.setTotal(list);
        return  page;
    }

    public List<KyRepairDeclare> selectByListRepairDeclare(String companyCode){
        return   repairDeclareMapper.selectByListRepairDeclare(companyCode);
    }

    public int selectBycountRepairnum(int type,String code){
        return   repairDeclareMapper.selectBycountRepairnum(type,code);
    }

}