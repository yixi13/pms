package com.pms.repair.service.impl;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.pms.service.impl.BaseServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.pms.repair.entity.KyRepairMaintenance;
import com.pms.repair.mapper.KyRepairMaintenanceMapper;
import com.pms.repair.service.IKyRepairMaintenanceService;

import java.util.List;
import java.util.Map;

/**
 * 
 *
 * @author ljb
 * @email 517770986@qq.com
 * @date 2018-07-18 16:40:45
 */
@Service
public class KyRepairMaintenanceServiceImpl extends BaseServiceImpl<KyRepairMaintenanceMapper,KyRepairMaintenance> implements IKyRepairMaintenanceService {
    @Autowired
    KyRepairMaintenanceMapper kyRepairMaintenanceMapper;

    public  KyRepairMaintenance seteleByMaintenance(Long repairMaintenanceId){
        return  kyRepairMaintenanceMapper.seteleByMaintenance(repairMaintenanceId);
    }

    public Page<KyRepairMaintenance> selectByPageList(Page<KyRepairMaintenance> page, Map<String,Object> map){
        int count=kyRepairMaintenanceMapper.selectCount(new EntityWrapper<KyRepairMaintenance>().eq("repair_declare_id",map.get("repairDeclareId")));
        List<KyRepairMaintenance> list= kyRepairMaintenanceMapper.selectByPageList(map);
        page.setRecords(list);
        page.setTotal(count);
        return  page;
    }



    public Page<KyRepairMaintenance> selectByPageMap(Page<KyRepairMaintenance> page, Map<String,Object> map){
        int count=0;
        if (!map.get("ids").equals("-1")) {
            count = kyRepairMaintenanceMapper.countByPageMap(map);
        }
        List<KyRepairMaintenance> list= kyRepairMaintenanceMapper.selectByPageMap(map);
        page.setRecords(list);
        page.setTotal(count);
        return  page;
    }
}