package com.pms.repair.service;

import com.baomidou.mybatisplus.mapper.Wrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.pms.repair.entity.KyCardRecharge;
import com.pms.service.IBaseService;

/**
 * 
 *
 * @author ljb
 * @email 517770986@qq.com
 * @date 2018-07-18 16:40:45
 */

public interface IKyCardRechargeService extends IBaseService<KyCardRecharge> {
    KyCardRecharge findById(Long cardId);

    Page<KyCardRecharge> selectByKyCardRechargePage(Page<KyCardRecharge> page, Wrapper<KyCardRecharge> wrapper);
}