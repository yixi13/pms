package com.pms.repair.service;

import com.baomidou.mybatisplus.plugins.Page;
import com.pms.repair.entity.KyRepairDeclare;
import com.pms.repair.entity.KyRepairMaintenance;
import com.pms.service.IBaseService;

import java.util.Map;

/**
 * 
 *
 * @author ljb
 * @email 517770986@qq.com
 * @date 2018-07-18 16:40:45
 */

public interface IKyRepairMaintenanceService extends IBaseService<KyRepairMaintenance> {

    KyRepairMaintenance seteleByMaintenance(Long repairMaintenanceId);

    Page<KyRepairMaintenance> selectByPageList(Page<KyRepairMaintenance> page, Map<String,Object> map);

     Page<KyRepairMaintenance> selectByPageMap(Page<KyRepairMaintenance> page, Map<String,Object> map);
}