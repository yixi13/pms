package com.pms.repair.service;

import com.pms.repair.entity.KyRechargeHistoricalRecords;
import com.pms.service.IBaseService;

import java.util.List;

/**
 * 
 *
 * @author ljb
 * @email 517770986@qq.com
 * @date 2018-08-06 14:37:25
 */

public interface IKyRechargeHistoricalRecordsService extends IBaseService<KyRechargeHistoricalRecords> {

    List<KyRechargeHistoricalRecords> selectListByData(Long equipmentInfoId);
}