package com.pms.repair.service;

import com.baomidou.mybatisplus.mapper.Wrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.pms.repair.entity.KyRepairDeclare;
import com.pms.repair.entity.KyRepairMaintenance;
import com.pms.service.IBaseService;

import java.util.List;
import java.util.Map;

/**
 * 
 *
 * @author ljb
 * @email 517770986@qq.com
 * @date 2018-07-18 16:40:45
 */

public interface IKyRepairDeclareService extends IBaseService<KyRepairDeclare> {
    Page<Map<String, Object>> getByPipRepairDeclarePage(Page<Map<String, Object>> page,Map<String,Object> map);
    public Map<String,Object> getByPipRepairDeclare(Long repairMaintenanceId);
    Page<KyRepairDeclare> selectByPageRepairDeclare(Page<KyRepairDeclare> page, Map<String,Object> map);

    List<KyRepairDeclare> selectByListRepairDeclare(String companyCode);

    int  selectBycountRepairnum(int type,String code);
}