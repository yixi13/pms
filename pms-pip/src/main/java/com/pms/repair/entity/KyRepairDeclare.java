package com.pms.repair.entity;
import com.baomidou.mybatisplus.activerecord.Model;
import java.io.Serializable;
import java.util.Date;
import javax.persistence.*;
import com.pms.util.Description;
import com.pms.entity.BaseModel;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;


/**
 * 
 * 
 * @author ljb
 * @email 517770986@qq.com
 * @date 2018-07-18 16:40:45
 */
@TableName( "ky_repair_declare")
public class KyRepairDeclare extends BaseModel<KyRepairDeclare> {
private static final long serialVersionUID = 1L;


	/**
	 *设备保修申报表
	 */
    @TableId("repair_declare_id")
    private Long repairDeclareId;
	
	    /**
     *设备ID
     */
    @TableField("equipment_info_id")
    @Description("设备ID")
    private Long equipmentInfoId;
	
	    /**
     *测点名称
     */
    @TableField("equipment_info_name")
    @Description("测点名称")
    private String equipmentInfoName;
	
	    /**
     *所属分区
     */
    @TableField("belonged_dma")
    @Description("所属分区")
    private String belongedDma;
	
	    /**
     *1.位置2.设备故障3.环境因素
     */
    @TableField("declare_type")
    @Description("1.未知2.设备故障3.环境因素")
    private Integer declareType;
	
	    /**
     *申报人
     */
    @TableField("declare_person")
    @Description("申报人")
    private String declarePerson;

	/**
	 *申报人电话
	 */
	@TableField("declare_phone")
	@Description("申报人电话")
	private String declarePhone;

	/**
	 *故障描述
	 */
	@TableField("description")
	@Description("故障描述")
	private String description;

	/**
     *申报时间
     */
    @TableField("declare_data")
    @Description("申报时间")
    private Date declareData;
	
	    /**
     *受理人
     */
    @TableField("accept_person")
    @Description("受理人")
    private String acceptPerson;
	/**
	 *受理人电话
	 */
	@TableField("accept_phone")
	@Description("受理人电话")
	private String acceptPhone;

	/**
     *受理时间
     */
    @TableField("accept_data")
    @Description("受理时间")
    private Date acceptData;
	
	    /**
     *状态1待处理2拒绝受理3已受理
     */
    @TableField("state_")
    @Description("状态1待处理2拒绝受理3已受理")
    private Integer state;
	/**
	 *分区id
	 */
	@TableField("organizationId")
	@Description("分区id")
	private Long organizationId;
	/**
	 *二级dma分区对应的机构id
	 */
	@TableField("organizationCompanyId")
	@Description("二级dma分区对应的机构id")
	private Long organizationCompanyId;
	/**
	 * 一级dma分区对应的机构id
	 */
	@TableField("organizationGroupId")
	@Description("一级dma分区对应的机构id")
	private Long organizationGroupId;

	/**
     *
     */
    @TableField("crt_time")
    @Description("")
    private Date crtTime;
	
	    /**
     *
     */
    @TableField("crt_user")
    @Description("")
    private String crtUser;
	
	    /**
     *
     */
    @TableField("upd_time")
    @Description("")
    private Date updTime;
	
	    /**
     *
     */
    @TableField("upd_user")
    @Description("")
    private String updUser;
	/**
	 *角色ID
	 */
	@TableField("role_id")
	@Description("角色ID")
	private Long roleId;

	/**
	 *是否已读
	 */
	@TableField("is_read")
	@Description("是否已读(1已读2未读)")
	private Integer isRead;

	/**
	 *系统编码
	 */
	@TableField("company_code")
	@Description("系统编码")
	private String companyCode;
	/**
	 * 标记是否已经维修 1 是 2否
	 */
	@TableField("target_")
	@Description("标记是否已经维修 1 是 2否")
	private Integer target;

// ID赋值
public KyRepairDeclare(){
        this.repairDeclareId= returnIdLong();
        }
@Override
protected Serializable pkVal() {
        return this.repairDeclareId;
}
	/**
	 * 设置：设备保修申报表
	 */
	public KyRepairDeclare setRepairDeclareId(Long repairDeclareId) {
		this.repairDeclareId = repairDeclareId;
		return this;
	}
	/**
	 * 获取：设备保修申报表
	 */
	public Long getRepairDeclareId() {
		return repairDeclareId;
	}
	/**
	 * 设置：设备ID
	 */
	public KyRepairDeclare setEquipmentInfoId(Long equipmentInfoId) {
		this.equipmentInfoId = equipmentInfoId;
		return this;
	}
	/**
	 * 获取：设备ID
	 */
	public Long getEquipmentInfoId() {
		return equipmentInfoId;
	}
	/**
	 * 设置：测点名称
	 */
	public KyRepairDeclare setEquipmentInfoName(String equipmentInfoName) {
		this.equipmentInfoName = equipmentInfoName;
		return this;
	}
	/**
	 * 获取：测点名称
	 */
	public String getEquipmentInfoName() {
		return equipmentInfoName;
	}
	/**
	 * 设置：所属分区
	 */
	public KyRepairDeclare setBelongedDma(String belongedDma) {
		this.belongedDma = belongedDma;
		return this;
	}
	/**
	 * 获取：所属分区
	 */
	public String getBelongedDma() {
		return belongedDma;
	}
	/**
	 * 设置：1.位置2.设备故障3.环境因素
	 */
	public KyRepairDeclare setDeclareType(Integer declareType) {
		this.declareType = declareType;
		return this;
	}

	public String getAcceptPhone() {
		return acceptPhone;
	}

	public void setAcceptPhone(String acceptPhone) {
		this.acceptPhone = acceptPhone;
	}

	/**
	 * 获取：1.位置2.设备故障3.环境因素
	 */
	public Integer getDeclareType() {
		return declareType;
	}
	/**
	 * 设置：申报人
	 */
	public KyRepairDeclare setDeclarePerson(String declarePerson) {
		this.declarePerson = declarePerson;
		return this;
	}
	/**
	 * 获取：申报人
	 */
	public String getDeclarePerson() {
		return declarePerson;
	}
	/**
	 * 设置：申报时间
	 */
	public KyRepairDeclare setDeclareData(Date declareData) {
		this.declareData = declareData;
		return this;
	}
	/**
	 * 获取：申报时间
	 */
	public Date getDeclareData() {
		return declareData;
	}
	/**
	 * 设置：受理人
	 */
	public KyRepairDeclare setAcceptPerson(String acceptPerson) {
		this.acceptPerson = acceptPerson;
		return this;
	}
	/**
	 * 获取：受理人
	 */
	public String getAcceptPerson() {
		return acceptPerson;
	}
	/**
	 * 设置：受理时间
	 */
	public KyRepairDeclare setAcceptData(Date acceptData) {
		this.acceptData = acceptData;
		return this;
	}
	/**
	 * 获取：受理时间
	 */
	public Date getAcceptData() {
		return acceptData;
	}
	/**
	 * 设置：状态1待处理2拒绝受理3已受理
	 */
	public KyRepairDeclare setState(Integer state) {
		this.state = state;
		return this;
	}
	/**
	 * 获取：状态1待处理2拒绝受理3已受理
	 */
	public Integer getState() {
		return state;
	}
	/**
	 * 设置：
	 */
	public KyRepairDeclare setCrtTime(Date crtTime) {
		this.crtTime = crtTime;
		return this;
	}
	/**
	 * 获取：
	 */
	public Date getCrtTime() {
		return crtTime;
	}
	/**
	 * 设置：
	 */
	public KyRepairDeclare setCrtUser(String crtUser) {
		this.crtUser = crtUser;
		return this;
	}
	/**
	 * 获取：
	 */
	public String getCrtUser() {
		return crtUser;
	}
	/**
	 * 设置：
	 */
	public KyRepairDeclare setUpdTime(Date updTime) {
		this.updTime = updTime;
		return this;
	}
	/**
	 * 获取：
	 */
	public Date getUpdTime() {
		return updTime;
	}
	/**
	 * 设置：
	 */
	public KyRepairDeclare setUpdUser(String updUser) {
		this.updUser = updUser;
		return this;
	}

	public static long getSerialVersionUID() {
		return serialVersionUID;
	}


	/**
	 * 获取：
	 */
	public String getUpdUser() {
		return updUser;
	}

	public String getDeclarePhone() {
		return declarePhone;
	}

	public String getDescription() {
		return description;
	}

	public void setDeclarePhone(String declarePhone) {
		this.declarePhone = declarePhone;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Long getOrganizationId() {
		return organizationId;
	}

	public Long getOrganizationCompanyId() {
		return organizationCompanyId;
	}

	public Long getOrganizationGroupId() {
		return organizationGroupId;
	}

	public void setOrganizationId(Long organizationId) {
		this.organizationId = organizationId;
	}

	public void setOrganizationCompanyId(Long organizationCompanyId) {
		this.organizationCompanyId = organizationCompanyId;
	}

	public Long getRoleId() {
		return roleId;
	}

	public Integer getIsRead() {
		return isRead;
	}

	public void setRoleId(Long roleId) {
		this.roleId = roleId;
	}

	public void setIsRead(Integer isRead) {
		this.isRead = isRead;
	}

	public String getCompanyCode() {
		return companyCode;
	}

	public void setCompanyCode(String companyCode) {
		this.companyCode = companyCode;
	}

	public void setOrganizationGroupId(Long organizationGroupId) {
		this.organizationGroupId = organizationGroupId;
	}

	public Integer getTarget() {
		return target;
	}

	public void setTarget(Integer target) {
		this.target = target;
	}
}
