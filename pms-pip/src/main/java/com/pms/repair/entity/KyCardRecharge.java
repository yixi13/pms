package com.pms.repair.entity;
import com.baomidou.mybatisplus.activerecord.Model;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import javax.persistence.*;
import com.pms.util.Description;
import com.pms.entity.BaseModel;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;


/**
 * 
 * 
 * @author ljb
 * @email 517770986@qq.com
 * @date 2018-07-18 16:40:45
 */
@TableName( "ky_card_recharge")
public class KyCardRecharge extends BaseModel<KyCardRecharge> {
private static final long serialVersionUID = 1L;


	    /**
	 *数据库充值表
	 */
    @TableId("card_id")
    private Long cardId;
	
	    /**
     *设备ID
     */
    @TableField("equipment_info_id")
    @Description("设备ID")
    private Long equipmentInfoId;
	
	    /**
     *测点名称
     */
    @TableField("equipment_info_name")
    @Description("测点名称")
    private String equipmentInfoName;
	
	    /**
     *充值总额
     */
    @TableField("total_amount")
    @Description("充值总额")
    private BigDecimal totalAmount;
	
	    /**
     *当前余额
     */
    @TableField("current_balance")
    @Description("当前余额")
    private BigDecimal currentBalance;
	
	    /**
     *手机卡号
     */
    @TableField("phone_")
    @Description("手机卡号")
    private String phone;
	
	    /**
     *最近充值人
     */
    @TableField("recently_name")
    @Description("最近充值人")
    private String recentlyName;

	    /**
     *最近充值金额
     */
    @TableField("recently_current_balance")
    @Description("最近充值金额")
    private BigDecimal recentlyCurrentBalance;

	    /**
     *最近充值时间
     */
    @TableField("recently_data")
    @Description("最近充值时间")
    private Date recentlyData;


	/**
     *状态1.正常2.余额不足3欠费
     */
    @TableField("state_")
    @Description("状态1.正常2.余额不足3欠费")
    private Integer state
;
	
	    /**
     *每月收费标准
     */
    @TableField("monthly_fee_standard")
    @Description("每月收费标准")
    private BigDecimal monthlyFeeStandard;
	/**
	 *
	 */
	@TableField("crt_time")
	@Description("")
	private Date crtTime;

	/**
	 *
	 */
	@TableField("crt_user")
	@Description("")
	private String crtUser;

	/**
	 *
	 */
	@TableField("upd_time")
	@Description("")
	private Date updTime;

	/**
	 *
	 */
	@TableField("upd_user")
	@Description("")
	private String updUser;

	@TableField(exist = false)
	private String phoneNo;

	@TableField(exist = false)
	private String managerName;


	// ID赋值
public KyCardRecharge(){
        this.cardId= returnIdLong();
        }
@Override
protected Serializable pkVal() {
        return this.cardId;
}
	/**
	 * 设置：数据库充值表
	 */
	public KyCardRecharge setCardId(Long cardId) {
		this.cardId = cardId;
		return this;
	}
	/**
	 * 获取：数据库充值表
	 */
	public Long getCardId() {
		return cardId;
	}
	/**
	 * 设置：设备ID
	 */
	public KyCardRecharge setEquipmentInfoId(Long equipmentInfoId) {
		this.equipmentInfoId = equipmentInfoId;
		return this;
	}
	/**
	 * 获取：设备ID
	 */
	public Long getEquipmentInfoId() {
		return equipmentInfoId;
	}
	/**
	 * 设置：测点名称
	 */
	public KyCardRecharge setEquipmentInfoName(String equipmentInfoName) {
		this.equipmentInfoName = equipmentInfoName;
		return this;
	}
	/**
	 * 获取：测点名称
	 */
	public String getEquipmentInfoName() {
		return equipmentInfoName;
	}
	/**
	 * 设置：充值总额
	 */
	public KyCardRecharge setTotalAmount(BigDecimal totalAmount) {
		this.totalAmount = totalAmount;
		return this;
	}
	/**
	 * 获取：充值总额
	 */
	public BigDecimal getTotalAmount() {
		return totalAmount;
	}
	/**
	 * 设置：当前余额
	 */
	public KyCardRecharge setCurrentBalance(BigDecimal currentBalance) {
		this.currentBalance = currentBalance;
		return this;
	}
	/**
	 * 获取：当前余额
	 */
	public BigDecimal getCurrentBalance() {
		return currentBalance;
	}
	/**
	 * 设置：充值人电话
	 */
	public KyCardRecharge setPhone(String phone) {
		this.phone = phone;
		return this;
	}
	/**
	 * 获取：充值人电话
	 */
	public String getPhone() {
		return phone;
	}
	/**
	 * 设置：最近充值人
	 */
	public KyCardRecharge setRecentlyName(String recentlyName) {
		this.recentlyName = recentlyName;
		return this;
	}
	/**
	 * 获取：最近充值人
	 */
	public String getRecentlyName() {
		return recentlyName;
	}
	/**
	 * 设置：最近充值金额
	 */
	public KyCardRecharge setRecentlyCurrentBalance(BigDecimal recentlyCurrentBalance) {
		this.recentlyCurrentBalance = recentlyCurrentBalance;
		return this;
	}
	/**
	 * 获取：最近充值金额
	 */
	public BigDecimal getRecentlyCurrentBalance() {
		return recentlyCurrentBalance;
	}
	/**
	 * 设置：最近充值时间
	 */
	public KyCardRecharge setRecentlyData(Date recentlyData) {
		this.recentlyData = recentlyData;
		return this;
	}
	/**
	 * 获取：最近充值时间
	 */
	public Date getRecentlyData() {
		return recentlyData;
	}
	/**
	 * 设置：状态1.正常2.余额不足3欠费
	 */
	public KyCardRecharge setState(Integer state) {
		this.state = state;
		return this;
	}
	/**
	 * 获取：状态1.正常2.余额不足3欠费
	 */
	public Integer getState() {
		return state;
	}
	/**
	 * 设置：每月收费标准
	 */
	public KyCardRecharge setMonthlyFeeStandard(BigDecimal monthlyFeeStandard) {
		this.monthlyFeeStandard = monthlyFeeStandard;
		return this;
	}
	/**
	 * 获取：每月收费标准
	 */
	public BigDecimal getMonthlyFeeStandard() {
		return monthlyFeeStandard;
	}


	public Date getCrtTime() {
		return crtTime;
	}

	public String getCrtUser() {
		return crtUser;
	}

	public Date getUpdTime() {
		return updTime;
	}

	public String getUpdUser() {
		return updUser;
	}
	public void setCrtTime(Date crtTime) {
		this.crtTime = crtTime;
	}

	public void setCrtUser(String crtUser) {
		this.crtUser = crtUser;
	}

	public void setUpdTime(Date updTime) {
		this.updTime = updTime;
	}

	public void setUpdUser(String updUser) {
		this.updUser = updUser;
	}

	public String getPhoneNo() {
		return phoneNo;
	}

	public String getManagerName() {
		return managerName;
	}

	public void setManagerName(String managerName) {
		this.managerName = managerName;
	}

	public void setPhoneNo(String phoneNo) {
		this.phoneNo = phoneNo;
	}

}
