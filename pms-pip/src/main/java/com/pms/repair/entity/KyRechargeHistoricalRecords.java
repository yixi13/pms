package com.pms.repair.entity;
import com.baomidou.mybatisplus.activerecord.Model;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import javax.persistence.*;
import com.pms.util.Description;
import com.pms.entity.BaseModel;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;


/**
 * 
 * 
 * @author ljb
 * @email 517770986@qq.com
 * @date 2018-08-06 14:37:25
 */
@TableName( "ky_recharge_historical_records")
public class KyRechargeHistoricalRecords extends BaseModel<KyRechargeHistoricalRecords> {
private static final long serialVersionUID = 1L;


	/**
	 *
	 */
    @TableId("id")
    private Long id;
	
	/**
     *数据卡ID
     */
    @TableField("card_id")
    @Description("")
    private Long cardId;
	/**
	 * 设备ID
	 */
	@TableField("equipment_info_id")
	@Description("设备ID")
	private Long  equipmentInfoId;


	/**
     *最近充值人
     */
    @TableField("recently_name")
    @Description("")
    private String recentlyName;
	/**
	 *充值后余额
	 */
	@TableField("current_money")
	@Description("充值后余额")
	private BigDecimal currentMoney;
	/**
     *最近充值金额
     */
    @TableField("recently_current_balance")
    @Description("")
    private BigDecimal recentlyCurrentBalance;
	
	/**
     *最近充值时间
     */
    @TableField("recently_data")
    @Description("")
    private Date recentlyData;
	/**
	 *
	 */
	@TableField("crt_time")
	@Description("")
	private Date crtTime;

	/**
	 *
	 */
	@TableField("crt_user")
	@Description("")
	private String crtUser;

	/**
	 *
	 */
	@TableField("upd_time")
	@Description("")
	private Date updTime;

	/**
	 *
	 */
	@TableField("upd_user")
	@Description("")
	private String updUser;


	public BigDecimal getCurrentMoney() {
		return currentMoney;
	}

	public void setCurrentMoney(BigDecimal currentMoney) {
		this.currentMoney = currentMoney;
	}

	// ID赋值
public KyRechargeHistoricalRecords(){
        this.id= returnIdLong();
        }
@Override
protected Serializable pkVal() {
        return this.id;
}
	/**
	 * 设置：
	 */
	public KyRechargeHistoricalRecords setId(Long id) {
		this.id = id;
		return this;
	}
	/**
	 * 获取：
	 */
	public Long getId() {
		return id;
	}
	/**
	 * 设置：
	 */
	public KyRechargeHistoricalRecords setCardId(Long cardId) {
		this.cardId = cardId;
		return this;
	}
	/**
	 * 获取：
	 */
	public Long getCardId() {
		return cardId;
	}
	/**
	 * 设置：
	 */
	public KyRechargeHistoricalRecords setRecentlyName(String recentlyName) {
		this.recentlyName = recentlyName;
		return this;
	}
	/**
	 * 获取：
	 */
	public String getRecentlyName() {
		return recentlyName;
	}
	/**
	 * 设置：
	 */
	public KyRechargeHistoricalRecords setRecentlyCurrentBalance(BigDecimal recentlyCurrentBalance) {
		this.recentlyCurrentBalance = recentlyCurrentBalance;
		return this;
	}
	/**
	 * 获取：
	 */
	public BigDecimal getRecentlyCurrentBalance() {
		return recentlyCurrentBalance;
	}
	/**
	 * 设置：
	 */
	public KyRechargeHistoricalRecords setRecentlyData(Date recentlyData) {
		this.recentlyData = recentlyData;
		return this;
	}
	/**
	 * 获取：
	 */
	public Date getRecentlyData() {
		return recentlyData;
	}

	public Long getEquipmentInfoId() {
		return equipmentInfoId;
	}

	public Date getCrtTime() {
		return crtTime;
	}

	public String getCrtUser() {
		return crtUser;
	}

	public Date getUpdTime() {
		return updTime;
	}

	public String getUpdUser() {
		return updUser;
	}

	public void setCrtTime(Date crtTime) {
		this.crtTime = crtTime;
	}

	public void setCrtUser(String crtUser) {
		this.crtUser = crtUser;
	}

	public void setUpdTime(Date updTime) {
		this.updTime = updTime;
	}

	public void setUpdUser(String updUser) {
		this.updUser = updUser;
	}


	public void setEquipmentInfoId(Long equipmentInfoId) {
		this.equipmentInfoId = equipmentInfoId;
	}
}
