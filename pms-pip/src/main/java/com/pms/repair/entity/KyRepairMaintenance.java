package com.pms.repair.entity;
import com.baomidou.mybatisplus.activerecord.Model;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import javax.persistence.*;
import com.pms.util.Description;
import com.pms.entity.BaseModel;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;


/**
 * 
 * 
 * @author ljb
 * @email 517770986@qq.com
 * @date 2018-07-18 16:40:45
 */
@TableName( "ky_repair_maintenance")
public class KyRepairMaintenance extends BaseModel<KyRepairMaintenance> {
private static final long serialVersionUID = 1L;


	/**
	 *设备维修信息表
	 */
    @TableId("repair_maintenance_id")
    private Long repairMaintenanceId;
	
	    /**
     *测点名称
     */
    @TableField("equipment_info_name")
    @Description("测点名称")
    private String equipmentInfoName;
	
	    /**
     *设备申报信息ID
     */
    @TableField("repair_declare_id")
    @Description("设备申报信息ID")
    private Long repairDeclareId;
	
	    /**
     *开始时间
     */
    @TableField("start_data")
    @Description("开始时间")
    private Date startData;
	
	    /**
     *结束时间
     */
    @TableField("end_data")
    @Description("结束时间")
    private Date endData;
	
	    /**
     *维修状态1维修成功2维修失败
     */
    @TableField("state_")
    @Description("维修状态1.维修成功2维修失败3.待处理")
    private Integer state;
	
	    /**
     *维修费用
     */
    @TableField("maintenance_costs")
    @Description("维修费用")
    private BigDecimal maintenanceCosts;
	/**
	 *维修描述
	 */
	@TableField("maintenance_description")
	@Description("维修描述")
	private String maintenanceDescription;

	/**
	 *设备ID
	 */
	@TableField("equipment_info_id")
	@Description("设备ID")
	private Long equipmentInfoId;

	@TableField(exist = false)
	private String acceptPerson;

	@TableField(exist = false)
	private String acceptPhone;

	@TableField(exist = false)
	private String description;



	// ID赋值
public KyRepairMaintenance(){
        this.repairMaintenanceId= returnIdLong();
        }
@Override
protected Serializable pkVal() {
        return this.repairMaintenanceId;
}
	/**
	 * 设置：设备维修信息表
	 */
	public KyRepairMaintenance setRepairMaintenanceId(Long repairMaintenanceId) {
		this.repairMaintenanceId = repairMaintenanceId;
		return this;
	}
	/**
	 * 获取：设备维修信息表
	 */
	public Long getRepairMaintenanceId() {
		return repairMaintenanceId;
	}
	/**
	 * 设置：测点名称
	 */
	public KyRepairMaintenance setEquipmentInfoName(String equipmentInfoName) {
		this.equipmentInfoName = equipmentInfoName;
		return this;
	}
	/**
	 * 获取：测点名称
	 */
	public String getEquipmentInfoName() {
		return equipmentInfoName;
	}
	/**
	 * 设置：设备申报信息ID
	 */
	public KyRepairMaintenance setRepairDeclareId(Long repairDeclareId) {
		this.repairDeclareId = repairDeclareId;
		return this;
	}
	/**
	 * 获取：设备申报信息ID
	 */
	public Long getRepairDeclareId() {
		return repairDeclareId;
	}
	/**
	 * 设置：开始时间
	 */
	public KyRepairMaintenance setStartData(Date startData) {
		this.startData = startData;
		return this;
	}
	/**
	 * 获取：开始时间
	 */
	public Date getStartData() {
		return startData;
	}
	/**
	 * 设置：结束时间
	 */
	public KyRepairMaintenance setEndData(Date endData) {
		this.endData = endData;
		return this;
	}
	/**
	 * 获取：结束时间
	 */
	public Date getEndData() {
		return endData;
	}
	/**
	 * 设置：维修状态1.待处理2维修成功3维修失败
	 */
	public KyRepairMaintenance setState(Integer state) {
		this.state = state;
		return this;
	}
	/**
	 * 获取：维修状态1.待处理2维修成功3维修失败
	 */
	public Integer getState() {
		return state;
	}
	/**
	 * 设置：维修费用
	 */
	public KyRepairMaintenance setMaintenanceCosts(BigDecimal maintenanceCosts) {
		this.maintenanceCosts = maintenanceCosts;
		return this;
	}
	/**
	 * 获取：维修费用
	 */
	public BigDecimal getMaintenanceCosts() {
		return maintenanceCosts;
	}

	public String getMaintenanceDescription() {
		return maintenanceDescription;
	}

	public void setMaintenanceDescription(String maintenanceDescription) {
		this.maintenanceDescription = maintenanceDescription;
	}

	public String getAcceptPerson() {
		return acceptPerson;
	}

	public String getAcceptPhone() {
		return acceptPhone;
	}

	public String getDescription() {
		return description;
	}

	public void setAcceptPerson(String acceptPerson) {
		this.acceptPerson = acceptPerson;
	}

	public void setAcceptPhone(String acceptPhone) {
		this.acceptPhone = acceptPhone;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Long getEquipmentInfoId() {
		return equipmentInfoId;
	}

	public void setEquipmentInfoId(Long equipmentInfoId) {
		this.equipmentInfoId = equipmentInfoId;
	}
}
