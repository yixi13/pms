package com.pms.repair.web;
import com.pms.exception.R;
import com.pms.repair.entity.KyCardRecharge;
import com.pms.repair.service.IKyCardRechargeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Configurable;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import java.util.List;
/**
 * Created by hm on 2018/8/20.
 */
@Component
@Configurable
@EnableScheduling
public class KyCardtimerDeductionController {
    @Autowired
    IKyCardRechargeService kyCardRechargeService;
    /**
     * 定时器扣除数据卡充值费用
     * @return
     */
    //每个月1号凌晨0零点来执行
    @Scheduled(cron = "0 0 0 1 * ?")
    //每1时执行一次
//    @Scheduled(cron = "0 */1 * * *1?")
    public R cardTimerDeductionExpense(){
        List<KyCardRecharge> list=kyCardRechargeService.selectList(null);
        if (list.size()>0){
            for (int x=0;x<list.size();x++){
                KyCardRecharge  cardRecharge= kyCardRechargeService.selectById(list.get(x).getCardId());
                cardRecharge.setCurrentBalance(cardRecharge.getCurrentBalance().subtract(cardRecharge.getMonthlyFeeStandard()));//计算当前余额
                int y = cardRecharge.getCurrentBalance().compareTo(cardRecharge.getMonthlyFeeStandard());
                if(cardRecharge.getCurrentBalance().signum()==-1){
                    cardRecharge.setState(3);//欠费
                }else if (y==1||y==0){
                    cardRecharge.setState(1);//正常
                }else if (y==-1){
                    cardRecharge.setState(2);//余额不足
                }
                kyCardRechargeService.updateById(cardRecharge);
            }
        }
        return  R.ok();
    }

}
