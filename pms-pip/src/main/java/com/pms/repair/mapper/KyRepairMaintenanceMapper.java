package com.pms.repair.mapper;
import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.pms.repair.entity.KyRepairMaintenance;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * 
 * 
 * @author ljb
 * @email 517770986@qq.com
 * @date 2018-07-18 16:40:45
 */
public interface KyRepairMaintenanceMapper extends BaseMapper<KyRepairMaintenance> {
    public KyRepairMaintenance seteleByMaintenance(Long repairMaintenanceId);

    List<KyRepairMaintenance> selectByPageList(Map<String,Object> map );
         int countByPageMap(Map<String,Object> map );
    List<KyRepairMaintenance> selectByPageMap(Map<String,Object> map );
}
