package com.pms.repair.mapper;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.pms.repair.entity.KyRechargeHistoricalRecords;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 
 * 
 * @author ljb
 * @email 517770986@qq.com
 * @date 2018-08-06 14:37:25
 */
public interface KyRechargeHistoricalRecordsMapper extends BaseMapper<KyRechargeHistoricalRecords> {

     List<KyRechargeHistoricalRecords> getListByData(@Param("equipmentInfoId")Long equipmentInfoId);

}
