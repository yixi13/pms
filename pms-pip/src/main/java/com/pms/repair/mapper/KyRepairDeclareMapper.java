package com.pms.repair.mapper;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.baomidou.mybatisplus.mapper.Wrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.pms.repair.entity.KyRepairDeclare;
import com.pms.repair.entity.KyRepairMaintenance;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.session.RowBounds;

import java.util.List;
import java.util.Map;

/**
 * 
 * 
 * @author ljb
 * @email 517770986@qq.com
 * @date 2018-07-18 16:40:45
 */
public interface KyRepairDeclareMapper extends BaseMapper<KyRepairDeclare> {
    List<Map<String,Object>> getByPipRepairDeclarePage(Map<String,Object> map);
    Map<String,Object> getByPipRepairDeclare(Long repairMaintenanceId);
    List<KyRepairDeclare> selectByPageRepairDeclare(Map<String, Object> map);
    int countApiPipRepairDeclarePage(Map<String,Object> map);
    int  countRepairDeclare(Map<String, Object> map);
    public List<KyRepairDeclare> selectByListRepairDeclare(String companyCode);
    public int selectBycountRepairnum(@Param(value = "type") int type,@Param(value = "code")String code);
}
