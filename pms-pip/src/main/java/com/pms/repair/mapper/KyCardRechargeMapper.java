package com.pms.repair.mapper;
import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.baomidou.mybatisplus.mapper.Wrapper;
import com.pms.repair.entity.KyCardRecharge;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.session.RowBounds;

import java.util.List;

/**
 * 
 * 
 * @author ljb
 * @email 517770986@qq.com
 * @date 2018-07-18 16:40:45
 */
public interface KyCardRechargeMapper extends BaseMapper<KyCardRecharge> {

    public  KyCardRecharge findById(Long cardId);

    List<KyCardRecharge> selectByKyCardRechargePage(RowBounds rowBounds, @Param("ew") Wrapper<KyCardRecharge> wrapper);
}
