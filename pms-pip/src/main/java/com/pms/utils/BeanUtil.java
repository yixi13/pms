package com.pms.utils;

import org.apache.poi.ss.formula.functions.T;

import java.lang.reflect.Field;

/**
 * Created by 宏铭科技
 *
 * @auther ljb
 * @Date 2018/5/29.
 */
public class BeanUtil {

    /**
     * 同类复制(空不复制)
     *
     * @param origin            被复制的实体类
     * @param destination       复制之后的实体类
     * @param <T>
     */
    public static<T> void mergeObject(T origin, T destination) {
        if (origin == null || destination == null)
            return;
        if (!origin.getClass().equals(destination.getClass()))
            return;

        Field[] fields = origin.getClass().getDeclaredFields();
        for (int i = 0; i < fields.length; i++) {
            try {
                fields[i].setAccessible(true);
                Object value = fields[i].get(origin);
                if (null != value) {
                    fields[i].set(destination, value);
                }
                fields[i].setAccessible(false);
            } catch (Exception e) {
            }
        }

    }
}
