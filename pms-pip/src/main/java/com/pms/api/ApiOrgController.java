package com.pms.api;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.pms.controller.BaseController;
import com.pms.exception.R;
import com.pms.lbs.entity.GeoPoi;
import com.pms.lbs.entity.Geotable;
import com.pms.lbs.service.ILbsService;
import com.pms.partitionPoints.bean.OrgBean;
import com.pms.partitionPoints.entity.KyEquipmentInfo;
import com.pms.partitionPoints.entity.KyEquipmentUploadData;
import com.pms.partitionPoints.entity.KyGroupOrganization;
import com.pms.partitionPoints.entity.KyOrganization;
import com.pms.partitionPoints.service.IKyEquipmentInfoService;
import com.pms.partitionPoints.service.IKyGroupOrganizationService;
import com.pms.partitionPoints.service.IKyOrganizationEquipmentService;
import com.pms.partitionPoints.service.IKyOrganizationService;
import com.pms.util.DateUtil;
import com.pms.validator.Assert;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.util.*;

/**
 * Created by lk on 2018/8/22
 */
@RestController
@RequestMapping(value = "/org")
@Api(value="app分区接口" ,description = "app分区接口")
public class ApiOrgController extends BaseController {

    @Autowired
    IKyOrganizationService kyOrganizationService;
    @Autowired
    IKyOrganizationEquipmentService kyOrganizationEquipmentService;
    @Autowired
    IKyEquipmentInfoService kyEquipmentInfoService;

    /**
     *根据公司编码查询对应的分区展示(带测点)
     * @param
     * @return
     */
    @RequestMapping(value = "/findByCode",method = RequestMethod.GET)
    @ResponseBody
    @ApiOperation(value = "根据公司编码查询对应的分区展示(带测点)")
    @ApiImplicitParams({
            @ApiImplicitParam(name="companyCode",value="公司编码",required = true,dataType = "String",paramType="form"),
            @ApiImplicitParam(name="type",value="角色类型",required = true,dataType = "int",paramType="form"),
            @ApiImplicitParam(name="roleId",value="角色id",required = true,dataType = "long",paramType="form")
    })
    public R findByGroupId(String companyCode,Integer type,Long roleId){
        Assert.isBlank(companyCode,"公司编码不能为空");
        Assert.isNull(type,"角色类型不能为空");
        Assert.isNull(roleId,"角色id不能为空");
        if(type==1){
            roleId = null;
        }
        //角色所有的分区
        List<OrgBean> objList = kyOrganizationService.selectByGroupIdAndCode(roleId,companyCode);
        //分区对应的id集合
        List<Integer> idList = new ArrayList<>();
        Set<Integer> parentIdList = new HashSet<Integer>();
        //返回树结构
        List<OrgBean> newList = new ArrayList<OrgBean>();
        for(OrgBean bean:objList){
            idList.add(bean.getId());
            parentIdList.add(bean.getParentId());
        }
        if(objList.size()>0){
            for(OrgBean bean:objList){
                Integer id = bean.getId();
                Integer parentId = bean.getParentId();
                //父级id不在id集合内，则它是顶级
                if(!idList.contains(parentId)){
                    bean = kyOrganizationService.sortList(id,objList,bean,parentIdList);
                    if(type==1){
                        List<OrgBean> list= kyOrganizationService.getEquipmentsByOrgId(id);
                        bean.getChildren().addAll(list);
                    }
                    newList.add(bean);
                }
            }
        }
        return R.ok().put("data",newList);
    }

    /**
     *根据角色查询对应的分区展示(不带测点)
     * @param
     * @return
     */
    @RequestMapping(value = "/findOrgByCode",method = RequestMethod.GET)
    @ResponseBody
    @ApiOperation(value = "根据角色查询对应的分区展示(不带测点)")
    @ApiImplicitParams({
            @ApiImplicitParam(name="companyCode",value="公司编码",required = true,dataType = "string",paramType="form"),
            @ApiImplicitParam(name="type",value="角色类型",required = true,dataType = "int",paramType="form"),
            @ApiImplicitParam(name="roleId",value="角色id",required = true,dataType = "long",paramType="form")
    })
    public R findOrgByGroupIdAndRoleType(String companyCode,Integer type,Long roleId){
        Assert.isBlank(companyCode,"系统编码不能为空");
        Assert.isNull(type,"角色类型不能为空");
        Assert.isNull(roleId,"角色id不能为空");
        if(type==1){
            roleId = null;
        }
        //角色所有的分区
        List<OrgBean> objList = kyOrganizationService.selectByGroupIdAndCode(roleId,companyCode);
        //分区对应的id集合
        List<Integer> idList = new ArrayList<>();
        Set<Integer> parentIdList = new HashSet<Integer>();
        //返回树结构
        List<OrgBean> newList = new ArrayList<OrgBean>();
        for(OrgBean bean:objList){
            idList.add(bean.getId());
            parentIdList.add(bean.getParentId());
        }
        if(objList.size()>0){
            for(OrgBean bean:objList){
                Integer id = bean.getId();
                Integer parentId = bean.getParentId();
                //父级id不在id集合内，则它是顶级
                if(!idList.contains(parentId)){
                    bean = kyOrganizationService.sortOrgList(id,objList,bean,parentIdList);
                    newList.add(bean);
                }
            }
        }
        return R.ok().put("data",newList);
    }

    /**
     * 计量分区曲线分析
     *
     * @param
     * @return
     */
    @RequestMapping(value = "/getOrgCurveAnalysis",method = RequestMethod.GET)
    @ResponseBody
    @ApiOperation(value = "计量分区曲线分析(h5调用，弃用)")
    @ApiImplicitParams({
            @ApiImplicitParam(name="orgMaxId",value="顶级分区id",required = true,dataType = "int",paramType="form"),
            @ApiImplicitParam(name="orgId",value="对应的分区id",required = true,dataType = "int",paramType="form"),
            @ApiImplicitParam(name="startTime",value="开始时间",required = true,dataType = "string",paramType="form"),
            @ApiImplicitParam(name="endTime",value="结束时间",required = true,dataType = "string",paramType="form")
    })
    public R getEquipmentCurveAnalysis(Integer orgMaxId,Integer orgId,String startTime,String endTime){
        Assert.isNull(orgId,"分区id不能为空");
        Assert.isNull(orgMaxId,"系统id不能为空");
        Assert.isNull(startTime,"开始时间不能为空");
        Assert.isNull(endTime,"结束时间不能为空");
        Date startDate = DateUtil.stringToDate(startTime);

        Date endDate = DateUtil.stringToDate(endTime);
        List<Map<String,Object>> list = kyOrganizationEquipmentService.getOrgWaterDayByEquIdsAdTime(orgMaxId,orgId,startDate,endDate);//产销差，漏损量，漏损率
        return R.ok().put("data",list);
    }

    /**
     * 测点曲线分析
     *
     * @param
     * @return
     */
    @RequestMapping(value = "/getEquipmentCurveAnalysisByDate",method = RequestMethod.GET)
    @ResponseBody
    @ApiOperation(value = "测点曲线分析(h5调用，弃用)")
    @ApiImplicitParams({
            @ApiImplicitParam(name="orgMaxId",value="顶级分区id",required = true,dataType = "int",paramType="form"),
            @ApiImplicitParam(name="equIds",value="测点拼接id",required = true,dataType = "int",paramType="form"),
            @ApiImplicitParam(name="startTime",value="开始时间",required = true,dataType = "String",paramType="form"),
            @ApiImplicitParam(name="endTime",value="结束时间",required = true,dataType = "String",paramType="form")
    })
    public R getEquipmentCurveAnalysisByDate(Integer orgMaxId,String equIds,String startTime,String endTime){
        Assert.isBlank(equIds,"测点拼接字符串");
        Assert.isNull(orgMaxId,"顶级机构id");
        Assert.isNull(startTime,"开始时间不能为空");
        Assert.isNull(endTime,"结束时间不能为空");
        Date startDate = DateUtil.stringToDate(startTime);
        Date endDate = DateUtil.stringToDate(endTime);
        Set<Map<String,Object>> set = new HashSet<>();
        String[] idList = equIds.split(",");
        for(int i=0;i<idList.length;i++){
            Map<String,Object> map = new HashMap<>();
            Integer id = Integer.parseInt(idList[i]);
            KyEquipmentInfo ko = kyEquipmentInfoService.selectById(id);
            List<KyEquipmentUploadData> list = kyOrganizationEquipmentService.getEquipmentDateByIdsAndTime(orgMaxId,id,startDate,endDate);
            map.put("name",ko.getName());
            map.put("id",id);
            map.put("list",list);
            set.add(map);
        }
        return R.ok().put("data",set);
    }

//    /**
//     * 判断当前角色是否有查看分区的权限
//     *
//     * @param
//     * @return
//     */
//    @RequestMapping(value = "/isHavePermission",method = RequestMethod.GET)
//    @ResponseBody
//    @ApiOperation(value = "判断当前角色是否有查看分区的权限")
//    @ApiImplicitParams({
//            @ApiImplicitParam(name="roleId",value="角色id",required = true,dataType = "long",paramType="form"),
//            @ApiImplicitParam(name="orgId",value="分区id",required = true,dataType = "int",paramType="form")
//    })
//    public  R isHavePermission(Long roleId,Integer orgId){
//        int i = kyGroupOrganizationService.selectCount(new EntityWrapper<KyGroupOrganization>().eq("group_id",roleId).eq("organization_id",orgId));
//        if(i>0){
//            return R.ok();
//        }else{
//            return R.error("没有权限");
//        }
//
//    }

    /**
     * 测点曲线分析h5url
     *
     * @param
     * @return
     */
    @RequestMapping(value = "/getEquipmentCurveAnalysisUrl",method = RequestMethod.GET)
    @ResponseBody
    @ApiOperation(value = "测点曲线分析h5url，弃用")
    @ApiImplicitParams({
            @ApiImplicitParam(name="type",value="数据类型:1.瞬时流量2.电源电压3.GPRS信号强度4.压力5水位:",required = true,dataType = "int",paramType="form"),
            @ApiImplicitParam(name="orgMaxId",value="顶级分区id",required = true,dataType = "int",paramType="form"),
            @ApiImplicitParam(name="equIds",value="测点拼接id",required = true,dataType = "int",paramType="form"),
            @ApiImplicitParam(name="startTime",value="开始时间",required = true,dataType = "String",paramType="form"),
            @ApiImplicitParam(name="endTime",value="结束时间",required = true,dataType = "String",paramType="form")
    })
    public R getEquipmentCurveAnalysisUrl(Integer type,Integer orgMaxId,String equIds,String startTime,String endTime){
        Assert.isNull(type,"数据类型不能为空");
        Assert.isBlank(equIds,"测点拼接字符串");
        Assert.isNull(orgMaxId,"顶级机构id");
        Assert.isNull(startTime,"开始时间不能为空");
        Assert.isNull(endTime,"结束时间不能为空");
        String url = "http://url";
        url = url+"?type="+type+"&orgMaxId="+orgMaxId+"&equIds="+equIds+"&startTime="+startTime+"&endTime="+endTime;
        return R.ok().put("data",url);
    }

    /**
     * 计量分区曲线分析h5url
     *
     * @param
     * @return
     */
    @RequestMapping(value = "/getOrgCurveAnalysisUrl",method = RequestMethod.GET)
    @ResponseBody
    @ApiOperation(value = "计量分区曲线分析(h5调用,弃用)")
    @ApiImplicitParams({
            @ApiImplicitParam(name="type",value="数据类型:1.产销差2.漏损量3.漏损率",required = true,dataType = "int",paramType="form"),
            @ApiImplicitParam(name="orgMaxId",value="顶级分区id",required = true,dataType = "int",paramType="form"),
            @ApiImplicitParam(name="orgId",value="对应的分区id",required = true,dataType = "int",paramType="form"),
            @ApiImplicitParam(name="startTime",value="开始时间",required = true,dataType = "string",paramType="form"),
            @ApiImplicitParam(name="endTime",value="结束时间",required = true,dataType = "string",paramType="form")
    })
    public R getOrgCurveAnalysisUrl(Integer type,Integer orgMaxId,Integer orgId,String startTime,String endTime){
        Assert.isNull(type,"数据类型不能为空");
        Assert.isNull(orgId,"分区id不能为空");
        Assert.isNull(orgMaxId,"系统id不能为空");
        Assert.isNull(startTime,"开始时间不能为空");
        Assert.isNull(endTime,"结束时间不能为空");
        String url = "http://url";
        url = url+"?type="+type+"&orgMaxId="+orgMaxId+"&orgId="+orgId+"&startTime="+startTime+"&endTime="+endTime;
        return R.ok().put("data",url);
    }

}
