package com.pms.api;
import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.pms.controller.BaseController;
import com.pms.entity.BaseUserInfo;
import com.pms.entity.UserInfo;
import com.pms.exception.R;
import com.pms.partitionPoints.entity.KyEquipmentInfo;
import com.pms.partitionPoints.entity.KyOrganization;
import com.pms.partitionPoints.entity.KyOrganizationEquipment;
import com.pms.partitionPoints.service.IKyEquipmentInfoService;
import com.pms.partitionPoints.service.IKyOrganizationEquipmentService;
import com.pms.partitionPoints.service.IKyOrganizationService;
import com.pms.repair.entity.KyRepairDeclare;
import com.pms.repair.service.IKyRepairDeclareService;
import com.pms.rpc.UserService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
/**
 * Created by hm on 2018/9/6.
 */
@RestController
@RequestMapping("/pipRepairDeclare")
@Api(value="APP接口管网设备保修申报表",description = "APP接口管网设备保修申报表")
public class ApiKyRepairDeclareController extends BaseController {
    @Autowired
    IKyRepairDeclareService repairDeclareService;
    @Autowired
    UserService userService;
    @Autowired
    IKyOrganizationService kyOrganizationService;
    @Autowired
    IKyEquipmentInfoService kyEquipmentInfoService;
    @Autowired
    IKyOrganizationEquipmentService kyOrganizationEquipmentService;
    /**
     * 分页查询设备保修信息
     * @param roleId
     * @param state
     * @param limit
     * @param page
     * @return
     */
    @PostMapping("/getBypipRepairDeclarePage")
    @ApiOperation(value = "查询设备保修信息")
    @ApiImplicitParams({
            @ApiImplicitParam(name="companyCode",value="系统编码",required = true,dataType = "String",paramType="form"),
            @ApiImplicitParam(name="roleId",value="角色ID",required = true,dataType = "String",paramType="form"),
            @ApiImplicitParam(name="state",value="状态(1全部2待完成3已完成)",required = true,dataType = "Integer",paramType="form"),
            @ApiImplicitParam(name="limit",value="分页条数",required = false,dataType = "int",paramType="form"),
            @ApiImplicitParam(name="page",value="分页页数",required = false,dataType = "int",paramType="form"),
            @ApiImplicitParam(name="userId",value="用户ID",required = true,dataType = "Long",paramType="form"),
    })
    public R getByIotRepairDeclarePage(Long userId,Long roleId,String  companyCode,Integer state, int limit, int page){
        parameterIsNull(roleId,"请填写角色ID");
        parameterIsNull(userId,"请填写用户ID");
        parameterIsNull(companyCode,"请填系统编码");
        parameterIsNull(state,"请填写查询状态");
        if (limit<1){limit=10;}
        if (page<1){page=0;}
        Map<String,Object> map=new HashMap<>();
        UserInfo user= userService.selectbyPipUser(userId);
        if (null==user||null==user.getId()){return  R.error(400,"用户信息不存在");}
        if (user.getGroupType()==1){//管理员
            map.put("companyCode",companyCode);
            map.put("roleId",null);
        }else {
            map.put("roleId",roleId);
            map.put("companyCode",companyCode);
        }
        map.put("limit",limit);
        map.put("page",(page-1)*limit);
        map.put("state",state);
        Page<Map<String,Object>> pageList = new Page<>(page,limit);
        pageList = repairDeclareService.getByPipRepairDeclarePage(pageList,map);
        return R.ok().put("data",pageList);
    }


    /**
     * 添加申报管网测点维修
     * @param roleId
     * @param equipmentInfoId
     * @param userId
     * @param declareType
     * @param description
     * @param companyCode
     * @return
     */
    @PostMapping("/app/add")
    @ApiOperation(value = "添加申报管网测点维修")
    @ApiImplicitParams({
            @ApiImplicitParam(name="equipmentInfoId",value="测点ID",required = true,dataType = "Long",paramType="form"),
            @ApiImplicitParam(name="userId",value="用户ID",required = true,dataType = "Long",paramType="form"),
            @ApiImplicitParam(name="declareType",value="1.未知2.设备故障3.环境因素",required = true,dataType = "Integer",paramType="form"),
            @ApiImplicitParam(name="description",value="故障描述",required = true,dataType = "String",paramType="form"),
            @ApiImplicitParam(name="companyCode",value="系统编码",required = true,dataType = "String",paramType="form"),
            @ApiImplicitParam(name="roleId",value="角色ID",required = true,dataType = "String",paramType="form"),
    })
    public R add(Long roleId,Long equipmentInfoId,Long userId,Integer declareType,String description,String companyCode){
        parameterIsNull(equipmentInfoId,"请填写测点ID");
        parameterIsNull(userId,"请填写用户ID");
        parameterIsNull(roleId,"请填写角色ID");
        parameterIsNull(declareType,"请填写故障类型");
        parameterIsNull(description,"请填写故障描述");
        parameterIsNull(companyCode,"请填写系统编码");
        BaseUserInfo user= userService.getUserByUserId(userId);
        if (null==user||null==user.getId()){return  R.error(400,"用户信息不存在");}
        KyEquipmentInfo eq=kyEquipmentInfoService.selectById(equipmentInfoId);
        if (eq==null){return  R.error(400,"测点信息不存在");}
        KyRepairDeclare entity=new KyRepairDeclare();
        entity.setEquipmentInfoName(eq.getName());
        KyRepairDeclare declare=repairDeclareService.selectOne(new EntityWrapper<KyRepairDeclare>().eq("equipment_info_id",equipmentInfoId).and().eq("state_",1));
        if (declare!=null){return  R.error(400,"该测点已申报,请查看");}
        entity.setCompanyCode(companyCode);//给你公司编码
        entity.setDeclareType(declareType);//故障类型
        entity.setDeclarePerson(user.getName());//申报人
        entity.setDeclarePhone(user.getMobilePhone());//申报人电话
        entity.setDescription(description);//故障描述
        entity.setTarget(2);
        entity.setEquipmentInfoId(equipmentInfoId);//设备ID
        entity.setDeclareData(new Date());//申报时间
        entity.setState(1);//状态1待处理2拒绝受理3已受理
        entity.setRoleId(roleId);
        entity.setCrtTime(new Date());//创建时间
        entity.setIsRead(2);//是否已读(1已读2未读)
        repairDeclareService.insert(entity);
        return R.ok();
    }

    /***
     *查询设备保修信息
     * @param repairDeclareId
     * @param state
     * @return
     */
    @PostMapping("/getBypipRepairDeclare")
    @ApiOperation(value = "查询设备保修信息")
    @ApiImplicitParams({
            @ApiImplicitParam(name="repairDeclareId",value="设备保修信息ID",required = true,dataType = "Long",paramType="form"),
            @ApiImplicitParam(name="state",value="状态(1待受理2待维修3拒绝受理4.维修成功5维修失败)",required = true,dataType = "Integer",paramType="form"),
    })
    public R getByIotRepairDeclare(Long repairDeclareId, Integer state){
        parameterIsNull(repairDeclareId,"请填写设备保修信息ID");
        parameterIsNull(state,"请填写查询状态");
        Map<String, Object>  map =new HashMap<>();
        if (state==4||state==5){
            Map<String, Object>  repairDeclare=repairDeclareService.getByPipRepairDeclare(repairDeclareId);
            if (repairDeclare==null){return  R.error(400,"设备保修信息不存在");}
            if (repairDeclare.get("state_").equals(1)){
                map.put("state",4);
            }else {
                map.put("state",5);
            }
            map.put("equipmentInfoName",repairDeclare.get("equipment_info_name"));//测点名称
            map.put("declareData",repairDeclare.get("declare_data"));//更新时间：该状态更新的最新时间
            map.put("declareType",repairDeclare.get("declare_type") );//故障类型1.未知2.设备故障3.环境因素
            map.put("description",repairDeclare.get("description"));//故障描述
            map.put("acceptPerson",repairDeclare.get("acceptPerson"));//受理人
            map.put("acceptPhone",repairDeclare.get("acceptPhone"));//受理电话
            map.put("crtUser",repairDeclare.get("crtUser"));//操作员
            map.put("startData",repairDeclare.get("start_data"));//开始时间
            map.put("endData",repairDeclare.get("end_data"));//结束时间
            map.put("maintenanceCosts",repairDeclare.get("maintenance_costs"));//维修费用
            map.put("maintenanceDescription",repairDeclare.get("maintenance_description"));//维修描述
            KyRepairDeclare en = repairDeclareService.selectById(repairDeclareId);
            en.setIsRead(1);//修改为已读
            repairDeclareService.updateById(en);
        }else {
            KyRepairDeclare repairDeclare = repairDeclareService.selectById(repairDeclareId);
            if (repairDeclare==null){return  R.error(400,"设备保修信息不存在");}
            if (repairDeclare.getState()==3){
                map.put("state",2);
            }else if (repairDeclare.getState()==2){
                map.put("state",3);
            }else {
                map.put("state",1);
            }
            map.put("equipmentInfoName",repairDeclare.getEquipmentInfoName());//测点名称
            map.put("declareData",repairDeclare.getDeclareData());//更新时间：该状态更新的最新时间
            map.put("declareType",repairDeclare.getDeclareType());//故障类型1.未知2.设备故障3.环境因素
            map.put("description",repairDeclare.getDescription());//故障描述
            map.put("acceptPerson",repairDeclare.getAcceptPerson());//受理人
            map.put("acceptPhone",repairDeclare.getAcceptPhone());//受理电话
            map.put("crtUser",repairDeclare.getCrtUser());//操作员
            map.put("updUser",repairDeclare.getUpdUser());
            map.put("updTime",repairDeclare.getUpdTime());
            map.put("acceptDta",repairDeclare.getAcceptData());
            repairDeclare.setIsRead(1);//修改为已读
            repairDeclareService.updateById(repairDeclare);
        }
        return (R.ok()).put("data",map);
    }

}
