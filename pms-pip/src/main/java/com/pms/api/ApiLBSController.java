package com.pms.api;

import com.pms.lbs.entity.GeoPoi;
import com.pms.lbs.entity.Geotable;
import com.pms.lbs.service.ILbsService;
import com.pms.controller.BaseController;
import com.pms.exception.R;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by ljb on 2018/1/10.
 */
@RestController
@RequestMapping(value = "/lbs", method = RequestMethod.POST)
@Api(value="LBS 业务处理接口" ,description = "LBS 业务处理接口")
public class ApiLBSController extends BaseController {

    @Autowired
    private ILbsService iLbsService;

    @RequestMapping(value = "/createGeotable", method = RequestMethod.POST)
    @ApiOperation(value = "创建LBS表格")
    @ApiImplicitParam(name="name",value="LBS表明",required = true,dataType = "String",paramType="form")
    public R createGeotable(String name) {
        Map resultMap = new HashMap();
        Geotable geotable = new Geotable();
        iLbsService.createGeotable(name);
        return R.ok();
    }
    @ApiOperation(value = "查询LBS表格")
    @ApiImplicitParam(name="name",value="LBS表明",required = true,dataType = "String",paramType="form")
    @RequestMapping(value = "/listGeotable", method = RequestMethod.POST)
    public R listGeotable(String name) {
        Map resultMap = new HashMap();
        List<Geotable> list =  iLbsService.getGeotableList(name);
        resultMap.put("list",list);
        return R.ok(resultMap);
    }
    @ApiOperation(httpMethod = "POST",value = "百度LBS-添加数据", notes = "百度LBS-添加数据")
    @ApiImplicitParams({
            @ApiImplicitParam(name="title",value="数据名称",required = false,dataType = "String",paramType="form"),
            @ApiImplicitParam(name="address",value="位置数据地址",required = false,dataType = "String",paramType="form"),
            @ApiImplicitParam(name="tags",value="位置数据类别",required = false,dataType = "String",paramType="form"),
            @ApiImplicitParam(name="latitude",value="维度",required = false,dataType = "String",paramType="form"),
            @ApiImplicitParam(name="longitude",value="精度",required = false,dataType = "String",paramType="form"),
            @ApiImplicitParam(name="polygons",value="若该字段传入数据，则判定为面状位置数据。格式为：经度,纬度;经度,纬度;经度,纬度",
                    required = false,dataType = "String",paramType="form"),
            @ApiImplicitParam(name="type",value="类别",required = false,dataType = "String",paramType="form"),
            @ApiImplicitParam(name="level",value="级别",required = false,dataType = "String",paramType="form")
    })
    @RequestMapping(value = "/createPoi", method = RequestMethod.POST)
    public R createPoi(String title,String address,String tags,String latitude,String longitude,String polygons,
                  String type,String level,String spId) {
        Map resultMap = new HashMap();
        Long id = iLbsService.createPoi(longitude,latitude,polygons,address,title,tags,level,type,spId);
        resultMap.put("id", id);
        return R.ok(resultMap);
    }

    @ApiOperation(httpMethod = "POST",value = "百度LBS-分页查询数据", notes = "百度LBS-分页查询数据")
    @RequestMapping(value = "/listPoi", method = RequestMethod.POST)
    public R listPoi(Integer page_index,Integer page_size, String title,String tags, String level,String type,String spId) {
        Map resultMap = new HashMap();
        List<GeoPoi> geoPois = iLbsService.getPoiList(page_index, page_size, title, tags, level, type, spId);
        resultMap.put("geoPois", geoPois);
        return R.ok(resultMap);
    }

    @ApiOperation(httpMethod = "POST",value = "百度LBS-根据ID查询", notes = "百度LBS-根据ID查询")
    @RequestMapping(value = "/detailPoi", method = RequestMethod.POST)
    public R detailPoi(String id) {
        Map resultMap = new HashMap();
        GeoPoi geoPois = iLbsService.getPoiById(id);
        resultMap.put("geoPois", geoPois);
        return R.ok(resultMap);
    }

}
