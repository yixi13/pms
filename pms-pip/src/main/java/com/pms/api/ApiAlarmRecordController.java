package com.pms.api;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.pms.alarm.entity.KyAlarmRecord;
import com.pms.alarm.service.IKyAlarmRecordService;
import com.pms.controller.BaseController;
import com.pms.exception.R;
import com.pms.partitionPoints.entity.KyOrganization;
import com.pms.partitionPoints.service.IKyOrganizationService;
import com.pms.util.DateUtil;
import com.pms.validator.Assert;
import com.xiaoleilu.hutool.util.StrUtil;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * <p>
 * 设备上传数据 前端控制器
 * </p>
 *
 * @author ljb
 * @since 2018-07-16
 */
@RestController
@RequestMapping("/apiAlarm/alarmRecord")
@Api(value="API报警记录接口")
public class ApiAlarmRecordController extends BaseController {

    @Autowired
    IKyAlarmRecordService iKyAlarmRecordService;
    @Autowired
    IKyOrganizationService iKyOrganizationService;

    @ApiOperation(value = "获取报警记录list")
    @RequestMapping(value = "/list",method = RequestMethod.GET)
    @ApiImplicitParams({
            @ApiImplicitParam(name="current",value="页数",required = true,dataType = "String",paramType="form"),
            @ApiImplicitParam(name="size",value="每页条数",required = true,dataType = "String",paramType="form"),
            @ApiImplicitParam(name="starTime",value="开始时间",required = true,dataType = "String",paramType="form"),
            @ApiImplicitParam(name="endTime",value="结束时间",required = true,dataType = "String",paramType="form"),
            @ApiImplicitParam(name="alarmType",value="报警类别",required = false,dataType = "String",paramType="form"),
            @ApiImplicitParam(name="equipmentIds",value="测点ids",required = false,dataType = "String",paramType="form"),
            @ApiImplicitParam(name="content",value="模糊查询描述",required = false,dataType = "String",paramType="form")
    })
    public R list(Integer current, Integer size, String starTime, String endTime,
                  String alarmType,String content,String isReade,
                  @RequestHeader("X-Code") String companyCode,
                  @RequestHeader("X-groupId") String groupId,
                  @RequestHeader("X-groupType") String groupType){
        Assert.isNull(current,"current 不能为空");
        Assert.isNull(size,"size 不能为空");

        EntityWrapper<Map<String,Object>> ew = new EntityWrapper<Map<String,Object>>();
        if (StrUtil.isNotEmpty(alarmType)){
            ew.eq("kar.changeType", alarmType);
        }
        String equipmentIds = "";
        List<KyOrganization> orgList = iKyOrganizationService.getOrganizationByRoleTypeAndRoleIdAndCompanyCode(
                    Integer.valueOf(groupType),Long.valueOf(groupId),companyCode);
        String orgIds = "";
        for (KyOrganization org : orgList) {
            orgIds += org.getId() + ",";
        }
        if (orgIds.length() > 0 ) {
            orgIds = orgIds.substring(0, orgIds.length() - 1);
            List<Map<String,Object>> eqList = iKyOrganizationService.getEquipmentsByOrgIds(orgIds);
            for (Map<String, Object> eq : eqList) {
                equipmentIds += eq.get("id") + ",";
            }
            if (equipmentIds.length() >0 ) {
                equipmentIds = equipmentIds.substring(0, equipmentIds.length() - 1);
                ew.in("kar.equipmentId", equipmentIds);
            }else {
                ew.in("kar.equipmentId", "-1");
            }
        }else{
            ew.in("kar.equipmentId", "-1");
        }

        if (StrUtil.isNotEmpty(starTime)) {
            starTime = starTime + " 00:00:00";
            ew.and("kar.acquisitionTime >'" + starTime + "'");
        }
        if (StrUtil.isNotEmpty(endTime)) {
            endTime = endTime + " 23:59:59";
            ew.and("kar.acquisitionTime <'" + endTime + "'");
        }
        if (StrUtil.isNotEmpty(content)){
            ew.and().like("kar.changeDescription", content).
                    or(" kei.name LIKE '%"+content+"%' ");
        }
        if (StrUtil.isNotEmpty(isReade)) {
            ew.eq("kar.isReade", isReade);
        }
        ew.orderBy("kar.isReade desc");
        ew.orderBy("kar.acquisitionTime desc");
        Page<Map<String,Object>> page = new Page(current, size);
        page = iKyAlarmRecordService.selectAlarmRecordRecordMapsPage(page,ew);
        return R.ok().put("page",page);
    }


    @ApiOperation(value = "修改为已读")
    @RequestMapping(value = "/updateToReade",method = RequestMethod.POST)
    @ApiImplicitParams({
            @ApiImplicitParam(name="alarmIds", value="报警ids",required = true,dataType = "String",paramType="form")
    })
    public R updateToReade(String alarmIds){
        Assert.isBlank(alarmIds,"报警ids");
        EntityWrapper<KyAlarmRecord> ew = new EntityWrapper<KyAlarmRecord>();
        ew.in("id", alarmIds);
        List<KyAlarmRecord> records = iKyAlarmRecordService.selectList(ew);
        for (KyAlarmRecord record:records){
            record.setIsReade(1);
        }
        iKyAlarmRecordService.updateBatchById(records);
        return R.ok();
    }


    @ApiOperation(value = "获取详情")
    @RequestMapping(value = "/{alarmId}",method = RequestMethod.GET)
    @ApiImplicitParams({
            @ApiImplicitParam(name="alarmId", value="报警ids",required = true,dataType = "String",paramType="form")
    })
    public R detail(@PathVariable String alarmId){
        Map<String , Object> record = iKyAlarmRecordService.selectApiAlarmRecord(alarmId);
        return R.ok().put("alarmRecord",record);
    }


}
