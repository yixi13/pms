package com.pms.realtime.service.impl;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.mapper.SqlHelper;
import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.pms.realtime.entity.RealTime;
import com.pms.realtime.mapper.RealTimeMapper;
import com.pms.realtime.service.IRealTimeService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author ljb
 * @since 2018-07-20
 */
@Service
public class RealTimeServiceImpl extends ServiceImpl<RealTimeMapper, RealTime> implements IRealTimeService {

    Logger logger = LoggerFactory.getLogger(this.getClass());

    @Override
    public Page<Map<String, Object>> selectRealTimeDataPage(Page<Map<String, Object>> page, EntityWrapper<Map<String, Object>> ew) {
        SqlHelper.fillWrapper(page, ew);
        logger.info(ew.getSqlSegment());
        page.setRecords(baseMapper.selectRealTimeDataPage(page, ew));
        return page;
    }

    @Override
    public Page<Map<String, Object>> selectRealTimeDataPageNew(
            Page<Map<String, Object>> page,Map<String, Object> parameter) {
        Integer count = baseMapper.countRealTimeData(parameter);
        page.setTotal(count);
        List<Map<String,Object>> equipmentDatas =
                baseMapper.selectRealTimeData(parameter);
        page.setRecords(equipmentDatas);
        return page;
    }
}
