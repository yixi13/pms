package com.pms.realtime.service;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.service.IService;
import com.pms.realtime.entity.RealTime;

import java.util.Map;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author ljb
 * @since 2018-07-20
 */
public interface IRealTimeService extends IService<RealTime>{

    Page<Map<String,Object>> selectRealTimeDataPage(Page<Map<String, Object>> page, EntityWrapper<Map<String, Object>> ew);

    Page<Map<String,Object>> selectRealTimeDataPageNew(Page<Map<String, Object>> page, Map<String, Object> parameter);
}
