package com.pms.realtime.entity;


public class TempEquipmentDataVo {
	
	private Integer id;
	private String partitionName;
	private String dmaNames;
	private String equipmentName;
	private String communicationStatus;
	private String equipmentStatus;
	private String creatTime;
	private String acquisitionTime;
	private String dataSources;
	private String channelType;
	
	//@FiledMappingKeyAnnotation(value="netCumulativeFlow",viewName="净累计流量",unit="m3")
	private String netCumulativeFlow;
	
	//@FiledMappingKeyAnnotation(value="positiveCumulativeFlow",viewName="正累计流量",unit="m3")
	private String positiveCumulativeFlow;
	
	//@FiledMappingKeyAnnotation(value="negativeAccumulatedFlow",viewName="负累计流量",unit="m3")
	private String negativeAccumulatedFlow;
	
	//@FiledMappingKeyAnnotation(value="instantaneousFlow",viewName="瞬时流量",unit="m3/h")
	private String instantaneousFlow;
	
	//@FiledMappingKeyAnnotation(value="waterLevel",viewName="水位",unit="m")
	private String waterLevel;
	
	//@FiledMappingKeyAnnotation(value="pressure",viewName="压力",unit="Mpa")
	private String pressure;
	
	//@FiledMappingKeyAnnotation(value="supplyVoltage",viewName="电源电压",unit="v")
	private String supplyVoltage;
	
	//@FiledMappingKeyAnnotation(value="batteryVoltageAlarm",viewName="电池电压报警")
	private String batteryVoltageAlarm;
	
	//@FiledMappingKeyAnnotation(value="serialCommunicationState",viewName="串口通讯状态")
	private String serialCommunicationState;
	
	//@FiledMappingKeyAnnotation(value="gprsSignalIntensity",viewName="GPRS信号强度")
	private String gprsSignalIntensity;
	
	//@FiledMappingKeyAnnotation(value="doorSwitch",viewName="箱门开关")
	private String doorSwitch;
	
	//@FiledMappingKeyAnnotation(value="pressureFault",viewName="压力故障")
	private String pressureFault;
	
	//@FiledMappingKeyAnnotation(value="higherPressureAlarm",viewName="压力上限报警")
	private String higherPressureAlarm;
	
	//@FiledMappingKeyAnnotation(value="lowerPressureAlarm",viewName="压力下限报警")
	private String lowerPressureAlarm;
	
	//@FiledMappingKeyAnnotation(value="batteryVoltageLowAlarm",viewName="电池电压低报警")
	private String batteryVoltageLowAlarm;
	
	//@FiledMappingKeyAnnotation(value="batteryVoltageTooLowAlarm",viewName="电池电压过低报警")
	private String batteryVoltageTooLowAlarm;
	
	//@FiledMappingKeyAnnotation(value="pressureStatus",viewName="压力状态")
	private String pressureStatus;
	
	//@FiledMappingKeyAnnotation(value="powerSupplyVoltage",viewName="电源电压状态")
	private String powerSupplyVoltage;
	
	//@FiledMappingKeyAnnotation(value="waterLevelFault",viewName="水位故障")
	private String waterLevelFault;
	
	//@FiledMappingKeyAnnotation(value="waterUpLevelAlarm",viewName="水位上限报警")
	private String waterUpLevelAlarm;
	
	//@FiledMappingKeyAnnotation(value="waterDownLevelAlarm",viewName="水位下限报警")
	private String waterDownLevelAlarm;
	
	//@FiledMappingKeyAnnotation(value="waterLevelStatus",viewName="水位状态")
	private String waterLevelStatus;
	
	private String longitude;
	private String latitude;
	private String installationTime;
	private String equipmentAddress;
	private String phoneNo;
	
	
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getPartitionName() {
		return partitionName;
	}

	public String getDmaNames() {
		return dmaNames;
	}

	public void setDmaNames(String dmaNames) {
		this.dmaNames = dmaNames;
	}

	public void setPartitionName(String partitionName) {
		this.partitionName = partitionName;
	}
	public String getEquipmentName() {
		return equipmentName;
	}
	public void setEquipmentName(String equipmentName) {
		this.equipmentName = equipmentName;
	}
	public String getCommunicationStatus() {
		return communicationStatus;
	}
	public void setCommunicationStatus(String communicationStatus) {
		this.communicationStatus = communicationStatus;
	}
	public String getEquipmentStatus() {
		return equipmentStatus;
	}
	public void setEquipmentStatus(String equipmentStatus) {
		this.equipmentStatus = equipmentStatus;
	}
	public String getCreatTime() {
		return creatTime;
	}
	public void setCreatTime(String creatTime) {
		this.creatTime = creatTime;
	}
	public String getAcquisitionTime() {
		return acquisitionTime;
	}
	public void setAcquisitionTime(String acquisitionTime) {
		this.acquisitionTime = acquisitionTime;
	}
	public String getNetCumulativeFlow() {
		return netCumulativeFlow;
	}
	public void setNetCumulativeFlow(String netCumulativeFlow) {
		this.netCumulativeFlow = netCumulativeFlow;
	}
	public String getPositiveCumulativeFlow() {
		return positiveCumulativeFlow;
	}
	public void setPositiveCumulativeFlow(String positiveCumulativeFlow) {
		this.positiveCumulativeFlow = positiveCumulativeFlow;
	}
	public String getNegativeAccumulatedFlow() {
		return negativeAccumulatedFlow;
	}
	public void setNegativeAccumulatedFlow(String negativeAccumulatedFlow) {
		this.negativeAccumulatedFlow = negativeAccumulatedFlow;
	}
	public String getInstantaneousFlow() {
		return instantaneousFlow;
	}
	public void setInstantaneousFlow(String instantaneousFlow) {
		this.instantaneousFlow = instantaneousFlow;
	}
	public String getWaterLevel() {
		return waterLevel;
	}
	public void setWaterLevel(String waterLevel) {
		this.waterLevel = waterLevel;
	}
	public String getPressure() {
		return pressure;
	}
	public void setPressure(String pressure) {
		this.pressure = pressure;
	}
	public String getSupplyVoltage() {
		return supplyVoltage;
	}
	public void setSupplyVoltage(String supplyVoltage) {
		this.supplyVoltage = supplyVoltage;
	}
	public String getBatteryVoltageAlarm() {
		return batteryVoltageAlarm;
	}
	public void setBatteryVoltageAlarm(String batteryVoltageAlarm) {
		this.batteryVoltageAlarm = batteryVoltageAlarm;
	}
	public String getSerialCommunicationState() {
		return serialCommunicationState;
	}
	public void setSerialCommunicationState(String serialCommunicationState) {
		this.serialCommunicationState = serialCommunicationState;
	}
	public String getGprsSignalIntensity() {
		return gprsSignalIntensity;
	}
	public void setGprsSignalIntensity(String gprsSignalIntensity) {
		this.gprsSignalIntensity = gprsSignalIntensity;
	}
	public String getDoorSwitch() {
		return doorSwitch;
	}
	public void setDoorSwitch(String doorSwitch) {
		this.doorSwitch = doorSwitch;
	}
	public String getPressureFault() {
		return pressureFault;
	}
	public void setPressureFault(String pressureFault) {
		this.pressureFault = pressureFault;
	}
	public String getHigherPressureAlarm() {
		return higherPressureAlarm;
	}
	public void setHigherPressureAlarm(String higherPressureAlarm) {
		this.higherPressureAlarm = higherPressureAlarm;
	}
	public String getLowerPressureAlarm() {
		return lowerPressureAlarm;
	}
	public void setLowerPressureAlarm(String lowerPressureAlarm) {
		this.lowerPressureAlarm = lowerPressureAlarm;
	}
	public String getBatteryVoltageLowAlarm() {
		return batteryVoltageLowAlarm;
	}
	public void setBatteryVoltageLowAlarm(String batteryVoltageLowAlarm) {
		this.batteryVoltageLowAlarm = batteryVoltageLowAlarm;
	}
	public String getBatteryVoltageTooLowAlarm() {
		return batteryVoltageTooLowAlarm;
	}
	public void setBatteryVoltageTooLowAlarm(String batteryVoltageTooLowAlarm) {
		this.batteryVoltageTooLowAlarm = batteryVoltageTooLowAlarm;
	}
	public String getPressureStatus() {
		return pressureStatus;
	}
	public void setPressureStatus(String pressureStatus) {
		this.pressureStatus = pressureStatus;
	}
	public String getPowerSupplyVoltage() {
		return powerSupplyVoltage;
	}
	public void setPowerSupplyVoltage(String powerSupplyVoltage) {
		this.powerSupplyVoltage = powerSupplyVoltage;
	}
	public String getWaterLevelFault() {
		return waterLevelFault;
	}
	public void setWaterLevelFault(String waterLevelFault) {
		this.waterLevelFault = waterLevelFault;
	}
	public String getWaterUpLevelAlarm() {
		return waterUpLevelAlarm;
	}
	public void setWaterUpLevelAlarm(String waterUpLevelAlarm) {
		this.waterUpLevelAlarm = waterUpLevelAlarm;
	}
	public String getWaterDownLevelAlarm() {
		return waterDownLevelAlarm;
	}
	public void setWaterDownLevelAlarm(String waterDownLevelAlarm) {
		this.waterDownLevelAlarm = waterDownLevelAlarm;
	}
	public String getWaterLevelStatus() {
		return waterLevelStatus;
	}
	public void setWaterLevelStatus(String waterLevelStatus) {
		this.waterLevelStatus = waterLevelStatus;
	}
	public String getLongitude() {
		return longitude;
	}
	public void setLongitude(String longitude) {
		this.longitude = longitude;
	}
	public String getLatitude() {
		return latitude;
	}
	public void setLatitude(String latitude) {
		this.latitude = latitude;
	}
	public String getInstallationTime() {
		return installationTime;
	}
	public void setInstallationTime(String installationTime) {
		this.installationTime = installationTime;
	}
	public String getEquipmentAddress() {
		return equipmentAddress;
	}
	public void setEquipmentAddress(String equipmentAddress) {
		this.equipmentAddress = equipmentAddress;
	}
	public String getPhoneNo() {
		return phoneNo;
	}
	public void setPhoneNo(String phoneNo) {
		this.phoneNo = phoneNo;
	}
	public String getDataSources() {
		return dataSources;
	}
	public void setDataSources(String dataSources) {
		this.dataSources = dataSources;
	}
	public String getChannelType() {
		return channelType;
	}
	public void setChannelType(String channelType) {
		this.channelType = channelType;
	}
	
	
	
	
	
	
	
}
