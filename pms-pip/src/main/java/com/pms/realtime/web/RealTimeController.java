package com.pms.realtime.web;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.pms.controller.BaseController;
import com.pms.exception.R;
import com.pms.partitionPoints.service.IKyOrganizationService;
import com.pms.realtime.service.IRealTimeService;
import com.pms.validator.Assert;
import com.xiaoleilu.hutool.util.StrUtil;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by 宏铭科技
 *
 * @auther ljb
 * @Date 2018/4/18.
 */
@RestController
@RequestMapping(value = "/realtime")
@Api(value="实施监控接口")
public class RealTimeController extends BaseController{

    @Autowired
    private IKyOrganizationService iKyOrganizationService;
    @Autowired
    private IRealTimeService iRealTimeService;

    @ApiOperation(value = "获取所有实时数据")
    @RequestMapping(value = "/list",method = RequestMethod.GET)
    @ApiImplicitParams({
            @ApiImplicitParam(name="orgIds",value="机构ids",required = true,dataType = "String",paramType="form"),
            @ApiImplicitParam(name="equipmentIds",value="测点ids",required = true,dataType = "String",paramType="form"),
            @ApiImplicitParam(name="current",value="当前页",required = true,dataType = "String",paramType="form"),
            @ApiImplicitParam(name="size",value="每页条数",required = true,dataType = "String",paramType="form"),
            @ApiImplicitParam(name="equipmentStatus",value="测点状态",required = true,dataType = "String",paramType="form")
    })
    public R list(Integer current, Integer size, String orgIds,
                  String equipmentStatus ,String equipmentIds,String conditions){
//        Assert.isBlank(areaIds,"分区ids");
        Assert.isNull(current,"current 不能为空");
        Assert.isNull(size,"size 不能为空");
        Assert.isBlank(orgIds,"orgIds 不能为空");

        // 通过机构ids 查询所属的groupId
        String groupIds = iKyOrganizationService.getGroupIds(orgIds);
        if (groupIds == null) {
//            groupIds = "3";
            return  R.error(400,"没有对应的测点数据");
        }
        String[] groupIdsArr = groupIds.split(",");
        Map<String, Object> parameter = new HashMap<>();
        parameter.put("groupIds", groupIdsArr);
        parameter.put("equipmentStatus",equipmentStatus);
        parameter.put("pageSize",size);
        parameter.put("startRow",(current-1) * size);

        if (StrUtil.isNotEmpty(equipmentIds)) {
            parameter.put("equipmentIds", equipmentIds);
        }
        if (StrUtil.isNotEmpty(conditions)) {
            parameter.put("conditions", conditions);
        }
        Page<Map<String,Object>> page = new Page(current,size);
        page = iRealTimeService.selectRealTimeDataPageNew(page, parameter);
        return R.ok().put("page",page);
    }

}
