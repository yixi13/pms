package com.pms.realtime.mapper;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.pms.realtime.entity.RealTime;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * <p>
  *  Mapper 接口
 * </p>
 *
 * @author ljb
 * @since 2018-07-20
 */
public interface RealTimeMapper extends BaseMapper<RealTime> {

    List<Map<String,Object>> selectRealTimeDataPage(Page<Map<String, Object>> page,@Param("ew") EntityWrapper<Map<String, Object>> ew);

    Integer countRealTimeData(@Param("parameter")Map<String, Object> parameter);

    List<Map<String,Object>> selectRealTimeData(@Param("parameter") Map<String, Object> parameter);
}