package com.pms.lbs.service.impl;

import com.alibaba.fastjson.JSONObject;
import com.pms.lbs.entity.GeoPoi;
import com.pms.lbs.entity.Geotable;
import com.pms.lbs.service.ILbsService;
import com.pms.lbs.utils.LbsConfig;
import com.pms.exception.RRException;
import com.pms.validator.Assert;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by 宏铭科技
 *
 * 百度LBS 云存储V3接口类
 *
 * @auther ljb
 * @Date 2018/5/7.
 */
@Service
public class LbsServiceImpl implements ILbsService {

    Logger logger = LoggerFactory.getLogger(this.getClass());
    /**
     * LBS 上面创建geotable
     *
     * @param name              geotable的中文名称
     * @return
     */
    @Override
    public JSONObject createGeotable(String name) {
        Assert.isNull(name,"name不能为空");
        JSONObject json = LbsConfig.createGeotable(name,1,1);
        logger.info(json.toJSONString());
        return json;
    }

    /**
     * 查询LBS 表list
     * @param name
     * @return
     */
    @Override
    public List<Geotable> getGeotableList(String name) {
        Assert.isNull(name,"name不能为空");
        JSONObject json = LbsConfig.getGeotableList(name);
        logger.info(json.toJSONString());
        List<Geotable> list = JSONObject.parseArray(json.get("geotables").toString(),Geotable.class);
        return list;
    }

    /**
     *  更具LBS返回的ID 查询表详情
     * @param Id
     * @return
     */
    @Override
    public JSONObject getGeotableById(String Id) {
        Assert.isNull(Id,"id不能为空");
        JSONObject json = LbsConfig.getGeotableById(Id);
        logger.info(json.toJSONString());
        return null;
    }

    @Override
    public Long createPoi(String longitude, String latitude, String polygons, String address, String title, String tags,
                          String level, String type,String spId) {
        Assert.isNull(longitude,"longitude不能为空");
        Assert.isNull(latitude,"latitude不能为空");
        JSONObject json = LbsConfig.createPoi(longitude,latitude,polygons, address, title,tags, level,type,spId);
        if (!"0".equals(json.get("status").toString())) {
            throw new RRException("插入百度LBS点出错!");
        }
        logger.info(json.toJSONString());
        return Long.valueOf(json.get("id").toString());
    }

    @Override
    public List<GeoPoi> getPoiList(Integer page_index, Integer page_size, String title, String tags, String level, String type, String spId) {
        JSONObject json = LbsConfig.getPoiList(page_index, page_size, title, tags, level, type, spId);
        if (!"0".equals(json.get("status").toString())) {
            throw new RRException("插入百度LBS点出错!");
        }
        logger.info(json.toJSONString());
        List<GeoPoi> geoPois = JSONObject.parseArray(json.get("pois").toString(),GeoPoi.class);
        return geoPois;
    }

    @Override
    public GeoPoi getPoiById(String id) {
        JSONObject json = LbsConfig.getPoiById(id);
        if (!"0".equals(json.get("status").toString())) {
            throw new RRException("插入百度LBS点出错!");
        }
        logger.info(json.toJSONString());
        GeoPoi geoPois = JSONObject.parseObject(json.get("poi").toString(),GeoPoi.class);
        return geoPois;
    }

    /**
     * 修改LBS 数据
     */
    @Override
    public Long updatePoiById(String id,String title,String address,String tags,String latitude,String longitude, String level,String type,String spId) {
        JSONObject json = LbsConfig.updatePoiById(id, title, address, tags, latitude, longitude,  level, type, spId);
        if (!"0".equals(json.get("status").toString())) {
            throw new RRException("插入百度LBS点出错!");
        }
        logger.info(json.toJSONString());
        return null;
    }

    /**
     * 批量删除 lbs 数据
     * @param ids
     * @return
     */
    @Override
    public boolean deletePoiByIds(String ids) {
        JSONObject json = LbsConfig.deletePoiByIds(ids);
        logger.info(json.toJSONString());
        return true;
    }
}
