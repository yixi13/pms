package com.pms.lbs.service;

import com.alibaba.fastjson.JSONObject;
import com.pms.lbs.entity.GeoPoi;
import com.pms.lbs.entity.Geotable;

import java.util.List;

/**
 * Created by 宏铭科技
 *
 * 百度LBS 云存储V4接口类
 *
 * @auther ljb
 * @Date 2018/5/7.
 */
public interface ILbsService{


    /**
     * LBS 上面创建geotable
     *
     * @param name              geotable的中文名称
     * @return
     */
    public JSONObject createGeotable(String name);

    /**
     * 查询LBS 表list
     * @param name
     * @return
     */
    public List<Geotable> getGeotableList(String name);

    /**
     *  更具LBS返回的ID 查询表详情
     * @param Id
     * @return
     */
    public JSONObject getGeotableById(String Id);

    /**
     * LBS 创建表数据
     */
    public Long createPoi(String longitude, String latitude,String polygons, String address, String title,String tags,
                                String level,String type, String spId );

    /**
     *  分页获取LBS  点数量
     * @param page_index
     * @param page_size
     * @param title
     * @param tags
     * @param level
     * @param type
     * @param spId
     * @return
     */
    public List<GeoPoi> getPoiList(Integer page_index,Integer page_size, String title,String tags, String level,String type,String spId);

    /**
     * 根据id 查询LBS 数据
     * @param id
     * @return
     */
    public GeoPoi getPoiById(String id);

    /**
     * 修改lbs 数据
     * @return
     */
    public Long updatePoiById(String id,String title,String address,String tags,String latitude,String longitude, String level,String type,String spId);

    /**
     * 批量删除 lbs 数据
     * @param ids
     * @return
     */
    public boolean deletePoiByIds(String ids);


}
