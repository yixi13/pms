package com.pms.lbs.utils;

import com.alibaba.fastjson.JSONObject;
import com.pms.util.httpUtil.HttpClientUtil;
import com.xiaoleilu.hutool.util.StrUtil;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

/**
 * Created by 宏铭科技
 *
 * @auther ljb
 * @Date 2018/5/7.
 */
@Component
public class LbsConfig {


    Logger logger = LoggerFactory.getLogger(this.getClass());

    // 百度LBS提供的AK值
    private static String AK = "h4I1NguAiFaEjAKlSKHXxvRODEH3mYs2";
    // 百度LBS GeotableId
    private static String geotableId = "1000003845";

    // 百度LBS创建表  POST请求
    private static final String createGeotableUrl = "http://api.map.baidu.com/geodata/v4/geotable/create";
    // 百度LBS 查询创建过的表
    private static final String listGeotableUrl = "http://api.map.baidu.com/geodata/v4/geotable/list";
    // 百度LBS 更具id查询表详情
    private static final String detailGeotableUrl = "http://api.map.baidu.com/geodata/v4/geotable/detail";
    //百度LBS创建数据请求
    private static final String poiCreateUrl = "http://api.map.baidu.com/geodata/v4/poi/create";
    // 百度LBS修改数据请求
    private static final String poiUpdateUrl = "http://api.map.baidu.com/geodata/v4/poi/update";
    // 百度LBS查询数据请求
    private static final String poiSelectListUrl = "http://api.map.baidu.com/geodata/v4/poi/list";
    // 百度LBS-根据id查询数据
    private static final String poiSelectByIdUrl = "http://api.map.baidu.com/geodata/v4/poi/detail";
    // 百度LBS-根据id删除数据
    private static final String poiDeleteByIdUrl = "http://api.map.baidu.com/geodata/v4/poi/delete";
    // 百度LBS周边搜索数据请求
    private static final String poiGeoSearchUrl = "http://api.map.baidu.com/geosearch/v4/nearby";
    // 百度LBS创建列  POST请求
    private static final String createColumnUrl = "http://api.map.baidu.com/geodata/v4/column/create";

    public LbsConfig(String AK, String geotableId) {
        this.AK = AK;
        this.geotableId = geotableId;
    }

    public LbsConfig(String AK) {
        this.AK = AK;
    }

    public LbsConfig() {
    }

    /**
     * LBS 上面创建geotable
     *
     * @param name         geotable的中文名称
     * @param geotype      数据的类型 1：点；3：面
     * @param is_published 是否发布到检索 0：未自动发布到云检索，1：自动发布到云检索；
     * @return
     */
    public static JSONObject createGeotable(String name, Integer geotype, Integer is_published) {
        StringBuilder param = new StringBuilder("ak=").append(AK).
                append("&name=").append(name).append("&geotype=").append(geotype).
                append("&is_published=").append(is_published);
        JSONObject ResponseJSON = HttpClientUtil.doPost(
                createGeotableUrl, param.toString());
        return ResponseJSON;
    }

    /**
     * 查询LBS 表list
     *
     * @param name
     */
    public static JSONObject getGeotableList(String name) {
        StringBuilder param = new StringBuilder("ak=").append(LbsConfig.getAK());
        if (!StrUtil.isEmpty(name)) {
            param.append("&name=").append(name);
        }
        JSONObject ResponseJSON = JSONObject.parseObject(HttpClientUtil.doGet(
                listGeotableUrl, param.toString()));
        return ResponseJSON;
    }

    /**
     * 更具LBS返回的ID 查询表详情
     *
     * @param Id
     * @return
     */
    public static JSONObject getGeotableById(String Id) {
        StringBuilder param = new StringBuilder("ak=").
                append(LbsConfig.getAK()).append("&id=").append(Id);

        JSONObject ResponseJSON = JSONObject.parseObject(
                HttpClientUtil.doGet(detailGeotableUrl, param.toString()));
        return ResponseJSON;
    }

    /**
     * LBS 创建数据
     */
    public static JSONObject createPoi(String longitude, String latitude, String polygons, String address, String title, String tags,
                                       String level, String type, String spId) {
        JSONObject ResponseJSON = null;
        StringBuilder param = new StringBuilder("ak=").append(LbsConfig.getAK()).append("&geotable_id=").append(geotableId).append("&coord_type=3")
                .append("&latitude=").append(latitude).append("&longitude=").append(longitude);
        if (StringUtils.isNotBlank(polygons)) {
            param.append("&polygons=").append(polygons);
        }
        if (StringUtils.isNotBlank(address)) {
            param.append("&address=").append(address);
        }
        if (StringUtils.isNotBlank(title)) {
            param.append("&title=").append(title);
        }
        if (StringUtils.isNotBlank(tags)) {
            param.append("&tags=").append(tags);
        }
        if (StringUtils.isNotBlank(level)) {
            param.append("&level=").append(level);
        }
        if (StringUtils.isNotBlank(type)) {
            param.append("&type=").append(type);
        }
        if (StringUtils.isNotBlank(spId)) {
            param.append("&spId=").append(spId);
        }
        ResponseJSON = HttpClientUtil.doPost(poiCreateUrl, param.toString());
        return ResponseJSON;
    }

    /**
     * 分页查询 数据
     */
    public static JSONObject getPoiList(Integer page_index, Integer page_size, String title, String tags, String level, String type, String spId) {
        JSONObject ResponseJSON = null;
        StringBuilder param = new StringBuilder("ak=").append(LbsConfig.getAK()).append("&geotable_id=").append(geotableId).append("&coord_type=3");

        if (StringUtils.isNotBlank(title)) {
            param.append("&title=").append(title);
        }
        if (StringUtils.isNotBlank(tags)) {
            param.append("&tags=").append(tags);
        }
        if (page_index != null) {
            param.append("&page_index=").append(page_index);
        }
        if (page_size != null) {
            param.append("&page_size=").append(page_size);
        }
        if (level != null) {
//            param.append("&filter=").append("level:").append(level);
            param.append("&level=").append(level);
        }
        if (type != null) {
            param.append("&type$=").append(type);
        }
        if (spId != null) {
            param.append("&spId=").append(spId);
        }
        System.out.println("param=" + param);
        ResponseJSON = HttpClientUtil.doPost(poiSelectListUrl, param.toString());
        return ResponseJSON;
    }

    /**
     * LBS 根据LBS id查询数据
     *
     * @param id
     * @return
     */
    public static JSONObject getPoiById(String id) {
        JSONObject ResponseJSON = null;
        StringBuilder param = new StringBuilder("ak=").append(LbsConfig.getAK()).append("&geotable_id=").append(geotableId).append("&id=").append(id);
        System.out.println("param=" + param);
        ResponseJSON = HttpClientUtil.doPost(poiSelectByIdUrl, param.toString());
        return ResponseJSON;
    }

    public static JSONObject updatePoiById(String id, String title, String address, String tags, String latitude, String longitude, String level, String type, String spId) {
        JSONObject ResponseJSON = null;
        StringBuilder param = new StringBuilder("ak=").append(LbsConfig.getAK()).append("&geotable_id=").append(geotableId).append("&coord_type=3")
                .append("&latitude=").append(latitude).append("&longitude=").append(longitude).append("&id=").append(id);
        if (StringUtils.isNotBlank(address)) {
            param.append("&address=").append(address);
        }
        if (StringUtils.isNotBlank(title)) {
            param.append("&title=").append(title);
        }
        if (StringUtils.isNotBlank(tags)) {
            param.append("&tags=").append(tags);
        }
        if (StringUtils.isNotBlank(level)) {
            param.append("&level=").append(level);
        }
        if (StringUtils.isNotBlank(type)) {
            param.append("&type=").append(type);
        }
        if (StringUtils.isNotBlank(spId)) {
            param.append("&spId=").append(spId);
        }
        System.out.println("param=" + param);
        ResponseJSON = HttpClientUtil.doPost(poiUpdateUrl, param.toString());
        return ResponseJSON;
    }

    public static JSONObject deletePoiByIds(String ids) {
        JSONObject ResponseJSON = null;
        StringBuilder param = new StringBuilder("ak=").append(LbsConfig.getAK()).append("&geotable_id=").append(geotableId).append("&ids=").append(ids);
        System.out.println("param=" + param);
        ResponseJSON = HttpClientUtil.doPost(poiDeleteByIdUrl, param.toString());
        return ResponseJSON;


    }


    public static String getAK() {
        return AK;
    }

    public static void setAK(String AK) {
        LbsConfig.AK = AK;
    }

    public static String getGeotableId() {
        return geotableId;
    }

    public static void setGeotableId(String geotableId) {
        LbsConfig.geotableId = geotableId;
    }

//    public static void main(String[] args) {
//        String polygons = "104.0547,30.6787;104.067636,30.667394;104.065193,30.659193;104.059012,30.657454;104.052544,30.664785;";
//
//        JSONObject ResponseJSON = createPoi("104.0547", "30.6787", polygons, null,
//                "下级title", "下级tags", "1","1",null);
////        JSONObject ResponseJSON = getPoiList(null, null, null, null, null, "red$", null);
////        JSONObject ResponseJSON = deletePoiByIds("1001373274858355568,1001373730137482028,1001373856054670878");
//
//        System.out.printf("ResponseJSON =" + ResponseJSON);
//    }


}
