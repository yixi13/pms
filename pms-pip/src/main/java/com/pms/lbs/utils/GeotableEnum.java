package com.pms.lbs.utils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by 宏铭科技
 *
 * 创建的表的枚举方法
 * 
 * @auther ljb
 * @Date 2018/5/8.
 */
public enum GeotableEnum {
    w_pip("1000003845"),

    test("188732"); 

    /** 描述 */
    private String id;

    private GeotableEnum(String id) {
        this.id = id;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public static Map<String, Map<String, Object>> toMap() {
        GeotableEnum[] ary = GeotableEnum.values();
        Map<String, Map<String, Object>> enumMap = new HashMap<String, Map<String, Object>>();
        for (int num = 0; num < ary.length; num++) {
            Map<String, Object> map = new HashMap<String, Object>();
            String key = ary[num].name();
            map.put("id", ary[num].getId());
            enumMap.put(key, map);
        }
        return enumMap;
    }

    @SuppressWarnings({ "rawtypes", "unchecked" })
    public static List toList() {
        GeotableEnum[] ary = GeotableEnum.values();
        List list = new ArrayList();
        for (int i = 0; i < ary.length; i++) {
            Map<String, String> map = new HashMap<String, String>();
            map.put("id", ary[i].getId());
            map.put("name", ary[i].name());
            list.add(map);
        }
        return list;
    }

    public static GeotableEnum getEnum(String name) {
        GeotableEnum[] arry = GeotableEnum.values();
        for (int i = 0; i < arry.length; i++) {
            if (arry[i].name().equalsIgnoreCase(name)) {
                return arry[i];
            }
        }
        return null;
    }

    /**
     * 取枚举的json字符串
     *
     * @return
     */
    public static String getJsonStr() {
        GeotableEnum[] enums = GeotableEnum.values();
        StringBuffer jsonStr = new StringBuffer("[");
        for (GeotableEnum senum : enums) {
            if (!"[".equals(jsonStr.toString())) {
                jsonStr.append(",");
            }
            jsonStr.append("{id:'").append(senum).append("',id:'").append(senum.getId()).append("'}");
        }
        jsonStr.append("]");
        return jsonStr.toString();
    }
}
