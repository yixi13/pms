package com.pms.lbs.entity;

import java.io.Serializable;

/**
 * Created by 宏铭科技
 * 百度LBS 表实体类
 * @auther ljb
 * @Date 2018/5/14.
 */
public class Geotable implements Serializable {
    private static final long serialVersionUID = 1L;
    /**
     * geotable的id标识
     */
    private String id;
    /**
     * 位置数据表名称
     */
    private String name;
    /**
     * 用户id
     */
    private String user_id;
    /**
     *  是否发布到云检索；1（发布）、0（不发布）
     */
    private String is_published;
    private String create_time;
    private String modify_time;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public String getIs_published() {
        return is_published;
    }

    public void setIs_published(String is_published) {
        this.is_published = is_published;
    }

    public String getCreate_time() {
        return create_time;
    }

    public void setCreate_time(String create_time) {
        this.create_time = create_time;
    }

    public String getModify_time() {
        return modify_time;
    }

    public void setModify_time(String modify_time) {
        this.modify_time = modify_time;
    }
}
