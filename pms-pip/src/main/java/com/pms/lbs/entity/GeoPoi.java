package com.pms.lbs.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * Created by 宏铭科技
 *
 * 百度LBS 表数据 实体类
 * @auther ljb
 * @Date 2018/5/24.
 */
@Data
//自动生成无参数构造函数
@NoArgsConstructor
@AllArgsConstructor
public class GeoPoi implements Serializable {
    private static final long serialVersionUID = 1L;

    private String id;              // 主键id
    private String coord_type;          // 用户上传的坐标的类型：1、2、3、4
    private String geotable_id;         // 创建数据的对应数据表id
    private String ak;                   // 用户的访问权限key

    private String title;           // 位置数据名称
    private String address;         // 位置数据地址
    private String tags;            // 位置数据类别
    private double latitude;       // 用户上传的纬度  必选。非百度墨卡托坐标时，取值为[-90,90]
    private double longitude;       // 用户上传的经度 必选。非百度墨卡托坐标时，取值为[-180,180]
    /**
     * 可选
     * 若该字段传入数据，则判定为面状位置数据。
     * 格式为：经度,纬度;经度,纬度;经度,纬度
     * 单个多边形最多创建400个边界点，最大字符长度为10240。
     */
    private String polygons;       // 多边形边界点坐标数据
    /**
     * 1：GPS经纬度坐标 2：国测局加密经纬度坐标  3：百度加密经纬度坐标 4：百度加密墨卡托坐标
     */


    //////////////// 以下为自选字段
    private String type;           // 数据类别
    private String level;          // 数据级别
    private String spId;            // 上级id
}
