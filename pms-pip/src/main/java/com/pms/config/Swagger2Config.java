package com.pms.config;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.ParameterBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.schema.ModelRef;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Parameter;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import java.util.ArrayList;
import java.util.List;

/**
 * Swagger2配置
 *
 * @author zyl
 * @create 2017-06-09 16:45
 **/
@Configuration
@EnableSwagger2 // 启用swagger
public class Swagger2Config{

    public static final String SWAGGER_SCAN_BASE_PACKAGE = "com.pms.repair.web";
    public static final String SWAGGER_SCAN_API_PACKAGE = "com.pms.api";
    public static final String SWAGGER_SCAN_BASE_PACKAGE1 = "com.pms.partitionPoints.web";
    @Bean
    public Docket createRestApi() {
        return new Docket(DocumentationType.SWAGGER_2)
                .groupName("groupApi")
                .apiInfo(apiInfo())
                .select()
                .apis(RequestHandlerSelectors.basePackage("com.pms"))
                .paths(PathSelectors.any())
                .build()
                .globalOperationParameters(setHeader());
//                .pathMapping("/com.pms.api/mpi/estate");
    }
    private ApiInfo apiInfo() {
        return new ApiInfoBuilder()
                .title("pms-pip API接口")
                .description("宏铭科技：http://www.schmkj.cn")
                .termsOfServiceUrl("http://www.schmkj.cn")
                .contact("宏铭科技")
                .version("2.0")
                .build();
    }
    private ApiInfo webInfo() {
        return new ApiInfoBuilder()
                .title("pms-pip WEB")
                .description("宏铭科技：http://www.schmkj.cn")
                .termsOfServiceUrl("http://www.schmkj.cn")
                .contact("宏铭科技")
                .version("2.0")
                .build();
    }
    @Bean
    public Docket createRestWeb() {
        return new Docket(DocumentationType.SWAGGER_2)
                .groupName("groupBase")
                .apiInfo(webInfo())
                .select()
                .apis(RequestHandlerSelectors.basePackage(SWAGGER_SCAN_BASE_PACKAGE))
                .paths(PathSelectors.any())
                .build()
                .globalOperationParameters(setHeader()); // 在这里可以设置请求的统一前缀
    }

    @Bean
    public Docket createRestWeb1() {
        return new Docket(DocumentationType.SWAGGER_2)
                .groupName("groupBase1")
                .apiInfo(webInfo())
                .select()
                .apis(RequestHandlerSelectors.basePackage(SWAGGER_SCAN_BASE_PACKAGE1))
                .paths(PathSelectors.any())
                .build()
                .globalOperationParameters(setHeader()); // 在这里可以设置请求的统一前缀
    }

    private List<Parameter> setHeader() {
        ParameterBuilder tokenPar = new ParameterBuilder();
        List<Parameter> pars = new ArrayList<>();
        tokenPar.name("X-Token").description("token").modelRef(new ModelRef("string")).parameterType("header").required(false).build();
        pars.add(tokenPar.build());
        tokenPar.name("X-Name").description("token").modelRef(new ModelRef("string")).parameterType("header").required(false).build();
        pars.add(tokenPar.build());
        tokenPar.name("X-Code").description("token").modelRef(new ModelRef("string")).parameterType("header").required(false).build();
        pars.add(tokenPar.build());
        tokenPar.name("X-groupId").description("token").modelRef(new ModelRef("string")).parameterType("header").required(false).build();
        pars.add(tokenPar.build());
        tokenPar.name("X-groupType").description("token").modelRef(new ModelRef("string")).parameterType("header").required(false).build();
        pars.add(tokenPar.build());
        return pars;
    }
}
