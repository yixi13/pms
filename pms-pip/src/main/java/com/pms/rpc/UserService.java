package com.pms.rpc;

import com.pms.authority.PermissionInfo;
import com.pms.entity.BaseUserInfo;
import com.pms.entity.UserInfo;
import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

/**
 * Created by Administrator on 2017/12/28 0028.
 */
@FeignClient(name = "pms-system")
@RequestMapping("api")
public interface UserService {
    @RequestMapping(value = "/user/username/{username}", method = RequestMethod.GET)
    UserInfo getUserByUsername(@PathVariable("username") String username);

    @RequestMapping(value = "/user/un/{username}/permissions", method = RequestMethod.GET)
    List<PermissionInfo> getPermissionByUsername(@PathVariable("username") String username);
    @RequestMapping(value = "/permissions", method = RequestMethod.GET)
    List<PermissionInfo> getAllPermissionInfo();

    @RequestMapping(value = "/user/seleteByuser/{username}",method = RequestMethod.GET, produces="application/json")
    public BaseUserInfo seleteByuser(@PathVariable("username")String username);

    @RequestMapping(value = "/user/getSingleuserInfo/{id}", method = RequestMethod.GET, produces = "application/json")
     UserInfo getSingleuserInfo(@PathVariable("id") Long id);

    @RequestMapping(value = "/user/seleteById/{id}",method = RequestMethod.GET, produces="application/json")
    public  BaseUserInfo getUserByUserId(@PathVariable("id")Long id);

    @RequestMapping(value = "/user/selectbyPipUser/{id}",method = RequestMethod.GET, produces="application/json")
    public @ResponseBody
    UserInfo selectbyPipUser(@PathVariable("id")Long id);

    @RequestMapping(value = "/user/un/getApiPermission/{userId}/{menuType}/api", method = RequestMethod.GET)
    Map<String,Object> getApiPermission(@PathVariable("userId") Long userId, @PathVariable("menuType")Integer menuType, @RequestParam("title")String title, @RequestParam("permissionsType") Integer permissionsType, @RequestParam("method")String method);

    @RequestMapping(value = "/user/username/{username}/{companyCode}", method = RequestMethod.GET, produces = "application/json")
    public @ResponseBody
    UserInfo getUserByUsername(@PathVariable("username") String username,@PathVariable("companyCode")String companyCode);
}
