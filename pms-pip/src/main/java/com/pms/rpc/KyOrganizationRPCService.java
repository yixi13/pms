package com.pms.rpc;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;
import com.pms.partitionPoints.entity.KyGroupOrganization;
import com.pms.partitionPoints.entity.KyOrganization;
import com.pms.partitionPoints.entity.KyOrganizationEquipment;
import com.pms.partitionPoints.mapper.KyOrganizationMapper;
import com.pms.partitionPoints.service.IKyGroupOrganizationService;
import com.pms.partitionPoints.service.IKyOrganizationEquipmentService;
import com.pms.partitionPoints.service.IKyOrganizationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import java.util.*;

/**
 * Created by Administrator on 2018/8/16 0016.
 */
@RestController
@RequestMapping("api")
public class KyOrganizationRPCService {

    @Autowired
    IKyOrganizationService kyOrganizationService;
    @Autowired
    IKyGroupOrganizationService kyGroupOrganizationService;
    @Autowired
    IKyOrganizationEquipmentService kyOrganizationEquipmentService;
    @Autowired
    KyOrganizationMapper kyOrganizationMapper;

    @RequestMapping(value = "/kyOrganization/add",method = RequestMethod.POST, produces="application/json")
    public void addLoginLog(@RequestParam("code")String code, @RequestParam("name")String name){
        KyOrganization ko = new KyOrganization();
        ko.setCreatetime(new Date());
        ko.setCode(code);
        ko.setName(name);
        ko.setOrganizationgradeid(1);
        ko.setStatus(1);
        ko.setParentid(0);
        ko.setType(1);
//        kyOrganizationService.insert(ko);
        kyOrganizationService.insertOrgMax(ko);
    }



    @RequestMapping(value = "/deleteGroupOrganization/{groupId}",method = RequestMethod.POST, produces="application/json")
    public boolean deleteGroupOrganization(@PathVariable("groupId")Long groupId){
        kyGroupOrganizationService.delete(new EntityWrapper<KyGroupOrganization>().eq("group_id",groupId));
        return  true;
    }

    @RequestMapping(value = "/getGroupOrganizationList/{groupId}",method = RequestMethod.POST, produces="application/json")
    public  List<Map<String,Object>> getGroupOrganizationList(@PathVariable("groupId")Long groupId){
       List<Map<String,Object>> list=kyGroupOrganizationService.selectMaps(new EntityWrapper<KyGroupOrganization>().eq("group_id",groupId));
       return  list;
    }

    @HystrixCommand()
    @RequestMapping(value = "/kyOrganization/getOrgMeasure",method = RequestMethod.POST, produces="application/json")
    public  List<Map<String,Object>> getOrgMeasure(Integer orgMaxId,Integer orgId,Date startDate,Date endDate){
        List<Map<String,Object>> list = kyOrganizationEquipmentService.getOrgWaterDayByEquIdsAdTime(orgMaxId,orgId,startDate,endDate);//产销差，漏损量，漏损率
        return  list;
    }

    @RequestMapping(value = "/kyOrganization/getEquipmentMeasure",method = RequestMethod.POST, produces="application/json")
    public  Set<Map<String,Object>> getEquipmentMeasure(Integer orgMaxId,String equIds,Date startDate,Date endDate,String param){
        String[] idList = equIds.split(",");
        List<Integer> list = new ArrayList<>();
        for(int i=0;i<idList.length;i++){
            Integer id = Integer.parseInt(idList[i]);
            list.add(id);
        }
        Set<Map<String,Object>> set = kyOrganizationEquipmentService.getEquipmentDateByIdListAndTime(orgMaxId,list,startDate,endDate,param);
        return  set;
    }


    @HystrixCommand
    @RequestMapping(value = "/deleteSystemDataByCode/{code}",method = RequestMethod.POST, produces="application/json")
    public void deleteSystemDataByCode(@PathVariable("code")String code){
        kyOrganizationService.deleteSystemByCode(code);
    }

    @RequestMapping(value = "/kyOrganization/updateByCode",method = RequestMethod.POST, produces="application/json")
    public void updateByCode(@RequestParam("code")String code, @RequestParam("name")String name){
        KyOrganization ko = kyOrganizationService.selectOne(new EntityWrapper<KyOrganization>().eq("code",code));
        ko.setName(name);
        kyOrganizationService.updateById(ko);
    }
}
