package com.pms.rpc;

import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.Map;

/**
 * Created by Administrator on 2017/12/28 0028.
 */
@FeignClient("pms-system")
@RequestMapping("com/pms/api")
public interface AreaCnService {

    @RequestMapping(value = "/areaCn/county/{countyGb}")
    public   Map<String,Object> getcounty(@PathVariable("countyGb") Integer countyGb);
}
