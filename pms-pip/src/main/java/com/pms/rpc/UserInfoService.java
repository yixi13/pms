package com.pms.rpc;

import com.pms.entity.UserInfo;
import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;


/**
 * Created by hm on 2018/1/15.
 */

@FeignClient("pms-gateway")
@RequestMapping("gateway")
public interface UserInfoService {

    @RequestMapping(value = "/user/getCurrentUser", method = RequestMethod.GET,produces="application/json")
    public UserInfo getCurrentUser();
}
