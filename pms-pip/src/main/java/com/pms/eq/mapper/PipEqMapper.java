package com.pms.eq.mapper;

import com.pms.eq.entity.PipEq;
import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
  * 设备表 Mapper 接口
 * </p>
 *
 * @author ljb
 * @since 2018-05-28
 */
public interface PipEqMapper extends BaseMapper<PipEq> {

}