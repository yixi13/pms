package com.pms.eq.mapper;

import com.pms.eq.entity.PipEqArea;
import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
  * 设备区域表 Mapper 接口
 * </p>
 *
 * @author ljb
 * @since 2018-05-28
 */
public interface PipEqAreaMapper extends BaseMapper<PipEqArea> {

}