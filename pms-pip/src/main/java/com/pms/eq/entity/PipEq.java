package com.pms.eq.entity;

import java.util.Date;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableName;
import java.io.Serializable;

/**
 * <p>
 * 设备表
 * </p>
 *
 * @author ljb
 * @since 2018-05-29
 */
@TableName("pip_eq")
public class PipEq extends Model<PipEq> {

    private static final long serialVersionUID = 1L;

    /**
     * 设备表主键
     */
    @TableId("pip_eq_id")
	private Long pipEqId;
    /**
     * 设备名称
     */
	private String name;
    /**
     * 机构id
     */
	@TableField("agency_id")
	private Long agencyId;
    /**
     * 状态(1：正常；2：禁用；3：删除)
     */
	private Integer status;
    /**
     * 所在区域id
     */
	@TableField("pip_eq_area_id")
	private Long pipEqAreaId;
    /**
     * 级别,几级测点,前端显示不同颜色
     */
	private Integer level;
    /**
     * 数据采集类别
     */
	@TableField("data_coll_type")
	private Long dataCollType;
    /**
     * 设备类型id
     */
	@TableField("eq_type_id")
	private Long eqTypeId;
    /**
     * 设备地址
     */
	@TableField("eq_address")
	private String eqAddress;
    /**
     * 设备标识
     */
	@TableField("eq_mark")
	private String eqMark;
    /**
     * 设备安装时间
     */
	@TableField("eq_install_time")
	private Date eqInstallTime;
    /**
     * 备注
     */
	private String description;
    /**
     * 精度
     */
	private String longitude;
    /**
     * 纬度
     */
	private String latitude;
    /**
     * 百度lbs id
     */
	@TableField("lbs_id")
	private Long lbsId;
    /**
     * 创建人id
     */
	@TableField("create_by")
	private Long createBy;
    /**
     * 创建时间
     */
	@TableField("create_time")
	private Date createTime;
    /**
     * 修改人id
     */
	@TableField("update_by")
	private Long updateBy;
    /**
     * 修改时间
     */
	@TableField("update_time")
	private Date updateTime;
    /**
     * 负责人id
     */
	@TableField("manager_id")
	private Long managerId;
    /**
     * 通讯状态（1：在线，2：离线）
     */
	@TableField("communication_status")
	private Integer communicationStatus;
    /**
     * 设备状态（1：在线，2：离线）
     */
	@TableField("eq_status")
	private Integer eqStatus;
    /**
     * 流动方向(1表示流入 0 表示流出)
     */
	private Integer flow;
    /**
     * 串口通讯状态（0：正常，1：警告）
     */
	@TableField("serial_communication_state")
	private Integer serialCommunicationState;
    /**
     * 箱门开关（0：正常，1：警告）
     */
	@TableField("door_switch")
	private Integer doorSwitch;
    /**
     * 压力故障（0：正常，1：警告）
     */
	private Integer pressure;
    /**
     * 电池电压（0：正常，1：警告）
     */
	@TableField("battery_voltage")
	private Integer batteryVoltage;
    /**
     * 水位（0：正常，1：警告）
     */
	@TableField("water_level")
	private Integer waterLevel;


	public Long getPipEqId() {
		return pipEqId;
	}

	public void setPipEqId(Long pipEqId) {
		this.pipEqId = pipEqId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Long getAgencyId() {
		return agencyId;
	}

	public void setAgencyId(Long agencyId) {
		this.agencyId = agencyId;
	}

	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	public Long getPipEqAreaId() {
		return pipEqAreaId;
	}

	public void setPipEqAreaId(Long pipEqAreaId) {
		this.pipEqAreaId = pipEqAreaId;
	}

	public Integer getLevel() {
		return level;
	}

	public void setLevel(Integer level) {
		this.level = level;
	}

	public Long getDataCollType() {
		return dataCollType;
	}

	public void setDataCollType(Long dataCollType) {
		this.dataCollType = dataCollType;
	}

	public Long getEqTypeId() {
		return eqTypeId;
	}

	public void setEqTypeId(Long eqTypeId) {
		this.eqTypeId = eqTypeId;
	}

	public String getEqAddress() {
		return eqAddress;
	}

	public void setEqAddress(String eqAddress) {
		this.eqAddress = eqAddress;
	}

	public String getEqMark() {
		return eqMark;
	}

	public void setEqMark(String eqMark) {
		this.eqMark = eqMark;
	}

	public Date getEqInstallTime() {
		return eqInstallTime;
	}

	public void setEqInstallTime(Date eqInstallTime) {
		this.eqInstallTime = eqInstallTime;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getLongitude() {
		return longitude;
	}

	public void setLongitude(String longitude) {
		this.longitude = longitude;
	}

	public String getLatitude() {
		return latitude;
	}

	public void setLatitude(String latitude) {
		this.latitude = latitude;
	}

	public Long getLbsId() {
		return lbsId;
	}

	public void setLbsId(Long lbsId) {
		this.lbsId = lbsId;
	}

	public Long getCreateBy() {
		return createBy;
	}

	public void setCreateBy(Long createBy) {
		this.createBy = createBy;
	}

	public Date getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	public Long getUpdateBy() {
		return updateBy;
	}

	public void setUpdateBy(Long updateBy) {
		this.updateBy = updateBy;
	}

	public Date getUpdateTime() {
		return updateTime;
	}

	public void setUpdateTime(Date updateTime) {
		this.updateTime = updateTime;
	}

	public Long getManagerId() {
		return managerId;
	}

	public void setManagerId(Long managerId) {
		this.managerId = managerId;
	}

	public Integer getCommunicationStatus() {
		return communicationStatus;
	}

	public void setCommunicationStatus(Integer communicationStatus) {
		this.communicationStatus = communicationStatus;
	}

	public Integer getEqStatus() {
		return eqStatus;
	}

	public void setEqStatus(Integer eqStatus) {
		this.eqStatus = eqStatus;
	}

	public Integer getFlow() {
		return flow;
	}

	public void setFlow(Integer flow) {
		this.flow = flow;
	}

	public Integer getSerialCommunicationState() {
		return serialCommunicationState;
	}

	public void setSerialCommunicationState(Integer serialCommunicationState) {
		this.serialCommunicationState = serialCommunicationState;
	}

	public Integer getDoorSwitch() {
		return doorSwitch;
	}

	public void setDoorSwitch(Integer doorSwitch) {
		this.doorSwitch = doorSwitch;
	}

	public Integer getPressure() {
		return pressure;
	}

	public void setPressure(Integer pressure) {
		this.pressure = pressure;
	}

	public Integer getBatteryVoltage() {
		return batteryVoltage;
	}

	public void setBatteryVoltage(Integer batteryVoltage) {
		this.batteryVoltage = batteryVoltage;
	}

	public Integer getWaterLevel() {
		return waterLevel;
	}

	public void setWaterLevel(Integer waterLevel) {
		this.waterLevel = waterLevel;
	}

	@Override
	protected Serializable pkVal() {
		return this.pipEqId;
	}

}
