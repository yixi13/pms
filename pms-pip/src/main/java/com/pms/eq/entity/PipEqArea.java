package com.pms.eq.entity;

import java.util.Date;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableName;
import java.io.Serializable;

/**
 * <p>
 * 设备区域表
 * </p>
 *
 * @author ljb
 * @since 2018-05-29
 */
@TableName("pip_eq_area")
public class PipEqArea extends Model<PipEqArea> {

    private static final long serialVersionUID = 1L;

    /**
     * 设备区域表主键id
     */
    @TableId("pip_eq_area_id")
	private Long pipEqAreaId;
    /**
     * 区域名称
     */
	private String name;
    /**
     * 机构id
     */
	@TableField("agency_id")
	private Long agencyId;
    /**
     * 区域级别
     */
	private Integer level;
    /**
     * 经度
     */
	private String latitude;
    /**
     * 纬度
     */
	private String longitude;
    /**
     * LBS 多边形点数据
     */
	private String polygons;
    /**
     * LBS 主键id
     */
	@TableField("lbs_id")
	private Long lbsId;
	@TableField("create_by")
	private Long createBy;
	@TableField("create_time")
	private Date createTime;


	public Long getPipEqAreaId() {
		return pipEqAreaId;
	}

	public void setPipEqAreaId(Long pipEqAreaId) {
		this.pipEqAreaId = pipEqAreaId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Long getAgencyId() {
		return agencyId;
	}

	public void setAgencyId(Long agencyId) {
		this.agencyId = agencyId;
	}

	public Integer getLevel() {
		return level;
	}

	public void setLevel(Integer level) {
		this.level = level;
	}

	public String getLatitude() {
		return latitude;
	}

	public void setLatitude(String latitude) {
		this.latitude = latitude;
	}

	public String getLongitude() {
		return longitude;
	}

	public void setLongitude(String longitude) {
		this.longitude = longitude;
	}

	public String getPolygons() {
		return polygons;
	}

	public void setPolygons(String polygons) {
		this.polygons = polygons;
	}

	public Long getLbsId() {
		return lbsId;
	}

	public void setLbsId(Long lbsId) {
		this.lbsId = lbsId;
	}

	public Long getCreateBy() {
		return createBy;
	}

	public void setCreateBy(Long createBy) {
		this.createBy = createBy;
	}

	public Date getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	@Override
	protected Serializable pkVal() {
		return this.pipEqAreaId;
	}

}
