package com.pms.eq.service;

import com.pms.eq.entity.PipEqArea;
import com.baomidou.mybatisplus.service.IService;

/**
 * <p>
 * 设备区域表 服务类
 * </p>
 *
 * @author ljb
 * @since 2018-05-28
 */
public interface IPipEqAreaService extends IService<PipEqArea> {

    boolean insertAndLbs(PipEqArea pipEqArea, String pipEqIds);
}
