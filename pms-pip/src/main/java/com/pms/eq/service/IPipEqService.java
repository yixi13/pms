package com.pms.eq.service;

import com.pms.eq.entity.PipEq;
import com.baomidou.mybatisplus.service.IService;
import org.springframework.transaction.annotation.Transactional;

/**
 * <p>
 * 设备表 服务类
 * </p>
 *
 * @author ljb
 * @since 2018-05-28
 */
public interface IPipEqService extends IService<PipEq> {
    /**
     * 数据坐标插入 百度LBS 并保存
     * @param pipEq
     * @return
     */
    @Transactional
    boolean insertAndLbs(PipEq pipEq);

    @Transactional
    boolean updateAndLbs(PipEq pipEq);

    @Transactional
    boolean deleteAndLbs(Long id);

}
