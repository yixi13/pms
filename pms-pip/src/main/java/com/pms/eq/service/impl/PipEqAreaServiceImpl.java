package com.pms.eq.service.impl;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.pms.eq.entity.PipEq;
import com.pms.eq.entity.PipEqArea;
import com.pms.eq.mapper.PipEqAreaMapper;
import com.pms.eq.service.IPipEqAreaService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.pms.eq.service.IPipEqService;
import com.pms.lbs.service.ILbsService;
import com.pms.util.idutil.IdWorker;
import com.pms.util.idutil.SnowFlake;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

/**
 * <p>
 * 设备区域表 服务实现类
 * </p>
 *
 * @author ljb
 * @since 2018-05-28
 */
@Service
public class PipEqAreaServiceImpl extends ServiceImpl<PipEqAreaMapper, PipEqArea> implements IPipEqAreaService {

    @Autowired
    IPipEqService iPipEqService;
    @Autowired
    ILbsService iLbsService;


    @Override
    @Transactional
    public boolean insertAndLbs(PipEqArea pipEqArea, String pipEqIds) {
        List<PipEq> pipEqs = new ArrayList<>();
        String [] ids = pipEqIds.split(",");
        Long pipEqAreaId = new SnowFlake(0,0).nextId();
        pipEqArea.setPipEqAreaId(pipEqAreaId);

        for (int i=0; i<ids.length ; i++) {
            String pipEqId = ids[i];
            PipEq pipEq = iPipEqService.selectById(pipEqId);
            if (pipEq == null) {
                continue;
            }
            pipEq.setPipEqAreaId(pipEqAreaId);
            pipEqs.add(pipEq);
        }
        // 插入百度lbs
        Long lbsId =  iLbsService.createPoi(pipEqArea.getLongitude(), pipEqArea.getLatitude(), pipEqArea.getPolygons(), null,
                pipEqArea.getName(),null,(pipEqArea.getLevel() == null ? null : pipEqArea.getLevel().toString()) , null, null);
        pipEqArea.setLbsId(lbsId);

        this.insert(pipEqArea);
        //批量修改所选点  属于这个新添加的面
        iPipEqService.updateBatchById(pipEqs);
        return false;
    }

}
