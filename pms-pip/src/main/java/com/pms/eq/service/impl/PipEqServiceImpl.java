package com.pms.eq.service.impl;

import com.pms.eq.entity.PipEq;
import com.pms.eq.mapper.PipEqMapper;
import com.pms.eq.service.IPipEqService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.pms.lbs.service.ILbsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;

/**
 * <p>
 * 设备表 服务实现类
 * </p>
 *
 * @author ljb
 * @since 2018-05-28
 */
@Service
public class PipEqServiceImpl extends ServiceImpl<PipEqMapper, PipEq> implements IPipEqService {

    @Autowired
    ILbsService iLbsService;

    @Override
    @Transactional
    public boolean insertAndLbs(PipEq pipEq) {
        Long lbsId =  iLbsService.createPoi(pipEq.getLongitude(), pipEq.getLatitude(), null, pipEq.getEqAddress(),
                pipEq.getName(),null,(pipEq.getLevel() == null ? null : pipEq.getLevel().toString()) , null, null);
        pipEq.setLbsId(lbsId);
        return insert(pipEq);
    }

    @Override
    @Transactional
    public boolean updateAndLbs(PipEq pipEq) {
        if (pipEq.getLbsId() == null) {
            Long lbsId =  iLbsService.createPoi(pipEq.getLongitude(), pipEq.getLatitude(), null, pipEq.getEqAddress(),
                    pipEq.getName(),null,(pipEq.getLevel() == null ? null : pipEq.getLevel().toString()) , null, null);
            pipEq.setLbsId(lbsId);
        }else {
            iLbsService.updatePoiById(pipEq.getLbsId().toString(),pipEq.getName(),pipEq.getEqAddress(),null,pipEq.getLatitude(),
                    pipEq.getLongitude(), (pipEq.getLevel() == null ? null : pipEq.getLevel().toString()),null,null);
        }
        return updateById(pipEq);
    }

    @Override
    @Transactional
    public boolean deleteAndLbs(Long id) {
        PipEq pipEq = this.selectById(id);
        if (pipEq != null) {
            if (pipEq.getLbsId() != null) {
                iLbsService.deletePoiByIds(pipEq.getLbsId().toString());
            }
            return deleteById(id);
        }
        return false;
    }
}
