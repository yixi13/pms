package com.pms.eq.web;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.pms.entity.UserInfo;
import com.pms.eq.entity.PipEq;
import com.pms.eq.entity.PipEqArea;
import com.pms.eq.service.IPipEqAreaService;
import com.pms.eq.service.IPipEqService;
import com.pms.exception.R;
import com.pms.rpc.UserService;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import com.pms.controller.BaseController;

import java.util.Date;
import java.util.List;

/**
 * <p>
 * 设备区域表 前端控制器
 * </p>
 *
 * @author ljb
 * @since 2018-05-28
 */
@Controller
@RequestMapping("/eq/pipEqArea")
public class PipEqAreaController extends BaseController {
    @Autowired
    private IPipEqAreaService iPipEqAreaService;
    @Autowired
    private UserService userService;

    @RequestMapping(value = "",method = RequestMethod.POST)
    @ApiOperation(value = "新增区域")
    public R save(@RequestBody PipEqArea pipEqArea ,String pipEqIds){
        parameterIsBlank(pipEqArea.getName(),"区域名称不能为空");
        parameterIsNull(pipEqArea.getAgencyId(),"所属机构不能为空");

        parameterIsBlank(pipEqIds,"选点不能为空");

        String userName = getCurrentUserName();
        UserInfo user =  userService.getUserByUsername(userName);
        pipEqArea.setCreateBy(Long.valueOf(user.getId()));
        pipEqArea.setCreateTime(new Date());

        boolean a = iPipEqAreaService.insertAndLbs(pipEqArea,pipEqIds);
        return R.ok();
    }
    @GetMapping("/list")
    @ApiOperation(value = "分页查询设备列表")
    public R list(Integer current, Integer size) {
        parameterIsNull(current,"current 不能为空");
        parameterIsNull(size,"size 不能为空");
        EntityWrapper<PipEqArea> ew = new  EntityWrapper<PipEqArea>();
        ew.orderBy("create_time desc");
        Page<PipEqArea> page = new Page(current, size);
        page = iPipEqAreaService.selectPage(page, ew);
        return R.ok().put("page",page);
    }

    @RequestMapping("/update")
    @ApiOperation(value = "修改设备信息")
//    @RequiresPermissions("pms:eq:update")
    public R update(@RequestBody PipEqArea pipEqArea){
        iPipEqAreaService.updateById(pipEqArea);
        return R.ok();
    }

    @RequestMapping(value = "/batch",method = RequestMethod.DELETE)
    @ApiOperation(value = "删除设备信息")
//    @RequiresPermissions("pms:eq:delete")
    public R delete(@RequestBody List Ids){
        iPipEqAreaService.deleteBatchIds(Ids);
        return R.ok();
    }

    /**
     * 删除
     * @param id
     * @return
     */
    @RequestMapping(value = "/{id}",method = RequestMethod.DELETE)
    //    @RequiresPermissions("pms:eq:delete")
    public R delete(@PathVariable Long id){
        iPipEqAreaService.deleteById (id);
        return R.ok();
    }
}
