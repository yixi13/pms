package com.pms.eq.web;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.pms.entity.UserInfo;
import com.pms.eq.entity.PipEq;
import com.pms.eq.service.IPipEqService;
import com.pms.exception.R;
import com.pms.result.ObjectRestResponse;
import com.pms.rpc.UserService;
import com.pms.utils.BeanUtil;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import com.pms.controller.BaseController;

import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * <p>
 * 设备表 前端控制器
 * </p>
 *
 * @author ljb
 * @since 2018-05-28
 */
@RestController
@Api(value = "设备信息",description = "设备信息")
@RequestMapping("/eq/pipEq")
public class PipEqController extends BaseController {

    @Autowired
    private IPipEqService iPipEqService;
    @Autowired
    private UserService userService;

    @RequestMapping(value = "",method = RequestMethod.POST)
    @ApiOperation(value = "新增设备")
    public R save(@RequestBody PipEq pipEq){
        parameterIsBlank(pipEq.getName(),"设备名称不能为空");
        parameterIsNull(pipEq.getAgencyId(),"所属机构不能为空");
        parameterIsBlank(pipEq.getLongitude(),"精度不能为空");
        parameterIsBlank(pipEq.getLatitude(),"纬度不能为空");
        parameterIsBlank(pipEq.getEqAddress(),"地址不能为空");

        String userName = getCurrentUserName();
        UserInfo user =  userService.getUserByUsername(userName);
        pipEq.setCreateBy(Long.valueOf(user.getId()));
        pipEq.setCreateTime( new Date());
        iPipEqService.insertAndLbs(pipEq);
        return R.ok();
    }
    @GetMapping("/list")
    @ApiOperation(value = "分页查询设备列表")
    public R list(Integer current, Integer size) {
        parameterIsNull(current,"current 不能为空");
        parameterIsNull(size,"size 不能为空");
        EntityWrapper<PipEq> ew = new  EntityWrapper<PipEq>();
        ew.orderBy("create_time desc");
        Page<PipEq> page = new Page(current, size);
        page = iPipEqService.selectPage(page, ew);
        return R.ok().put("page",page);
    }

    @RequestMapping(value = "",method = RequestMethod.PUT)
    @ApiOperation(value = "修改设备信息")
//    @RequiresPermissions("pms:eq:update")
    public R update(@RequestBody PipEq pipEq){
        parameterIsNull(pipEq.getPipEqId(),"pipEqId不能为空");

        String userName = getCurrentUserName();
        UserInfo user =  userService.getUserByUsername(userName);
        PipEq pipOld = iPipEqService.selectById(pipEq.getPipEqId());
        pipOld.setUpdateBy(Long.valueOf(user.getId()));
        pipOld.setUpdateTime( new Date());

        BeanUtil.mergeObject(pipEq,pipOld);
        iPipEqService.updateAndLbs(pipOld);
        return R.ok();
    }

    /**
     * 详情
     * @param id
     * @return
     */
    @RequestMapping(value = "/{id}",method = RequestMethod.GET)
    //    @RequiresPermissions("pms:eq:delete")
    public R detail(@PathVariable Long id){
        PipEq eq =  iPipEqService.selectById (id);
        return R.ok().put("pipEq",eq);
    }

    /**
     * 删除
     * @param id
     * @return
     */
    @RequestMapping(value = "/{id}",method = RequestMethod.DELETE)
    //    @RequiresPermissions("pms:eq:delete")
    public R delete(@PathVariable Long id){
        iPipEqService.deleteAndLbs(id);
        return R.ok();
    }

    @RequestMapping(value = "/batch",method = RequestMethod.DELETE)
    @ApiOperation(value = "删除设备信息")
//    @RequiresPermissions("pms:eq:delete")
    public R delete(@RequestBody List Ids){
        iPipEqService.deleteBatchIds(Ids);
        return R.ok();
    }

}
