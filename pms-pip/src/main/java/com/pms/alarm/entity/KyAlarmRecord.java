package com.pms.alarm.entity;

import com.baomidou.mybatisplus.enums.IdType;
import java.math.BigDecimal;
import java.util.Date;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableName;
import java.io.Serializable;

/**
 * <p>
 * 设备上传数据
 * </p>
 *
 * @author ljb
 * @since 2018-07-16
 */
@TableName("ky_alarm_record")
public class KyAlarmRecord extends Model<KyAlarmRecord> {

    private static final long serialVersionUID = 1L;

    /**
     * 报警记录
     */
	@TableId(value="id", type= IdType.AUTO)
	private Long id;
    /**
     * 设备id
     */
	private Integer equipmentId;
    /**
     * 记录时间
     */
	private Date creatTime;
    /**
     * 采集时间
     */
	private Date acquisitionTime;
    /**
     * 设备状态(0：异常，1：正常)
     */
	private Integer equipmentStatus;
    /**
     * 通讯状态(0：异常，1：正常)
     */
	private Integer communicationStatus;
    /**
     * 变化类型
     */
	private String changeType;
    /**
     * 变化描述
     */
	private String changeDescription;
    /**
     * 数据来源
     */
	private String dataSources;
    /**
     * 净累计流量
     */
	private BigDecimal netCumulativeFlow;
    /**
     * 正累计流量
     */
	private BigDecimal positiveCumulativeFlow;
    /**
     * 负累计流量
     */
	private BigDecimal negativeAccumulatedFlow;
    /**
     * 瞬时流量
     */
	private BigDecimal instantaneousFlow;
    /**
     * 电源电压
     */
	private BigDecimal supplyVoltage;
    /**
     * GPRS信号强度
     */
	private BigDecimal gprsSignalIntensity;
    /**
     * 压力
     */
	private BigDecimal pressure;
    /**
     * 水位
     */
	private BigDecimal waterLevel;
    /**
     * 电池电压报警
     */
	private String batteryVoltageAlarm;
    /**
     * 串口通讯状态
     */
	private String serialCommunicationState;
    /**
     * 箱门开关
     */
	private String doorSwitch;
    /**
     * 压力故障
     */
	private String pressureFault;
    /**
     * 压力上限报警
     */
	private String higherPressureAlarm;
    /**
     * 压力下限报警
     */
	private String lowerPressureAlarm;
    /**
     * 电池电压低报警
     */
	private String batteryVoltageLowAlarm;
    /**
     * 电池电压过低报警
     */
	private String batteryVoltageTooLowAlarm;
    /**
     * 压力状态
     */
	private String pressureStatus;
    /**
     * 电源电压状态
     */
	private String powerSupplyVoltage;
    /**
     * 水位故障
     */
	private String waterLevelFault;
    /**
     * 水位上限报警
     */
	private String waterUpLevelAlarm;
    /**
     * 水位下限报警
     */
	private String waterDownLevelAlarm;
    /**
     * 水位状态
     */
	private String waterLevelStatus;
    /**
     * 是否已读
     */
	private Integer isReade;


	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Integer getEquipmentId() {
		return equipmentId;
	}

	public void setEquipmentId(Integer equipmentId) {
		this.equipmentId = equipmentId;
	}

	public Date getCreatTime() {
		return creatTime;
	}

	public void setCreatTime(Date creatTime) {
		this.creatTime = creatTime;
	}

	public Date getAcquisitionTime() {
		return acquisitionTime;
	}

	public void setAcquisitionTime(Date acquisitionTime) {
		this.acquisitionTime = acquisitionTime;
	}

	public Integer getEquipmentStatus() {
		return equipmentStatus;
	}

	public void setEquipmentStatus(Integer equipmentStatus) {
		this.equipmentStatus = equipmentStatus;
	}

	public Integer getCommunicationStatus() {
		return communicationStatus;
	}

	public void setCommunicationStatus(Integer communicationStatus) {
		this.communicationStatus = communicationStatus;
	}

	public String getChangeType() {
		return changeType;
	}

	public void setChangeType(String changeType) {
		this.changeType = changeType;
	}

	public String getChangeDescription() {
		return changeDescription;
	}

	public void setChangeDescription(String changeDescription) {
		this.changeDescription = changeDescription;
	}

	public String getDataSources() {
		return dataSources;
	}

	public void setDataSources(String dataSources) {
		this.dataSources = dataSources;
	}

	public BigDecimal getNetCumulativeFlow() {
		return netCumulativeFlow;
	}

	public void setNetCumulativeFlow(BigDecimal netCumulativeFlow) {
		this.netCumulativeFlow = netCumulativeFlow;
	}

	public BigDecimal getPositiveCumulativeFlow() {
		return positiveCumulativeFlow;
	}

	public void setPositiveCumulativeFlow(BigDecimal positiveCumulativeFlow) {
		this.positiveCumulativeFlow = positiveCumulativeFlow;
	}

	public BigDecimal getNegativeAccumulatedFlow() {
		return negativeAccumulatedFlow;
	}

	public void setNegativeAccumulatedFlow(BigDecimal negativeAccumulatedFlow) {
		this.negativeAccumulatedFlow = negativeAccumulatedFlow;
	}

	public BigDecimal getInstantaneousFlow() {
		return instantaneousFlow;
	}

	public void setInstantaneousFlow(BigDecimal instantaneousFlow) {
		this.instantaneousFlow = instantaneousFlow;
	}

	public BigDecimal getSupplyVoltage() {
		return supplyVoltage;
	}

	public void setSupplyVoltage(BigDecimal supplyVoltage) {
		this.supplyVoltage = supplyVoltage;
	}

	public BigDecimal getGprsSignalIntensity() {
		return gprsSignalIntensity;
	}

	public void setGprsSignalIntensity(BigDecimal gprsSignalIntensity) {
		this.gprsSignalIntensity = gprsSignalIntensity;
	}

	public BigDecimal getPressure() {
		return pressure;
	}

	public void setPressure(BigDecimal pressure) {
		this.pressure = pressure;
	}

	public BigDecimal getWaterLevel() {
		return waterLevel;
	}

	public void setWaterLevel(BigDecimal waterLevel) {
		this.waterLevel = waterLevel;
	}

	public String getBatteryVoltageAlarm() {
		return batteryVoltageAlarm;
	}

	public void setBatteryVoltageAlarm(String batteryVoltageAlarm) {
		this.batteryVoltageAlarm = batteryVoltageAlarm;
	}

	public String getSerialCommunicationState() {
		return serialCommunicationState;
	}

	public void setSerialCommunicationState(String serialCommunicationState) {
		this.serialCommunicationState = serialCommunicationState;
	}

	public String getDoorSwitch() {
		return doorSwitch;
	}

	public void setDoorSwitch(String doorSwitch) {
		this.doorSwitch = doorSwitch;
	}

	public String getPressureFault() {
		return pressureFault;
	}

	public void setPressureFault(String pressureFault) {
		this.pressureFault = pressureFault;
	}

	public String getHigherPressureAlarm() {
		return higherPressureAlarm;
	}

	public void setHigherPressureAlarm(String higherPressureAlarm) {
		this.higherPressureAlarm = higherPressureAlarm;
	}

	public String getLowerPressureAlarm() {
		return lowerPressureAlarm;
	}

	public void setLowerPressureAlarm(String lowerPressureAlarm) {
		this.lowerPressureAlarm = lowerPressureAlarm;
	}

	public String getBatteryVoltageLowAlarm() {
		return batteryVoltageLowAlarm;
	}

	public void setBatteryVoltageLowAlarm(String batteryVoltageLowAlarm) {
		this.batteryVoltageLowAlarm = batteryVoltageLowAlarm;
	}

	public String getBatteryVoltageTooLowAlarm() {
		return batteryVoltageTooLowAlarm;
	}

	public void setBatteryVoltageTooLowAlarm(String batteryVoltageTooLowAlarm) {
		this.batteryVoltageTooLowAlarm = batteryVoltageTooLowAlarm;
	}

	public String getPressureStatus() {
		return pressureStatus;
	}

	public void setPressureStatus(String pressureStatus) {
		this.pressureStatus = pressureStatus;
	}

	public String getPowerSupplyVoltage() {
		return powerSupplyVoltage;
	}

	public void setPowerSupplyVoltage(String powerSupplyVoltage) {
		this.powerSupplyVoltage = powerSupplyVoltage;
	}

	public String getWaterLevelFault() {
		return waterLevelFault;
	}

	public void setWaterLevelFault(String waterLevelFault) {
		this.waterLevelFault = waterLevelFault;
	}

	public String getWaterUpLevelAlarm() {
		return waterUpLevelAlarm;
	}

	public void setWaterUpLevelAlarm(String waterUpLevelAlarm) {
		this.waterUpLevelAlarm = waterUpLevelAlarm;
	}

	public String getWaterDownLevelAlarm() {
		return waterDownLevelAlarm;
	}

	public void setWaterDownLevelAlarm(String waterDownLevelAlarm) {
		this.waterDownLevelAlarm = waterDownLevelAlarm;
	}

	public String getWaterLevelStatus() {
		return waterLevelStatus;
	}

	public void setWaterLevelStatus(String waterLevelStatus) {
		this.waterLevelStatus = waterLevelStatus;
	}

	public Integer getIsReade() {
		return isReade;
	}

	public void setIsReade(Integer isReade) {
		this.isReade = isReade;
	}

	@Override
	protected Serializable pkVal() {
		return this.id;
	}

}
