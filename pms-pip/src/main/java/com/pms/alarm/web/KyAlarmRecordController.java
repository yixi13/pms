package com.pms.alarm.web;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.pms.alarm.entity.KyAlarmRecord;
import com.pms.alarm.service.IKyAlarmRecordService;
import com.pms.alarm.service.impl.KyAlarmRecordServiceImpl;
import com.pms.exception.R;
import com.pms.partitionPoints.entity.KyOrganization;
import com.pms.partitionPoints.service.IKyOrganizationService;
import com.pms.validator.Assert;
import com.xiaoleilu.hutool.util.StrUtil;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import com.pms.controller.BaseController;

import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * <p>
 * 设备上传数据 前端控制器
 * </p>
 *
 * @author ljb
 * @since 2018-07-16
 */
@RestController
@RequestMapping("/alarm/kyAlarmRecord")
@Api(value="报警记录接口")
public class KyAlarmRecordController extends BaseController {

    @Autowired
    IKyAlarmRecordService iKyAlarmRecordService;
    @Autowired
    IKyOrganizationService iKyOrganizationService;

    @ApiOperation(value = "获取报警记录list")
    @RequestMapping(value = "/list",method = RequestMethod.GET)
    @ApiImplicitParams({
            @ApiImplicitParam(name="areaIds",value="分区id",required = true,dataType = "String",paramType="form"),
            @ApiImplicitParam(name="equipmentIds",value="测点ids",required = true,dataType = "String",paramType="form"),
            @ApiImplicitParam(name="starTime",value="开始时间",required = true,dataType = "String",paramType="form"),
            @ApiImplicitParam(name="endTime",value="结束时间",required = true,dataType = "String",paramType="form"),
            @ApiImplicitParam(name="alarmType",value="报警类别",required = true,dataType = "String",paramType="form")
    })
    public R list(Integer current, Integer size, String equipmentIds,Integer isReade,
                 String starTime, String endTime,String alarmType,String content){

        Assert.isNull(current,"current 不能为空");
        Assert.isNull(size,"size 不能为空");

        EntityWrapper<Map<String,Object>> ew = new EntityWrapper<Map<String,Object>>();
        if (StrUtil.isNotEmpty(alarmType)){
            ew.eq("kar.changeType", alarmType);
        }
        if (isReade != null) {
            ew.eq("kar.isReade", isReade);
        }
        if (StrUtil.isNotEmpty(equipmentIds)){
            ew.in("kar.equipmentId", equipmentIds);
        }
        if (StrUtil.isNotEmpty(content)){
            ew.and().like("kar.changeDescription", content).
                    or(" kei.name LIKE '%"+content+"%' ");
        }
        if (StrUtil.isNotEmpty(starTime)) {
            starTime = starTime + " 00:00:00";
            ew.and("kar.acquisitionTime >'" + starTime + "'");
        }
        if (StrUtil.isNotEmpty(endTime)) {
            endTime = endTime + " 23:59:59";
            ew.and("kar.acquisitionTime <'" + endTime + "'");
        }
        ew.orderBy("kar.isReade desc");
        ew.orderBy("kar.acquisitionTime desc");
        Page<Map<String,Object>> page = new Page(current, size);
        page = iKyAlarmRecordService.selectAlarmRecordRecordMapsPage(page,ew);
        return R.ok().put("page",page);
    }


    @ApiOperation(value = "修改为已读")
    @RequestMapping(value = "/updateToReade",method = RequestMethod.POST)
    @ApiImplicitParams({
            @ApiImplicitParam(name="alarmIds", value="报警ids",required = true,dataType = "String",paramType="form")
    })
    public R updateToReade(String alarmIds){
        Assert.isBlank(alarmIds,"报警ids");
        EntityWrapper<KyAlarmRecord> ew = new EntityWrapper<KyAlarmRecord>();
        ew.in("id", alarmIds);
        List<KyAlarmRecord> records = iKyAlarmRecordService.selectList(ew);
        for (KyAlarmRecord record:records){
            record.setIsReade(1);
        }
        iKyAlarmRecordService.updateBatchById(records);
        return R.ok();
    }


    @ApiOperation(value = "删除")
    @RequestMapping(value = "/{alarmIds}",method = RequestMethod.DELETE)
    @ApiImplicitParams({
            @ApiImplicitParam(name="alarmIds", value="报警ids",required = true,dataType = "String",paramType="form")
    })
    public R delete(@PathVariable String alarmIds){
        iKyAlarmRecordService.deleteById(Long.valueOf(alarmIds));
        return R.ok();
    }

    @ApiOperation(value = "删除")
    @RequestMapping(value = "/batch",method = RequestMethod.DELETE)
    @ApiImplicitParams({
            @ApiImplicitParam(name="alarmIds", value="List报警ids",required = true,dataType = "String",paramType="form")
    })
    public R deleteBatch(String alarmIds){
        Assert.isBlank(alarmIds,"报警ids");
        iKyAlarmRecordService.deleteBatch(alarmIds);
        return R.ok();
    }


    @ApiOperation(value = "查询所有未读的数据")
    @RequestMapping(value = "/getNotReade",method = RequestMethod.GET)
    public R countNoReade(@RequestHeader("X-Code") String companyCode,
                          @RequestHeader("X-groupType") String groupType,
                          @RequestHeader("X-groupId") String groupId){

        List<KyOrganization> organizationList = iKyOrganizationService.
                getOrganizationByRoleTypeAndRoleIdAndCompanyCode(
                        Integer.valueOf(groupType),
                        Long.valueOf(groupId),
                        companyCode);
        String orgIds = "";
        for (KyOrganization kyOrganization : organizationList) {
            orgIds += kyOrganization.getId()+",";
        }
        orgIds = orgIds.substring(0, orgIds.length() - 1);
        Integer count = iKyAlarmRecordService.selectNoRead(orgIds);
        return R.ok().put("count",count);
    }
}
