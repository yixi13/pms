package com.pms.alarm.web;

import io.swagger.annotations.Api;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import com.pms.controller.BaseController;
import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 * 报警类型表 前端控制器
 * </p>
 *
 * @author ljb
 * @since 2018-07-16
 */
@RestController
@RequestMapping("/alarm/kyAlarmType")
@Api(value="报警类型接口")
public class KyAlarmTypeController extends BaseController {
	
}
