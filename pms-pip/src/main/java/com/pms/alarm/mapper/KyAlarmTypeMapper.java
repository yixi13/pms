package com.pms.alarm.mapper;

import com.pms.alarm.entity.KyAlarmType;
import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
  * 报警类型表 Mapper 接口
 * </p>
 *
 * @author ljb
 * @since 2018-07-16
 */
public interface KyAlarmTypeMapper extends BaseMapper<KyAlarmType> {

}