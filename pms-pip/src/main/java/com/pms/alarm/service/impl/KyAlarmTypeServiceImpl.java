package com.pms.alarm.service.impl;

import com.pms.alarm.entity.KyAlarmType;
import com.pms.alarm.mapper.KyAlarmTypeMapper;
import com.pms.alarm.service.IKyAlarmTypeService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 报警类型表 服务实现类
 * </p>
 *
 * @author ljb
 * @since 2018-07-16
 */
@Service
public class KyAlarmTypeServiceImpl extends ServiceImpl<KyAlarmTypeMapper, KyAlarmType> implements IKyAlarmTypeService {
	
}
