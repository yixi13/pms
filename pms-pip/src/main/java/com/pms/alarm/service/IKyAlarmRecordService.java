package com.pms.alarm.service;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.pms.alarm.entity.KyAlarmRecord;
import com.baomidou.mybatisplus.service.IService;

import java.util.Map;

/**
 * <p>
 * 设备上传数据 服务类
 * </p>
 *
 * @author ljb
 * @since 2018-07-16
 */
public interface IKyAlarmRecordService extends IService<KyAlarmRecord> {

    Page<Map<String,Object>> selectAlarmRecordRecordMapsPage(Page<Map<String, Object>> page, EntityWrapper<Map<String, Object>> ew);

    void deleteBatch(String alarmIds);

    Map<String,Object> selectApiAlarmRecord(String alarmId);

    /**
     * 根据机构id 查询未读的报警信息
     * @param orgIds
     * @return
     */
    Integer selectNoRead(String orgIds);
}
