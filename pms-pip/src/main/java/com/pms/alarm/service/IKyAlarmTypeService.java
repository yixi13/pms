package com.pms.alarm.service;

import com.pms.alarm.entity.KyAlarmType;
import com.baomidou.mybatisplus.service.IService;

/**
 * <p>
 * 报警类型表 服务类
 * </p>
 *
 * @author ljb
 * @since 2018-07-16
 */
public interface IKyAlarmTypeService extends IService<KyAlarmType> {
	
}
