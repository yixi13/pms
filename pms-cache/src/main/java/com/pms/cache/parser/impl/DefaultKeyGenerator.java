package com.pms.cache.parser.impl;

import com.baomidou.mybatisplus.mapper.Wrapper;
import com.pms.cache.constants.CacheScope;
import com.pms.cache.parser.IKeyGenerator;
import com.pms.cache.parser.IUserKeyGenerator;
import com.pms.cache.utils.ReflectionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.Serializable;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@Service
public class DefaultKeyGenerator extends IKeyGenerator {
    protected Logger logger = LoggerFactory.getLogger(getClass());
    @Autowired(required = false)
    private IUserKeyGenerator userKeyGenerator;

    @Override
    public String buildKey(String key, CacheScope scope, Class<?>[] parameterTypes, Object[] arguments) {
        boolean isFirst = true;
        if (key.indexOf("{") > 0) {
            key = key.replace("{", ":{");
            Pattern pattern = Pattern.compile("\\d+\\.?[\\w]*");
            Matcher matcher = pattern.matcher(key);
            while (matcher.find()) {
                String tmp = matcher.group();
                String express[] = matcher.group().split("\\.");
                String i = express[0];
                int index = Integer.parseInt(i) - 1;
                Object value = arguments[index];
                if (parameterTypes[index].isAssignableFrom(List.class)) {
                    List result = (List) arguments[index];
                    value = result.get(0);
                }
                if (value == null || value.equals("null"))
                    value = "";
                if (express.length > 1) {
                    String field = express[1];
                    value = ReflectionUtils.getFieldValue(value, field);
                }
                //新增 Wrapper类解析
                if (parameterTypes[index].isAssignableFrom(Wrapper.class)&& arguments[index] instanceof Wrapper) {
                    Wrapper<?> wp = null;
                    String wpSql = null;
                    int spaceIndex = -1;
                    wp = (Wrapper<?>) arguments[index];
                    wpSql= wp.getSqlSegment();
                    if(wpSql!=null){
                        spaceIndex = wpSql.indexOf("\n");
                        if(spaceIndex!=-1){
                            wpSql = wpSql.substring(spaceIndex+2);
                        }
                        // 拼接 order by,having 等条件语句
                        value = value.toString()+wpSql;
                    }
                }
                if (isFirst) {
                    key = key.replace("{" + tmp + "}", value.toString());
                } else {
                    key = key.replace("{" + tmp + "}", LINK + value.toString());
                }
            }
        }
        if(key.equals("{1}")){
            key = key.replace("{", "{");
            Pattern pattern = Pattern.compile("\\d+\\.?[\\w]*");
            Matcher matcher = pattern.matcher(key);
            while (matcher.find()) {
                String tmp = matcher.group();
                String express[] = matcher.group().split("\\.");
                String i = express[0];
                int index = Integer.parseInt(i) - 1;
                Object value = arguments[index];
                if (parameterTypes[index].isAssignableFrom(List.class)) {
                    List result = (List) arguments[index];
                    value = result.get(0);
                }
                if (value == null || value.equals("null"))
                    value = "";
                if (express.length > 1) {
                    String field = express[1];
                    value = ReflectionUtils.getFieldValue(value, field);
                }
                if (isFirst) {
                    key = key.replace("{" + tmp + "}", value.toString());
                } else {
                    key = key.replace("{" + tmp + "}", LINK + value.toString());
                }
            }
        }
        logger.debug("buildKey-->>  "+ key);
        return key;
    }

    @Override
    public IUserKeyGenerator getUserKeyGenerator() {
        return userKeyGenerator;
    }

}
