package com.pms.cache.parser;

/**
 * 当前用户信息身份标志
 *
 */
public interface IUserKeyGenerator {
     String getCurrentUserAccount();
}
