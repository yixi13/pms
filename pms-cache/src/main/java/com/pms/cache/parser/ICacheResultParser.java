package com.pms.cache.parser;

import java.lang.reflect.Type;

/**
 * cache结果解析
 * <p/>
 * 解决问题：
 * @since 1.7
 */
public interface ICacheResultParser {
    /**
     * 解析结果
     *
     * @param value
     * @param returnType
     * @param origins
     * @return
     */
     Object parse(String value, Type returnType, Class<?>... origins);
    /**
     * 解析结果
     *
     * @param value
     * @param returnType
     * @param origins
     * @return
     */
    Object parse(Class<?> pointClass,String value, Type returnType, Class<?>... origins);

}
