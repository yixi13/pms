package com.pms.cache.parser.impl;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONException;
import com.alibaba.fastjson.JSONObject;
import com.pms.cache.parser.ICacheResultParser;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

/**
 * 默认缓存结果解析类
 *
 * @since 1.7
 */
public class DefaultResultParser implements ICacheResultParser {
    protected Logger logger = LoggerFactory.getLogger(getClass());
    @Override
    public Object parse(Class<?> pointClass,String value, Type type, Class<?>... origins) {
        Object result = null;
        if (type instanceof ParameterizedType) {
            ParameterizedType parameterizedType = (ParameterizedType) type;
            Type rawType = parameterizedType.getRawType();
            logger.debug("--Type-->>  "+rawType);
            if (((Class) rawType).isAssignableFrom(List.class)) {
               // logger.debug("--getActualTypeArguments-->>  "+(Class)parameterizedType.getActualTypeArguments()[0]);
                //result = JSON.parseArray(value, (Class) parameterizedType.getActualTypeArguments()[0]);
//                result=JSON.parseArray(value);

/* =================2017-10-12 修改，处理类型转换异常问题==============================*/
                JSONArray resultJSONArray = JSON.parseArray(value);
                if(null!=resultJSONArray){
                    Type objType = parameterizedType.getActualTypeArguments()[0];
                    if(objType.getTypeName().equals("T")||objType.getClass().getName().equals("T")){//泛型  返回基类
                        objType = pointClass;
                    }
                    List<Object> returnList = new ArrayList<Object>();
                    for(int x=0;x<resultJSONArray.size();x++){
                        Object obj = JSONObject.parseObject(resultJSONArray.get(x).toString(),objType);
                        returnList.add(obj);
                    }
                    return  returnList;
                }
            }else{
                result = JSON.parseObject(value,type);
            }

        } else if (origins == null) {
            if(type.getTypeName().equals("T")){//泛型  返回基类
                type = pointClass;
            }
            try {
                result = JSON.parseObject(value, (Class) type);
            }catch (JSONException e){
                logger.debug("valueJson转换异常,value值为:"+value+",type类型为:"+type);
                result=value;
            }
        } else {
            result = JSON.parseObject(value, origins[0]);
        }
        return result;
    }
    @Override
    public Object parse(String value, Type type, Class<?>... origins) {
        Object result = null;
        if (type instanceof ParameterizedType) {
            ParameterizedType parameterizedType = (ParameterizedType) type;
            Type rawType = parameterizedType.getRawType();
            logger.debug("--Type-->>  "+rawType);
            if (((Class) rawType).isAssignableFrom(List.class)) {
                // logger.debug("--getActualTypeArguments-->>  "+(Class)parameterizedType.getActualTypeArguments()[0]);
                //result = JSON.parseArray(value, (Class) parameterizedType.getActualTypeArguments()[0]);
//                result=JSON.parseArray(value);

/* =================2017-10-12 修改，处理类型转换异常问题==============================*/
                JSONArray resultJSONArray = JSON.parseArray(value);
                if(null!=resultJSONArray){
                    List<Object> returnList = new ArrayList<Object>();
                    Type objType = parameterizedType.getActualTypeArguments()[0];
                    for(int x=0;x<resultJSONArray.size();x++){
                        Object obj = JSONObject.parseObject(resultJSONArray.get(x).toString(),objType);
                        returnList.add(obj);
                    }
                    return  returnList;
                }
            }else{
                result = JSON.parseObject(value,type);
            }

        } else if (origins == null) {
            try {
                result = JSON.parseObject(value, (Class) type);
            }catch (JSONException e){
                logger.debug("valueJson转换异常,value值为:"+value+",type类型为:"+type);
                result=value;
            }

        } else {
            result = JSON.parseObject(value, origins[0]);
        }
        return result;
    }
}
