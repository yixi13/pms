package com.pms.cache.utils;

import com.pms.cache.annotation.Cache;
import com.pms.cache.annotation.CacheClear;
import com.pms.cache.api.CacheAPI;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * redis缓存
 * 所有缓存 key值自动增加前缀 xxny:
 */
@Component
public class RedisCacheUtil {

    @Autowired
    private CacheAPI cacheAPI;

    /**
     * 通过key值获取缓存数据, key值自动增加前缀 xxny:
     * @param key
     * @return
     */
    @Cache(key ="pms{1}")
    public Object getVlue(String key){ return null; }

    @Cache(key ="pms{1}")
    public String getVlueStr(String key){ return null; }

    /**
     * 保存或更新缓存数据，key值自动增加前缀 xxny:
     * @param key
     * @return
     */
    public void saveAndUpdate(String key,Object obj,Integer expireMinutes){
        if(expireMinutes==null||expireMinutes<1){cacheAPI.set("pms:"+key,obj,720);}
        else{
            cacheAPI.set("pms:"+key,obj,expireMinutes);
        }
    }
    /**
     * 通过key值 清除缓存，key值自动增加前缀 xxny:
     * @param key key值
     * @return
     */
    @CacheClear(key ="pms{1}")
    public void clearByKey(String key){}
    /**
     * 通过前缀 清楚缓存，自动增加前缀 xxny:
     * @param pre 前缀
     * @return
     */
    @CacheClear(pre ="pms{1}")
    public  void clearByPre(String pre){}

    /**
     * 通过key值清楚缓存
     * @param key
     */
    public void clearOtherCacleByKey(String key){
        cacheAPI.remove(key);
    }
    /**
     * 通过 前缀 清楚缓存
     * @param pre
     */
    public void clearOtherCacleByPre(String pre){
        cacheAPI.removeByPre(pre);
    }
}
