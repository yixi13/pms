package com.pms.cache.aspect;

import com.pms.cache.annotation.BaseCacheClear;
import com.pms.cache.annotation.CacheClear;
import com.pms.cache.api.CacheAPI;
import com.pms.cache.constants.CacheScope;
import com.pms.cache.parser.IKeyGenerator;
import com.pms.cache.parser.impl.DefaultKeyGenerator;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.aspectj.lang.reflect.MethodSignature;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.lang.reflect.Method;
import java.util.concurrent.ConcurrentHashMap;

/**
 * 清除缓存注解拦截
 * 注：清除时,key，key,pre, 字符串 都需加 : 在前
 * eg: @BaseCacheClear(pre=":"),@BaseCacheClear(key=":selectById")，@BaseCacheClear(keys={":selectById",":selectPage"})
 *
 * @since 1.7
 */
@Aspect
@Service
public class BaseCacheClearAspect {
    @Autowired
    private IKeyGenerator keyParser;
    @Autowired
    private CacheAPI cacheAPI;
    protected Logger log = Logger.getLogger(this.getClass());
    private ConcurrentHashMap<String, IKeyGenerator> generatorMap = new ConcurrentHashMap<String, IKeyGenerator>();

    @Pointcut("@annotation(com.pms.cache.annotation.BaseCacheClear)")
    public void aspect() {
    }

    @Around("aspect()&&@annotation(anno)")
    public Object interceptor(ProceedingJoinPoint invocation, BaseCacheClear anno)
            throws Throwable {
        //获取切点所在类的类名 (全路径 类名 eg:com.pms.service.impl.EstateAgencyServiceImpl)
        String pointClassName = invocation.getTarget().getClass().getName();
        //获取 实现类名称 eg: EstateAgencyServiceImpl
        int lastPointIndex=pointClassName.lastIndexOf(".");
        pointClassName = pointClassName.substring(lastPointIndex+1);
        //获取 基类名称 eg: EstateAgency
        int serviceImplIndex = pointClassName.lastIndexOf("ServiceImpl");
        pointClassName =pointClassName.substring(0,serviceImplIndex);
        //基类名称 首字母转小写
        pointClassName= toLowerCaseFirstOne(pointClassName);

        MethodSignature signature = (MethodSignature) invocation.getSignature();
        Method method = signature.getMethod();
        Class<?>[] parameterTypes = method.getParameterTypes();
        Object[] arguments = invocation.getArgs();
        String key = "";
        if (StringUtils.isNotBlank(anno.key())) {
            key = getKey(pointClassName,anno, anno.key(), CacheScope.application,
                    parameterTypes, arguments);
            log.debug("BaseCacheClearAspect --- Key -->>" +key);
            cacheAPI.remove(key);
        }if (StringUtils.isNotBlank(anno.pre())) {
            key = getKey(pointClassName,anno, anno.pre(), CacheScope.application,
                    parameterTypes, arguments);
            log.debug("BaseCacheClearAspect --- preKey -->>" +key);
            cacheAPI.removeByPre(key);
        }if (anno.keys().length > 1) {
            for (String tmp : anno.keys()) {
                tmp = getKey(pointClassName,anno, tmp, CacheScope.application, parameterTypes,
                        arguments);
                log.debug("BaseCacheClearAspect --- keysKey -->>" +key);
                cacheAPI.removeByPre(tmp);
            }
        }
        return invocation.proceed();
    }

    /**
     * 解析表达式
     *
     * @param anno
     * @param parameterTypes
     * @param arguments
     * @return
     * @throws InstantiationException
     * @throws IllegalAccessException
     */
    private String getKey(String preKey,BaseCacheClear anno, String key, CacheScope scope,
                          Class<?>[] parameterTypes, Object[] arguments)
            throws InstantiationException, IllegalAccessException {
        String finalKey;
        String generatorClsName = anno.generator().getName();
        IKeyGenerator keyGenerator = null;
        if (anno.generator().equals(DefaultKeyGenerator.class)) {
            keyGenerator = keyParser;
        } else {
            if (generatorMap.containsKey(generatorClsName)) {
                keyGenerator = generatorMap.get(generatorClsName);
            } else {
                keyGenerator = anno.generator().newInstance();
                generatorMap.put(generatorClsName, keyGenerator);
            }
        }
        finalKey = keyGenerator.getKey(preKey+key, scope, parameterTypes, arguments);
        return finalKey;
    }
    //首字母转小写
    public static String toLowerCaseFirstOne(String s){
        if(Character.isLowerCase(s.charAt(0)))
            return s;
        else
            return (new StringBuilder()).append(Character.toLowerCase(s.charAt(0))).append(s.substring(1)).toString();
    }
}
