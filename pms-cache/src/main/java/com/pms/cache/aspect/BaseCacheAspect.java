package com.pms.cache.aspect;

import com.pms.cache.annotation.BaseCache;
import com.pms.cache.annotation.Cache;
import com.pms.cache.api.CacheAPI;
import com.pms.cache.parser.ICacheResultParser;
import com.pms.cache.parser.IKeyGenerator;
import com.pms.cache.parser.impl.DefaultKeyGenerator;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.aspectj.lang.reflect.MethodSignature;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.lang.reflect.Method;
import java.lang.reflect.Type;
import java.util.concurrent.ConcurrentHashMap;

/**
 * 缓存开启注解拦截
 * 注:  生成key时  自动拼接 基类名称
 * eg: news:selectById{1}  其中 news: 为自动拼接内容
 * @since 1.7
 */
@Aspect
@Service
public class BaseCacheAspect {
    @Autowired
    private IKeyGenerator keyParser;
    @Autowired
    private CacheAPI cacheAPI;
    protected Logger log = Logger.getLogger(this.getClass());
    private ConcurrentHashMap<String, ICacheResultParser> parserMap = new ConcurrentHashMap<String, ICacheResultParser>();
    private ConcurrentHashMap<String, IKeyGenerator> generatorMap = new ConcurrentHashMap<String, IKeyGenerator>();

    @Pointcut("@annotation(com.pms.cache.annotation.BaseCache)")
    public void aspect() {
    }

    @Around("aspect()&&@annotation(anno)")
    public Object interceptor(ProceedingJoinPoint invocation, BaseCache anno)
            throws Throwable {
        Object result = null;
        //获取切点所在类的类名 (全路径 类名 eg:com.pms.service.impl.EstateAgencyServiceImpl)
        String pointClassName =invocation.getTarget().getClass().getName();
        //获取 基类包路径前缀 eg: com.pms.
        int pointClassNamePrefixIndex = pointClassName.indexOf("service");
        String pointClassNamePrefix = pointClassName.substring(0,pointClassNamePrefixIndex);
        //获取 实现类名称 eg: EstateAgencyServiceImpl
        int lastPointIndex=pointClassName.lastIndexOf(".");
        pointClassName = pointClassName.substring(lastPointIndex+1);
        //获取 基类名称 eg: EstateAgency
        int serviceImplIndex = pointClassName.lastIndexOf("ServiceImpl");
        pointClassName =pointClassName.substring(0,serviceImplIndex);
        Class pointClass =null;
        String key = "";
        String value = "";
        try {
            pointClass = Class.forName(pointClassNamePrefix+"entity."+pointClassName);
            //基类名称 首字母转小写
            pointClassName= toLowerCaseFirstOne(pointClassName);
            // 获取 切点 方法签名对象
            MethodSignature signature = (MethodSignature) invocation.getSignature();
            // 获取 切点 方法对象
            Method method = signature.getMethod();
            // 获取 方法的所有参数类型
            Class<?>[] parameterTypes = method.getParameterTypes();
            // 返回目标方法的参数
            Object[] arguments = invocation.getArgs();
            key = getKey(pointClassName,anno, parameterTypes, arguments);
            log.debug("CacheAspect --- key -->>" +key);
            value = cacheAPI.get(key);
            log.debug("CacheAspect --- value -->>" +value);
            Type returnType = method.getGenericReturnType();
            result = getResult(pointClass,anno, result, value, returnType);
            log.debug("CacheAspect --- result -->>" +result);
        }catch (ClassNotFoundException ex){
            log.error("读取redis缓存时,com.pms.entity."+pointClassName+"类型加载出错:", ex);
        } catch (Exception e) {
            log.error("获取缓存失败：" + key, e);
        } finally {
            if (result == null) {
                result = invocation.proceed();
                if (StringUtils.isNotBlank(key)) {
                    cacheAPI.set(key, result, anno.expire());
                }
            }
        }
        return result;
    }

    /**
     * 解析表达式
     *
     * @param anno
     * @param parameterTypes
     * @param arguments
     * @return
     * @throws InstantiationException
     * @throws IllegalAccessException
     */
    private String getKey(String preKey,BaseCache anno, Class<?>[] parameterTypes,
                          Object[] arguments) throws InstantiationException,
            IllegalAccessException {
        String key;
        /*anno.generator() 得到 DefaultKeyGenerator.class
         *anno.generator().getName() 的到 DefaultKeyGenerator对象的具体名称
         */
        String generatorClsName = anno.generator().getName();
        IKeyGenerator keyGenerator = null;
        if (anno.generator().equals(DefaultKeyGenerator.class)) {
            // 缓存键值表达式对象 赋值
            keyGenerator = keyParser;
        }else {//没有用到 暂不考虑
            if (generatorMap.contains(generatorClsName)) {
                keyGenerator = generatorMap.get(generatorClsName);
            } else {
                keyGenerator = anno.generator().newInstance();
                generatorMap.put(generatorClsName, keyGenerator);
            }
        }

        key = keyGenerator.getKey(preKey+":"+anno.key(), anno.scope(), parameterTypes,
                arguments);
        return key;
    }

    /**
     * 解析结果
     *
     * @param anno
     * @param result
     * @param value
     * @param returnType
     * @return
     * @throws InstantiationException
     * @throws IllegalAccessException
     */
    private Object getResult(BaseCache anno, Object result, String value,
                             Type returnType) throws InstantiationException,
            IllegalAccessException {
        String parserClsName = anno.parser().getName();
        ICacheResultParser parser = null;
        if (parserMap.containsKey(parserClsName)) {
            parser = parserMap.get(parserClsName);
        } else {
            parser = anno.parser().newInstance();
            parserMap.put(parserClsName, parser);
        }
        if (parser != null) {
            if (anno.result()[0].equals(Object.class)) {
                result = parser.parse(value, returnType,
                        null);

            } else {
                result = parser.parse(value, returnType,
                        anno.result());
            }
        }
        return result;
    }

    /**
     * 解析结果
     *
     * @param anno
     * @param result
     * @param value
     * @param returnType
     * @return
     * @throws InstantiationException
     * @throws IllegalAccessException
     */
    private Object getResult( Class<?> pointClass,BaseCache anno, Object result, String value,
                             Type returnType) throws InstantiationException,
            IllegalAccessException {
        String parserClsName = anno.parser().getName();
        ICacheResultParser parser = null;
        if (parserMap.containsKey(parserClsName)) {
            parser = parserMap.get(parserClsName);
        } else {
            parser = anno.parser().newInstance();
            parserMap.put(parserClsName, parser);
        }
        if (parser != null) {
            if (anno.result()[0].equals(Object.class)) {
                result = parser.parse(pointClass,value, returnType,
                        null);

            } else {
                result = parser.parse(pointClass,value, returnType,
                        anno.result());
            }
        }
        return result;
    }
    //首字母转小写
    public static String toLowerCaseFirstOne(String s){
        if(Character.isLowerCase(s.charAt(0)))
            return s;
        else
            return (new StringBuilder()).append(Character.toLowerCase(s.charAt(0))).append(s.substring(1)).toString();
    }
}
