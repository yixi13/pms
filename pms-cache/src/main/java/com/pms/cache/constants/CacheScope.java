package com.pms.cache.constants;

public enum CacheScope {
    user, application
}
