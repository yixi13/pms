package com.pms.web;

import com.pms.common.MQConstant;
import com.pms.service.IMessageQueueService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;


/**
 * Created by Administrator on 2017/11/20 0020.
 */
@RestController
@RequestMapping("testMq/mq")
public class TestController {

    Logger logger = LoggerFactory.getLogger(getClass());

    @Autowired
    private IMessageQueueService messageQueueService;


    //    @RequestMapping(value = "/send",method = RequestMethod.POST)
    @PostMapping(value = "/send")
    @ResponseBody
    public void send(String msg) {
        logger.info("发送MQ消息:msg={}", msg);
        messageQueueService.send("HELLO.us", "测试发送消息");
    }

}
