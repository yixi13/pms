package com.pms.common;

/**
 * Created by Administrator on 2017/11/29 0029.
 * @desc Rabbit消息队列相关常量
 */
public final class MQConstant {
    private MQConstant(){
    }

    //exchange name
    public static final String DEFAULT_EXCHANGE = "KSHOP";

    //exchange name
    public static final String TOPIC_EXCHANGE = "topic.us";

    //exchange name
    public static final String FANOUT_EXCHANGE = "FANOUT";

    //exchange name
    public static final String HEADERS_EXCHANGE = "HEADERS";

    //DLX QUEUE
    public static final String DEFAULT_DEAD_LETTER_QUEUE_NAME = "kshop.dead.letter.queue";

    //DLX repeat QUEUE 死信转发队列
    public static final String DEFAULT_REPEAT_TRADE_QUEUE_NAME = "kshop.repeat.trade.queue";


    //Hello 测试消息队列名称
    public static final String HELLO_QUEUE_NAME = "HELLO";

    //测试所用的routing key
    public static final String HELLO_KEY = "HELLO.*";
}
