package com.pms.processor;

/**
 * Created by Administrator on 2017/11/30 0030.
 */

import com.pms.common.MQConstant;
import org.springframework.amqp.rabbit.annotation.RabbitHandler;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Component;

/**
 *
 * @author victor
 * @desc hello 消息队列消费者
 */
@Component
@RabbitListener(queues = MQConstant.HELLO_QUEUE_NAME)
public class HelloProcessor {
    @RabbitHandler
    public void process(String content) {
        System.out.println("接受消息:" + content);
    }
}
