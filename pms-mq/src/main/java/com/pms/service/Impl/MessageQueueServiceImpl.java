package com.pms.service.Impl;

import com.pms.common.MQConstant;
import com.pms.service.IMessageQueueService;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Created by Administrator on 2017/11/29 0029.
 */
@Service("messageQueueService")
public class MessageQueueServiceImpl implements IMessageQueueService {
    @Autowired
    private RabbitTemplate rabbitTemplate;

    @Override
    public void send(String queueName, String msg) {
        rabbitTemplate.convertAndSend(MQConstant.TOPIC_EXCHANGE,queueName, msg);
    }
}
