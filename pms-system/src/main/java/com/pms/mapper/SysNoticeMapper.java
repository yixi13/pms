package com.pms.mapper;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.pms.entity.SysNotice;
import com.baomidou.mybatisplus.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * <p>
  *  Mapper 接口
 * </p>
 *
 * @author ljb
 * @since 2018-07-17
 */
public interface SysNoticeMapper extends BaseMapper<SysNotice> {

    List<Map<String,Object>> selectSysNoticeMapsPage(Page<Map<String, Object>> page,@Param("ew") EntityWrapper<Map<String, Object>> ew);

    List<SysNotice> selectEffNoticeList();
}