package com.pms.mapper.push;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.pms.entity.push.PushMsgRecordMember;

/**
 * 
 * 
 * @author zyl
 * @email 517770986@qq.com
 * @date 2017-12-01 11:12:07
 */
public interface PushMsgRecordMemberMapper extends BaseMapper<PushMsgRecordMember> {
	
}
