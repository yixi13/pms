package com.pms.mapper;
import com.pms.entity.AreaCn;
import com.baomidou.mybatisplus.mapper.BaseMapper;

import java.util.List;
import java.util.Map;

/**
 * 
 * 
 * @author zyl
 * @email 517770986@qq.com
 * @date 2017-12-21 16:39:49
 */
public interface AreaCnMapper extends BaseMapper<AreaCn> {
    List<AreaCn> getProvince();

    List<AreaCn> getCitys(Integer gbCode);

    List<AreaCn> getCountys(Integer gbCode);

    List<Map<String,Object>> getAreaCnMap(Map<String,Object> paramMap);
}
