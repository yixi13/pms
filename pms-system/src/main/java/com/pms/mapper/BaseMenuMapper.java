package com.pms.mapper;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.baomidou.mybatisplus.mapper.Wrapper;
import com.pms.entity.BaseMenu;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * <p>
  *  Mapper 接口
 * </p>
 *
 * @author zyl
 * @since 2017-07-06
 */
public interface BaseMenuMapper extends BaseMapper<BaseMenu> {
    public List<BaseMenu> selectMenuByAuthorityId(@Param("authorityId") String authorityId, @Param("authorityType") String authorityType);


    /**
     * 根据用户和组的权限关系查找用户可访问菜单
     * @param userId
     * @return
     */
    public List<BaseMenu> selectAuthorityMenuByUserId(@Param("userId") int userId);

    public List<BaseMenu> selectAuthorityMenu(@Param("userId")Long userId,@Param("menuType")Integer menuType,@Param("type")Integer type);

    public List<BaseMenu> ApiSelectAuthorityMenu(@Param("userId")Long userId,@Param("type")Integer type);


    public List<BaseMenu>  selectTreeTrue(@Param("userId")Long userId);


    /**
     * 根据用户和组的权限关系查找用户可访问的系统
     * @param userId
     * @return
     */
    public List<BaseMenu> selectAuthoritySystemByUserId(@Param("userId") int userId);

    List<BaseMenu> selectListTree();

    Map<String, Object> getApiPermissionMenu(@Param("userId")Long userId,@Param("menuType") Integer  menuType,@Param("title") String title);

    public List<BaseMenu> getAuthorityMenuAndSuperadmin(@Param("userId")Long userId);
}