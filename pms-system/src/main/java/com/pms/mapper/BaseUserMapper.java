package com.pms.mapper;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.baomidou.mybatisplus.mapper.Wrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.pms.entity.BaseUser;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.session.RowBounds;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

/**
 * <p>
  *  Mapper 接口
 * </p>
 *
 * @author zyl
 * @since 2017-07-06
 */
public interface BaseUserMapper extends BaseMapper<BaseUser> {
     BaseUser getBaseUserByUserName(@Param("username") String userName, @Param("companyCode")String companyCode );
     List<BaseUser> selectMemberByGroupId(@Param("groupId") int groupId);
     List<BaseUser> selectLeaderByGroupId(@Param("groupId") int groupId);
     BaseUser selectByPerssId(Serializable var1);
     BaseUser getByuseraccount(@Param("username") String username,@Param("companyCode")String companyCode);
      BaseUser selectbyCompanyCode(@Param("mobilePhone")String mobilePhone,@Param("companyCode")String companyCode);
      List<Map<String,Object>> selectPageList(Map<String,Object> map);
        public List<Map<String,Object>> selectByCompanyCode(@Param("phone")String phone);
       BaseUser selectbyPipUser(@Param("userId") Long userId);
    int countByPageList(Map<String,Object> map);
    public BaseUser   selectByUser(@Param("companyCode")String companyCode ,@Param("username")String username,@Param("phone")String  phone);

    List<Map<String,Object>> selectPageCompanyCodeByUser(Map<String,Object> map);

    int countByPageCompanyCodeByUser(Map<String,Object> map);

    BaseUser getBySuperUser(@Param("username") String username);
}