package com.pms.mapper;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.baomidou.mybatisplus.mapper.Wrapper;
import com.pms.entity.BaseGroup;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * <p>
  *  Mapper 接口
 * </p>
 *
 * @author zyl
 * @since 2017-07-06
 */
public interface BaseGroupMapper extends BaseMapper<BaseGroup> {

     List<BaseGroup> selectListByGroup(@Param("userId") String userId);
     void deleteGroupMembersById(@Param("groupId") int groupId);
     void deleteGroupLeadersById(@Param("groupId") int groupId);
     void insertGroupMembersById(@Param("groupId") int groupId, @Param("userId") int userId);
     void insertGroupLeadersById(@Param("groupId") int groupId, @Param("userId") int userId);

     List<Map<String,Object>> getBygroupElement(@Param("menuType")int  menuType, @Param("groupId")Long groupId,@Param("groupType")Integer groupType);
     List<BaseGroup>  selectGroupList(@Param("companyCode") String companyCode, @Param("groupType")Integer groupType);
     List<BaseGroup> selectByPageList(Map<String,Object> map);
     int countByPageList(Map<String,Object> map);
}