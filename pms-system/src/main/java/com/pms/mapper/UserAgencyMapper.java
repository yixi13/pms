package com.pms.mapper;
import com.pms.entity.UserAgency;
import com.baomidou.mybatisplus.mapper.BaseMapper;
/**
 * 
 * 
 * @author zyl
 * @email 517770986@qq.com
 * @date 2017-11-28 10:32:10
 */
public interface UserAgencyMapper extends BaseMapper<UserAgency> {
	
}
