package com.pms.mapper;
import com.baomidou.mybatisplus.mapper.Wrapper;
import com.pms.entity.CompanyCode;
import com.baomidou.mybatisplus.mapper.BaseMapper;

import java.util.List;
import java.util.Map;

/**
 * 
 * 
 * @author ljb
 * @email 517770986@qq.com
 * @date 2018-07-17 14:57:26
 */
public interface CompanyCodeMapper extends BaseMapper<CompanyCode> {
    CompanyCode selectByCode(String code);

    List<CompanyCode> selectBypageMap(Map<String,Object> map);

    int countByPageList(Map<String,Object> map);
}
