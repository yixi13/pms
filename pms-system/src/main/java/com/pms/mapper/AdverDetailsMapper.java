package com.pms.mapper;
import com.pms.entity.AdverDetails;
import com.baomidou.mybatisplus.mapper.BaseMapper;

import java.io.Serializable;

/**
 * 
 * 
 * @author zyl
 * @email 517770986@qq.com
 * @date 2017-11-20 14:28:50
 */
public interface AdverDetailsMapper extends BaseMapper<AdverDetails> {
    AdverDetails queryById(Serializable id);

}
