package com.pms.mapper;
import com.pms.entity.LoginLog;
import com.baomidou.mybatisplus.mapper.BaseMapper;
/**
 * 
 *
 * @author lk
 * @email
 * @date 2018-04-18 10:47:17
 */
public interface LoginLogMapper extends BaseMapper<LoginLog> {
	
}
