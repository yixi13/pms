package com.pms.mapper;
import com.pms.entity.SysMember;
import com.baomidou.mybatisplus.mapper.BaseMapper;
/**
 * 
 * 
 * @author zyl
 * @email 517770986@qq.com
 * @date 2017-10-25 09:40:12
 */
public interface SysMemberMapper extends BaseMapper<SysMember> {
	
}
