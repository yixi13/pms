package com.pms.mapper;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.pms.entity.BaseResourceAuthority;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * <p>
  *  Mapper 接口
 * </p>
 *
 * @author zyl
 * @since 2017-07-06
 */
public interface BaseResourceAuthorityMapper extends BaseMapper<BaseResourceAuthority> {
    void deleteByAuthorityIdAndResourceType(@Param("authorityId") String authorityId, @Param("resourceType") String resourceType);
    List<BaseResourceAuthority> selectByGroupId(@Param("groupId") int groupId);
    List<BaseResourceAuthority> getAuthorityElement(@Param("authority_type") String authority_type, @Param("authority_id") String authority_id, @Param("resource_type") String resource_type);


    List<Map<String,Object>>  getAuthorityMenu(@Param("id")Long id);



}