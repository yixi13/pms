package com.pms.mapper;
import com.pms.entity.UserCommunity;
import com.baomidou.mybatisplus.mapper.BaseMapper;
/**
 * 用户社区关联表
 * 
 * @author zyl
 * @email 517770986@qq.com
 * @date 2018-03-19 10:46:43
 */

public interface UserCommunityMapper extends BaseMapper<UserCommunity> {
	
}
