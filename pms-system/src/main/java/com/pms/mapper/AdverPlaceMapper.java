package com.pms.mapper;
import com.pms.entity.AdverPlace;
import com.baomidou.mybatisplus.mapper.BaseMapper;
/**
 * 
 * 
 * @author zyl
 * @email 517770986@qq.com
 * @date 2017-11-20 14:28:50
 */
public interface AdverPlaceMapper extends BaseMapper<AdverPlace> {
	
}
