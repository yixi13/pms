package com.pms.mapper;
import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.pms.entity.BaseAgency;

/**
 * 
 * 
 * @author ljb
 * @email 517770986@qq.com
 * @date 2018-05-24 15:49:53
 */
public interface BaseAgencyMapper extends BaseMapper<BaseAgency> {
	
}
