package com.pms.mapper;

import com.pms.entity.SysNoticeUser;
import com.baomidou.mybatisplus.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;

/**
 * <p>
  *  Mapper 接口
 * </p>
 *
 * @author ljb
 * @since 2018-07-17
 */
public interface SysNoticeUserMapper extends BaseMapper<SysNoticeUser> {

    void deleteByNoticeId(Long noticeId);

    Integer selectCountNotReade(@Param("userId") Long userId, @Param("source") Integer source,@Param("companyCode") String companyCode);
}