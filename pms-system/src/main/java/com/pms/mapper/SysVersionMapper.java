package com.pms.mapper;
import com.pms.entity.SysVersion;
import com.baomidou.mybatisplus.mapper.BaseMapper;
/**
 * 
 * 
 * @author lk
 * @date 2018-07-18 13:44:01
 */
public interface SysVersionMapper extends BaseMapper<SysVersion> {
	
}
