package com.pms.mapper;
import com.pms.entity.SysMemberAddress;
import com.baomidou.mybatisplus.mapper.BaseMapper;
/**
 * 
 * 
 * @author zyl
 * @email 517770986@qq.com
 * @date 2017-10-25 15:05:11
 */
public interface SysMemberAddressMapper extends BaseMapper<SysMemberAddress> {
	
}
