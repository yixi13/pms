package com.pms.mapper;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.pms.entity.GateLog;

/**
 * <p>
  *  Mapper 接口
 * </p>
 *
 * @author zyl
 * @since 2017-07-06
 */
public interface GateLogMapper extends BaseMapper<GateLog> {

}