package com.pms.mapper;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.pms.entity.BaseElement;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * <p>
  *  Mapper 接口
 * </p>
 *
 * @author zyl
 * @since 2017-07-06
 */
public interface BaseElementMapper extends BaseMapper<BaseElement> {
     List<BaseElement> selectAuthorityElementByUserId(@Param("userType")int userType,@Param("groupType")int groupType,@Param("userId")String userId);
     List<BaseElement> selectAuthorityMenuElementByUserId(@Param("userId") String userId, @Param("menuId") String menuId);
     Map<String, Object> getApiPermissionElement(@Param("userId")Long userId, @Param("userType")int userType,@Param("groupType") int groupType,@Param("method") String method,@Param("title") String title,@Param("menuType")int menuType);

     List<BaseElement> getAndSuperAdminAuthorityElement(@Param("userId")String userId);
}