package com.pms.service;
import com.baomidou.mybatisplus.mapper.Wrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.service.IService;
import com.pms.entity.BaseGroupMember;
import com.pms.entity.BaseUser;
import com.pms.entity.CompanyCode;

import java.util.Map;

/**
 * 
 *
 * @author ljb
 * @email 517770986@qq.com
 * @date 2018-07-17 14:57:26
 */

public interface ICompanyCodeService extends  IBaseService<CompanyCode> {

    String getInviteCode(Integer len);

    Boolean insert_user_groupMember(CompanyCode entity,BaseUser user,BaseGroupMember groupMember);

    Page<CompanyCode> selectBypageMap(Page<CompanyCode> page, Map<String,Object> map);
}