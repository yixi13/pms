package com.pms.service;
import com.baomidou.mybatisplus.service.IService;
import com.pms.entity.AdverPlace;
/**
 * 
 *
 * @author zyl
 * @email 517770986@qq.com
 * @date 2017-11-20 14:28:50
 */
public interface IAdverPlaceService extends  IService<AdverPlace> {

}