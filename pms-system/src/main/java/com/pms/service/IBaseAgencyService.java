package com.pms.service;
import com.baomidou.mybatisplus.service.IService;
import com.pms.entity.BaseAgency;
/**
 * 
 *
 * @author ljb
 * @email 517770986@qq.com
 * @date 2018-05-24 15:49:53
 */

public interface IBaseAgencyService extends  IBaseService<BaseAgency> {

}