package com.pms.service;

import com.baomidou.mybatisplus.mapper.Wrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.service.IService;
import com.pms.entity.BaseGroup;
import com.pms.entity.vo.GroupUsers;

import java.util.List;
import java.util.Map;


/**
 * <p>
 *  服务类
 * </p>
 *
 * @author zyl
 * @since 2017-07-06
 */
public interface IBaseGroupService extends IService<BaseGroup> {
     void modifyGroupUsers(int groupId, String members, String leaders);

    /**
     * 根据组Id获取组群关联用户
     * @param groupId
     * @return
     */
    GroupUsers getGroupUsers(int groupId);

    List<BaseGroup> queryListByGroup(String userId);
    List<BaseGroup>  selectGroupList(String companyCode ,Integer groupType);

    List<Map<String,Object>> getBygroupElement(int  menuType,Long groupId,Integer groupType);

    Page<BaseGroup> selectByPageList(Page<BaseGroup> page, Map<String,Object> map);
}
