package com.pms.service.impl;

import org.springframework.stereotype.Service;
import com.pms.entity.BaseAgency;
import com.pms.mapper.BaseAgencyMapper;
import com.pms.service.IBaseAgencyService;
/**
 * 
 *
 * @author ljb
 * @email 517770986@qq.com
 * @date 2018-05-24 15:49:53
 */
@Service
public class BaseAgencyServiceImpl extends BaseServiceImpl<BaseAgencyMapper,BaseAgency> implements IBaseAgencyService {

}