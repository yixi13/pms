package com.pms.service.impl;

import org.springframework.stereotype.Service;
import com.pms.entity.SysVersion;
import com.pms.mapper.SysVersionMapper;
import com.pms.service.ISysVersionService;
/**
 * 
 *
 * @author lk
 * @date 2018-07-18 13:44:01
 */
@Service
public class SysVersionServiceImpl extends BaseServiceImpl<SysVersionMapper,SysVersion> implements ISysVersionService {

}