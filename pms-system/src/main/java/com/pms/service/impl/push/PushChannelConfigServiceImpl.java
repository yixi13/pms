package com.pms.service.impl.push;
import com.pms.entity.push.PushChannelConfig;
import com.pms.mapper.push.PushChannelConfigMapper;
import com.pms.service.impl.BaseServiceImpl;
import com.pms.service.push.IPushChannelConfigService;
import org.springframework.stereotype.Service;
/**
 * 
 *
 * @author zyl
 * @email 517770986@qq.com
 * @date 2017-12-01 11:12:07
 */
@Service
public class PushChannelConfigServiceImpl extends BaseServiceImpl<PushChannelConfigMapper,PushChannelConfig> implements IPushChannelConfigService {

}