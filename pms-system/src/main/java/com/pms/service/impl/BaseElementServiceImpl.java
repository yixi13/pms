package com.pms.service.impl;

import com.baomidou.mybatisplus.service.impl.ServiceImpl;

import com.pms.entity.BaseElement;
import com.pms.mapper.BaseElementMapper;
import com.pms.service.IBaseElementService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author zyl
 * @since 2017-07-06
 */
@Service
public class BaseElementServiceImpl extends ServiceImpl<BaseElementMapper, BaseElement> implements IBaseElementService {
    @Autowired
    BaseElementMapper baseElementMapper;
    @Override
    public List<BaseElement> getAuthorityElementByUserId(String userId, String menuId) {
        return baseElementMapper.selectAuthorityMenuElementByUserId(userId,menuId);
    }

    @Override
    public List<BaseElement> getAuthorityElement(int userType,int groupType,String userId) {
        return baseElementMapper.selectAuthorityElementByUserId(userType,groupType,userId);
    }
    @Override
    public Map<String, Object> getApiPermissionElement(Long userId, int userType, int groupType, String method, String title,int menuType){
        return baseElementMapper.getApiPermissionElement(userId,userType,groupType,method,title,menuType);
    }

    @Override
    public List<BaseElement> getAndSuperAdminAuthorityElement(String userId) {
        return baseElementMapper.getAndSuperAdminAuthorityElement(userId);
    }
}
