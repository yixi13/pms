package com.pms.service.impl;

import org.springframework.stereotype.Service;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.pms.entity.AdverAgency;
import com.pms.mapper.AdverAgencyMapper;
import com.pms.service.IAdverAgencyService;
/**
 * 
 *
 * @author zyl
 * @email 517770986@qq.com
 * @date 2017-11-20 14:28:50
 */
@Service
public class AdverAgencyServiceImpl extends ServiceImpl<AdverAgencyMapper,AdverAgency> implements IAdverAgencyService {


}