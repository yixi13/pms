package com.pms.service.impl;

import org.springframework.stereotype.Service;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.pms.entity.AdverPlace;
import com.pms.mapper.AdverPlaceMapper;
import com.pms.service.IAdverPlaceService;
/**
 * 
 *
 * @author zyl
 * @email 517770986@qq.com
 * @date 2017-11-20 14:28:50
 */
@Service
public class  AdverPlaceServiceImpl extends ServiceImpl<AdverPlaceMapper,AdverPlace> implements IAdverPlaceService {

}