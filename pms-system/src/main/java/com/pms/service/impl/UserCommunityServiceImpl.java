package com.pms.service.impl;

import com.baomidou.mybatisplus.mapper.SqlHelper;
import com.baomidou.mybatisplus.mapper.Wrapper;
import com.pms.cache.annotation.Cache;
import com.pms.entity.BaseUser;
import org.springframework.stereotype.Service;
import com.pms.entity.UserCommunity;
import com.pms.mapper.UserCommunityMapper;
import com.pms.service.IUserCommunityService;
/**
 * 用户社区关联表
 *
 * @author zyl
 * @email 517770986@qq.com
 * @date 2018-03-19 10:46:43
 */
@Service
public class UserCommunityServiceImpl extends BaseServiceImpl<UserCommunityMapper,UserCommunity> implements IUserCommunityService {

    @Override
    public UserCommunity selectOne(Wrapper<UserCommunity> wrapper) {
        return SqlHelper.getObject(baseMapper.selectList(wrapper));
    }


}