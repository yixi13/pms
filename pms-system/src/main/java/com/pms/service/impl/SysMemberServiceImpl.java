package com.pms.service.impl;

import com.baomidou.mybatisplus.mapper.SqlHelper;
import com.baomidou.mybatisplus.mapper.Wrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.pms.cache.annotation.BaseCache;
import com.pms.cache.annotation.Cache;
import org.springframework.stereotype.Service;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.pms.entity.SysMember;
import com.pms.mapper.SysMemberMapper;
import com.pms.service.ISysMemberService;

import java.io.Serializable;

/**
 * 
 *
 * @author zyl
 * @email 517770986@qq.com
 * @date 2017-10-25 09:40:12
 */
@Service
public class SysMemberServiceImpl extends BaseServiceImpl<SysMemberMapper,SysMember> implements ISysMemberService {

    @Cache(key = "sysMember:selectById{1}")
    public SysMember selectById(Serializable id){
        SysMember user=baseMapper.selectById(id);
        user.setPassWord("");
        return user;
    }


    public Page<SysMember> selectPage(Page<SysMember> page, Wrapper<SysMember> wrapper) {
        SqlHelper.fillWrapper(page, wrapper);
        page.setRecords(baseMapper.selectPage(page, wrapper));
        return page;
    }
}