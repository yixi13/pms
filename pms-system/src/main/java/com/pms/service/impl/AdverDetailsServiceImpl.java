package com.pms.service.impl;

import com.pms.mapper.BaseElementMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.pms.entity.AdverDetails;
import com.pms.mapper.AdverDetailsMapper;
import com.pms.service.IAdverDetailsService;

import java.io.Serializable;

/**
 * 
 *
 * @author zyl
 * @email 517770986@qq.com
 * @date 2017-11-20 14:28:50
 */
@Service
public class AdverDetailsServiceImpl extends ServiceImpl<AdverDetailsMapper,AdverDetails> implements IAdverDetailsService {
    @Autowired
    AdverDetailsMapper adverDetailsMapper;


    public AdverDetails queryById(Serializable id) {
        return adverDetailsMapper.queryById(id);
    }


}
