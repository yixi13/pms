package com.pms.service.impl;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;

import com.pms.core.CommonConstant;
import com.pms.entity.BaseResourceAuthority;
import com.pms.entity.BaseUser;
import com.pms.entity.vo.AuthorityMenuTree;
import com.pms.mapper.BaseResourceAuthorityMapper;
import com.pms.service.IBaseResourceAuthorityService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author zyl
 * @since 2017-07-06
 */
@Service
public class BaseResourceAuthorityServiceImpl extends ServiceImpl<BaseResourceAuthorityMapper, BaseResourceAuthority> implements IBaseResourceAuthorityService {
   @Autowired
   BaseResourceAuthorityMapper baseResourceAuthorityMapper;

    @Override
    public List<Integer> getAuthorityElement(int groupId) {
        List<BaseResourceAuthority> authorities = baseResourceAuthorityMapper.getAuthorityElement(CommonConstant.AUTHORITY_TYPE_GROUP,groupId+"",CommonConstant.RESOURCE_TYPE_BTN);
        List<Integer> ids = new ArrayList<Integer>();
        for(BaseResourceAuthority auth:authorities){
            ids.add(Integer.parseInt(auth.getResourceId()));
        }
        return ids;
    }


    @Override
    public void modifyAuthorityElement(int groupId, int menuId, int elementId) {
        BaseResourceAuthority authority = new BaseResourceAuthority(CommonConstant.AUTHORITY_TYPE_GROUP,CommonConstant.RESOURCE_TYPE_BTN);
        authority.setAuthorityId(groupId + "");
        authority.setResourceId(elementId + "");
        authority.setParentId("-1");
        authority.setCrtTime(new Date());
        baseResourceAuthorityMapper.insert (authority);
    }

    @Override
    public void modifyAuthorityMenu(Long groupId, List<AuthorityMenuTree> menuTrees) {
        baseResourceAuthorityMapper.deleteByAuthorityIdAndResourceType(groupId+"", CommonConstant.RESOURCE_TYPE_MENU);
        BaseResourceAuthority authority = null;
        for(AuthorityMenuTree menuTree:menuTrees){
            authority = new BaseResourceAuthority(CommonConstant.AUTHORITY_TYPE_GROUP,CommonConstant.RESOURCE_TYPE_MENU);
            authority.setAuthorityId(groupId+"");
            authority.setResourceId(menuTree.getId()+"");
            authority.setParentId("-1");
            authority.setCrtTime(new Date());
            authority.setPermissionsId(menuTree.getPermissionsId());
            baseResourceAuthorityMapper.insert (authority);
        }
    }
}
