package com.pms.service.impl.push;
import cn.jpush.api.push.PushResult;
import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.pms.entity.push.PushChannelConfig;
import com.pms.entity.push.PushKeyConfig;
import com.pms.entity.push.PushMsgRecord;
import com.pms.entity.push.PushMsgRecordMember;
import com.pms.exception.RRException;
import com.pms.service.push.IPushChannelConfigService;
import com.pms.service.push.IPushKeyConfigService;
import com.pms.service.push.IPushMsgRecordMemberService;
import com.pms.service.push.IPushMsgRecordService;
import com.pms.util.JpushClientUtil;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class PushServiceImpl {

	@Autowired
	private IPushChannelConfigService pushChannelConfigService;
	@Autowired
	private IPushKeyConfigService pushKeyConfigService;
	@Autowired
	private IPushMsgRecordService pushMsgRecordService;
	@Autowired
	private IPushMsgRecordMemberService pushMsgRecordMemberService;
	
	
	protected final Logger logger = LogManager.getLogger(this.getClass());
	/**
	 * 极光单推  Alias 作为 别名推送
	 * @param agencyId		app机构id
	 * @param memberId		推送用户id
	 * @param MSG_TITLE		推送标题(可以为空)
	 * @param MSG_INFO		推送详情
	 * @param msgType		消息类别:{11   用户投诉信息-->物业,12 物业投诉信息-->用户 21:用户报修信息-->物业，22:物业报修信息-->用户，
	 * 								3订单，4系统，5社区公告，6物业交费}
	 */
	public void JpushMsgByAlias(Long agencyId,Long memberId,
			String MSG_TITLE,String MSG_INFO,Integer msgType){
		if (null==memberId){
			throw  new RRException("会员ID不能为空",400);
		}
		if (null==agencyId){
			throw  new RRException("机构ID不能为空",400);
		}
		if (null==MSG_INFO){
			throw  new RRException("推送详情不能为空",400);
		}
		//获取配置的key
		PushKeyConfig key = pushKeyConfigService.selectOne(new EntityWrapper<PushKeyConfig>().eq("agency_id",agencyId));
		if(key != null){
			String api_key = key.getApiKey();//改用极光后 ios/Android 通用一个key
			String secret_key= key.getSecretKey();
			//获取对应设备号
			PushChannelConfig channelConfig = pushChannelConfigService.selectOne(new EntityWrapper<PushChannelConfig>().eq("member_id",memberId));
			if(channelConfig != null){
				String alias = channelConfig.getAlias();//极光推送设备唯一号
				//推送
				PushResult result = JpushClientUtil.pushByAlias(api_key,secret_key,alias,MSG_INFO,msgType);
				//保存消息记录
				if(result != null && result.isResultOK()){
					PushMsgRecord record = new PushMsgRecord();
					record.setAgencyId(agencyId);
					record.setMsgTitle(MSG_TITLE);
					record.setMsgInfo(MSG_INFO);
					record.setMsgType(msgType);
					record.setSendType(1);//1单推by Alias; 2组推by tag
					 pushMsgRecordService.insert(record);
					
					PushMsgRecordMember msgMember = new PushMsgRecordMember();
					msgMember.setIsRead(2);
					msgMember.setMemberId(memberId);
					msgMember.setMsgId(record.getMsgId());
					pushMsgRecordMemberService.insert(msgMember);
				}else{
					logger.error("针对别名" + alias + "的信息推送失败！");
				}
			}else{
				logger.error("该手机用户Alias信息为空!");
			}
		}else{
			logger.error("该app机构没有配置推送信息!");
		}
	}

	/**
	 * 极光组推  Tag  分组推送
	 * @param agencyId		app机构id
	 * @param MSG_TITLE		推送标题(可以为空)
	 * @param MSG_INFO		推送详情
	 * @param tag			社区机构id
	 * @param msgType		消息类别:{11   用户投诉信息-->物业,12 物业投诉信息-->用户 21:用户报修信息-->物业，22:物业报修信息-->用户，
	 * 								3订单，4系统，5社区公告，6物业交费}
	 */
	public void JpushMsgByTag(Long agencyId, String MSG_TITLE,String MSG_INFO,
			Integer msgType,String[] tag){
		if (null==agencyId){
			throw  new RRException("机构ID不能为空",400);
		}
		if (null==MSG_INFO){
			throw  new RRException("推送详情不能为空",400);
		}
		//获取配置的key
		PushKeyConfig key = pushKeyConfigService.selectOne(new EntityWrapper<PushKeyConfig>().eq("agency_id",agencyId));
		if(key != null){
			String api_key = key.getApiKey();//改用极光后 ios/Android 通用一个key
			String secret_key= key.getSecretKey();
			//推送
			PushResult result = JpushClientUtil.pushByTag(api_key, secret_key, tag, MSG_INFO, msgType);
			//保存消息记录
			if(result != null && result.isResultOK()){
				PushMsgRecord record = new PushMsgRecord();
				record.setAgencyId(agencyId);
				record.setMsgTitle(MSG_TITLE);
				record.setMsgInfo(MSG_INFO);
				record.setMsgType(msgType);
				record.setSendType(2);//1单推by Alias; 2组推by tag
				pushMsgRecordService.insert(record);
			}else{
				logger.error("针对tag" + tag + "的信息推送失败！");
			}
		}else{
			logger.error("该app机构没有配置推送信息!");
		}
	}
}
