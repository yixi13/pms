package com.pms.service.impl;

import com.baomidou.mybatisplus.service.impl.ServiceImpl;

import com.pms.entity.BaseGroupLeader;
import com.pms.mapper.BaseGroupLeaderMapper;
import com.pms.service.IBaseGroupLeaderService;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author zyl
 * @since 2017-07-06
 */
@Service
public class BaseGroupLeaderServiceImpl extends ServiceImpl<BaseGroupLeaderMapper, BaseGroupLeader> implements IBaseGroupLeaderService {
	
}
