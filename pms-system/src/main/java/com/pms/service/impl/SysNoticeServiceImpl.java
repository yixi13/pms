package com.pms.service.impl;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.mapper.SqlHelper;
import com.baomidou.mybatisplus.plugins.Page;
import com.pms.entity.BaseUser;
import com.pms.entity.SysNotice;
import com.pms.entity.SysNoticeUser;
import com.pms.mapper.SysNoticeMapper;
import com.pms.service.IBaseUserService;
import com.pms.service.ISysNoticeService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.pms.service.ISysNoticeUserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author ljb
 * @since 2018-07-17
 */
@Service
public class SysNoticeServiceImpl extends ServiceImpl<SysNoticeMapper, SysNotice> implements ISysNoticeService {

    @Autowired
    private IBaseUserService iBaseUserService;
    @Autowired
    private ISysNoticeUserService iSysNoticeUserService;

    Logger logger = LoggerFactory.getLogger(this.getClass());

    @Override
    @Transactional
    public void insertAndUser(SysNotice sysNotice) {
        this.insert(sysNotice);
        List<BaseUser> listUser = iBaseUserService.selectList(new EntityWrapper<BaseUser>());
        List<SysNoticeUser> listUserNotice = new ArrayList<SysNoticeUser>();
        for (BaseUser user : listUser) {
            SysNoticeUser noticeUser = new SysNoticeUser();
            noticeUser.setNoticeId(sysNotice.getId());
            noticeUser.setUserId(user.getId());
            noticeUser.setIsRead(2);
            noticeUser.setCreateDate(new Date());
            listUserNotice.add(noticeUser);
        }
        iSysNoticeUserService.insertBatch(listUserNotice);

    }

    @Override
    public Page<Map<String, Object>> selectSysNoticeMapsPage(Page<Map<String, Object>> page, EntityWrapper<Map<String, Object>> ew) {
        SqlHelper.fillWrapper(page, ew);
        logger.info(ew.getSqlSegment());
        page.setRecords(baseMapper.selectSysNoticeMapsPage(page, ew));
        return page;
    }

    @Override
    public List<SysNotice> selectEffNoticeList() {
        return baseMapper.selectEffNoticeList();
    }
}
