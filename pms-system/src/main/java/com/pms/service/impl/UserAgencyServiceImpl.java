package com.pms.service.impl;

import org.springframework.stereotype.Service;
import com.pms.entity.UserAgency;
import com.pms.mapper.UserAgencyMapper;
import com.pms.service.IUserAgencyService;
/**
 * 
 *
 * @author zyl
 * @email 517770986@qq.com
 * @date 2017-11-28 10:32:10
 */
@Service
public class UserAgencyServiceImpl extends BaseServiceImpl<UserAgencyMapper,UserAgency> implements IUserAgencyService {

}