package com.pms.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.pms.entity.AreaCn;
import com.pms.mapper.AreaCnMapper;
import com.pms.service.IAreaCnService;

import java.util.List;
import java.util.Map;

/**
 * 
 *
 * @author zyl
 * @email 517770986@qq.com
 * @date 2017-12-21 16:39:49
 */
@Service
public class AreaCnServiceImpl extends BaseServiceImpl<AreaCnMapper,AreaCn> implements IAreaCnService {

    @Autowired
    private AreaCnMapper areaCnMapper;
    @Override
    public List<AreaCn> getProvince() {
        return areaCnMapper.getProvince();
    }


    @Override
    public List<AreaCn> getCitys(Integer gbCode) {
        return areaCnMapper.getCitys(gbCode);
    }

    @Override
    public List<AreaCn> getCountys(Integer gbCode) {
        return areaCnMapper.getCountys(gbCode);
    }

    @Override
    public List<Map<String,Object>> getAreaCnMap(Map<String,Object> paramMap){return areaCnMapper.getAreaCnMap(paramMap);}
}