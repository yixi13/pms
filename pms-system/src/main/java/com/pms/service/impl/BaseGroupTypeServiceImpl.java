package com.pms.service.impl;

import com.baomidou.mybatisplus.service.impl.ServiceImpl;

import com.pms.entity.BaseGroupType;
import com.pms.mapper.BaseGroupTypeMapper;
import com.pms.service.IBaseGroupTypeService;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author zyl
 * @since 2017-07-06
 */
@Service
public class BaseGroupTypeServiceImpl extends ServiceImpl<BaseGroupTypeMapper, BaseGroupType> implements IBaseGroupTypeService {
	
}
