package com.pms.service.impl;

import com.baomidou.mybatisplus.mapper.SqlHelper;
import com.baomidou.mybatisplus.mapper.Wrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.pms.cache.annotation.BaseCache;
import com.pms.cache.annotation.Cache;
import com.pms.entity.BaseGroup;
import com.pms.entity.BaseUser;
import com.pms.mapper.BaseUserMapper;
import com.pms.service.IBaseUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author zyl
 * @since 2017-07-06
 */
@Service
public class BaseUserServiceImpl extends BaseServiceImpl<BaseUserMapper, BaseUser> implements IBaseUserService {
   @Autowired
   BaseUserMapper baseUserMapper;

    public BaseUser selectById_User(Long id) {
        BaseUser user= baseMapper.selectById(id);
        user.setPassword("");
        return user;
    }


    @Override
    public BaseUser getUserByUsername(String username,String companyCode ) {

        return baseUserMapper.getBaseUserByUserName(username,companyCode);
    }

    @Override
    public BaseUser selectByPerssId(Long id) {
        BaseUser user=baseMapper.selectByPerssId(id);
        return user;
    }
    @Override
    public BaseUser selectbyCompanyCode(String mobilePhone,String companyCode){
        BaseUser user=baseUserMapper.selectbyCompanyCode(mobilePhone,companyCode);
        return user;
    }

    public BaseUser   selectByUser(String companyCode ,String username,String  phone){
        BaseUser user=baseUserMapper.selectByUser(companyCode,username,phone);
        return user;
    }

    @Override
//    @Cache(key ="selectOne{1.paramNameValuePairs}")
    public BaseUser selectOne(Wrapper<BaseUser> wrapper) {
        return SqlHelper.getObject(baseMapper.selectList(wrapper));
    }

    @Override
    public BaseUser getByuseraccount(String username,String companyCode){
        BaseUser baseUser = baseUserMapper.getByuseraccount(username,companyCode);
        return  baseUser;
    }

    public  Page<Map<String,Object>> selectPageList( Page<Map<String,Object>>  page, Map<String,Object> map){
        int count=baseUserMapper.countByPageList(map);
        List<Map<String,Object>> list= baseMapper.selectPageList(map);
        page.setRecords(list);
        page.setSize(Integer.parseInt(map.get("limit").toString()));
        page.setTotal(count);
        return  page;
    }
    /**
     * 提供给超级平台所用
     * @param page
     * @param map
     * @return
     */
    public  Page<Map<String,Object>> selectPageCompanyCodeByUser( Page<Map<String,Object>>  page, Map<String,Object> map){
        int count=baseUserMapper.countByPageCompanyCodeByUser(map);
        List<Map<String,Object>> list= baseMapper.selectPageCompanyCodeByUser(map);
        page.setRecords(list);
        page.setTotal(count);
        return  page;
    }

    public List<Map<String,Object>> selectByCompanyCode(String phone){
        return  baseUserMapper.selectByCompanyCode(phone);
    }

    public   BaseUser selectbyPipUser(Long userId){
        return baseUserMapper.selectbyPipUser(userId);
    }


    @Override
    public BaseUser getBySuperUser(String username){
        BaseUser baseUser = baseUserMapper.getBySuperUser(username);
        return  baseUser;
    }
}
