package com.pms.service.impl;

import com.baomidou.mybatisplus.service.impl.ServiceImpl;

import com.pms.entity.GateLog;
import com.pms.mapper.GateLogMapper;
import com.pms.service.IGateLogService;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author zyl
 * @since 2017-07-06
 */
@Service
public class GateLogServiceImpl extends ServiceImpl<GateLogMapper, GateLog> implements IGateLogService {
	
}
