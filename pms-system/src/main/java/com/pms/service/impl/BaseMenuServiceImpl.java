package com.pms.service.impl;

import com.baomidou.mybatisplus.service.impl.ServiceImpl;

import com.pms.core.CommonConstant;
import com.pms.entity.BaseMenu;
import com.pms.entity.vo.AuthorityMenuTree;
import com.pms.mapper.BaseMenuMapper;
import com.pms.service.IBaseMenuService;
import com.pms.util.TreeUtil;
import org.apache.ibatis.annotations.Param;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author zyl
 * @since 2017-07-06
 */
@Service
public class BaseMenuServiceImpl extends ServiceImpl<BaseMenuMapper, BaseMenu> implements IBaseMenuService {
  @Autowired
  BaseMenuMapper baseMenuMapper;

    @Override
    public List<BaseMenu> selectAuthoritySystemByUserId(int userId) {
        return baseMenuMapper.selectAuthoritySystemByUserId(userId);
    }

    public List<BaseMenu> selectListTree(){
        return this.baseMapper.selectListTree();
    }

    @Override
    public List<AuthorityMenuTree> getAuthorityMenu(int groupId) {
        List<BaseMenu> menus = baseMenuMapper.selectMenuByAuthorityId(String.valueOf(groupId), CommonConstant.AUTHORITY_TYPE_GROUP);
        List<AuthorityMenuTree> trees = new ArrayList<AuthorityMenuTree>();
        AuthorityMenuTree node = null;
        for (BaseMenu menu : menus) {
            node = new AuthorityMenuTree();
            node.setText(menu.getTitle());
            BeanUtils.copyProperties(menu, node);
            trees.add(node);
        }
        return trees;
    }


    @Override
    public List<BaseMenu> getUserAuthorityMenuByUserId(int id) {
        return baseMenuMapper.selectAuthorityMenuByUserId(id);
    }

    @Override
    public List<BaseMenu> selectAuthorityMenu(Long userId,Integer menuType,Integer type)    {
        return baseMenuMapper.selectAuthorityMenu(userId,menuType,type);
    }

    @Override
    public List<BaseMenu> getUserAuthoritySystemByUserId(int id) {
        return baseMenuMapper.selectAuthoritySystemByUserId(id);
    }
    public List<BaseMenu>  selectTreeTrue(Long userId){
        return baseMenuMapper.selectTreeTrue(userId);
    }

    public List<BaseMenu> ApiSelectAuthorityMenu(Long userId,Integer type){
        return baseMenuMapper.ApiSelectAuthorityMenu(userId,type);
    }
    public  Map<String, Object> getApiPermissionMenu(Long userId, Integer  menuType, String title){
        return baseMenuMapper.getApiPermissionMenu(userId,menuType,title);
    }


    public List<BaseMenu> getAuthorityMenuAndSuperadmin(Long userId){
        return baseMenuMapper.getAuthorityMenuAndSuperadmin(userId);
    }
}
