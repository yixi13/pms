package com.pms.service.impl;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;

import com.pms.entity.BaseGroup;
import com.pms.entity.vo.GroupUsers;
import com.pms.mapper.BaseGroupMapper;
import com.pms.mapper.BaseUserMapper;
import com.pms.service.IBaseGroupService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.util.List;
import java.util.Map;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author zyl
 * @since 2017-07-06
 */
@Service
public class BaseGroupServiceImpl extends ServiceImpl<BaseGroupMapper, BaseGroup> implements IBaseGroupService {
    @Autowired
    BaseGroupMapper baseGroupMapper;
    @Autowired
    BaseUserMapper baseUserMapper;

    @Override
    public GroupUsers getGroupUsers(int groupId) {
        return new GroupUsers(baseUserMapper.selectMemberByGroupId(groupId),baseUserMapper.selectLeaderByGroupId(groupId));
    }
    public List<BaseGroup> queryListByGroup(String userId) {
        return baseGroupMapper.selectListByGroup(userId);
    }


    @Override
    public void modifyGroupUsers(int groupId, String members, String leaders) {
        baseGroupMapper.deleteGroupLeadersById(groupId);
        baseGroupMapper.deleteGroupMembersById(groupId);
        if(!StringUtils.isEmpty(members)){
            String[] mem = members.split(",");
            for(String m:mem){
                baseGroupMapper.insertGroupMembersById(groupId,Integer.parseInt(m));
            }
        }
        if(!StringUtils.isEmpty(leaders)){
            String[] mem = leaders.split(",");
            for(String m:mem){
                baseGroupMapper.insertGroupLeadersById(groupId,Integer.parseInt(m));
            }
        }
    }

    @Override
    public  List<BaseGroup>  selectGroupList(String companyCode ,Integer groupType){
        return  baseGroupMapper.selectGroupList(companyCode,groupType);
    }

    @Override
    public  List<Map<String,Object>> getBygroupElement(int  menuType,Long groupId,Integer groupType){
        return  baseGroupMapper.getBygroupElement(menuType,groupId,groupType);
    }

    public Page<BaseGroup> selectByPageList(Page<BaseGroup> page, Map<String,Object> map){
        int count=baseGroupMapper.countByPageList(map);
        List<BaseGroup> list= baseGroupMapper.selectByPageList(map);
        page.setRecords(list);
        page.setSize(Integer.parseInt(map.get("limit").toString()));
        page.setTotal(count);
        return  page;
    }
}
