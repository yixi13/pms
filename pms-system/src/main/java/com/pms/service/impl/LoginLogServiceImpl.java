package com.pms.service.impl;

import org.springframework.stereotype.Service;
import com.pms.entity.LoginLog;
import com.pms.mapper.LoginLogMapper;
import com.pms.service.ILoginLogService;
/**
 * 
 *
 * @author lk
 * @email
 * @date 2018-04-18 10:47:17
 */
@Service
public class LoginLogServiceImpl extends BaseServiceImpl<LoginLogMapper,LoginLog> implements ILoginLogService {

}