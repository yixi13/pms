package com.pms.service.impl;
import com.pms.service.ISysMemberAddressService;
import org.springframework.stereotype.Service;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.pms.entity.SysMemberAddress;
import com.pms.mapper.SysMemberAddressMapper;

/**
 * 
 *
 * @author zyl
 * @email 517770986@qq.com
 * @date 2017-10-25 15:05:11
 */
@Service
public class SysMemberAddressServiceImpl extends ServiceImpl<SysMemberAddressMapper,SysMemberAddress> implements ISysMemberAddressService {



}