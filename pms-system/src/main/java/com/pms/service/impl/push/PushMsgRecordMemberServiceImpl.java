package com.pms.service.impl.push;

import com.pms.service.impl.BaseServiceImpl;
import org.springframework.stereotype.Service;
import com.pms.entity.push.PushMsgRecordMember;
import com.pms.mapper.push.PushMsgRecordMemberMapper;
import com.pms.service.push.IPushMsgRecordMemberService;
/**
 * 
 *
 * @author zyl
 * @email 517770986@qq.com
 * @date 2017-12-01 11:12:07
 */
@Service
public class PushMsgRecordMemberServiceImpl extends BaseServiceImpl<PushMsgRecordMemberMapper,PushMsgRecordMember> implements IPushMsgRecordMemberService {

}