package com.pms.service.impl.push;

import com.pms.entity.push.PushKeyConfig;
import com.pms.service.impl.BaseServiceImpl;
import org.springframework.stereotype.Service;
import com.pms.mapper.push.PushKeyConfigMapper;
import com.pms.service.push.IPushKeyConfigService;
/**
 * 
 *
 * @author zyl
 * @email 517770986@qq.com
 * @date 2017-12-01 11:12:07
 */
@Service
public class PushKeyConfigServiceImpl extends BaseServiceImpl<PushKeyConfigMapper,PushKeyConfig> implements IPushKeyConfigService {

}