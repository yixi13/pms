package com.pms.service.impl;

import com.pms.entity.SysNotice;
import com.pms.entity.SysNoticeUser;
import com.pms.mapper.SysNoticeUserMapper;
import com.pms.service.ISysNoticeService;
import com.pms.service.ISysNoticeUserService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author ljb
 * @since 2018-07-17
 */
@Service
public class SysNoticeUserServiceImpl extends ServiceImpl<SysNoticeUserMapper, SysNoticeUser> implements ISysNoticeUserService {

    @Autowired
    private ISysNoticeService iSysNoticeService;

    @Override
    public void addSysNoticeUserByNewUser(Long userId) {
        // 查询有效期的公告
        List<SysNotice> noticeList = iSysNoticeService.selectEffNoticeList();

        if (noticeList.size() > 0) {
            List<SysNoticeUser> noticeUserList = new ArrayList<>();
            for (SysNotice notice : noticeList) {
                SysNoticeUser snu = new SysNoticeUser();
                snu.setIsRead(2);
                snu.setCreateDate(new Date());
                snu.setNoticeId(notice.getId());
                snu.setUserId(userId);
                noticeUserList.add(snu);
            }
            this.insertBatch(noticeUserList);
        }
    }

    @Override
    public void deleteByNoticeId(Long noticeId) {
        baseMapper.deleteByNoticeId(noticeId);
    }

    @Override
    public Integer selectCountNotReade(Long userId, Integer source, String companyCode) {
        return baseMapper.selectCountNotReade(userId, source,companyCode);
    }
}
