package com.pms.service.impl;

import com.baomidou.mybatisplus.service.impl.ServiceImpl;

import com.pms.entity.BaseGroupMember;
import com.pms.mapper.BaseGroupMemberMapper;
import com.pms.service.IBaseGroupMemberService;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author zyl
 * @since 2017-07-06
 */
@Service
public class BaseGroupMemberServiceImpl extends ServiceImpl<BaseGroupMemberMapper, BaseGroupMember> implements IBaseGroupMemberService {
	
}
