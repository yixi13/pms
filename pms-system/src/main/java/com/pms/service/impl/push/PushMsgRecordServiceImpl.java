package com.pms.service.impl.push;

import com.pms.service.impl.BaseServiceImpl;
import org.springframework.stereotype.Service;
import com.pms.entity.push.PushMsgRecord;
import com.pms.mapper.push.PushMsgRecordMapper;
import com.pms.service.push.IPushMsgRecordService;
/**
 * 
 *
 * @author zyl
 * @email 517770986@qq.com
 * @date 2017-12-01 11:12:07
 */
@Service
public class PushMsgRecordServiceImpl extends BaseServiceImpl<PushMsgRecordMapper,PushMsgRecord> implements IPushMsgRecordService {

}