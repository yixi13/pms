package com.pms.service;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.pms.entity.SysNotice;
import com.baomidou.mybatisplus.service.IService;

import java.util.List;
import java.util.Map;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author ljb
 * @since 2018-07-17
 */
public interface ISysNoticeService extends IService<SysNotice> {

    void insertAndUser(SysNotice sysNotice);

    Page<Map<String,Object>> selectSysNoticeMapsPage(Page<Map<String, Object>> page, EntityWrapper<Map<String, Object>> ew);

    /**
     * 查询有效期内的公告list
     * @return
     */
    List<SysNotice> selectEffNoticeList();
}
