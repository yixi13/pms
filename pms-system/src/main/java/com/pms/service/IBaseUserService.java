package com.pms.service;
import com.baomidou.mybatisplus.mapper.Wrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.pms.entity.BaseUser;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author zyl
 * @since 2017-07-06
 */
public interface IBaseUserService extends IBaseService<BaseUser> {
    BaseUser getUserByUsername(String username,String companyCode);
    BaseUser selectByPerssId(Long var1);
    BaseUser getByuseraccount(String username,String companyCode);

    Page<Map<String,Object>> selectPageList( Page<Map<String,Object>>  var1, Map<String,Object> map);

    BaseUser selectbyCompanyCode(String mobilePhone,String companyCode);
    BaseUser selectById_User(Long id);
    List<Map<String,Object>> selectByCompanyCode(String phone);
    BaseUser selectbyPipUser(Long userId);
    BaseUser   selectByUser(String companyCode ,String username,String  phone);

    Page<Map<String,Object>> selectPageCompanyCodeByUser( Page<Map<String,Object>> var1, Map<String,Object> map);

    BaseUser getBySuperUser(String username);
}
