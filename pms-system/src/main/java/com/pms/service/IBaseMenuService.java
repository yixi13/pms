package com.pms.service;

import com.baomidou.mybatisplus.service.IService;
import com.pms.entity.BaseMenu;
import com.pms.entity.vo.AuthorityMenuTree;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author zyl
 * @since 2017-07-06
 */
public interface IBaseMenuService extends IService<BaseMenu> {
    List<BaseMenu> getUserAuthorityMenuByUserId(int id);
    List<BaseMenu> getUserAuthoritySystemByUserId(int id);
    List<BaseMenu> selectAuthorityMenu(Long userId,Integer menuType,Integer type);

    List<BaseMenu> ApiSelectAuthorityMenu(Long userId,Integer type);

    List<BaseMenu> selectListTree();
    /**
     * 获取组关联的菜单
     * @param groupId
     * @return
     */
    List<AuthorityMenuTree> getAuthorityMenu(int groupId);
    List<BaseMenu> selectAuthoritySystemByUserId(@Param("userId") int userId);
    public List<BaseMenu>  selectTreeTrue(Long userId);
    Map<String, Object> getApiPermissionMenu(Long userId, Integer  menuType, String title);

    List<BaseMenu> getAuthorityMenuAndSuperadmin(Long userId);
}
