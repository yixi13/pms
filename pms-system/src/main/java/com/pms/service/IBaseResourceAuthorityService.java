package com.pms.service;

import com.baomidou.mybatisplus.service.IService;
import com.pms.entity.BaseResourceAuthority;
import com.pms.entity.vo.AuthorityMenuTree;


import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author zyl
 * @since 2017-07-06
 */
public interface IBaseResourceAuthorityService extends IService<BaseResourceAuthority> {

    /**
     * 修改组群菜单
     * @param groupId
     * @param menuTrees 菜单树
     */
    void modifyAuthorityMenu(Long groupId, List<AuthorityMenuTree> menuTrees);

    void modifyAuthorityElement(int groupId, int menuId, int elementId);

    List<Integer> getAuthorityElement(int groupId);
}
