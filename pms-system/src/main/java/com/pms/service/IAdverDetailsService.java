package com.pms.service;
import com.baomidou.mybatisplus.service.IService;
import com.pms.entity.AdverDetails;

import java.io.Serializable;

/**
 * 
 *
 * @author zyl
 * @email 517770986@qq.com
 * @date 2017-11-20 14:28:50
 */
public interface IAdverDetailsService extends  IService<AdverDetails> {


    AdverDetails queryById(Serializable id);
}