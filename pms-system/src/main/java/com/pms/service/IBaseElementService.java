package com.pms.service;

import com.baomidou.mybatisplus.service.IService;
import com.pms.entity.BaseElement;

import java.util.List;
import java.util.Map;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author zyl
 * @since 2017-07-06
 */
public interface IBaseElementService extends IService<BaseElement> {
    List<BaseElement> getAuthorityElement(int userType,int groupType,String userId);
    List<BaseElement> getAuthorityElementByUserId(String userId, String menuId);
    Map<String, Object> getApiPermissionElement(Long userId,int userType, int groupType,String method,String title,int menuType);


    List<BaseElement> getAndSuperAdminAuthorityElement(String userId);
}
