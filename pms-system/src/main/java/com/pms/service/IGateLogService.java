package com.pms.service;

import com.baomidou.mybatisplus.service.IService;
import com.pms.entity.GateLog;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author zyl
 * @since 2017-07-06
 */
public interface IGateLogService extends IService<GateLog> {
	
}
