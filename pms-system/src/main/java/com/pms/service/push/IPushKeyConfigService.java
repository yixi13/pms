package com.pms.service.push;

import com.pms.entity.push.PushKeyConfig;
import com.pms.service.IBaseService;

/**
 * 
 *
 * @author zyl
 * @email 517770986@qq.com
 * @date 2017-12-01 11:12:07
 */

public interface IPushKeyConfigService extends IBaseService<PushKeyConfig> {

}