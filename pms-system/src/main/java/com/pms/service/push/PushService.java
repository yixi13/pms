package com.pms.service.push;

/**
 * Created by hm on 2017/12/1.
 */
public interface PushService  {
    /**单推*/
    void JpushMsgByAlias(Long agencyId, Long memberId, String MSG_TITLE, String MSG_INFO, Integer msgType);

    /**组推*/
    void JpushMsgByTag(Long agencyId, String MSG_TITLE, String MSG_INFO, Integer msgType, String[] tag);
}
