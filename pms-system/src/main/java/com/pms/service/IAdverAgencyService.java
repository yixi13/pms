package com.pms.service;
import com.baomidou.mybatisplus.service.IService;
import com.pms.entity.AdverAgency;
/**
 * 
 *
 * @author zyl
 * @email 517770986@qq.com
 * @date 2017-11-20 14:28:50
 */
public interface IAdverAgencyService extends  IService<AdverAgency> {

}