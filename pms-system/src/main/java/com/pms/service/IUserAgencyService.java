package com.pms.service;
import com.baomidou.mybatisplus.service.IService;
import com.pms.entity.UserAgency;
/**
 * 
 *
 * @author zyl
 * @email 517770986@qq.com
 * @date 2017-11-28 10:32:10
 */
public interface IUserAgencyService extends  IBaseService<UserAgency> {


}