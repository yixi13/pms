package com.pms.service;
import com.baomidou.mybatisplus.service.IService;
import com.pms.entity.AreaCn;

import java.util.List;
import java.util.Map;

/**
 * 
 *
 * @author zyl
 * @email 517770986@qq.com
 * @date 2017-12-21 16:39:49
 */
public interface IAreaCnService extends  IBaseService<AreaCn> {


    List<AreaCn> getProvince();

    List<AreaCn> getCitys(Integer gbCode);

    List<AreaCn> getCountys(Integer gbCode);

    List<Map<String,Object>> getAreaCnMap(Map<String,Object> paramMap);
}