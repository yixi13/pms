package com.pms.service;

import com.baomidou.mybatisplus.service.IService;
import com.pms.entity.SysMemberAddress;




/* *
 *
 * @author zyl
 * @email 517770986@qq.com
 * @date 2017-10-25 09:40:12
 */
public interface ISysMemberAddressService extends IService<SysMemberAddress> {


}