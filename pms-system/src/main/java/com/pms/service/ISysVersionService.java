package com.pms.service;
import com.baomidou.mybatisplus.service.IService;
import com.pms.entity.SysVersion;
/**
 * 
 *
 * @author lk
 * @date 2018-07-18 13:44:01
 */

public interface ISysVersionService extends  IBaseService<SysVersion> {

}