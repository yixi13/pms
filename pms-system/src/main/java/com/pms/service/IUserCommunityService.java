package com.pms.service;
import com.baomidou.mybatisplus.service.IService;
import com.pms.entity.UserCommunity;
/**
 * 用户社区关联表
 *
 * @author zyl
 * @email 517770986@qq.com
 * @date 2018-03-19 10:46:43
 */

public interface IUserCommunityService extends  IBaseService<UserCommunity> {

}