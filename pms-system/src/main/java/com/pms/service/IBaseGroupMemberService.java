package com.pms.service;

import com.baomidou.mybatisplus.service.IService;
import com.pms.entity.BaseGroupMember;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author zyl
 * @since 2017-07-06
 */
public interface IBaseGroupMemberService extends IService<BaseGroupMember> {
	
}
