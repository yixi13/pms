package com.pms.service;
import com.baomidou.mybatisplus.service.IService;
import com.pms.entity.LoginLog;
/**
 * 
 *
 * @author lk
 * @email
 * @date 2018-04-18 10:47:17
 */

public interface ILoginLogService extends  IBaseService<LoginLog> {

}