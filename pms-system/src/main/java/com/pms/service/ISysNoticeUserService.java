package com.pms.service;

import com.pms.entity.SysNoticeUser;
import com.baomidou.mybatisplus.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author ljb
 * @since 2018-07-17
 */
public interface ISysNoticeUserService extends IService<SysNoticeUser> {
    /**
     * 添加用户时  添加公告关联信息
     * @param userId
     */
    void addSysNoticeUserByNewUser(Long userId);

    /**
     * 根据 通知ID删除中间表信息
     * @param noticeId
     */
    void deleteByNoticeId(Long noticeId);

    Integer selectCountNotReade(Long userId, Integer source, String companyCode);
}
