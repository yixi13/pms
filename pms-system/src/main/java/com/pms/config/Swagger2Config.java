package com.pms.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

/**
 * Swagger2配置
 *
 * @author zyl
 * @create 2017-06-09 16:45
 **/
@Configuration
@EnableSwagger2
public class Swagger2Config {
    public static final String SWAGGER_SCAN_BASE_PACKAGE = "com.pms.web";
    public static final String SWAGGER_API_PACKAGE = "com.pms.api";
    @Bean
    public Docket createRestWeb() {
        return new Docket(DocumentationType.SWAGGER_2)
                .groupName("WEB")
                .apiInfo(webInfo())
                .select()
                .apis(RequestHandlerSelectors.basePackage(SWAGGER_SCAN_BASE_PACKAGE))
                .paths(PathSelectors.any())
                .build();
//                .pathMapping("/api/admin"); // 在这里可以设置请求的统一前缀
    }
    private ApiInfo webInfo() {
        return new ApiInfoBuilder()
                .title("pms-sys WEB")
                .description("宏铭科技：http://www.schmkj.cn")
                .termsOfServiceUrl("http://www.schmkj.cn")
                .contact("宏铭科技")
                .version("2.0")
                .build();
    }


    @Bean
    public Docket createRestApi() {
        return new Docket(DocumentationType.SWAGGER_2)
                .groupName("API")
                .apiInfo(apiInfo())
                .select()
                .apis(RequestHandlerSelectors.basePackage(SWAGGER_API_PACKAGE))
                .paths(PathSelectors.any())
                .build();
    }
    private ApiInfo apiInfo() {
        return new ApiInfoBuilder()
                .title("pms-sys API")
                .description("宏铭科技：http://www.schmkj.cn")
                .termsOfServiceUrl("http://www.schmkj.cn")
                .contact("宏铭科技")
                .version("2.0")
                .build();
    }

}
