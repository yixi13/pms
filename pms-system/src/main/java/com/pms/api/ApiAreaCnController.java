package com.pms.api;

import com.pms.exception.R;
import com.pms.service.IAreaCnService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Administrator on 2017/8/30.
 */
@RestController
@RequestMapping("/api/sys/areaCn")
@Api(value="app区域接口",description = "app区域接口")
public class ApiAreaCnController {

    @Autowired
    private IAreaCnService areaCnService;

    @PostMapping("/getAreaCnMap")
    @ApiOperation("查询区域")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "provinceGb", value = "省级区域编号", required = false, dataType = "String", paramType = "form"),
            @ApiImplicitParam(name = "cityGb", value = "市级区域编号", required = false, dataType = "String", paramType = "form"),
      })
    public R getAreaCnMap(String provinceGb, String cityGb){
        Map<String,Object> paramMap = new HashMap<String,Object>();
        if(StringUtils.isNotBlank(provinceGb)){
            paramMap.put("provinceGb",provinceGb);
        }
        if(StringUtils.isNotBlank(cityGb)){
            paramMap.put("cityGb",cityGb);
        }
        List<Map<String,Object>> AreaCnMapList = areaCnService.getAreaCnMap(paramMap);
        if(null==AreaCnMapList){
            AreaCnMapList  = new ArrayList<>();
        }
        return R.ok().put("data",AreaCnMapList);
    }





}
