package com.pms.api;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.pms.controller.BaseController;
import com.pms.entity.BaseUser;
import com.pms.entity.SysNotice;
import com.pms.entity.SysNoticeUser;
import com.pms.exception.R;
import com.pms.exception.RRException;
import com.pms.rpc.UserInfoService;
import com.pms.service.IBaseUserService;
import com.pms.service.ISysNoticeService;
import com.pms.service.ISysNoticeUserService;
import com.pms.validator.Assert;
import com.xiaoleilu.hutool.util.StrUtil;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Date;
import java.util.List;
import java.util.Map;


/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author ljb
 * @since 2018-07-17
 */
@RestController
@RequestMapping("/apiSysNotice")
@Api(value = "api系统通知模块",description = "api系统通知模块")
public class ApiSysNoticeController extends BaseController {

    @Autowired
    private UserInfoService userInfoService;
    @Autowired
    private IBaseUserService iBaseUserService;

    @Autowired
    private ISysNoticeService iSysNoticeService;
    @Autowired
    private ISysNoticeUserService iSysNoticeUserService;

    @ApiOperation(value = "通知详情")
    @RequestMapping(value = "/{noticeId}",method = RequestMethod.GET)
    @ApiImplicitParams({
            @ApiImplicitParam(name="noticeId", value="公告id",required = true,dataType = "String",paramType="form")
    })
    public R detail(@PathVariable String noticeId){
        SysNotice notice = iSysNoticeService.selectById(noticeId);
        BaseUser user = iBaseUserService.selectById(notice.getCreatorId());
        return R.ok().put("notice",notice).put("userName",user.getUsername());
    }

    @ApiOperation(value = "获取公告list")
    @RequestMapping(value = "/list",method = RequestMethod.GET)
    @ApiImplicitParams({
            @ApiImplicitParam(name="current",value="当前页",required = true,dataType = "String",paramType="form"),
            @ApiImplicitParam(name="size",value="每页条数",required = true,dataType = "String",paramType="form"),
            @ApiImplicitParam(name="content",value="模糊查询字段",required = false,dataType = "String",paramType="form"),
            @ApiImplicitParam(name="source",value="所属系统",required = true,dataType = "Integer",paramType="form")
    })
    public R list(Integer current, Integer size, String content,
                  Integer source, Integer isEff,
                  @RequestHeader("X-Name") String userName,
                  @RequestHeader("X-Code") String companyCode){

        parameterIsNull(current,"current 不能为空");
        parameterIsNull(size,"size 不能为空");
        parameterIsNull(source,"source 不能为空");

        EntityWrapper<Map<String,Object>> ew = new EntityWrapper<Map<String,Object>>();
        if (StrUtil.isNotEmpty(content)){
            ew.like("sn.title", content);
        }
        ew.eq("sn.status", "1");
        ew.eq("sn.source", source);
        ew.eq("sn.company_code", companyCode);
        BaseUser user = iBaseUserService.getUserByUsername(userName,companyCode);
        if (user == null) {
            throw new RRException("用户不存在");
        }
        ew.eq("snu.userId", user.getId());
        if (isEff != null) {
            ew.and("startTime < NOW()");
        }
        ew.orderBy("snu.isRead desc");
        ew.orderBy("sn.createTime desc");

        Page<Map<String,Object>> page = new Page(current, size);
        page = iSysNoticeService.selectSysNoticeMapsPage(page,ew);
        return R.ok().put("page",page);

    }

    @ApiOperation(value = "修改为已读")
    @RequestMapping(value = "/updateToReade",method = RequestMethod.POST)
    @ApiImplicitParams({
            @ApiImplicitParam(name="noticeId", value="公告id",required = true,dataType = "String",paramType="form")
    })
    public R updateToReade(String noticeId,
                           @RequestHeader("X-Name") String userName,
                           @RequestHeader("X-Code") String companyCode){
        Assert.isBlank(noticeId,"noticeId 不能为空");
        EntityWrapper<SysNoticeUser> ew = new EntityWrapper<SysNoticeUser>();
        ew.in("noticeId", noticeId);
        BaseUser user = iBaseUserService.getUserByUsername(userName,companyCode);
        if (user == null) {
            throw new RRException("该用户不存在!");
        }
        ew.eq("userId", user.getId());

        List<SysNoticeUser> records = iSysNoticeUserService.selectList(ew);
        for (SysNoticeUser record:records){
            record.setIsRead(1);
        }
        iSysNoticeUserService.updateBatchById(records);
        return R.ok();
    }
}
