package com.pms.api;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.mapper.Wrapper;
import com.pms.controller.BaseController;
import com.pms.entity.SysVersion;
import com.pms.exception.R;
import com.pms.service.ISysVersionService;
import com.pms.validator.Assert;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by lk on 2018/8/23.
 */
@RestController
@RequestMapping("apiSysVersion")
@Api(value="App查询版本接口",description = "App查询版本接口")
public class ApiSysVersionController extends BaseController {
    @Autowired
    ISysVersionService sysVersionService;

    /**
     * 查询最新版本
     * @param type
     */
    @RequestMapping(value="/query")
    @ApiOperation(value="查询APP最新版本", notes="查询APP最新版本")
    @ApiImplicitParam(name="type",value="类型：1.网站 2.安卓 3.IOS",required = true,dataType = "Integer",paramType="form")
    public R query(Integer type){
        Assert.isNull(type,"类型不能为空");
        if(type!=2&&type!=3){
            return  R.error("类型值不对");
        }
        Wrapper<SysVersion> wrapper = new EntityWrapper<SysVersion>();
        wrapper.eq("type",type);
        wrapper.orderBy("id",false);
        SysVersion sv  = sysVersionService.selectOne(wrapper);
        return R.ok().put("data",sv);
    }

}
