package com.pms.api;

import com.pms.controller.BaseController;
import com.pms.entity.BaseUser;
import com.pms.exception.R;
import com.pms.service.IBaseElementService;
import com.pms.service.IBaseMenuService;
import com.pms.service.IBaseUserService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by hm on 2018/10/8.
 */
@RestController
@RequestMapping("api/menu")
@Api(value = "APP菜单权限",description = "APP菜单权限")
public class APiBaseMenuCotroller  extends BaseController {
    @Autowired
    IBaseElementService baseElementService;
    @Autowired
    IBaseMenuService baseMenuService;
    @Autowired
    IBaseUserService baseUserService;
    /**
     * 查询API查询是否有权限
     * @param userId
     * @param menuType
     * @param title
     * @return
     */
    @GetMapping("/getApiPermission")
    @ApiOperation(value = "查询API查询是否有权限", notes = "查询API查询是否有权限")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "userId", value = "用户ID", paramType = "form", dataType = "Long"),
            @ApiImplicitParam(name = "menuType", value = "菜单类型（1管网2二汞3平台）", paramType = "form", dataType = "Integer"),
            @ApiImplicitParam(name = "title", value = "菜单名称", paramType = "form", dataType = "String"),
            @ApiImplicitParam(name = "permissionsType", value = "查询类型（1.查询菜单2.查询权限）", paramType = "form", dataType = "Integer"),
            @ApiImplicitParam(name = "method", value = "请求类型（POST）", paramType = "form", dataType = "String"),
    })
    public R getApiPermission(Long userId, Integer menuType, String title, Integer permissionsType, String method) {
        parameterIsNull(userId,"用户ID不存在");
        parameterIsNull(menuType,"菜单类型不能为空");
        parameterIsNull(title,"菜单名称不能为空");
        parameterIsNull(permissionsType,"查询类型不能为空");
        parameterIsNull(method,"请求类型不能为空");
        Map<String, Object> menus =new HashMap<>();
        if (permissionsType ==1){//查询菜单
            menus = baseMenuService.getApiPermissionMenu(userId, menuType, title);
            if (null==menus){
                return R.error(400,"您没有进入该页面的权限");
            }
        }else if (permissionsType ==2){//查询权限
            BaseUser baseUser = baseUserService.selectById(userId);
            if (null!=baseUser){
                menus = baseElementService.getApiPermissionElement(userId,baseUser.getGroupType(),baseUser.getRoleType(),method,title,menuType);
                if (null==menus){
                    return R.error(400,"没有相应页面编辑的入口或禁止进行编辑");
                }
            }
        }
        return R.ok().put("data",menus);
    }
}
