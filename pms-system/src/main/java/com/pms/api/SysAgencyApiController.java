package com.pms.api;

import com.pms.controller.BaseController;
import io.swagger.annotations.Api;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("sysAgency")
@Api(value="机构查询相关接口",description = "机构查询相关接口")
public class SysAgencyApiController extends BaseController {
    Logger logger = LoggerFactory.getLogger(getClass());


}