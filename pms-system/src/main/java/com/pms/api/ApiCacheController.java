package com.pms.api;

import com.pms.cache.api.CacheAPI;
import com.pms.controller.BaseController;
import com.pms.exception.R;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/cache")
@Api(value="app清除后台缓存接口",description = "app清除后台缓存接口")
public class ApiCacheController extends BaseController {
    private Logger logger = LoggerFactory.getLogger(getClass());
    @Autowired
    private CacheAPI cacheAPI;
    @Value("${spring.redis.sysName}")
    private String cacheSysName;
    @PostMapping("/clearCache")
    @ApiOperation(value = "清除后台缓存",notes = "清楚缓存")
    @ApiImplicitParams({
            @ApiImplicitParam(name="cacheKey",value="缓存键值，默认清除所有",required = false,dataType = "String",paramType="form")
    })
    public R clearCache(String cacheKey){
        if(StringUtils.isBlank(cacheKey)){
            cacheAPI.removeByPre(cacheSysName);
        }else{
            if (!cacheKey.startsWith(cacheSysName)) {
                cacheKey = cacheSysName+ ":" + cacheKey+"*";
            }
            cacheAPI.removeByPre(cacheKey);
        }
        return R.ok();
    }
}