package com.pms.web;

import com.pms.entity.BaseResourceAuthority;
import com.pms.service.IBaseResourceAuthorityService;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import com.pms.controller.BaseController;
/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author zyl
 * @since 2017-07-06
 */
@Controller
@RequestMapping("resourceAuthority")
public class BaseResourceAuthorityController extends BaseController {


}
