package com.pms.web;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.pms.entity.BaseGroupType;
import com.pms.entity.vo.AuthorityMenuTree;
import com.pms.result.ObjectRestResponse;
import com.pms.result.TableResultResponse;
import com.pms.service.IBaseGroupTypeService;
import io.swagger.annotations.Api;
import org.apache.commons.lang.StringUtils;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import com.pms.controller.BaseController;
import java.util.Date;
import java.util.List;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author zyl
 * @since 2017-07-06
 */
@Controller
@RequestMapping("groupType")
@Api(value = "角色类型",description = "角色类型")
public class BaseGroupTypeController extends BaseController {
    org.slf4j.Logger logger = LoggerFactory.getLogger(getClass());
    @Autowired
    IBaseGroupTypeService baseGroupTypeService;

    @RequestMapping(value = "/page")
    @ResponseBody
    public TableResultResponse<BaseGroupType> page(int limit, int page, String name){
        logger.debug("----------->  " +limit);
        logger.debug("----------->  " +page);
        Page<BaseGroupType> pages = new Page<BaseGroupType>(page,limit);
        EntityWrapper entityWrapper = new EntityWrapper();
        if(StringUtils.isNotBlank(name)){
            entityWrapper.like("title",name);
        }
        pages =baseGroupTypeService.selectPage(pages,entityWrapper);

        return new TableResultResponse<BaseGroupType>(pages.getTotal(),pages.getRecords());
    }


    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    @ResponseBody
    public ObjectRestResponse<List<AuthorityMenuTree>> getMenuAuthority(@PathVariable int id){
        return new ObjectRestResponse().data(baseGroupTypeService.selectById(id));
    }

    @RequestMapping(value = "/all")
    @ResponseBody
    public List<BaseGroupType> list(){
        return baseGroupTypeService.selectList(new EntityWrapper<BaseGroupType>());
    }

    /**
     * 新增
     * @param entity
     * @return
     */
    @RequestMapping(value = "",method = RequestMethod.POST)
    @ResponseBody
    public ObjectRestResponse<BaseGroupType> add(@RequestBody BaseGroupType entity){
        entity.setCrtTime(new Date());
        baseGroupTypeService.insert(entity);
        return new ObjectRestResponse<BaseGroupType>().rel(true);
    }
    /**
     * 更改
     * @param entity
     * @return
     */
    @RequestMapping(value = "/{id}",method = RequestMethod.PUT)
    @ResponseBody
    public ObjectRestResponse<BaseGroupType> update(@RequestBody BaseGroupType entity){
        baseGroupTypeService.updateById(entity);
        return new ObjectRestResponse<BaseGroupType>().rel(true);
    }
    /**
     * 删除
     * @param id
     * @return
     */
    @RequestMapping(value = "/{id}",method = RequestMethod.DELETE)
    @ResponseBody
    public ObjectRestResponse<BaseGroupType> update(@PathVariable int id){
        baseGroupTypeService.deleteById (id);
        return new ObjectRestResponse<BaseGroupType>().rel(true);
    }
}
