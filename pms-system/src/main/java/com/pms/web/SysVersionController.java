package com.pms.web;

import com.pms.exception.R;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.plugins.Page;
import org.springframework.web.bind.annotation.*;
import com.pms.controller.BaseController;
import com.pms.service.ISysVersionService;
import com.pms.entity.SysVersion;
import com.pms.result.JsonResult;
import com.pms.result.ObjectRestResponse;
import com.pms.result.TableResultResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;

@RestController
@RequestMapping("sysVersion")
public class SysVersionController extends BaseController {
    Logger logger = LoggerFactory.getLogger(getClass());
    @Autowired
    ISysVersionService sysVersionService;

    /**
         * 新增
         * @param entity
         * @return
         */
    @RequestMapping(value = "",method = RequestMethod.POST)
    @ResponseBody
    public ObjectRestResponse<SysVersion> add(@RequestBody SysVersion entity){
            sysVersionService.insert(entity);
        return new ObjectRestResponse<SysVersion>().rel(true);
    }
    /**
     * 修改
     * @param entity
     * @return
     */
    @RequestMapping(value = "/{id}",method = RequestMethod.POST)
    @ResponseBody
    public ObjectRestResponse<SysVersion> update(@RequestBody SysVersion entity){
            sysVersionService.updateById(entity);
        return new ObjectRestResponse<SysVersion>().rel(true);
    }

    /**
 * 删除
 * @param id
 * @return
 */
    @RequestMapping(value = "/{id}",method = RequestMethod.DELETE)
    @ResponseBody
    public ObjectRestResponse<SysVersion> deleteById(@PathVariable int id){
            sysVersionService.deleteById (id);
        return new ObjectRestResponse<SysVersion>().rel(true);
    }


    /**
     * findByID
     * @param id
     * @return
     */
    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    @ResponseBody
    public ObjectRestResponse<SysVersion> findById(@PathVariable int id){
        return new ObjectRestResponse<SysVersion>().rel(true).data( sysVersionService.selectById(id));
    }


    @RequestMapping(value = "/page",method = RequestMethod.GET)
    @ResponseBody
    public TableResultResponse<SysVersion> page(@RequestParam(defaultValue = "10") int limit, @RequestParam(defaultValue = "1")int offset){
        Page<SysVersion> page = new Page<SysVersion>(offset,limit);
        page = sysVersionService.selectPage(page,new EntityWrapper<SysVersion>());
        return new TableResultResponse<SysVersion>(page.getTotal(),page.getRecords());
    }

    @RequestMapping(value = "/all",method = RequestMethod.GET)
    @ResponseBody
    public List<SysVersion> all(){
        return sysVersionService.selectList(null);
    }

    /**
     * 获取网站最新版本
     * @param
     * @return
     */
    @RequestMapping(value = "/getVersion", method = RequestMethod.GET)
    @ResponseBody
    @ApiOperation(value = "获取网站最新版本")
    public R getVersion(){
        SysVersion sv = sysVersionService.selectOne(new EntityWrapper<SysVersion>().eq("type",1));
        return R.ok().put("data",sv);
    }


}