package com.pms.web;
import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.mapper.Wrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.pms.entity.SysMember;
import com.pms.entity.UserInfo;
import com.pms.exception.R;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import com.pms.controller.BaseController;
import com.pms.service.ISysMemberService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


@RestController
@RequestMapping("sysMember")
@Api(value = "后台会员管理接口",description = "后台会员管理接口")
public class SysMemberController extends BaseController{
//    Logger logger = LoggerFactory.getLogger(getClass());
//    @Autowired
//    ISysMemberService sysMemberService;
//
//    @Autowired
//    public UserInfoService userInfoService;
//
//    /**
//     * 查询我的会员信息
//     * @param memberId  会员ID
//     * @return
//     */
//
//    @RequestMapping(value = "/queryBymember/{memberId}", method = RequestMethod.POST)
//    @ApiImplicitParams({
//            @ApiImplicitParam(name = "memberId", value = "会员ID", required = true, dataType = "Long",paramType = "form")
//    })
//    public R queryBymember(Long  memberId){
//         parameterIsNull(memberId,"会员memberId不能为空");
//         SysMember  sysMember=sysMemberService.selectById(memberId);
//         parameterIsNull(sysMember,"该会员信息不存在");
//        return R.ok().put("data",sysMember).putDescription(SysMember.class);
//    }
//
//    /**
//     * 分页查询会员
//     * @param size
//     * @param page
//     * @param phone
//     * @return
//     */
//    @ApiOperation("分页查询会员")
//    @RequestMapping(value = "/page",method = RequestMethod.POST)
//    @ApiImplicitParams({
//            @ApiImplicitParam(name = "size", value = "条数（默认为10）", required = false, dataType = "Integer",paramType = "form"),
//            @ApiImplicitParam(name = "page", value = "页数（默认为1）", required = false, dataType = "Integer",paramType = "form"),
//            @ApiImplicitParam(name = "phone", value = "会员手机号", required = false, dataType = "String",paramType = "form")
//    })
//    public R page(Integer size,Integer page, String phone){
//        UserInfo userInfo = userInfoService.getCurrentUser();
//        if (null == size || size < 1){ size=10;}
//        if (null== page ||  page<1 ){page=1;}
//        Page<SysMember> pages = new Page<SysMember>(page,size);
//        Wrapper<SysMember> ew= new EntityWrapper<SysMember>().eq("phone_",phone);
//        if (phone==null){pages = sysMemberService.selectPage(pages,null);
//        }else { pages = sysMemberService.selectPage(pages,ew);}
//        return R.ok().put("data",pages).putDescription(SysMember.class);
//    }

}