package com.pms.web;

import com.pms.exception.R;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
/**
 * @author zyl
 * @create 2017-07-17 14:29
 **/
@RestController
public class HomeController {
    Logger logger = LoggerFactory.getLogger(getClass());
    @RequestMapping(value = "/index",method = RequestMethod.GET)
    public R index() {

        logger.info("-------------index ---------");
        return R.ok();
    }
}
