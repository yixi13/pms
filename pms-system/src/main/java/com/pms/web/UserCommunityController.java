package com.pms.web;

import org.springframework.beans.factory.annotation.Autowired;
import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.plugins.Page;
import org.springframework.web.bind.annotation.*;
import com.pms.controller.BaseController;
import com.pms.service.IUserCommunityService;
import com.pms.entity.UserCommunity;
import com.pms.result.JsonResult;
import com.pms.result.ObjectRestResponse;
import com.pms.result.TableResultResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;

@RestController
@RequestMapping("userCommunity")
public class UserCommunityController extends BaseController {
    Logger logger = LoggerFactory.getLogger(getClass());
    @Autowired
    IUserCommunityService userCommunityService;

    /**
         * 新增
         * @param entity
         * @return
         */
    @RequestMapping(value = "",method = RequestMethod.POST)
    @ResponseBody
    public ObjectRestResponse<UserCommunity> add(@RequestBody UserCommunity entity){
            userCommunityService.insert(entity);
        return new ObjectRestResponse<UserCommunity>().rel(true);
    }
    /**
     * 修改
     * @param entity
     * @return
     */
    @RequestMapping(value = "/{id}",method = RequestMethod.POST)
    @ResponseBody
    public ObjectRestResponse<UserCommunity> update(@RequestBody UserCommunity entity){
            userCommunityService.updateById(entity);
        return new ObjectRestResponse<UserCommunity>().rel(true);
    }

    /**
 * 删除
 * @param id
 * @return
 */
    @RequestMapping(value = "/{id}",method = RequestMethod.DELETE)
    @ResponseBody
    public ObjectRestResponse<UserCommunity> deleteById(@PathVariable int id){
            userCommunityService.deleteById (id);
        return new ObjectRestResponse<UserCommunity>().rel(true);
    }


    /**
     * findByID
     * @param id
     * @return
     */
    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    @ResponseBody
    public ObjectRestResponse<UserCommunity> findById(@PathVariable int id){
        return new ObjectRestResponse<UserCommunity>().rel(true).data( userCommunityService.selectById(id));
    }


    @RequestMapping(value = "/page",method = RequestMethod.GET)
    @ResponseBody
    public TableResultResponse<UserCommunity> page(@RequestParam(defaultValue = "10") int limit, @RequestParam(defaultValue = "1")int offset){
        Page<UserCommunity> page = new Page<UserCommunity>(offset,limit);
        page = userCommunityService.selectPage(page,new EntityWrapper<UserCommunity>());
        return new TableResultResponse<UserCommunity>(page.getTotal(),page.getRecords());
    }

    @RequestMapping(value = "/all",method = RequestMethod.GET)
    @ResponseBody
    public List<UserCommunity> all(){
        return userCommunityService.selectList(null);
    }


}