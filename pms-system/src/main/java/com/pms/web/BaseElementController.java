package com.pms.web;
import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.pms.controller.BaseController;
import com.pms.entity.BaseElement;
import com.pms.entity.BaseUser;
import com.pms.entity.vo.AuthorityMenuTree;
import com.pms.exception.R;
import com.pms.result.ObjectRestResponse;
import com.pms.result.TableResultResponse;
import com.pms.service.IBaseElementService;
import com.pms.service.IBaseUserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.Date;
import java.util.List;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author zyl
 * @since 2017-07-06
 */
@Controller
@RequestMapping("/element")
public class BaseElementController extends BaseController {
    Logger logger = LoggerFactory.getLogger(getClass());
	@Autowired
    IBaseUserService baseUserService;
    @Autowired
    IBaseElementService baseElementService;

    @RequestMapping(value = "/page", method = RequestMethod.GET)
    @ResponseBody
    public TableResultResponse<BaseElement> page(@RequestParam  int limit,
                                                 @RequestParam int page,
                                                 @RequestParam  int menuId) {
        if (limit<1){limit=10;}
        if (page<1){page=1;}
        BaseElement element = new BaseElement();
        element.setMenuId(menuId + "");
        Page<BaseElement> pages= new Page<BaseElement>(page,limit);
        logger.info("---- -->>"+menuId);
        pages = baseElementService.selectPage(pages,new EntityWrapper<BaseElement>().eq("menu_id",menuId));
        return new TableResultResponse<BaseElement>(pages.getTotal(), pages.getRecords());
    }

    @RequestMapping(value = "/user", method = RequestMethod.GET)
    @ResponseBody
    public R getAuthorityElement(String menuId) {
        String companyCode = request.getHeader("X-Code");
        String username = request.getHeader("X-Name");;
        Long userId = baseUserService.getUserByUsername(username,companyCode).getId();
        List<BaseElement> elements = baseElementService.getAuthorityElementByUserId(userId + "",menuId);
        return R.ok().put("date",elements);
    }

    @RequestMapping(value = "/user/menu", method = RequestMethod.GET)
    @ResponseBody
    public R getAuthorityElement() {
        String companyCode = request.getHeader("X-Code");
        String username = request.getHeader("X-Name");;
        BaseUser user = baseUserService.getUserByUsername(username,companyCode);
        if (user==null){return  R.error(400,"用户信息不存在");}
        List<BaseElement> elements = baseElementService.getAuthorityElement(user.getGroupType(),user.getRoleType(),user.getId().toString());
        return R.ok().put("date",elements);
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    @ResponseBody
    public ObjectRestResponse<List<AuthorityMenuTree>> getMenuAuthority(@PathVariable int id){
        return new ObjectRestResponse().data(baseElementService.selectById(id));
    }


    /**
     * 新增
     * @param entity
     * @return
     */
    @RequestMapping(value = "",method = RequestMethod.POST)
    @ResponseBody
    public ObjectRestResponse<BaseElement> add(@RequestBody BaseElement entity){
        baseElementService.insert(entity);
        return new ObjectRestResponse<BaseElement>().rel(true);
    }
    /**
     * 更改
     * @param entity
     * @return
     */
    @RequestMapping(value = "/{id}",method = RequestMethod.PUT)
    @ResponseBody
    public ObjectRestResponse<BaseElement> update(@RequestBody BaseElement entity){
        baseElementService.updateById(entity);
        return new ObjectRestResponse<BaseElement>().rel(true);
    }
    /**
     * 删除
     * @param id
     * @return
     */
    @RequestMapping(value = "/{id}",method = RequestMethod.DELETE)
    @ResponseBody
    public ObjectRestResponse<BaseElement> update(@PathVariable int id){
        baseElementService.deleteById (id);
        return new ObjectRestResponse<BaseElement>().rel(true);
    }

    /**
     * 查询所有
     * @return
     */
    @RequestMapping(value = "/all",method = RequestMethod.GET)
    @ResponseBody
    public List<BaseElement> all(){
        return baseElementService.selectList(null);
    }


    /**
     * 根据菜单ID查询当前菜单权限
     * @param menuId
     * @return
     */
    @RequestMapping(value = "/selectByMenu",method = RequestMethod.GET)
    @ResponseBody
    public List<BaseElement> selectByMenu(String menuId){
        parameterIsBlank(menuId,"菜单ID不能为空");
        return baseElementService.selectList(new EntityWrapper<BaseElement>().eq("menu_id",menuId));
    }


}
