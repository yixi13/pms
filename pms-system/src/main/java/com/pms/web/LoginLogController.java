package com.pms.web;

import com.baomidou.mybatisplus.mapper.Wrapper;
import com.pms.entity.BaseGroup;
import com.pms.entity.BaseUser;
import com.pms.exception.R;
import com.pms.service.IBaseUserService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.plugins.Page;
import org.springframework.web.bind.annotation.*;
import com.pms.controller.BaseController;
import com.pms.service.ILoginLogService;
import com.pms.entity.LoginLog;
import com.pms.result.JsonResult;
import com.pms.result.ObjectRestResponse;
import com.pms.result.TableResultResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;

@RestController
@RequestMapping("loginLog")
@Api(value = "后台操作日志管理",description = "后台操作日志管理")
public class LoginLogController extends BaseController {
    Logger logger = LoggerFactory.getLogger(getClass());
    @Autowired
    ILoginLogService loginLogService;
    @Autowired
    IBaseUserService baseUserService;
    /**
     * 查询该用户的系统日志
     * @return
     */
    @GetMapping(value = "/page")
    @ApiOperation("查询该用户的系统日志")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "limit", value = "条数", required = true, dataType = "int", paramType = "form"),
            @ApiImplicitParam(name = "page", value = "页数", required = true, dataType = "int", paramType = "form"),
            @ApiImplicitParam(name = "name", value = "", required = false, dataType = "String", paramType = "form"),
            @ApiImplicitParam(name = "userId", value = "用户ID", required = true, dataType = "userId", paramType = "form"),
    })
    public R page(int limit,int page, String name,Long userId){
        parameterIsNull(userId,"用户ID不能为空");
        if (limit<1){limit=10;}
        if (page<1){page=1;}
        Page<LoginLog> pages = new Page<>(page,limit);
        BaseUser user =baseUserService.selectOne(new EntityWrapper<BaseUser>().eq("id",userId));
        if (user==null){ return  R.error(400,"用户信息不存在");}
        Wrapper<LoginLog> ew=new EntityWrapper();
        ew.eq("user_id",user.getId());
        if (StringUtils.isNotBlank(name)){
            ew.and().like("nick_name",name);
        }
        Page<LoginLog> pageList = loginLogService.selectPage(pages,ew);
        return  R.ok().put("data",pageList);
    }


}