package com.pms.web;

import com.pms.controller.BaseController;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;


/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author ljb
 * @since 2018-07-17
 */
@Controller
@RequestMapping("/sysNoticeUser")
public class SysNoticeUserController extends BaseController {
	
}
