package com.pms.web;

import com.pms.exception.R;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.plugins.Page;
import org.springframework.web.bind.annotation.*;
import com.pms.controller.BaseController;
import com.pms.service.IAreaCnService;
import com.pms.entity.AreaCn;
import com.pms.result.ObjectRestResponse;
import com.pms.result.TableResultResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("areaCn")
public class AreaCnController extends BaseController {
    Logger logger = LoggerFactory.getLogger(getClass());
    @Autowired
    IAreaCnService areaCnService;

//    /**
//         * 新增
//         * @param entity
//         * @return
//         */
//    @RequestMapping(value = "",method = RequestMethod.POST)
//    public ObjectRestResponse<AreaCn> add(@RequestBody AreaCn entity){
//            areaCnService.insert(entity);
//        return new ObjectRestResponse<AreaCn>().rel(true);
//    }
//    /**
//     * 修改
//     * @param entity
//     * @return
//     */
//    @RequestMapping(value = "/{id}",method = RequestMethod.POST)
//    public ObjectRestResponse<AreaCn> update(@RequestBody AreaCn entity){
//            areaCnService.updateById(entity);
//        return new ObjectRestResponse<AreaCn>().rel(true);
//    }
//
//    /**
// * 删除
// * @param id
// * @return
// */
//    @RequestMapping(value = "/{id}",method = RequestMethod.DELETE)
//    public ObjectRestResponse<AreaCn> deleteById(@PathVariable int id){
//            areaCnService.deleteById (id);
//        return new ObjectRestResponse<AreaCn>().rel(true);
//    }
//
//
//    /**
//     * findByID
//     * @param id
//     * @return
//     */
//    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
//    public ObjectRestResponse<AreaCn> findById(@PathVariable int id){
//        return new ObjectRestResponse<AreaCn>().rel(true).data( areaCnService.selectById(id));
//    }


    @RequestMapping(value = "/page",method = RequestMethod.GET)
    public TableResultResponse<AreaCn> page(@RequestParam(defaultValue = "10") int limit, @RequestParam(defaultValue = "1")int offset){
        Page<AreaCn> page = new Page<AreaCn>(offset,limit);
        page = areaCnService.selectPage(page,new EntityWrapper<AreaCn>());
        if (page==null){
            return new TableResultResponse<AreaCn>();
        }else{
            return new TableResultResponse<AreaCn>(page.getTotal(),page.getRecords());
        }
    }

    
    @RequestMapping(value = "/all",method = RequestMethod.GET)
    public List<AreaCn> all(){
        return areaCnService.selectList(null);
    }




    /**
     * 分页查询地址
     * @param size
     * @param page
     * @param
     * @return
     */
    @ApiOperation("分页查询地址")
    @RequestMapping(value = "/page",method = RequestMethod.POST)
    @ApiImplicitParams({
            @ApiImplicitParam(name = "size", value = "条数（默认为10）", required = false, dataType = "Integer",paramType = "form"),
            @ApiImplicitParam(name = "page", value = "页数（默认为1）", required = false, dataType = "Integer",paramType = "form"),
     })
    public R page(Integer size, Integer page){
        if (null == size || size < 1){ size=10;}
        if (null== page ||  page<1 ){page=1;}
        Page<AreaCn> pages = new Page<AreaCn>(page,size);
        pages = areaCnService.selectPage(pages,new EntityWrapper<AreaCn>());
        return R.ok().put("data",pages).putDescription(AreaCn.class);
}



    @ApiOperation(value = "获取所有的省")
    @GetMapping("/getProvince")
    public R getProvince(){
        List<AreaCn> list = areaCnService.getProvince();
        return R.ok().put("provinces",list);
    }

    /**
     * 获取省下面的市区
     * @param gbCode 选择的省的 gbCode
     * @return
     */
    @GetMapping("/getCity")
    @ApiOperation(value = "更具省编码查询市")
    @ApiImplicitParam(name = "gbCode", value = "省GB编码", required = true, dataType = "Integer", paramType = "query")
    public R getCity(Integer gbCode){
        List<AreaCn> list = areaCnService.getCitys(gbCode);
        return R.ok().put("citys",list);
    }

    /**
     *
     * @param gbCode
     * @return
     */
    @GetMapping("/getCounty")
    @ApiOperation(value = "更具市编码查询所有区县")
    @ApiImplicitParam(name = "gbCode", value = "所属市GB编码", required = true, dataType = "Integer", paramType = "query")
    public R getCounty(Integer gbCode){
        List<AreaCn> list = areaCnService.getCountys(gbCode);
        return R.ok().put("countys",list);
    }

    @GetMapping("/getAreaCnMap")
    @ApiOperation("查询区域")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "provinceGb", value = "省级区域编号", required = false, dataType = "String", paramType = "form"),
            @ApiImplicitParam(name = "cityGb", value = "市级区域编号", required = false, dataType = "String", paramType = "form"),
    })
    public R getAreaCnMap(String provinceGb,String cityGb){
        Map<String,Object> paramMap = new HashMap<String,Object>();
        if(StringUtils.isNotBlank(provinceGb)){
            paramMap.put("provinceGb",provinceGb);
        }
        if(StringUtils.isNotBlank(cityGb)){
            paramMap.put("cityGb",cityGb);
        }
        List<Map<String,Object>> AreaCnMapList = areaCnService.getAreaCnMap(paramMap);
        if(null==AreaCnMapList){
            AreaCnMapList  = new ArrayList<Map<String,Object>>();
        }
        return R.ok().put("data",AreaCnMapList);
    }

}