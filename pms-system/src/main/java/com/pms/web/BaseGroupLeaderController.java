package com.pms.web;

import com.pms.entity.BaseGroupLeader;
import com.pms.service.IBaseGroupLeaderService;
import io.swagger.annotations.Api;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import com.pms.controller.BaseController;
/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author zyl
 * @since 2017-07-06
 */
@Controller
@RequestMapping("/system/baseGroupLeader")
public class BaseGroupLeaderController extends BaseController{
	
}
