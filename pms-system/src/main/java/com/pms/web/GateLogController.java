package com.pms.web;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.pms.controller.BaseController;
import com.pms.entity.GateLog;
import com.pms.result.TableResultResponse;
import com.pms.service.IGateLogService;
import io.swagger.annotations.Api;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author zyl
 * @since 2017-07-06
 */
@RestController
@RequestMapping("gateLog")
public class GateLogController extends BaseController{
    Logger logger = LoggerFactory.getLogger(getClass());
    @Autowired
    IGateLogService gateLogService;
    @RequestMapping(value = "/page",method = RequestMethod.GET)
    @ResponseBody
    public TableResultResponse<GateLog> page(@RequestParam(defaultValue = "10") int limit, @RequestParam(defaultValue = "1")int offset, String name){

        Page<GateLog> page = new Page<GateLog>(offset,limit);
        page = gateLogService.selectPage(page,new EntityWrapper<GateLog>().like("crt_name",name).orderBy("crt_time",false));
        return new TableResultResponse<GateLog>(page.getTotal(),page.getRecords());
    }
}
