package com.pms.web;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.mapper.Wrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.pms.entity.SysMember;
import com.pms.exception.R;
import com.pms.service.ISysMemberAddressService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import com.pms.controller.BaseController;
import com.pms.entity.SysMemberAddress;
import org.springframework.web.bind.annotation.RequestMapping;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
@RestController
@RequestMapping("sysMemberAddress")
@Api(value = "后台收货地址管理接口",description = "后台收货地址管理接口")
public class SysMemberAddressController extends BaseController {
    Logger logger = LoggerFactory.getLogger(getClass());
    @Autowired
    ISysMemberAddressService sysMemberAddressService;


    /**
     * 分页查询会员收货地址
     * @param size
     * @param page
     * @param phone
     * @return
     */
    @ApiOperation("后台收货地址管理接口")
    @RequestMapping(value = "/page",method = RequestMethod.POST)
    @ApiImplicitParams({
            @ApiImplicitParam(name = "size", value = "条数（默认为10）", required = false, dataType = "Integer",paramType = "form"),
            @ApiImplicitParam(name = "page", value = "页数（默认为1）", required = false, dataType = "Integer",paramType = "form"),
            @ApiImplicitParam(name = "phone", value = "会员手机号", required = false, dataType = "String",paramType = "form")
    })
    public R page(Integer size,Integer page, String phone){
        if (null == size || size < 1){ size=10;}
        if (null== page ||  page<1 ){page=1;}
        Page<SysMemberAddress> pages = new Page<SysMemberAddress>(page,size);
        Wrapper<SysMemberAddress> ew= new EntityWrapper<SysMemberAddress>().eq("phone_",phone);
        if (phone==null){pages = sysMemberAddressService.selectPage(pages,null);
        }else { pages = sysMemberAddressService.selectPage(pages,ew);}
        return R.ok().put("data",pages).putDescription(SysMemberAddress.class);
      }

}