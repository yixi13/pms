package com.pms.entity;
import java.io.Serializable;

import com.pms.util.Description;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;


/**
 * 
 * 
 * @author zyl
 * @email 517770986@qq.com
 * @date 2017-11-20 14:28:50
 */
@TableName( "adver_place")
public class AdverPlace extends BaseModel<AdverPlace> {
private static final long serialVersionUID = 1L;


	/**
	 *广告位置表ID
	 */
    @TableId("place_id")
    private Long placeId;
	
	    /**
     *广告位置名称
     */
    @TableField("name_")
    @Description("广告位置名称")
    private String name;
	
	    /**
     *广告参数值
     */
    @TableField("key_word")
    @Description("广告参数值")
    private String keyWord;
	
	    /**
     *广告类型值1代表APP名称 2广告类型参数
     */
    @TableField("depths_")
    @Description("广告类型值1代表APP名称 2广告类型参数")
    private Integer depths;
	
	    /**
     *父级参数ID
     */
    @TableField("parent_Id")
    @Description("父级参数ID")
    private Long parentId;
	
// ID赋值
public AdverPlace(){
        this.placeId= returnIdLong();
        }
@Override
protected Serializable pkVal() {
        return this.placeId;
}
	/**
	 * 设置：广告位置表ID
	 */
	public AdverPlace setPlaceId(Long placeId) {
		this.placeId = placeId;
		return this;
	}
	/**
	 * 获取：广告位置表ID
	 */
	public Long getPlaceId() {
		return placeId;
	}
	/**
	 * 设置：广告位置名称
	 */
	public AdverPlace setName(String name) {
		this.name = name;
		return this;
	}
	/**
	 * 获取：广告位置名称
	 */
	public String getName() {
		return name;
	}
	/**
	 * 设置：广告参数值
	 */
	public AdverPlace setKeyWord(String keyWord) {
		this.keyWord = keyWord;
		return this;
	}
	/**
	 * 获取：广告参数值
	 */
	public String getKeyWord() {
		return keyWord;
	}
	/**
	 * 设置：广告类型值1代表APP名称 2广告类型参数
	 */
	public AdverPlace setDepths(Integer depths) {
		this.depths = depths;
		return this;
	}
	/**
	 * 获取：广告类型值1代表APP名称 2广告类型参数
	 */
	public Integer getDepths() {
		return depths;
	}
	/**
	 * 设置：父级参数ID
	 */
	public AdverPlace setParentId(Long parentId) {
		this.parentId = parentId;
		return this;
	}
	/**
	 * 获取：父级参数ID
	 */
	public Long getParentId() {
		return parentId;
	}


}
