package com.pms.entity;
import java.io.Serializable;
import java.util.Date;

import com.pms.util.Description;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;


/**
 * 
 * 
 * @author lk
 * @email
 * @date 2018-04-18 10:47:17
 */
@TableName( "login_log")
public class LoginLog extends BaseModel<LoginLog> {
private static final long serialVersionUID = 1L;


	    /**
	 *
	 */
    @TableId("id")
    private Long id;
	
	    /**
     *
     */
    @TableField("user_id")
    @Description("")
    private Long userId;
	
	    /**
     *用户昵称
     */
    @TableField("nick_name")
    @Description("用户昵称")
    private String nickName;
	
	    /**
     *登录时间
     */
    @TableField("login_in_time")
    @Description("登录时间")
    private Date loginInTime;
	
	    /**
     *退出时间
     */
    @TableField("login_out_time")
    @Description("退出时间")
    private Date loginOutTime;

	@TableField("explain")
	@Description("说明")
	private String explain;

// ID赋值
public LoginLog(){
        this.id= returnStaticIdLong();
        }
@Override
protected Serializable pkVal() {
        return this.id;
}
	/**
	 * 设置：
	 */
	public LoginLog setId(Long id) {
		this.id = id;
		return this;
	}
	/**
	 * 获取：
	 */
	public Long getId() {
		return id;
	}
	/**
	 * 设置：
	 */
	public LoginLog setUserId(Long userId) {
		this.userId = userId;
		return this;
	}
	/**
	 * 获取：
	 */
	public Long getUserId() {
		return userId;
	}
	/**
	 * 设置：用户昵称
	 */
	public LoginLog setNickName(String nickName) {
		this.nickName = nickName;
		return this;
	}
	/**
	 * 获取：用户昵称
	 */
	public String getNickName() {
		return nickName;
	}
	/**
	 * 设置：登录时间
	 */
	public LoginLog setLoginInTime(Date loginInTime) {
		this.loginInTime = loginInTime;
		return this;
	}
	/**
	 * 获取：登录时间
	 */
	public Date getLoginInTime() {
		return loginInTime;
	}
	/**
	 * 设置：退出时间
	 */
	public LoginLog setLoginOutTime(Date loginOutTime) {
		this.loginOutTime = loginOutTime;
		return this;
	}
	/**
	 * 获取：退出时间
	 */
	public Date getLoginOutTime() {
		return loginOutTime;
	}

	public String getExplain() {
		return explain;
	}

	public void setExplain(String explain) {
		this.explain = explain;
	}
}
