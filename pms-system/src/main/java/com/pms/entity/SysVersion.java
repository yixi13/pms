package com.pms.entity;
import com.baomidou.mybatisplus.activerecord.Model;
import java.io.Serializable;
import java.util.Date;
import javax.persistence.*;
import com.pms.util.Description;
import com.pms.entity.BaseModel;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;


/**
 * 
 * 
 * @author lk
 * @date 2018-07-18 13:44:01
 */
@TableName( "sys_version")
public class SysVersion extends BaseModel<SysVersion> {
private static final long serialVersionUID = 1L;


	    /**
	 *
	 */
    @TableId("id")
    private Long id;
	
	    /**
     *版本号
     */
    @TableField("version_no")
    @Description("版本号")
    private String versionNo;
	
	    /**
     *版本对应的数字
     */
    @TableField("version_num")
    @Description("版本对应的数字")
    private Integer versionNum;
	
	    /**
     *上传或更新时间
     */
    @TableField("up_time")
    @Description("上传或更新时间")
    private Date upTime;
	
	    /**
     *版本说明
     */
    @TableField("descr")
    @Description("版本说明")
    private String descr;
	
	    /**
     *下载地址
     */
    @TableField("download_url")
    @Description("下载地址")
    private String downloadUrl;
	
	    /**
     *状态：1.正常更新 2.强制更新
     */
    @TableField("status")
    @Description("状态：1.正常更新 2.强制更新")
    private Integer status;
	
	    /**
     *1.网站 2.app
     */
    @TableField("type")
    @Description("1.网站 2.app")
    private Integer type;
	
// ID赋值
public SysVersion(){
        this.id= returnIdLong();
        }
@Override
protected Serializable pkVal() {
        return this.id;
}
	/**
	 * 设置：
	 */
	public SysVersion setId(Long id) {
		this.id = id;
		return this;
	}
	/**
	 * 获取：
	 */
	public Long getId() {
		return id;
	}
	/**
	 * 设置：版本号
	 */
	public SysVersion setVersionNo(String versionNo) {
		this.versionNo = versionNo;
		return this;
	}
	/**
	 * 获取：版本号
	 */
	public String getVersionNo() {
		return versionNo;
	}
	/**
	 * 设置：版本对应的数字
	 */
	public SysVersion setVersionNum(Integer versionNum) {
		this.versionNum = versionNum;
		return this;
	}
	/**
	 * 获取：版本对应的数字
	 */
	public Integer getVersionNum() {
		return versionNum;
	}
	/**
	 * 设置：上传或更新时间
	 */
	public SysVersion setUpTime(Date upTime) {
		this.upTime = upTime;
		return this;
	}
	/**
	 * 获取：上传或更新时间
	 */
	public Date getUpTime() {
		return upTime;
	}
	/**
	 * 设置：版本说明
	 */
	public SysVersion setDescr(String descr) {
		this.descr = descr;
		return this;
	}
	/**
	 * 获取：版本说明
	 */
	public String getDescr() {
		return descr;
	}
	/**
	 * 设置：下载地址
	 */
	public SysVersion setDownloadUrl(String downloadUrl) {
		this.downloadUrl = downloadUrl;
		return this;
	}
	/**
	 * 获取：下载地址
	 */
	public String getDownloadUrl() {
		return downloadUrl;
	}
	/**
	 * 设置：状态：1.正常更新 2.强制更新
	 */
	public SysVersion setStatus(Integer status) {
		this.status = status;
		return this;
	}
	/**
	 * 获取：状态：1.正常更新 2.强制更新
	 */
	public Integer getStatus() {
		return status;
	}
	/**
	 * 设置：1.网站 2.app
	 */
	public SysVersion setType(Integer type) {
		this.type = type;
		return this;
	}
	/**
	 * 获取：1.网站 2.app
	 */
	public Integer getType() {
		return type;
	}


}
