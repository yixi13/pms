package com.pms.entity;
import java.io.Serializable;
import java.util.Date;

import com.pms.util.Description;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableName;


/**
 * 
 * 
 * @author zyl
 * @email 517770986@qq.com
 * @date 2017-11-28 10:32:10
 */
@TableName( "base_user_agency")
public class UserAgency extends BaseModel<UserAgency> {
private static final long serialVersionUID = 1L;


	    /**
     *用户ID
     */
    @TableField("user_id")
    @Description("用户ID")
    private Long userId;
	
	    /**
     *机构ID
     */
    @TableField("agency_id")
    @Description("机构ID")
    private Long agencyId;
	
	    /**
     *是否默认1是 2否
     */
    @TableField("state_")
    @Description("是否默认1是 2否")
    private Integer state;
	
	    /**
     *机构类型1物业2社区
     */
    @TableField("type_")
    @Description("机构类型1物业2社区")
    private Integer type;
	
	    /**
     *创建时间
     */
    @TableField("creation_time")
    @Description("创建时间")
    private Date creationTime;


	@Override
	protected Serializable pkVal() {
			return this.userId;
	}

	/**
	 * 设置：用户ID
	 */
	public UserAgency setUserId(Long userId) {
		this.userId = userId;
		return this;
	}
	/**
	 * 获取：用户ID
	 */
	public Long getUserId() {
		return userId;
	}
	/**
	 * 设置：机构ID
	 */
	public UserAgency setAgencyId(Long agencyId) {
		this.agencyId = agencyId;
		return this;
	}
	/**
	 * 获取：机构ID
	 */
	public Long getAgencyId() {
		return agencyId;
	}
	/**
	 * 设置：是否默认1是 2否
	 */
	public UserAgency setState(Integer state) {
		this.state = state;
		return this;
	}
	/**
	 * 获取：是否默认1是 2否
	 */
	public Integer getState() {
		return state;
	}
	/**
	 * 设置：机构类型1物业2社区
	 */
	public UserAgency setType(Integer type) {
		this.type = type;
		return this;
	}
	/**
	 * 获取：机构类型1物业2社区
	 */
	public Integer getType() {
		return type;
	}
	/**
	 * 设置：创建时间
	 */
	public UserAgency setCreationTime(Date creationTime) {
		this.creationTime = creationTime;
		return this;
	}
	/**
	 * 获取：创建时间
	 */
	public Date getCreationTime() {
		return creationTime;
	}


}
