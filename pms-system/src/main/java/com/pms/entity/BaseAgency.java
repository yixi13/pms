package com.pms.entity;
import com.baomidou.mybatisplus.activerecord.Model;
import java.io.Serializable;
import java.util.Date;

import com.pms.util.Description;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;


/**
 * 
 * 
 * @author ljb
 * @email 517770986@qq.com
 * @date 2018-05-24 15:49:53
 */
@TableName( "base_agency")
public class BaseAgency extends Model<BaseAgency> {
private static final long serialVersionUID = 1L;


	 /**
	 *机构表
	 */
    @TableId("agency_id")
    private Long agencyId;
	
	    /**
     *机构名称
     */
    @TableField("agency_name")
    @Description("机构名称")
    private String agencyName;
	
	    /**
     *1平台2自来水公司3物业4设备厂家5水司分区6物业社区
     */
    @TableField("agency_type")
    @Description("1平台2自来水公司3物业4设备厂家5水司分区6物业社区")
    private Integer agencyType;
	
	    /**
     *父级机构ID
     */
    @TableField("parent_id")
    @Description("父级机构ID")
    private Long parentId;
	
	    /**
     *机构电话
     */
    @TableField("agency_phone")
    @Description("机构电话")
    private String agencyPhone;
	
	    /**
     *机构图片
     */
    @TableField("agency_img")
    @Description("机构图片")
    private String agencyImg;
	
	    /**
     *
     */
    @TableField("create_by")
    @Description("")
    private Long createBy;
	
	    /**
     *
     */
    @TableField("create_time")
    @Description("")
    private Date createTime;
	
	    /**
     *
     */
    @TableField("update_by")
    @Description("")
    private Long updateBy;
	
	    /**
     *
     */
    @TableField("update_time")
    @Description("")
    private Date updateTime;
	
	    /**
     *机构状态(1-启用，2-禁用)
     */
    @TableField("agency_state")
    @Description("机构状态(1-启用，2-禁用)")
    private Integer agencyState;
	
	    /**
     *机构所在地址区编码
     */
    @TableField("area_gb")
    @Description("机构所在地址区编码")
    private Integer areaGb;
	
	    /**
     *地址经度
     */
    @TableField("area_longitude")
    @Description("地址经度")
    private Double areaLongitude;
	
	    /**
     *地址纬度
     */
    @TableField("area_latitude")
    @Description("地址纬度")
    private Double areaLatitude;
	
	    /**
     *对应LBS数据id
     */
    @TableField("lbs_poi_id")
    @Description("对应LBS数据id")
    private String lbsPoiId;
	
	    /**
     *地址的详细详细
     */
    @TableField("agency_address")
    @Description("地址的详细详细")
    private String agencyAddress;
	
	    /**
     *级别(一级、二级、三级)
     */
    @TableField("level_")
    @Description("级别(一级、二级、三级)")
    private Integer level;

@Override
protected Serializable pkVal() {
        return this.agencyId;
}
	/**
	 * 设置：机构表
	 */
	public BaseAgency setAgencyId(Long agencyId) {
		this.agencyId = agencyId;
		return this;
	}
	/**
	 * 获取：机构表
	 */
	public Long getAgencyId() {
		return agencyId;
	}
	/**
	 * 设置：机构名称
	 */
	public BaseAgency setAgencyName(String agencyName) {
		this.agencyName = agencyName;
		return this;
	}
	/**
	 * 获取：机构名称
	 */
	public String getAgencyName() {
		return agencyName;
	}
	/**
	 * 设置：1平台2自来水公司3小区4设备厂家
	 */
	public BaseAgency setAgencyType(Integer agencyType) {
		this.agencyType = agencyType;
		return this;
	}
	/**
	 * 获取：1平台2自来水公司3小区4设备厂家
	 */
	public Integer getAgencyType() {
		return agencyType;
	}
	/**
	 * 设置：父级机构ID
	 */
	public BaseAgency setParentId(Long parentId) {
		this.parentId = parentId;
		return this;
	}
	/**
	 * 获取：父级机构ID
	 */
	public Long getParentId() {
		return parentId;
	}
	/**
	 * 设置：机构电话
	 */
	public BaseAgency setAgencyPhone(String agencyPhone) {
		this.agencyPhone = agencyPhone;
		return this;
	}
	/**
	 * 获取：机构电话
	 */
	public String getAgencyPhone() {
		return agencyPhone;
	}
	/**
	 * 设置：机构图片
	 */
	public BaseAgency setAgencyImg(String agencyImg) {
		this.agencyImg = agencyImg;
		return this;
	}
	/**
	 * 获取：机构图片
	 */
	public String getAgencyImg() {
		return agencyImg;
	}
	/**
	 * 设置：
	 */
	public BaseAgency setCreateBy(Long createBy) {
		this.createBy = createBy;
		return this;
	}
	/**
	 * 获取：
	 */
	public Long getCreateBy() {
		return createBy;
	}
	/**
	 * 设置：
	 */
	public BaseAgency setCreateTime(Date createTime) {
		this.createTime = createTime;
		return this;
	}
	/**
	 * 获取：
	 */
	public Date getCreateTime() {
		return createTime;
	}
	/**
	 * 设置：
	 */
	public BaseAgency setUpdateBy(Long updateBy) {
		this.updateBy = updateBy;
		return this;
	}
	/**
	 * 获取：
	 */
	public Long getUpdateBy() {
		return updateBy;
	}
	/**
	 * 设置：
	 */
	public BaseAgency setUpdateTime(Date updateTime) {
		this.updateTime = updateTime;
		return this;
	}
	/**
	 * 获取：
	 */
	public Date getUpdateTime() {
		return updateTime;
	}
	/**
	 * 设置：机构状态(1-启用，2-禁用)
	 */
	public BaseAgency setAgencyState(Integer agencyState) {
		this.agencyState = agencyState;
		return this;
	}
	/**
	 * 获取：机构状态(1-启用，2-禁用)
	 */
	public Integer getAgencyState() {
		return agencyState;
	}
	/**
	 * 设置：机构所在地址区编码
	 */
	public BaseAgency setAreaGb(Integer areaGb) {
		this.areaGb = areaGb;
		return this;
	}
	/**
	 * 获取：机构所在地址区编码
	 */
	public Integer getAreaGb() {
		return areaGb;
	}
	/**
	 * 设置：地址经度
	 */
	public BaseAgency setAreaLongitude(Double areaLongitude) {
		this.areaLongitude = areaLongitude;
		return this;
	}
	/**
	 * 获取：地址经度
	 */
	public Double getAreaLongitude() {
		return areaLongitude;
	}
	/**
	 * 设置：地址纬度
	 */
	public BaseAgency setAreaLatitude(Double areaLatitude) {
		this.areaLatitude = areaLatitude;
		return this;
	}
	/**
	 * 获取：地址纬度
	 */
	public Double getAreaLatitude() {
		return areaLatitude;
	}
	/**
	 * 设置：对应LBS数据id
	 */
	public BaseAgency setLbsPoiId(String lbsPoiId) {
		this.lbsPoiId = lbsPoiId;
		return this;
	}
	/**
	 * 获取：对应LBS数据id
	 */
	public String getLbsPoiId() {
		return lbsPoiId;
	}
	/**
	 * 设置：地址的详细详细
	 */
	public BaseAgency setAgencyAddress(String agencyAddress) {
		this.agencyAddress = agencyAddress;
		return this;
	}
	/**
	 * 获取：地址的详细详细
	 */
	public String getAgencyAddress() {
		return agencyAddress;
	}
	/**
	 * 设置：级别(一级、二级、三级)
	 */
	public BaseAgency setLevel(Integer level) {
		this.level = level;
		return this;
	}
	/**
	 * 获取：级别(一级、二级、三级)
	 */
	public Integer getLevel() {
		return level;
	}


}
