package com.pms.entity;
import com.baomidou.mybatisplus.activerecord.Model;
import java.io.Serializable;
import java.util.Date;
import javax.persistence.*;
import com.pms.util.Description;
import com.pms.entity.BaseModel;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;


/**
 * 
 * 
 * @author ljb
 * @email 517770986@qq.com
 * @date 2018-07-17 14:57:26
 */
@TableName( "company_code")
public class CompanyCode extends BaseModel<CompanyCode> {
private static final long serialVersionUID = 1L;


	    /**
	 *公司编码
	 */
    @TableId("id")
    private Long id;
	
	/**
     *
     */
    @TableField("company_name")
    @Description("")
    private String companyName;
	
	    /**
     *唯一编码
     */
    @TableField("company_code")
    @Description("唯一编码")
    private String companyCode;
	/**
	 *联系号码
	 */
	@TableField("phone_")
	@Description("联系号码")
	private String phone;

	@TableField("name_")
	@Description("联系人姓名")
	private String name;

	@TableField("detail_")
	@Description("简介")
	private String detail;

	@TableField("status_")
	@Description("1启用2禁用")
	private Integer status;


	@TableField("type_")
	@Description("1系统生成2.自定义")
	private Integer type;

	@TableField("crt_time")
	@Description("创建时间")
	private Date crtTime;

	@TableField("upd_time")
	@Description("修改时间")
	private Date updTime;

	// ID赋值
public CompanyCode(){
        this.id= returnStaticIdLong();
        }
@Override
protected Serializable pkVal() {
        return this.id;
}
	/**
	 * 设置：公司编码
	 */
	public CompanyCode setId(Long id) {
		this.id = id;
		return this;
	}
	/**
	 * 获取：公司编码
	 */
	public Long getId() {
		return id;
	}
	/**
	 * 设置：
	 */
	public CompanyCode setCompanyName(String companyName) {
		this.companyName = companyName;
		return this;
	}
	/**
	 * 获取：
	 */
	public String getCompanyName() {
		return companyName;
	}
	/**
	 * 设置：唯一编码
	 */
	public CompanyCode setCompanyCode(String companyCode) {
		this.companyCode = companyCode;
		return this;
	}
	/**
	 * 获取：唯一编码
	 */
	public String getCompanyCode() {
		return companyCode;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	public String getName() {
		return name;
	}

	public String getDetail() {
		return detail;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setDetail(String detail) {
		this.detail = detail;
	}

	public Integer getType() {
		return type;
	}

	public void setType(Integer type) {
		this.type = type;
	}

	public Date getCrtTime() {
		return crtTime;
	}

	public Date getUpdTime() {
		return updTime;
	}

	public void setCrtTime(Date crtTime) {
		this.crtTime = crtTime;
	}

	public void setUpdTime(Date updTime) {
		this.updTime = updTime;
	}
}
