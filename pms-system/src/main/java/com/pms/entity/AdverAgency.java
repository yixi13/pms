package com.pms.entity;
import java.io.Serializable;
import java.util.Date;

import com.pms.util.Description;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;


/**
 * 
 * 
 * @author zyl
 * @email 517770986@qq.com
 * @date 2017-11-20 14:28:50
 */
@TableName( "adver_agency")
public class AdverAgency extends BaseModel<AdverAgency> {
private static final long serialVersionUID = 1L;


	/**
	 *广告机构表
	 */
    @TableId("id_")
    private Long id;
	
	    /**
     *广告ID
     */
    @TableField("adver_id")
    @Description("广告ID")
    private Long adverId;
	
	    /**
     *机构ID
     */
    @TableField("agency_id")
    @Description("机构ID")
    private Long agencyId;
	
	    /**
     *创建时间
     */
    @TableField("create_time")
    @Description("创建时间")
    private Date createTime;
	
	    /**
     *创建人
     */
    @TableField("create_by")
    @Description("创建人")
    private Long createBy;
	
// ID赋值
public AdverAgency(){
        this.id= returnIdLong();
        }
@Override
protected Serializable pkVal() {
        return this.id;
}
	/**
	 * 设置：广告机构表
	 */
	public AdverAgency setId(Long id) {
		this.id = id;
		return this;
	}
	/**
	 * 获取：广告机构表
	 */
	public Long getId() {
		return id;
	}
	/**
	 * 设置：广告ID
	 */
	public AdverAgency setAdverId(Long adverId) {
		this.adverId = adverId;
		return this;
	}
	/**
	 * 获取：广告ID
	 */
	public Long getAdverId() {
		return adverId;
	}
	/**
	 * 设置：机构ID
	 */
	public AdverAgency setAgencyId(Long agencyId) {
		this.agencyId = agencyId;
		return this;
	}
	/**
	 * 获取：机构ID
	 */
	public Long getAgencyId() {
		return agencyId;
	}
	/**
	 * 设置：创建时间
	 */
	public AdverAgency setCreateTime(Date createTime) {
		this.createTime = createTime;
		return this;
	}
	/**
	 * 获取：创建时间
	 */
	public Date getCreateTime() {
		return createTime;
	}
	/**
	 * 设置：创建人
	 */
	public AdverAgency setCreateBy(Long createBy) {
		this.createBy = createBy;
		return this;
	}
	/**
	 * 获取：创建人
	 */
	public Long getCreateBy() {
		return createBy;
	}


}
