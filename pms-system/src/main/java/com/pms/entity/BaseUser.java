package com.pms.entity;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;
import com.baomidou.mybatisplus.enums.IdType;
import com.fasterxml.jackson.annotation.JsonIgnore;
import io.swagger.models.auth.In;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
/**
 * <p>
 *
 * </p>
 *
 * @author zyl
 * @since 2017-07-06
 */
@TableName("base_user")
public class BaseUser extends BaseModel<BaseUser>  {

	private static final long serialVersionUID = 1L;

//	@TableId(value="id", type= IdType.AUTO)
	private Long id;
	private String username;
	private String password;
	private String name;
	@TableField("mobile_phone")
	private String mobilePhone;
	@TableField("tel_phone")
	private String telPhone;
	private String email;
	private Integer sex;
	/**
	 * 用户角色
	 * 1.平台2.厂家3.水厂4.物业
	 */
	private Integer type;
	private Integer status;
	private String description;
	@TableField("crt_time")
	private Date crtTime;
	@TableField("crt_user")
	private String crtUser;
	@TableField("crt_name")
	private String crtName;
	@TableField("crt_host")
	private String crtHost;
	@TableField("upd_time")
	private Date updTime;
	@TableField("upd_user")
	private String updUser;
	@TableField("upd_name")
	private String updName;
	@TableField("upd_host")
	private String updHost;
	@TableField("company_code")
	private String companyCode;
	@TableField("img_")
	private String img;
	/**
	 * 删除状态1正常2删除
	 */
	@TableField("deleted_state")
	private int deletedState;
	/**
	 * 临时字段
	 */
	@JsonIgnore
	@TableField(exist = false)
	private String userCom;
	/**
	 * 临时字段
	 */
	@TableField(exist = false)
	private Long groupId;

	@TableField(exist = false)
	private List<UserAgency> userAgency;


	@TableField(exist = false)
	private Integer  groupMemberId;

	@TableField(exist = false)
	private String  groupname;

	@TableField(exist = false)
	private Date  loginInTime;

	@TableField(exist = false)
	private Integer  groupType;

	@TableField(exist = false)
	private Integer  roleType;

	public String getImg() {
		return img;
	}

	public void setImg(String img) {
		this.img = img;
	}

	public Integer getGroupType() {
		return groupType;
	}

	public void setGroupType(Integer groupType) {
		this.groupType = groupType;
	}

	public Date getLoginInTime() {
		return loginInTime;
	}

	public void setLoginInTime(Date loginInTime) {
		this.loginInTime = loginInTime;
	}

	public String getGroupname() {
		return groupname;
	}

	public void setGroupname(String groupname) {
		this.groupname = groupname;
	}

	public String getUserCom() {
		return userCom;
	}

	public Long getGroupId() {
		return groupId;
	}

	public List<UserAgency> getUserAgency() {
		return userAgency;
	}

	public Integer getGroupMemberId() {
		return groupMemberId;
	}

	public void setUserCom(String userCom) {
		this.userCom = userCom;
	}

	public void setGroupId(Long groupId) {
		this.groupId = groupId;
	}

	public void setUserAgency(List<UserAgency> userAgency) {
		this.userAgency = userAgency;
	}

	public void setGroupMemberId(Integer groupMemberId) {
		this.groupMemberId = groupMemberId;
	}

	// ID赋值
	public BaseUser(){
		this.id= returnStaticIdLong();
	}
	@Override
	protected Serializable pkVal() {
		return this.id;
	}

	public BaseUser setId(Long id) {
		this.id = id;
		return this;
	}
	public Long getId() {
		return id;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getMobilePhone() {
		return mobilePhone;
	}

	public void setMobilePhone(String mobilePhone) {
		this.mobilePhone = mobilePhone;
	}

	public String getTelPhone() {
		return telPhone;
	}

	public void setTelPhone(String telPhone) {
		this.telPhone = telPhone;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public Integer getSex() {
		return sex;
	}

	public void setSex(Integer sex) {
		this.sex = sex;
	}

	public Integer getType() {
		return type;
	}

	public void setType(Integer type) {
		this.type = type;
	}

	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Date getCrtTime() {
		return crtTime;
	}

	public void setCrtTime(Date crtTime) {
		this.crtTime = crtTime;
	}

	public String getCrtUser() {
		return crtUser;
	}

	public void setCrtUser(String crtUser) {
		this.crtUser = crtUser;
	}

	public String getCrtName() {
		return crtName;
	}

	public void setCrtName(String crtName) {
		this.crtName = crtName;
	}

	public String getCrtHost() {
		return crtHost;
	}

	public void setCrtHost(String crtHost) {
		this.crtHost = crtHost;
	}

	public Date getUpdTime() {
		return updTime;
	}

	public void setUpdTime(Date updTime) {
		this.updTime = updTime;
	}

	public String getUpdUser() {
		return updUser;
	}

	public void setUpdUser(String updUser) {
		this.updUser = updUser;
	}

	public String getUpdName() {
		return updName;
	}

	public void setUpdName(String updName) {
		this.updName = updName;
	}

	public String getUpdHost() {
		return updHost;
	}

	public void setUpdHost(String updHost) {
		this.updHost = updHost;
	}

	public String getCompanyCode() {
		return companyCode;
	}

	public void setCompanyCode(String companyCode) {
		this.companyCode = companyCode;
	}

	public Integer getRoleType() {
		return roleType;
	}

	public void setRoleType(Integer roleType) {
		this.roleType = roleType;
	}

	public int getDeletedState() {
		return deletedState;
	}

	public void setDeletedState(int deletedState) {
		this.deletedState = deletedState;
	}
}
