package com.pms.entity;
import java.io.Serializable;
import java.util.Date;

import com.pms.util.Description;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;


/**
 * 
 * 
 * @author zyl
 * @email 517770986@qq.com
 * @date 2017-10-25 09:40:12
 */
@TableName( "sys_member")
public class SysMember extends BaseModel<SysMember> {
private static final long serialVersionUID = 1L;

	//会员ID
    @TableId("member_id")
	@Description("会员ID")
    private Long memberId;
	
	    //用户账号
    @TableField("user_account")
    @Description("用户账号")
    private String userAccount;
	
	    //会员密码
    @TableField("pass_word")
    @Description("会员密码")
    private String passWord;
	
	    //会员昵称
    @TableField("nick_name")
    @Description("会员昵称")
    private String nickName;
	
	    //个性签名
    @TableField("member_sign")
    @Description("个性签名")
    private String memberSign;
	
	    //会员真实姓名
    @TableField("real_name")
    @Description("会员真实姓名")
    private String realName;
	
	    //手机号
    @TableField("phone_")
    @Description("手机号")
    private String phone;
	
	    //所属区域
    @TableField("area_id")
    @Description("所属区域")
    private Long areaId;
	
	    //地址
    @TableField("address_")
    @Description("地址")
    private String address;
	
	    //图像
    @TableField("img_")
    @Description("图像")
    private String img;
	
	    //
    @TableField("email_")
    @Description("邮箱")
    private String email;
	
	    //性别 0女 1是男
    @TableField("sex_")
    @Description("性别 0女 1是男")
    private Integer sex;
	
	    //年龄
    @TableField("age_")
    @Description("年龄")
    private Integer age;
	
	    //身份证
    @TableField("id_card")
    @Description("身份证")
    private String idCard;

	
	    //会员类型
    @TableField("mem_type")
    @Description("会员类型")
    private Integer memType;
	
	    //是否禁用 1启用 2-禁用
    @TableField("is_disable")
    @Description("是否禁用1启用 2-禁用")
    private Integer isDisable;
	
	    //禁用类型 1-圈子发布不健康信息
    @TableField("disable_type")
    @Description("禁用类型 1-圈子发布不健康信息")
    private Integer disableType;
	
	    //禁用时间 1-圈子
    @TableField("disable_time")
    @Description("禁用时间 ")
    private Date disableTime;
	
	    //邀请码
    @TableField("my_recommend")
    @Description("邀请码")
    private String myRecommend;
	
	    //会员等级
    @TableField("grade_id")
    @Description("会员等级")
    private String gradeId;
	
	    //
    @TableField("create_by")
    @Description("创建人")
    private String createBy;
	
	    //
    @TableField("create_time")
    @Description("创建时间")
    private Date createTime;
	
	    //
    @TableField("update_by")
    @Description("修改人")
    private String updateBy;
	
	    //
    @TableField("update_time")
    @Description("修改时间")
    private Date updateTime;
	
	    //
    @TableField("sn_")
    @Description("sn码")
    private String sn;
	
	    //推荐人
    @TableField("referrer_id")
    @Description("推荐人")
    private String referrerId;
	

	    //手机mac码
    @TableField("mac_address")
    @Description("手机mac码")
    private String macAddress;
	
	    //
    @TableField("account_type")
    @Description("")
    private Integer accountType;
	
	    //积分
    @TableField("integral")
    @Description("积分")
    private Integer integral;

	//微信ID 唯一标识
	@TableField("weixin_openid")
	@Description("微信ID 唯一标识")
	private String weixinOpenid;



// ID赋值
public SysMember(){
        this.memberId= returnStaticIdLong();
        }
@Override
protected Serializable pkVal() {
        return this.memberId;
}


	public String getWeixinOpenid() {
		return weixinOpenid;
	}

	public void setWeixinOpenid(String weixinOpenid) {
		this.weixinOpenid = weixinOpenid;
	}

	/**
	 * 设置：
	 */
	public void setMemberId(Long memberId) {
		this.memberId = memberId;
	}
	/**
	 * 获取：
	 */
	public Long getMemberId() {
		return memberId;
	}
	/**
	 * 设置：用户账号
	 */
	public void setUserAccount(String userAccount) {
		this.userAccount = userAccount;
	}
	/**
	 * 获取：用户账号
	 */
	public String getUserAccount() {
		return userAccount;
	}
	/**
	 * 设置：会员密码
	 */
	public void setPassWord(String passWord) {
		this.passWord = passWord;
	}
	/**
	 * 获取：会员密码
	 */
	public String getPassWord() {
		return passWord;
	}
	/**
	 * 设置：
	 */
	public void setNickName(String nickName) {
		this.nickName = nickName;
	}
	/**
	 * 获取：
	 */
	public String getNickName() {
		return nickName;
	}
	/**
	 * 设置：个性签名
	 */
	public void setMemberSign(String memberSign) {
		this.memberSign = memberSign;
	}
	/**
	 * 获取：个性签名
	 */
	public String getMemberSign() {
		return memberSign;
	}
	/**
	 * 设置：会员真实姓名
	 */
	public void setRealName(String realName) {
		this.realName = realName;
	}
	/**
	 * 获取：会员真实姓名
	 */
	public String getRealName() {
		return realName;
	}
	/**
	 * 设置：手机号
	 */
	public void setPhone(String phone) {
		this.phone = phone;
	}
	/**
	 * 获取：手机号
	 */
	public String getPhone() {
		return phone;
	}
	/**
	 * 设置：所属区域
	 */
	public void setAreaId(Long areaId) {
		this.areaId = areaId;
	}
	/**
	 * 获取：所属区域
	 */
	public Long getAreaId() {
		return areaId;
	}
	/**
	 * 设置：地址
	 */
	public void setAddress(String address) {
		this.address = address;
	}
	/**
	 * 获取：地址
	 */
	public String getAddress() {
		return address;
	}
	/**
	 * 设置：图像
	 */
	public void setImg(String img) {
		this.img = img;
	}
	/**
	 * 获取：图像
	 */
	public String getImg() {
		return img;
	}
	/**
	 * 设置：
	 */
	public void setEmail(String email) {
		this.email = email;
	}
	/**
	 * 获取：
	 */
	public String getEmail() {
		return email;
	}
	/**
	 * 设置：性别 0女 1是男
	 */
	public void setSex(Integer sex) {
		this.sex = sex;
	}
	/**
	 * 获取：性别 0女 1是男
	 */
	public Integer getSex() {
		return sex;
	}
	/**
	 * 设置：年龄
	 */
	public void setAge(Integer age) {
		this.age = age;
	}
	/**
	 * 获取：年龄
	 */
	public Integer getAge() {
		return age;
	}
	/**
	 * 设置：身份证
	 */
	public void setIdCard(String idCard) {
		this.idCard = idCard;
	}
	/**
	 * 获取：身份证
	 */
	public String getIdCard() {
		return idCard;
	}

	/**
	 * 设置：会员类型
	 */
	public void setMemType(Integer memType) {
		this.memType = memType;
	}
	/**
	 * 获取：会员类型
	 */
	public Integer getMemType() {
		return memType;
	}
	/**
	 * 设置：是否禁用 2-禁用
	 */
	public void setIsDisable(Integer isDisable) {
		this.isDisable = isDisable;
	}
	/**
	 * 获取：是否禁用 2-禁用
	 */
	public Integer getIsDisable() {
		return isDisable;
	}
	/**
	 * 设置：禁用类型 1-圈子发布不健康信息
	 */
	public void setDisableType(Integer disableType) {
		this.disableType = disableType;
	}
	/**
	 * 获取：禁用类型 1-圈子发布不健康信息
	 */
	public Integer getDisableType() {
		return disableType;
	}
	/**
	 * 设置：禁用时间 1-圈子
	 */
	public void setDisableTime(Date disableTime) {
		this.disableTime = disableTime;
	}
	/**
	 * 获取：禁用时间 1-圈子
	 */
	public Date getDisableTime() {
		return disableTime;
	}
	/**
	 * 设置：邀请码
	 */
	public void setMyRecommend(String myRecommend) {
		this.myRecommend = myRecommend;
	}
	/**
	 * 获取：邀请码
	 */
	public String getMyRecommend() {
		return myRecommend;
	}
	/**
	 * 设置：会员等级
	 */
	public void setGradeId(String gradeId) {
		this.gradeId = gradeId;
	}
	/**
	 * 获取：会员等级
	 */
	public String getGradeId() {
		return gradeId;
	}
	/**
	 * 设置：
	 */
	public void setCreateBy(String createBy) {
		this.createBy = createBy;
	}
	/**
	 * 获取：
	 */
	public String getCreateBy() {
		return createBy;
	}
	/**
	 * 设置：
	 */
	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}
	/**
	 * 获取：
	 */
	public Date getCreateTime() {
		return createTime;
	}
	/**
	 * 设置：
	 */
	public void setUpdateBy(String updateBy) {
		this.updateBy = updateBy;
	}
	/**
	 * 获取：
	 */
	public String getUpdateBy() {
		return updateBy;
	}
	/**
	 * 设置：
	 */
	public void setUpdateTime(Date updateTime) {
		this.updateTime = updateTime;
	}
	/**
	 * 获取：
	 */
	public Date getUpdateTime() {
		return updateTime;
	}
	/**
	 * 设置：
	 */
	public void setSn(String sn) {
		this.sn = sn;
	}
	/**
	 * 获取：
	 */
	public String getSn() {
		return sn;
	}
	/**
	 * 设置：推荐人
	 */
	public void setReferrerId(String referrerId) {
		this.referrerId = referrerId;
	}
	/**
	 * 获取：推荐人
	 */
	public String getReferrerId() {
		return referrerId;
	}
		/**
	 * 设置：手机mac码
	 */
	public void setMacAddress(String macAddress) {
		this.macAddress = macAddress;
	}
	/**
	 * 获取：手机mac码
	 */
	public String getMacAddress() {
		return macAddress;
	}
	/**
	 * 设置：
	 */
	public void setAccountType(Integer accountType) {
		this.accountType = accountType;
	}
	/**
	 * 获取：
	 */
	public Integer getAccountType() {
		return accountType;
	}
	/**
	 * 设置：积分
	 */
	public void setIntegral(Integer integral) {
		this.integral = integral;
	}
	/**
	 * 获取：积分
	 */
	public Integer getIntegral() {
		return integral;
	}


}
