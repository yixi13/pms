package com.pms.entity;
import java.io.Serializable;

import com.pms.util.Description;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;


/**
 * 
 * 
 * @author zyl
 * @email 517770986@qq.com
 * @date 2017-12-21 16:39:49
 */
@TableName( "area_cn")
public class AreaCn extends BaseModel<AreaCn> {
private static final long serialVersionUID = 1L;



	 /**
	 *
	 */
    @TableId("id")
    private Long id;
	
	    /**
     *省/直辖市名称
     */
    @TableField("province_name")
    @Description("省/直辖市名称")
    private String provinceName;
	
	    /**
     *市名称
     */
    @TableField("city_name")
    @Description("市名称")
    private String cityName;
	
	    /**
     *区县名称
     */
    @TableField("county_name")
    @Description("区县名称")
    private String countyName;
	
	    /**
     *省GB码
     */
    @TableField("province_gb")
    @Description("省GB码")
    private Integer provinceGb;
	
	    /**
     *市GB码
     */
    @TableField("city_gb")
    @Description("市GB码")
    private Integer cityGb;
	
	    /**
     *区县GB码
     */
    @TableField("county_gb")
    @Description("区县GB码")
    private Integer countyGb;
	
	    /**
     *别名
     */
    @TableField("aliases")
    @Description("别名")
    private String aliases;
	
// ID赋值
public AreaCn(){
        this.id= returnIdLong();
        }
@Override
protected Serializable pkVal() {
        return this.id;
}
	/**
	 * 设置：
	 */
	public AreaCn setId(Long id) {
		this.id = id;
		return this;
	}
	/**
	 * 获取：
	 */
	public Long getId() {
		return id;
	}
	/**
	 * 设置：省/直辖市名称
	 */
	public AreaCn setProvinceName(String provinceName) {
		this.provinceName = provinceName;
		return this;
	}
	/**
	 * 获取：省/直辖市名称
	 */
	public String getProvinceName() {
		return provinceName;
	}
	/**
	 * 设置：市名称
	 */
	public AreaCn setCityName(String cityName) {
		this.cityName = cityName;
		return this;
	}
	/**
	 * 获取：市名称
	 */
	public String getCityName() {
		return cityName;
	}
	/**
	 * 设置：区县名称
	 */
	public AreaCn setCountyName(String countyName) {
		this.countyName = countyName;
		return this;
	}
	/**
	 * 获取：区县名称
	 */
	public String getCountyName() {
		return countyName;
	}
	/**
	 * 设置：省GB码
	 */
	public AreaCn setProvinceGb(Integer provinceGb) {
		this.provinceGb = provinceGb;
		return this;
	}
	/**
	 * 获取：省GB码
	 */
	public Integer getProvinceGb() {
		return provinceGb;
	}
	/**
	 * 设置：市GB码
	 */
	public AreaCn setCityGb(Integer cityGb) {
		this.cityGb = cityGb;
		return this;
	}
	/**
	 * 获取：市GB码
	 */
	public Integer getCityGb() {
		return cityGb;
	}
	/**
	 * 设置：区县GB码
	 */
	public AreaCn setCountyGb(Integer countyGb) {
		this.countyGb = countyGb;
		return this;
	}
	/**
	 * 获取：区县GB码
	 */
	public Integer getCountyGb() {
		return countyGb;
	}
	/**
	 * 设置：别名
	 */
	public AreaCn setAliases(String aliases) {
		this.aliases = aliases;
		return this;
	}
	/**
	 * 获取：别名
	 */
	public String getAliases() {
		return aliases;
	}


}
