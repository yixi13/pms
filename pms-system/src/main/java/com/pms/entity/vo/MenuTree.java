package com.pms.entity.vo;

import com.baomidou.mybatisplus.annotations.TableField;
import com.pms.entity.BaseElement;
import com.pms.entity.TreeNode;

import java.util.List;

/**
 * @author zyl
 * @create 2017-07-06 15:12
 **/
public class MenuTree extends TreeNode{
    String icon;
    String title;
    String href;
    boolean spread = false;

    @TableField(exist = true)
    private List<BaseElement> baseElement;

    public List<BaseElement> getBaseElement() {
        return baseElement;
    }

    public void setBaseElement(List<BaseElement> baseElement) {
        this.baseElement = baseElement;
    }

    public MenuTree() {
    }

    public MenuTree(int id, String name, int parentId) {
        this.id = id;
        this.parentId = parentId;
        this.title = name;
    }
    public MenuTree(int id, String name, MenuTree parent) {
        this.id = id;
        this.parentId = parent.getId();
        this.title = name;
    }
    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getHref() {
        return href;
    }

    public void setHref(String href) {
        this.href = href;
    }

    public boolean isSpread() {
        return spread;
    }

    public void setSpread(boolean spread) {
        this.spread = spread;
    }
}
