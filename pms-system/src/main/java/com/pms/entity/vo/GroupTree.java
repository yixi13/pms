package com.pms.entity.vo;


import com.pms.entity.TreeNode;

/**
 * @author zyl
 * @create 2017-07-06 15:15
 **/
public class GroupTree extends TreeNode {
    String label;

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
