package com.pms.entity;

import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;
import com.pms.util.Description;

import java.io.Serializable;
import java.util.Date;


/**
 * 用户社区关联表
 * 
 * @author zyl
 * @email 517770986@qq.com
 * @date 2018-03-19 10:46:43
 */
@TableName( "user_community")
public class UserCommunity extends BaseModel<UserCommunity> {
private static final long serialVersionUID = 1L;


	    /**
	 *用户小区表id
	 */
    @TableId("user_house_id")
    private Long userHouseId;
	
	    /**
     *用户表id
     */
    @TableField("user_id")
    @Description("用户表id")
    private Long userId;
	
	    /**
     *用户名称
     */
    @TableField("user_name")
    @Description("用户名称")
    private String userName;
	
	    /**
     *小区id
     */
    @TableField("user_community_id")
    @Description("小区id")
    private Long userCommunityId;

	    /**
     *创建时间
     */
    @TableField("create_time")
    @Description("创建时间")
    private Date createTime;

	@TableField("county_gb")
	@Description("区县GB码")
	private Integer countyGb;

	@TableField(exist = false)
	private AreaCn areaCn;
	/**
	 * 默认1.默认2.否
	 */
	@TableField("thedefault")
	@Description("默认小区")
	private Integer thedefault;

// ID赋值
public UserCommunity(){
        this.userHouseId= returnIdLong();
        }
@Override
protected Serializable pkVal() {
        return this.userHouseId;
}
	/**
	 * 设置：用户小区表id
	 */
	public UserCommunity setUserHouseId(Long userHouseId) {
		this.userHouseId = userHouseId;
		return this;
	}
	/**
	 * 获取：用户小区表id
	 */
	public Long getUserHouseId() {
		return userHouseId;
	}
	/**
	 * 设置：用户表id
	 */
	public UserCommunity setUserId(Long userId) {
		this.userId = userId;
		return this;
	}

	public void setAreaCn(AreaCn areaCn) {
		this.areaCn = areaCn;
	}
	public AreaCn getAreaCn() {
		return areaCn;
	}
	/**
	 * 获取：用户表id
	 */
	public Long getUserId() {
		return userId;
	}
	/**
	 * 设置：用户名称
	 */
	public UserCommunity setUserName(String userName) {
		this.userName = userName;
		return this;
	}

	public Integer getCountyGb() {
		return countyGb;
	}

	public void setCountyGb(Integer countyGb) {
		this.countyGb = countyGb;
	}

	/**
	 * 获取：用户名称
	 */
	public String getUserName() {
		return userName;
	}

	public Long getUserCommunityId() {
		return userCommunityId;
	}

	public void setUserCommunityId(Long userCommunityId) {
		this.userCommunityId = userCommunityId;
	}

	/**
	 * 设置：创建时间
	 */
	public UserCommunity setCreateTime(Date createTime) {
		this.createTime = createTime;
		return this;
	}
	/**
	 * 获取：创建时间
	 */
	public Date getCreateTime() {
		return createTime;
	}


	public Integer getThedefault() {
		return thedefault;
	}

	public void setThedefault(Integer thedefault) {
		this.thedefault = thedefault;
	}
}
