package com.pms.entity;
import java.io.Serializable;
import java.util.Date;

import com.pms.util.Description;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;


/**
 * 
 * 
 * @author zyl
 * @email 517770986@qq.com
 * @date 2017-10-25 15:05:11
 */
@TableName( "sys_member_address")
public class SysMemberAddress extends BaseModel<SysMemberAddress> {
private static final long serialVersionUID = 1L;

	    //会员地址ID
    @TableId("member_address_id")
    private Long memberAddressId;
	
	    //机构ID
    @TableField("sp_id")
    @Description("机构ID")
    private String spId;
	
	    //会员ID
    @TableField("member_id")
    @Description("会员ID")
    private String memberId;

	//
	@TableField("menuList")
	@Description(" ")
	private String menuList;

	//座机
	@TableField("tel_phone")
	@Description("座机")
	private String telPhone;

	//姓名
    @TableField("person_name")
    @Description("姓名")
    private String personName;
	
	    //省ID
    @TableField("provincial_id")
    @Description("省ID")
    private String provincialId;
	
	    //市ID
    @TableField("city_id")
    @Description("市ID")
    private String cityId;
	
	    //县级ID
    @TableField("area_id")
    @Description("县级ID")
    private String areaId;
	
	    //省级地址
    @TableField("provincial_address")
    @Description("省级地址")
    private String provincialAddress;
	
	    //市级地址
    @TableField("city_address")
    @Description("市级地址")
    private String cityAddress;
	
	    //县级地址
    @TableField("area_address")
    @Description("县级地址")
    private String areaAddress;
	
	    //详细地址
    @TableField("detail_address")
    @Description("详细地址")
    private String detailAddress;
	
	    //收件人电话
    @TableField("phone_")
    @Description("收件人电话")
    private String phone;
	
	    //邮政编码
    @TableField("post_code")
    @Description("邮政编码")
    private String postCode;
	
	    //是否为默认地址1是0否
    @TableField("is_default")
    @Description("是否为默认地址1是0否")
    private Integer isDefault;
	
	    //创建人
    @TableField("create_by")
    @Description("创建人")
    private String createBy;
	
	    //创建时间
    @TableField("create_time")
    @Description("创建时间")
    private Date createTime;
	
	    //修改人
    @TableField("update_by")
    @Description("修改人")
    private String updateBy;
	
	    //修改时间
    @TableField("update_time")
    @Description("修改时间")
    private Date updateTime;
	
// ID赋值
public SysMemberAddress(){
        this.memberAddressId= returnIdLong();
        }
@Override
protected Serializable pkVal() {
        return this.memberAddressId;
}
	/**
	 * 设置：会员地址ID
	 */
	public void setMemberAddressId(Long memberAddressId) {
		this.memberAddressId = memberAddressId;
	}
	/**
	 * 获取：会员地址ID
	 */
	public Long getMemberAddressId() {
		return memberAddressId;
	}
	/**
	 * 设置：机构ID
	 */
	public void setSpId(String spId) {
		this.spId = spId;
	}
	/**
	 * 获取：机构ID
	 */
	public String getSpId() {
		return spId;
	}
	/**
	 * 设置：会员ID
	 */
	public void setMemberId(String memberId) {
		this.memberId = memberId;
	}
	/**
	 * 获取：会员ID
	 */
	public String getMemberId() {
		return memberId;
	}
	/**
	 * 设置：姓名
	 */
	public void setPersonName(String personName) {
		this.personName = personName;
	}
	/**
	 * 获取：姓名
	 */
	public String getPersonName() {
		return personName;
	}
	/**
	 * 设置：省ID
	 */
	public void setProvincialId(String provincialId) {
		this.provincialId = provincialId;
	}
	/**
	 * 获取：省ID
	 */
	public String getProvincialId() {
		return provincialId;
	}
	/**
	 * 设置：市ID
	 */
	public void setCityId(String cityId) {
		this.cityId = cityId;
	}
	/**
	 * 获取：市ID
	 */
	public String getCityId() {
		return cityId;
	}
	/**
	 * 设置：县级ID
	 */
	public void setAreaId(String areaId) {
		this.areaId = areaId;
	}
	/**
	 * 获取：县级ID
	 */
	public String getAreaId() {
		return areaId;
	}
	/**
	 * 设置：省级地址
	 */
	public void setProvincialAddress(String provincialAddress) {
		this.provincialAddress = provincialAddress;
	}
	/**
	 * 获取：省级地址
	 */
	public String getProvincialAddress() {
		return provincialAddress;
	}
	/**
	 * 设置：市级地址
	 */
	public void setCityAddress(String cityAddress) {
		this.cityAddress = cityAddress;
	}
	/**
	 * 获取：市级地址
	 */
	public String getCityAddress() {
		return cityAddress;
	}
	/**
	 * 设置：县级地址
	 */
	public void setAreaAddress(String areaAddress) {
		this.areaAddress = areaAddress;
	}
	/**
	 * 获取：县级地址
	 */
	public String getAreaAddress() {
		return areaAddress;
	}
	/**
	 * 设置：详细地址
	 */
	public void setDetailAddress(String detailAddress) {
		this.detailAddress = detailAddress;
	}
	/**
	 * 获取：详细地址
	 */
	public String getDetailAddress() {
		return detailAddress;
	}
	/**
	 * 设置：收件人电话
	 */
	public void setPhone(String phone) {
		this.phone = phone;
	}
	/**
	 * 获取：收件人电话
	 */
	public String getPhone() {
		return phone;
	}
	/**
	 * 设置：邮政编码
	 */
	public void setPostCode(String postCode) {
		this.postCode = postCode;
	}
	/**
	 * 获取：邮政编码
	 */
	public String getPostCode() {
		return postCode;
	}
	/**
	 * 设置：是否为默认地址1是0否
	 */
	public void setIsDefault(Integer isDefault) {
		this.isDefault = isDefault;
	}
	/**
	 * 获取：是否为默认地址1是0否
	 */
	public Integer getIsDefault() {
		return isDefault;
	}
	/**
	 * 设置：创建人
	 */
	public void setCreateBy(String createBy) {
		this.createBy = createBy;
	}
	/**
	 * 获取：创建人
	 */
	public String getCreateBy() {
		return createBy;
	}
	/**
	 * 设置：创建时间
	 */
	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}
	/**
	 * 获取：创建时间
	 */
	public Date getCreateTime() {
		return createTime;
	}
	/**
	 * 设置：修改人
	 */
	public void setUpdateBy(String updateBy) {
		this.updateBy = updateBy;
	}
	/**
	 * 获取：修改人
	 */
	public String getUpdateBy() {
		return updateBy;
	}
	/**
	 * 设置：修改时间
	 */
	public void setUpdateTime(Date updateTime) {
		this.updateTime = updateTime;
	}
	/**
	 * 获取：修改时间
	 */
	public Date getUpdateTime() {
		return updateTime;
	}

	public String getMenuList() {
		return menuList;
	}

	public String getTelPhone() {
		return telPhone;
	}

	public void setMenuList(String menuList) {
		this.menuList = menuList;
	}

	public void setTelPhone(String telPhone) {
		this.telPhone = telPhone;
	}
}
