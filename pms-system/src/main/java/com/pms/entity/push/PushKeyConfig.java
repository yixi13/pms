package com.pms.entity.push;
import java.io.Serializable;

import com.pms.entity.BaseModel;
import com.pms.util.Description;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;


/**
 * 
 * 
 * @author zyl
 * @email 517770986@qq.com
 * @date 2017-12-01 11:12:07
 */
@TableName( "push_key_config")
public class PushKeyConfig extends BaseModel<PushKeyConfig> {
private static final long serialVersionUID = 1L;


	    /**
	 *
	 */
    @TableId("id_")
    private Long id;
	
	    /**
     *机构id
     */
    @TableField("agency_id")
    @Description("机构id")
    private Long agencyId;
	
	    /**
     *机构名称
     */
    @TableField("anency_name")
    @Description("机构名称")
    private String anencyName;
	
	    /**
     *apiKey
     */
    @TableField("api_key")
    @Description("apiKey")
    private String apiKey;
	
	    /**
     *secretKey
     */
    @TableField("secret_key")
    @Description("secretKey")
    private String secretKey;
	
// ID赋值
public PushKeyConfig(){
        this.id= returnIdLong();
        }
@Override
protected Serializable pkVal() {
        return this.id;
}
	/**
	 * 设置：
	 */
	public PushKeyConfig setId(Long id) {
		this.id = id;
		return this;
	}
	/**
	 * 获取：
	 */
	public Long getId() {
		return id;
	}
	/**
	 * 设置：机构id
	 */
	public PushKeyConfig setAgencyId(Long agencyId) {
		this.agencyId = agencyId;
		return this;
	}
	/**
	 * 获取：机构id
	 */
	public Long getAgencyId() {
		return agencyId;
	}
	/**
	 * 设置：机构名称
	 */
	public PushKeyConfig setAnencyName(String anencyName) {
		this.anencyName = anencyName;
		return this;
	}
	/**
	 * 获取：机构名称
	 */
	public String getAnencyName() {
		return anencyName;
	}
	/**
	 * 设置：apiKey
	 */
	public PushKeyConfig setApiKey(String apiKey) {
		this.apiKey = apiKey;
		return this;
	}
	/**
	 * 获取：apiKey
	 */
	public String getApiKey() {
		return apiKey;
	}
	/**
	 * 设置：secretKey
	 */
	public PushKeyConfig setSecretKey(String secretKey) {
		this.secretKey = secretKey;
		return this;
	}
	/**
	 * 获取：secretKey
	 */
	public String getSecretKey() {
		return secretKey;
	}


}
