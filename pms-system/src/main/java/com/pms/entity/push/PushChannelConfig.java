package com.pms.entity.push;
import java.io.Serializable;
import java.util.Date;

import com.pms.entity.BaseModel;
import com.pms.util.Description;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;


/**
 * 
 * 
 * @author zyl
 * @email 517770986@qq.com
 * @date 2017-12-01 11:12:07
 */
@TableName( "push_channel_config")
public class PushChannelConfig extends BaseModel<PushChannelConfig> {
private static final long serialVersionUID = 1L;


	    /**
	 *
	 */
    @TableId("id_")
    private Long id;
	
	    /**
     *用户id
     */
    @TableField("member_id")
    @Description("用户id")
    private Long memberId;
	
	    /**
     *电话号码
     */
    @TableField("phone_")
    @Description("电话号码")
    private String phone;
	
	    /**
     *百度推送通道ID
     */
    @TableField("channel_id")
    @Description("百度推送通道ID")
    private Long channelId;
	
	    /**
     *极光推送别名(保存手机唯一标示)
     */
    @TableField("alias")
    @Description("极光推送别名(保存手机唯一标示)")
    private String alias;
	
	    /**
     *3 for android, 4 for ios
     */
    @TableField("phone_type")
    @Description("3 for android, 4 for ios")
    private Integer phoneType;
	
	    /**
     *创建时间
     */
    @TableField("create_time")
    @Description("创建时间")
    private Date createTime;
	
// ID赋值
public PushChannelConfig(){
        this.id= returnIdLong();
        }
@Override
protected Serializable pkVal() {
        return this.id;
}
	/**
	 * 设置：
	 */
	public PushChannelConfig setId(Long id) {
		this.id = id;
		return this;
	}
	/**
	 * 获取：
	 */
	public Long getId() {
		return id;
	}
	/**
	 * 设置：用户id
	 */
	public PushChannelConfig setMemberId(Long memberId) {
		this.memberId = memberId;
		return this;
	}
	/**
	 * 获取：用户id
	 */
	public Long getMemberId() {
		return memberId;
	}
	/**
	 * 设置：电话号码
	 */
	public PushChannelConfig setPhone(String phone) {
		this.phone = phone;
		return this;
	}
	/**
	 * 获取：电话号码
	 */
	public String getPhone() {
		return phone;
	}
	/**
	 * 设置：百度推送通道ID
	 */
	public PushChannelConfig setChannelId(Long channelId) {
		this.channelId = channelId;
		return this;
	}
	/**
	 * 获取：百度推送通道ID
	 */
	public Long getChannelId() {
		return channelId;
	}
	/**
	 * 设置：极光推送别名(保存手机唯一标示)
	 */
	public PushChannelConfig setAlias(String alias) {
		this.alias = alias;
		return this;
	}
	/**
	 * 获取：极光推送别名(保存手机唯一标示)
	 */
	public String getAlias() {
		return alias;
	}
	/**
	 * 设置：3 for android, 4 for ios
	 */
	public PushChannelConfig setPhoneType(Integer phoneType) {
		this.phoneType = phoneType;
		return this;
	}
	/**
	 * 获取：3 for android, 4 for ios
	 */
	public Integer getPhoneType() {
		return phoneType;
	}
	/**
	 * 设置：创建时间
	 */
	public PushChannelConfig setCreateTime(Date createTime) {
		this.createTime = createTime;
		return this;
	}
	/**
	 * 获取：创建时间
	 */
	public Date getCreateTime() {
		return createTime;
	}


}
