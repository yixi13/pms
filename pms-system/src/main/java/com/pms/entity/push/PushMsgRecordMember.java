package com.pms.entity.push;
import java.io.Serializable;
import java.util.Date;

import com.pms.entity.BaseModel;
import com.pms.util.Description;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;


/**
 * 
 * 
 * @author zyl
 * @email 517770986@qq.com
 * @date 2017-12-01 11:12:07
 */
@TableName( "push_msg_record_member")
public class PushMsgRecordMember extends BaseModel<PushMsgRecordMember> {
private static final long serialVersionUID = 1L;


	    /**
	 *消息用户 中间表
	 */
    @TableId("id_")
    private Long id;
	
	    /**
     *用户id
     */
    @TableField("member_id")
    @Description("用户id")
    private Long memberId;
	
	    /**
     *消息记录id
     */
    @TableField("msg_id")
    @Description("消息记录id")
    private Long msgId;
	
	    /**
     *是否已读
     */
    @TableField("is_read")
    @Description("是否已读")
    private Integer isRead;
	
	    /**
     *用户阅读时间
     */
    @TableField("read_time")
    @Description("用户阅读时间")
    private Date readTime;
	
// ID赋值
public PushMsgRecordMember(){
        this.id= returnIdLong();
        }
@Override
protected Serializable pkVal() {
        return this.id;
}
	/**
	 * 设置：消息用户 中间表
	 */
	public PushMsgRecordMember setId(Long id) {
		this.id = id;
		return this;
	}
	/**
	 * 获取：消息用户 中间表
	 */
	public Long getId() {
		return id;
	}
	/**
	 * 设置：用户id
	 */
	public PushMsgRecordMember setMemberId(Long memberId) {
		this.memberId = memberId;
		return this;
	}
	/**
	 * 获取：用户id
	 */
	public Long getMemberId() {
		return memberId;
	}
	/**
	 * 设置：消息记录id
	 */
	public PushMsgRecordMember setMsgId(Long msgId) {
		this.msgId = msgId;
		return this;
	}
	/**
	 * 获取：消息记录id
	 */
	public Long getMsgId() {
		return msgId;
	}
	/**
	 * 设置：是否已读
	 */
	public PushMsgRecordMember setIsRead(Integer isRead) {
		this.isRead = isRead;
		return this;
	}
	/**
	 * 获取：是否已读
	 */
	public Integer getIsRead() {
		return isRead;
	}
	/**
	 * 设置：用户阅读时间
	 */
	public PushMsgRecordMember setReadTime(Date readTime) {
		this.readTime = readTime;
		return this;
	}
	/**
	 * 获取：用户阅读时间
	 */
	public Date getReadTime() {
		return readTime;
	}


}
