package com.pms.entity.push;
import java.io.Serializable;
import java.util.Date;

import com.pms.entity.BaseModel;
import com.pms.util.Description;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;


/**
 * 
 * 
 * @author zyl
 * @email 517770986@qq.com
 * @date 2017-12-01 11:12:07
 */
@TableName( "push_msg_record")
public class PushMsgRecord extends BaseModel<PushMsgRecord> {
private static final long serialVersionUID = 1L;


	    /**
	 *推送消息记录表
	 */
    @TableId("msg_id")
    private Long msgId;
	
	    /**
     *机构id
     */
    @TableField("agency_id")
    @Description("机构id")
    private Long agencyId;
	
	    /**
     *推送消息标题
     */
    @TableField("msg_title")
    @Description("推送消息标题")
    private String msgTitle;
	
	    /**
     *推送消息内容
     */
    @TableField("msg_info")
    @Description("推送消息内容")
    private String msgInfo;
	
	    /**
     *消息类别 1：投诉，2：保修，3订单，4系统，5社区公告，6物业交费
     */
    @TableField("msg_type")
    @Description("消息类别 1：投诉，2：保修，3订单，4系统，5社区公告，6物业交费")
    private Integer msgType;
	
	    /**
     *推送类型 1：单推，2：组推(tag),3:全推
     */
    @TableField("send_type")
    @Description("推送类型 1：单推，2：组推(tag),3:全推")
    private Integer sendType;
	
	    /**
     *创建时间
     */
    @TableField("create_time")
    @Description("创建时间")
    private Date createTime;
	
// ID赋值
public PushMsgRecord(){
        this.msgId= returnIdLong();
        }
@Override
protected Serializable pkVal() {
        return this.msgId;
}
	/**
	 * 设置：推送消息记录表
	 */
	public PushMsgRecord setMsgId(Long msgId) {
		this.msgId = msgId;
		return this;
	}
	/**
	 * 获取：推送消息记录表
	 */
	public Long getMsgId() {
		return msgId;
	}
	/**
	 * 设置：机构id
	 */
	public PushMsgRecord setAgencyId(Long agencyId) {
		this.agencyId = agencyId;
		return this;
	}
	/**
	 * 获取：机构id
	 */
	public Long getAgencyId() {
		return agencyId;
	}
	/**
	 * 设置：推送消息标题
	 */
	public PushMsgRecord setMsgTitle(String msgTitle) {
		this.msgTitle = msgTitle;
		return this;
	}
	/**
	 * 获取：推送消息标题
	 */
	public String getMsgTitle() {
		return msgTitle;
	}
	/**
	 * 设置：推送消息内容
	 */
	public PushMsgRecord setMsgInfo(String msgInfo) {
		this.msgInfo = msgInfo;
		return this;
	}
	/**
	 * 获取：推送消息内容
	 */
	public String getMsgInfo() {
		return msgInfo;
	}
	/**
	 * 设置：消息类别 1：投诉，2：保修，3订单，4系统，5社区公告，6物业交费
	 */
	public PushMsgRecord setMsgType(Integer msgType) {
		this.msgType = msgType;
		return this;
	}
	/**
	 * 获取：消息类别 1：投诉，2：保修，3订单，4系统，5社区公告，6物业交费
	 */
	public Integer getMsgType() {
		return msgType;
	}
	/**
	 * 设置：推送类型 1：单推，2：组推(tag),3:全推
	 */
	public PushMsgRecord setSendType(Integer sendType) {
		this.sendType = sendType;
		return this;
	}
	/**
	 * 获取：推送类型 1：单推，2：组推(tag),3:全推
	 */
	public Integer getSendType() {
		return sendType;
	}
	/**
	 * 设置：创建时间
	 */
	public PushMsgRecord setCreateTime(Date createTime) {
		this.createTime = createTime;
		return this;
	}
	/**
	 * 获取：创建时间
	 */
	public Date getCreateTime() {
		return createTime;
	}


}
