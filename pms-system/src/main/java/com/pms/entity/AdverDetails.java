package com.pms.entity;
import java.io.Serializable;
import java.util.Date;
import java.util.List;

import com.pms.util.Description;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;


/**
 * 
 * 
 * @author zyl
 * @email 517770986@qq.com
 * @date 2017-11-20 14:28:50
 */
@TableName( "adver_details")
public class AdverDetails extends BaseModel<AdverDetails> {
private static final long serialVersionUID = 1L;


	/**
	 *广告详情表
	 */
    @TableId("adver_id")
    private Long adverId;
	
	    /**
     *广告名称
     */
    @TableField("adver_name")
    @Description("广告名称")
    private String adverName;
	
	    /**
     *开始时间
     */
    @TableField("start_time")
    @Description("开始时间")
    private Date startTime;
	
	    /**
     *结束时间
     */
    @TableField("end_time")
    @Description("结束时间")
    private Date endTime;
	
	    /**
     *位置ID
     */
    @TableField("place_id")
    @Description("位置ID")
    private Long placeId;
	
	    /**
     *广告路径
     */
    @TableField("adver_url")
    @Description("广告路径")
    private String adverUrl;
	
	    /**
     *1）APP 备用字段
     */
    @TableField("client_type")
    @Description("1）APP 备用字段")
    private Integer clientType;

	    /**
     *
     */
    @TableField("click_count")
    @Description("")
    private Integer clickCount;
	
	    /**
     *1 商品 ，2 内连接  3外链接4.分类
     */
    @TableField("content_type")
    @Description("1 商品 ，2 内连接  3外链接4.分类")
    private Integer contentType;
	
	    /**
     *广告所服务对象名称
     */
    @TableField("adver_value")
    @Description("广告所服务对象名称")
    private String adverValue;
	
	    /**
     *广告所服务的对象id
     */
    @TableField("adver_key_id")
    @Description("广告所服务的对象id")
    private Long adverKeyId;
	
	    /**
     *机构ID
     */
    @TableField("agency_id")
    @Description("机构ID")
    private Long agencyId;

	@TableField(exist = false)
	private List<AdverAgency> adverAgency;

	public List<AdverAgency> getAdverAgency() {
		return adverAgency;
	}

	public void setAdverAgency(List<AdverAgency> adverAgency) {
		this.adverAgency = adverAgency;
	}

	// ID赋值
public AdverDetails(){
        this.adverId= returnIdLong();
        }
@Override
protected Serializable pkVal() {
        return this.adverId;
}
	/**
	 * 设置：广告详情表
	 */
	public AdverDetails setAdverId(Long adverId) {
		this.adverId = adverId;
		return this;
	}
	/**
	 * 获取：广告详情表
	 */
	public Long getAdverId() {
		return adverId;
	}
	/**
	 * 设置：广告名称
	 */
	public AdverDetails setAdverName(String adverName) {
		this.adverName = adverName;
		return this;
	}
	/**
	 * 获取：广告名称
	 */
	public String getAdverName() {
		return adverName;
	}
	/**
	 * 设置：开始时间
	 */
	public AdverDetails setStartTime(Date startTime) {
		this.startTime = startTime;
		return this;
	}
	/**
	 * 获取：开始时间
	 */
	public Date getStartTime() {
		return startTime;
	}
	/**
	 * 设置：结束时间
	 */
	public AdverDetails setEndTime(Date endTime) {
		this.endTime = endTime;
		return this;
	}
	/**
	 * 获取：结束时间
	 */
	public Date getEndTime() {
		return endTime;
	}
	/**
	 * 设置：位置ID
	 */
	public AdverDetails setPlaceId(Long placeId) {
		this.placeId = placeId;
		return this;
	}
	/**
	 * 获取：位置ID
	 */
	public Long getPlaceId() {
		return placeId;
	}
	/**
	 * 设置：广告路径
	 */
	public AdverDetails setAdverUrl(String adverUrl) {
		this.adverUrl = adverUrl;
		return this;
	}
	/**
	 * 获取：广告路径
	 */
	public String getAdverUrl() {
		return adverUrl;
	}
	/**
	 * 设置：1）APP 备用字段
	 */
	public AdverDetails setClientType(Integer clientType) {
		this.clientType = clientType;
		return this;
	}
	/**
	 * 获取：1）APP 备用字段
	 */
	public Integer getClientType() {
		return clientType;
	}

	/**
	 * 设置：
	 */
	public AdverDetails setClickCount(Integer clickCount) {
		this.clickCount = clickCount;
		return this;
	}
	/**
	 * 获取：
	 */
	public Integer getClickCount() {
		return clickCount;
	}
	/**
	 * 设置：1 商品 ，2 内连接  3外链接4.分类
	 */
	public AdverDetails setContentType(Integer contentType) {
		this.contentType = contentType;
		return this;
	}
	/**
	 * 获取：1 商品 ，2 内连接  3外链接4.分类
	 */
	public Integer getContentType() {
		return contentType;
	}
	/**
	 * 设置：广告所服务对象名称
	 */
	public AdverDetails setAdverValue(String adverValue) {
		this.adverValue = adverValue;
		return this;
	}
	/**
	 * 获取：广告所服务对象名称
	 */
	public String getAdverValue() {
		return adverValue;
	}
	/**
	 * 设置：广告所服务的对象id
	 */
	public AdverDetails setAdverKeyId(Long adverKeyId) {
		this.adverKeyId = adverKeyId;
		return this;
	}
	/**
	 * 获取：广告所服务的对象id
	 */
	public Long getAdverKeyId() {
		return adverKeyId;
	}
	/**
	 * 设置：机构ID
	 */
	public AdverDetails setAgencyId(Long agencyId) {
		this.agencyId = agencyId;
		return this;
	}
	/**
	 * 获取：机构ID
	 */
	public Long getAgencyId() {
		return agencyId;
	}


}
