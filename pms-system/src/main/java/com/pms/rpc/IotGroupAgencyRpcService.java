package com.pms.rpc;

import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;
import java.util.Map;

/**
 * Created by hm on 2018/9/6.
 */
@FeignClient("pms-iot")
@RequestMapping("api")
public interface IotGroupAgencyRpcService {

    @RequestMapping(value = "/del/delGroupAgencyByRoleId/{roleId}", method = RequestMethod.GET)
    public void delGroupAgencyByRoleId(@PathVariable("roleId") Long roleId);


    @RequestMapping(value = "/getGroupAgencyByRoleIdList/{groupId}",method = RequestMethod.POST, produces="application/json")
    public List<Map<String,Object>> getGroupAgencyByRoleIdList(@PathVariable("groupId")Long groupId);

}
