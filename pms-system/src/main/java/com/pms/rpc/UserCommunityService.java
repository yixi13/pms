package com.pms.rpc;
import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.pms.entity.UserCommunity;
import com.pms.service.IUserCommunityService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.Map;

/**
 * Created by hm on 2018/3/26.
 */
@Controller
@RequestMapping("api")
public class UserCommunityService {

    @Autowired
    IUserCommunityService userCommunityService;

    @RequestMapping(value = "/user/selectByuserCommunity/{userId}",method = RequestMethod.GET, produces="application/json")
    public @ResponseBody Map<String, Object> selectByuserCommunity(@PathVariable("userId")Long userId){
        return userCommunityService.selectMap(new EntityWrapper<UserCommunity>().eq("thedefault",1).and().eq("user_id",userId));
    }


    @RequestMapping(value = "/user/getUserCommunity/{userId}/{userCommunityId}",method = RequestMethod.POST, produces="application/json")
    public @ResponseBody Map<String, Object> getUserCommunity(@PathVariable("userId")Long userId,@PathVariable("userCommunityId")Long userCommunityId){
        return userCommunityService.selectMap(new EntityWrapper<UserCommunity>().eq("user_community_id",userCommunityId).and().eq("user_id",userId));
    }

}
