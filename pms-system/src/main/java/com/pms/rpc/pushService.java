package com.pms.rpc;
import com.pms.service.push.PushService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
/**
 * Created by hm on 2017/12/1.
 */
public class pushService {

    Logger logger = LoggerFactory.getLogger(getClass());
    @Autowired
    PushService pushService;

    @RequestMapping(value = "/pushByAlias",method = RequestMethod.GET, produces="application/json")
    public void JpushMsgByAlias(Long agencyId,Long memberId,
                                String MSG_TITLE,String MSG_INFO,Integer msgType){
         pushService.JpushMsgByAlias(agencyId,memberId,
                 MSG_TITLE, MSG_INFO, msgType);
    }

    @RequestMapping(value = "/pushByTag",method = RequestMethod.GET, produces="application/json")
    public void JpushMsgByTag(Long agencyId, String MSG_TITLE,String MSG_INFO,
                              Integer msgType,String[] tag){
        pushService.JpushMsgByTag( agencyId,  MSG_TITLE, MSG_INFO,
                 msgType,tag);
    }


}
