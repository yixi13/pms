package com.pms.rpc;

import com.pms.entity.LoginLog;
import com.pms.service.IBaseUserService;
import com.pms.service.ILoginLogService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Date;


/**
 * @author lk
 *
 **/
@RestController
@RequestMapping("api")
public class LoginLogService {
    Logger logger = LoggerFactory.getLogger(getClass());
    @Autowired
    IBaseUserService baseUserService;
    @Autowired
    ILoginLogService loginLogService;


    @RequestMapping(value = "/loginLog/addLoginLog",method = RequestMethod.POST, produces="application/json")
    public void addLoginLog(@RequestParam("userId")Long userId, @RequestParam("nickName")String nickName,@RequestParam("explain")String explain){
        LoginLog loginLog = new LoginLog();
        loginLog.setLoginInTime(new Date());
        loginLog.setUserId(userId);
        loginLog.setNickName(nickName);
        loginLog.setExplain(explain);
        loginLogService.insert(loginLog);
    }


}
