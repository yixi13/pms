package com.pms.rpc;

import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.util.List;
import java.util.Map;

/**
 * Created by hm on 2018/1/15.
 */

@FeignClient("pms-iot")
@RequestMapping("api")
public interface ICommunityService {

    @RequestMapping(value = "/selectMap/{communityId}", method = RequestMethod.GET)
    public Map<String, Object> selectMap(Long communityId);

    @RequestMapping(value = "/rpcCommunity/selectMapAll", method = RequestMethod.GET)
    List<Map<String, Object>> selectMapAll(Integer areaGb);


}


