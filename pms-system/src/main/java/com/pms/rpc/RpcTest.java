package com.pms.rpc;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * Created by ljb on 2017/11/6.
 */
@Controller
@RequestMapping("api")
public class RpcTest {

    @ResponseBody
    @RequestMapping(value = "/sys/rpcTest", method = RequestMethod.GET)
    public String rpcTest(){
        String a = "rpc/sys 服务端";
        return a;
    }
}
