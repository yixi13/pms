package com.pms.rpc;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.pms.entity.BaseUser;
import com.pms.entity.MemberInfo;
import com.pms.entity.SysMember;
import com.pms.entity.UserInfo;
import com.pms.service.ISysMemberService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by hm on 2017/10/25.
 */
@RestController
@RequestMapping("api")
public class MemberService {
    Logger logger = LoggerFactory.getLogger(getClass());
    @Autowired
    ISysMemberService sysMemberService;


    @RequestMapping(value = "/member/memberId/{memberId}",method = RequestMethod.GET, produces="application/json")
    public SysMember queryBymember(@PathVariable("memberId")Long memberId){
        SysMember sysMember = sysMemberService.selectById(memberId);
        return sysMember;
    }

    @RequestMapping(value = "/member/{phone}", method = RequestMethod.GET)
    public  SysMember queryByphone(@PathVariable("phone") String phone){
        return  sysMemberService.selectOne(new EntityWrapper<SysMember>().eq("phone_",phone));
    }
}
