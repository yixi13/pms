package com.pms.rpc;


import com.pms.entity.GateLog;
import com.pms.entity.LogInfo;
import com.pms.service.IGateLogService;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author zyl
 * @create 2017-07-06 16:30
 **/

@RestController
@RequestMapping("api")
public class LogService {
    @Autowired
    private IGateLogService gateLogService;
    @RequestMapping(value="/log/save",method = RequestMethod.POST)
    public void saveLog(@RequestBody LogInfo info){
        GateLog log = new GateLog();
        BeanUtils.copyProperties(info,log);
        gateLogService.insert (log);
    }
}
