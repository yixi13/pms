package com.pms.rpc;

import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;
import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;
import java.util.Map;

/**
 * Created by hm on 2018/8/16.
 */
@FeignClient("pms-pip")
@RequestMapping("api")
public interface IKyOrganizationRPCService {

    @RequestMapping(value = "/kyOrganization/add",method = RequestMethod.POST, produces="application/json")
    public void addLoginLog(@RequestParam("code")String code, @RequestParam("name")String name);


    @RequestMapping(value = "/deleteGroupOrganization/{groupId}",method = RequestMethod.POST, produces="application/json")
    public boolean deleteGroupOrganization(@PathVariable("groupId")Long groupId);

    @RequestMapping(value = "/getGroupOrganizationList/{groupId}",method = RequestMethod.POST, produces="application/json")
    public List<Map<String,Object>> getGroupOrganizationList(@PathVariable("groupId")Long groupId);


    @RequestMapping(value = "/deleteSystemDataByCode/{code}",method = RequestMethod.POST, produces="application/json")
    public void deleteSystemDataByCode(@PathVariable("code")String code);



    @RequestMapping(value = "/kyOrganization/updateByCode",method = RequestMethod.POST, produces="application/json")
    public void updateByCode(@RequestParam("code")String code, @RequestParam("name")String name);

}
