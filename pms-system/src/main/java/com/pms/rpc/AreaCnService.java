package com.pms.rpc;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.pms.entity.AreaCn;
import com.pms.entity.SysMember;
import com.pms.exception.R;
import com.pms.service.IAreaCnService;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by hm on 2017/10/25.
 */
@RestController
@RequestMapping("api")
public class AreaCnService {
    @Autowired
    IAreaCnService areaCnService;

    @RequestMapping(value = "/areaCn/county/{countyGb}")
    public Map<String,Object> getcounty(@PathVariable("countyGb")Integer countyGb){
        if (countyGb==null){throw  new RuntimeException("编码为空");}
        return  areaCnService.selectMap(new EntityWrapper<AreaCn>().eq("county_gb",countyGb));
    }


    }
