package com.pms.rpc;
import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.Map;

/**
 * Created by hm on 2018/1/15.
 */

@FeignClient("pms-iot")
@RequestMapping("api")
public interface ICompanyService {


    @RequestMapping(value = "/selectMap/{companyId}", method = RequestMethod.GET)
    public Map<String, Object> selectMap(Long companyId);


    @RequestMapping(value = "/company/cleanUpCompanyData", method = RequestMethod.POST, produces="application/json")
    public Integer cleanUpCompanyData(@RequestParam("companyCode") String companyCode);
}
