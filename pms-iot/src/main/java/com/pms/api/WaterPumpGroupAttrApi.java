package com.pms.api;
import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.mapper.Wrapper;
import com.pms.config.DynamicDataSourceHolder;
import com.pms.controller.BaseController;
import com.pms.entity.Community;
import com.pms.entity.WaterPumpGroup;
import com.pms.entity.WaterPumpGroupAttr;
import com.pms.entity.WaterPumpGroupFault;
import com.pms.exception.R;
import com.pms.rpc.UserService;
import com.pms.service.*;
import com.pms.util.DateUtil;
import com.pms.utils.WoGiaFiledConstant;
import com.pms.validator.Assert;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.text.SimpleDateFormat;
import java.util.*;


/**
 * 设备组属性数据接口
 */
@RestController
@Api(value = "设备组属性Api接口", description = "设备组属性Api接口")
@RequestMapping(value = "waterPumpGroupAttr", method = RequestMethod.POST)
public class WaterPumpGroupAttrApi extends BaseController {
    @Autowired
    IWaterPumpGroupAttrService waterPumpGroupAttrService;
    @Autowired
    UserService userService;
    @Autowired
    IWaterPumpGroupFaultService waterPumpGroupFaultService;
    @Autowired
    IWaterPumpGroupService waterPumpGroupService;
    @Autowired
    ICommunityService communityService;
    @Autowired
    IWoGiaWpAttrService woGiaWpAttrService;

    @ApiOperation(value = "根据水泵组id查询设备组对应的数据")
    @RequestMapping(value = "/queryByDeviceName", method = RequestMethod.POST)
    @ApiImplicitParams({
            @ApiImplicitParam(name = "deviceName", value = "水泵组名称", required = true, dataType = "String", paramType = "form")
    })
    public R queryWaterPumpById(String deviceName) {
        Assert.parameterIsBlank(deviceName,"水泵组名称不能为空");
        if(deviceName.startsWith("WOGIA")){//  如果是沃佳开头 ，资源来源于沃佳
            String[] deviceNameArr = deviceName.split("\\|");
            String tablename = deviceNameArr[0].trim();// 表名称
            tablename=tablename.toLowerCase();
            logger.info("=============切换数据源为:" +"woGiaDatasource"+"==============");
            int tablenameIsExistCount = woGiaWpAttrService.selectTableIsExist(tablename);
            if(tablenameIsExistCount<1){// 表不存在
                return R.ok().put("waterPumpGroupAttr",woGiaWpAttrService.defaultQueryByDeviceNameReturnMap(deviceName));
            }else{
                Map<String,String> sqlParamMap = new HashMap<String,String>();
                sqlParamMap.put("tablename",tablename);
                sqlParamMap.put("whereStr","order by Time desc limit 0,1");//根据上报时间降序,查询最新一条数据
                List<Map<String,Object>> woGiaWpAttrList = woGiaWpAttrService.selectAttrByTablenameAndColumn(sqlParamMap);
                if(woGiaWpAttrList==null||woGiaWpAttrList.isEmpty()){
                    return R.ok().put("waterPumpGroupAttr",woGiaWpAttrService.defaultQueryByDeviceNameReturnMap(deviceName));
                }
                String columnPrifix =null;
                try {
                    columnPrifix = deviceNameArr[2].trim();// 列名前缀
                }catch (ArrayIndexOutOfBoundsException ex){
                    return R.error(400,"水泵组名称格式错误");
                }
                Map<String,Object> waterPumpGroupAttrMap = woGiaWpAttrService.convertWoGiaData(woGiaWpAttrList.get(0),columnPrifix);
                waterPumpGroupAttrMap.put("deviceName",deviceName);//设备组名称
                return R.ok().put("waterPumpGroupAttr",waterPumpGroupAttrMap);
            }
        }else{// 其余默认从本地查询
            Wrapper<WaterPumpGroupAttr> wp = new EntityWrapper<WaterPumpGroupAttr>();
            wp.eq("deviceName",deviceName);
            wp.orderBy("reportTimeFormat",false);
            wp.isNotNull("SBZT");
            WaterPumpGroupAttr waterPumpGroupAttr = waterPumpGroupAttrService.selectOneRecord(wp);
            return R.ok().put("waterPumpGroupAttr",waterPumpGroupAttr);
        }
    }


    @ApiOperation(value = "根据水泵组名称和开始及结束时间查询对应的属性历史数据")
    @RequestMapping(value = "/queryWaterPumpGroupAttrListByDeviceName", method = RequestMethod.POST)
    @ApiImplicitParams({
            @ApiImplicitParam(name = "deviceName", value = "水泵组名称", required = true, dataType = "String", paramType = "form"),
            @ApiImplicitParam(name = "startTime", value = "开始时间", required = true, dataType = "long", paramType = "form"),
            @ApiImplicitParam(name = "endTime", value = "结束时间", required = true, dataType = "long", paramType = "form")
    })
    public R queryWaterPumpGroupAttrListByDeviceName(String deviceName,long startTime,long endTime) {
        parameterIsNull(startTime, "开始时间不能为空");
        parameterIsNull(endTime, "结束时间不能为空");
        parameterIsNull(deviceName, "设备组名称不能为空");
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String startTimeStr = sdf.format(new Date(startTime*1000L));
        String endTimeStr = sdf.format(new Date(endTime*1000L));
//        Wrapper<WaterPumpGroupAttr> wp = new EntityWrapper<WaterPumpGroupAttr>();
//        wp.eq("deviceName",deviceName);
//        wp.between("reportTimeFormat",startTimeStr,endTimeStr);
//        wp.orderBy("createTime",false);
//        wp.last("limit 100");
//        List<WaterPumpGroupAttr> waterPumpGroupAttrList = waterPumpGroupAttrService.selectList(wp);
        List<Map<String,Object>> list = waterPumpGroupAttrService.selectByDeviceNameAndReportTimeFormat(deviceName,startTimeStr,endTimeStr);
        return R.ok().put("waterPumpGroupAttrList",list);
    }

    @ApiOperation(value = "根据小区id和日期查询对应的故障历史数据")
    @RequestMapping(value = "/queryErrorCodeByDate", method = RequestMethod.POST)
    @ApiImplicitParams({
            @ApiImplicitParam(name = "date", value = "日期", required = true, dataType = "long", paramType = "form"),
//            @ApiImplicitParam(name = "deviceName", value = "设备名称", required = true, dataType = "String", paramType = "form"),
            @ApiImplicitParam(name = "communityId", value = "小区id", required = true, dataType = "long", paramType = "form")
    })
    public R queryErrorCodeByDate(long communityId,long date) {
        parameterIsNull(date, "日期不能为空");
        parameterIsNull(communityId, "小区id不能为空");
        Community community = communityService.selectById(communityId);
        parameterIsNull(community, "没有此小区");
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        String dateStr = sdf.format(new Date(date*1000L));
//        Wrapper<WaterPumpGroupFault> wp = new EntityWrapper<WaterPumpGroupFault>();
//        wp.like("reportTimeFormat",dateStr);
//        wp.eq("deviceName",deviceName);
//        List<WaterPumpGroupFault> list = waterPumpGroupFaultService.selectList(wp);
        List<WaterPumpGroupFault> list = waterPumpGroupFaultService.selectByCommunityIdAndDate(communityId,dateStr);
        return R.ok().put("WaterPumpGroupFaultList",list);
    }

    @ApiOperation(value = "根据水泵组名称和日期查询对应的故障历史数据")
    @RequestMapping(value = "/queryErrorCodeByDeviceNameAndDate", method = RequestMethod.POST)
    @ApiImplicitParams({
            @ApiImplicitParam(name = "deviceName", value = "设备名称", required = true, dataType = "String", paramType = "form"),
            @ApiImplicitParam(name = "date", value = "日期", required = false, dataType = "long", paramType = "form")
    })
    public R queryErrorCodeByDate(String deviceName,Long date) {
        parameterIsEmpty(deviceName,"设备名称不能为空");
//        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        String dateStr;
        if(date!=null){
            dateStr = DateUtil.format(new Date(date*1000L),"yyyy-MM-dd");
        }else{
            dateStr = DateUtil.format(new Date(),"yyyy-MM-dd");
        }
        if(deviceName.startsWith("WOGIA")){
            String[] deviceNameArr = deviceName.split("\\|");
            String tablename = deviceNameArr[0].trim();// 表名称
            tablename=tablename.toLowerCase();
            int tablenameIsExistCount = woGiaWpAttrService.selectTableIsExist(tablename);
            if(tablenameIsExistCount<1){// 表不存在
                return R.ok().put("waterPumpGroupAttrList", new ArrayList<Map<String, Object>>());
            }
            String columnPrifix=null;
            try {
                columnPrifix = deviceNameArr[2].trim();// 列名前缀
            }catch (ArrayIndexOutOfBoundsException ex){
                return R.error(400,"水泵组名称格式错误");
            }
            Map<String,String> sqlParamMap = new HashMap<String,String>();
            sqlParamMap.put("tablename",tablename);
            sqlParamMap.put("columnPrifix",columnPrifix+"%");
            List<String> columnList=woGiaWpAttrService.selectAllColumnByTableName(sqlParamMap);
            List<String>  faultColumnList = new ArrayList<String>();
            if(columnList!=null&&!columnList.isEmpty()){
                if(columnList.contains(columnPrifix+WoGiaFiledConstant.OUTLET_PRESSURE_SUPER_HIGH)){//超高压力
                    faultColumnList.add(columnPrifix+WoGiaFiledConstant.OUTLET_PRESSURE_SUPER_HIGH);
                }
                if(columnList.contains(columnPrifix+WoGiaFiledConstant.FREQUENCY_STATUS)){//变频器故障
                    faultColumnList.add(columnPrifix+WoGiaFiledConstant.FREQUENCY_STATUS);
                }
                if(columnList.contains(columnPrifix+WoGiaFiledConstant.INLET_PRESSURE_LOW)){//稳流罐故障
                    faultColumnList.add(columnPrifix+WoGiaFiledConstant.INLET_PRESSURE_LOW);
                }
            }
            if(!faultColumnList.isEmpty()){// 判断是否拥有报警字段
                StringBuffer columnStr = new StringBuffer("");
                StringBuffer faultColumnStr =new StringBuffer("( ");
                faultColumnStr.append(faultColumnList.get(0)+"= " + 1.0F);
                columnStr.append(faultColumnList.get(0));
                for(int z=1,len=faultColumnList.size();z<len;z++){
                    faultColumnStr.append(" or "+ faultColumnList.get(z)+"= " + 1.0F);
                    columnStr.append(","+faultColumnList.get(z));
                }
                faultColumnStr.append(")");
                sqlParamMap.put("columnStr",columnStr.toString());
                sqlParamMap.put("whereStr","where Time like '"+dateStr+"%'"+" and "+faultColumnStr.toString());
                List<Map<String,Object>> woGiaWpAttrList = woGiaWpAttrService.selectAttrByTablenameAndColumn(sqlParamMap);
                if(woGiaWpAttrList!=null&&!woGiaWpAttrList.isEmpty()){
                    List<Map<String,Object>> returnList = new ArrayList<Map<String,Object>>();
                    for(int z=0,length=woGiaWpAttrList.size();z<length;z++){
                        Map<String,Object> MAP= woGiaWpAttrService.convertWoGiaData(woGiaWpAttrList.get(z),columnPrifix);
                        MAP.put("isSocketPush",0);
                        MAP.put("isSmsPush",0);
                        returnList.add(MAP);
                    }
                    return R.ok().put("WaterPumpGroupFaultList",returnList);
                }
            }// 判断是否拥有报警字段
            return R.ok().put("WaterPumpGroupFaultList",new ArrayList<>());
        }
        Wrapper<WaterPumpGroupFault> wp = new EntityWrapper();
        wp.eq("deviceName",deviceName);
        wp.like("reportTimeFormat",dateStr);
        List<WaterPumpGroupFault> list = waterPumpGroupFaultService.selectList(wp);
        return R.ok().put("WaterPumpGroupFaultList",list);
    }


    @ApiOperation(value = "根据水泵组名称和开始及结束时间查询对应的属性历史数据")
    @RequestMapping(value = "/queryWaterPumpGroupAttrListByDeviceNameAndReportTime", method = RequestMethod.POST)
    @ApiImplicitParams({
            @ApiImplicitParam(name = "deviceName", value = "水泵组名称", required = true, dataType = "String", paramType = "form"),
            @ApiImplicitParam(name = "startTime", value = "开始时间,默认取当前时间往前2小时,unix时间戳", required = false, dataType = "long", paramType = "form"),
            @ApiImplicitParam(name = "endTime", value = "结束时间,默认取当前时间,unix时间戳", required = false, dataType = "long", paramType = "form")
    })
    public R queryWaterPumpGroupAttrListByDeviceNameAndReportTime(String deviceName,Long startTime,Long endTime) {
        parameterIsNull(deviceName, "设备组名称不能为空");
        if(endTime==null){
            endTime = System.currentTimeMillis()/1000;
        }
        if(startTime==null){
            startTime = DateUtil.addDate(new Date(),11,-2).getTime()/1000;
        }
        if(startTime>=endTime){
            return R.error(400,"结束时间需大于起始时间");
        }
         List<Map<String,Object>> list = waterPumpGroupAttrService.selectByDeviceNameAndReportTime(deviceName,startTime,endTime);
        return R.ok().put("waterPumpGroupAttrList",list);
    }
    @ApiOperation(value = "根据水泵组名称和开始及结束时间查询对应的属性历史数据")
    @RequestMapping(value = "/queryWaterPumpGroupAttrListByDeviceNameAndReportTime2", method = RequestMethod.POST)
    @ApiImplicitParams({
            @ApiImplicitParam(name = "deviceName", value = "水泵组名称", required = true, dataType = "String", paramType = "form"),
            @ApiImplicitParam(name = "startTime", value = "开始时间,yyyy-MM-dd HH:mm:ss格式,默认当前时间往前2小时", required = false, dataType = "String", paramType = "form"),
            @ApiImplicitParam(name = "endTime", value = "结束时间,yyyy-MM-dd HH:mm:ss格式,默认当前时间", required = false, dataType = "String", paramType = "form"),
            @ApiImplicitParam(name = "returnType", value = "返回数据结构(默认返回，2-根据展示项区分)", required = false, dataType = "int", paramType = "form"),
            @ApiImplicitParam(name = "readType", value = "是否查询不断变化的值(默认查询,2-不查询)", required = false, dataType = "int", paramType = "form"),
    })
    public R queryWaterPumpGroupAttrListByDeviceNameAndReportTime2(Integer returnType,Integer readType,String deviceName,String startTime,String endTime) {
        Long startTimeLong =null; Long endTimeLong =null;
        Date startTimeDate = null; Date endTimeDate = null;
        parameterIsNull(deviceName, "设备组名称不能为空");
        if(StringUtils.isBlank(endTime)){
            endTimeDate = new Date();
//            endTime = System.currentTimeMillis()/1000;
        }else{
            endTimeDate = DateUtil.stringToDate(endTime);
            if(endTimeDate==null){
                return R.error(400,"结束时间格式错误");
            }
        }
        if(startTime==null){
            startTimeDate = DateUtil.addDate(endTimeDate,11,-2);
        }else{
            startTimeDate = DateUtil.stringToDate(startTime);
            if(startTimeDate==null){
                return R.error(400,"开始时间格式错误");
            }
        }
        startTimeLong = startTimeDate.getTime()/1000;
        endTimeLong = endTimeDate.getTime()/1000;
        if(startTimeLong >= endTimeLong){
            return R.error(400,"结束时间需大于起始时间");
        }
        List<Map<String,Object>> list = waterPumpGroupAttrService.selectByDeviceNameAndReportTime(deviceName,startTimeLong,endTimeLong);
        if(returnType!=null&&returnType==2){
            Map<String,Object> waterPumpGroupAttrMap = new HashMap<String,Object>();
            waterPumpGroupAttrMap.put("dataType",200);// 表示为请求接口获取的数据
            int arrLength = list.size();
            Object[]  reportTimeArr = new Object[arrLength];//上报时间
            Object[]  pressureFeedbackArr = new Object[arrLength]; //反馈压力-出水压力
            Object[] waterFrequencyArr = new Object[arrLength]; // 水泵频率
            if(arrLength>0){
                for(int x=0;x<arrLength;x++){
                    reportTimeArr[x] = list.get(x).get("reportTimeFormat");
                    pressureFeedbackArr[x] =list.get(x).get("pressureFeedback");
                    waterFrequencyArr[x] =list.get(x).get("waterFrequency");
                }
            }
            waterPumpGroupAttrMap.put("reportTimeArr",reportTimeArr);
            waterPumpGroupAttrMap.put("pressureFeedbackArr",pressureFeedbackArr);
            waterPumpGroupAttrMap.put("waterFrequencyArr",waterFrequencyArr);
            return R.ok().put("waterPumpGroupAttrList",waterPumpGroupAttrMap);
        }

        return R.ok().put("waterPumpGroupAttrList",list);
    }


    @ApiOperation(value = "根据水泵组名称和开始及结束时间查询对应的属性历史数据-查询2s数据变化的")
    @RequestMapping(value = "/readWpAttrRecordByNameAndTime3", method = RequestMethod.POST)
    @ApiImplicitParams({
            @ApiImplicitParam(name = "deviceName", value = "水泵组名称", required = true, dataType = "String", paramType = "form"),
            @ApiImplicitParam(name = "startTime", value = "开始时间,yyyy-MM-dd HH:mm:ss格式,默认当前时间往前2小时", required = false, dataType = "String", paramType = "form"),
            @ApiImplicitParam(name = "endTime", value = "结束时间,yyyy-MM-dd HH:mm:ss格式,默认当前时间", required = false, dataType = "String", paramType = "form"),
            @ApiImplicitParam(name = "returnType", value = "返回数据结构(默认返回，2-根据展示项区分)", required = false, dataType = "int", paramType = "form"),
            @ApiImplicitParam(name = "readType", value = "是否查询不断变化的值(默认查询,2-不查询)", required = false, dataType = "int", paramType = "form"),
    })
    public R readWpAttrRecordByNameAndTime3(Integer returnType,Integer readType,String deviceName,String startTime,String endTime) {
        Long startTimeLong =null; Long endTimeLong =null;
        Date startTimeDate = null; Date endTimeDate = null;
        parameterIsNull(deviceName, "设备组名称不能为空");
        if(StringUtils.isBlank(endTime)){
            endTimeDate = new Date();
//            endTime = System.currentTimeMillis()/1000;
        }else{
            endTimeDate = DateUtil.stringToDate(endTime);
            if(endTimeDate==null){
                return R.error(400,"结束时间格式错误");
            }
        }
        if(startTime==null){
            startTimeDate = DateUtil.addDate(endTimeDate,11,-2);
        }else{
            startTimeDate = DateUtil.stringToDate(startTime);
            if(startTimeDate==null){
                return R.error(400,"开始时间格式错误");
            }
        }
        startTimeLong = startTimeDate.getTime()/1000;
        endTimeLong = endTimeDate.getTime()/1000;
        if(startTimeLong >= endTimeLong){
            return R.error(400,"结束时间需大于起始时间");
        }
        List<Map<String,Object>> list = null;
        if(readType!=null&&readType==2){//不查询变化的值
            list = waterPumpGroupAttrService.selectByDeviceNameAndReportTime(deviceName,startTimeLong,endTimeLong);
        }else{// 查询不断变化的值
            list = waterPumpGroupAttrService.selectAttrRecordByFuction(deviceName,startTimeLong,endTimeLong);
        }
        if(returnType!=null&&returnType==2){
            return R.ok().put("waterPumpGroupAttrList",formatWpRecordData(list));
        }
        return R.ok().put("waterPumpGroupAttrList",list);
    }



    @ApiOperation(value = "查询固定时间采集的水泵组属性记录")
    @RequestMapping(value = "/readWpGroupRecordByNameAndTime", method = RequestMethod.POST)
    @ApiImplicitParams({
            @ApiImplicitParam(name = "deviceName", value = "水泵组名称", required = true, dataType = "String", paramType = "form"),
            @ApiImplicitParam(name = "startTime", value = "开始时间,yyyy-MM-dd HH:mm:ss格式,默认当前时间往前2小时", required = false, dataType = "String", paramType = "form"),
            @ApiImplicitParam(name = "endTime", value = "结束时间,yyyy-MM-dd HH:mm:ss格式,默认当前时间", required = false, dataType = "String", paramType = "form"),
            @ApiImplicitParam(name = "returnType", value = "返回数据结构(默认返回，2-根据展示项区分)", required = false, dataType = "int", paramType = "form"),
   })
    public R readWpGroupRecordByNameAndTime(Integer returnType,String deviceName,String startTime,String endTime) {

        Long startTimeLong =null; Long endTimeLong =null;
        Date startTimeDate = null; Date endTimeDate = null;
        parameterIsNull(deviceName, "设备组名称不能为空");
        if(StringUtils.isBlank(endTime)){
            endTimeDate = new Date();
//            endTime = System.currentTimeMillis()/1000;
            endTime = DateUtil.format(endTimeDate);
        }else{
            endTimeDate = DateUtil.stringToDate(endTime);
            if(endTimeDate==null){
                return R.error(400,"结束时间格式错误");
            }
        }
        if(StringUtils.isBlank(startTime)){
            startTimeDate = DateUtil.addDate(endTimeDate,11,-2);
            startTime = DateUtil.format(startTimeDate);
        }else{
            startTimeDate = DateUtil.stringToDate(startTime);
            if(startTimeDate==null){
                return R.error(400,"开始时间格式错误");
            }
        }
        startTimeLong = startTimeDate.getTime()/1000;
        endTimeLong = endTimeDate.getTime()/1000;
        if(startTimeLong >= endTimeLong){
            return R.error(400,"结束时间需大于起始时间");
        }
        if(deviceName.startsWith("WOGIA")){
            String[] deviceNameArr = deviceName.split("\\|");
            String tablename = deviceNameArr[0].trim();// 表名称
            tablename=tablename.toLowerCase();
            int tablenameIsExistCount = woGiaWpAttrService.selectTableIsExist(tablename);
            if(tablenameIsExistCount<1){// 表不存在
                if(returnType!=null&&returnType==2) {
                    return R.ok().put("waterPumpGroupAttrList", formatWpRecordData(new ArrayList<Map<String, Object>>()));
                }
                return R.ok().put("waterPumpGroupAttrList", new ArrayList<Map<String, Object>>());
            }
            Map<String,String> sqlParamMap = new HashMap<String,String>();
            sqlParamMap.put("tablename",tablename);
            sqlParamMap.put("whereStr","where Time >= '"+startTime+"' and Time < '"+endTime +"' order by Time asc");//根据上报时间降序,查询最新一条数据
            List<Map<String,Object>> woGiaWpAttrList = woGiaWpAttrService.selectAttrByTablenameAndColumn(sqlParamMap);
            if(woGiaWpAttrList==null||woGiaWpAttrList.isEmpty()){
                if(returnType!=null&&returnType==2) {
                    return R.ok().put("waterPumpGroupAttrList", formatWpRecordData(new ArrayList<Map<String, Object>>()));
                }
                return R.ok().put("waterPumpGroupAttrList", new ArrayList<Map<String, Object>>());
            }
            String columnPrifix=null;
            try {
                columnPrifix = deviceNameArr[2].trim();// 列名前缀
            }catch (ArrayIndexOutOfBoundsException ex){
                return R.error(400,"水泵组名称格式错误");
            }
            List<Map<String,Object>> wpAttrMap = new ArrayList<Map<String,Object>>();
            for(int z=0,length=woGiaWpAttrList.size();z<length;z++){
                wpAttrMap.add(woGiaWpAttrService.convertWoGiaData(woGiaWpAttrList.get(z),columnPrifix));
            }
            if(returnType!=null&&returnType==2) {
                return R.ok().put("waterPumpGroupAttrList", formatWpRecordData(wpAttrMap));
            }
            return R.ok().put("waterPumpGroupAttrList",wpAttrMap);
        }
        List<Map<String,Object>> list = waterPumpGroupAttrService.readWpGroupRecordByNameAndTime(deviceName,startTimeLong,endTimeLong);
        if(returnType!=null&&returnType==2){
            return R.ok().put("waterPumpGroupAttrList",formatWpRecordData(list));
        }
        return R.ok().put("waterPumpGroupAttrList",list);
    }


    public Map<String,Object> formatWpRecordData( List<Map<String,Object>> list){
        Map<String,Object> waterPumpGroupAttrMap = new HashMap<String,Object>();
        waterPumpGroupAttrMap.put("dataType",200);// 表示为请求接口获取的数据
        waterPumpGroupAttrMap.put("dataDesc","出水压力折线图展示数据为:(pressureFeedbackArr-出水压力,SZGWYLArr-市政管网压力,settingPressureArr-设定压力,ultraTightlyAlarmArr-超压报警压力。单位:Mpa),水泵频率图展示数据为:(waterFrequencyArr-运行频率,XXPLArr-下限频率。单位:Hz)");// 表示为请求接口获取的数据
        int arrLength = list.size();
        Object[]  reportTimeArr = new Object[arrLength];//上报时间
        Object[]  pressureFeedbackArr = new Object[arrLength]; //反馈压力-出水压力
        Object[]  SZGWYLArr = new Object[arrLength]; //市政管网压力-进水水压力
        Object[]  settingPressureArr = new Object[arrLength]; //额定压力
        Object[]  ultraTightlyAlarmArr = new Object[arrLength]; //超压报警压力
        Object[] XXPLArr = new Object[arrLength]; // 下限频率
        Object[] waterFrequencyArr = new Object[arrLength]; // 水泵频率
        if(arrLength>0){
            for(int x=0;x<arrLength;x++){
                reportTimeArr[x] = list.get(x).get("reportTimeFormat").toString();
                pressureFeedbackArr[x] =list.get(x).get("pressureFeedback");
                SZGWYLArr[x] = list.get(x).get("SZGWYL");
                settingPressureArr[x] = list.get(x).get("settingPressure");
                ultraTightlyAlarmArr[x] = list.get(x).get("ultraTightlyAlarm");
                XXPLArr[x] = list.get(x).get("XXPL");
                waterFrequencyArr[x] =list.get(x).get("waterFrequency");
            }
        }
        waterPumpGroupAttrMap.put("reportTimeArr",reportTimeArr);
        waterPumpGroupAttrMap.put("pressureFeedbackArr",pressureFeedbackArr);
        waterPumpGroupAttrMap.put("SZGWYLArr",SZGWYLArr);
        waterPumpGroupAttrMap.put("settingPressureArr",settingPressureArr);
        waterPumpGroupAttrMap.put("ultraTightlyAlarmArr",ultraTightlyAlarmArr);
        waterPumpGroupAttrMap.put("XXPLArr",XXPLArr);
        waterPumpGroupAttrMap.put("waterFrequencyArr",waterFrequencyArr);
        return waterPumpGroupAttrMap;
    }







}
