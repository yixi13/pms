package com.pms.api;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.mapper.Wrapper;
import com.pms.entity.WaterPumpGroupAttr;
import com.pms.entity.WaterPumpGroupFault;
import com.pms.exception.R;
import com.pms.service.IWaterPumpGroupAttrService;
import com.pms.service.IWaterPumpGroupFaultService;
import com.pms.util.DateUtil;
import com.pms.utils.FiledMarkUtil;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by Administrator on 2018/3/19.
 */
@RestController
@RequestMapping(value = "/waterPumpGroupAttr")
@Api(value="App水泵属性接口",description = "App水泵属性接口")
public class ApiWaterPumpGroupAttrController {
    @Autowired
    IWaterPumpGroupFaultService waterPumpGroupFaultService;
    @Autowired
    IWaterPumpGroupAttrService waterPumpGroupAttrService;
    @ApiOperation(value = "查询设备故障记录")
    @RequestMapping(value = "/readFaultWaterPumpGroupAttrList",method = RequestMethod.POST)
    @ApiImplicitParams({
            @ApiImplicitParam(name="deviceName",value="设备名称",required = true,dataType = "string",paramType="form"),
            @ApiImplicitParam(name="startTime",value="起始时间",required = false,dataType = "string",paramType="form"),
            @ApiImplicitParam(name="endTime",value="截止时间",required = false,dataType = "string",paramType="form"),
    })
    public R readFaultWaterPumpGroupAttrList(String deviceName,String startTime,String endTime){
        if(StringUtils.isBlank(deviceName)){
            return R.error(400,"设备名称不能为空");
        }
        Date startDate = null;
        Date endDate = null;
        startDate= DateUtil.stringToDate(startTime);
        endDate = DateUtil.stringToDate(endTime);
        Wrapper<WaterPumpGroupFault> wp= new EntityWrapper<WaterPumpGroupFault>();
        wp.eq("deviceName",deviceName);
        if(startDate!=null){
            wp.ge("reportTimeFormat",startDate);// >=
        }
        if(endDate!=null){
            wp.le("reportTimeFormat",endDate);// <=
        }

        List<WaterPumpGroupFault> faultList = waterPumpGroupFaultService.selectList(wp);
        return R.ok().put("data",faultList);
    }
    @ApiOperation(value = "查询设备属性")
    @RequestMapping(value = "/readWaterPumpGroupAttrList",method = RequestMethod.POST)
    @ApiImplicitParams({
            @ApiImplicitParam(name="deviceName",value="设备名称",required = true,dataType = "string",paramType="form"),
            @ApiImplicitParam(name="startTime",value="起始时间",required = false,dataType = "string",paramType="form"),
            @ApiImplicitParam(name="endTime",value="截止时间",required = false,dataType = "string",paramType="form"),
    })
    public R readWaterPumpGroupAttrList(String deviceName,String startTime,String endTime){
        if(StringUtils.isBlank(deviceName)){
            return R.error(400,"设备名称不能为空");
        }
        Date startDate = null;
        Date endDate = null;
        startDate= DateUtil.stringToDate(startTime);
        endDate = DateUtil.stringToDate(endTime);
        Wrapper<WaterPumpGroupAttr> wp= new EntityWrapper<WaterPumpGroupAttr>();
        wp.eq("deviceName",deviceName);
        if(startDate!=null){
            wp.ge("reportTimeFormat",startDate);// >=
        }
        if(endDate!=null){
            wp.le("reportTimeFormat",endDate);// <=
        }
        List<WaterPumpGroupAttr> faultList = waterPumpGroupAttrService.selectList(wp);
        return R.ok().put("data",faultList);
    }
    @ApiOperation(value = "查询设备属性详情")
    @GetMapping(value = "/readWaterPumpGroupAttrDetail/{waterPumpGroupAttrId}")
    @ApiImplicitParams({
            @ApiImplicitParam(name="waterPumpGroupAttrId",value="id值",required = true,dataType = "long",paramType="form"),
     })
    public R readWaterPumpGroupAttrDetail(@PathVariable Long waterPumpGroupAttrId){
        if(waterPumpGroupAttrId==null){
            return R.error(400,"id值不能为空");
        }
        WaterPumpGroupAttr faultList = waterPumpGroupAttrService.selectById(waterPumpGroupAttrId);
        return R.ok().put("data",faultList);
    }
    @ApiOperation(value = "查询设备属性描述")
    @GetMapping(value = "/readWaterPumpGroupAttrDesc")
    @ApiImplicitParams({})
    public R readWaterPumpGroupAttrDesc(){
        return R.ok().put("data", FiledMarkUtil.getFiledMarkValue(WaterPumpGroupAttr.class));
    }
}
