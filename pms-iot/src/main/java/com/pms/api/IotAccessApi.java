package com.pms.api;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONException;
import com.alibaba.fastjson.JSONObject;
import com.pms.controller.BaseController;
import com.pms.exception.R;
import com.pms.exception.RRException;
import com.pms.service.tsdb.IIotDeviceService;
import com.pms.validator.Assert;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;
import java.util.StringTokenizer;

/**
 * 聚合函数类型{1-求平均数，2-总记录数，3-求总和,4-求相邻值的差,5/6-求最大/小值,7/8-求倍数/除数,9/10-求第一个/最后一个值}
 */
@RestController
@Api(value = "百度物接入Api接口", description = "百度物接入Api接口")
@RequestMapping(value = "iotAccess", method = RequestMethod.POST)
public class IotAccessApi extends BaseController {
    @Autowired
    IIotDeviceService iotDeviceService;

    @ApiOperation(value = "查询实列列表")
    @RequestMapping(value = "/readEndpointList", method = RequestMethod.POST)
    @ApiImplicitParams({
            @ApiImplicitParam(name = "address", value = "查询地址(1-广州，2-北京默认)", required = false, dataType = "int", paramType = "form"),
            @ApiImplicitParam(name = "order", value = "排序(true-升序,false(默认)-降序)", required = false, dataType = "string", paramType = "form"),
            @ApiImplicitParam(name = "orderBy", value = "排序字段(1(默认)-创建时间,2-实列名称)", required = false, dataType = "int", paramType = "form"),
            @ApiImplicitParam(name = "pageNo", value = "查询页码，默认1", required = false, dataType = "int", paramType = "form"),
            @ApiImplicitParam(name = "pageSize", value = "查询数量，默认10", required = false, dataType = "int", paramType = "form"),
            @ApiImplicitParam(name = "q", value = "根据实列名称模糊查询", required = false, dataType = "string", paramType = "form"),
    })
    public R readEndpointList(Integer address,String order,String q,Integer orderBy,Integer pageNo,Integer pageSize) {
        JSONObject paramJson = new JSONObject();
        if (pageNo == null || pageNo < 1) {
            pageNo = 1;
        }
        if (pageSize == null || pageSize < 1) {
            pageSize = 10;
        }
        if(pageSize>200){//最大值200
            pageSize = 200;
        }
        paramJson.put("pageNo", pageNo);
        paramJson.put("pageSize", pageSize);
        if(StringUtils.isNotBlank(order) && order.equals("true")){// 默认desc
            paramJson.put("order","asc");
        }
        if(orderBy!=null&&orderBy==2){// 默认createTime
            paramJson.put("orderBy","name");
        }
        if(StringUtils.isNotBlank(q)){
            paramJson.put("q",q);
        }
        JSONObject dataJson = iotDeviceService.getEndpointList( address,paramJson);
        return R.ok().put("code", dataJson.get("iotHttpCode")).put("data", dataJson);
    }
    @ApiOperation(value = "根据实列名称查询实列详情")
    @RequestMapping(value = "/readEndpointDetail", method = RequestMethod.POST)
    @ApiImplicitParams({
            @ApiImplicitParam(name = "address", value = "查询地址(1-广州，2-北京默认)", required = false, dataType = "int", paramType = "form"),
            @ApiImplicitParam(name = "name", value = "实列名称", required = true, dataType = "string", paramType = "form"),
 })
    public R readEndpointDetail(Integer address,String name) {
        JSONObject dataJson = iotDeviceService.getEndpointDetail( address,name);
        return R.ok().put("code", dataJson.get("iotHttpCode")).put("data", dataJson);
    }
    @ApiOperation(value = "创建实列")
    @RequestMapping(value = "/createEndpoint", method = RequestMethod.POST)
    @ApiImplicitParams({
            @ApiImplicitParam(name = "address", value = "查询地址(1-广州，2-北京默认)", required = false, dataType = "int", paramType = "form"),
            @ApiImplicitParam(name = "name", value = "实列名称", required = true, dataType = "string", paramType = "form"),
    })
    public R createEndpoint(Integer address,String name) {
        JSONObject dataJson = iotDeviceService.createEndpoint( address,name);
        return R.ok().put("code", dataJson.get("iotHttpCode")).put("data", dataJson);
    }
    @ApiOperation(value = "删除实列")
    @RequestMapping(value = "/delEndpoint", method = RequestMethod.POST)
    @ApiImplicitParams({
            @ApiImplicitParam(name = "address", value = "查询地址(1-广州，2-北京默认)", required = false, dataType = "int", paramType = "form"),
            @ApiImplicitParam(name = "name", value = "实列名称", required = true, dataType = "string", paramType = "form"),
    })
    public R delEndpoint(Integer address,String name) {
        JSONObject dataJson = iotDeviceService.delEndpoint( address,name);
        return R.ok().put("code", dataJson.get("iotHttpCode")).put("data", dataJson);
    }

    @ApiOperation(value = "查询设备列表")
    @RequestMapping(value = "/readThingDeviceList", method = RequestMethod.POST)
    @ApiImplicitParams({
            @ApiImplicitParam(name = "endpointName", value = "实列名称", required = true, dataType = "string", paramType = "form"),
            @ApiImplicitParam(name = "address", value = "查询地址(1-广州，2-北京默认)", required = false, dataType = "int", paramType = "form"),
            @ApiImplicitParam(name = "order", value = "排序(true-升序,false(默认)-降序)", required = false, dataType = "string", paramType = "form"),
            @ApiImplicitParam(name = "orderBy", value = "排序字段(1(默认)-创建时间,2-实列名称)", required = false, dataType = "int", paramType = "form"),
            @ApiImplicitParam(name = "pageNo", value = "查询页码，默认1", required = false, dataType = "int", paramType = "form"),
            @ApiImplicitParam(name = "pageSize", value = "查询数量，默认10", required = false, dataType = "int", paramType = "form"),
            @ApiImplicitParam(name = "q", value = "根据实列名称模糊查询", required = false, dataType = "string", paramType = "form"),
    })
    public R readThingDeviceList(Integer address,String endpointName,String order,String q,Integer orderBy,Integer pageNo,Integer pageSize) {
        JSONObject paramJson = new JSONObject();
        if (pageNo == null || pageNo < 1) {
            pageNo = 1;
        }
        if (pageSize == null || pageSize < 1) {
            pageSize = 10;
        }
        if(pageSize>200){//最大值200
            pageSize = 200;
        }
        paramJson.put("pageNo", pageNo);
        paramJson.put("pageSize", pageSize);
        if(StringUtils.isNotBlank(order) && order.equals("true")){// 默认desc
            paramJson.put("order","asc");
        }
        if(orderBy!=null&&orderBy==2){// 默认createTime
            paramJson.put("orderBy","name");
        }
        if(StringUtils.isNotBlank(q)){
            paramJson.put("q",q);
        }
        JSONObject dataJson = iotDeviceService.getThingDeviceList( address,endpointName,paramJson);
        return R.ok().put("code", dataJson.get("iotHttpCode")).put("data", dataJson);
    }
    @ApiOperation(value = "根据设备名称查询设备详情")
    @RequestMapping(value = "/readThingDeviceDetail", method = RequestMethod.POST)
    @ApiImplicitParams({
            @ApiImplicitParam(name = "endpointName", value = "实列名称", required = true, dataType = "string", paramType = "form"),
            @ApiImplicitParam(name = "address", value = "查询地址(1-广州，2-北京默认)", required = false, dataType = "int", paramType = "form"),
            @ApiImplicitParam(name = "name", value = "名称", required = true, dataType = "string", paramType = "form"),
    })
    public R readThingDeviceDetail(Integer address,String name,String endpointName) {
        JSONObject dataJson = iotDeviceService.getThingDeviceDetail( address,endpointName,name);
        return R.ok().put("code", dataJson.get("iotHttpCode")).put("data", dataJson);
    }
    @ApiOperation(value = "创建设备")
    @RequestMapping(value = "/createThingDevice", method = RequestMethod.POST)
    @ApiImplicitParams({
            @ApiImplicitParam(name = "endpointName", value = "实列名称", required = true, dataType = "string", paramType = "form"),
            @ApiImplicitParam(name = "address", value = "查询地址(1-广州，2-北京默认)", required = false, dataType = "int", paramType = "form"),
            @ApiImplicitParam(name = "name", value = "设备名称", required = true, dataType = "string", paramType = "form"),
    })
    public R createThingDevice(Integer address,String name,String endpointName) {
        JSONObject dataJson = iotDeviceService.createThingDevice( address,endpointName,name);
        return R.ok().put("code", dataJson.get("iotHttpCode")).put("data", dataJson);
    }
    @ApiOperation(value = "删除设备")
    @RequestMapping(value = "/delThingDevice", method = RequestMethod.POST)
    @ApiImplicitParams({
            @ApiImplicitParam(name = "endpointName", value = "实列名称", required = true, dataType = "string", paramType = "form"),
            @ApiImplicitParam(name = "address", value = "查询地址(1-广州，2-北京默认)", required = false, dataType = "int", paramType = "form"),
            @ApiImplicitParam(name = "name", value = "设备名称", required = true, dataType = "string", paramType = "form"),
    })
    public R deThingDevice(Integer address,String name,String endpointName) {
        JSONObject dataJson = iotDeviceService.delThingDevice( address,endpointName,name);
        return R.ok().put("code", dataJson.get("iotHttpCode")).put("data", dataJson);
    }


    @ApiOperation(value = "查询策略列表")
    @RequestMapping(value = "/readPolicyList", method = RequestMethod.POST)
    @ApiImplicitParams({
            @ApiImplicitParam(name = "endpointName", value = "实列名称", required = true, dataType = "string", paramType = "form"),
            @ApiImplicitParam(name = "principalName", value = "身份名称", required = false, dataType = "string", paramType = "form"),
            @ApiImplicitParam(name = "address", value = "查询地址(1-广州，2-北京默认)", required = false, dataType = "int", paramType = "form"),
            @ApiImplicitParam(name = "order", value = "排序(true-升序,false(默认)-降序)", required = false, dataType = "string", paramType = "form"),
            @ApiImplicitParam(name = "orderBy", value = "排序字段(1(默认)-创建时间,2-实列名称)", required = false, dataType = "int", paramType = "form"),
            @ApiImplicitParam(name = "pageNo", value = "查询页码，默认1", required = false, dataType = "int", paramType = "form"),
            @ApiImplicitParam(name = "pageSize", value = "查询数量，默认10", required = false, dataType = "int", paramType = "form"),
    })
    public R readPolicyList(Integer address,String principalName,String endpointName,String order,Integer orderBy,Integer pageNo,Integer pageSize) {
        JSONObject paramJson = new JSONObject();
        if (pageNo == null || pageNo < 1) {
            pageNo = 1;
        }
        if (pageSize == null || pageSize < 1) {
            pageSize = 10;
        }
        if(pageSize>200){//最大值200
            pageSize = 200;
        }
        paramJson.put("pageNo", pageNo);
        paramJson.put("pageSize", pageSize);
        if(StringUtils.isNotBlank(principalName)){
            paramJson.put("principalName", principalName);
        }
        if(StringUtils.isNotBlank(order) && order.equals("true")){// 默认desc
            paramJson.put("order","asc");
        }
        if(orderBy!=null&&orderBy==2){// 默认createTime
            paramJson.put("orderBy","name");
        }
        JSONObject dataJson = iotDeviceService.readPolicyList( address,endpointName,paramJson);
        return R.ok().put("code", dataJson.get("iotHttpCode")).put("data", dataJson);
    }
    @ApiOperation(value = "根据策略名称查询策略详情")
    @RequestMapping(value = "/readPolicyDetail", method = RequestMethod.POST)
    @ApiImplicitParams({
            @ApiImplicitParam(name = "endpointName", value = "实列名称", required = true, dataType = "string", paramType = "form"),
            @ApiImplicitParam(name = "address", value = "查询地址(1-广州，2-北京默认)", required = false, dataType = "int", paramType = "form"),
            @ApiImplicitParam(name = "name", value = "策略名称", required = true, dataType = "string", paramType = "form"),
    })
    public R readPolicyDetail(Integer address,String name,String endpointName) {
        JSONObject dataJson = iotDeviceService.readPolicyDetail( address,endpointName,name);
        return R.ok().put("code", dataJson.get("iotHttpCode")).put("data", dataJson);
    }
    @ApiOperation(value = "创建策略")
    @RequestMapping(value = "/createPolicy", method = RequestMethod.POST)
    @ApiImplicitParams({
            @ApiImplicitParam(name = "endpointName", value = "实列名称", required = true, dataType = "string", paramType = "form"),
            @ApiImplicitParam(name = "address", value = "查询地址(1-广州，2-北京默认)", required = false, dataType = "int", paramType = "form"),
            @ApiImplicitParam(name = "name", value = "策略名称", required = true, dataType = "string", paramType = "form"),
    })
    public R createPolicy(Integer address,String name,String endpointName) {
        JSONObject dataJson = iotDeviceService.createPolicy( address,endpointName,name);
        return R.ok().put("code", dataJson.get("iotHttpCode")).put("data", dataJson);
    }
    @ApiOperation(value = "删除策略")
    @RequestMapping(value = "/delPolicy", method = RequestMethod.POST)
    @ApiImplicitParams({
            @ApiImplicitParam(name = "endpointName", value = "实列名称", required = true, dataType = "string", paramType = "form"),
            @ApiImplicitParam(name = "address", value = "查询地址(1-广州，2-北京默认)", required = false, dataType = "int", paramType = "form"),
            @ApiImplicitParam(name = "name", value = "策略名称", required = true, dataType = "string", paramType = "form"),
    })
    public R delPolicy(Integer address,String name,String endpointName) {
        JSONObject dataJson = iotDeviceService.delPolicy( address,endpointName,name);
        return R.ok().put("code", dataJson.get("iotHttpCode")).put("data", dataJson);
    }

    @ApiOperation(value = "查询身份列表")
    @RequestMapping(value = "/readPrincipalList", method = RequestMethod.POST)
    @ApiImplicitParams({
            @ApiImplicitParam(name = "endpointName", value = "实列名称", required = true, dataType = "string", paramType = "form"),
            @ApiImplicitParam(name = "thingName", value = "设备名称", required = false, dataType = "string", paramType = "form"),
            @ApiImplicitParam(name = "address", value = "查询地址(1-广州，2-北京默认)", required = false, dataType = "int", paramType = "form"),
            @ApiImplicitParam(name = "order", value = "排序(true-升序,false(默认)-降序)", required = false, dataType = "string", paramType = "form"),
            @ApiImplicitParam(name = "orderBy", value = "排序字段(1(默认)-创建时间,2-实列名称)", required = false, dataType = "int", paramType = "form"),
            @ApiImplicitParam(name = "pageNo", value = "查询页码，默认1", required = false, dataType = "int", paramType = "form"),
            @ApiImplicitParam(name = "pageSize", value = "查询数量，默认10", required = false, dataType = "int", paramType = "form"),
            @ApiImplicitParam(name = "q", value = "根据实列名称模糊查询", required = false, dataType = "string", paramType = "form"),
    })
    public R readPrincipalList(Integer address,String thingName,String endpointName,String order,String q,Integer orderBy,Integer pageNo,Integer pageSize) {
        JSONObject paramJson = new JSONObject();
        if (pageNo == null || pageNo < 1) {
            pageNo = 1;
        }
        if (pageSize == null || pageSize < 1) {
            pageSize = 10;
        }
        if(pageSize>200){//最大值200
            pageSize = 200;
        }
        paramJson.put("pageNo", pageNo);
        paramJson.put("pageSize", pageSize);
        if(StringUtils.isNotBlank(thingName)){
            paramJson.put("thingName", thingName);
        }
        if(StringUtils.isNotBlank(order) && order.equals("true")){// 默认desc
            paramJson.put("order","asc");
        }
        if(orderBy!=null&&orderBy==2){// 默认createTime
            paramJson.put("orderBy","name");
        }
        if(StringUtils.isNotBlank(q)){
            paramJson.put("q",q);
        }
        JSONObject dataJson = iotDeviceService.getPrincipalList( address,endpointName,paramJson);
        return R.ok().put("code", dataJson.get("iotHttpCode")).put("data", dataJson);
    }
    @ApiOperation(value = "根据身份名称查询身份详情")
    @RequestMapping(value = "/readPrincipalDetail", method = RequestMethod.POST)
    @ApiImplicitParams({
            @ApiImplicitParam(name = "endpointName", value = "实列名称", required = true, dataType = "string", paramType = "form"),
            @ApiImplicitParam(name = "address", value = "查询地址(1-广州，2-北京默认)", required = false, dataType = "int", paramType = "form"),
            @ApiImplicitParam(name = "name", value = "身份名称", required = true, dataType = "string", paramType = "form"),
    })
    public R readPrincipalDetail(Integer address,String name,String endpointName) {
        JSONObject dataJson = iotDeviceService.getPrincipalDetail( address,endpointName,name);
        return R.ok().put("code", dataJson.get("iotHttpCode")).put("data", dataJson);
    }
    @ApiOperation(value = "创建身份")
    @RequestMapping(value = "/createPrincipal", method = RequestMethod.POST)
    @ApiImplicitParams({
            @ApiImplicitParam(name = "endpointName", value = "实列名称", required = true, dataType = "string", paramType = "form"),
            @ApiImplicitParam(name = "address", value = "查询地址(1-广州，2-北京默认)", required = false, dataType = "int", paramType = "form"),
            @ApiImplicitParam(name = "name", value = "身份名称", required = true, dataType = "string", paramType = "form"),
    })
    public R createPrincipal(Integer address,String name,String endpointName) {
        JSONObject dataJson = iotDeviceService.createPrincipal( address,endpointName,name);
        return R.ok().put("code", dataJson.get("iotHttpCode")).put("data", dataJson);
    }
    @ApiOperation(value = "删除身份")
    @RequestMapping(value = "/delPrincipal", method = RequestMethod.POST)
    @ApiImplicitParams({
            @ApiImplicitParam(name = "endpointName", value = "实列名称", required = true, dataType = "string", paramType = "form"),
            @ApiImplicitParam(name = "address", value = "查询地址(1-广州，2-北京默认)", required = false, dataType = "int", paramType = "form"),
            @ApiImplicitParam(name = "name", value = "身份名称", required = true, dataType = "string", paramType = "form"),
    })
    public R delPrincipal(Integer address,String name,String endpointName) {
        JSONObject dataJson = iotDeviceService.delPrincipal( address,endpointName,name);
        return R.ok().put("code", dataJson.get("iotHttpCode")).put("data", dataJson);
    }
    @ApiOperation(value = "重新生成身份密钥")
    @RequestMapping(value = "/rebuildPrincipal", method = RequestMethod.POST)
    @ApiImplicitParams({
            @ApiImplicitParam(name = "endpointName", value = "实列名称", required = true, dataType = "string", paramType = "form"),
            @ApiImplicitParam(name = "address", value = "查询地址(1-广州，2-北京默认)", required = false, dataType = "int", paramType = "form"),
            @ApiImplicitParam(name = "name", value = "身份名称", required = true, dataType = "string", paramType = "form"),
    })
    public R rebuildPrincipal(Integer address,String name,String endpointName) {
        JSONObject dataJson = iotDeviceService.editPrincipal( address,endpointName,name);
        return R.ok().put("code", dataJson.get("iotHttpCode")).put("data", dataJson);
    }

    @ApiOperation(value = "身份绑定策略")
    @RequestMapping(value = "/principalBindPolicy", method = RequestMethod.POST)
    @ApiImplicitParams({
            @ApiImplicitParam(name = "endpointName", value = "实列名称", required = true, dataType = "string", paramType = "form"),
            @ApiImplicitParam(name = "address", value = "查询地址(1-广州，2-北京默认)", required = false, dataType = "int", paramType = "form"),
            @ApiImplicitParam(name = "policyName", value = "策略名称", required = true, dataType = "string", paramType = "form"),
            @ApiImplicitParam(name = "principalName", value = "身份名称", required = true, dataType = "string", paramType = "form"),
    })
    public R principalBindPolicy(Integer address,String policyName,String endpointName,String principalName) {
        JSONObject dataJson = iotDeviceService.principalBindPolicy( address,endpointName,principalName,policyName);
        return R.ok().put("code", dataJson.get("iotHttpCode")).put("data", dataJson);
    }
    @ApiOperation(value = "身份解绑策略")
    @RequestMapping(value = "/principalRemovePolicy", method = RequestMethod.POST)
    @ApiImplicitParams({
            @ApiImplicitParam(name = "endpointName", value = "实列名称", required = true, dataType = "string", paramType = "form"),
            @ApiImplicitParam(name = "address", value = "查询地址(1-广州，2-北京默认)", required = false, dataType = "int", paramType = "form"),
            @ApiImplicitParam(name = "policyName", value = "策略名称", required = true, dataType = "string", paramType = "form"),
            @ApiImplicitParam(name = "principalName", value = "身份名称", required = true, dataType = "string", paramType = "form"),
    })
    public R principalRemovePolicy(Integer address,String policyName,String endpointName,String principalName) {
        JSONObject dataJson = iotDeviceService.principalRemovePolicy( address,endpointName,principalName,policyName);
        return R.ok().put("code", dataJson.get("iotHttpCode")).put("data", dataJson);
    }
    @ApiOperation(value = "设备绑定身份")
    @RequestMapping(value = "/thingBindPrincipal", method = RequestMethod.POST)
    @ApiImplicitParams({
            @ApiImplicitParam(name = "endpointName", value = "实列名称", required = true, dataType = "string", paramType = "form"),
            @ApiImplicitParam(name = "address", value = "查询地址(1-广州，2-北京默认)", required = false, dataType = "int", paramType = "form"),
            @ApiImplicitParam(name = "thingName", value = "设备名称", required = true, dataType = "string", paramType = "form"),
            @ApiImplicitParam(name = "principalName", value = "身份名称", required = true, dataType = "string", paramType = "form"),
    })
    public R thingBindPrincipal(Integer address,String thingName,String endpointName,String principalName) {
        JSONObject dataJson = iotDeviceService.thingBindPrincipal( address,endpointName,principalName,thingName);
        return R.ok().put("code", dataJson.get("iotHttpCode")).put("data", dataJson);
    }
    @ApiOperation(value = "设备解绑身份")
    @RequestMapping(value = "/thingRemovePrincipal", method = RequestMethod.POST)
    @ApiImplicitParams({
            @ApiImplicitParam(name = "endpointName", value = "实列名称", required = true, dataType = "string", paramType = "form"),
            @ApiImplicitParam(name = "address", value = "查询地址(1-广州，2-北京默认)", required = false, dataType = "int", paramType = "form"),
            @ApiImplicitParam(name = "thingName", value = "设备名称", required = true, dataType = "string", paramType = "form"),
            @ApiImplicitParam(name = "principalName", value = "身份名称", required = true, dataType = "string", paramType = "form"),
    })
    public R thingRemovePrincipal(Integer address,String thingName,String endpointName,String principalName) {
        JSONObject dataJson = iotDeviceService.thingRemovePrincipal( address,endpointName,principalName,thingName);
        return R.ok().put("code", dataJson.get("iotHttpCode")).put("data", dataJson);
    }

    @ApiOperation(value = "查询策略下的主题列表")
    @RequestMapping(value = "/readTopicListByPolicyName", method = RequestMethod.POST)
    @ApiImplicitParams({
            @ApiImplicitParam(name = "endpointName", value = "实列名称", required = true, dataType = "string", paramType = "form"),
            @ApiImplicitParam(name = "policyName", value = "策略名称", required = true, dataType = "string", paramType = "form"),
            @ApiImplicitParam(name = "address", value = "查询地址(1-广州，2-北京默认)", required = false, dataType = "int", paramType = "form"),
            @ApiImplicitParam(name = "order", value = "排序(true-升序,false(默认)-降序)", required = false, dataType = "string", paramType = "form"),
            @ApiImplicitParam(name = "orderBy", value = "排序字段(1(默认)-创建时间,2-实列名称)", required = false, dataType = "int", paramType = "form"),
            @ApiImplicitParam(name = "pageNo", value = "查询页码，默认1", required = false, dataType = "int", paramType = "form"),
            @ApiImplicitParam(name = "pageSize", value = "查询数量，默认10", required = false, dataType = "int", paramType = "form"),
    })
    public R readTopicListByPolicyName(Integer address,String policyName,String endpointName,String order,Integer orderBy,Integer pageNo,Integer pageSize) {
        JSONObject paramJson = new JSONObject();
        if (pageNo == null || pageNo < 1) {
            pageNo = 1;
        }
        if (pageSize == null || pageSize < 1) {
            pageSize = 10;
        }
        if(pageSize>200){//最大值200
            pageSize = 200;
        }
        paramJson.put("pageNo", pageNo);
        paramJson.put("pageSize", pageSize);
        if(StringUtils.isNotBlank(policyName)){
            paramJson.put("policyName", policyName);
        }
        if(StringUtils.isNotBlank(order) && order.equals("true")){// 默认desc
            paramJson.put("order","asc");
        }
        if(orderBy!=null&&orderBy==2){// 默认createTime
            paramJson.put("orderBy","name");
        }
        JSONObject dataJson = iotDeviceService.readTopicList( address,endpointName,paramJson);
        return R.ok().put("code", dataJson.get("iotHttpCode")).put("data", dataJson);
    }
    @ApiOperation(value = "查询指定主题详情")
    @RequestMapping(value = "/readTopicDetail", method = RequestMethod.POST)
    @ApiImplicitParams({
            @ApiImplicitParam(name = "endpointName", value = "实列名称", required = true, dataType = "string", paramType = "form"),
            @ApiImplicitParam(name = "address", value = "查询地址(1-广州，2-北京默认)", required = false, dataType = "int", paramType = "form"),
            @ApiImplicitParam(name = "permissionUuid", value = "主题的permissionUuid", required = true, dataType = "string", paramType = "form"),
    })
    public R readTopicDetail(Integer address,String permissionUuid,String endpointName) {
        JSONObject dataJson = iotDeviceService.readTopicDetail( address,endpointName,permissionUuid);
        return R.ok().put("code", dataJson.get("iotHttpCode")).put("data", dataJson);
    }
    @ApiOperation(value = "策略设置主题")
    @RequestMapping(value = "/addTopicToPolicy", method = RequestMethod.POST)
    @ApiImplicitParams({
            @ApiImplicitParam(name = "endpointName", value = "实列名称", required = true, dataType = "string", paramType = "form"),
            @ApiImplicitParam(name = "address", value = "查询地址(1-广州，2-北京默认)", required = false, dataType = "int", paramType = "form"),
            @ApiImplicitParam(name = "policyName", value = "策略名称", required = true, dataType = "string", paramType = "form"),
            @ApiImplicitParam(name = "operations", value = "允许的操作,数组，以,分开", required = true, dataType = "string", paramType = "form"),
            @ApiImplicitParam(name = "topic", value = "主题名称", required = true, dataType = "string", paramType = "form"),
   })
    public R addTopicToPolicy(Integer address,String policyName,String endpointName,String operations,String topic) {
        if(StringUtils.isBlank(endpointName)){
            return  R.error(400,"实列名称不能为空");
        }
        if(StringUtils.isBlank(policyName)){
            return  R.error(400,"策略名称不能为空");
        }
        if(StringUtils.isBlank(operations)){
            return  R.error(400,"允许的操作不能为空");
        }
        StringTokenizer stringTokenizer = new StringTokenizer(operations, ",");
        JSONArray operationsList = new JSONArray();
        while (stringTokenizer.hasMoreTokens()) {
            operationsList.add(stringTokenizer.nextToken());
        }
        if(StringUtils.isBlank(topic)){
            return  R.error(400,"请选择主题");
        }
        JSONObject paramJson = new JSONObject();
        paramJson.put("policyName",policyName);
        paramJson.put("operations",operationsList);
        paramJson.put("topic",topic);
        JSONObject dataJson = iotDeviceService.addTopicToPolicy( address,endpointName,paramJson);
        return R.ok().put("code", dataJson.get("iotHttpCode")).put("data", dataJson);
    }
    @ApiOperation(value = "删除主题")
    @RequestMapping(value = "/delTopic", method = RequestMethod.POST)
    @ApiImplicitParams({
            @ApiImplicitParam(name = "endpointName", value = "实列名称", required = true, dataType = "string", paramType = "form"),
            @ApiImplicitParam(name = "address", value = "查询地址(1-广州，2-北京默认)", required = false, dataType = "int", paramType = "form"),
            @ApiImplicitParam(name = "permissionUuid", value = "主题的permissionUuid", required = true, dataType = "string", paramType = "form"),
    })
    public R delTopic(Integer address,String permissionUuid,String endpointName) {
        JSONObject dataJson = iotDeviceService.delTopic( address,endpointName,permissionUuid);
        return R.ok().put("code", dataJson.get("iotHttpCode")).put("data", dataJson);
    }

    @ApiOperation(value = "更新主题")
    @RequestMapping(value = "/updateTopic", method = RequestMethod.POST)
    @ApiImplicitParams({
            @ApiImplicitParam(name = "endpointName", value = "实列名称", required = true, dataType = "string", paramType = "form"),
            @ApiImplicitParam(name = "address", value = "查询地址(1-广州，2-北京默认)", required = false, dataType = "int", paramType = "form"),
            @ApiImplicitParam(name = "permissionUuid", value = "主题的permissionUuid", required = true, dataType = "string", paramType = "form"),
            @ApiImplicitParam(name = "operations", value = "允许的操作,数组，以,分开", required = true, dataType = "string", paramType = "form"),
            @ApiImplicitParam(name = "topic", value = "主题名称", required = true, dataType = "string", paramType = "form"),
    })
    public R updateTopic(Integer address,String permissionUuid,String endpointName,String operations,String topic) {
        if(StringUtils.isBlank(endpointName)){
            return  R.error(400,"实列名称不能为空");
        }
        if(StringUtils.isBlank(permissionUuid)){
            return  R.error(400,"参数permissionUuid不能为空");
        }
        if(StringUtils.isBlank(operations)){
            return  R.error(400,"允许的操作不能为空");
        }
        StringTokenizer stringTokenizer = new StringTokenizer(operations, ",");
        JSONArray operationsList = new JSONArray();
        while (stringTokenizer.hasMoreTokens()) {
            operationsList.add(stringTokenizer.nextToken());
        }
        if(StringUtils.isBlank(topic)){
            return  R.error(400,"请选择主题");
        }
        JSONObject paramJson = new JSONObject();
        paramJson.put("operations",operationsList);
        paramJson.put("topic",topic);
        JSONObject dataJson = iotDeviceService.updateTopic( address,endpointName,permissionUuid,paramJson);
        return R.ok().put("code", dataJson.get("iotHttpCode")).put("data", dataJson);
    }
}
