package com.pms.api;
import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.pms.controller.BaseController;
import com.pms.entity.*;
import com.pms.exception.R;
import com.pms.rpc.UserService;
import com.pms.service.IIotAgencyService;
import com.pms.service.IIotRepairDeclareService;
import com.pms.service.IIotWaterPumpGroupService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
/**
 * Created by hm on 2018/9/3.
 */
@RestController
@RequestMapping("/iotRepairDeclare")
@Api(value="APP接口二汞设备保修申报表",description = "APP接口二汞设备保修申报表")
public class ApiIotRepairDeclareController extends BaseController {
    @Autowired
    IIotRepairDeclareService iotRepairDeclareService;
    @Autowired
    IIotWaterPumpGroupService waterPumpGroupService;
    @Autowired
    UserService userService;
    @Autowired
    IIotAgencyService orgService;
    /**
     * 分页查询设备保修信息
     * @param roleId
     * @param state
     * @param limit
     * @param page
     * @return
     */
    @PostMapping("/getByIotRepairDeclarePage")
    @ApiOperation(value = "查询设备保修信息")
    @ApiImplicitParams({
            @ApiImplicitParam(name="companyCode",value="系统编码",required = true,dataType = "String",paramType="form"),
            @ApiImplicitParam(name="roleId",value="角色ID",required = true,dataType = "Long",paramType="form"),
            @ApiImplicitParam(name="state",value="状态(1全部2待完成3已完成)",required = true,dataType = "Integer",paramType="form"),
            @ApiImplicitParam(name="limit",value="分页条数",required = false,dataType = "int",paramType="form"),
            @ApiImplicitParam(name="page",value="分页页数",required = false,dataType = "int",paramType="form"),
            @ApiImplicitParam(name="userId",value="用户ID",required = true,dataType = "Long",paramType="form"),
    })
    public R getByIotRepairDeclarePage(Long userId,Long roleId,String  companyCode,Integer state, int limit, int page){
        parameterIsNull(roleId,"请填写角色ID");
        parameterIsNull(userId,"请填写用户ID");
        parameterIsNull(companyCode,"请填系统编码");
        parameterIsNull(state,"请填写查询状态");
        if (limit<1){limit=10;}
        if (page<1){page=1;}
        Map<String,Object> map=new HashMap<>();
        UserInfo user= userService.selectbyPipUser(userId);
        if (null==user||null==user.getId()){return  R.error(400,"用户信息不存在");}
        if (user.getGroupType()==1){//管理员
            map.put("companyCode",companyCode);
            map.put("roleId",null);
        }else {
            map.put("roleId",roleId);
            map.put("companyCode",companyCode);
        }
        map.put("limit",limit);
        map.put("page",(page-1) * limit);
        map.put("state",state);
        Page<Map<String,Object>> pageList = new Page<>(page,limit);
        pageList = iotRepairDeclareService.getByIotRepairDeclarePage(pageList,map);
        return R.ok().put("data",pageList);
    }


    /**
     * 添加申报二汞水泵组维修
     * @param groupId
     * @param declareType
     * @param description
     * @return
     */
    @PostMapping("/app/add")
    @ApiOperation(value = "添加申报二汞水泵组维修")
    @ApiImplicitParams({
            @ApiImplicitParam(name="groupId",value="水泵组ID",required = true,dataType = "Long",paramType="form"),
            @ApiImplicitParam(name="roleId",value="角色ID",required = true,dataType = "Long",paramType="form"),
            @ApiImplicitParam(name="userId",value="用户ID",required = true,dataType = "Long",paramType="form"),
            @ApiImplicitParam(name="declareType",value="1.未知2.设备故障3.环境因素",required = true,dataType = "Integer",paramType="form"),
            @ApiImplicitParam(name="description",value="故障描述",required = true,dataType = "String",paramType="form"),
            @ApiImplicitParam(name="companyCode",value="系统编码",required = true,dataType = "String",paramType="form"),
            @ApiImplicitParam(name="agencyId",value="机构ID",required = true,dataType = "Long",paramType="form"),
    })
    public R add(Long roleId,Long groupId,Long userId,Integer declareType,String description,String companyCode,Long agencyId){
        parameterIsNull(groupId,"请填写水泵组ID");
        parameterIsNull(userId,"请填写用户ID");
        parameterIsNull(roleId,"请填写角色ID");
        parameterIsNull(declareType,"请填写故障类型");
        parameterIsNull(description,"请填写故障描述");
        parameterIsNull(companyCode,"请填写系统编码");
        parameterIsNull(agencyId,"请填写机构ID");
        BaseUserInfo user= userService.getUserByUserId(userId);
        if (null==user||null==user.getId()){return  R.error(400,"用户信息不存在");}
        IotRepairDeclare entity=new IotRepairDeclare();
        IotAgency agency = orgService.selectById(agencyId);
        parameterIsNull(agency,"机构信息不存在");
        if (agency.getLevel()==2){
            entity.setAgencyId(agencyId);
        }else  if (agency.getLevel()==3){
            entity.setAgencyId(agency.getParentId());
            entity.setCommunityId(agency.getId());
        }else  if (agency.getLevel()==4){
            IotAgency  parentAgency = orgService.selectById(agency.getParentId());
            parameterIsNull(parentAgency,"小区机构信息不存在");
            entity.setAgencyId(parentAgency.getParentId());
            entity.setCommunityId(agency.getParentId());
            entity.setHouseId(agency.getId());
        }
        IotRepairDeclare declare=iotRepairDeclareService.selectOne(new EntityWrapper<IotRepairDeclare>().eq("group_id",groupId).and().eq("state_",1));
        if (declare!=null){return  R.error(400,"该水泵组已申报,请查看");}
        IotWaterPumpGroup eq=waterPumpGroupService.selectById(groupId);
        if (eq==null){return  R.error(400,"水泵组信息不存在");}
        entity.setCompanyCode(companyCode);
        entity.setRoleId(roleId);
        entity.setGroupName(eq.getName());//水泵组名称
        entity.setBelongedCommunity(eq.getCommunityName());//所属小区
        entity.setBelongedHouse(eq.getParentName());//所属水泵房
        entity.setDeclareType(declareType);//故障类型
        entity.setDeclarePerson(user.getName());//申报人
        entity.setDeclarePhone(user.getMobilePhone());//申报人电话
        entity.setDescription(description);//故障描述
        entity.setGroupId(groupId);//设备ID
        entity.setDeclareData(new Date());//申报时间
        entity.setState(1);//状态1待处理2拒绝受理3已受理
        entity.setCrtUser(user.getName());//创建人
        entity.setCrtTime(new Date());//创建时间
        entity.setIsRead(2);//是否已读(1已读2未读)
        iotRepairDeclareService.insert(entity);
        return R.ok();
    }

    /***
     *查询设备保修信息
     * @param repairDeclareId
     * @param state
     * @return
     */
    @PostMapping("/getByIotRepairDeclare")
    @ApiOperation(value = "查询设备保修信息")
    @ApiImplicitParams({
            @ApiImplicitParam(name="repairDeclareId",value="设备保修信息ID",required = true,dataType = "Long",paramType="form"),
            @ApiImplicitParam(name="state",value="状态(1待受理2待维修3拒绝受理4.维修成功5维修失败)",required = true,dataType = "Integer",paramType="form"),
             })
    public R getByIotRepairDeclare(Long repairDeclareId, Integer state){
        parameterIsNull(repairDeclareId,"请填写设备保修信息ID");
        parameterIsNull(state,"请填写查询状态");
        Map<String, Object>  map =new HashMap<>();
        if (state==4||state==5){
            Map<String, Object>  iotRepairDeclare=iotRepairDeclareService.getByIotRepairDeclare(repairDeclareId);
            if (iotRepairDeclare==null){return  R.error(400,"设备保修信息不存在");}
            if (iotRepairDeclare.get("state_").equals(1)){
                map.put("state",4);
            }else {
                map.put("state",5);
            }
            map.put("belongedCommunity",iotRepairDeclare.get("belonged_community"));//所属小区
            map.put("belongedHouse",iotRepairDeclare.get("belonged_house"));//所属水泵房
            map.put("groupName",iotRepairDeclare.get("group_name"));//所属设备组
            map.put("declareData",iotRepairDeclare.get("declare_data"));//更新时间：该状态更新的最新时间
            map.put("declareType",iotRepairDeclare.get("declare_type"));//故障类型1.未知2.设备故障3.环境因素
            map.put("description",iotRepairDeclare.get("description"));//故障描述
            map.put("acceptPerson",iotRepairDeclare.get("acceptPerson"));//受理人
            map.put("acceptPhone",iotRepairDeclare.get("acceptPhone"));//受理电话
            map.put("crtUser",iotRepairDeclare.get("crtUser"));//操作员
            map.put("startData",iotRepairDeclare.get("start_data"));//开始时间
            map.put("endData",iotRepairDeclare.get("end_data"));//结束时间
            map.put("maintenanceCosts",iotRepairDeclare.get("maintenance_costs"));//维修费用
            map.put("maintenanceDescription",iotRepairDeclare.get("maintenance_description"));//维修描述
            IotRepairDeclare en = iotRepairDeclareService.selectById(repairDeclareId);
            en.setIsRead(1);//修改为已读
            iotRepairDeclareService.updateById(en);
        }else {
            IotRepairDeclare iotRepairDeclare = iotRepairDeclareService.selectById(repairDeclareId);
            if (iotRepairDeclare==null){return  R.error(400,"设备保修信息不存在");}
            if (iotRepairDeclare.getState()==3){
                map.put("state",2);
            }else if (iotRepairDeclare.getState()==2){
                map.put("state",3);
            }else {
                map.put("state",1);
            }
            map.put("belongedCommunity",iotRepairDeclare.getBelongedCommunity());//所属小区
            map.put("belongedHouse",iotRepairDeclare.getBelongedHouse());//所属水泵房
            map.put("groupName",iotRepairDeclare.getGroupName());//所属设备组
            map.put("declareData",iotRepairDeclare.getDeclareData());//更新时间：该状态更新的最新时间
            map.put("declareType",iotRepairDeclare.getDeclareType());//故障类型1.未知2.设备故障3.环境因素
            map.put("description",iotRepairDeclare.getDescription());//故障描述
            map.put("acceptPerson",iotRepairDeclare.getAcceptPerson());//受理人
            map.put("acceptPhone",iotRepairDeclare.getAcceptPhone());//受理电话
            map.put("crtUser",iotRepairDeclare.getCrtUser());//操作员
            map.put("updUser",iotRepairDeclare.getUpdUser());
            map.put("updTime",iotRepairDeclare.getUpdTime());
            map.put("acceptDta",iotRepairDeclare.getAcceptData());
            iotRepairDeclare.setIsRead(1);//修改为已读
            iotRepairDeclareService.updateById(iotRepairDeclare);
        }
        return (R.ok()).put("data",map);
    }

}
