package com.pms.api;

        import com.alibaba.fastjson.JSONObject;
        import com.alibaba.fastjson.TypeReference;
        import com.baidubce.services.tsdb.model.*;
        import com.pms.controller.BaseController;
        import com.pms.entity.QueryParam;
        import com.pms.exception.R;
        import com.pms.service.tsdb.ITsdbService;
        import com.pms.util.DateUtil;
        import com.pms.utils.TsdbReqParamUtil;
        import com.pms.validator.Assert;
        import io.swagger.annotations.*;
        import org.apache.commons.lang.StringUtils;
        import org.springframework.beans.BeanUtils;
        import org.springframework.beans.factory.annotation.Autowired;
        import org.springframework.web.bind.annotation.RequestBody;
        import org.springframework.web.bind.annotation.RequestMapping;
        import org.springframework.web.bind.annotation.RequestMethod;
        import org.springframework.web.bind.annotation.RestController;

        import java.text.SimpleDateFormat;
        import java.util.*;

/**
 * 聚合函数类型{1-求平均数，2-总记录数，3-求总和,4-求相邻值的差,5/6-求最大/小值,7/8-求倍数/除数,9/10-求第一个/最后一个值}
 */
@RestController
@Api(value = "测试TSDB接口", description = "测试TSDB接口")
@RequestMapping(value = "testTsdb", method = RequestMethod.POST)
public class TsdbTestApi extends BaseController {
    @Autowired
    ITsdbService tsdbService;

    @ApiOperation(value = "查询度量")
    @RequestMapping(value = "/readMetrics", method = RequestMethod.POST)
    @ApiImplicitParams({
            @ApiImplicitParam(name = "address", value = "查询地址(1-广州默认，2-北京)", required = false, dataType = "int", paramType = "form"),
            @ApiImplicitParam(name = "dataType", value = "数据返回类型(1-全数据，2-度量集合(默认))", required = false, dataType = "string", paramType = "form"),
    })
    public R readMetrics(Integer address,Integer dataType) {
        if (dataType == null || dataType != 1) {
            return R.ok().put("data", tsdbService.readMetricResponse(address,null));
        }
        List<String> metricList = tsdbService.readMetricList(address,null);
        if (metricList == null) {
            metricList = new ArrayList<String>();
        }
        return R.ok().put("data", metricList);
    }

    @ApiOperation(value = "查询度量下的域")
    @RequestMapping(value = "/readFilds", method = RequestMethod.POST)
    @ApiImplicitParams({
            @ApiImplicitParam(name = "address", value = "查询地址(1-广州默认，2-北京)", required = false, dataType = "int", paramType = "form"),
            @ApiImplicitParam(name = "metric", value = "度量", required = true, dataType = "string", paramType = "form"),
            @ApiImplicitParam(name = "dataType", value = "数据返回类型(1-全数据,2-域集合(默认))", required = false, dataType = "string", paramType = "form"),
    })
    public R readFilds(Integer address,String metric, Integer dataType) {
        if (dataType == null || dataType != 1) {
            return R.ok().put("data", tsdbService.readFieldResponse(address,null, metric));
        }
        return R.ok().put("data", tsdbService.readFieldMap(address,null, metric));
    }

    @ApiOperation(value = "查询度量下的标签")
    @RequestMapping(value = "/readTags", method = RequestMethod.POST)
    @ApiImplicitParams({
            @ApiImplicitParam(name = "address", value = "查询地址(1-广州默认，2-北京)", required = false, dataType = "int", paramType = "form"),
            @ApiImplicitParam(name = "metric", value = "度量", required = true, dataType = "string", paramType = "form"),
            @ApiImplicitParam(name = "dataType", value = "数据返回类型(1-全数据,2-标签Map集合(默认))", required = false, dataType = "string", paramType = "form"),
    })
    public R readTags(Integer address,String metric, Integer dataType) {
        if (dataType == null || dataType != 1) {
            return R.ok().put("data", tsdbService.readTagResponse( address,null, metric));
        }
        return R.ok().put("data", tsdbService.readTagMap(address,null, metric));
    }

    @ApiOperation(value = "查询数据点,参数类型为对象，暂不使用")
    @RequestMapping(value = "/queryDataPoint", method = RequestMethod.POST)
    @ApiImplicitParams({
            @ApiImplicitParam(name = "address", value = "查询地址(1-广州默认，2-北京)", required = false, dataType = "int", paramType = "form"),
            @ApiImplicitParam(name = "dataType", value = "数据返回类型(1-全数据,2-标签Map集合(默认))", required = false, dataType = "string", paramType = "form"),
    })
    public R queryDataPoint(Integer address,Integer dataType
            , @ApiParam(name = "query", value = "查询对象", required = true) @RequestBody QueryParam query) {
        Query queryParam = new Query();
        BeanUtils.copyProperties(query, queryParam);
        if (dataType != null && dataType == 1) {
            QueryDatapointsResponse datapointsResponse = tsdbService.readDataPointResponse(address,Arrays.asList(queryParam));
            return R.ok().put("data", datapointsResponse);
        } else {
            return R.ok().put("data", tsdbService.readDataPointResult(address,Arrays.asList(queryParam)));
        }
    }

    @ApiOperation(value = "查询数据点")
    @RequestMapping(value = "/readDataPoint", method = RequestMethod.POST)
    @ApiImplicitParams({
            @ApiImplicitParam(name = "address", value = "查询地址(1-广州默认，2-北京)", required = false, dataType = "int", paramType = "form"),
            @ApiImplicitParam(name = "dataType", value = "数据返回类型(1-全数据,2-result集合(默认))", required = false, dataType = "string", paramType = "form"),
            @ApiImplicitParam(name = "metric", value = "度量", required = true, dataType = "string", paramType = "form"),
            @ApiImplicitParam(name = "fields", value = "域,空值默认域", required = false, dataType = "string", paramType = "form"),
            // 查询对应的 标签的值
            @ApiImplicitParam(name = "tags", value = "标签名称,查询数据对应的标签值,多个以,号分开", required = false, dataType = "int", paramType = "form"),
            // 查询数量 参数
            @ApiImplicitParam(name = "limit", value = "查询数量", required = false, dataType = "int", paramType = "form"), //标签参数
            // 查询数量 参数
            @ApiImplicitParam(name = "order", value = "排序参数,基于时间,默认1(1-升序，2-降序)", required = false, dataType = "int", paramType = "form"), @ApiImplicitParam(name = "tags", value = "标签", required = false, dataType = "string", paramType = "form"),
           /* ===过滤器 参数  start===*/
            @ApiImplicitParam(name = "filterStartTimeType", value = "起始时间类型,默认为3(1年,2月,3日,4时,5分,6秒)", required = false, dataType = "int", paramType = "form"),
            @ApiImplicitParam(name = "filterStartTimeValue", value = "起始时间值,默认为1", required = false, dataType = "int", paramType = "form"),
            @ApiImplicitParam(name = "filterEndTimeType", value = "截止时间类型(1年,2月,3日,4时,5分,6秒)", required = false, dataType = "int", paramType = "form"),
            @ApiImplicitParam(name = "filterEndTimeValue", value = "截止时间值", required = false, dataType = "int", paramType = "form"),
            //根据标签过滤
            @ApiImplicitParam(name = "tagKeys", value = "标签Key值,多个key以,号分开", required = false, dataType = "string", paramType = "form"),
            @ApiImplicitParam(name = "tagValues", value = "标签value值,多个key以|号分开,key对应的多个值以,号分开", required = false, dataType = "string", paramType = "form"),
            @ApiImplicitParam(name = "tagFilterType", value = "标签过滤类型(1-in,2-notIn,3-like匹配),多个key以,号分开", required = false, dataType = "String", paramType = "form"),
            //根据  域值 过滤
            @ApiImplicitParam(name = "fieldNames", value = "域名称,多个以,号分开", required = false, dataType = "string", paramType = "form"),
            @ApiImplicitParam(name = "fieldValues", value = "域的过滤值,多个以,号分开", required = false, dataType = "string", paramType = "form"),
            @ApiImplicitParam(name = "fieldTypes", value = "域值过滤类型(1-等于,2不等于,3大于,4小于,5大于等于,6小于等于),多个key以,号分开", required = false, dataType = "String", paramType = "form"),
     /* ===过滤器 参数  end===*/
            // 插值,空值补全 参数
            @ApiImplicitParam(name = "fillType", value = "空值补全类型(1-线性补值,2-按前一个值补值,3-固定值补值)", required = false, dataType = "int", paramType = "form"),
            @ApiImplicitParam(name = "fillInterval", value = "补值间隔", required = false, dataType = "string", paramType = "form"),
            @ApiImplicitParam(name = "fillMaxWriteInterval", value = "补值最大写入间隔", required = false, dataType = "string", paramType = "form"),
            @ApiImplicitParam(name = "fillFixedValue", value = "固定值(固定补值传入)", required = false, dataType = "string", paramType = "form"),
            @ApiImplicitParam(name = "fillFixedValueType", value = "固定值类型,默认3(1-整数,2-小数,3-字符串)", required = false, dataType = "int", paramType = "form"),
            //根据标签值分组
            @ApiImplicitParam(name = "groupTagKeys", value = "标签key值(基于查询的域并根据标签值分组),以|区别查询的域,以,区别key", required = false, dataType = "string", paramType = "form"),
            //聚合函数参数
            @ApiImplicitParam(name = "aggregatorsType", value = "聚合函数类型,以|区别查询的域", required = false, dataType = "string", paramType = "form"),
            @ApiImplicitParam(name = "aggregatorsSamplingType", value = "聚合函数时间类型(1年,2月,3日,4时,5分,6秒),以|区别查询的域", required = false, dataType = "string", paramType = "form"),
            @ApiImplicitParam(name = "aggregatorsSamplingValue", value = "聚合函数时间值,以|区别查询的域", required = false, dataType = "string", paramType = "form"),
            @ApiImplicitParam(name = "aggregatorsValue", value = "聚合函数所需传值,以|区别查询的域(null字符串过滤)", required = false, dataType = "string", paramType = "form"),
            // 页码 参数
            @ApiImplicitParam(name = "marker", value = "页码参数(上一次查询返回的nextMarker值,判断truncated是否为true)", required = false, dataType = "string", paramType = "form"),
            @ApiImplicitParam(name = "dataType", value = "数据返回类型(1-全数据,2-标签Map集合(默认))", required = false, dataType = "string", paramType = "form"),
    })
    public R readDataPoint(Integer address,Integer dataType, String metric, String fields, String marker, Integer limit, Integer order, String tags
            , Integer filterStartTimeType, Integer filterStartTimeValue, Integer filterEndTimeType, Integer filterEndTimeValue
            , String tagKeys, String tagValues, String tagFilterType
            , String fieldNames, String fieldValues, String fieldTypes
            , Integer fillType, String fillInterval, String fillMaxWriteInterval, String fillFixedValue, Integer fillFixedValueType
            , String groupTagKeys
            , String aggregatorsType, String aggregatorsSamplingType, String aggregatorsSamplingValue, String aggregatorsValue) {
        if (StringUtils.isBlank(metric)) {
            return R.error(400, "参数metric不能为空");
        }
        // 设置查询域
        List<String> fieldList = null;
        if (StringUtils.isNotBlank(fields)) {
            fieldList = Arrays.asList(fields.split(","));
        }
        // 设置 过滤器 对象
        String startTime = null;// 起始时间
        String endTime = null;// 截止时间
        List<FieldFilter> fieldFilters = new ArrayList<FieldFilter>();
        List<TagFilter> tagFilters = new ArrayList<TagFilter>();
        //时间过滤
        if (filterStartTimeType != null && filterStartTimeValue != null) {
            startTime = TsdbReqParamUtil.convertRequestRelativeTimeStr_BaiDu(filterStartTimeType, filterStartTimeValue);
        }
        if (filterEndTimeType != null && filterEndTimeValue != null) {
            endTime = TsdbReqParamUtil.convertRequestRelativeTimeStr_BaiDu(filterEndTimeType, filterEndTimeValue);
        }
        //标签过滤
        if (StringUtils.isNotBlank(tagKeys) && StringUtils.isNotBlank(tagValues) && StringUtils.isNotBlank(tagFilterType)) {
            String[] tagKeyArr = tagKeys.split(",");
            String[] tagValueArr = tagValues.split("\\|");
            String[] tagTypeArr = tagFilterType.split(",");
            if (tagKeyArr.length == tagValueArr.length && tagKeyArr.length == tagTypeArr.length) {
                for (int x = 0; x < tagKeyArr.length; x++) {
                    if (tagTypeArr[x].equals("1")) {// in
                        String[] tagKeyValueArr = tagValueArr[x].split(",");
                        tagFilters.add(TsdbReqParamUtil.createFiltersTagFilter_in(tagKeyArr[x], Arrays.asList(tagKeyValueArr)));
                    }
                    if (tagTypeArr[x].equals("2")) {// notIn
                        String[] tagKeyValueArr = tagValueArr[x].split(",");
                        tagFilters.add(TsdbReqParamUtil.createFiltersTagFilter_notIn(tagKeyArr[x], Arrays.asList(tagKeyValueArr)));
                    }
                    if (tagTypeArr[x].equals("3")) {// like匹配
                        tagFilters.add(TsdbReqParamUtil.createFiltersTagFilter_like(tagKeyArr[x], tagValueArr[x]));
                    }
                }
            } else {
                return R.error(400, "标签过滤参数出错");
            }
        }
        //域值过滤
        if (StringUtils.isNotBlank(fieldNames) && StringUtils.isNotBlank(fieldValues) && StringUtils.isNotBlank(fieldTypes)) {
            String[] fieldNameArr = fieldNames.split(",");
            String[] fieldValueArr = fieldValues.split(",");
            String[] fieldTypeArr = fieldTypes.split(",");
            if (fieldNameArr.length == fieldValueArr.length && fieldNameArr.length == fieldTypeArr.length) {
                for (int x = 0; x < fieldNameArr.length; x++) {
                    String fieldFilterValue = TsdbReqParamUtil.createFiltersValue(fieldTypeArr[x], fieldValueArr[x]);
                    if (fieldFilterValue != null) {
                        fieldFilters.add(TsdbReqParamUtil.createFieldFilter(fieldNameArr[x], fieldFilterValue));
                    }
                }
            } else {
                return R.error(400, "域值过滤参数出错");
            }
        }
        Filters filters = TsdbReqParamUtil.createFilters(startTime, endTime, fieldFilters, tagFilters);

        //插值对象
        Fill fillParam = null;
        if (fillType != null && StringUtils.isNotBlank(fillInterval)) {
            if (fillType == 1) {
                fillParam = TsdbReqParamUtil.createFill("Linear", fillInterval, fillMaxWriteInterval, null);
            }
            if (fillType == 2) {
                fillParam = TsdbReqParamUtil.createFill("Previous", fillInterval, fillMaxWriteInterval, null);
            }
            if (fillType == 3) {
                if (StringUtils.isNotBlank(fillFixedValue)) {
                    Object fillFixedValueObj = null;
                    if (fillFixedValueType == null) {
                        fillFixedValueType = 3;
                    }
                    switch (fillFixedValueType) {
                        case 1:
                            fillFixedValueObj = Long.parseLong(fillFixedValue);
                            break;
                        case 2:
                            fillFixedValueObj = Double.parseDouble(fillFixedValue);
                            break;
                        case 3:
                            fillFixedValueObj = fillFixedValue;
                            break;
                        default:
                            fillFixedValueObj = fillFixedValue;
                    }
                    fillParam = TsdbReqParamUtil.createFill("Fixed", fillInterval, fillMaxWriteInterval, fillFixedValueObj);
                }
            }
        }
        //查询对应的标签值
        List<String> tagsParam = null;
        if (StringUtils.isNotBlank(tags)) {
            tagsParam = Arrays.asList(tags.split(","));
        }
        //分组对象
        List<GroupBy> groupBysParam = null;
        if (StringUtils.isNotBlank(groupTagKeys)) {
            String[] groupTagKeyArr = groupTagKeys.split("\\|");
            if (fieldList == null || fieldList.isEmpty()) {//如果查询的域为默认域
                if (groupTagKeyArr.length != 1) {
                    return R.error(400, "参数groupTagKeys格式错误,和参数fields不匹配");
                }
            } else {
                if (groupTagKeyArr.length != fieldList.size()) {
                    return R.error(400, "参数groupTagKeys格式错误,和参数fields不匹配");
                }
            }
            for (int x = 0; x < groupTagKeyArr.length; x++) {
                groupBysParam = new ArrayList<GroupBy>();
                groupBysParam.add(new GroupBy().withName("Tag").withTags(Arrays.asList(groupTagKeyArr[x].split(","))));
            }
        }
        //聚合函数
        List<Aggregator> aggregatorsParam = null;
        if (StringUtils.isNotBlank(aggregatorsType)) {
            String[] aggregatorsTypeArr = aggregatorsType.split("\\|");
            String[] aggregatorsSamplingTypeArr = null;
            String[] aggregatorsSamplingValueArr = null;
            String[] aggregatorsValueArr = null;
            if (StringUtils.isNotBlank(aggregatorsSamplingType) && StringUtils.isNotBlank(aggregatorsSamplingValue)) {
                aggregatorsSamplingTypeArr = aggregatorsSamplingType.split("\\|");
                aggregatorsSamplingValueArr = aggregatorsSamplingValue.split("\\|");
                if (aggregatorsSamplingTypeArr.length != aggregatorsSamplingValueArr.length) {
                    return R.error(400, "聚合函数参数出错");
                }
                if (aggregatorsTypeArr.length != aggregatorsSamplingTypeArr.length) {
                    return R.error(400, "聚合函数参数出错");
                }
            }
            if (StringUtils.isNotBlank(aggregatorsValue)) {//函数值
                aggregatorsValueArr = aggregatorsValue.split("\\|");
                if (aggregatorsTypeArr.length != aggregatorsValueArr.length) {
                    return R.error(400, "聚合函数参数出错");
                }
            }
            if (aggregatorsTypeArr.length == 1) {
                if (fieldList != null && fieldList.size() != 1) {
                    return R.error(400, "聚合函数参数和参数fields匹配出错");
                }
            }
            if (aggregatorsTypeArr.length > 1) {
                if (fieldList == null || aggregatorsTypeArr.length != fieldList.size()) {
                    return R.error(400, "聚合函数参数和参数fields匹配出错");
                }
            }
            aggregatorsParam = new ArrayList<Aggregator>();
            for (int x = 0; x < aggregatorsTypeArr.length; x++) {
                String aggregatorName = TsdbReqParamUtil.returnAggregatorNameStrByNum(aggregatorsTypeArr[x]);
                String sampling = null;
                if (aggregatorsSamplingTypeArr != null) {
                    sampling = aggregatorsSamplingValueArr[x] + TsdbReqParamUtil.returnTimeStrByNum(aggregatorsSamplingTypeArr[x]);
                }
                Aggregator aggregator = TsdbReqParamUtil.createAggregator(aggregatorName, sampling);
                try {
                    if (aggregatorName.equals("Scale")) {//求倍数
                        aggregator.withFactor(Double.parseDouble(aggregatorsValueArr[x]));
                    }
                    if (aggregatorName.equals("Div")) {//求除数
                        aggregator.withDivisor(Double.parseDouble(aggregatorsValueArr[x]));
                    }
                } catch (NullPointerException ex) {
                    return R.error(400, "聚合函数参数aggregatorsValue出错");
                } catch (ArrayIndexOutOfBoundsException ex) {
                    return R.error(400, "聚合函数参数aggregatorsValue出错");
                } catch (NumberFormatException ex) {
                    return R.error(400, "聚合函数参数aggregatorsValue出错");
                }
                aggregatorsParam.add(aggregator);
            }
        }
        //排序参数
        String orderParam = null;
        if (order == null || order != 1) {
            orderParam = "Desc";
        }
        if (order != null && order == 1) {
            orderParam = "Asc";
        }
        //根据请求参数生成Query对象
        Query query = TsdbReqParamUtil.createFieldsQuery(metric, filters, fieldList, fillParam, marker, tagsParam, groupBysParam, limit, aggregatorsParam, orderParam);
        if (dataType != null && dataType == 1) {
            QueryDatapointsResponse datapointsResponse = tsdbService.readDataPointResponse( address,Arrays.asList(query));
            return R.ok().put("data", datapointsResponse);
        } else {
            return R.ok().put("data", tsdbService.readDataPointResult( address,Arrays.asList(query)));
        }
    }


    @ApiOperation(value = "写入数据点-用于生成 多域 度量")
    @RequestMapping(value = "/writeDatapoints", method = RequestMethod.POST)
    @ApiImplicitParams({
            @ApiImplicitParam(name = "address", value = "查询地址(1-广州默认，2-北京)", required = false, dataType = "int", paramType = "form"),
            @ApiImplicitParam(name = "metric", value = "度量名称", required = true, dataType = "string", paramType = "form"),
            @ApiImplicitParam(name = "flieds", value = "域名称,多个以,分开", required = true, dataType = "string", paramType = "form"),
            @ApiImplicitParam(name = "tagJsonStr", value = "标签JsonStr", required = false, dataType = "string", paramType = "form"),
    })
    public R writeDatapoints(Integer address,String metric,String flieds,String tagJsonStr) {
        Assert.parameterIsBlank(metric,"度量名称不能为空");
        Assert.parameterIsBlank(flieds,"域名称不能为空");
        String[] fliedArr = flieds.split(",");
        Map<String,String> tagMap = new HashMap<String, String>();
        if(StringUtils.isNotBlank(tagJsonStr)){;
            tagMap= JSONObject.parseObject(tagJsonStr, new TypeReference<Map<String, String>>(){});
        }
        List<Datapoint> datapoints = new ArrayList<Datapoint>();
        for(int x=0;x<fliedArr.length;x++){
            Datapoint datapoint2 = new Datapoint();
            datapoint2.withMetric(metric);
            datapoint2.withField(fliedArr[x]);
            datapoint2.withTags(tagMap);
            datapoint2.addLongValue(System.currentTimeMillis(),0L);
            datapoints.add(datapoint2);
        }
        return R.ok().put("data", tsdbService.writeDatapoints(address,datapoints));
    }
    @ApiOperation(value = "查询数据点-单域")
    @RequestMapping(value = "/readSignDataPoint", method = RequestMethod.POST)
    @ApiImplicitParams({
            @ApiImplicitParam(name = "address", value = "查询地址(1-广州默认，2-北京)", required = false, dataType = "int", paramType = "form"),
            @ApiImplicitParam(name = "metric", value = "度量,多度量以,分开,默认查所有", required = false, dataType = "int", paramType = "form"), //标签参数
            // 查询数量 参数
            @ApiImplicitParam(name = "limit", value = "查询数量", required = false, dataType = "int", paramType = "form"), //标签参数
            // 页码 参数
            @ApiImplicitParam(name = "marker", value = "页码参数(上一次查询返回的nextMarker值,判断truncated是否为true)", required = false, dataType = "string", paramType = "form"),

            @ApiImplicitParam(name = "filterStartTime", value = "起始时间,yyyyMMddHHmmss", required = false, dataType = "string", paramType = "form"),
            @ApiImplicitParam(name = "filterEndTime", value = "截止时间，yyyyMMddHHmmss", required = false, dataType = "string", paramType = "form"),
            //根据标签过滤
            @ApiImplicitParam(name = "tagKeys", value = "标签Key值,多个key以,号分开", required = false, dataType = "string", paramType = "form"),
            @ApiImplicitParam(name = "tagValues", value = "标签value值,多个key以|号分开,key对应的多个值以,号分开", required = false, dataType = "string", paramType = "form"),
            @ApiImplicitParam(name = "tagFilterType", value = "标签过滤类型(1-in,2-notIn,3-like匹配),多个key以,号分开", required = false, dataType = "String", paramType = "form"),
    })
    public R readSignDataPoint(Integer address,String metric,String marker,Integer limit
            ,String filterStartTime,String filterEndTime
            , String tagKeys, String tagValues, String tagFilterType){
        List<Query> queryList = new ArrayList<Query>();
        List<String> metricList = null;
        if(StringUtils.isNotBlank(metric)){
            metricList =Arrays.asList( metric.split(",")) ;
        }else{
            metricList = tsdbService.readMetricList(address,null);
        }

        Long absoStartTime = null;
        Long absoEndTime = null;
        if(StringUtils.isNotBlank(filterStartTime)){
            absoStartTime = DateUtil.stringToDate(filterStartTime).getTime();
        }
        if(StringUtils.isNotBlank(filterEndTime)){
            absoEndTime = DateUtil.stringToDate(filterEndTime).getTime();
        }
        List<TagFilter> tagFilters = new ArrayList<TagFilter>();
        //标签过滤
        if (StringUtils.isNotBlank(tagKeys) && StringUtils.isNotBlank(tagValues) && StringUtils.isNotBlank(tagFilterType)) {
            String[] tagKeyArr = tagKeys.split(",");
            String[] tagValueArr = tagValues.split("\\|");
            String[] tagTypeArr = tagFilterType.split(",");
            if (tagKeyArr.length == tagValueArr.length && tagKeyArr.length == tagTypeArr.length) {
                for (int x = 0; x < tagKeyArr.length; x++) {
                    if (tagTypeArr[x].equals("1")) {// in
                        String[] tagKeyValueArr = tagValueArr[x].split(",");
                        tagFilters.add(TsdbReqParamUtil.createFiltersTagFilter_in(tagKeyArr[x], Arrays.asList(tagKeyValueArr)));
                    }
                    if (tagTypeArr[x].equals("2")) {// notIn
                        String[] tagKeyValueArr = tagValueArr[x].split(",");
                        tagFilters.add(TsdbReqParamUtil.createFiltersTagFilter_notIn(tagKeyArr[x], Arrays.asList(tagKeyValueArr)));
                    }
                    if (tagTypeArr[x].equals("3")) {// like匹配
                        tagFilters.add(TsdbReqParamUtil.createFiltersTagFilter_like(tagKeyArr[x], tagValueArr[x]));
                    }
                }
            } else {
                return R.error(400, "标签过滤参数出错");
            }
        }
        Filters filters = TsdbReqParamUtil.createAbsoFilters_SignFiled(absoStartTime, absoEndTime, null, tagFilters);
        if(metricList!=null && !metricList.isEmpty()){
            int forSize=metricList.size();
            if(metricList.size()>7){
                forSize = 7;//最多查询7条数据
            }
            for(int x=0;x<forSize;x++){
                Query query = TsdbReqParamUtil.createFieldsQuery(metricList.get(x), filters, null, null, marker, null, null, limit, null, null);
                queryList.add(query);
            }
        }
        if(!queryList.isEmpty()){
            return R.ok().put("data", tsdbService.readDataPointResult(address,queryList));
        }else{
            return R.ok().put("data", new ArrayList<Result>());
        }

    }


}
