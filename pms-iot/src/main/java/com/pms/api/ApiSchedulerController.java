package com.pms.api;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.pms.entity.CameraUserInformation;
import com.pms.entity.EqCamera;
import com.pms.exception.RRException;
import com.pms.service.ICameraUserInformationService;
import com.pms.service.IEqCameraService;
import com.pms.util.FluoriteCloudUtile;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

/**
 * Created by hm on 2018/1/11.
 */
@Component
public class ApiSchedulerController {
//    @Autowired
//    private ICameraUserInformationService service;
//    @Autowired
//    private IEqCameraService eqCameraService;
//
//
//    /**
//     * 220180316
//     * 定时器更新摄像头
//     * @throws Exception
//     */
//    @Scheduled(cron="0 0 12 * * ?")
//    public void updateEqCanmeraKey() throws Exception {
//        SimpleDateFormat format =  new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
//        Date timeMp=new Date();
//        String tim= format.format(timeMp);
//        //更新商城摄像头
//        List<EqCamera> listCamera = eqCameraService.findEqCameraByExpireTime(tim,500);
//        for (EqCamera eqCamera : listCamera) {
//            if(eqCamera==null){
//                continue;
//            }
//            CameraUserInformation cameraUser =service.selectById(eqCamera.getCameraUserId());
//            if(cameraUser !=null){
//                JSONObject json = FluoriteCloudUtile.fluoriteCloud1(cameraUser.getAppkey(),cameraUser.getAppsecret());
//                String code = json.getString("code");
//                String accessToken = "";
//                String expireTime = "";
//                if (code.equals("200")) {
//                    String data = json.getString("data");
//                    if (StringUtils.isNotEmpty(data)) {
//                        JSONObject jsonResult = JSON.parseObject(data);
//                        accessToken = jsonResult.getString("accessToken");// token码
//                        expireTime = jsonResult.getString("expireTime");// 结束时间
//                    }
//                }
//                if (StringUtils.isNotBlank(accessToken) && StringUtils.isNotBlank(expireTime)) {
//                    eqCamera.setToken(accessToken);// 保存token码
//                    Long time=new Long(expireTime);
//                    String d = format.format(time);//时间戳转换成标准时间
//                    Date  e = format.parse(d);     //标准时间转换成国际时间
//                    Calendar cal = Calendar.getInstance();//使用默认时区和语言环境获得一个日历
//                    cal.setTime(e);
//                    cal.add(Calendar.DATE, -1);  //结束时间减1天
//                    eqCamera.setExpireTime(format.parse(format.format(cal.getTime())));
//                    eqCamera.setCurrentdateTime(new Date());
//                    eqCameraService.updateById(eqCamera);
//                }else{
//                    throw new RRException("获取摄像头Tocken失败");
//                }
//            }
//        }
//    }


}