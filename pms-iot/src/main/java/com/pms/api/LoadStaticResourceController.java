package com.pms.api;

import org.springframework.ui.Model;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * Created by Administrator on 2018/4/9.
 */
@Controller(value = "/resource/")
public class LoadStaticResourceController {

    @RequestMapping(value ="/getCurveView",method = RequestMethod.GET)
    public String getUsers(Model model,String deviceName) {
        model.addAttribute("deviceName", deviceName);
        return "curveView";
    }
}
