package com.pms.api;
import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.pms.controller.BaseController;
import com.pms.entity.AreaCommunity;
import com.pms.entity.Community;
import com.pms.exception.R;
import com.pms.service.IAreaCommunityService;
import com.pms.service.ICommunityService;
import com.pms.validator.Assert;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Map;

/**
 * Created by hm on 2018/3/19.
 */
@RestController
@RequestMapping(value = "/pms/apiCommunity")
@Api(value="App根据区域地址查询社区",description = "App根据区域地址查询社区")
public class ApiCommunityController extends BaseController {
    @Autowired
    ICommunityService communityService;

    @Autowired
    IAreaCommunityService areaCommunityService;
    /**
     * 根据区级地址查询社区
     * @param areaGb
     * @return
     */
    @ApiOperation(value = "根据区级地址查询社区")
    @GetMapping(value = "/queryByareaGb")
    @ApiImplicitParams({
            @ApiImplicitParam(name="areaGb",value="所属区域编号",required = true,dataType = "Integer",paramType="form")
    })
    public R queryByareaGb(Integer areaGb){
        Assert.isNull(areaGb,"所属区域编号不能为空");
        List<Community> community=communityService.selectList(new EntityWrapper<Community>().eq("area_gb",areaGb));
        return R.ok().put("data",community);
    }

    /**
     * 查询开通小区的市级地址
     * @return
     */
    @GetMapping("/getAreaCnAndCommunity")
    @ApiOperation("查询开通小区的市级地址")
    @ApiImplicitParams({
    })
    public R getAreaCnAndCommunity(){
        List<Map<String,Object>> community=areaCommunityService.selectByAreaCommunity();
        return R.ok().put("data",community);
    }



    /**
     * 查询开通小区的市级地址
     * @return
     */
    @GetMapping("/selectAreaGbAndCommunity")
    @ApiOperation("根据市编码查询小区")
    @ApiImplicitParams({
            @ApiImplicitParam(name="areaGb",value="所属区域编号",required = true,dataType = "Integer",paramType="form"),
            @ApiImplicitParam(name="communityName",value="小区名称",required = true,dataType = "String",paramType="form"),
    })
    public R selectAreaGbAndCommunity(Integer areaGb,String communityName){
        Assert.isNull(areaGb,"所属区域编号不能为空");
        List<Map<String,Object>> community=areaCommunityService.selectAreaGbAndCommunity(new EntityWrapper<AreaCommunity>().eq("ac.county_gb",areaGb).like("c.community_name",communityName));
        return R.ok().put("data",community);
    }

}
