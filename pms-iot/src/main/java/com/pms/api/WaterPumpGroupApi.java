package com.pms.api;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.mapper.Wrapper;
import com.pms.controller.BaseController;
import com.pms.entity.WaterPumpGroup;
import com.pms.exception.R;
import com.pms.rpc.UserService;
import com.pms.service.IWaterPumpGroupService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;


/**
 * 水泵组相关接口
 */
@RestController
@Api(value = "水泵组Api接口", description = "水泵组Api接口")
@RequestMapping(value = "waterPumpGroup", method = RequestMethod.POST)
public class WaterPumpGroupApi extends BaseController {
    @Autowired
    IWaterPumpGroupService waterPumpGroupService;
    @Autowired
    UserService userService;

    @ApiOperation(value = "根据水泵组id查询对应的数据")
    @RequestMapping(value = "/queryWaterPumpGroupById", method = RequestMethod.POST)
    @ApiImplicitParams({
            @ApiImplicitParam(name = "pumpId", value = "水泵id", required = true, dataType = "Long", paramType = "form")
    })
    public R queryWaterPumpById(Long pumpId) {
        Wrapper<WaterPumpGroup> wp = new EntityWrapper<WaterPumpGroup>();
        wp.eq("pump_id",pumpId);
        WaterPumpGroup waterPumpGroup = waterPumpGroupService.selectOne(wp);
        return R.ok().put("waterPumpGroup",waterPumpGroup);
    }


//    @ApiOperation(value = "根据用户id查询对应的数据")
//    @RequestMapping(value = "/queryWaterPumpsByUserId", method = RequestMethod.POST)
//    @ApiImplicitParams({
//            @ApiImplicitParam(name = "userId", value = "用户id", required = true, dataType = "Long", paramType = "form")
//    })
//    public R queryWaterPumpsByUserId(Long userId) {
//        parameterIsNull(userId,"用户ID不能为空");
//        BaseUserInfo baseUserInfo = userService.getUserByUserId(userId);
//        parameterIsNull(baseUserInfo,"没有此用户");
//        Long companyId = baseUserInfo.getCompanyId();
//        Long communityId = baseUserInfo.getCommunityId();
//        Long areaId = baseUserInfo.getAreaId();
//        Wrapper<WaterPumpGroup> wp = new EntityWrapper<WaterPumpGroup>();
//        if(communityId!=null){
//            wp.eq("community_id",communityId);
//        }else{
//            if(areaId!=null&&companyId!=null){
//                wp.eq("company_id",companyId);
//                wp.eq("area_id",areaId);
//            }else if(companyId!=null&&areaId==null){
//                wp.eq("company_id",companyId);
//            }else if(companyId==null&&areaId!=null){
//                throw new RuntimeException("数据添加有误");
//            }
//        }
//        List<WaterPumpGroup> waterPumpList = waterPumpService.selectList(wp);
//        return R.ok().put("waterPumpList",waterPumpList);
//
//    }
}
