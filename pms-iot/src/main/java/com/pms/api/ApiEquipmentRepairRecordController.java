package com.pms.api;

import com.pms.controller.BaseController;
import com.pms.entity.BaseUserInfo;
import com.pms.entity.EquipmentRepairRecord;
import com.pms.exception.R;
import com.pms.rpc.UserService;
import com.pms.service.IEquipmentRepairRecordService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Date;

/**
 * Created by lk on 2018/4/25
 */
@RestController
@RequestMapping("apiEquipmentRepairRecord")
@Api(value="设备报修记录",description = "设备报修记录")
public class ApiEquipmentRepairRecordController extends BaseController {
    @Autowired
    IEquipmentRepairRecordService equipmentRepairRecordService;

    @Autowired
    UserService userService;

    /**
     * 设备报修记录添加
     */
    @RequestMapping(value="/addEquipmentRepairRecord")
    @ApiOperation(value="设备报修记录添加", notes="设备报修记录添加")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "memberId", value = "会员id", required = true, dataType = "Long", paramType = "form"),
            @ApiImplicitParam(name = "repairPhone", value = "报修人手机号码", required = true, dataType = "String", paramType = "form"),
            @ApiImplicitParam(name = "eqModel", value = "设备型号", required = true, dataType = "String", paramType = "form"),
            @ApiImplicitParam(name = "eqClassId", value = "分类id", required = true, dataType = "String", paramType = "form"),
            @ApiImplicitParam(name = "eqName", value = "设备名称", required = true, dataType = "Long", paramType = "form"),
            @ApiImplicitParam(name = "problemDescr", value = "问题描述", required = true, dataType = "String", paramType = "form"),
            @ApiImplicitParam(name = "address", value = "设备具体地址", required = true, dataType = "String", paramType = "form")
    })
    public R addEquipmentRepairRecord(Long memberId,String repairPhone,String eqModel,Long eqClassId,String eqName,String problemDescr,String address){
        parameterIsNull(memberId,"用户id不存在");
        BaseUserInfo baseUserInfo = userService.getUserByUserId(memberId);
        parameterIsNull(baseUserInfo,"用户不存在");
        parameterIsBlank(repairPhone,"报修人手机号码不能为空");
        parameterIsBlank(eqModel,"设备型号不能为空");
        parameterIsNull(eqClassId,"设备分类id不能为空");
        parameterIsBlank(eqName,"设备名称不能为空");
        parameterIsBlank(problemDescr,"问题描述不能为空");
        parameterIsBlank(address,"具体地址不能为空");
        EquipmentRepairRecord equipmentRepairRecord = new EquipmentRepairRecord();
        equipmentRepairRecord.setRepairMemberId(memberId);
        equipmentRepairRecord.setRepairPhone(repairPhone);
        equipmentRepairRecord.setEqModel(eqModel);
        equipmentRepairRecord.setEqClassId(eqClassId);
        equipmentRepairRecord.setEqName(eqName);
        equipmentRepairRecord.setProblemDescr(problemDescr);
        equipmentRepairRecord.setAddress(address);
        equipmentRepairRecord.setCreateTime(new Date());
        equipmentRepairRecord.setIsHandle(1);
        equipmentRepairRecordService.insert(equipmentRepairRecord);
        return R.ok();
    }
    /**
     * 设备报修处理
     */
    @RequestMapping(value="/handleEquipmentRepairRecord")
    @ApiOperation(value="设备报修处理", notes="设备报修处理")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "id", value = "设备报修id", required = true, dataType = "Long", paramType = "form"),
            @ApiImplicitParam(name = "memberId", value = "c处理人会员id", required = true, dataType = "Long", paramType = "form"),
            @ApiImplicitParam(name = "handlePhone", value = "处理人手机号码", required = true, dataType = "String", paramType = "form"),
            @ApiImplicitParam(name = "handleReply", value = "处理回复", required = false, dataType = "String", paramType = "form")
    })
    public R handleEquipmentRepairRecord(Long id,Long memberId,String handlePhone,String handleReply){
        parameterIsNull(id,"设备报修记录id不能为空");
        EquipmentRepairRecord equipmentRepairRecord = equipmentRepairRecordService.selectById(id);
        parameterIsNull(equipmentRepairRecord,"没有对应的设备报修记录");
        parameterIsNull(memberId,"处理人会员id不能为空");
        BaseUserInfo baseUserInfo = userService.getUserByUserId(memberId);
        parameterIsNull(baseUserInfo,"处理人用户信息不存在");
        parameterIsBlank(handlePhone,"处理人手机号码不能为空");
        equipmentRepairRecord.setIsHandle(2);
        equipmentRepairRecord.setHandleTime(new Date());
        equipmentRepairRecord.setHandleMemberId(memberId);
        equipmentRepairRecord.setHandlePhone(handlePhone);
        equipmentRepairRecord.setHandleReply(handleReply);
        equipmentRepairRecordService.updateById(equipmentRepairRecord);
        return R.ok();
    }
}
