package com.pms.api;
import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.mapper.Wrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.pms.controller.BaseController;
import com.pms.entity.BaseUserInfo;
import com.pms.entity.EqCamera;
import com.pms.entity.WaterPumpHouse;
import com.pms.exception.R;
import com.pms.exception.RRException;
import com.pms.rpc.UserService;
import com.pms.rpc.userCommunityService;
import com.pms.service.ICameraUserInformationService;
import com.pms.service.IEqCameraService;
import com.pms.service.IWaterPumpHouseService;
import com.pms.util.FluoriteCloudUtile;
import com.pms.validator.Assert;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Map;
/**
 * Created by hm on 2018/3/15.
 */
@RestController
//@RequestMapping(value = "/pms/apiEqCamera")
//@Api(value="App摄像头接口",description = "App摄像头接口")
public class ApiEqCameraController extends BaseController {
    @Autowired
    private ICameraUserInformationService service;
    @Autowired
    private IEqCameraService eqCameraService;
    @Autowired
    private IWaterPumpHouseService waterPumpHouseService;
    @Autowired
    private UserService userService;
    @Autowired
    private com.pms.cache.utils.RedisCacheUtil RedisCacheUtil;
    @Autowired
    private userCommunityService communityService;
//    /**
//     * 分页查询摄像头列表
//     * @param waterPumpHouseId
//     * @param userId
//     * @param pageNum
//     * @param pageSize
//     * @return
//     */
//    @PostMapping("/page/camera")
//    @ApiOperation(value = "分页查询摄像头列表")
//    @ApiImplicitParams({
//            @ApiImplicitParam(name = "waterPumpHouseId", value = "水泵房ID", required = true, dataType = "Long", paramType = "form"),
//            @ApiImplicitParam(name = "userId", value = "用户ID", required = true, dataType = "Long", paramType = "form"),
//            @ApiImplicitParam(name = "pageNum", value = "分页页数（默认为1）", required = false, dataType = "String", paramType = "form"),
//            @ApiImplicitParam(name = "pageSize", value = "分页条数（默认为10）", required = false, dataType = "String", paramType = "form")
//    })
//    public R pageCamera(Long waterPumpHouseId, Long userId, String pageNum, String pageSize){
//        Assert.isNull(waterPumpHouseId,"店铺Id不能为空");
//        Assert.isNull(userId,"会员Id不能为空");
//        if ( pageSize == null ){ pageSize="10";}
//        if ( pageNum == null   ){pageNum="1";}
//        Page<EqCamera> pages = new Page<EqCamera>(Integer.parseInt(pageNum),Integer.parseInt(pageSize));
//        WaterPumpHouse waterPumpHouse= waterPumpHouseService.selectById(waterPumpHouseId);
//        Assert.isNull(waterPumpHouse,"该水泵房信息不存在");
//        Map<String, Object>  user= communityService.getUserCommunity(userId,waterPumpHouse.getCommunityId());
//        if (null==user||user.isEmpty()){
//            throw new RRException("对不起你没有查询当前水泵房摄像头的权限",400);
//        }
//        Wrapper<EqCamera> eq=new EntityWrapper<EqCamera>();
//        eq.eq("water_pump_house_id",waterPumpHouseId);
//            Page<Map<String, Object>>  arrPage =eqCameraService.selectByISMapsPage(pages,eq);
//            List<Map<String, Object>> data =  arrPage.getRecords();
//            for (int x=0;x< data.size();x++){
//                capture(Long.parseLong(data.get(x).get("id").toString()));
//            }
//            return R.ok().put("data",arrPage);
//    }
//
//
//
//
//
//    public R capture(Long eqId){
//        EqCamera eqCamera=eqCameraService.selectById(eqId);
//        Assert.isNull(eqCamera," 该摄像头信息不存在");
//        String  redisPhoto =RedisCacheUtil.getVlueStr("PMS_PhotoCapture"+eqId);
//        if (redisPhoto == null){//如果缓存对象过时
//            JSONObject capture = FluoriteCloudUtile.capture(eqCamera.getKey(),eqCamera.getEquipmentId(),eqCamera.getToken());
//            if(capture.getString("code").equals("200")){
//                JSONObject data= capture.getJSONObject("data");
//                Object img=data.get("picUrl");
//                eqCamera.setPicurl(img.toString());
//                eqCameraService.updateById(eqCamera);
//                RedisCacheUtil.saveAndUpdate("PMS_PhotoCapture"+eqId, img,10);
//                return  R.ok().put("data",img);
//            }else{
//                throw new RRException(capture.toJSONString(),400);
//            }
//        }else{
//            return  R.ok().put("data",redisPhoto);
//        }
//    }


}

