package com.pms.web;

import com.pms.controller.BaseController;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;


/**
 * <p>
 * 报警类型表 前端控制器
 * </p>
 *
 * @author ljb
 * @since 2018-08-20
 */
@Controller
@RequestMapping("/pms/iotAlarmType")
public class IotAlarmTypeController extends BaseController {
	
}
