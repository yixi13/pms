package com.pms.web;
import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.mapper.Wrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.pms.controller.BaseController;
import com.pms.entity.BaseUserInfo;
import com.pms.entity.Community;
import com.pms.entity.Company;
import com.pms.exception.R;
import com.pms.result.TableResultResponse;
import com.pms.rpc.UserService;
import com.pms.service.ICompanyService;
import com.pms.validator.Assert;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import java.util.Date;
import java.util.List;
/**
 * <p>
 * 水泵所属公司表 前端控制器
 * </p>
 * @author lk
 * @since 2017-12-25
 */
@RestController
@RequestMapping("/pms/company")
public class CompanyController extends BaseController {
    @Autowired
    ICompanyService companyService;
    @Autowired
    UserService userService;

    /**
     * 添加公司信息
     * @param
     * @return
     */
    @ApiOperation(value = "添加公司信息")
    @RequestMapping(value = "/add",method = RequestMethod.POST)
    @ApiImplicitParams({
            @ApiImplicitParam(name="companyName",value="公司名称",required = true,dataType = "String",paramType="form"),
            @ApiImplicitParam(name="shortName",value="公司简称",required = true,dataType = "String",paramType="form"),
            @ApiImplicitParam(name="companyAddress",value="公司地址",required = true,dataType = "String",paramType="form"),
            @ApiImplicitParam(name="companyIntro",value="公司简介",required = true,dataType = "String",paramType="form")
    })
    public R add(String companyName,String shortName, String companyAddress, String companyIntro){
        Assert.isBlank(companyName,"公司名称不能为空");
        Assert.isBlank(shortName,"公司简称不能为空");
        Assert.isBlank(companyAddress,"公司地址不能为空");
        Assert.isBlank(companyIntro,"公司简介不能为空");
        Company company = new Company();
        company.setCompanyAddress(companyAddress);
        company.setCompanyName(companyName);
        company.setShortName(shortName);
        company.setCompanyIntro(companyIntro);
        company.setCreateTime(new Date());
        companyService.insert(company);
        return R.ok();
    }
    /**
     * 删除公司信息
     * @param
     * @return
     */
    @ApiOperation(value = "删除公司信息")
    @RequestMapping(value = "/delete",method = RequestMethod.POST)
    @ApiImplicitParams({
                @ApiImplicitParam(name="userId",value="用户Id",required = true,dataType = "Long",paramType="form"),
            @ApiImplicitParam(name="companyId",value="公司Id",required = true,dataType = "Long",paramType="form")
    })
    public R delete(Long userId, Long companyId){
        parameterIsNull(userId,"用户ID不能为空");
        parameterIsNull(companyId,"公司ID不能为空");
        //验证用户权限
        Company company = companyService.selectById(companyId);
        Assert.isNull(company,"没有此公司");
        companyService.deleteCompanyAndWaterPumps(companyId);
        return R.ok();
    }
    /**
     * 编辑公司信息
     * @param
     * @return
     */
    @ApiOperation(value = "编辑公司信息")
    @RequestMapping(value = "/update",method = RequestMethod.POST)
    @ApiImplicitParams({
            @ApiImplicitParam(name="userId",value="用户Id",required = true,dataType = "Long",paramType="form"),
            @ApiImplicitParam(name="companyId",value="公司Id",required = true,dataType = "Long",paramType="form"),
            @ApiImplicitParam(name="companyName",value="公司名称",required = true,dataType = "String",paramType="form"),
            @ApiImplicitParam(name="shortName",value="公司简称",required = true,dataType = "String",paramType="form"),
            @ApiImplicitParam(name="companyAddress",value="公司地址",required = true,dataType = "String",paramType="form"),
            @ApiImplicitParam(name="companyIntro",value="公司简介",required = true,dataType = "String",paramType="form")
    })
    public R update(Long userId, Long companyId,String companyName,String shortName, String companyAddress, String companyIntro){
        parameterIsNull(userId,"用户ID不能为空");
        parameterIsNull(companyId,"公司ID不能为空");
        //验证用户权限
        Company company = companyService.selectById(companyId);
        Assert.isNull(company,"没有此公司");
        if(!StringUtils.isEmpty(companyName)){
            company.setCompanyName(companyName);
        }
        if(!StringUtils.isEmpty(shortName)){
            company.setShortName(shortName);
        }
        if(!StringUtils.isEmpty(companyAddress)){
            company.setCompanyAddress(companyAddress);
        }
        if(!StringUtils.isEmpty(companyIntro)){
            company.setCompanyAddress(companyAddress);
        }
        company.setUpdateBy(String.valueOf(userId));
        company.setUpdateTime(new Date());
        companyService.updateById(company);
        return R.ok();
    }

    /**
     * 查询公司信息
     * @param
     * @return
     */
    @ApiOperation(value = "查询公司信息")
    @RequestMapping(value = "/query",method = RequestMethod.POST)
    @ApiImplicitParams({
            @ApiImplicitParam(name="userId",value="用户id",required = true,dataType = "Long",paramType="form"),
    })
    public R query(Long userId){
        Assert.isNull(userId,"用户id不能为空");
        BaseUserInfo baseUserInfo = userService.getUserByUserId(userId);
        Assert.isNull(baseUserInfo,"没有此用户");
        Long companyId = baseUserInfo.getCompanyId();
        Wrapper<Company> wp = new EntityWrapper<Company>();
        if(companyId!=null){
            wp.eq("company_id",companyId);
        }
        List<Company> companyList = companyService.selectList(wp);
        return R.ok().put("companyList",companyList);
    }

    /**
     * 根据id查询公司信息
     * @param
     * @return
     */
    @ApiOperation(value = "根据id查询公司信息")
    @RequestMapping(value = "/queryById",method = RequestMethod.POST)
    @ApiImplicitParams({
            @ApiImplicitParam(name="companyId",value="公司id",required = true,dataType = "Long",paramType="form"),
    })
    public R queryById(Long companyId){
        Assert.isNull(companyId,"公司id不能为空");
        Company company = companyService.selectById(companyId);
        Assert.isNull(company,"没有此公司");
        return R.ok().put("company",company);
    }
    /**
     * 分页查询
     * @param limit
     * @param page
     * @param companyName
     * @return
     */
    @RequestMapping(value = "/page",method = RequestMethod.GET)
    public TableResultResponse<Company> page(Integer limit,Integer page, String companyName){
        if (limit==null){limit=10;}
        if (page==null){page=1;}
        Page<Company> pages = new Page<Company>(page,limit);
        pages = companyService.selectPage(pages,new EntityWrapper<Company>().like("company_name",companyName));
        if (pages==null){
            return new TableResultResponse<Company>();
        }else{
            return new TableResultResponse<Company>(pages.getTotal(),pages.getRecords());
        }
    }



}
