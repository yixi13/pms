package com.pms.web;

import com.pms.controller.BaseController;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author asus
 * @since 2018-03-19
 */
@Controller
@RequestMapping("/pms/waterPumpGroupFault")
public class WaterPumpGroupFaultController extends BaseController {
	
}
