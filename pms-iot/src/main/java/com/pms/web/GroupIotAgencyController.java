package com.pms.web;
import com.pms.exception.R;
import com.pms.util.idutil.SnowFlake;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import com.baomidou.mybatisplus.mapper.EntityWrapper;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import com.pms.controller.BaseController;
import com.pms.service.IGroupIotAgencyService;
import com.pms.entity.GroupIotAgency;
import org.springframework.web.bind.annotation.RequestMapping;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/groupIotAgency")
@Api(value="二供模块角色机构管理信息" ,description = "二供模块角色机构管理信息")
public class GroupIotAgencyController extends BaseController {
    Logger logger = LoggerFactory.getLogger(getClass());
    @Autowired
    IGroupIotAgencyService groupIotAgencyService;

//    /**
//     * 添加二汞机构关联信息
//     * @param groupId
//     * @param groupIotAgencyList
//     * @return
//     */
//    @PostMapping("/add")
//    @ApiOperation("添加二汞机构关联信息")
//    @ApiImplicitParams({
//            @ApiImplicitParam(name="groupId",value="角色ID",required = true,dataType = "Long",paramType="form"),
//            @ApiImplicitParam(name="groupIotAgencyList",value="分区ID(数组)",required = true,dataType = "Long[]",paramType="form"),
//    })
//    public R add(Long groupId, Long[] groupIotAgencyList){
//        parameterIsNull(groupId,"角色ID不能为空");
//        parameterIsNull(groupIotAgencyList,"分区ID不能为空");
//        List<GroupIotAgency> list=new ArrayList<>();
//        for(Long i : groupIotAgencyList) {
//            GroupIotAgency  groupOrganization=new GroupIotAgency();
//            groupOrganization.setId(new SnowFlake(0,0).nextId());
//            groupOrganization.setGroupId(groupId);
//            groupOrganization.setAgencyId(i);
//            list.add(groupOrganization);
//        }
//        groupIotAgencyService.insertBatch(list);
//        return R.ok();
//    }

    /**
     * 修改二汞机构关联信息
     * @param groupId
     * @param groupIotAgencyList
     * @return
     */
    @PostMapping("/updata")
    @ApiOperation("修改二汞机构关联信息")
    @ApiImplicitParams({
            @ApiImplicitParam(name="groupId",value="角色ID",required = true,dataType = "Long",paramType="form"),
            @ApiImplicitParam(name="groupIotAgencyList",value="分区ID(数组)",required = true,dataType = "String",paramType="form"),
    })
    public R update(Long groupId, String groupIotAgencyList){
        parameterIsNull(groupId,"角色ID不能为空");
        parameterIsNull(groupIotAgencyList,"分区ID不能为空");
        if (StringUtils.isNotBlank(groupIotAgencyList.toString())){
            groupIotAgencyService.delete(new EntityWrapper<GroupIotAgency>().eq("group_id",groupId));
             String[] grouplist=groupIotAgencyList.split(",");
            for(int x=0;x<grouplist.length;x++){
                String id = grouplist[x];
                GroupIotAgency  groupIotAgency=new GroupIotAgency();
                groupIotAgency.setId(new SnowFlake(0,0).nextId());
                groupIotAgency.setGroupId(groupId);
                groupIotAgency.setAgencyId(Long.parseLong(id));
                groupIotAgencyService.insert(groupIotAgency);
            }
        }
        return R.ok();
    }

    @PostMapping("/selectList")
    @ApiOperation("查询二汞机构关联信息")
    @ApiImplicitParams({
            @ApiImplicitParam(name="groupId",value="角色ID",required = true,dataType = "Long",paramType="form"),
    })
    public R selectList(Long groupId){
        parameterIsNull(groupId,"角色ID不能为空");
       List<GroupIotAgency> list=groupIotAgencyService.selectList(new EntityWrapper<GroupIotAgency>().eq("group_id",groupId));
        return R.ok().put("data",list);
    }

}