package com.pms.web;
import com.pms.entity.IotCardRecharge;
import com.pms.exception.R;
import com.pms.service.IIotCardRechargeService;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import java.util.List;
/**
 * 定时器扣除数据卡充值费用
 */
@Component
public class IotCardtimerDeductionController {
    @Autowired
    IIotCardRechargeService iotCardRechargeService;
    /**
     * 定时器扣除数据卡充值费用
     * @return
     */
    @ApiOperation(value = "定时器扣除数据库充值费用")
//    @Scheduled(cron = "0 0 1 1 1 ？ *")
    public R cardTimerDeductionExpense(){
        List<IotCardRecharge> list=iotCardRechargeService.selectList(null);
        if (list.size()>0){
            for (int x=0;x<list.size();x++){
                IotCardRecharge  cardRecharge= iotCardRechargeService.selectById(list.get(x).getCardId());
                cardRecharge.setCurrentBalance(cardRecharge.getCurrentBalance().subtract(cardRecharge.getMonthlyFeeStandard()));//计算当前余额
                int y = cardRecharge.getCurrentBalance().compareTo(cardRecharge.getMonthlyFeeStandard());
                if(cardRecharge.getCurrentBalance().signum()==-1){
                    cardRecharge.setState(3);//欠费
                }else if (y==1||y==0){
                    cardRecharge.setState(1);//正常
                }else if (y==-1){
                    cardRecharge.setState(2);//余额不足
                }
                iotCardRechargeService.updateById(cardRecharge);
            }
        }
        return  R.ok();
    }


}
