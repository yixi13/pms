package com.pms.web;

import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.mapper.Wrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.pms.controller.BaseController;
import com.pms.entity.*;
import com.pms.exception.R;
import com.pms.result.TableResultResponse;
import com.pms.rpc.UserService;
import com.pms.service.ICommunityService;
import com.pms.service.ICompanyService;
import com.pms.service.IWaterPumpHouseService;
import com.pms.service.IWaterPumpGroupService;
import com.pms.service.tsdb.IIotDeviceService;
import com.pms.utils.IotAccessHttpClientUtil;
import com.pms.validator.Assert;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.Date;
import java.util.List;

/**
 * <p>
 * 水泵表 前端控制器
 * </p>
 *
 * @author lk
 * @since 2017-12-25
 */
@RestController
@RequestMapping("/pms/waterPumpGroup")
public class WaterPumpGroupController extends BaseController {
    @Autowired
    IWaterPumpGroupService waterPumpGroupService;
    @Autowired
    IWaterPumpHouseService waterPumpHouseService;
    @Autowired
    ICommunityService communityService;
    @Autowired
    ICompanyService companyService;
    @Autowired
    UserService userService;
    @Autowired
    IIotDeviceService iotDeviceService;
    @Autowired
    IotAccessHttpClientUtil iotAccessHttpClientUtil;
    /**
     * 删除水泵组信息
     * @param
     * @return
     */
    @ApiOperation(value = "删除水泵组信息")
    @RequestMapping(value = "/delete",method = RequestMethod.POST)
    @ApiImplicitParams({
            @ApiImplicitParam(name="groupId",value="水泵ID",required = true,dataType = "Long",paramType="form")
    })
    public R delete(Long groupId,Long userId){
        parameterIsNull(groupId,"水泵组ID不能为空");
        parameterIsNull(userId,"用户ID不能为空");
        //查看会员权限
        WaterPumpGroup waterPump = waterPumpGroupService.selectOne(new EntityWrapper<WaterPumpGroup>().eq("group_id",groupId));
        Assert.parameterIsNull(waterPump,"没有对应的水泵");
        waterPumpGroupService.deleteById(groupId);
        return R.ok();
    }

    /**
     * 添加水泵组信息
     * @param
     * @return
     */
    @ApiOperation(value = "添加水泵组信息")
    @RequestMapping(value = "/add",method = RequestMethod.POST)
    @ApiImplicitParams({
            @ApiImplicitParam(name="groupAlias",value="水泵组名称",required = true,dataType = "String",paramType="form"),
            @ApiImplicitParam(name="groupIntroduce",value="水泵组简介",required = true,dataType = "String",paramType="form"),
            @ApiImplicitParam(name="houseId",value="水泵房id",required = true,dataType = "Long",paramType="form"),
            @ApiImplicitParam(name="areaGb",value="区域编号",required = true,dataType = "int",paramType="form"),
            @ApiImplicitParam(name="communityId",value="所属小区id",required = true,dataType = "Long",paramType="form"),
            @ApiImplicitParam(name="companyId",value="所属公司id",required = true,dataType = "Long",paramType="form")
    })
    public R add(Long houseId,Long communityId,Integer areaGb,Long companyId,String groupIntroduce,String groupAlias){
        parameterIsNull(houseId,"水泵房ID不能为空");
        parameterIsNull(areaGb,"所属区域编号不能为空");
        parameterIsNull(communityId,"所属小区ID不能为空");
        parameterIsNull(companyId,"所属公司ID不能为空");
        parameterIsNull(groupAlias,"水泵组名称不能为空");
        if(groupAlias.length()>15){
            return R.error(400,"泵组名称过长");
        }
        if(StringUtils.isNotBlank(groupIntroduce)){
            if(groupIntroduce.length()>40){
                return R.error(400,"水泵组简介内容过长");
            }
        }
        WaterPumpGroup waterPumpGroup = new WaterPumpGroup();
        WaterPumpHouse waterPumpHouse = waterPumpHouseService.selectOne(new EntityWrapper<WaterPumpHouse>().eq("water_pump_house_id",houseId));
        Assert.isNull(waterPumpHouse,"对应的水泵房不存在");
        Community community = communityService.selectOne(new EntityWrapper<Community>().eq("community_id",communityId));
        Assert.isNull(community,"对应的小区不存在");
        Company company = companyService.selectOne(new EntityWrapper<Company>().eq("company_id",companyId));
        Assert.isNull(community,"对应的公司不存在");
        waterPumpGroup.setGroupAlias(groupAlias);
        waterPumpGroup.setAreaGb(areaGb);
        waterPumpGroup.setCreateTime(new Date());
//        waterPump.setAreaName();
        waterPumpGroup.setCommunityId(communityId);
        waterPumpGroup.setHouseId(houseId);
        waterPumpGroup.setCommunityName(community.getCommunityName());
        waterPumpGroup.setCompanyId(companyId);
        waterPumpGroup.setCompanyName(company.getCompanyName());
        waterPumpGroup.setGroupIntroduce(groupIntroduce);
        //查询水泵房水泵数量
        int countWpNum = waterPumpGroupService.selectCount(new EntityWrapper<WaterPumpGroup>().eq("house_id",houseId));
        waterPumpGroup.setGroupName("wp"+"_"+waterPumpGroup.getAreaGb()+"_"+community.getCommunitySort()+"_"+waterPumpHouse.getHouseSort()+"_"+(countWpNum+1));
        //物接入 创建接入实列====》创建设备===》设置身份====》设置策略(策略绑定主题)===>创建数据储存

        waterPumpGroupService.insert(waterPumpGroup);
        iotDeviceService.createWaterPumpConfig(null,waterPumpGroup.getGroupName());
        return R.ok();
    }

    /**
     * 编辑水泵组信息
     * @param
     * @return
     */
    @ApiOperation(value = "编辑水泵组信息")
    @RequestMapping(value = "/update",method = RequestMethod.POST)
    @ApiImplicitParams({
            @ApiImplicitParam(name="groupAlias",value="水泵组名称",required = true,dataType = "String",paramType="form"),
            @ApiImplicitParam(name="groupId",value="水泵组id",required = true,dataType = "Long",paramType="form"),
            @ApiImplicitParam(name="groupIntroduce",value="水泵介绍",required = true,dataType = "String",paramType="form"),
            @ApiImplicitParam(name="houseId",value="水泵房Id",required = true,dataType = "Long",paramType="form"),
            @ApiImplicitParam(name="areaGb",value="区域编号",required = true,dataType = "int",paramType="form"),
            @ApiImplicitParam(name="communityId",value="社区Id",required = true,dataType = "Long",paramType="form"),
            @ApiImplicitParam(name="companyId",value="公司Id",required = true,dataType = "Long",paramType="form")
    })
    public R update(Long groupId,String groupIntroduce,Long houseId,Integer areaGb,Long communityId,Long companyId,String groupAlias){
        parameterIsNull(groupId,"水泵id不能为空");
        Assert.isBlank(groupIntroduce,"公司介绍不能为空");
        parameterIsNull(houseId,"水泵房id不能为空");
        parameterIsNull(areaGb,"区域ID不能为空");
        parameterIsNull(communityId,"社区ID不能为空");
        parameterIsNull(companyId,"公司ID不能为空");

        WaterPumpGroup waterPumpGroup = waterPumpGroupService.selectById(groupId);
        Assert.isNull(waterPumpGroup,"没有对应的水泵");
        WaterPumpHouse waterPumpHouse = waterPumpHouseService.selectOne(new EntityWrapper<WaterPumpHouse>().eq("water_pump_house_id",houseId));
        Assert.isNull(waterPumpHouse,"没有对应的水泵房");
        Community community = communityService.selectOne(new EntityWrapper<Community>().eq("community_id",communityId));
        Assert.isNull(community,"对应的小区不存在");
        Company company = companyService.selectOne(new EntityWrapper<Company>().eq("company_id",companyId));
        Assert.isNull(community,"对应的公司不存在");
        if(StringUtils.isNotBlank(groupIntroduce)){
            if(groupIntroduce.length()>40){
                return R.error(400,"水泵组简介内容过长");
            }
            waterPumpGroup.setGroupIntroduce(groupIntroduce);
        }
        if(StringUtils.isNotBlank(groupAlias)){
            if(groupAlias.length()>15){
                return R.error(400,"水泵组名称内容过长");
            }
            waterPumpGroup.setGroupAlias(groupAlias);
        }
        waterPumpGroup.setCompanyId(companyId);
        waterPumpGroup.setCompanyName(company.getCompanyName());
        waterPumpGroup.setHouseId(houseId);
        waterPumpGroup.setHouseName(waterPumpHouse.getHouseName());
        waterPumpGroup.setAreaGb(areaGb);
        waterPumpGroup.setCommunityId(communityId);
        waterPumpGroup.setCommunityName(community.getCommunityName());
        waterPumpGroupService.updateById(waterPumpGroup);
        return R.ok();
    }

    /**
     * 根据id查询水泵信息
     * @param
     * @return
     */
    @ApiOperation(value = "根据id查询水泵信息")
    @RequestMapping(value = "/groupId",method = RequestMethod.POST)
    @ApiImplicitParams({
            @ApiImplicitParam(name="groupId",value="水泵ID",required = true,dataType = "Long",paramType="form")
    })
    public R queryById(Long groupId){
        parameterIsNull(groupId,"水泵ID不能为空");
        WaterPumpGroup waterPumpGroup = waterPumpGroupService.selectOne(new EntityWrapper<WaterPumpGroup>().eq("group_id",groupId));
        Assert.parameterIsNull(waterPumpGroup,"没有对应的水泵");
        return R.ok().put("waterPumpGroup",waterPumpGroup);
    }

    /**
     * 查询水泵信息
     * @param
     * @return
     */
    @ApiOperation(value = "查询水泵信息")
    @RequestMapping(value = "/query",method = RequestMethod.POST)
    @ApiImplicitParams({
            @ApiImplicitParam(name="userId",value="用户ID",required = true,dataType = "Long",paramType="form")
    })
    public R query(Long userId){
        parameterIsNull(userId,"用户ID不能为空");
        BaseUserInfo baseUserInfo = userService.getUserByUserId(userId);
        parameterIsNull(baseUserInfo,"没有此用户");
        Long companyId = baseUserInfo.getCompanyId();
        Long communityId = baseUserInfo.getCommunityId();
        Long areaId = baseUserInfo.getAreaId();
        Wrapper<WaterPumpGroup> wp = new EntityWrapper<WaterPumpGroup>();
        if(communityId!=null){
            wp.eq("community_id",communityId);
        }else{
            if(areaId!=null&&companyId!=null){
                wp.eq("company_id",companyId);
                wp.eq("area_id",areaId);
            }else if(companyId!=null&&areaId==null){
                wp.eq("company_id",companyId);
            }else if(companyId==null&&areaId!=null){
                throw new RuntimeException("数据添加有误");
            }
        }
        List<WaterPumpGroup> waterPumpGroupList = waterPumpGroupService.selectList(wp);
        return R.ok().put("waterPumpGroupList",waterPumpGroupList);
    }


    /**
     * 分页查询
     * @param limit
     * @param page
     * @param groupAlias
     * @return
     */
    @RequestMapping(value = "/page",method = RequestMethod.GET)
    public TableResultResponse<WaterPumpGroup> page(Integer limit, Integer page, String groupAlias){
        if (limit==null){limit=10;}
        if (page==null){page=1;}
        Page<WaterPumpGroup> pages = new Page<WaterPumpGroup>(page,limit);
        Wrapper<WaterPumpGroup> WP=new EntityWrapper<WaterPumpGroup>();
        if(StringUtils.isNotBlank(groupAlias)){
            WP.like("group_alias",groupAlias.trim());
        }
        pages = waterPumpGroupService.selectPage(pages,WP);
        if (pages==null){
            return new TableResultResponse<WaterPumpGroup>();
        }else{
            return new TableResultResponse<WaterPumpGroup>(pages.getTotal(),pages.getRecords());
        }
    }

}
