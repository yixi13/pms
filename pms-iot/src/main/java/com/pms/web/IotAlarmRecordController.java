package com.pms.web;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.mysql.jdbc.StringUtils;
import com.pms.controller.BaseController;
import com.pms.entity.IotAlarmRecord;
import com.pms.entity.IotWaterPumpGroup;
import com.pms.exception.R;
import com.pms.rpc.IotWaterPumpGroupService;
import com.pms.service.IGroupIotAgencyService;
import com.pms.service.IIotAlarmRecordService;
import com.pms.service.IIotWaterPumpGroupService;
import com.pms.service.IWaterPumpGroupService;
import com.pms.validator.Assert;
import com.xiaoleilu.hutool.util.StrUtil;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 设备上传数据 前端控制器
 * </p>
 *
 * @author ljb
 * @since 2018-08-20
 */

@RestController
@RequestMapping("/alarm/alarmRecord")
@Api(value="二供报警" ,description = "二供报警")
public class IotAlarmRecordController extends BaseController {

    @Autowired
    IIotAlarmRecordService iIotAlarmRecordService;
    @Autowired
    IGroupIotAgencyService groupIotAgencyService;
    @Autowired
    IIotWaterPumpGroupService iIotWaterPumpGroupService;

    @ApiOperation(value = "获取报警记录list")
    @RequestMapping(value = "/list",method = RequestMethod.POST)
    @ApiImplicitParams({
            @ApiImplicitParam(name="groupIds",value="设备组id",required = true,dataType = "String",paramType="form"),
            @ApiImplicitParam(name="starTime",value="开始时间",required = true,dataType = "String",paramType="form"),
            @ApiImplicitParam(name="endTime",value="结束时间",required = true,dataType = "String",paramType="form"),
            @ApiImplicitParam(name="alarmType",value="报警类别",required = true,dataType = "String",paramType="form")
    })
    public R list(Integer current, Integer size, String groupIds,Integer isReade,
                  String starTime, String endTime,String alarmType,String content){
        Assert.isNull(current,"current 不能为空");
        Assert.isNull(size,"size 不能为空");

        EntityWrapper<Map<String,Object>> ew = new EntityWrapper<Map<String,Object>>();
        if (StrUtil.isNotEmpty(alarmType)){
            ew.eq("iat.id", alarmType);
        }
        if (isReade!=null){
            ew.eq("ar.is_reade", isReade);
        }
        if (StrUtil.isNotEmpty(groupIds)){
            ew.in("ar.group_id", groupIds);
        }else{
            ew.in("ar.group_id", "-1");
        }
        if (StrUtil.isNotEmpty(starTime)) {
            starTime = starTime + " 00:00:00";
            ew.and("ar.creat_time >'" + starTime + "'");
        }
        if (StrUtil.isNotEmpty(endTime)) {
            endTime = endTime + " 23:59:59";
            ew.and("ar.creat_time <'" + endTime + "'");
        }
        if (StrUtil.isNotEmpty(content)){
            ew.and().like("ar.describe", content).
                    or(" wpg.group_name LIKE '%"+content+"%' ").
                    or(" wpg.community_name LIKE '%"+content+"%' ").
                    or(" wpg.house_name LIKE '%"+content+"%' ");
        }
        ew.orderBy("ar.is_reade desc");
        ew.orderBy("ar.creat_time desc");
        Page<Map<String,Object>> page = new Page(current, size);
        page = iIotAlarmRecordService.selectAlarmRecordRecordMapsPage(page,ew);
        return R.ok().put("page",page);
    }


    @ApiOperation(value = "修改为已读")
    @RequestMapping(value = "/updateToReade",method = RequestMethod.POST)
    @ApiImplicitParams({
            @ApiImplicitParam(name="alarmIds", value="报警ids",required = true,dataType = "String",paramType="form")
    })
    public R updateToReade(String alarmIds){
        Assert.isBlank(alarmIds,"报警ids");
        EntityWrapper<IotAlarmRecord> ew = new EntityWrapper<IotAlarmRecord>();
        ew.in("id", alarmIds);
        List<IotAlarmRecord> records = iIotAlarmRecordService.selectList(ew);
        for (IotAlarmRecord record:records){
            record.setIsReade(1);
        }
        iIotAlarmRecordService.updateBatchById(records);
        return R.ok();
    }


    @ApiOperation(value = "删除")
    @RequestMapping(value = "/{alarmIds}",method = RequestMethod.DELETE)
    @ApiImplicitParams({
            @ApiImplicitParam(name="alarmIds", value="报警ids",required = true,dataType = "String",paramType="form")
    })
    public R delete(@PathVariable String alarmIds){
        iIotAlarmRecordService.deleteById(Long.valueOf(alarmIds));
        return R.ok();
    }

    @ApiOperation(value = "删除")
    @RequestMapping(value = "/batch",method = RequestMethod.DELETE)
    @ApiImplicitParams({
            @ApiImplicitParam(name="alarmIds", value="List报警ids",required = true,dataType = "String",paramType="form")
    })
    public R deleteBatch(String alarmIds){
        Assert.isBlank(alarmIds,"报警ids");
        iIotAlarmRecordService.deleteBatch(alarmIds);
        return R.ok();
    }


    @ApiOperation(value = "查询所有未读的数据/有问题再说")
    @RequestMapping(value = "/getNotReade",method = RequestMethod.GET)
    public R countNoReade(@RequestHeader("X-Code") String companyCode,
                          @RequestHeader("X-groupType") String groupType,
                          @RequestHeader("X-groupId") String roleId){

        List<IotWaterPumpGroup> iotWaterPumpGroupList = iIotWaterPumpGroupService.selectList(
                new EntityWrapper<IotWaterPumpGroup>().eq("company_code",companyCode)
        );
        String groupIds = "";
        for (IotWaterPumpGroup iotWaterPumpGroup : iotWaterPumpGroupList) {
            groupIds += iotWaterPumpGroup.getId() + ",";
        }
        EntityWrapper ew = new EntityWrapper<IotAlarmRecord>();
        ew.eq("is_reade", 2);
        if (groupIds.length() > 0) {
            groupIds = groupIds.substring(0, groupIds.length() - 1);
            ew.in("group_id", groupIds);
        }
        Integer count = iIotAlarmRecordService.selectCount(ew);
        return R.ok().put("count",count);
    }
    
}
