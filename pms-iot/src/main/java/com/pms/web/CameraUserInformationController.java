package com.pms.web;

import com.pms.entity.WaterPumpHouse;
import org.springframework.beans.factory.annotation.Autowired;
import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.plugins.Page;
import org.springframework.web.bind.annotation.*;
import com.pms.controller.BaseController;
import com.pms.service.ICameraUserInformationService;
import com.pms.entity.CameraUserInformation;
import com.pms.result.JsonResult;
import com.pms.result.ObjectRestResponse;
import com.pms.result.TableResultResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;

@RestController
@RequestMapping("cameraUserInformation")
public class CameraUserInformationController extends BaseController {
    Logger logger = LoggerFactory.getLogger(getClass());
    @Autowired
    ICameraUserInformationService cameraUserInformationService;
    /**
         * 新增
         * @param entity
         * @return
         */
    @RequestMapping(value = "",method = RequestMethod.POST)
    @ResponseBody
    public ObjectRestResponse<CameraUserInformation> add(@RequestBody CameraUserInformation entity){
            cameraUserInformationService.insert(entity);
        return new ObjectRestResponse<CameraUserInformation>().rel(true);
    }
    /**
     * 修改
     * @param entity
     * @return
     */
    @RequestMapping(value = "/{id}",method = RequestMethod.POST)
    @ResponseBody
    public ObjectRestResponse<CameraUserInformation> update(@RequestBody CameraUserInformation entity){
            cameraUserInformationService.updateById(entity);
        return new ObjectRestResponse<CameraUserInformation>().rel(true);
    }

    /**
 * 删除
 * @param id
 * @return
 */
    @RequestMapping(value = "/{id}",method = RequestMethod.DELETE)
    @ResponseBody
    public ObjectRestResponse<CameraUserInformation> deleteById(@PathVariable int id){
            cameraUserInformationService.deleteById (id);
        return new ObjectRestResponse<CameraUserInformation>().rel(true);
    }


    /**
     * findByID
     * @param id
     * @return
     */
    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    @ResponseBody
    public ObjectRestResponse<CameraUserInformation> findById(@PathVariable int id){
        return new ObjectRestResponse<CameraUserInformation>().rel(true).data( cameraUserInformationService.selectById(id));
    }


    @RequestMapping(value = "/page",method = RequestMethod.GET)
    @ResponseBody
    public TableResultResponse<CameraUserInformation> page(@RequestParam(defaultValue = "10") int limit, @RequestParam(defaultValue = "1")int offset){
        Page<CameraUserInformation> page = new Page<CameraUserInformation>(offset,limit);
        page = cameraUserInformationService.selectPage(page,new EntityWrapper<CameraUserInformation>());
        if (page==null){
            return new TableResultResponse<CameraUserInformation>();
        }else{
            return new TableResultResponse<CameraUserInformation>(page.getTotal(),page.getRecords());
        }
    }

    @RequestMapping(value = "/all",method = RequestMethod.GET)
    @ResponseBody
    public List<CameraUserInformation> all(){
        return cameraUserInformationService.selectList(null);
    }


}