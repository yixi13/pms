package com.pms.web;

import com.baomidou.mybatisplus.mapper.Wrapper;
import org.springframework.beans.factory.annotation.Autowired;
import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.plugins.Page;
import org.springframework.web.bind.annotation.*;
import com.pms.controller.BaseController;
import com.pms.service.IEquipmentRepairRecordService;
import com.pms.entity.EquipmentRepairRecord;
import com.pms.result.JsonResult;
import com.pms.result.ObjectRestResponse;
import com.pms.result.TableResultResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;

@RestController
@RequestMapping("equipmentRepairRecord")
public class EquipmentRepairRecordController extends BaseController {
    Logger logger = LoggerFactory.getLogger(getClass());
    @Autowired
    IEquipmentRepairRecordService equipmentRepairRecordService;

    /**
         * 新增
         * @param entity
         * @return
         */
    @RequestMapping(value = "",method = RequestMethod.POST)
    @ResponseBody
    public ObjectRestResponse<EquipmentRepairRecord> add(@RequestBody EquipmentRepairRecord entity){
            equipmentRepairRecordService.insert(entity);
        return new ObjectRestResponse<EquipmentRepairRecord>().rel(true);
    }
    /**
     * 修改
     * @param entity
     * @return
     */
    @RequestMapping(value = "/{id}",method = RequestMethod.POST)
    @ResponseBody
    public ObjectRestResponse<EquipmentRepairRecord> update(@RequestBody EquipmentRepairRecord entity){
            equipmentRepairRecordService.updateById(entity);
        return new ObjectRestResponse<EquipmentRepairRecord>().rel(true);
    }

    /**
 * 删除
 * @param id
 * @return
 */
    @RequestMapping(value = "/{id}",method = RequestMethod.DELETE)
    @ResponseBody
    public ObjectRestResponse<EquipmentRepairRecord> deleteById(@PathVariable int id){
            equipmentRepairRecordService.deleteById (id);
        return new ObjectRestResponse<EquipmentRepairRecord>().rel(true);
    }


    /**
     * findByID
     * @param id
     * @return
     */
    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    @ResponseBody
    public ObjectRestResponse<EquipmentRepairRecord> findById(@PathVariable int id){
        return new ObjectRestResponse<EquipmentRepairRecord>().rel(true).data( equipmentRepairRecordService.selectById(id));
    }


    @RequestMapping(value = "/page",method = RequestMethod.GET)
    @ResponseBody
    public TableResultResponse<EquipmentRepairRecord> page(@RequestParam(defaultValue = "10") int limit, @RequestParam(defaultValue = "1")int offset,Integer isHandle){
        Page<EquipmentRepairRecord> page = new Page<EquipmentRepairRecord>(offset,limit);
        Wrapper<EquipmentRepairRecord> wrapper = new EntityWrapper<EquipmentRepairRecord>();
        if(isHandle!=null){
            wrapper.eq("is_handle",isHandle);
        }
        wrapper.orderBy("create_time",false);
        page = equipmentRepairRecordService.selectPage(page,wrapper);
        return new TableResultResponse<EquipmentRepairRecord>(page.getTotal(),page.getRecords());
    }

    @RequestMapping(value = "/all",method = RequestMethod.GET)
    @ResponseBody
    public List<EquipmentRepairRecord> all(){
        return equipmentRepairRecordService.selectList(null);
    }


}