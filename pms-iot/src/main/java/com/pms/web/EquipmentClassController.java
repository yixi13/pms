package com.pms.web;

import org.springframework.beans.factory.annotation.Autowired;
import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.plugins.Page;
import org.springframework.web.bind.annotation.*;
import com.pms.controller.BaseController;
import com.pms.service.IEquipmentClassService;
import com.pms.entity.EquipmentClass;
import com.pms.result.JsonResult;
import com.pms.result.ObjectRestResponse;
import com.pms.result.TableResultResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;

@RestController
@RequestMapping("equipmentClass")
public class EquipmentClassController extends BaseController {
    Logger logger = LoggerFactory.getLogger(getClass());
    @Autowired
    IEquipmentClassService equipmentClassService;

    /**
         * 新增
         * @param entity
         * @return
         */
    @RequestMapping(value = "",method = RequestMethod.POST)
    @ResponseBody
    public ObjectRestResponse<EquipmentClass> add(@RequestBody EquipmentClass entity){
            equipmentClassService.insert(entity);
        return new ObjectRestResponse<EquipmentClass>().rel(true);
    }
    /**
     * 修改
     * @param entity
     * @return
     */
    @RequestMapping(value = "/{id}",method = RequestMethod.POST)
    @ResponseBody
    public ObjectRestResponse<EquipmentClass> update(@RequestBody EquipmentClass entity){
            equipmentClassService.updateById(entity);
        return new ObjectRestResponse<EquipmentClass>().rel(true);
    }

    /**
 * 删除
 * @param id
 * @return
 */
    @RequestMapping(value = "/{id}",method = RequestMethod.DELETE)
    @ResponseBody
    public ObjectRestResponse<EquipmentClass> deleteById(@PathVariable int id){
            equipmentClassService.deleteById (id);
        return new ObjectRestResponse<EquipmentClass>().rel(true);
    }


    /**
     * findByID
     * @param id
     * @return
     */
    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    @ResponseBody
    public ObjectRestResponse<EquipmentClass> findById(@PathVariable int id){
        return new ObjectRestResponse<EquipmentClass>().rel(true).data( equipmentClassService.selectById(id));
    }


    @RequestMapping(value = "/page",method = RequestMethod.GET)
    @ResponseBody
    public TableResultResponse<EquipmentClass> page(@RequestParam(defaultValue = "10") int limit, @RequestParam(defaultValue = "1")int offset){
        Page<EquipmentClass> page = new Page<EquipmentClass>(offset,limit);
        page = equipmentClassService.selectPage(page,new EntityWrapper<EquipmentClass>());
        return new TableResultResponse<EquipmentClass>(page.getTotal(),page.getRecords());
    }

    @RequestMapping(value = "/all",method = RequestMethod.GET)
    @ResponseBody
    public List<EquipmentClass> all(){
        return equipmentClassService.selectList(null);
    }


}