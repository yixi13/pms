package com.pms.web;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.mapper.Wrapper;
import com.pms.entity.WaterPumpGroupAttr;
import com.pms.exception.R;
import com.pms.service.IWaterPumpGroupAttrService;
import com.pms.util.DateUtil;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.apache.commons.lang3.StringUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author asus
 * @since 2018-01-10
 */
@RestController
@Api(value = "水泵后台管理接口", description = "水泵后台管理接口")
@RequestMapping("/iot/waterPumpAttr")
public class WaterPumpGroupAttrController {
    IWaterPumpGroupAttrService waterPumpAttrService;
    /**
     * 查询水泵信息
     * @param
     * @return
     */
    @ApiOperation(value = "查询水泵信息")
    @RequestMapping(value = "/readInfo",method = RequestMethod.POST)
    @ApiImplicitParams({
            @ApiImplicitParam(name="deviceName",value="水泵设备名称",required = false,dataType = "string",paramType="form"),
            @ApiImplicitParam(name="startTime",value="查询起始时间",required = false,dataType = "string",paramType="form"),
            @ApiImplicitParam(name="endTime",value="查询截止时间",required = false,dataType = "string",paramType="form"),
    })
    public R queryById(String deviceName,String startTime,String endTime){
        if(StringUtils.isBlank(deviceName)){
            deviceName ="thing";
        }
        List<WaterPumpGroupAttr> WaterPumpAttrList = new ArrayList<WaterPumpGroupAttr>();
        Wrapper<WaterPumpGroupAttr> waterPumpWp =  new EntityWrapper<WaterPumpGroupAttr>();
        waterPumpWp.ge("reportTime", DateUtil.stringToDate(startTime).getTime()/1000);//  >=
        waterPumpWp.le("reportTime", DateUtil.stringToDate(endTime).getTime()/1000);//  <=
        WaterPumpAttrList = waterPumpAttrService.selectList(waterPumpWp);
        return R.ok().put("waterPump",WaterPumpAttrList);
    }

    /**
     * 查询水泵信息
     * @param
     * @return
     */
    @ApiOperation(value = "查询水泵属性集合")
    @RequestMapping(value = "/readAttrList",method = RequestMethod.POST)
    @ApiImplicitParams({
    })
    public R readAttrList(){
        return R.ok().put("waterPump",WaterPumpWebSocketUtil.getWaterPumpFiled());
    }


}
