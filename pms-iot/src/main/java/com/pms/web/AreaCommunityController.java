package com.pms.web;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.pms.controller.BaseController;
import com.pms.entity.AreaCommunity;
import com.pms.result.ObjectRestResponse;
import com.pms.result.TableResultResponse;
import com.pms.service.IAreaCommunityService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("areaCommunity")
public class AreaCommunityController extends BaseController {
    Logger logger = LoggerFactory.getLogger(getClass());
    @Autowired
    IAreaCommunityService areaCommunityService;

    /**
         * 新增
         * @param entity
         * @return
         */
    @RequestMapping(value = "",method = RequestMethod.POST)
    @ResponseBody
    public ObjectRestResponse<AreaCommunity> add(@RequestBody AreaCommunity entity){
            areaCommunityService.insert(entity);
        return new ObjectRestResponse<AreaCommunity>().rel(true);
    }
    /**
     * 修改
     * @param entity
     * @return
     */
    @RequestMapping(value = "/{id}",method = RequestMethod.POST)
    @ResponseBody
    public ObjectRestResponse<AreaCommunity> update(@RequestBody AreaCommunity entity){
            areaCommunityService.updateById(entity);
        return new ObjectRestResponse<AreaCommunity>().rel(true);
    }

    /**
 * 删除
 * @param id
 * @return
 */
    @RequestMapping(value = "/{id}",method = RequestMethod.DELETE)
    @ResponseBody
    public ObjectRestResponse<AreaCommunity> deleteById(@PathVariable int id){
            areaCommunityService.deleteById (id);
        return new ObjectRestResponse<AreaCommunity>().rel(true);
    }


    /**
     * findByID
     * @param id
     * @return
     */
    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    @ResponseBody
    public ObjectRestResponse<AreaCommunity> findById(@PathVariable int id){
        return new ObjectRestResponse<AreaCommunity>().rel(true).data( areaCommunityService.selectById(id));
    }


    @RequestMapping(value = "/page",method = RequestMethod.GET)
    @ResponseBody
    public TableResultResponse<AreaCommunity> page(@RequestParam(defaultValue = "10") int limit, @RequestParam(defaultValue = "1")int offset){
        Page<AreaCommunity> page = new Page<AreaCommunity>(offset,limit);
        page = areaCommunityService.selectPage(page,new EntityWrapper<AreaCommunity>());
        return new TableResultResponse<AreaCommunity>(page.getTotal(),page.getRecords());
    }

    @RequestMapping(value = "/all",method = RequestMethod.GET)
    @ResponseBody
    public List<AreaCommunity> all(){
        return areaCommunityService.selectList(null);
    }


}