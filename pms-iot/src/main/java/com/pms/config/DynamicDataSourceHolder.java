package com.pms.config;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 动态数据源持有者，负责利用ThreadLocal存取数据源名称
 */
public class DynamicDataSourceHolder {
    protected Logger logger = LoggerFactory.getLogger(getClass());
    /**
     * 本地线程共享对象
     */
    private static final ThreadLocal<String> THREAD_LOCAL = new ThreadLocal<>();

    public static void putDataSource(String name) {
        THREAD_LOCAL.set(name);
    }

    public static String getDataSource() {
        String currentDataSourceKey = THREAD_LOCAL.get();
        if(StringUtils.isBlank(currentDataSourceKey)){
            currentDataSourceKey= DataSourceNameConstant.LOCAL_DATASOURCE;
        }
        return currentDataSourceKey;
    }

    public static void removeDataSource() {
        THREAD_LOCAL.remove();
    }
}
