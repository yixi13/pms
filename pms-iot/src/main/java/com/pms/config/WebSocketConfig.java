package com.pms.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.socket.server.standard.ServerEndpointExporter;

/**
 * Created by Administrator on 2018/1/8.
 */
@Configuration
public class WebSocketConfig {
    /**
     * 要注入ServerEndpointExporter，这个bean会自动注册使用了@ServerEndpoint注解声明的Websocket endpoint。
     * 使用独立的servlet容器，而不是直接使用springboot的内置容器时注入ServerEndpointExporter，因为它将由servlet容器自己提供和管理。
     * @return
     */
    @Bean
    public ServerEndpointExporter serverEndpointExporter() {
        return new ServerEndpointExporter();
    }

}
