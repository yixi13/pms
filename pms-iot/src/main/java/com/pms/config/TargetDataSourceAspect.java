package com.pms.config;

import com.pms.annotation.TargetDataSource;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.After;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;
import org.aspectj.lang.reflect.MethodSignature;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Service;

import java.lang.reflect.Method;

/**
 * 数据源 切面
 * @since 1.7
 */
@Aspect
@Order(-1)
@Service
public class TargetDataSourceAspect {
    protected Logger log = LoggerFactory.getLogger(getClass());

    @Pointcut("@annotation(com.pms.annotation.TargetDataSource)")
    public void aspectTargetDataSource() {
    }

    @Before("aspectTargetDataSource()&& @annotation(anno)")
    public void  interceptor(JoinPoint joinPoint,TargetDataSource anno) throws Throwable {
        DynamicDataSourceHolder.putDataSource(anno.name());
    }

    //执行完切面后，将线程共享中的数据源名称清空
    @After("aspectTargetDataSource()&& @annotation(anno)")
    public void after(JoinPoint joinPoint,TargetDataSource anno){
        DynamicDataSourceHolder.removeDataSource();
    }
}
