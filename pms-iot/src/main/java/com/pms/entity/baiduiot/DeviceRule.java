package com.pms.entity.baiduiot;

import java.util.List;

/**
 * 百度天工规则引擎实体
 *
 * @author zyl
 * @create 2017-12-22 15:46
 **/
public class DeviceRule {
    public static class TDestinations{

        private	String	value;	/*test.tsdb.iot.gz.baidubce.com*/
        private	String	kind;	/*TSDB*/

        public void setValue(String value){
            this.value = value;
        }
        public String getValue(){
            return this.value;
        }

        public void setKind(String value){
            this.kind = value;
        }
        public String getKind(){
            return this.kind;
        }

    }
    private List<TDestinations> destinations;	/*List<TDestinations>*/
    public void setDestinations(List<TDestinations> value){
        this.destinations = value;
    }
    public List<TDestinations> getDestinations(){
        return this.destinations;
    }

    public static class TSources{

        private	String	condition;	/*<>*/
        private	String	description;	/*This is condition 1*/
        private	String	name;	/*name*/
        private	String	value;	/*aaa*/
        private	String	type;	/*string*/

        public void setCondition(String value){
            this.condition = value;
        }
        public String getCondition(){
            return this.condition;
        }

        public void setDescription(String value){
            this.description = value;
        }
        public String getDescription(){
            return this.description;
        }

        public void setName(String value){
            this.name = value;
        }
        public String getName(){
            return this.name;
        }

        public void setValue(String value){
            this.value = value;
        }
        public String getValue(){
            return this.value;
        }

        public void setType(String value){
            this.type = value;
        }
        public String getType(){
            return this.type;
        }

    }
    private	List<TSources>	sources;	/*List<TSources>*/
    public void setSources(List<TSources> value){
        this.sources = value;
    }
    public List<TSources> getSources(){
        return this.sources;
    }

    private	String	name;	/*device xxxx to TSDB yyyy*/

    public void setName(String value){
        this.name = value;
    }
    public String getName(){
        return this.name;
    }


}
