package com.pms.entity;

import com.baomidou.mybatisplus.enums.IdType;
import java.util.Date;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableName;
import java.io.Serializable;

/**
 * <p>
 * 设备上传数据
 * </p>
 *
 * @author ljb
 * @since 2018-08-20
 */
@TableName("iot_alarm_record")
public class IotAlarmRecord extends Model<IotAlarmRecord> {

    private static final long serialVersionUID = 1L;

    /**
     * 主键
     */
	@TableId(value="id", type= IdType.AUTO)
	private Long id;
    /**
     * 报警类型(关联类别表)
     */
	@TableField("alarm_type")
	private Integer alarmType;
    /**
     * 报警描述
     */
	private String describe;
    /**
     * 设备组id
     */
	@TableField("group_id")
	private Long groupId;
    /**
     * 记录时间
     */
	@TableField("creat_time")
	private Date creatTime;
    /**
     * 设备状态(0：异常，1：正常)
     */
	@TableField("group_status")
	private Integer groupStatus;
    /**
     * 通讯状态(0：异常，1：正常)
     */
	@TableField("communication_status")
	private Integer communicationStatus;
    /**
     * 超压报警
     */
	@TableField("sup_pressure")
	private String supPressure;
    /**
     * 变频器报警
     */
	private String inverter;
    /**
     * 是否已读，1已读，2未读
     */
	@TableField("is_reade")
	private Integer isReade;


	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Integer getAlarmType() {
		return alarmType;
	}

	public void setAlarmType(Integer alarmType) {
		this.alarmType = alarmType;
	}

	public String getDescribe() {
		return describe;
	}

	public void setDescribe(String describe) {
		this.describe = describe;
	}

	public Long getGroupId() {
		return groupId;
	}

	public void setGroupId(Long groupId) {
		this.groupId = groupId;
	}

	public Date getCreatTime() {
		return creatTime;
	}

	public void setCreatTime(Date creatTime) {
		this.creatTime = creatTime;
	}

	public Integer getGroupStatus() {
		return groupStatus;
	}

	public void setGroupStatus(Integer groupStatus) {
		this.groupStatus = groupStatus;
	}

	public Integer getCommunicationStatus() {
		return communicationStatus;
	}

	public void setCommunicationStatus(Integer communicationStatus) {
		this.communicationStatus = communicationStatus;
	}

	public String getSupPressure() {
		return supPressure;
	}

	public void setSupPressure(String supPressure) {
		this.supPressure = supPressure;
	}

	public String getInverter() {
		return inverter;
	}

	public void setInverter(String inverter) {
		this.inverter = inverter;
	}

	public Integer getIsReade() {
		return isReade;
	}

	public void setIsReade(Integer isReade) {
		this.isReade = isReade;
	}

	@Override
	protected Serializable pkVal() {
		return this.id;
	}

}
