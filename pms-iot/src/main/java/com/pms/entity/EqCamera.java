package com.pms.entity;
import com.baomidou.mybatisplus.activerecord.Model;
import java.io.Serializable;
import java.util.Date;
import javax.persistence.*;
import com.pms.util.Description;
import com.pms.entity.BaseModel;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;


/**
 * 
 * 
 * @author zyl
 * @email 517770986@qq.com
 * @date 2018-03-15 11:21:02
 */
@TableName( "eq_camera")
public class EqCamera extends BaseModel<EqCamera> {
private static final long serialVersionUID = 1L;

	/**
	 *用户id
	 */
    @TableId("id_")
    private Long id;
	
	    /**
     *摄像头位置
     */
    @TableField("place_")
    @Description("摄像头位置")
    private String place;
	
	    /**
     *设备序列号
     */
    @TableField("key_")
    @Description("设备序列号")
    private String key;
	
	    /**
     *备注
     */
    @TableField("remack_")
    @Description("备注")
    private String remack;
	
	    /**
     *水泵房ID
     */
    @TableField("water_pump_house_id")
    @Description("水泵房ID")
    private Long waterPumpHouseId;
	
	    /**
     *设备id
     */
    @TableField("equipment_id")
    @Description("设备id")
    private String equipmentId;
	
	    /**
     *封面地址
     */
    @TableField("front_address")
    @Description("封面地址")
    private String frontAddress;
	
	    /**
     *验证码
     */
    @TableField("verification_code")
    @Description("验证码")
    private String verificationCode;
	
	    /**
     *token码
     */
    @TableField("token_")
    @Description("token码")
    private String token;
	
	    /**
     *token结束时间
     */
    @TableField("expire_time")
    @Description("token结束时间")
    private Date expireTime;
	
	    /**
     *当前时间
     */
    @TableField("currentdate_time")
    @Description("当前时间")
    private Date currentdateTime;
	
	    /**
     *用户ID
     */
    @TableField("user_id")
    @Description("用户ID")
    private Long userId;
	
	    /**
     *创建时间
     */
    @TableField("create_time")
    @Description("创建时间")
    private Date createTime;
	
	    /**
     *创建人
     */
    @TableField("create_by")
    @Description("创建人")
    private String createBy;
	
	    /**
     *摄像头用户配置信息ID
     */
    @TableField("camera_user_id")
    @Description("摄像头用户配置信息ID")
    private Long cameraUserId;
	
	    /**
     *抓拍图片
     */
    @TableField("picUrl")
    @Description("抓拍图片")
    private String picurl;

	/**
	 * 临时字段
	 */
	@TableField(exist = false)
	private String appKey;

	@TableField(exist = false)
	private CameraUserInformation cameraUserInformation;


// ID赋值
public EqCamera(){
        this.id= returnIdLong();
        }
@Override
protected Serializable pkVal() {
        return this.id;
}
	/**
	 * 设置：用户id
	 */
	public EqCamera setId(Long id) {
		this.id = id;
		return this;
	}
	/**
	 * 获取：用户id
	 */
	public Long getId() {
		return id;
	}
	/**
	 * 设置：摄像头位置
	 */
	public EqCamera setPlace(String place) {
		this.place = place;
		return this;
	}
	/**
	 * 获取：摄像头位置
	 */
	public String getPlace() {
		return place;
	}
	/**
	 * 设置：设备序列号
	 */
	public EqCamera setKey(String key) {
		this.key = key;
		return this;
	}
	/**
	 * 获取：设备序列号
	 */
	public String getKey() {
		return key;
	}
	/**
	 * 设置：备注
	 */
	public EqCamera setRemack(String remack) {
		this.remack = remack;
		return this;
	}
	/**
	 * 获取：备注
	 */
	public String getRemack() {
		return remack;
	}
	/**
	 * 设置：水泵房ID
	 */
	public EqCamera setWaterPumpHouseId(Long waterPumpHouseId) {
		this.waterPumpHouseId = waterPumpHouseId;
		return this;
	}
	/**
	 * 获取：水泵房ID
	 */
	public Long getWaterPumpHouseId() {
		return waterPumpHouseId;
	}
	/**
	 * 设置：设备id
	 */
	public EqCamera setEquipmentId(String equipmentId) {
		this.equipmentId = equipmentId;
		return this;
	}
	/**
	 * 获取：设备id
	 */
	public String getEquipmentId() {
		return equipmentId;
	}
	/**
	 * 设置：封面地址
	 */
	public EqCamera setFrontAddress(String frontAddress) {
		this.frontAddress = frontAddress;
		return this;
	}
	/**
	 * 获取：封面地址
	 */
	public String getFrontAddress() {
		return frontAddress;
	}
	/**
	 * 设置：验证码
	 */
	public EqCamera setVerificationCode(String verificationCode) {
		this.verificationCode = verificationCode;
		return this;
	}
	/**
	 * 获取：验证码
	 */
	public String getVerificationCode() {
		return verificationCode;
	}
	/**
	 * 设置：token码
	 */
	public EqCamera setToken(String token) {
		this.token = token;
		return this;
	}
	/**
	 * 获取：token码
	 */
	public String getToken() {
		return token;
	}
	/**
	 * 设置：token结束时间
	 */
	public EqCamera setExpireTime(Date expireTime) {
		this.expireTime = expireTime;
		return this;
	}
	/**
	 * 获取：token结束时间
	 */
	public Date getExpireTime() {
		return expireTime;
	}
	/**
	 * 设置：当前时间
	 */
	public EqCamera setCurrentdateTime(Date currentdateTime) {
		this.currentdateTime = currentdateTime;
		return this;
	}
	/**
	 * 获取：当前时间
	 */
	public Date getCurrentdateTime() {
		return currentdateTime;
	}
	/**
	 * 设置：用户ID
	 */
	public EqCamera setUserId(Long userId) {
		this.userId = userId;
		return this;
	}
	/**
	 * 获取：用户ID
	 */
	public Long getUserId() {
		return userId;
	}
	/**
	 * 设置：创建时间
	 */
	public EqCamera setCreateTime(Date createTime) {
		this.createTime = createTime;
		return this;
	}
	/**
	 * 获取：创建时间
	 */
	public Date getCreateTime() {
		return createTime;
	}
	/**
	 * 设置：创建人
	 */
	public EqCamera setCreateBy(String createBy) {
		this.createBy = createBy;
		return this;
	}
	/**
	 * 获取：创建人
	 */
	public String getCreateBy() {
		return createBy;
	}
	/**
	 * 设置：摄像头用户配置信息ID
	 */
	public EqCamera setCameraUserId(Long cameraUserId) {
		this.cameraUserId = cameraUserId;
		return this;
	}
	/**
	 * 获取：摄像头用户配置信息ID
	 */
	public Long getCameraUserId() {
		return cameraUserId;
	}
	/**
	 * 设置：抓拍图片
	 */
	public EqCamera setPicurl(String picurl) {
		this.picurl = picurl;
		return this;
	}
	/**
	 * 获取：抓拍图片
	 */
	public String getPicurl() {
		return picurl;
	}

	public String getAppKey() {
		return appKey;
	}

	public void setAppKey(String appKey) {
		this.appKey = appKey;
	}

	public CameraUserInformation getCameraUserInformation() {
		return cameraUserInformation;
	}

	public void setCameraUserInformation(CameraUserInformation cameraUserInformation) {
		this.cameraUserInformation = cameraUserInformation;
	}


}
