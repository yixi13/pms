package com.pms.entity;
import com.baomidou.mybatisplus.activerecord.Model;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import javax.persistence.*;
import com.pms.util.Description;
import com.pms.entity.BaseModel;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;


/**
 * 
 * 
 * @author lk
 * @email 307169223@qq.com
 * @date 2018-08-20 09:57:14
 */
@TableName( "iot_repair_maintenance")
public class IotRepairMaintenance extends BaseModel<IotRepairMaintenance> {
private static final long serialVersionUID = 1L;


	    /**
	 *设备维修信息表
	 */
    @TableId("repair_maintenance_id")
    private Long repairMaintenanceId;
	
	    /**
     *设备申报信息ID
     */
    @TableField("repair_declare_id")
    @Description("设备申报信息ID")
    private Long repairDeclareId;
	
	    /**
     *水泵组名称
     */
    @TableField("group_name")
    @Description("水泵组名称")
    private String groupName;
	
	    /**
     *所属水泵房
     */
    @TableField("belonged_house")
    @Description("所属水泵房")
    private String belongedHouse;
	
	    /**
     *所属小区
     */
    @TableField("belonged_community")
    @Description("所属小区")
    private String belongedCommunity;
	
	    /**
     *开始时间
     */
    @TableField("start_data")
    @Description("开始时间")
    private Date startData;
	
	    /**
     *结束时间
     */
    @TableField("end_data")
    @Description("结束时间")
    private Date endData;
	
	    /**
     *维修状态1.维修成功2维修失败3.待处理
     */
    @TableField("state_")
    @Description("维修状态1.维修成功2维修失败3.待处理")
    private Integer state;
	
	    /**
     *维修费用
     */
    @TableField("maintenance_costs")
    @Description("维修费用")
    private BigDecimal maintenanceCosts;
	
	    /**
     *维修描述
     */
    @TableField("maintenance_description")
    @Description("维修描述")
    private String maintenanceDescription;
	
	    /**
     *水泵组ID
     */
    @TableField("group_id")
    @Description("水泵组ID")
    private Long groupId;


	@TableField(exist = false)
	private String acceptPerson;

	@TableField(exist = false)
	private String acceptPhone;

	@TableField(exist = false)
	private String description;

// ID赋值
public IotRepairMaintenance(){
        this.repairMaintenanceId= returnIdLong();
        }
@Override
protected Serializable pkVal() {
        return this.repairMaintenanceId;
}
	/**
	 * 设置：设备维修信息表
	 */
	public IotRepairMaintenance setRepairMaintenanceId(Long repairMaintenanceId) {
		this.repairMaintenanceId = repairMaintenanceId;
		return this;
	}
	/**
	 * 获取：设备维修信息表
	 */
	public Long getRepairMaintenanceId() {
		return repairMaintenanceId;
	}
	/**
	 * 设置：设备申报信息ID
	 */
	public IotRepairMaintenance setRepairDeclareId(Long repairDeclareId) {
		this.repairDeclareId = repairDeclareId;
		return this;
	}
	/**
	 * 获取：设备申报信息ID
	 */
	public Long getRepairDeclareId() {
		return repairDeclareId;
	}
	/**
	 * 设置：水泵组名称
	 */
	public IotRepairMaintenance setGroupName(String groupName) {
		this.groupName = groupName;
		return this;
	}
	/**
	 * 获取：水泵组名称
	 */
	public String getGroupName() {
		return groupName;
	}
	/**
	 * 设置：所属水泵房
	 */
	public IotRepairMaintenance setBelongedHouse(String belongedHouse) {
		this.belongedHouse = belongedHouse;
		return this;
	}
	/**
	 * 获取：所属水泵房
	 */
	public String getBelongedHouse() {
		return belongedHouse;
	}
	/**
	 * 设置：所属小区
	 */
	public IotRepairMaintenance setBelongedCommunity(String belongedCommunity) {
		this.belongedCommunity = belongedCommunity;
		return this;
	}
	/**
	 * 获取：所属小区
	 */
	public String getBelongedCommunity() {
		return belongedCommunity;
	}
	/**
	 * 设置：开始时间
	 */
	public IotRepairMaintenance setStartData(Date startData) {
		this.startData = startData;
		return this;
	}
	/**
	 * 获取：开始时间
	 */
	public Date getStartData() {
		return startData;
	}
	/**
	 * 设置：结束时间
	 */
	public IotRepairMaintenance setEndData(Date endData) {
		this.endData = endData;
		return this;
	}
	/**
	 * 获取：结束时间
	 */
	public Date getEndData() {
		return endData;
	}
	/**
	 * 设置：维修状态1.维修成功2维修失败3.待处理
	 */
	public IotRepairMaintenance setState(Integer state) {
		this.state = state;
		return this;
	}
	/**
	 * 获取：维修状态1.维修成功2维修失败3.待处理
	 */
	public Integer getState() {
		return state;
	}
	/**
	 * 设置：维修费用
	 */
	public IotRepairMaintenance setMaintenanceCosts(BigDecimal maintenanceCosts) {
		this.maintenanceCosts = maintenanceCosts;
		return this;
	}
	/**
	 * 获取：维修费用
	 */
	public BigDecimal getMaintenanceCosts() {
		return maintenanceCosts;
	}
	/**
	 * 设置：维修描述
	 */
	public IotRepairMaintenance setMaintenanceDescription(String maintenanceDescription) {
		this.maintenanceDescription = maintenanceDescription;
		return this;
	}
	/**
	 * 获取：维修描述
	 */
	public String getMaintenanceDescription() {
		return maintenanceDescription;
	}
	/**
	 * 设置：水泵组ID
	 */
	public IotRepairMaintenance setGroupId(Long groupId) {
		this.groupId = groupId;
		return this;
	}
	/**
	 * 获取：水泵组ID
	 */
	public Long getGroupId() {
		return groupId;
	}

	public String getAcceptPerson() {
		return acceptPerson;
	}

	public String getAcceptPhone() {
		return acceptPhone;
	}

	public String getDescription() {
		return description;
	}

	public void setAcceptPerson(String acceptPerson) {
		this.acceptPerson = acceptPerson;
	}

	public void setAcceptPhone(String acceptPhone) {
		this.acceptPhone = acceptPhone;
	}

	public void setDescription(String description) {
		this.description = description;
	}
}
