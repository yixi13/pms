package com.pms.entity;

import com.baomidou.mybatisplus.enums.IdType;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableName;
import java.io.Serializable;

/**
 * <p>
 * 报警类型表
 * </p>
 *
 * @author ljb
 * @since 2018-08-20
 */
@TableName("iot_alarm_type")
public class IotAlarmType extends Model<IotAlarmType> {

    private static final long serialVersionUID = 1L;

    /**
     * 报警记录类别
     */
	@TableId(value="id", type= IdType.AUTO)
	private Integer id;
    /**
     * 量名称
     */
	private String name;
    /**
     * 正常值
     */
	private String normalValue;
    /**
     * 报警值
     */
	private String alarmValue;
    /**
     * 相关量
     */
	private String correlationQuantity;
    /**
     * 数字编码
     */
	private String codeNo;


	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getNormalValue() {
		return normalValue;
	}

	public void setNormalValue(String normalValue) {
		this.normalValue = normalValue;
	}

	public String getAlarmValue() {
		return alarmValue;
	}

	public void setAlarmValue(String alarmValue) {
		this.alarmValue = alarmValue;
	}

	public String getCorrelationQuantity() {
		return correlationQuantity;
	}

	public void setCorrelationQuantity(String correlationQuantity) {
		this.correlationQuantity = correlationQuantity;
	}

	public String getCodeNo() {
		return codeNo;
	}

	public void setCodeNo(String codeNo) {
		this.codeNo = codeNo;
	}

	@Override
	protected Serializable pkVal() {
		return this.id;
	}

}
