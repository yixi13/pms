package com.pms.entity;
import com.baomidou.mybatisplus.activerecord.Model;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import javax.persistence.*;
import com.pms.util.Description;
import com.pms.entity.BaseModel;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;


/**
 * 
 * 
 * @author lk
 * @email 307169223@qq.com
 * @date 2018-08-20 09:57:14
 */
@TableName( "iot_card_recharge")
public class IotCardRecharge extends BaseModel<IotCardRecharge> {
private static final long serialVersionUID = 1L;


	    /**
	 *数据库充值表
	 */
    @TableId("card_id")
    private Long cardId;
	
	    /**
     *水泵组ID
     */
    @TableField("group_id")
    @Description("水泵组ID")
    private Long groupId;
	
	    /**
     *水泵房名称
     */
    @TableField("group_name")
    @Description("水泵房名称")
    private String groupName;
	
	    /**
     *充值总额
     */
    @TableField("total_amount")
    @Description("充值总额")
    private BigDecimal totalAmount;
	
	    /**
     *当前余额
     */
    @TableField("current_balance")
    @Description("当前余额")
    private BigDecimal currentBalance;
	
	    /**
     *手机卡号
     */
    @TableField("phone_")
    @Description("手机卡号")
    private String phone;
	
	    /**
     *最近充值人
     */
    @TableField("recently_name")
    @Description("最近充值人")
    private String recentlyName;
	
	    /**
     *最近充值金额
     */
    @TableField("recently_current_balance")
    @Description("最近充值金额")
    private BigDecimal recentlyCurrentBalance;
	
	    /**
     *最近充值时间
     */
    @TableField("recently_data")
    @Description("最近充值时间")
    private Date recentlyData;
	
	    /**
     *每月收费标准
     */
    @TableField("monthly_fee_standard")
    @Description("每月收费标准")
    private BigDecimal monthlyFeeStandard;
	
	    /**
     *状态1.正常2.余额不足3欠费
     */
    @TableField("state_")
    @Description("状态1.正常2.余额不足3欠费")
    private Integer state;
	
	    /**
     *创建人
     */
    @TableField("crt_time")
    @Description("创建人")
    private Date crtTime;
	
	    /**
     *创建时间
     */
    @TableField("crt_user")
    @Description("创建时间")
    private String crtUser;
	
	    /**
     *修改时间
     */
    @TableField("upd_time")
    @Description("修改时间")
    private Date updTime;
	
	    /**
     *修改人
     */
    @TableField("upd_user")
    @Description("修改人")
    private String updUser;

	@TableField(exist = false)
	private String linkPhone;

	@TableField(exist = false)
	private String linkName;
// ID赋值
public IotCardRecharge(){
        this.cardId= returnStaticIdLong();
        }
@Override
protected Serializable pkVal() {
        return this.cardId;
}
	/**
	 * 设置：数据库充值表
	 */
	public IotCardRecharge setCardId(Long cardId) {
		this.cardId = cardId;
		return this;
	}
	/**
	 * 获取：数据库充值表
	 */
	public Long getCardId() {
		return cardId;
	}
	/**
	 * 设置：水泵组ID
	 */
	public IotCardRecharge setGroupId(Long groupId) {
		this.groupId = groupId;
		return this;
	}
	/**
	 * 获取：水泵组ID
	 */
	public Long getGroupId() {
		return groupId;
	}
	/**
	 * 设置：水泵房名称
	 */
	public IotCardRecharge setGroupName(String groupName) {
		this.groupName = groupName;
		return this;
	}
	/**
	 * 获取：水泵房名称
	 */
	public String getGroupName() {
		return groupName;
	}
	/**
	 * 设置：充值总额
	 */
	public IotCardRecharge setTotalAmount(BigDecimal totalAmount) {
		this.totalAmount = totalAmount;
		return this;
	}
	/**
	 * 获取：充值总额
	 */
	public BigDecimal getTotalAmount() {
		return totalAmount;
	}
	/**
	 * 设置：当前余额
	 */
	public IotCardRecharge setCurrentBalance(BigDecimal currentBalance) {
		this.currentBalance = currentBalance;
		return this;
	}
	/**
	 * 获取：当前余额
	 */
	public BigDecimal getCurrentBalance() {
		return currentBalance;
	}
	/**
	 * 设置：手机卡号
	 */
	public IotCardRecharge setPhone(String phone) {
		this.phone = phone;
		return this;
	}
	/**
	 * 获取：手机卡号
	 */
	public String getPhone() {
		return phone;
	}
	/**
	 * 设置：最近充值人
	 */
	public IotCardRecharge setRecentlyName(String recentlyName) {
		this.recentlyName = recentlyName;
		return this;
	}
	/**
	 * 获取：最近充值人
	 */
	public String getRecentlyName() {
		return recentlyName;
	}
	/**
	 * 设置：最近充值金额
	 */
	public IotCardRecharge setRecentlyCurrentBalance(BigDecimal recentlyCurrentBalance) {
		this.recentlyCurrentBalance = recentlyCurrentBalance;
		return this;
	}
	/**
	 * 获取：最近充值金额
	 */
	public BigDecimal getRecentlyCurrentBalance() {
		return recentlyCurrentBalance;
	}
	/**
	 * 设置：最近充值时间
	 */
	public IotCardRecharge setRecentlyData(Date recentlyData) {
		this.recentlyData = recentlyData;
		return this;
	}
	/**
	 * 获取：最近充值时间
	 */
	public Date getRecentlyData() {
		return recentlyData;
	}
	/**
	 * 设置：每月收费标准
	 */
	public IotCardRecharge setMonthlyFeeStandard(BigDecimal monthlyFeeStandard) {
		this.monthlyFeeStandard = monthlyFeeStandard;
		return this;
	}
	/**
	 * 获取：每月收费标准
	 */
	public BigDecimal getMonthlyFeeStandard() {
		return monthlyFeeStandard;
	}
	/**
	 * 设置：状态1.正常2.余额不足3欠费
	 */
	public IotCardRecharge setState(Integer state) {
		this.state = state;
		return this;
	}
	/**
	 * 获取：状态1.正常2.余额不足3欠费
	 */
	public Integer getState() {
		return state;
	}
	/**
	 * 设置：创建人
	 */
	public IotCardRecharge setCrtTime(Date crtTime) {
		this.crtTime = crtTime;
		return this;
	}
	/**
	 * 获取：创建人
	 */
	public Date getCrtTime() {
		return crtTime;
	}
	/**
	 * 设置：创建时间
	 */
	public IotCardRecharge setCrtUser(String crtUser) {
		this.crtUser = crtUser;
		return this;
	}
	/**
	 * 获取：创建时间
	 */
	public String getCrtUser() {
		return crtUser;
	}
	/**
	 * 设置：修改时间
	 */
	public IotCardRecharge setUpdTime(Date updTime) {
		this.updTime = updTime;
		return this;
	}
	/**
	 * 获取：修改时间
	 */
	public Date getUpdTime() {
		return updTime;
	}
	/**
	 * 设置：修改人
	 */
	public IotCardRecharge setUpdUser(String updUser) {
		this.updUser = updUser;
		return this;
	}
	/**
	 * 获取：修改人
	 */
	public String getUpdUser() {
		return updUser;
	}

	public String getLinkPhone() {
		return linkPhone;
	}

	public String getLinkName() {
		return linkName;
	}

	public void setLinkPhone(String linkPhone) {
		this.linkPhone = linkPhone;
	}

	public void setLinkName(String linkName) {
		this.linkName = linkName;
	}
}
