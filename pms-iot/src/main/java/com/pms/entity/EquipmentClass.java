package com.pms.entity;
import com.baomidou.mybatisplus.activerecord.Model;
import java.io.Serializable;
import java.util.Date;
import javax.persistence.*;
import com.pms.util.Description;
import com.pms.entity.BaseModel;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;


/**
 * 
 * 
 * @author lk
 * @email
 * @date 2018-04-25 14:26:06
 */
@TableName( "equipment_class")
public class EquipmentClass extends BaseModel<EquipmentClass> {
private static final long serialVersionUID = 1L;


	    /**
	 *
	 */
    @TableId("id")
    private Long id;
	
	    /**
     *分类名称
     */
    @TableField("class_name")
    @Description("分类名称")
    private String className;
	
// ID赋值
public EquipmentClass(){
        this.id= returnIdLong();
        }
@Override
protected Serializable pkVal() {
        return this.id;
}
	/**
	 * 设置：
	 */
	public EquipmentClass setId(Long id) {
		this.id = id;
		return this;
	}
	/**
	 * 获取：
	 */
	public Long getId() {
		return id;
	}
	/**
	 * 设置：分类名称
	 */
	public EquipmentClass setClassName(String className) {
		this.className = className;
		return this;
	}
	/**
	 * 获取：分类名称
	 */
	public String getClassName() {
		return className;
	}


}
