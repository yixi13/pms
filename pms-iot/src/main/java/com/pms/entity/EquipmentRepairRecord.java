package com.pms.entity;
import com.baomidou.mybatisplus.activerecord.Model;
import java.io.Serializable;
import java.util.Date;
import javax.persistence.*;
import com.pms.util.Description;
import com.pms.entity.BaseModel;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;


/**
 * 
 *
 * @author lk
 * @email
 * @date 2018-04-25 14:26:06
 */
@TableName( "equipment_repair_record")
public class EquipmentRepairRecord extends BaseModel<EquipmentRepairRecord> {
private static final long serialVersionUID = 1L;


	    /**
	 *
	 */
    @TableId("id")
    private Long id;
	
	    /**
     *设备id
     */
    @TableField("eq_id")
    @Description("设备id")
    private Long eqId;
	
	    /**
     *设备名称
     */
    @TableField("eq_name")
    @Description("设备名称")
    private String eqName;
	
	    /**
     *设备类型id
     */
    @TableField("eq_class_id")
    @Description("设备类型id")
    private Long eqClassId;
	
	    /**
     *设备型号
     */
    @TableField("eq_model")
    @Description("设备型号")
    private String eqModel;
	
	    /**
     *上报时间（创建时间）
     */
    @TableField("create_time")
    @Description("上报时间（创建时间）")
    private Date createTime;
	
	    /**
     *问题详情描述
     */
    @TableField("problem_descr")
    @Description("问题详情描述")
    private String problemDescr;
	
	    /**
     *报修人会员id
     */
    @TableField("repair_member_id")
    @Description("报修人会员id")
    private Long repairMemberId;
	
	    /**
     *报修人电话
     */
    @TableField("repair_phone")
    @Description("报修人电话")
    private String repairPhone;
	
	    /**
     *具体地址
     */
    @TableField("address")
    @Description("具体地址")
    private String address;
	
	    /**
     *是否处理：1.未处理，2.已处理
     */
    @TableField("is_handle")
    @Description("是否处理：1.未处理，2.已处理")
    private Integer isHandle;
	
	    /**
     *处理时间
     */
    @TableField("handle_time")
    @Description("处理时间")
    private Date handleTime;
	
	    /**
     *处理人会员id
     */
    @TableField("handle_member_id")
    @Description("处理人会员id")
    private Long handleMemberId;
	
	    /**
     *处理人电话
     */
    @TableField("handle_phone")
    @Description("处理人电话")
    private String handlePhone;
	
	    /**
     *处理回复
     */
    @TableField("handle_reply")
    @Description("处理回复")
    private String handleReply;
	
// ID赋值
public EquipmentRepairRecord(){
        this.id= returnIdLong();
        }
@Override
protected Serializable pkVal() {
        return this.id;
}
	/**
	 * 设置：
	 */
	public EquipmentRepairRecord setId(Long id) {
		this.id = id;
		return this;
	}
	/**
	 * 获取：
	 */
	public Long getId() {
		return id;
	}
	/**
	 * 设置：设备id
	 */
	public EquipmentRepairRecord setEqId(Long eqId) {
		this.eqId = eqId;
		return this;
	}
	/**
	 * 获取：设备id
	 */
	public Long getEqId() {
		return eqId;
	}
	/**
	 * 设置：设备名称
	 */
	public EquipmentRepairRecord setEqName(String eqName) {
		this.eqName = eqName;
		return this;
	}
	/**
	 * 获取：设备名称
	 */
	public String getEqName() {
		return eqName;
	}
	/**
	 * 设置：设备类型id
	 */
	public EquipmentRepairRecord setEqClassId(Long eqClassId) {
		this.eqClassId = eqClassId;
		return this;
	}
	/**
	 * 获取：设备类型id
	 */
	public Long getEqClassId() {
		return eqClassId;
	}
	/**
	 * 设置：设备型号
	 */
	public EquipmentRepairRecord setEqModel(String eqModel) {
		this.eqModel = eqModel;
		return this;
	}
	/**
	 * 获取：设备型号
	 */
	public String getEqModel() {
		return eqModel;
	}
	/**
	 * 设置：上报时间（创建时间）
	 */
	public EquipmentRepairRecord setCreateTime(Date createTime) {
		this.createTime = createTime;
		return this;
	}
	/**
	 * 获取：上报时间（创建时间）
	 */
	public Date getCreateTime() {
		return createTime;
	}
	/**
	 * 设置：问题详情描述
	 */
	public EquipmentRepairRecord setProblemDescr(String problemDescr) {
		this.problemDescr = problemDescr;
		return this;
	}
	/**
	 * 获取：问题详情描述
	 */
	public String getProblemDescr() {
		return problemDescr;
	}
	/**
	 * 设置：报修人会员id
	 */
	public EquipmentRepairRecord setRepairMemberId(Long repairMemberId) {
		this.repairMemberId = repairMemberId;
		return this;
	}
	/**
	 * 获取：报修人会员id
	 */
	public Long getRepairMemberId() {
		return repairMemberId;
	}
	/**
	 * 设置：报修人电话
	 */
	public EquipmentRepairRecord setRepairPhone(String repairPhone) {
		this.repairPhone = repairPhone;
		return this;
	}
	/**
	 * 获取：报修人电话
	 */
	public String getRepairPhone() {
		return repairPhone;
	}
	/**
	 * 设置：具体地址
	 */
	public EquipmentRepairRecord setAddress(String address) {
		this.address = address;
		return this;
	}
	/**
	 * 获取：具体地址
	 */
	public String getAddress() {
		return address;
	}
	/**
	 * 设置：是否处理：1.未处理，2.已处理
	 */
	public EquipmentRepairRecord setIsHandle(Integer isHandle) {
		this.isHandle = isHandle;
		return this;
	}
	/**
	 * 获取：是否处理：1.未处理，2.已处理
	 */
	public Integer getIsHandle() {
		return isHandle;
	}
	/**
	 * 设置：处理时间
	 */
	public EquipmentRepairRecord setHandleTime(Date handleTime) {
		this.handleTime = handleTime;
		return this;
	}
	/**
	 * 获取：处理时间
	 */
	public Date getHandleTime() {
		return handleTime;
	}
	/**
	 * 设置：处理人会员id
	 */
	public EquipmentRepairRecord setHandleMemberId(Long handleMemberId) {
		this.handleMemberId = handleMemberId;
		return this;
	}
	/**
	 * 获取：处理人会员id
	 */
	public Long getHandleMemberId() {
		return handleMemberId;
	}
	/**
	 * 设置：处理人电话
	 */
	public EquipmentRepairRecord setHandlePhone(String handlePhone) {
		this.handlePhone = handlePhone;
		return this;
	}
	/**
	 * 获取：处理人电话
	 */
	public String getHandlePhone() {
		return handlePhone;
	}
	/**
	 * 设置：处理回复
	 */
	public EquipmentRepairRecord setHandleReply(String handleReply) {
		this.handleReply = handleReply;
		return this;
	}
	/**
	 * 获取：处理回复
	 */
	public String getHandleReply() {
		return handleReply;
	}


}
