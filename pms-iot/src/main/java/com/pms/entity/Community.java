package com.pms.entity;

import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableName;

import java.io.Serializable;
import java.util.Date;

/**
 * <p>
 * 社区（小区）表
 * </p>
 *
 * @author lk
 * @since 2017-12-25
 */
@TableName("community")
public class Community extends BaseModel<Community> {
    private static final long serialVersionUID = 1L;

    @TableId("community_id")
	private Long communityId;
    /**
     * 小区名称
     */
	@TableField("community_name")
	private String communityName;
    /**
     * 小区地址
     */
	@TableField("community_address")
	private String communityAddress;
    /**
     * 所属区域编号
     */
	@TableField("area_gb")
	private Integer areaGb;
    /**
     * 所属区域名称
     */
	@TableField("area_name")
	private String areaName;
	/**
	 * 水泵所属公司id
	 */
	@TableField("company_id")
	private Long companyId;
	/**
	 * 水泵所属公司名称
	 */
	@TableField("company_name")
	private String companyName;
	/**
	 * 更新时间
	 */
	@TableField("update_time")
	private Date updateTime;
	/**
	 * 更新者
	 */
	@TableField("update_by")
	private String updateBy;
	/**
	 * 创建时间
	 */
	@TableField("create_time")
	private Date createTime;
	/**
	 * 创建者
	 */
	@TableField("create_by")
	private String createBy;
	/**
	 * 社区排序
	 */
	@TableField("community_sort")
	private Long communitySort;
	/**
	 * 对应lbs数据id
	 */
	@TableField("lbs_poi_id")
	private String lbsPoiId;
	/**
	 * 经度
	 */
	@TableField("longitude")
	private Double longitude;
	/**
	 * 纬度
	 */
	@TableField("latitude")
	private Double latitude;

	public String getLbsPoiId() {
		return lbsPoiId;
	}

	public void setLbsPoiId(String lbsPoiId) {
		this.lbsPoiId = lbsPoiId;
	}

	public Double getLongitude() {
		return longitude;
	}

	public void setLongitude(Double longitude) {
		this.longitude = longitude;
	}

	public Double getLatitude() {
		return latitude;
	}

	public void setLatitude(Double latitude) {
		this.latitude = latitude;
	}

	public Long getCommunitySort() {
		return communitySort;
	}

	public void setCommunitySort(Long communitySort) {
		this.communitySort = communitySort;
	}

	public Long getCommunityId() {
		return communityId;
	}

	public void setCommunityId(Long communityId) {
		this.communityId = communityId;
	}

	public String getCommunityName() {
		return communityName;
	}

	public void setCommunityName(String communityName) {
		this.communityName = communityName;
	}

	public String getCommunityAddress() {
		return communityAddress;
	}

	public void setCommunityAddress(String communityAddress) {
		this.communityAddress = communityAddress;
	}

	public Integer getAreaGb() {
		return areaGb;
	}

	public void setAreaGb(Integer areaGb) {
		this.areaGb = areaGb;
	}

	public String getAreaName() {
		return areaName;
	}

	public void setAreaName(String areaName) {
		this.areaName = areaName;
	}

	public Long getCompanyId() {
		return companyId;
	}

	public void setCompanyId(Long companyId) {
		this.companyId = companyId;
	}

	public String getCompanyName() {
		return companyName;
	}

	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}

	public Date getUpdateTime() {
		return updateTime;
	}

	public void setUpdateTime(Date updateTime) {
		this.updateTime = updateTime;
	}

	public String getUpdateBy() {
		return updateBy;
	}

	public void setUpdateBy(String updateBy) {
		this.updateBy = updateBy;
	}

	public Date getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	public String getCreateBy() {
		return createBy;
	}

	public void setCreateBy(String createBy) {
		this.createBy = createBy;
	}

	@Override
	protected Serializable pkVal() {
		return this.communityId;
	}
	// ID赋值
	public Community(){
		this.communityId= returnIdLong();
	}
}
