package com.pms.entity;

import java.util.Date;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;
import com.pms.utils.FiledMark;

import java.io.Serializable;

/**
 * <p>
 * 
 * </p>
 *
 * @author asus
 * @since 2018-01-10
 */
@TableName("water_pump_group_attr")
public class WaterPumpGroupAttr extends BaseModel<WaterPumpGroupAttr> {

    private static final long serialVersionUID = 1L;
	@TableId("waterPumpGroupId")
	private Long waterPumpGroupId;
    /**
     * 上报时间-unix时间戳
     */
	@FiledMark(comment="上报时间",desc="unix时间戳格式",unit="")
	private Long reportTime;
    /**
     * 设备组名称
     */
	private String deviceName;
    /**
     * 设备状态(0-在线)
     */
	private Integer deviceStatus;
    /**
     * 水泵频率 -(1个变频器对应所有泵,1个变频器对应一个泵)
     */
	@TableField("waterFrequency")
	@FiledMark(comment="水泵频率",desc="",unit="Hz")
	private Integer waterFrequency;
    /**
     * 反馈压力
     */
	@FiledMark(comment="反馈压力",desc="即出口压力",unit="Mpa")
	private Double pressureFeedback;
    /**
     * 设定压力
     */
	@FiledMark(comment="设定压力",desc="",unit="Mpa")
	private Double settingPressure;
    /**
     * 超压报警
     */
	@FiledMark(comment="超压报警",desc="压力极限值",unit="Mpa")
	private Double ultraTightlyAlarm;
    /**
     * 添加时间
     */
	private Date createTime;
	@FiledMark(comment="泵运行数量",desc="",unit="台")
	private Integer count;

	@FiledMark(comment="上报时间",desc="",unit="")
	private Date reportTimeFormat;
	@FiledMark(comment="最新故障码",desc="0-正常,1-水位低,2-变频器故障,3-超压报警,4-传感器故障,5-自检故障" +
	",6/9/12/15/18/21 - 1/2/3/4/5/6号泵故障,7/10/13/16/19/22 -1/2/3/4/5/6号泵过流,8/11/14/17/20/23 - 1/2/3/4/5/6号泵缺相,"
	,unit="")
	private Integer ZXGZ;
	@FiledMark(comment="下行频率",desc="",unit="Hz")
	private Integer XXPL;
	@FiledMark(comment="水泵状态",desc="根据,分割字符串顺序获取每个泵的状态(0-变频运行,1-工频运行,2-未运行(停机),3-电磁阀,4-未使用,5-故障,6-检修,7-现场手动)",unit="")
	private String SBZT;
	@FiledMark(comment="工作状态",desc="根据,分割字符串获取工作状态(0-自动运行,1-变频器运行,2-巡检,3-休眠,4-消防,5-远控,6-限压运行,7-限压停机,8-定时停机,其余未使用)",unit="")
	private String GZZT;
	@FiledMark(comment="市政管网压力",desc="即进口压力",unit="Mpa")
	private Double SZGWYL;
	@FiledMark(comment="是否故障状态",desc="0-正常,1-故障",unit="")
	private Integer isFault;

	public Long getWaterPumpGroupId() {
		return waterPumpGroupId;
	}

	public void setWaterPumpGroupId(Long waterPumpGroupId) {
		this.waterPumpGroupId = waterPumpGroupId;
	}

	public Long getReportTime() {
		return reportTime;
	}

	public void setReportTime(Long reportTime) {
		this.reportTime = reportTime;
	}

	public Integer getZXGZ() {
		return ZXGZ;
	}

	public void setZXGZ(Integer ZXGZ) {
		this.ZXGZ = ZXGZ;
	}

	public Integer getXXPL() {
		return XXPL;
	}

	public void setXXPL(Integer XXPL) {
		this.XXPL = XXPL;
	}

	public String getSBZT() {
		return SBZT;
	}

	public void setSBZT(String SBZT) {
		this.SBZT = SBZT;
	}

	public String getGZZT() {
		return GZZT;
	}

	public void setGZZT(String GZZT) {
		this.GZZT = GZZT;
	}

	public Double getSZGWYL() {
		return SZGWYL;
	}

	public void setSZGWYL(Double SZGWYL) {
		this.SZGWYL = SZGWYL;
	}

	public Integer getIsFault() {
		return isFault;
	}

	public void setIsFault(Integer isFault) {
		this.isFault = isFault;
	}

	public Date getReportTimeFormat() {
		return reportTimeFormat;
	}

	public void setReportTimeFormat(Date reportTimeFormat) {
		this.reportTimeFormat = reportTimeFormat;
	}

	public Integer getCount() {
		return count;
	}

	public void setCount(Integer count) {
		this.count = count;
	}

	public String getDeviceName() {
		return deviceName;
	}

	public void setDeviceName(String deviceName) {
		this.deviceName = deviceName;
	}

	public Integer getDeviceStatus() {
		return deviceStatus;
	}

	public void setDeviceStatus(Integer deviceStatus) {
		this.deviceStatus = deviceStatus;
	}

	public Integer getWaterFrequency() {
		return waterFrequency;
	}

	public void setWaterFrequency(Integer waterFrequency) {
		this.waterFrequency = waterFrequency;
	}

	public Double getPressureFeedback() {
		return pressureFeedback;
	}

	public void setPressureFeedback(Double pressureFeedback) {
		this.pressureFeedback = pressureFeedback;
	}

	public Double getSettingPressure() {
		return settingPressure;
	}

	public void setSettingPressure(Double settingPressure) {
		this.settingPressure = settingPressure;
	}

	public Double getUltraTightlyAlarm() {
		return ultraTightlyAlarm;
	}

	public void setUltraTightlyAlarm(Double ultraTightlyAlarm) {
		this.ultraTightlyAlarm = ultraTightlyAlarm;
	}

	public Date getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	@Override
	protected Serializable pkVal() {
		return this.waterPumpGroupId;
	}
	// ID赋值
	public WaterPumpGroupAttr(){
		this.waterPumpGroupId= returnIdLong();
	}
}
