package com.pms.entity;
import com.baomidou.mybatisplus.activerecord.Model;
import java.io.Serializable;
import java.util.Date;
import javax.persistence.*;
import com.pms.util.Description;
import com.pms.entity.BaseModel;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;


/**
 * 角色二汞机构关联表
 * @author ljb
 * @email 517770986@qq.com
 * @date 2018-07-31 10:28:59
 */
@TableName( "iot_group_agency")
public class GroupIotAgency extends BaseModel<GroupIotAgency> {
private static final long serialVersionUID = 1L;


	/**
	 *角色机构
	 */
    @TableId("id")
    private Long id;
	
	    /**
     *角色ID
     */
    @TableField("group_id")
    @Description("角色ID")
    private Long groupId;
	
	    /**
     *机构ID
     */
    @TableField("agency_id")
    @Description("机构ID")
    private Long agencyId;
	
// ID赋值
public GroupIotAgency(){
        this.id= returnIdLong();
        }
@Override
protected Serializable pkVal() {
        return this.id;
}
	/**
	 * 设置：角色机构
	 */
	public GroupIotAgency setId(Long id) {
		this.id = id;
		return this;
	}
	/**
	 * 获取：角色机构
	 */
	public Long getId() {
		return id;
	}
	/**
	 * 设置：角色ID
	 */
	public GroupIotAgency setGroupId(Long groupId) {
		this.groupId = groupId;
		return this;
	}
	/**
	 * 获取：角色ID
	 */
	public Long getGroupId() {
		return groupId;
	}
	/**
	 * 设置：机构ID
	 */
	public GroupIotAgency setAgencyId(Long agencyId) {
		this.agencyId = agencyId;
		return this;
	}
	/**
	 * 获取：机构ID
	 */
	public Long getAgencyId() {
		return agencyId;
	}


}
