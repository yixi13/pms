package com.pms.entity;
import java.io.Serializable;
import java.util.Date;
import com.pms.util.Description;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;


/**
 * 
 * 
 * @author zyl
 * @email 517770986@qq.com
 * @date 2018-03-15 11:21:02
 */
@TableName( "camera_user_information")
public class CameraUserInformation extends BaseModel<CameraUserInformation> {
private static final long serialVersionUID = 1L;


	 /**
	 *
	 */
    @TableId("id_")
    private Long id;
	
	    /**
     *账号
     */
    @TableField("appKey")
    @Description("账号")
    private String appkey;
	
	    /**
     *密码
     */
    @TableField("appSecret")
    @Description("密码")
    private String appsecret;
	
	    /**
     *用户别名
     */
    @TableField("user_name")
    @Description("用户别名")
    private String userName;
	
	    /**
     *统计总计
     */
    @TableField("sum_")
    @Description("统计总计")
    private Integer sum;
	
	    /**
     *
     */
    @TableField("create_time")
    @Description("")
    private Date createTime;
	
// ID赋值
public CameraUserInformation(){
        this.id= returnIdLong();
        }
@Override
protected Serializable pkVal() {
        return this.id;
}
	/**
	 * 设置：
	 */
	public CameraUserInformation setId(Long id) {
		this.id = id;
		return this;
	}
	/**
	 * 获取：
	 */
	public Long getId() {
		return id;
	}
	/**
	 * 设置：账号
	 */
	public CameraUserInformation setAppkey(String appkey) {
		this.appkey = appkey;
		return this;
	}
	/**
	 * 获取：账号
	 */
	public String getAppkey() {
		return appkey;
	}
	/**
	 * 设置：密码
	 */
	public CameraUserInformation setAppsecret(String appsecret) {
		this.appsecret = appsecret;
		return this;
	}
	/**
	 * 获取：密码
	 */
	public String getAppsecret() {
		return appsecret;
	}
	/**
	 * 设置：用户别名
	 */
	public CameraUserInformation setUserName(String userName) {
		this.userName = userName;
		return this;
	}
	/**
	 * 获取：用户别名
	 */
	public String getUserName() {
		return userName;
	}
	/**
	 * 设置：统计总计
	 */
	public CameraUserInformation setSum(Integer sum) {
		this.sum = sum;
		return this;
	}
	/**
	 * 获取：统计总计
	 */
	public Integer getSum() {
		return sum;
	}
	/**
	 * 设置：
	 */
	public CameraUserInformation setCreateTime(Date createTime) {
		this.createTime = createTime;
		return this;
	}
	/**
	 * 获取：
	 */
	public Date getCreateTime() {
		return createTime;
	}


}
