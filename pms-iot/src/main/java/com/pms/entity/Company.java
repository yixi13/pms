package com.pms.entity;

import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableName;

import java.io.Serializable;
import java.util.Date;

/**
 * <p>
 * 水泵所属公司表
 * </p>
 *
 * @author lk
 * @since 2017-12-25
 */
@TableName("company")
public class Company extends BaseModel<Company> {
    private static final long serialVersionUID = 1L;

    @TableId("company_id")
	private Long companyId;
    /**
     * 公司名称
     */
	@TableField("company_name")
	private String companyName;
	/**
	 * 公司简称
	 */
	@TableField("short_name")
	private String shortName;
    /**
     * 公司地址
     */
	@TableField("company_address")
	private String companyAddress;
    /**
     * 公司简介
     */
	@TableField("company_intro")
	private String companyIntro;
	/**
	 * 更新时间
	 */
	@TableField("update_time")
	private Date updateTime;
	/**
	 * 更新者
	 */
	@TableField("update_by")
	private String updateBy;
	/**
	 * 创建时间
	 */
	@TableField("create_time")
	private Date createTime;
	/**
	 * 创建者
	 */
	@TableField("create_by")
	private String createBy;


	public Long getCompanyId() {
		return companyId;
	}

	public void setCompanyId(Long companyId) {
		this.companyId = companyId;
	}

	public String getCompanyName() {
		return companyName;
	}

	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}

	public String getCompanyAddress() {
		return companyAddress;
	}

	public void setCompanyAddress(String companyAddress) {
		this.companyAddress = companyAddress;
	}

	public String getCompanyIntro() {
		return companyIntro;
	}

	public void setCompanyIntro(String companyIntro) {
		this.companyIntro = companyIntro;
	}

	public Date getUpdateTime() {
		return updateTime;
	}

	public void setUpdateTime(Date updateTime) {
		this.updateTime = updateTime;
	}

	public String getUpdateBy() {
		return updateBy;
	}

	public void setUpdateBy(String updateBy) {
		this.updateBy = updateBy;
	}

	public Date getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	public String getCreateBy() {
		return createBy;
	}

	public void setCreateBy(String createBy) {
		this.createBy = createBy;
	}

	public String getShortName() {
		return shortName;
	}

	public void setShortName(String shortName) {
		this.shortName = shortName;
	}

	@Override
	protected Serializable pkVal() {
		return this.companyId;
	}

	// ID赋值
	public Company(){
		this.companyId= returnIdLong();
	}

}
