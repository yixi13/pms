package com.pms.entity;

import java.util.Date;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableName;
import com.pms.utils.FiledMark;

import java.io.Serializable;

/**
 * <p>
 * 
 * </p>
 *
 * @author asus
 * @since 2018-07-23
 */
@TableName("iot_water_pump_group_attr")
public class IotWaterPumpGroupAttr extends Model<IotWaterPumpGroupAttr> {

    private static final long serialVersionUID = 1L;

    /**
     * 主键
     */
	private Long waterPumpGroupId;
    /**
     * 上报时间-unix时间戳
     */
	private Long reportTime;
    /**
     * 设备组名称
     */
	private String deviceName;
    /**
     * 设备状态(0-在线)
     */
	private Integer deviceStatus;
    /**
     * 水泵频率，单位Hz
     */
	private String waterFrequency;
    /**
     * 反馈压力,单位Mpa
     */
	private Double pressureFeedback;
    /**
     * 设定压力,单位Mpa
     */
	private Double settingPressure;
    /**
     * 超压报警,单位Mpa
     */
	private Double ultraTightlyAlarm;
    /**
     * 添加时间
     */
	private Date createTime;
    /**
     * 运行数量,单位台
     */
	private Integer count;
    /**
     * 格式化后的上报时间
     */
	private String reportTimeFormat;
    /**
     * 最新故障
     */
	@TableField("ZXGZ")
	private Integer zxgz;
    /**
     * 下行频率，单位Hz
     */
	@TableField("XXPL")
	private Integer xxpl;
    /**
     * 水泵状态
     */
	@FiledMark(comment="水泵状态",desc="根据,分割字符串顺序获取每个泵的状态(0-变频运行,1-工频运行,2-未运行(停机),3-电磁阀,4-未使用,5-故障,6-检修,7-现场手动)",unit="")
	@TableField("SBZT")
	private String sbzt;
    /**
     * 工作状态
     */
	@TableField("GZZT")
	private String gzzt;
    /**
     * 市政管网压力,单位Mpa
     */
	@TableField("SZGWYL")
	private Double szgwyl;
    /**
     * 是否故障(0-不故障，1-故障)
     */
	private Integer isFault;


	public Long getWaterPumpGroupId() {
		return waterPumpGroupId;
	}

	public void setWaterPumpGroupId(Long waterPumpGroupId) {
		this.waterPumpGroupId = waterPumpGroupId;
	}

	public Long getReportTime() {
		return reportTime;
	}

	public void setReportTime(Long reportTime) {
		this.reportTime = reportTime;
	}

	public String getDeviceName() {
		return deviceName;
	}

	public void setDeviceName(String deviceName) {
		this.deviceName = deviceName;
	}

	public Integer getDeviceStatus() {
		return deviceStatus;
	}

	public void setDeviceStatus(Integer deviceStatus) {
		this.deviceStatus = deviceStatus;
	}

	public String getWaterFrequency() {
		return waterFrequency;
	}

	public void setWaterFrequency(String waterFrequency) {
		this.waterFrequency = waterFrequency;
	}

	public Double getPressureFeedback() {
		return pressureFeedback;
	}

	public void setPressureFeedback(Double pressureFeedback) {
		this.pressureFeedback = pressureFeedback;
	}

	public Double getSettingPressure() {
		return settingPressure;
	}

	public void setSettingPressure(Double settingPressure) {
		this.settingPressure = settingPressure;
	}

	public Double getUltraTightlyAlarm() {
		return ultraTightlyAlarm;
	}

	public void setUltraTightlyAlarm(Double ultraTightlyAlarm) {
		this.ultraTightlyAlarm = ultraTightlyAlarm;
	}

	public Date getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	public Integer getCount() {
		return count;
	}

	public void setCount(Integer count) {
		this.count = count;
	}

	public String getReportTimeFormat() {
		return reportTimeFormat;
	}

	public void setReportTimeFormat(String reportTimeFormat) {
		this.reportTimeFormat = reportTimeFormat;
	}

	public Integer getZxgz() {
		return zxgz;
	}

	public void setZxgz(Integer zxgz) {
		this.zxgz = zxgz;
	}

	public Integer getXxpl() {
		return xxpl;
	}

	public void setXxpl(Integer xxpl) {
		this.xxpl = xxpl;
	}

	public String getSbzt() {
		return sbzt;
	}

	public void setSbzt(String sbzt) {
		this.sbzt = sbzt;
	}

	public String getGzzt() {
		return gzzt;
	}

	public void setGzzt(String gzzt) {
		this.gzzt = gzzt;
	}

	public Double getSzgwyl() {
		return szgwyl;
	}

	public void setSzgwyl(Double szgwyl) {
		this.szgwyl = szgwyl;
	}

	public Integer getIsFault() {
		return isFault;
	}

	public void setIsFault(Integer isFault) {
		this.isFault = isFault;
	}

	@Override
	protected Serializable pkVal() {
		return this.waterPumpGroupId;
	}

}
