package com.pms.entity;
import com.baomidou.mybatisplus.activerecord.Model;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import javax.persistence.*;
import com.pms.util.Description;
import com.pms.entity.BaseModel;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;


/**
 * 
 * 
 * @author lk
 * @email 307169223@qq.com
 * @date 2018-08-20 09:57:14
 */
@TableName( "iot_card_recharge_historical_records")
public class IotCardRechargeHistoricalRecords extends BaseModel<IotCardRechargeHistoricalRecords> {
private static final long serialVersionUID = 1L;


	    /**
	 *充值记录
	 */
    @TableId("id")
    private Long id;
	
	    /**
     *数据卡ID
     */
    @TableField("card_id")
    @Description("数据卡ID")
    private Long cardId;
	
	    /**
     *最近充值人
     */
    @TableField("recently_name")
    @Description("最近充值人")
    private String recentlyName;


	/**
	 *充值后余额
	 */
	@TableField("current_money")
	@Description("充值后余额")
	private BigDecimal currentMoney;
	    /**
     *最近充值金额
     */
    @TableField("recently_current_balance")
    @Description("最近充值金额")
    private BigDecimal recentlyCurrentBalance;
	
	    /**
     *最近充值时间
     */
    @TableField("recently_data")
    @Description("最近充值时间")
    private Date recentlyData;
	
	    /**
     *水泵房ID
     */
    @TableField("group_id")
    @Description("水泵房ID")
    private Long groupId;
	
	    /**
     *操作时间
     */
    @TableField("crt_time")
    @Description("操作时间")
    private Date crtTime;
	
	    /**
     *操作人
     */
    @TableField("crt_user")
    @Description("操作人")
    private String crtUser;
	
	    /**
     *修改时间
     */
    @TableField("upd_time")
    @Description("修改时间")
    private Date updTime;
	
	    /**
     *修改人
     */
    @TableField("upd_user")
    @Description("修改人")
    private String updUser;

	public BigDecimal getCurrentMoney() {
		return currentMoney;
	}

	public void setCurrentMoney(BigDecimal currentMoney) {
		this.currentMoney = currentMoney;
	}

	// ID赋值
public IotCardRechargeHistoricalRecords(){
        this.id= returnIdLong();
        }
@Override
protected Serializable pkVal() {
        return this.id;
}
	/**
	 * 设置：充值记录
	 */
	public IotCardRechargeHistoricalRecords setId(Long id) {
		this.id = id;
		return this;
	}
	/**
	 * 获取：充值记录
	 */
	public Long getId() {
		return id;
	}
	/**
	 * 设置：数据卡ID
	 */
	public IotCardRechargeHistoricalRecords setCardId(Long cardId) {
		this.cardId = cardId;
		return this;
	}
	/**
	 * 获取：数据卡ID
	 */
	public Long getCardId() {
		return cardId;
	}
	/**
	 * 设置：最近充值人
	 */
	public IotCardRechargeHistoricalRecords setRecentlyName(String recentlyName) {
		this.recentlyName = recentlyName;
		return this;
	}
	/**
	 * 获取：最近充值人
	 */
	public String getRecentlyName() {
		return recentlyName;
	}
	/**
	 * 设置：最近充值金额
	 */
	public IotCardRechargeHistoricalRecords setRecentlyCurrentBalance(BigDecimal recentlyCurrentBalance) {
		this.recentlyCurrentBalance = recentlyCurrentBalance;
		return this;
	}
	/**
	 * 获取：最近充值金额
	 */
	public BigDecimal getRecentlyCurrentBalance() {
		return recentlyCurrentBalance;
	}
	/**
	 * 设置：最近充值时间
	 */
	public IotCardRechargeHistoricalRecords setRecentlyData(Date recentlyData) {
		this.recentlyData = recentlyData;
		return this;
	}
	/**
	 * 获取：最近充值时间
	 */
	public Date getRecentlyData() {
		return recentlyData;
	}
	/**
	 * 设置：水泵房ID
	 */
	public IotCardRechargeHistoricalRecords setGroupId(Long groupId) {
		this.groupId = groupId;
		return this;
	}
	/**
	 * 获取：水泵房ID
	 */
	public Long getGroupId() {
		return groupId;
	}
	/**
	 * 设置：操作时间
	 */
	public IotCardRechargeHistoricalRecords setCrtTime(Date crtTime) {
		this.crtTime = crtTime;
		return this;
	}
	/**
	 * 获取：操作时间
	 */
	public Date getCrtTime() {
		return crtTime;
	}
	/**
	 * 设置：操作人
	 */
	public IotCardRechargeHistoricalRecords setCrtUser(String crtUser) {
		this.crtUser = crtUser;
		return this;
	}
	/**
	 * 获取：操作人
	 */
	public String getCrtUser() {
		return crtUser;
	}
	/**
	 * 设置：修改时间
	 */
	public IotCardRechargeHistoricalRecords setUpdTime(Date updTime) {
		this.updTime = updTime;
		return this;
	}
	/**
	 * 获取：修改时间
	 */
	public Date getUpdTime() {
		return updTime;
	}
	/**
	 * 设置：修改人
	 */
	public IotCardRechargeHistoricalRecords setUpdUser(String updUser) {
		this.updUser = updUser;
		return this;
	}
	/**
	 * 获取：修改人
	 */
	public String getUpdUser() {
		return updUser;
	}

}
