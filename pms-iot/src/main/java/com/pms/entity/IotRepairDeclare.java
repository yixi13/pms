package com.pms.entity;
import com.baomidou.mybatisplus.activerecord.Model;
import java.io.Serializable;
import java.util.Date;
import javax.persistence.*;
import com.pms.util.Description;
import com.pms.entity.BaseModel;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;


/**
 * 
 * 
 * @author lk
 * @email 307169223@qq.com
 * @date 2018-08-20 09:57:14
 */
@TableName( "iot_repair_declare")
public class IotRepairDeclare extends BaseModel<IotRepairDeclare> {
private static final long serialVersionUID = 1L;


	/**
	 *设备保修申报表
	 */
    @TableId("repair_declare_id")
    private Long repairDeclareId;
	
	    /**
     *水泵组ID
     */
    @TableField("group_id")
    @Description("水泵组ID")
    private Long groupId;
	
	    /**
     *水泵组名称
     */
    @TableField("group_name")
    @Description("水泵组名称")
    private String groupName;
	
	    /**
     *所属水泵房
     */
    @TableField("belonged_house")
    @Description("所属水泵房")
    private String belongedHouse;
	
	    /**
     *所属小区
     */
    @TableField("belonged_community")
    @Description("所属小区")
    private String belongedCommunity;
	
	    /**
     *1.未知2.设备故障3.环境因素
     */
    @TableField("declare_type")
    @Description("1.未知2.设备故障3.环境因素")
    private Integer declareType;
	
	    /**
     *申报人
     */
    @TableField("declare_person")
    @Description("申报人")
    private String declarePerson;
	
	    /**
     *申报人电话
     */
    @TableField("declare_phone")
    @Description("申报人电话")
    private String declarePhone;
	
	    /**
     *申报时间
     */
    @TableField("declare_data")
    @Description("申报时间")
    private Date declareData;
	
	    /**
     *受理人
     */
    @TableField("accept_person")
    @Description("受理人")
    private String acceptPerson;
	
	    /**
     *受理人电话
     */
    @TableField("accept_phone")
    @Description("受理人电话")
    private String acceptPhone;
	
	    /**
     *受理时间
     */
    @TableField("accept_data")
    @Description("受理时间")
    private Date acceptData;
	
	    /**
     *状态1待处理2拒绝受理3已受理
     */
    @TableField("state_")
    @Description("状态1待处理2拒绝受理3已受理")
    private Integer state;
	
	    /**
     *
     */
    @TableField("crt_time")
    @Description("")
    private Date crtTime;
	
	    /**
     *
     */
    @TableField("crt_user")
    @Description("")
    private String crtUser;
	
	    /**
     *
     */
    @TableField("upd_time")
    @Description("")
    private Date updTime;
	
	    /**
     *
     */
    @TableField("upd_user")
    @Description("")
    private String updUser;
	
	    /**
     *故障描述
     */
    @TableField("description")
	@Description("故障描述")
	private String description;
	/**
	 *角色ID
	 */
	@TableField("role_id")
	@Description("角色ID")
	private Long roleId;

	/**
	 *是否已读
	 */
	@TableField("is_read")
	@Description("是否已读(1已读2未读)")
	private Integer isRead;

	/**
	 *水泵房ID
	 */
	@TableField("house_id")
	@Description("水泵房ID")
	private Long houseId;
	/**
	 *小区ID
	 */
	@TableField("community_id")
	@Description("小区ID")
	private Long communityId;
	/**
	 *机构ID
	 */
	@TableField("agency_id")
	@Description("机构ID")
	private Long agencyId;
	/**
	 *系统编码
	 */
	@TableField("company_code")
	@Description("系统编码")
	private String companyCode;
	/**
	 * 标记是否已经维修 1 是 2否
	 */
	@TableField("target_")
	@Description("标记是否已经维修 1 是 2否")
	private Integer target;
// ID赋值
public IotRepairDeclare(){
        this.repairDeclareId= returnIdLong();
        }
@Override
protected Serializable pkVal() {
        return this.repairDeclareId;
}
	/**
	 * 设置：设备保修申报表
	 */
	public IotRepairDeclare setRepairDeclareId(Long repairDeclareId) {
		this.repairDeclareId = repairDeclareId;
		return this;
	}
	/**
	 * 获取：设备保修申报表
	 */
	public Long getRepairDeclareId() {
		return repairDeclareId;
	}
	/**
	 * 设置：水泵组ID
	 */
	public IotRepairDeclare setGroupId(Long groupId) {
		this.groupId = groupId;
		return this;
	}
	/**
	 * 获取：水泵组ID
	 */
	public Long getGroupId() {
		return groupId;
	}
	/**
	 * 设置：水泵组名称
	 */
	public IotRepairDeclare setGroupName(String groupName) {
		this.groupName = groupName;
		return this;
	}
	/**
	 * 获取：水泵组名称
	 */
	public String getGroupName() {
		return groupName;
	}
	/**
	 * 设置：所属水泵房
	 */
	public IotRepairDeclare setBelongedHouse(String belongedHouse) {
		this.belongedHouse = belongedHouse;
		return this;
	}
	/**
	 * 获取：所属水泵房
	 */
	public String getBelongedHouse() {
		return belongedHouse;
	}
	/**
	 * 设置：所属小区
	 */
	public IotRepairDeclare setBelongedCommunity(String belongedCommunity) {
		this.belongedCommunity = belongedCommunity;
		return this;
	}
	/**
	 * 获取：所属小区
	 */
	public String getBelongedCommunity() {
		return belongedCommunity;
	}
	/**
	 * 设置：1.未知2.设备故障3.环境因素
	 */
	public IotRepairDeclare setDeclareType(Integer declareType) {
		this.declareType = declareType;
		return this;
	}
	/**
	 * 获取：1.未知2.设备故障3.环境因素
	 */
	public Integer getDeclareType() {
		return declareType;
	}
	/**
	 * 设置：申报人
	 */
	public IotRepairDeclare setDeclarePerson(String declarePerson) {
		this.declarePerson = declarePerson;
		return this;
	}
	/**
	 * 获取：申报人
	 */
	public String getDeclarePerson() {
		return declarePerson;
	}
	/**
	 * 设置：申报人电话
	 */
	public IotRepairDeclare setDeclarePhone(String declarePhone) {
		this.declarePhone = declarePhone;
		return this;
	}
	/**
	 * 获取：申报人电话
	 */
	public String getDeclarePhone() {
		return declarePhone;
	}
	/**
	 * 设置：申报时间
	 */
	public IotRepairDeclare setDeclareData(Date declareData) {
		this.declareData = declareData;
		return this;
	}
	/**
	 * 获取：申报时间
	 */
	public Date getDeclareData() {
		return declareData;
	}
	/**
	 * 设置：受理人
	 */
	public IotRepairDeclare setAcceptPerson(String acceptPerson) {
		this.acceptPerson = acceptPerson;
		return this;
	}
	/**
	 * 获取：受理人
	 */
	public String getAcceptPerson() {
		return acceptPerson;
	}
	/**
	 * 设置：受理人电话
	 */
	public IotRepairDeclare setAcceptPhone(String acceptPhone) {
		this.acceptPhone = acceptPhone;
		return this;
	}
	/**
	 * 获取：受理人电话
	 */
	public String getAcceptPhone() {
		return acceptPhone;
	}
	/**
	 * 设置：受理时间
	 */
	public IotRepairDeclare setAcceptData(Date acceptData) {
		this.acceptData = acceptData;
		return this;
	}
	/**
	 * 获取：受理时间
	 */
	public Date getAcceptData() {
		return acceptData;
	}
	/**
	 * 设置：状态1待处理2拒绝受理3已受理
	 */
	public IotRepairDeclare setState(Integer state) {
		this.state = state;
		return this;
	}
	/**
	 * 获取：状态1待处理2拒绝受理3已受理
	 */
	public Integer getState() {
		return state;
	}
	/**
	 * 设置：
	 */
	public IotRepairDeclare setCrtTime(Date crtTime) {
		this.crtTime = crtTime;
		return this;
	}
	/**
	 * 获取：
	 */
	public Date getCrtTime() {
		return crtTime;
	}
	/**
	 * 设置：
	 */
	public IotRepairDeclare setCrtUser(String crtUser) {
		this.crtUser = crtUser;
		return this;
	}
	/**
	 * 获取：
	 */
	public String getCrtUser() {
		return crtUser;
	}
	/**
	 * 设置：
	 */
	public IotRepairDeclare setUpdTime(Date updTime) {
		this.updTime = updTime;
		return this;
	}
	/**
	 * 获取：
	 */
	public Date getUpdTime() {
		return updTime;
	}
	/**
	 * 设置：
	 */
	public IotRepairDeclare setUpdUser(String updUser) {
		this.updUser = updUser;
		return this;
	}
	/**
	 * 获取：
	 */
	public String getUpdUser() {
		return updUser;
	}
	/**
	 * 设置：故障描述
	 */
	public IotRepairDeclare setDescription(String description) {
		this.description = description;
		return this;
	}
	/**
	 * 获取：故障描述
	 */
	public String getDescription() {
		return description;
	}

	public Long getRoleId() {
		return roleId;
	}

	public void setRoleId(Long roleId) {
		this.roleId = roleId;
	}

	public Integer getIsRead() {
		return isRead;
	}

	public void setIsRead(Integer isRead) {
		this.isRead = isRead;
	}

	public Long getHouseId() {
		return houseId;
	}

	public Long getCommunityId() {
		return communityId;
	}

	public Long getAgencyId() {
		return agencyId;
	}

	public String getCompanyCode() {
		return companyCode;
	}

	public void setHouseId(Long houseId) {
		this.houseId = houseId;
	}

	public void setCommunityId(Long communityId) {
		this.communityId = communityId;
	}

	public void setAgencyId(Long agencyId) {
		this.agencyId = agencyId;
	}

	public void setCompanyCode(String companyCode) {
		this.companyCode = companyCode;
	}

	public Integer getTarget() {
		return target;
	}

	public void setTarget(Integer target) {
		this.target = target;
	}
}
