package com.pms.entity;

import com.baidubce.services.tsdb.model.Aggregator;
import com.baidubce.services.tsdb.model.Fill;
import com.baidubce.services.tsdb.model.Filters;
import com.baidubce.services.tsdb.model.GroupBy;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * Created by Administrator on 2017/12/25.
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@ApiModel(value = "query", description = "查询对象")
public class QueryParam {
    @ApiModelProperty(value = "metric",notes="度量")
    private String metric;
    private String field;
    @ApiModelProperty(value = "fields",notes="要查询的域")
    private List<String> fields;
    @ApiModelProperty(value = "fields",notes="要查询的标签")
    private List<String> tags;
    @ApiModelProperty(value = "filters",notes="过滤器")
    private Filters filters;
    @ApiModelProperty(value = "groupBy",notes="(基于域)分组")
    private List<GroupBy> groupBy;
    @ApiModelProperty(value = "limit",notes="查询条数")
    private Integer limit;
    @ApiModelProperty(value = "aggregators",notes="(基于域)聚合函数")
    private List<Aggregator> aggregators;
    @ApiModelProperty(value = "order",notes="排序")
    private String order;
    @ApiModelProperty(value = "fill",notes="插值补全")
    private Fill fill;
    @ApiModelProperty(value = "marker",notes="分页参数")
    private String marker;


}
