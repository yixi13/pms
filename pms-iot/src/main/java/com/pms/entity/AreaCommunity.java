package com.pms.entity;

import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;
import com.pms.util.Description;

import java.io.Serializable;
import java.util.Date;
import java.util.List;


/**
 * 
 * 
 * @author zyl
 * @email 517770986@qq.com
 * @date 2018-03-23 16:39:08
 */
@TableName( "area_community")
public class AreaCommunity extends BaseModel<AreaCommunity> {
private static final long serialVersionUID = 1L;


	    /**
	 *小区地址关联表
	 */
    @TableId("area_community_id")
    private Long areaCommunityId;
	
	    /**
     *市级编码
     */
    @TableField("city_gb")
    @Description("市级编码")
    private Integer cityGb;
	
	    /**
     *小区ID
     */
    @TableField("community_id")
    @Description("小区ID")
    private Long communityId;
	
	    /**
     *市名称
     */
    @TableField("city_name")
    @Description("市名称")
    private String cityName;
	
	    /**
     *
     */
    @TableField("create_time")
    @Description("")
    private Date createTime;


	/**
	 *区县GB码
	 */
	@TableField("county_gb")
	@Description("区县GB码")
	private Integer countyGb;


	/**
	 *区县名称
	 */
	@TableField("county_name")
	@Description("区县名称")
	private String countyName;
	/**
	 * 临时字段
	 */
	@TableField(exist = false)
	private List<Community> community;


// ID赋值
public AreaCommunity(){
        this.areaCommunityId= returnIdLong();
        }
@Override
protected Serializable pkVal() {
        return this.areaCommunityId;
}
	/**
	 * 设置：小区地址关联表
	 */
	public AreaCommunity setAreaCommunityId(Long areaCommunityId) {
		this.areaCommunityId = areaCommunityId;
		return this;
	}
	/**
	 * 获取：小区地址关联表
	 */
	public Long getAreaCommunityId() {
		return areaCommunityId;
	}
	/**
	 * 设置：市级编码
	 */
	public AreaCommunity setCityGb(Integer cityGb) {
		this.cityGb = cityGb;
		return this;
	}
	/**
	 * 获取：市级编码
	 */
	public Integer getCityGb() {
		return cityGb;
	}
	/**
	 * 设置：小区ID
	 */
	public AreaCommunity setCommunityId(Long communityId) {
		this.communityId = communityId;
		return this;
	}
	/**
	 * 获取：小区ID
	 */
	public Long getCommunityId() {
		return communityId;
	}
	/**
	 * 设置：市名称
	 */
	public AreaCommunity setCityName(String cityName) {
		this.cityName = cityName;
		return this;
	}
	/**
	 * 获取：市名称
	 */
	public String getCityName() {
		return cityName;
	}
	/**
	 * 设置：
	 */
	public AreaCommunity setCreateTime(Date createTime) {
		this.createTime = createTime;
		return this;
	}
	/**
	 * 获取：
	 */
	public Date getCreateTime() {
		return createTime;
	}

	public void setCommunity(List<Community> community) {
		this.community = community;
	}

	public List<Community> getCommunity() {
		return community;
	}

	public Integer getCountyGb() {
		return countyGb;
	}

	public String getCountyName() {
		return countyName;
	}

	public void setCountyGb(Integer countyGb) {
		this.countyGb = countyGb;
	}

	public void setCountyName(String countyName) {
		this.countyName = countyName;
	}
}
