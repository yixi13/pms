package com.pms.entity;

import java.util.Date;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableName;
import java.io.Serializable;

/**
 * <p>
 * 
 * </p>
 *
 * @author asus
 * @since 2018-07-18
 */
@TableName("iot_water_pump_group")
public class IotWaterPumpGroup extends Model<IotWaterPumpGroup> {

    private static final long serialVersionUID = 1L;

    @TableId("group_id")
	private Long id;
    /**
     * 水泵组名称
     */
	@TableField("group_name")
	private String name;
    /**
     * 水泵组编号
     */
	@TableField("group_code")
	private String code;
    /**
     * 水泵组简介
     */
	@TableField("group_desc")
	private String desc;
    /**
     * 负责人名称
     */
	@TableField("link_name")
	private String linkName;
    /**
     * 负责人电话
     */
	@TableField("link_phone")
	private String linkPhone;
    /**
     * 水泵房id
     */
	@TableField("house_id")
	private Long parentId;

	/**
	 * 水泵房名称
	 */
	@TableField("house_name")
	private String parentName;
    /**
     * 小区id
     */
	@TableField("community_id")
	private Long communityId;
	/**
	 * 小区名称
	 */
	@TableField("community_name")
	private String communityName;

    /**
     * 设备数据来源(1-系统。2-沃佳）
     */
	@TableField("data_source")
	private Integer dataSource;
    /**
     * 数据传输方式(1-有限网络，2-移动流量）
     */
	@TableField("data_type")
	private Integer dataType;
    /**
     * 移动流量传输的 卡号
     */
	@TableField("group_phone")
	private String groupPhone;
	@TableField("create_by")
	private String createBy;
	@TableField("create_time")
	private Date createTime;
	@TableField("update_by")
	private String updateBy;
	@TableField("update_time")
	private Date updateTime;
	/**
	 * 客户公司编码
	 */
	@TableField("company_code")
	private String companyCode;

	@TableField(exist=false)
	private Integer level = 5;

	public String getCompanyCode() {
		return companyCode;
	}

	public void setCompanyCode(String customerCode) {
		this.companyCode = customerCode;
	}
	public Integer getLevel() {
		return level;
	}

	public void setLevel(Integer level) {
		this.level = level;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public Long getId() {
		return id;
	}

	public String getName() {
		return name;
	}

	public String getCode() {
		return code;
	}

	public String getDesc() {
		return desc;
	}

	public void setDesc(String groupDesc) {
		this.desc = groupDesc;
	}

	public String getLinkName() {
		return linkName;
	}

	public void setLinkName(String linkName) {
		this.linkName = linkName;
	}

	public String getLinkPhone() {
		return linkPhone;
	}

	public void setLinkPhone(String linkPhone) {
		this.linkPhone = linkPhone;
	}

	public Long getParentId() {
		return parentId;
	}

	public void setParentId(Long houseId) {
		this.parentId = houseId;
	}

	public String getParentName() {
		return parentName;
	}

	public void setParentName(String parentName) {
		this.parentName = parentName;
	}

	public Long getCommunityId() {
		return communityId;
	}

	public void setCommunityId(Long communityId) {
		this.communityId = communityId;
	}

	public String getCommunityName() {
		return communityName;
	}

	public void setCommunityName(String communityName) {
		this.communityName = communityName;
	}

	public Integer getDataSource() {
		return dataSource;
	}

	public void setDataSource(Integer dataSource) {
		this.dataSource = dataSource;
	}

	public Integer getDataType() {
		return dataType;
	}

	public void setDataType(Integer dataType) {
		this.dataType = dataType;
	}

	public String getGroupPhone() {
		return groupPhone;
	}

	public void setGroupPhone(String groupPhone) {
		this.groupPhone = groupPhone;
	}

	public String getCreateBy() {
		return createBy;
	}

	public void setCreateBy(String createBy) {
		this.createBy = createBy;
	}

	public Date getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	public String getUpdateBy() {
		return updateBy;
	}

	public void setUpdateBy(String updateBy) {
		this.updateBy = updateBy;
	}

	public Date getUpdateTime() {
		return updateTime;
	}

	public void setUpdateTime(Date updateTime) {
		this.updateTime = updateTime;
	}

	@Override
	protected Serializable pkVal() {
		return this.id;
	}

}
