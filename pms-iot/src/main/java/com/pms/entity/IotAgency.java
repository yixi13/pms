package com.pms.entity;

import java.util.Date;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableName;
import java.io.Serializable;
import java.util.List;

/**
 * <p>
 * 
 * </p>
 *
 * @author asus
 * @since 2018-07-18
 */
@TableName("iot_agency")
public class IotAgency extends Model<IotAgency> {

    private static final long serialVersionUID = 1L;

    @TableId("agency_id")
	private Long id;
    /**
     * 机构名称
     */
	@TableField("agency_name")
	private String name;
    /**
     * 机构编码
     */
	@TableField("agency_code")
	private String code;
    /**
     * 上级机构id
     */
	@TableField("parent_id")
	private Long parentId;
	/**
	 * 客户公司编码
	 */
	@TableField("company_code")
	private String companyCode;
	/**
	 * 上级机构名称
	 */
	@TableField("parent_name")
	private String parentName;
    /**
     * 机构等级(2-物业/公司，3-小区，4-水泵房)
     */
	@TableField("agency_level")
	private Integer level;
    /**
     * 负责人名称
     */
	@TableField("manage_name")
	private String linkName;
    /**
     * 负责人电话
     */
	@TableField("manage_phone")
	private String linkPhone;
    /**
     * 机构地址
     */
	@TableField("agency_address")
	private String agencyAddress;
    /**
     * 机构简介
     */
	@TableField("agency_desc")
	private String desc;
    /**
     * 经度
     */
	private Double longitude;
    /**
     * 纬度
     */
	private Double latitude;
    /**
     * 状态（1-正常，2-禁用，3-删除）
     */
	private Integer status;
	@TableField("create_time")
	private Date createTime;
	@TableField("create_by")
	private String createBy;
	@TableField("update_by")
	private String updateBy;
	@TableField("update_time")
	private Date updateTime;
    /**
     * 省级行政区划编号
     */
	@TableField("province_gb")
	private Integer provinceGb;
    /**
     * 市级行政区划编号
     */
	@TableField("city_gb")
	private Integer cityGb;
    /**
     * 县/区级行政区划编号
     */
	@TableField("area_gb")
	private Integer areaGb;

	@TableField(exist=false)
	private List<IotAgency> children;

	public List<IotAgency> getChildren() {
		return children;
	}

	public void setChildren(List<IotAgency> children) {
		this.children = children;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public Long getParentId() {
		return parentId;
	}

	public void setParentId(Long parentId) {
		this.parentId = parentId;
	}

	public String getParentName() {
		return parentName;
	}

	public void setParentName(String parentName) {
		this.parentName = parentName;
	}

	public String getCompanyCode() {
		return companyCode;
	}

	public void setCompanyCode(String customerCode) {
		this.companyCode = customerCode;
	}

	public Integer getLevel() {
		return level;
	}

	public void setLevel(Integer level) {
		this.level = level;
	}

	public String getLinkName() {
		return linkName;
	}

	public void setLinkName(String linkName) {
		this.linkName = linkName;
	}

	public String getLinkPhone() {
		return linkPhone;
	}

	public void setLinkPhone(String linkPhone) {
		this.linkPhone = linkPhone;
	}

	public String getAgencyAddress() {
		return agencyAddress;
	}

	public void setAgencyAddress(String agencyAddress) {
		this.agencyAddress = agencyAddress;
	}

	public String getDesc() {
		return desc;
	}

	public void setDesc(String agencyDesc) {
		this.desc = agencyDesc;
	}

	public Double getLongitude() {
		return longitude;
	}

	public void setLongitude(Double longitude) {
		this.longitude = longitude;
	}

	public Double getLatitude() {
		return latitude;
	}

	public void setLatitude(Double latitude) {
		this.latitude = latitude;
	}

	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	public Date getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	public String getCreateBy() {
		return createBy;
	}

	public void setCreateBy(String createBy) {
		this.createBy = createBy;
	}

	public String getUpdateBy() {
		return updateBy;
	}

	public void setUpdateBy(String updateBy) {
		this.updateBy = updateBy;
	}

	public Date getUpdateTime() {
		return updateTime;
	}

	public void setUpdateTime(Date updateTime) {
		this.updateTime = updateTime;
	}

	public Integer getProvinceGb() {
		return provinceGb;
	}

	public void setProvinceGb(Integer provinceGb) {
		this.provinceGb = provinceGb;
	}

	public Integer getCityGb() {
		return cityGb;
	}

	public void setCityGb(Integer cityGb) {
		this.cityGb = cityGb;
	}

	public Integer getAreaGb() {
		return areaGb;
	}

	public void setAreaGb(Integer areaGb) {
		this.areaGb = areaGb;
	}

	@Override
	protected Serializable pkVal() {
		return this.id;
	}

}
