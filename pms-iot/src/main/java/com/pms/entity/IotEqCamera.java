package com.pms.entity;
import java.io.Serializable;
import java.util.Date;
import com.pms.util.Description;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;
/**
 * 
 * 
 * @author lk
 * @email 307169223@qq.com
 * @date 2018-08-21 13:52:06
 */
@TableName( "iot_eq_camera")
public class IotEqCamera extends BaseModel<IotEqCamera> {
private static final long serialVersionUID = 1L;


	    /**
	 *
	 */
    @TableId("eq_id")
    private Long eqId;
	
	    /**
     *摄像头位置
     */
    @TableField("place_")
    @Description("摄像头位置")
    private String place;
	
	    /**
     *设备序列号
     */
    @TableField("key_")
    @Description("设备序列号")
    private String key;

	/**
	 *通道
	 */
	@TableField("front_address")
	@Description("通道")
	private String frontAddress;

	    /**
     *备注
     */
    @TableField("remack_")
    @Description("备注")
    private String remack;
	
	    /**
     *水泵房ID
     */
    @TableField("agency_id")
    @Description("水泵房ID")
    private Long agencyId;
	
	    /**
     *验证码
     */
    @TableField("verification_code")
    @Description("验证码")
    private String verificationCode;
	
	    /**
     *token码
     */
    @TableField("token_")
    @Description("token码")
    private String token;
	
	    /**
     *token结束时间
     */
    @TableField("expire_time")
    @Description("token结束时间")
    private Date expireTime;
	
	    /**
     *当前时间
     */
    @TableField("currentdate_time")
    @Description("当前时间")
    private Date currentdateTime;
	
	    /**
     *用户ID
     */
    @TableField("user_id")
    @Description("用户ID")
    private Long userId;
	
	    /**
     *创建时间
     */
    @TableField("create_time")
    @Description("创建时间")
    private Date createTime;
	
	    /**
     *创建人
     */
    @TableField("create_by")
    @Description("创建人")
    private String createBy;
	
	    /**
     *摄像头用户配置信息ID
     */
    @TableField("camera_user_id")
    @Description("摄像头用户配置信息ID")
    private Long cameraUserId;
	
	    /**
     *抓拍图片
     */
    @TableField("picUrl")
    @Description("抓拍图片")
    private String picurl;
	
	    /**
     *所属水泵房
     */
    @TableField("belonged_house")
    @Description("所属水泵房")
    private String belongedHouse;
	
	    /**
     *所属小区
     */
    @TableField("belonged_community")
    @Description("所属小区")
    private String belongedCommunity;
	
	    /**
     *所属物业
     */
    @TableField("belonged_agency")
    @Description("所属物业")
    private String belongedAgency;

	/**
	 *小区ID
	 */
	@TableField("community_id")
	@Description("小区ID")
	private Long communityId;

	/**
	 *机构ID
	 */
	@TableField("sp_id")
	@Description("机构ID")
	private Long spId;

	/**
	 *系统编码
	 */
	@TableField("company_code")
	@Description("系统编码")
	private String companyCode;

	@TableField(exist = false)
	private String appKey;

	@TableField(exist = false)
	private String appSecret;
	/**
	 * 在线状态：0-不在线，1-在线
	 */
	@TableField(exist = false)
	private int status;
	
// ID赋值
public IotEqCamera(){
        this.eqId= returnIdLong();
        }
@Override
protected Serializable pkVal() {
        return this.eqId;
}

	public String getFrontAddress() {
		return frontAddress;
	}

	public void setFrontAddress(String frontAddress) {
		this.frontAddress = frontAddress;
	}

	/**
	 * 设置：
	 */
	public IotEqCamera setEqId(Long eqId) {
		this.eqId = eqId;
		return this;
	}
	/**
	 * 获取：
	 */
	public Long getEqId() {
		return eqId;
	}
	/**
	 * 设置：摄像头位置
	 */
	public IotEqCamera setPlace(String place) {
		this.place = place;
		return this;
	}
	/**
	 * 获取：摄像头位置
	 */
	public String getPlace() {
		return place;
	}
	/**
	 * 设置：设备序列号
	 */
	public IotEqCamera setKey(String key) {
		this.key = key;
		return this;
	}
	/**
	 * 获取：设备序列号
	 */
	public String getKey() {
		return key;
	}
	/**
	 * 设置：备注
	 */
	public IotEqCamera setRemack(String remack) {
		this.remack = remack;
		return this;
	}
	/**
	 * 获取：备注
	 */
	public String getRemack() {
		return remack;
	}
	/**
	 * 设置：水泵房ID
	 */
	public IotEqCamera setAgencyId(Long agencyId) {
		this.agencyId = agencyId;
		return this;
	}
	/**
	 * 获取：水泵房ID
	 */
	public Long getAgencyId() {
		return agencyId;
	}
	/**
	 * 设置：验证码
	 */
	public IotEqCamera setVerificationCode(String verificationCode) {
		this.verificationCode = verificationCode;
		return this;
	}
	/**
	 * 获取：验证码
	 */
	public String getVerificationCode() {
		return verificationCode;
	}
	/**
	 * 设置：token码
	 */
	public IotEqCamera setToken(String token) {
		this.token = token;
		return this;
	}
	/**
	 * 获取：token码
	 */
	public String getToken() {
		return token;
	}
	/**
	 * 设置：token结束时间
	 */
	public IotEqCamera setExpireTime(Date expireTime) {
		this.expireTime = expireTime;
		return this;
	}
	/**
	 * 获取：token结束时间
	 */
	public Date getExpireTime() {
		return expireTime;
	}
	/**
	 * 设置：当前时间
	 */
	public IotEqCamera setCurrentdateTime(Date currentdateTime) {
		this.currentdateTime = currentdateTime;
		return this;
	}
	/**
	 * 获取：当前时间
	 */
	public Date getCurrentdateTime() {
		return currentdateTime;
	}
	/**
	 * 设置：用户ID
	 */
	public IotEqCamera setUserId(Long userId) {
		this.userId = userId;
		return this;
	}
	/**
	 * 获取：用户ID
	 */
	public Long getUserId() {
		return userId;
	}
	/**
	 * 设置：创建时间
	 */
	public IotEqCamera setCreateTime(Date createTime) {
		this.createTime = createTime;
		return this;
	}
	/**
	 * 获取：创建时间
	 */
	public Date getCreateTime() {
		return createTime;
	}
	/**
	 * 设置：创建人
	 */
	public IotEqCamera setCreateBy(String createBy) {
		this.createBy = createBy;
		return this;
	}
	/**
	 * 获取：创建人
	 */
	public String getCreateBy() {
		return createBy;
	}
	/**
	 * 设置：摄像头用户配置信息ID
	 */
	public IotEqCamera setCameraUserId(Long cameraUserId) {
		this.cameraUserId = cameraUserId;
		return this;
	}
	/**
	 * 获取：摄像头用户配置信息ID
	 */
	public Long getCameraUserId() {
		return cameraUserId;
	}
	/**
	 * 设置：抓拍图片
	 */
	public IotEqCamera setPicurl(String picurl) {
		this.picurl = picurl;
		return this;
	}
	/**
	 * 获取：抓拍图片
	 */
	public String getPicurl() {
		return picurl;
	}
	/**
	 * 设置：所属水泵房
	 */
	public IotEqCamera setBelongedHouse(String belongedHouse) {
		this.belongedHouse = belongedHouse;
		return this;
	}
	/**
	 * 获取：所属水泵房
	 */
	public String getBelongedHouse() {
		return belongedHouse;
	}
	/**
	 * 设置：所属小区
	 */
	public IotEqCamera setBelongedCommunity(String belongedCommunity) {
		this.belongedCommunity = belongedCommunity;
		return this;
	}
	/**
	 * 获取：所属小区
	 */
	public String getBelongedCommunity() {
		return belongedCommunity;
	}
	/**
	 * 设置：所属物业
	 */
	public IotEqCamera setBelongedAgency(String belongedAgency) {
		this.belongedAgency = belongedAgency;
		return this;
	}
	/**
	 * 获取：所属物业
	 */
	public String getBelongedAgency() {
		return belongedAgency;
	}

	public Long getCommunityId() {
		return communityId;
	}

	public Long getSpId() {
		return spId;
	}

	public String getCompanyCode() {
		return companyCode;
	}

	public void setCommunityId(Long communityId) {
		this.communityId = communityId;
	}

	public void setSpId(Long spId) {
		this.spId = spId;
	}

	public void setCompanyCode(String companyCode) {
		this.companyCode = companyCode;
	}

	public String getAppKey() {
		return appKey;
	}

	public String getAppSecret() {
		return appSecret;
	}

	public void setAppKey(String appKey) {
		this.appKey = appKey;
	}

	public void setAppSecret(String appSecret) {
		this.appSecret = appSecret;
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}
}
