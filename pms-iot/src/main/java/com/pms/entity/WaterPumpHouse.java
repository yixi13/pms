package com.pms.entity;

import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;

import java.io.Serializable;
import java.util.Date;

/**
 * <p>
 * 
 * </p>
 *
 * @author lk
 * @since 2017-12-27
 */
@TableName("water_pump_house")
public class WaterPumpHouse extends BaseModel<WaterPumpHouse> {
    private static final long serialVersionUID = 1L;

    /**
     * 水泵房id
     */
    @TableId("water_pump_house_id")
	private Long waterPumpHouseId;
    /**
     * 小区id
     */
	@TableField("community_id")
	private Long communityId;
    /**
     * 水泵房名称
     */
	@TableField("house_name")
	private String houseName;
    /**
     * 水泵房备注
     */
	@TableField("house_remark")
	private String houseRemark;
	/**
	 * 更新时间
	 */
	@TableField("update_time")
	private Date updateTime;
	/**
	 * 更新者
	 */
	@TableField("update_by")
	private String updateBy;
	/**
	 * 创建时间
	 */
	@TableField("create_time")
	private Date createTime;
	/**
	 * 创建者
	 */
	@TableField("create_by")
	private String createBy;

	/**
	 * 水泵房排序
	 */
	@TableField("house_sort")
	private Integer houseSort;
	/**
	 * 对应lbs数据id
	 */
	@TableField("lbs_poi_id")
	private String lbsPoiId;
	/**
	 * 经度
	 */
	@TableField("longitude")
	private Double longitude;
	/**
	 * 纬度
	 */
	@TableField("latitude")
	private Double latitude;
	/**
	 *临时字段水泵组
	 */
	@TableField(exist = false)
	private WaterPumpGroup waterPumpGroup;


	public String getLbsPoiId() {
		return lbsPoiId;
	}

	public void setLbsPoiId(String lbsPoiId) {
		this.lbsPoiId = lbsPoiId;
	}

	public Double getLongitude() {
		return longitude;
	}

	public void setLongitude(Double longitude) {
		this.longitude = longitude;
	}

	public Double getLatitude() {
		return latitude;
	}

	public void setLatitude(Double latitude) {
		this.latitude = latitude;
	}

	public WaterPumpGroup getWaterPumpGroup() {
		return waterPumpGroup;
	}

	public void setWaterPumpGroup(WaterPumpGroup waterPumpGroup) {
		this.waterPumpGroup = waterPumpGroup;
	}

	public Long getWaterPumpHouseId() {
		return waterPumpHouseId;
	}

	public void setWaterPumpHouseId(Long waterPumpHouseId) {
		this.waterPumpHouseId = waterPumpHouseId;
	}

	public Long getCommunityId() {
		return communityId;
	}

	public void setCommunityId(Long communityId) {
		this.communityId = communityId;
	}

	public String getHouseName() {
		return houseName;
	}

	public void setHouseName(String houseName) {
		this.houseName = houseName;
	}

	public String getHouseRemark() {
		return houseRemark;
	}

	public void setHouseRemark(String houseRemark) {
		this.houseRemark = houseRemark;
	}

	public Date getUpdateTime() {
		return updateTime;
	}

	public void setUpdateTime(Date updateTime) {
		this.updateTime = updateTime;
	}

	public String getUpdateBy() {
		return updateBy;
	}

	public void setUpdateBy(String updateBy) {
		this.updateBy = updateBy;
	}

	public Date getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	public String getCreateBy() {
		return createBy;
	}

	public void setCreateBy(String createBy) {
		this.createBy = createBy;
	}

	public Integer getHouseSort() {
		return houseSort;
	}

	public void setHouseSort(Integer houseSort) {
		this.houseSort = houseSort;
	}

	@Override
	protected Serializable pkVal() {
		return this.waterPumpHouseId;
	}
	// ID赋值
	public WaterPumpHouse(){
		this.waterPumpHouseId= returnIdLong();
	}
}
