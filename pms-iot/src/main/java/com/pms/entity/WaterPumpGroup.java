package com.pms.entity;

import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

/**
 * <p>
 * 水泵表
 * </p>
 *
 * @author lk
 * @since 2017-12-25
 */
@TableName("water_pump_group")
public class WaterPumpGroup extends BaseModel<WaterPumpGroup> {

    private static final long serialVersionUID = 1L;

    @TableId("group_id")
	private Long groupId;
    /**
     * 水泵组名称
     */
	@TableField("group_name")
	private String groupName;
    /**
     * 水泵组介绍
     */
	@TableField("group_introduce")
	private String groupIntroduce;
    /**
     * 区域id
     */
	@TableField("area_gb")
	private Integer areaGb;
    /**
     * 区域名称
     */
	@TableField("area_name")
	private String areaName;
	/**
	 * 水泵房ID
	 */
	@TableField("house_id")
	private Long houseId;
	/**
	 * 水泵房名称
	 */
	@TableField("house_name")
	private String houseName;

    /**
     * 所属小区id
     */
	@TableField("community_id")
	private Long communityId;
    /**
     * 所属小区名称
     */
	@TableField("community_name")
	private String communityName;
    /**
     * 所属公司id
     */
	@TableField("company_id")
	private Long companyId;
    /**
     * 所属公司名称
     */
	@TableField("company_name")
	private String companyName;
	/**
	 * 更新时间
	 */
	@TableField("update_time")
	private Date updateTime;
	/**
	 * 更新者
	 */
	@TableField("update_by")
	private String updateBy;
	/**
	 * 创建时间
	 */
	@TableField("create_time")
	private Date createTime;
	/**
	 * 创建者
	 */
	@TableField("create_by")
	private String createBy;

	/**
	 * 泵组别名
	 */
	@TableField("group_alias")
	private String groupAlias;

	public String getGroupAlias() {
		return groupAlias;
	}

	public void setGroupAlias(String groupAlias) {
		this.groupAlias = groupAlias;
	}

	/**
	 * 临时字段  水泵数据详情表
	 */
	@TableField(exist = false)


	private List<WaterPumpGroupAttr> waterPumpGroupAttr;

	public List<WaterPumpGroupAttr> getWaterPumpGroupAttr() {
		return waterPumpGroupAttr;
	}

	public void setWaterPumpGroupAttr(List<WaterPumpGroupAttr> waterPumpGroupAttr) {
		this.waterPumpGroupAttr = waterPumpGroupAttr;
	}

	public Long getHouseId() {
		return houseId;
	}

	public String getHouseName() {
		return houseName;
	}

	public void setHouseId(Long houseId) {
		this.houseId = houseId;
	}

	public void setHouseName(String houseName) {
		this.houseName = houseName;
	}

	public Long getGroupId() {
		return groupId;
	}

	public void setGroupId(Long groupId) {
		this.groupId = groupId;
	}

	public String getGroupName() {
		return groupName;
	}

	public void setGroupName(String groupName) {
		this.groupName = groupName;
	}

	public String getGroupIntroduce() {
		return groupIntroduce;
	}

	public void setGroupIntroduce(String groupIntroduce) {
		this.groupIntroduce = groupIntroduce;
	}

	public Integer getAreaGb() {
		return areaGb;
	}

	public void setAreaGb(Integer areaGb) {
		this.areaGb = areaGb;
	}

	public String getAreaName() {
		return areaName;
	}

	public void setAreaName(String areaName) {
		this.areaName = areaName;
	}

	public Long getCommunityId() {
		return communityId;
	}

	public void setCommunityId(Long communityId) {
		this.communityId = communityId;
	}

	public String getCommunityName() {
		return communityName;
	}

	public void setCommunityName(String communityName) {
		this.communityName = communityName;
	}

	public Long getCompanyId() {
		return companyId;
	}

	public void setCompanyId(Long companyId) {
		this.companyId = companyId;
	}

	public String getCompanyName() {
		return companyName;
	}

	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}

	public Date getUpdateTime() {
		return updateTime;
	}

	public void setUpdateTime(Date updateTime) {
		this.updateTime = updateTime;
	}

	public String getUpdateBy() {
		return updateBy;
	}

	public void setUpdateBy(String updateBy) {
		this.updateBy = updateBy;
	}

	public Date getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	public String getCreateBy() {
		return createBy;
	}

	public void setCreateBy(String createBy) {
		this.createBy = createBy;
	}

	@Override
	protected Serializable pkVal() {
		return this.groupId;
	}
	// ID赋值
	public WaterPumpGroup(){
		this.groupId= returnIdLong();
	}
}
