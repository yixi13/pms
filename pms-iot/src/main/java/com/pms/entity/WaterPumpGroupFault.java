package com.pms.entity;

import java.util.Date;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableName;
import java.io.Serializable;

/**
 * <p>
 * 
 * </p>
 *
 * @author asus
 * @since 2018-03-19
 */
@TableName("water_pump_group_fault")
public class WaterPumpGroupFault extends Model<WaterPumpGroupFault> {

    private static final long serialVersionUID = 1L;

	private Long waterPumpGroupId;
    /**
     * 最新故障嘛
     */
	@TableField("ZXGZ")
	private Integer zxgz;
    /**
     * 设备名称
     */
	private String deviceName;
    /**
     * 上报时间
     */
	private Date reportTimeFormat;
    /**
     * websocket是否成功推送(0-否，1-是)
     */
	private Integer isSocketPush;
    /**
     * 是否短信推送(0-否，1-是)
     */
	private Integer isSmsPush;
    /**
     * 创建时间
     */
	private Date createTime;


	public Long getWaterPumpGroupId() {
		return waterPumpGroupId;
	}

	public void setWaterPumpGroupId(Long waterPumpGroupId) {
		this.waterPumpGroupId = waterPumpGroupId;
	}

	public Integer getZxgz() {
		return zxgz;
	}

	public void setZxgz(Integer zxgz) {
		this.zxgz = zxgz;
	}

	public String getDeviceName() {
		return deviceName;
	}

	public void setDeviceName(String deviceName) {
		this.deviceName = deviceName;
	}

	public Date getReportTimeFormat() {
		return reportTimeFormat;
	}

	public void setReportTimeFormat(Date reportTimeFormat) {
		this.reportTimeFormat = reportTimeFormat;
	}

	public Integer getIsSocketPush() {
		return isSocketPush;
	}

	public void setIsSocketPush(Integer isSocketPush) {
		this.isSocketPush = isSocketPush;
	}

	public Integer getIsSmsPush() {
		return isSmsPush;
	}

	public void setIsSmsPush(Integer isSmsPush) {
		this.isSmsPush = isSmsPush;
	}

	public Date getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	@Override
	protected Serializable pkVal() {
		return this.waterPumpGroupId;
	}

}
