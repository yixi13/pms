package com.pms.lbs.service.impl;

import com.alibaba.fastjson.JSONObject;
import com.pms.lbs.service.ILbsService;
import com.pms.lbs.utils.LbsConfig;
import com.pms.util.httpUtil.HttpClientUtil;
import com.pms.validator.Assert;
import com.xiaoleilu.hutool.util.StrUtil;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

/**
 * Created by 宏铭科技
 *
 * 百度LBS 云存储V3接口类
 *
 * @auther ljb
 * @Date 2018/5/7.
 */
@Service
public class LbsServiceImpl implements ILbsService {

    Logger logger = LoggerFactory.getLogger(this.getClass());
    /**
     * LBS 上面创建geotable
     *
     * @param name              geotable的中文名称
     * @return
     */
    @Override
    public JSONObject createGeotable(String name) {
        Assert.isNull(name,"name不能为空");
        JSONObject json = LbsConfig.createGeotable(name,1,1);
        logger.info(json.toJSONString());
        return json;
    }

    /**
     * 查询LBS 表list
     * @param name
     * @return
     */
    @Override
    public JSONObject getGeotableList(String name) {
        Assert.isNull(name,"name不能为空");
        JSONObject json = LbsConfig.getGeotableList(name);
        logger.info(json.toJSONString());
        return json;
    }

    /**
     *  更具LBS返回的ID 查询表详情
     * @param Id
     * @return
     */
    @Override
    public JSONObject getGeotableById(String Id) {
        Assert.isNull(Id,"id不能为空");
        JSONObject json = LbsConfig.getGeotableById(Id);
        logger.info(json.toJSONString());
        return null;
    }

    /**
     * LBS 创建表数据
     * @param longitude     经度
     * @param latitude      维度
     * @param address       地址
     * @param title         标签
     * @param level         定位级别
     * @return
     */
    @Override
    public JSONObject createPoi(Double longitude, Double latitude, String address, String title, Integer level) {
        Assert.isNull(longitude,"longitude不能为空");
        Assert.isNull(latitude,"latitude不能为空");
        JSONObject json = LbsConfig.createPoi(longitude, latitude, address, title, "level",level.toString());
        logger.info(json.toJSONString());
        return json;
    }

}
