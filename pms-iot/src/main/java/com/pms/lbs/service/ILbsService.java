package com.pms.lbs.service;

import com.alibaba.fastjson.JSONObject;

/**
 * Created by 宏铭科技
 *
 * 百度LBS 云存储V3接口类
 *
 * @auther ljb
 * @Date 2018/5/7.
 */
public interface ILbsService{


    /**
     * LBS 上面创建geotable
     *
     * @param name              geotable的中文名称
     * @return
     */
    public JSONObject createGeotable(String name);

    /**
     * 查询LBS 表list
     * @param name
     * @return
     */
    public JSONObject getGeotableList(String name);

    /**
     *  更具LBS返回的ID 查询表详情
     * @param Id
     * @return
     */
    public JSONObject getGeotableById(String Id);

    /**
     * LBS 创建表数据
     * @param longitude     经度
     * @param latitude      维度
     * @param address       地址
     * @param title         标签
     * @param level         定位级别
     * @return
     */
    public JSONObject createPoi(Double longitude,Double latitude,String address,String title ,Integer level);



}
