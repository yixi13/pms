package com.pms.lbs.utils;

import com.alibaba.fastjson.JSONObject;
import com.pms.util.httpUtil.HttpClientUtil;
import com.xiaoleilu.hutool.util.StrUtil;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

/**
 * Created by 宏铭科技
 *
 * @auther ljb
 * @Date 2018/5/7.
 */
@Component
public class LbsConfig {



    Logger logger = LoggerFactory.getLogger(this.getClass());

    // 百度LBS提供的AK值
    private static String  AK = "h4I1NguAiFaEjAKlSKHXxvRODEH3mYs2";
    // 百度LBS GeotableId
    private static String geotableId = "188738";

    // 百度LBS创建表  POST请求
    private static final String createGeotableUrl="http://api.map.baidu.com/geodata/v3/geotable/create";
    // 百度LBS 查询创建过的表
    private static final String listGeotableUrl = "http://api.map.baidu.com/geodata/v3/geotable/list";
    // 百度LBS 更具id查询表详情
    private static final String detailGeotableUrl = "http://api.map.baidu.com/geodata/v3/geotable/detail";
    //百度LBS创建数据请求
    private static final String poiCreateUrl="http://api.map.baidu.com/geodata/v3/poi/create";
    // 百度LBS修改数据请求
    private static final String poiUpdateUrl="http://api.map.baidu.com/geodata/v3/poi/update";
    // 百度LBS查询数据请求
    private static final String poiSelectUrl="http://api.map.baidu.com/geodata/v3/poi/list";
    // 百度LBS周边搜索数据请求
    private static final String poiGeoSearchUrl="http://api.map.baidu.com/geosearch/v3/nearby";
    // 百度LBS创建列  POST请求
    private static final String createColumnUrl="http://api.map.baidu.com/geodata/v3/column/create";

    public  LbsConfig(String AK,String geotableId){
        this.AK = AK;
        this.geotableId = geotableId;
    }
    public  LbsConfig(String AK){
        this.AK = AK;
    }
    public  LbsConfig(){
    }

    /**
     * LBS 上面创建geotable
     *
     * @param name              geotable的中文名称
     * @param geotype           数据的类型 1：点；3：面
     * @param is_published      是否发布到检索 0：未自动发布到云检索，1：自动发布到云检索；
     * @return
     */
    public static JSONObject createGeotable(String name, Integer geotype, Integer is_published) {
        StringBuilder param = new StringBuilder("ak=").append(AK).
                append("&name=").append(name).append("&geotype=").append(geotype).
                append("&is_published=").append(is_published);
        JSONObject ResponseJSON =  HttpClientUtil.doPost(
                createGeotableUrl,param.toString());
        return ResponseJSON;
    }
    /**
     * 查询LBS 表list
     * @param name
     * @return
     */
    public static JSONObject getGeotableList(String name) {
        StringBuilder param = new StringBuilder("ak=").append(LbsConfig.getAK());
        if (!StrUtil.isEmpty(name)){
            param.append("&name=").append(name);
        }

        JSONObject ResponseJSON = JSONObject.parseObject(HttpClientUtil.doGet(
                listGeotableUrl, param.toString()));

        return ResponseJSON;
    }

    /**
     *  更具LBS返回的ID 查询表详情
     * @param Id
     * @return
     */

    public static JSONObject getGeotableById(String Id) {
        StringBuilder param = new StringBuilder("ak=").
                append(LbsConfig.getAK()).append("&id=").append(Id);

        JSONObject ResponseJSON = JSONObject.parseObject(
                HttpClientUtil.doGet( detailGeotableUrl, param.toString() ));
        return ResponseJSON;
    }

    /**
     * LBS 创建表数据
     * @param longitude     经度
     * @param latitude      维度
     * @param address       地址
     * @param title         标签
     * @param columnName    新添加列名
     * @param column        插入列的值
     * @return
     */
    public static JSONObject createPoi(Double longitude, Double latitude, String address, String title,String columnName ,String column) {
        JSONObject ResponseJSON = null;
        StringBuilder param = new StringBuilder("ak=").append(LbsConfig.getAK()).append("&geotable_id=").append(geotableId).append("&coord_type=3")
                .append("&latitude=").append(latitude).append("&longitude=").append(longitude);

        if(StringUtils.isNotBlank(address)){
            param.append("&address=").append(address);
        }
        if(StringUtils.isNotBlank(title)){
            param.append("&title=").append(title);
        }
        if(StringUtils.isNotBlank(columnName) && StringUtils.isNotBlank(column)){
            param.append("&"+columnName+"=").append(column);
        }
        ResponseJSON =  HttpClientUtil.doPost(poiCreateUrl,param.toString());
        return ResponseJSON;
    }


    public static String getAK() {
        return AK;
    }

    public static void setAK(String AK) {
        LbsConfig.AK = AK;
    }

    public static String getGeotableId() {
        return geotableId;
    }

    public static void setGeotableId(String geotableId) {
        LbsConfig.geotableId = geotableId;
    }

}
