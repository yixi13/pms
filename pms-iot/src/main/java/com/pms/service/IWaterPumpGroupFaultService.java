package com.pms.service;

import com.pms.entity.WaterPumpGroupFault;
import com.baomidou.mybatisplus.service.IService;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author asus
 * @since 2018-03-19
 */
public interface IWaterPumpGroupFaultService extends IService<WaterPumpGroupFault> {
    List<WaterPumpGroupFault> selectByCommunityIdAndDate(long communityId, String dateStr);
}
