package com.pms.service;

import com.pms.entity.Company;
import com.baomidou.mybatisplus.service.IService;

/**
 * <p>
 * 水泵所属公司表 服务类
 * </p>
 *
 * @author lk
 * @since 2017-12-25
 */
public interface ICompanyService extends IService<Company> {
	boolean deleteCompanyAndWaterPumps(Long companyId);
}
