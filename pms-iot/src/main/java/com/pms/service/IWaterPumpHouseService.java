package com.pms.service;

import com.baomidou.mybatisplus.service.IService;
import com.pms.entity.WaterPumpHouse;

import java.util.List;
import java.util.Map;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author lk
 * @since 2017-12-27
 */
public interface IWaterPumpHouseService extends IService<WaterPumpHouse> {
	boolean deleteWaterPumpHouseAndWaterPump(Long houseId);
	public Integer selectMaxWaterPumpHouseSort(Long communityId);

	/**
	 * 查询水泵房 基础信息 asus 20180329
	 * @param wpHouseId  水泵房户室id
	 * @param wpHouseLbsId  水泵房户室lbsid
	 * @param communityId  社区id
	 * @return
	 */
	List<Map<String,Object>> readWaterPumpHouseoutline(Long wpHouseId, String wpHouseLbsId, Long communityId);

	/**
	 * 查询水泵房 基础信息 asus 20180329
	 * @param communityId  社区id
	 * @return
	 */
	List<Map<String,Object>> readWaterPumpHouseoutline(Long communityId);

	/**
	 * 查询水泵房 基础信息 asus 20180329
	 * @param wpHouseId  水泵房户室id
	 * @param wpHouseLbsId  水泵房户室lbsid
	 * @return
	 */
	Map<String,Object> readWaterPumpHouseoutline(Long wpHouseId,String wpHouseLbsId);

	public  Integer testSetVAR();


	/**
	 * 查询水泵房 基础信息 asus 20180401
	 * @param wpHouseId      水泵房户室id
	 * @param wpHouseLbsId  水泵房户室lbsid
	 * @param communityId   社区id
	 * @return
	 */
	public List<Map<String,Object>> readWaterPumpHouseOutlineHouseList(Long wpHouseId, String wpHouseLbsId, Long communityId);
}
