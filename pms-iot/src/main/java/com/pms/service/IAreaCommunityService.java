package com.pms.service;
import com.baomidou.mybatisplus.mapper.Wrapper;
import com.pms.entity.AreaCommunity;

import java.util.List;
import java.util.Map;

/**
 * 
 *
 * @author zyl
 * @email 517770986@qq.com
 * @date 2018-03-23 16:39:08
 */

public interface IAreaCommunityService extends IBaseService<AreaCommunity>{
    List<Map<String,Object>> selectByAreaCommunity();
    List<Map<String, Object>> selectAreaGbAndCommunity(Wrapper<AreaCommunity> wrapper);
}