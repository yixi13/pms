package com.pms.service;
import com.baomidou.mybatisplus.service.IService;
import com.pms.entity.EquipmentRepairRecord;
/**
 * 
 *
 * @author lk
 * @email
 * @date 2018-04-25 14:26:06
 */

public interface IEquipmentRepairRecordService extends  IBaseService<EquipmentRepairRecord> {

}