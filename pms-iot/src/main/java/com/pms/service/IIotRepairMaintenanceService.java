package com.pms.service;
import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.service.IService;
import com.pms.entity.IotRepairMaintenance;

import java.util.Map;

/**
 * 
 *
 * @author lk
 * @email 307169223@qq.com
 * @date 2018-08-20 09:57:14
 */

public interface IIotRepairMaintenanceService extends  IBaseService<IotRepairMaintenance> {

    IotRepairMaintenance seteleByMaintenance(Long repairMaintenanceId);
    Page<IotRepairMaintenance> selectByPageList(Page<IotRepairMaintenance> page, Map<String,Object> map);

    Page<IotRepairMaintenance> selectByPageMap(Page<IotRepairMaintenance> page, Map<String,Object> map);
}