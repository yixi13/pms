package com.pms.service;

import com.baomidou.mybatisplus.mapper.SqlHelper;
import com.baomidou.mybatisplus.mapper.Wrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.pms.entity.IotAgency;
import com.baomidou.mybatisplus.service.IService;

import java.util.List;
import java.util.Map;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author asus
 * @since 2018-07-18
 */
public interface IIotAgencyService extends IService<IotAgency> {

    /**
     * 根据一级分区id查询 下级  二/三级分区id
     * @param agencyId
     * @return
     */
    List<Long> getSubordinateAgencyIds(Long agencyId);

    /**
     * 修改机构，判断 isEditName 修改下级机构的 parentName
     * @param entity
     * @param isEditName
     * @return
     */
    boolean updateById(IotAgency entity,boolean isEditName);
    /**
     * 查询 二供机构 管理模块 分页栏数据
     * @param page   分页对象
     * @param wrapper
     * @param map  查询过滤条件 (orgIds- 机构id  ,groupIds -水泵组id)
     * @return
     */
    Page<Map<String,Object>> selectPage(Page<Map<String,Object>> page,Wrapper<Map<String,Object>> wrapper, Map<String,String> map);

    /**
     * 查询数据用于 机构数展示
     * @param wrapper
     * @return
     */
    List<Map<String,Object>> selectMapList(Wrapper<IotAgency> wrapper);

    /**
     * 删除 机构及其下级机构/水泵组
     * @param orgIds 机构id
     */
    void delAgencyAndWaterPumpGroup(String orgIds);

    /**
     * 查询电子地图 小区,水泵房  信息展示
     * @param param {level-(1小区,1水泵房) communityId-小区id  houseId-水泵房id}
     */
    List<Map<String,Object>> selectAgencyMapInfoByLevel(Map<String,Object> param);
    /**
     * 将List转换为树结构
     * @param tree
     * @param checked 是否默认选中(0-未选中(默认),1-选中)
     * @param opened 是否默认展开(0-未展开(默认),1-展开)
     * @return
     */
    List<Map<String,Object>> agencyFactorTree(List<Map<String,Object>> tree,Integer checked,Integer opened);

    /**
     * 获取角色关联机构的 机构树
     * @return
     */
    Map<String,Object> readRoleAgencyTree(Wrapper<IotAgency> agencyWp);

    /**
     * 关联删除
     * @param id
     * @param level
     * @return
     */
    void joinDelete(Long id,Integer level);

    /**
     * 查询水泵房
     * @param param
     * @return
     */
    List<Long> selectByAgencyIdS(Map<String,Object> param);

}
