package com.pms.service.impl;

import com.pms.entity.WaterPumpGroupFault;
import com.pms.mapper.WaterPumpGroupFaultMapper;
import com.pms.service.IWaterPumpGroupFaultService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author asus
 * @since 2018-03-19
 */
@Service
public class WaterPumpGroupFaultServiceImpl extends ServiceImpl<WaterPumpGroupFaultMapper, WaterPumpGroupFault> implements IWaterPumpGroupFaultService {

    @Autowired
    WaterPumpGroupFaultMapper waterPumpGroupFaultMapper;
    @Override
    public List<WaterPumpGroupFault> selectByCommunityIdAndDate(long communityId, String dateStr) {
        return waterPumpGroupFaultMapper.selectByCommunityIdAndDate(communityId,dateStr);
    }
}
