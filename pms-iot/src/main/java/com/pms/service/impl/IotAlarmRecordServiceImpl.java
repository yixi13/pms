package com.pms.service.impl;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.mapper.SqlHelper;
import com.baomidou.mybatisplus.plugins.Page;
import com.pms.entity.IotAlarmRecord;
import com.pms.mapper.IotAlarmRecordMapper;
import com.pms.service.IIotAlarmRecordService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.util.Map;

/**
 * <p>
 * 设备上传数据 服务实现类
 * </p>
 *
 * @author ljb
 * @since 2018-08-20
 */
@Service
public class IotAlarmRecordServiceImpl extends ServiceImpl<IotAlarmRecordMapper, IotAlarmRecord> implements IIotAlarmRecordService {

    Logger logger = LoggerFactory.getLogger(this.getClass());

    @Override
    public Page<Map<String, Object>> selectAlarmRecordRecordMapsPage(Page<Map<String, Object>> page, EntityWrapper<Map<String, Object>> ew) {
        SqlHelper.fillWrapper(page, ew);
        logger.info(ew.getSqlSegment());
        page.setRecords(baseMapper.selectAlarmRecordRecordMapsPage(page, ew));
        return page;
    }

    @Override
    public void deleteBatch(String alarmIds) {
        baseMapper.deleteBatch(alarmIds);
    }

    @Override
    public Map<String, Object> selectApiAlarmRecord(String alarmId) {
        return baseMapper.selectApiAlarmRecord(alarmId);
    }

    public int selectNoReadAlarmRecordCount( EntityWrapper<Map<String, Object>> ew){
        return  baseMapper.selectNoReadAlarmRecordCount(ew);
    }
}
