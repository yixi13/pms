package com.pms.service.impl;

import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.mapper.SqlHelper;
import com.baomidou.mybatisplus.mapper.Wrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.pms.cache.annotation.BaseCache;
import com.pms.cache.annotation.Cache;
import com.pms.entity.WaterPumpGroup;
import com.pms.mapper.WaterPumpGroupMapper;
import com.pms.service.IWaterPumpGroupService;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 水泵表 服务实现类
 * </p>
 *
 * @author lk
 * @since 2017-12-25
 */
@Service
public class WaterPumpGroupServiceImpl extends BaseServiceImpl<WaterPumpGroupMapper, WaterPumpGroup> implements IWaterPumpGroupService {
    /**
     * 根据设备名称获取消息格式化规则
     * @param name
     * @return
     */
    public JSONObject getMessageFormatRuleJson(String name){
//        String ruleJsonStr ="{\"pressureFeedback\":{\"way\":\"divide\",\"val\":\"1000\",\"remark\":\"单位换算Pa为MPa\"},\"settingPressure\":{\"way\":\"divide\",\"val\":\"1000\",\"remark\":\"单位换算Pa为MPa\"},\"waterFrequency\":{\"way\":\"divide\",\"val\":\"10\",\"remark\":\"运行频率固定取值/10\"},\"XXPL\":{\"way\":\"divide\",\"val\":\"10\",\"remark\":\"下行频率固定取值/10\"},\"ultraTightlyAlarm\":{\"way\":\"divide\",\"val\":\"1000\",\"remark\":\"单位换算Pa为MPa\"},\"SZGWYL\":{\"way\":\"divide\",\"val\":\"1000\",\"remark\":\"单位换算Pa为MPa\"},\"SBZT\":{\"way\":\"hexadecimal\",\"val\":\"2\",\"isReversal\":\"1\",\"readWay\":\"index\",\"readVal\":\"1\",\"indexJson\":{\"0\":\"0\",\"1\":\"1\",\"2\":\"0\",\"3\":\"1\",\"4\":\"0\",\"5\":\"1\",\"6\":\"0\",\"7\":\"1\",\"8\":\"0\",\"9\":\"1\",\"10\":\"0\",\"11\":\"1\",\"12\":\"0\",\"13\":\"1\",\"14\":\"2\",\"15\":\"3\",}},\"GZZT\":{\"way\":\"hexadecimal\",\"val\":\"2\",\"isReversal\":\"1\",\"readWay\":\"index\",\"readVal\":\"1\",}}";
        String ruleJsonStr ="{\"pressureFeedback\":{\"way\":\"divide\",\"val\":\"1000\",\"remark\":\"单位换算Pa为MPa\"},\"settingPressure\":{\"way\":\"divide\",\"val\":\"1000\",\"remark\":\"单位换算Pa为MPa\"},\"waterFrequency\":{\"way\":\"divide\",\"val\":\"10\",\"remark\":\"运行频率固定取值/10\"},\"XXPL\":{\"way\":\"divide\",\"val\":\"10\",\"remark\":\"下行频率固定取值/10\"},\"ultraTightlyAlarm\":{\"way\":\"divide\",\"val\":\"1000\",\"remark\":\"单位换算Pa为MPa\"},\"SZGWYL\":{\"way\":\"divide\",\"val\":\"1000\",\"remark\":\"单位换算Pa为MPa\"},\"SBZT\":{\"way\":\"hexadecimal\",\"val\":\"2\",\"isReversal\":\"1\",\"isReversal\":\"1\",\"readWay\":\"compareChar\",},\"GZZT\":{\"way\":\"hexadecimal\",\"val\":\"2\",\"isReversal\":\"1\",\"readWay\":\"index\",\"readVal\":\"1\",}}";
        JSONObject ruleJson =null;
        ruleJson = JSONObject.parseObject(ruleJsonStr);
        return ruleJson;
    }

}
