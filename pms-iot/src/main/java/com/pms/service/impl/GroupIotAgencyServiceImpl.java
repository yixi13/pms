package com.pms.service.impl;

import org.springframework.stereotype.Service;
import com.pms.entity.GroupIotAgency;
import com.pms.mapper.GroupIotAgencyMapper;
import com.pms.service.IGroupIotAgencyService;
/**
 * 
 *
 * @author ljb
 * @email 517770986@qq.com
 * @date 2018-07-31 10:28:59
 */
@Service
public class GroupIotAgencyServiceImpl extends BaseServiceImpl<GroupIotAgencyMapper,GroupIotAgency> implements IGroupIotAgencyService {

    /**
     *  根据角色id 查询角色 拥有的机构权限
     * @param groupId 角色id
     * @return
     */
   public String getAgencyIdsStrByGroupId(String groupId){
       return baseMapper.getAgencyIdsStrByGroupId(groupId);
   }
}