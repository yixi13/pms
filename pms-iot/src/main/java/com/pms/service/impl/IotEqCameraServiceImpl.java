package com.pms.service.impl;
import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.mapper.SqlHelper;
import com.baomidou.mybatisplus.mapper.Wrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.pms.entity.CameraUserInformation;
import com.pms.exception.R;
import com.pms.service.ICameraUserInformationService;
import com.pms.util.FluoriteCloudUtile;
import com.pms.validator.Assert;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.pms.entity.IotEqCamera;
import com.pms.mapper.IotEqCameraMapper;
import com.pms.service.IIotEqCameraService;
import java.util.Date;
import java.util.List;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Map;

/**
 * 
 *
 * @author lk
 * @email 307169223@qq.com
 * @date 2018-08-21 13:52:06
 */
@Service
public class IotEqCameraServiceImpl extends BaseServiceImpl<IotEqCameraMapper,IotEqCamera> implements IIotEqCameraService {
    @Autowired
    IotEqCameraMapper baseDao;
    @Autowired
    private ICameraUserInformationService service;
    //查询当前过期的摄像头
    @Override
    public List<IotEqCamera> findEqCameraByExpireTime(String nowTime, int pageSize) {
        return baseDao.findEqCameraByExpireTime(stringToDate(nowTime), pageSize);
    }

    /**
     * 字符串转换为日期:不支持yyM[M]d[d]格式
     *
     * @param date
     * @return
     */
    public static final Date stringToDate(String date) {
        if (date == null) {
            return null;
        }
        String separator = String.valueOf(date.charAt(4));
        String pattern = "yyyyMMdd";
        if (!separator.matches("\\d*")) {
            pattern = "yyyy" + separator + "MM" + separator + "dd";
            if (date.length() < 10) {
                pattern = "yyyy" + separator + "M" + separator + "d";
            }
        } else if (date.length() < 8) {
            pattern = "yyyyMd";
        }
        pattern += " HH:mm:ss.SSS";
        pattern = pattern.substring(0, Math.min(pattern.length(), date.length()));
        try {
            return new SimpleDateFormat(pattern).parse(date);
        } catch (ParseException e) {
            return null;
        }
    }

    public  List<IotEqCamera> selectByList(Long agencyId){
        return baseDao.selectByList(agencyId);
    }

    public Map<String,Object> readHouseCamera(Long houseId){
        return baseMapper.readHouseCamera(houseId);
    }


    /**
     * 水泵房间ID
     * @param agencyIds
     * @return
     */
    public R deleteByIotEqCameraId(String agencyIds){
        List<IotEqCamera> eq= baseDao.selectList(new EntityWrapper<IotEqCamera>().in("agency_id",agencyIds));
        if (null== eq || eq.size()==0){
//            return R.error(400,"该水泵房下暂无摄像头");
            return R.ok("该水泵房下暂无摄像头");
        }
        for (int x=0;x<eq.size();x++){
            JSONObject cloud = FluoriteCloudUtile.deletefluoriteCloud(eq.get(x).getKey(),eq.get(x).getToken());
            if(cloud.getString("code").equals("200")){
                CameraUserInformation user=service.selectById(eq.get(x).getCameraUserId());
                Assert.isNull(user,"该摄像头用户配置信息不存在");
                user.setSum(user.getSum()-1);
                service.updateAllColumnById(user);
                baseDao.deleteById(eq.get(x).getEqId());
            }else{
                return R.error(Integer.parseInt(cloud.get("code").toString()),cloud.get("msg").toString());
            }
        }
        return R.ok();
    }

    public Page<IotEqCamera> selectPage(Page<IotEqCamera> page, Wrapper<IotEqCamera> wrapper) {
        SqlHelper.fillWrapper(page, wrapper);
        page.setRecords(baseMapper.selectPage(page, wrapper));
        return page;
    }


    public Page<IotEqCamera> selectByEqCameraAndCompanyCodeAll(Page<IotEqCamera> page, Map<String,Object> map) {
        int count=baseDao.countByEqCameraAndCompanyCodeAll(map);
        List<IotEqCamera> list= baseDao.selectByEqCameraAndCompanyCodeAll(map);
        if (list.size()>0){
            for (int x=0;x<list.size();x++){
                JSONObject obj =FluoriteCloudUtile.deviceInfo(list.get(x).getKey(),list.get(x).getToken());
                JSONObject data= obj.getJSONObject("data");
                list.get(x).setStatus(Integer.parseInt(data.get("status").toString()));
            }
        }
        page.setRecords(list);
        page.setTotal(count);
        return  page;
    }


}