package com.pms.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.pms.entity.CameraUserInformation;
import com.pms.mapper.CameraUserInformationMapper;
import com.pms.service.ICameraUserInformationService;

import java.util.List;

/**
 * 
 *
 * @author zyl
 * @email 517770986@qq.com
 * @date 2018-03-15 11:21:02
 */
@Service
public class CameraUserInformationServiceImpl extends BaseServiceImpl<CameraUserInformationMapper,CameraUserInformation> implements ICameraUserInformationService {
    @Autowired
    private CameraUserInformationMapper cameraUserInformationMapper;
    @Override
    public List<CameraUserInformation> seleteBySum() {
        return cameraUserInformationMapper.seleteBySum();
    }

}