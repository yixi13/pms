package com.pms.service.impl;

import com.baomidou.mybatisplus.mapper.SqlHelper;
import com.baomidou.mybatisplus.mapper.Wrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.pms.cache.annotation.BaseCache;
import com.pms.cache.annotation.Cache;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.pms.entity.EqCamera;
import com.pms.mapper.EqCameraMapper;
import com.pms.service.IEqCameraService;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * 
 *
 * @author zyl
 * @email 517770986@qq.com
 * @date 2018-03-15 11:21:02
 */
@Service
public class EqCameraServiceImpl extends BaseServiceImpl<EqCameraMapper,EqCamera> implements IEqCameraService {

    private  static String YYYY_MM_DD_HH_MM_SS = "yyyy-MM-dd HH:mm:ss";
    @Autowired
    private   EqCameraMapper eqCameraMapper;
    //查询当前过期的摄像头
    @Override
    public List<EqCamera> findEqCameraByExpireTime(String nowTime, int pageSize) {
        return eqCameraMapper.findEqCameraByExpireTime(stringToDate(nowTime), pageSize);
    }

    /**
     * 字符串转换为日期:不支持yyM[M]d[d]格式
     *
     * @param date
     * @return
     */
    public static final Date stringToDate(String date) {
        if (date == null) {
            return null;
        }
        String separator = String.valueOf(date.charAt(4));
        String pattern = "yyyyMMdd";
        if (!separator.matches("\\d*")) {
            pattern = "yyyy" + separator + "MM" + separator + "dd";
            if (date.length() < 10) {
                pattern = "yyyy" + separator + "M" + separator + "d";
            }
        } else if (date.length() < 8) {
            pattern = "yyyyMd";
        }
        pattern += " HH:mm:ss.SSS";
        pattern = pattern.substring(0, Math.min(pattern.length(), date.length()));
        try {
            return new SimpleDateFormat(pattern).parse(date);
        } catch (ParseException e) {
            return null;
        }
    }

    @Cache(key ="eqCamera:selectByISMapsPage{1}{2.paramNameValuePairs}")
    public Page<Map<String, Object>> selectByISMapsPage(Page page, Wrapper<EqCamera> wrapper) {
        SqlHelper.fillWrapper(page, wrapper);
        page.setRecords(eqCameraMapper.selectByISMapsPage(page, wrapper));
        return page;
    }


}