package com.pms.service.impl;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.pms.cache.annotation.BaseCacheClear;
import com.pms.entity.Community;
import com.pms.entity.WaterPumpGroup;
import com.pms.mapper.CommunityMapper;
import com.pms.mapper.WaterPumpGroupMapper;
import com.pms.service.ICommunityService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * <p>
 * 社区（小区）表 服务实现类
 * </p>
 *
 * @author lk
 * @since 2017-12-25
 */
@Service
public class CommunityServiceImpl extends BaseServiceImpl<CommunityMapper, Community> implements ICommunityService {

    @Autowired
    CommunityMapper communityMapper;
    @Autowired
    WaterPumpGroupMapper waterPumpMapper;

    @Override
    @Transactional
    @BaseCacheClear(pre=":")
    public boolean deleteCommunityAndWaterPumps(Long communityId) {
//        communityMapper.deleteById(communityId);
        deleteById(communityId);
        waterPumpMapper.delete(new EntityWrapper<WaterPumpGroup>().eq("community_id",communityId));
        return true;
    }

    /**
     * 查询最大的排序
     * @return
     */
   public Long selectMaxCommunitySort(){
       Long maxCommunitySort = communityMapper.selectMaxCommunitySort();
       if(maxCommunitySort==null||maxCommunitySort<1){
           maxCommunitySort = 0L;
       }
     return maxCommunitySort;
   }
}
