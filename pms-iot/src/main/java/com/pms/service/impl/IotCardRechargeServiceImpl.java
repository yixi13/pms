package com.pms.service.impl;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.pms.entity.IotRepairMaintenance;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.pms.entity.IotCardRecharge;
import com.pms.mapper.IotCardRechargeMapper;
import com.pms.service.IIotCardRechargeService;

import java.util.List;
import java.util.Map;

/**
 * 
 *
 * @author lk
 * @email 307169223@qq.com
 * @date 2018-08-20 09:57:14
 */
@Service
public class IotCardRechargeServiceImpl extends BaseServiceImpl<IotCardRechargeMapper,IotCardRecharge> implements IIotCardRechargeService {

    @Autowired
    IotCardRechargeMapper iotCardRechargeMapper;

    public  IotCardRecharge findById(Long cardId){
        return iotCardRechargeMapper.findById(cardId);
    }

    public Page<IotCardRecharge> selectByListPage(Page<IotCardRecharge> page, Map<String,Object> map){
        int count=iotCardRechargeMapper.countByListPage(map);
        List<IotCardRecharge> list= iotCardRechargeMapper.selectByPageList(map);
        page.setRecords(list);
        page.setSize(Integer.parseInt(map.get("limit").toString()));
        page.setTotal(count);
        return  page;
    }
}