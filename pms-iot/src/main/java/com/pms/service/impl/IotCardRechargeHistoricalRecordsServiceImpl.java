package com.pms.service.impl;

import com.pms.entity.IotCardRechargeHistoricalRecords;
import com.pms.mapper.IotCardRechargeHistoricalRecordsMapper;
import com.pms.service.IIotCardRechargeHistoricalRecordsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 
 *
 * @author lk
 * @email 307169223@qq.com
 * @date 2018-08-20 09:57:14
 */
@Service
public class IotCardRechargeHistoricalRecordsServiceImpl extends BaseServiceImpl<IotCardRechargeHistoricalRecordsMapper,IotCardRechargeHistoricalRecords> implements IIotCardRechargeHistoricalRecordsService {

    @Autowired
    public  IotCardRechargeHistoricalRecordsMapper iotCardRechargeHistoricalRecordsMapper;

    @Override
    public List<IotCardRechargeHistoricalRecords> selectListByData(Long groupId){
        List<IotCardRechargeHistoricalRecords> list= iotCardRechargeHistoricalRecordsMapper.getListByData(groupId);
        return list;
    }
}