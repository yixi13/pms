package com.pms.service.impl;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.mapper.SqlHelper;
import com.baomidou.mybatisplus.mapper.Wrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.pms.entity.IotWaterPumpGroupAttr;
import com.pms.mapper.IotWaterPumpGroupAttrMapper;
import com.pms.service.IIotWaterPumpGroupAttrService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.pms.service.IIotWaterPumpGroupService;
import com.pms.service.IWoGiaWpAttrService;
import com.pms.util.DateUtil;
import com.pms.utils.WoGiaFiledConstant;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.util.*;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author asus
 * @since 2018-07-23
 */
@Service
public class IotWaterPumpGroupAttrServiceImpl extends ServiceImpl<IotWaterPumpGroupAttrMapper, IotWaterPumpGroupAttr> implements IIotWaterPumpGroupAttrService {
    @Autowired
    IIotWaterPumpGroupService waterPumpGroupService;
    @Autowired
    IWoGiaWpAttrService woGiaWpAttrService;

   public Page<Map<String,Object>> readPumpGroupHistory(Page<Map<String,Object>> page,Map<String,Object> sqlMap){
       Integer deviceDataSource = (Integer) sqlMap.get("deviceDataSource");
       String deviceName = (String) sqlMap.get("deviceName");
       List<Map<String,Object>> returnList = new ArrayList<Map<String,Object>>();
       if(deviceDataSource==1){
           Wrapper<Map<String,Object>> wrapper = new EntityWrapper<Map<String,Object>>();
           wrapper.eq("deviceName",deviceName);
           wrapper.orderBy("reportTime",false);// 根据上报时间降序查询
           SqlHelper.fillWrapper(page, wrapper);
           List<Map<String,Object>> dataList = baseMapper.readPumpGroupHistory(page,wrapper);
           if(dataList==null){
               page.setRecords(null);
               return page;
           }
           for(int x=0,length =dataList.size();x<length;x++){
               Long count = (Long) dataList.get(x).get("count");// 小泵数量
               if(count==null){// 无数据上报
                   count=0L;
                   dataList.get(x).put("count",0);
               }
               String[] SBZT_arr = dataList.get(x).get("SBZT").toString().split(",");
               String[] waterFrequency_arr = dataList.get(x).get("waterFrequency").toString().split(",");
               for (int y = 0; y < count; y++) {
                   Map<String, Object> pumpAttrMap = new HashMap<String, Object>();
                   pumpAttrMap.put("pumpName", (y+1) + "号泵");
                   // 水泵频率
                   pumpAttrMap.put("pumpFrequency",waterFrequency_arr.length>y?waterFrequency_arr[y]:waterFrequency_arr[0]);
                   // 水泵运行状态
                   pumpAttrMap.put("pumpState",SBZT_arr.length>y?SBZT_arr[y]:SBZT_arr[0]);
                   pumpAttrMap.put("pumpStateName",waterPumpGroupService.formatPumpStateStr(pumpAttrMap.get("pumpState")+""));
                   pumpAttrMap.putAll(dataList.get(x));
                   returnList.add(pumpAttrMap);
               }
           }
           page.setRecords(returnList );
       }
       if(deviceDataSource==2){
           String[] deviceNameArr = deviceName.split("\\|");
           Map<String,String> cloumnParamMap = new HashMap<String,String>();
           cloumnParamMap.put("tablename",deviceNameArr[0]);
           cloumnParamMap.put("columnPrifix",deviceNameArr[2]);
           List<String> columnNameList=woGiaWpAttrService.selectAllColumnByTableName(cloumnParamMap);
           String readColumn = StringUtils.join(columnNameList,",");
           cloumnParamMap.put("tablename","`"+deviceNameArr[0]+"`");
           cloumnParamMap.put("columnStr","Time,"+readColumn);
           cloumnParamMap.put("whereStr"," order by Time desc limit "+( (page.getCurrent() -1)*page.getSize() )+","+page.getSize());
           List<Map<String,Object>> dataList = woGiaWpAttrService.selectAttrByTablenameAndColumn(cloumnParamMap);
           for(int x=0,length =dataList.size();x<length;x++){
               Map<String,Object> returnMap = new HashMap<String,Object>();
               //waterFrequency,SZGWYL,pressureFeedback,settingPressure
               for(Map.Entry<String, Object> entry : dataList.get(x).entrySet()){
                   String columnSuffix = entry.getKey().substring(1);
                   if(WoGiaFiledConstant.PUMP_SIGNAL_ONE.equals(columnSuffix) ){ //1号泵运行状态
                       Map<String, Object> pumpAttrMap =woGiaWpAttrService.generatePumpState(1, entry.getValue());
                       // 水泵频率
                        Object pumpFrequency =dataList.get(x).get(deviceNameArr[2]+WoGiaFiledConstant.FREQUENCY_CONVERTER_ONE);
                       pumpAttrMap.put("pumpFrequency",formatDoubleData(pumpFrequency));
                       //时间
                       Date reportTimeFormat = (Date) dataList.get(x).get("Time");
                       pumpAttrMap.put("reportTimeFormat", DateUtil.format(reportTimeFormat, DateUtil.DATE_PATTERN.YYYY_MM_DD_HH_MM_SS));
                       //进水压力
                       Object SZGWYL = dataList.get(x).get(deviceNameArr[2]+WoGiaFiledConstant.INLET_PRESSURE);
                       pumpAttrMap.put("SZGWYL",formatDoubleData(SZGWYL));
                       //额定压力
                       Object settingPressure = dataList.get(x).get(deviceNameArr[2]+WoGiaFiledConstant.SET_PRESSURE);
                       pumpAttrMap.put("settingPressure",formatDoubleData(settingPressure));
                       //出水压力
                       Object pressureFeedback = dataList.get(x).get(deviceNameArr[2]+WoGiaFiledConstant.OUT_PRESSURE);
                       pumpAttrMap.put("pressureFeedback",formatDoubleData(pressureFeedback));
                       returnList.add(pumpAttrMap);
                       continue;
                   }
                   if(WoGiaFiledConstant.PUMP_SIGNAL_TWO.equals(columnSuffix) ){ //2号泵运行状态
                       Map<String, Object> pumpAttrMap =woGiaWpAttrService.generatePumpState(2, entry.getValue());
                       // 水泵频率
                       Object pumpFrequency =dataList.get(x).get(deviceNameArr[2]+WoGiaFiledConstant.FREQUENCY_CONVERTER_TWO);
                       if(pumpFrequency==null){
                           pumpFrequency = dataList.get(x).get(deviceNameArr[2]+WoGiaFiledConstant.FREQUENCY_CONVERTER_ONE);
                       }
                       pumpAttrMap.put("pumpFrequency",formatDoubleData(pumpFrequency));
                       //时间
                       Date reportTimeFormat = (Date) dataList.get(x).get("Time");
                       pumpAttrMap.put("reportTimeFormat", DateUtil.format(reportTimeFormat, DateUtil.DATE_PATTERN.YYYY_MM_DD_HH_MM_SS));
                       //进水压力
                       Object SZGWYL = dataList.get(x).get(deviceNameArr[2]+WoGiaFiledConstant.INLET_PRESSURE);
                       pumpAttrMap.put("SZGWYL",formatDoubleData(SZGWYL));
                       //额定压力
                       Object settingPressure = dataList.get(x).get(deviceNameArr[2]+WoGiaFiledConstant.SET_PRESSURE);
                       pumpAttrMap.put("settingPressure",formatDoubleData(settingPressure));
                       //出水压力
                       Object pressureFeedback = dataList.get(x).get(deviceNameArr[2]+WoGiaFiledConstant.OUT_PRESSURE);
                       pumpAttrMap.put("pressureFeedback",formatDoubleData(pressureFeedback));
                       returnList.add(pumpAttrMap);
                       continue;
                   }
                   if(WoGiaFiledConstant.PUMP_SIGNAL_THREE.equals(columnSuffix) ){ //3号泵运行状态
                       Map<String, Object> pumpAttrMap =woGiaWpAttrService.generatePumpState(3, entry.getValue());
                       // 水泵频率
                       Object pumpFrequency =dataList.get(x).get(deviceNameArr[2]+WoGiaFiledConstant.FREQUENCY_CONVERTER_THREE);
                       if(pumpFrequency==null){
                           pumpFrequency = dataList.get(x).get(deviceNameArr[2]+WoGiaFiledConstant.FREQUENCY_CONVERTER_ONE);
                       }
                       pumpAttrMap.put("pumpFrequency",formatDoubleData(pumpFrequency));
                       //时间
                       Date reportTimeFormat = (Date) dataList.get(x).get("Time");
                       pumpAttrMap.put("reportTimeFormat", DateUtil.format(reportTimeFormat, DateUtil.DATE_PATTERN.YYYY_MM_DD_HH_MM_SS));
                       //进水压力
                       Object SZGWYL = dataList.get(x).get(deviceNameArr[2]+WoGiaFiledConstant.INLET_PRESSURE);
                       pumpAttrMap.put("SZGWYL",formatDoubleData(SZGWYL));
                       //额定压力
                       Object settingPressure = dataList.get(x).get(deviceNameArr[2]+WoGiaFiledConstant.SET_PRESSURE);
                       pumpAttrMap.put("settingPressure",formatDoubleData(settingPressure));
                       //出水压力
                       Object pressureFeedback = dataList.get(x).get(deviceNameArr[2]+WoGiaFiledConstant.OUT_PRESSURE);
                       pumpAttrMap.put("pressureFeedback",formatDoubleData(pressureFeedback));
                       returnList.add(pumpAttrMap);
                       continue;
                   }
                   if(WoGiaFiledConstant.PUMP_SIGNAL_FOUR.equals(columnSuffix) ){ //4号泵运行状态
                       Map<String, Object> pumpAttrMap =woGiaWpAttrService.generatePumpState(4, entry.getValue());
                       // 水泵频率
                       Object pumpFrequency =dataList.get(x).get(deviceNameArr[2]+WoGiaFiledConstant.FREQUENCY_CONVERTER_FOUR);
                       if(pumpFrequency==null){
                           pumpFrequency = dataList.get(x).get(deviceNameArr[2]+WoGiaFiledConstant.FREQUENCY_CONVERTER_ONE);
                       }
                       pumpAttrMap.put("pumpFrequency",formatDoubleData(pumpFrequency));
                       //时间
                       Date reportTimeFormat = (Date) dataList.get(x).get("Time");
                       pumpAttrMap.put("reportTimeFormat", DateUtil.format(reportTimeFormat, DateUtil.DATE_PATTERN.YYYY_MM_DD_HH_MM_SS));
                       //进水压力
                       Object SZGWYL = dataList.get(x).get(deviceNameArr[2]+WoGiaFiledConstant.INLET_PRESSURE);
                       pumpAttrMap.put("SZGWYL",formatDoubleData(SZGWYL));
                       //额定压力
                       Object settingPressure = dataList.get(x).get(deviceNameArr[2]+WoGiaFiledConstant.SET_PRESSURE);
                       pumpAttrMap.put("settingPressure",formatDoubleData(settingPressure));
                       //出水压力
                       Object pressureFeedback = dataList.get(x).get(deviceNameArr[2]+WoGiaFiledConstant.OUT_PRESSURE);
                       pumpAttrMap.put("pressureFeedback",formatDoubleData(pressureFeedback));
                       returnList.add(pumpAttrMap);
                       continue;
                   }
                   if(WoGiaFiledConstant.PUMP_SIGNAL_FIVE.equals(columnSuffix) ){ //5号泵运行状态
                       Map<String, Object> pumpAttrMap =woGiaWpAttrService.generatePumpState(5, entry.getValue());
                       // 水泵频率
                       Object pumpFrequency =dataList.get(x).get(deviceNameArr[2]+WoGiaFiledConstant.FREQUENCY_CONVERTER_FIVE);
                       if(pumpFrequency==null){
                           pumpFrequency = dataList.get(x).get(deviceNameArr[2]+WoGiaFiledConstant.FREQUENCY_CONVERTER_ONE);
                       }
                       pumpAttrMap.put("pumpFrequency",formatDoubleData(pumpFrequency));
                       //时间
                       Date reportTimeFormat = (Date) dataList.get(x).get("Time");
                       pumpAttrMap.put("reportTimeFormat", DateUtil.format(reportTimeFormat, DateUtil.DATE_PATTERN.YYYY_MM_DD_HH_MM_SS));
                       //进水压力
                       Object SZGWYL = dataList.get(x).get(deviceNameArr[2]+WoGiaFiledConstant.INLET_PRESSURE);
                       pumpAttrMap.put("SZGWYL",formatDoubleData(SZGWYL));
                       //额定压力
                       Object settingPressure = dataList.get(x).get(deviceNameArr[2]+WoGiaFiledConstant.SET_PRESSURE);
                       pumpAttrMap.put("settingPressure",formatDoubleData(settingPressure));
                       //出水压力
                       Object pressureFeedback = dataList.get(x).get(deviceNameArr[2]+WoGiaFiledConstant.OUT_PRESSURE);
                       pumpAttrMap.put("pressureFeedback",formatDoubleData(pressureFeedback));
                       returnList.add(pumpAttrMap);
                       continue;
                   }
                   if(WoGiaFiledConstant.PUMP_SIGNAL_SIX.equals(columnSuffix) ){ //6号泵运行状态
                       Map<String, Object> pumpAttrMap =woGiaWpAttrService.generatePumpState(6, entry.getValue());
                       // 水泵频率
                       Object pumpFrequency =dataList.get(x).get(deviceNameArr[2]+WoGiaFiledConstant.FREQUENCY_CONVERTER_SIX);
                       if(pumpFrequency==null){
                           pumpFrequency = dataList.get(x).get(deviceNameArr[2]+WoGiaFiledConstant.FREQUENCY_CONVERTER_ONE);
                       }
                       pumpAttrMap.put("pumpFrequency",formatDoubleData(pumpFrequency));
                       //时间
                       Date reportTimeFormat = (Date) dataList.get(x).get("Time");
                       pumpAttrMap.put("reportTimeFormat", DateUtil.format(reportTimeFormat, DateUtil.DATE_PATTERN.YYYY_MM_DD_HH_MM_SS));
                       //进水压力
                       Object SZGWYL = dataList.get(x).get(deviceNameArr[2]+WoGiaFiledConstant.INLET_PRESSURE);
                       pumpAttrMap.put("SZGWYL",formatDoubleData(SZGWYL));
                       //额定压力
                       Object settingPressure = dataList.get(x).get(deviceNameArr[2]+WoGiaFiledConstant.SET_PRESSURE);
                       pumpAttrMap.put("settingPressure",formatDoubleData(settingPressure));
                       //出水压力
                       Object pressureFeedback = dataList.get(x).get(deviceNameArr[2]+WoGiaFiledConstant.OUT_PRESSURE);
                       pumpAttrMap.put("pressureFeedback",formatDoubleData(pressureFeedback));
                       pumpAttrMap.put("deviceStatus",0);
                       returnList.add(pumpAttrMap);
                       continue;
                   }
               }// 表中最新数据 的 列
           }// 遍历数据
       }// 进入wogia 判断
       page.setRecords(returnList);
       return page;
    }


    public static Object formatDoubleData(Object obj){
        if(obj instanceof Double){
            BigDecimal bg = new BigDecimal((Double)obj);
            obj = bg.setScale(2, BigDecimal.ROUND_HALF_UP).doubleValue();
        }
        if(obj instanceof Float){
            BigDecimal bg = new BigDecimal(String.valueOf(obj));
            obj = bg.setScale(2, BigDecimal.ROUND_HALF_UP).doubleValue();
        }
       return obj;
    }

}
