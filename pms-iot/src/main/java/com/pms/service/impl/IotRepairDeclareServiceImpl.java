package com.pms.service.impl;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.mapper.SqlHelper;
import com.baomidou.mybatisplus.mapper.Wrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.pms.entity.IotRepairMaintenance;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;
import com.pms.entity.IotRepairDeclare;
import com.pms.mapper.IotRepairDeclareMapper;
import com.pms.service.IIotRepairDeclareService;

import java.util.List;
import java.util.Map;

/**
 * 
 *
 * @author lk
 * @email 307169223@qq.com
 * @date 2018-08-20 09:57:14
 */
@Service
public class IotRepairDeclareServiceImpl extends BaseServiceImpl<IotRepairDeclareMapper,IotRepairDeclare> implements IIotRepairDeclareService {
        @Autowired
    IotRepairDeclareMapper iotRepairDeclareMapper;

    public Page<Map<String, Object>> getByIotRepairDeclarePage( Page<Map<String,Object>> page,Map<String,Object> map){
        int count=iotRepairDeclareMapper.countByIotRepairDeclarePage(map);
        List<Map<String,Object>> list= iotRepairDeclareMapper.getByIotRepairDeclarePage(map);
        page.setRecords(list);
        page.setSize(Integer.parseInt(map.get("limit").toString()));
        page.setTotal(count);
        return  page;
    }


    public Map<String,Object> getByIotRepairDeclare(Long repairMaintenanceId){
        return  iotRepairDeclareMapper.getByIotRepairDeclare(repairMaintenanceId);
    }

    public  Page<IotRepairDeclare> selectByIotRepairDeclarePage(Page<IotRepairDeclare> page, Wrapper<IotRepairDeclare> wrapper){
        SqlHelper.fillWrapper(page, wrapper);
        page.setRecords(iotRepairDeclareMapper.selectByIotRepairDeclarePage(page,wrapper));
        return page;
    }

    public  List<IotRepairDeclare> selectByIotRepairIstrue(Wrapper<IotRepairDeclare> wrapper){

        return iotRepairDeclareMapper.selectByIotRepairDeclarePage(wrapper);
    }


}