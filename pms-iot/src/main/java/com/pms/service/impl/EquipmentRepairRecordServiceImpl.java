package com.pms.service.impl;

import org.springframework.stereotype.Service;
import com.pms.entity.EquipmentRepairRecord;
import com.pms.mapper.EquipmentRepairRecordMapper;
import com.pms.service.IEquipmentRepairRecordService;
/**
 * 
 *
 * @author lk
 * @email
 * @date 2018-04-25 14:26:06
 */
@Service
public class EquipmentRepairRecordServiceImpl extends BaseServiceImpl<EquipmentRepairRecordMapper,EquipmentRepairRecord> implements IEquipmentRepairRecordService {

}