package com.pms.service.impl;

import com.baomidou.mybatisplus.mapper.Wrapper;
import com.pms.cache.annotation.BaseCache;
import com.pms.cache.annotation.Cache;
import com.pms.entity.AreaCommunity;
import com.pms.mapper.AreaCommunityMapper;
import com.pms.service.IAreaCommunityService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

/**
 * 
 *
 * @author zyl
 * @email 517770986@qq.com
 * @date 2018-03-23 16:39:08
 */
@Service
public class AreaCommunityServiceImpl extends BaseServiceImpl<AreaCommunityMapper,AreaCommunity> implements IAreaCommunityService {

    @Autowired
   private AreaCommunityMapper areaCommunityMapper;

   public List<Map<String,Object>> selectByAreaCommunity(){
       return areaCommunityMapper.selectByAreaCommunity();
    }

    @Cache(key ="areaCommunity:selectAreaGbAndCommunity{1.paramNameValuePairs}")
    public List<Map<String, Object>> selectAreaGbAndCommunity(Wrapper<AreaCommunity> wrapper) {
       return baseMapper.selectAreaGbAndCommunity(wrapper);
    }


}