package com.pms.service.impl;

import com.pms.annotation.TargetDataSource;
import com.pms.config.DataSourceNameConstant;
import com.pms.exception.RRException;
import com.pms.mapper.WoGiaWpAttrMapper;
import com.pms.service.IWoGiaWpAttrService;
import com.pms.utils.WoGiaFiledConstant;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;

/**
 * <p>
 * 社区（小区）表 服务实现类
 * </p>
 *
 * @author lk
 * @since 2017-12-25
 */
@Service
public class WoGiaWpAttrServiceImpl implements IWoGiaWpAttrService {

    @Autowired
    WoGiaWpAttrMapper woGiaWpAttrMapper;

    /**
     * 根据表名查询数据
     * tablename-表名,columnStr-查询列名,whereStr-条件语句
     * @return
     */
    @TargetDataSource(name=DataSourceNameConstant.WOGIA_DATASOURCE)
   public List<Map<String,Object>> selectAttrByTablenameAndColumn(Map<String,String> map){
       if(map==null||map.isEmpty()){
           throw new RRException("查询参数不能为空",400);
       }
       if(StringUtils.isBlank(map.get("tablename")) ){
           throw new RRException("查询表名不能为空",400);
       }
       if(StringUtils.isBlank(map.get("columnStr")) ){// 如果查询字段为空，则默认查询所有
           map.put("columnStr","*");
       }
        return woGiaWpAttrMapper.selectAttrByTablenameAndColumn(map);
    }

    /**
     * 查询表名是否存在
     * @return
     */
    @TargetDataSource(name= DataSourceNameConstant.WOGIA_DATASOURCE)
    public Integer selectTableIsExist(String tablename) {
        return woGiaWpAttrMapper.selectTableIsExist(tablename);
    }



    /**
     * 验证 设备是否故障
     * 	@FiledMark(comment="最新故障码",desc="0-正常,1-水位低,2-变频器故障,3-超压报警,4-传感器故障,5-自检故障,6/9/12/15/18/21-1/2/3/4/5/6号泵故障,7/10/13/16/19/22-1/2/3/4/5/6号泵过流,8/11/14/17/20/23-1/2/3/4/5/6号泵缺相,"
    ,unit="")
     * @param map
     * @param columnPrifix
     * @return isFault-是否故障,ZXGZ-(2-变频器故障，1-稳流罐异常，3-超压报警)
     */
    public Map<String,Object> getWoGiaIsFault(Map<String,Object> map,String columnPrifix){
        Map<String,Object> IsFaultMap = new HashMap<String,Object>();
        Object faultValue = map.get(columnPrifix+ WoGiaFiledConstant.OUTLET_PRESSURE_SUPER_HIGH);
        if(faultValue!=null){
            if (faultValue instanceof Float) {
                Float waterFrequencyStatusDouble = ((Float) faultValue).floatValue();
                int w60Int=waterFrequencyStatusDouble.intValue();
                if(w60Int==1){
                    IsFaultMap.put("isFault",1);// 故障
                    IsFaultMap.put("ZXGZ",3);// 3-超压报警
                    return IsFaultMap;
                }
            }
            if(faultValue instanceof  Integer){
                int w60Int = Integer.parseInt(faultValue.toString());
                if(w60Int==1){
                    IsFaultMap.put("isFault",1);// 故障
                    IsFaultMap.put("ZXGZ",3);// 3-超压报警
                    return IsFaultMap;
                }
            }
        }
        faultValue = map.get(columnPrifix+ WoGiaFiledConstant.INLET_PRESSURE_LOW);//稳流罐
        if(faultValue!=null){
            if (faultValue instanceof Float) {
                Float waterFrequencyStatusDouble = ((Float) faultValue).floatValue();
                int w61Int=waterFrequencyStatusDouble.intValue();
                if(w61Int==1){
                    IsFaultMap.put("isFault",1);// 故障
                    IsFaultMap.put("ZXGZ",1);// 1-水位低
                    return IsFaultMap;
                }
            }
            if(faultValue instanceof  Integer){
                int w61Int = Integer.parseInt(faultValue.toString());
                if(w61Int==1){
                    IsFaultMap.put("isFault",1);// 故障
                    IsFaultMap.put("ZXGZ",1);// 1-水位低
                    return IsFaultMap;
                }
            }
        }
        faultValue = map.get(columnPrifix+ WoGiaFiledConstant.FREQUENCY_STATUS);// 1#变频器故障 0-正常1-故障
        if(faultValue!=null){
            if (faultValue instanceof Float) {
                Float waterFrequencyStatusDouble = ((Float) faultValue).floatValue();
                int w64Int=waterFrequencyStatusDouble.intValue();
                if(w64Int==1){
                    IsFaultMap.put("isFault",1);// 故障
                    IsFaultMap.put("ZXGZ",2);// 2-变频器故障
                    return IsFaultMap;
                }
            }
            if(faultValue instanceof  Integer){
                int w64Int = Integer.parseInt(faultValue.toString());
                if(w64Int==1){
                    IsFaultMap.put("isFault",1);// 故障
                    IsFaultMap.put("ZXGZ",2);// 2-变频器故障
                    return IsFaultMap;
                }
            }
        }
        faultValue = map.get(columnPrifix+ WoGiaFiledConstant.FREQUENCY_STATUS_TWO);// 2#变频器故障 0-正常1-故障
        if(faultValue!=null){
            if (faultValue instanceof Float) {
                Float waterFrequencyStatusDouble = ((Float) faultValue).floatValue();
                int w64Int=waterFrequencyStatusDouble.intValue();
                if(w64Int==1){
                    IsFaultMap.put("isFault",1);// 故障
                    IsFaultMap.put("ZXGZ",2);// 2-变频器故障
                    return IsFaultMap;
                }
            }
            if(faultValue instanceof  Integer){
                int w64Int = Integer.parseInt(faultValue.toString());
                if(w64Int==1){
                    IsFaultMap.put("isFault",1);// 故障
                    IsFaultMap.put("ZXGZ",2);// 2-变频器故障
                    return IsFaultMap;
                }
            }
        }
        faultValue = map.get(columnPrifix+ WoGiaFiledConstant.FREQUENCY_STATUS_THREE);// 3#变频器故障 0-正常1-故障
        if(faultValue!=null){
            if (faultValue instanceof Float) {
                Float waterFrequencyStatusDouble = ((Float) faultValue).floatValue();
                int w64Int=waterFrequencyStatusDouble.intValue();
                if(w64Int==1){
                    IsFaultMap.put("isFault",1);// 故障
                    IsFaultMap.put("ZXGZ",2);// 2-变频器故障
                    return IsFaultMap;
                }
            }
            if(faultValue instanceof  Integer){
                int w64Int = Integer.parseInt(faultValue.toString());
                if(w64Int==1){
                    IsFaultMap.put("isFault",1);// 故障
                    IsFaultMap.put("ZXGZ",2);// 2-变频器故障
                    return IsFaultMap;
                }
            }
        }
        faultValue = map.get(columnPrifix+ WoGiaFiledConstant.FREQUENCY_STATUS_FOUR);// 4#变频器故障 0-正常1-故障
        if(faultValue!=null){
            if (faultValue instanceof Float) {
                Float waterFrequencyStatusDouble = ((Float) faultValue).floatValue();
                int w64Int=waterFrequencyStatusDouble.intValue();
                if(w64Int==1){
                    IsFaultMap.put("isFault",1);// 故障
                    IsFaultMap.put("ZXGZ",2);// 2-变频器故障
                    return IsFaultMap;
                }
            }
            if(faultValue instanceof  Integer){
                int w64Int = Integer.parseInt(faultValue.toString());
                if(w64Int==1){
                    IsFaultMap.put("isFault",1);// 故障
                    IsFaultMap.put("ZXGZ",2);// 2-变频器故障
                    return IsFaultMap;
                }
            }
        }
        faultValue = map.get(columnPrifix+ WoGiaFiledConstant.FREQUENCY_STATUS_FIVE);// 5#变频器故障 0-正常1-故障
        if(faultValue!=null){
            if (faultValue instanceof Float) {
                Float waterFrequencyStatusDouble = ((Float) faultValue).floatValue();
                int w64Int=waterFrequencyStatusDouble.intValue();
                if(w64Int==1){
                    IsFaultMap.put("isFault",1);// 故障
                    IsFaultMap.put("ZXGZ",2);// 2-变频器故障
                    return IsFaultMap;
                }
            }
            if(faultValue instanceof  Integer){
                int w64Int = Integer.parseInt(faultValue.toString());
                if(w64Int==1){
                    IsFaultMap.put("isFault",1);// 故障
                    IsFaultMap.put("ZXGZ",2);// 2-变频器故障
                    return IsFaultMap;
                }
            }
        }
        faultValue = map.get(columnPrifix+ WoGiaFiledConstant.FREQUENCY_STATUS_SIX);// 6#变频器故障 0-正常1-故障
        if(faultValue!=null){
            if (faultValue instanceof Float) {
                Float waterFrequencyStatusDouble = ((Float) faultValue).floatValue();
                int w64Int=waterFrequencyStatusDouble.intValue();
                if(w64Int==1){
                    IsFaultMap.put("isFault",1);// 故障
                    IsFaultMap.put("ZXGZ",2);// 2-变频器故障
                    return IsFaultMap;
                }
            }
            if(faultValue instanceof  Integer){
                int w64Int = Integer.parseInt(faultValue.toString());
                if(w64Int==1){
                    IsFaultMap.put("isFault",1);// 故障
                    IsFaultMap.put("ZXGZ",2);// 2-变频器故障
                    return IsFaultMap;
                }
            }
        }
        if(IsFaultMap.get("isFault")==null){
            IsFaultMap.put("isFault",0);// 正常
            IsFaultMap.put("ZXGZ",0);// 正常
        }
        return IsFaultMap;
    }

    /**
     *  沃佳================0-停机 1工频 2-变频 3故障 4检修 5-现场手动
     * 本地================0-变频运行,1-工频运行,2-未运行(停机),3-电磁阀,4-未使用,5-故障,6-检修,7-现场手动
     * 根据mao结果  获取 SBZT 水泵状态
     * @param map 设备属性mysql结果集Map
     * @param columnPrifix column前缀
     * @return map(count-泵运行数量,SBZT-水泵状态)
     */
    public Map<String,Object> getWoGiaSBZT(Map<String,Object> map,String columnPrifix){
        Map<String,Object> SBZTMap = new HashMap<String,Object>();
        Integer waterFrequencyStatus = null;
        int waterFrequencyNum =0;
        StringBuffer  SBZTBuffer = new StringBuffer("");
        Object waterFrequencyStatusObj = map.get(columnPrifix+ WoGiaFiledConstant.PUMP_SIGNAL_ONE);
        if(waterFrequencyStatusObj==null){
            waterFrequencyStatusObj=0;
        }else{
            waterFrequencyNum +=1;
        }
        waterFrequencyStatus= ObjectToInteger(waterFrequencyStatusObj);
        switch (waterFrequencyStatus){
            case 0: SBZTBuffer.append("2,");break;// 停机的泵 不计入 变频器数量
            case 1: SBZTBuffer.append("1,");break;
            case 2: SBZTBuffer.append("0,");break;
            case 3: SBZTBuffer.append("5,");break;
            case 4: SBZTBuffer.append("6,");break;
            case 5: SBZTBuffer.append("7,");break;
            default: SBZTBuffer.append("2,");break;
        }
        waterFrequencyStatusObj =map.get(columnPrifix+ WoGiaFiledConstant.PUMP_SIGNAL_TWO);
        if(waterFrequencyStatusObj==null){
            waterFrequencyStatusObj=0;
        }else{
            waterFrequencyNum +=1;
        }
        waterFrequencyStatus= ObjectToInteger(waterFrequencyStatusObj);
        switch (waterFrequencyStatus){
            case 0: SBZTBuffer.append("2,");break;// 停机的泵 不计入 变频器数量
            case 1: SBZTBuffer.append("1,");break;
            case 2: SBZTBuffer.append("0,");break;
            case 3: SBZTBuffer.append("5,");break;
            case 4: SBZTBuffer.append("6,");break;
            case 5: SBZTBuffer.append("7,");break;
            default: SBZTBuffer.append("2,");break;
        }
        waterFrequencyStatusObj = map.get(columnPrifix+ WoGiaFiledConstant.PUMP_SIGNAL_THREE);
        if(waterFrequencyStatusObj==null){
            waterFrequencyStatusObj=0;
        }else{
            waterFrequencyNum +=1;
        }
        waterFrequencyStatus= ObjectToInteger(waterFrequencyStatusObj);
        switch (waterFrequencyStatus){
            case 0: SBZTBuffer.append("2,");break;// 停机的泵 不计入 变频器数量
            case 1: SBZTBuffer.append("1,");break;
            case 2: SBZTBuffer.append("0,");break;
            case 3: SBZTBuffer.append("5,");break;
            case 4: SBZTBuffer.append("6,");break;
            case 5: SBZTBuffer.append("7,");break;
            default: SBZTBuffer.append("2,");break;
        }
        waterFrequencyStatusObj = map.get(columnPrifix+ WoGiaFiledConstant.PUMP_SIGNAL_FOUR);
        if(waterFrequencyStatusObj==null){
            waterFrequencyStatusObj=0;
        }else{
            waterFrequencyNum +=1;
        }
        waterFrequencyStatus= ObjectToInteger(waterFrequencyStatusObj);
        switch (waterFrequencyStatus){
            case 0: SBZTBuffer.append("2,");break;// 停机的泵 不计入 变频器数量
            case 1: SBZTBuffer.append("1,");break;
            case 2: SBZTBuffer.append("0,");break;
            case 3: SBZTBuffer.append("5,");break;
            case 4: SBZTBuffer.append("6,");break;
            case 5: SBZTBuffer.append("7,");break;
            default: SBZTBuffer.append("2,");break;
        }
        waterFrequencyStatusObj = map.get(columnPrifix+ WoGiaFiledConstant.PUMP_SIGNAL_FIVE);
        if(waterFrequencyStatusObj==null){
            waterFrequencyStatusObj=0;
        }else{
            waterFrequencyNum +=1;
        }
        waterFrequencyStatus= ObjectToInteger(waterFrequencyStatusObj);
        switch (waterFrequencyStatus){
            case 0: SBZTBuffer.append("2,");break;// 停机的泵 不计入 变频器数量
            case 1: SBZTBuffer.append("1,");break;
            case 2: SBZTBuffer.append("0,");break;
            case 3: SBZTBuffer.append("5,");break;
            case 4: SBZTBuffer.append("6,");break;
            case 5: SBZTBuffer.append("7,");break;
            default: SBZTBuffer.append("2,");break;
        }
        waterFrequencyStatusObj = map.get(columnPrifix+ WoGiaFiledConstant.PUMP_SIGNAL_SIX);
        if(waterFrequencyStatusObj==null){
            waterFrequencyStatusObj=0;
        }else{
            waterFrequencyNum +=1;
        }
        waterFrequencyStatus= ObjectToInteger(waterFrequencyStatusObj);
        switch (waterFrequencyStatus){
            case 0: SBZTBuffer.append("2,");break;// 停机的泵 不计入 变频器数量
            case 1: SBZTBuffer.append("1,");break;
            case 2: SBZTBuffer.append("0,");break;
            case 3: SBZTBuffer.append("5,");break;
            case 4: SBZTBuffer.append("6,");break;
            case 5: SBZTBuffer.append("7,");break;
            default: SBZTBuffer.append("2,");break;
        }
        SBZTMap.put("count",waterFrequencyNum);
        SBZTMap.put("SBZT",SBZTBuffer.toString());
        SBZTMap.put("sbzt",SBZTMap.get("SBZT"));
        return SBZTMap;
    }

    public int ObjectToInteger(Object obj){
        Integer returnInt = 0;
        if (obj instanceof Double) {
            Double waterFrequencyStatusDouble = ((Double) obj).doubleValue();
            returnInt=waterFrequencyStatusDouble.intValue();
        }
        if (obj instanceof Float) {
            Float waterFrequencyStatusDouble = ((Float) obj).floatValue();
            returnInt=waterFrequencyStatusDouble.intValue();
        }
        if(obj instanceof  Integer){
            returnInt = Integer.parseInt(obj.toString());
        }
        return returnInt;
    }

    /**
     *  转换沃佳数据
     * @param map 设备属性mysql结果集Map
     * @param columnPrifix column前缀
     * @return map(count-泵运行数量,SBZT-水泵状态)
     */
    public Map<String,Object> convertWoGiaData(Map<String,Object> map,String columnPrifix){
        Map<String,Object> waterPumpGroupAttrMap = new HashMap<String,Object>();
        //泵 出水压力
        Object pressureFeedback = map.get(columnPrifix+ WoGiaFiledConstant.OUT_PRESSURE);
        waterPumpGroupAttrMap.put("pressureFeedback",pressureFeedback==null?0.0:pressureFeedback );
        //泵 进水压力
        Object SZGWYL = map.get(columnPrifix+ WoGiaFiledConstant.INLET_PRESSURE);
        waterPumpGroupAttrMap.put("szgwyl",SZGWYL==null?0.0:SZGWYL );
        waterPumpGroupAttrMap.put("SZGWYL",waterPumpGroupAttrMap.get("szgwyl"));//水泵下限频率
        //设定压力
        Object settingPressure = map.get(columnPrifix+ WoGiaFiledConstant.SET_PRESSURE);
        waterPumpGroupAttrMap.put("settingPressure",settingPressure==null?0.0:settingPressure );
                /*=============统计变频器数量 =====================*/
        //沃佳================0-停机 1工频 2-变频 3故障 4检修 5-现场手动
        //本地================0-变频运行,1-工频运行,2-未运行(停机),3-电磁阀,4-未使用,5-故障,6-检修,7-现场手动
        waterPumpGroupAttrMap.putAll( getWoGiaSBZT(map,columnPrifix) );
        //一号变频器水泵频率
        Object waterFrequency = map.get(columnPrifix+ WoGiaFiledConstant.FREQUENCY_CONVERTER_ONE);
        if (waterFrequency instanceof Float) {//Float 转 int
            Float waterFrequencyStatusDouble = ((Float) waterFrequency).floatValue();
            waterFrequency=waterFrequencyStatusDouble.intValue() +1;
        }
        waterPumpGroupAttrMap.put("waterFrequency",waterFrequency==null?0:waterFrequency);

        waterPumpGroupAttrMap.put("deviceStatus",0);//默认设备在线
        waterPumpGroupAttrMap.put("createTime", map.get("Time"));
        waterPumpGroupAttrMap.put("reportTimeFormat", map.get("Time"));

        waterPumpGroupAttrMap.put("ultraTightlyAlarm",0.7);// 默认设置超压为 0.7
        waterPumpGroupAttrMap.put("XXPL",10);//水泵下限频率
        waterPumpGroupAttrMap.put("xxpl",10);//水泵下限频率
        //判断故障
        waterPumpGroupAttrMap.putAll( getWoGiaIsFault(map,columnPrifix) );
        return waterPumpGroupAttrMap;
    }

    public Map<String,Object> defaultQueryByDeviceNameReturnMap(String deviceName){
        Map<String,Object> waterPumpGroupAttrMap = new HashMap<String,Object>();
        waterPumpGroupAttrMap.put("deviceName",deviceName);//设备组名称
        waterPumpGroupAttrMap.put("pressureFeedback",0.0);//泵 出水压力
        waterPumpGroupAttrMap.put("SZGWYL",0.0);//泵 进水压力
        waterPumpGroupAttrMap.put("szgwyl",0.0);//泵 进水压力
        waterPumpGroupAttrMap.put("settingPressure",0.0);//设定压力
        waterPumpGroupAttrMap.put("waterFrequency",0);//水泵频率
        waterPumpGroupAttrMap.put("XXPL",0);//水泵下限频率
        waterPumpGroupAttrMap.put("xxpl",0.0);//泵 进水压力
        waterPumpGroupAttrMap.put("count",0);//泵 运行数量
        waterPumpGroupAttrMap.put("isFault",0);//是否故障
        waterPumpGroupAttrMap.put("ZXGZ",0);//故障码
        waterPumpGroupAttrMap.put("zxgz",0);//故障码
        waterPumpGroupAttrMap.put("ultraTightlyAlarm",0.7);//超压报警
        waterPumpGroupAttrMap.put("SBZT","2,2,2,2,2,2,2,2,");//水泵状态
        waterPumpGroupAttrMap.put("sbzt","2,2,2,2,2,2,2,2,");//水泵状态
        waterPumpGroupAttrMap.put("deviceStatus",1);//不在线
        return waterPumpGroupAttrMap;
    }

    /**
     * 查询沃佳 最新的一条数据
     * @param deviceName-设备名称
     * @return
     */
    @TargetDataSource(name=DataSourceNameConstant.WOGIA_DATASOURCE)
   public Map<String,Object> selectNewOneAttrByTablenameAndColumn(String deviceName){
       Map<String,Object> returnDataMap = null;
       String[] deviceNameArr = deviceName.split("\\|");
       String tablename = deviceNameArr[0].trim();// 表名称
       int tablenameIsExistCount = selectTableIsExist(tablename);
       if(tablenameIsExistCount<1){// 表不存在
           returnDataMap=defaultQueryByDeviceNameReturnMap(deviceName);
           return returnDataMap;
       }else{
           Map<String,String> sqlParamMap = new HashMap<String,String>();
           sqlParamMap.put("tablename",tablename);
           sqlParamMap.put("whereStr","order by Time desc limit 0,1");//根据上报时间降序,查询最新一条数据
           List<Map<String,Object>> woGiaWpAttrList =selectAttrByTablenameAndColumn(sqlParamMap);
           if(woGiaWpAttrList==null||woGiaWpAttrList.isEmpty()){
               returnDataMap= defaultQueryByDeviceNameReturnMap(deviceName);
               return returnDataMap;
           }
           String columnPrifix =null;
           try {
               columnPrifix = deviceNameArr[2].trim();// 列名前缀
           }catch (ArrayIndexOutOfBoundsException ex){
               throw new RRException("水泵组名称格式错误",400);
           }
           returnDataMap= convertWoGiaData(woGiaWpAttrList.get(0),columnPrifix);
           returnDataMap.put("deviceName",deviceName);//设备组名称
           return returnDataMap;
       }
    }

    /**
     * 查询列名
     * @param map
     * @return
     */
    @TargetDataSource(name=DataSourceNameConstant.WOGIA_DATASOURCE)
    public List<String> selectAllColumnByTableName(Map<String,String> map){
        return woGiaWpAttrMapper.selectAllColumnByTableName(map);
    }

    /**
     * 获取 水泵状态前缀
     * @param columnprefix
     * @return
     */
    public String generate_SBZT_sqlStr(String columnprefix){
        // SBZT 查询水泵状态  CONCAT_WS(',',IFNULL(aw30,-1),IFNULL(aw31,-1))
        StringBuffer SBZT_sql =new StringBuffer(60);
        SBZT_sql.append("CONCAT_WS(','");
        // 1号泵运行状态
        SBZT_sql.append(",").append("IFNULL(").append(columnprefix).append(WoGiaFiledConstant.PUMP_SIGNAL_ONE).append(",-1)");
        // 2号泵运行状态
        SBZT_sql.append(",").append("IFNULL(").append(columnprefix).append(WoGiaFiledConstant.PUMP_SIGNAL_TWO).append(",-1)");
        // 3号泵运行状态
        SBZT_sql.append(",").append("IFNULL(").append(columnprefix).append(WoGiaFiledConstant.PUMP_SIGNAL_THREE).append(",-1)");
        // 4号泵运行状态
        SBZT_sql.append(",").append("IFNULL(").append(columnprefix).append(WoGiaFiledConstant.PUMP_SIGNAL_FOUR).append(",-1)");
        // 5号泵运行状态
        SBZT_sql.append(",").append("IFNULL(").append(columnprefix).append(WoGiaFiledConstant.PUMP_SIGNAL_FIVE).append(",-1)");
        // 6号泵运行状态
        SBZT_sql.append(",").append("IFNULL(").append(columnprefix).append(WoGiaFiledConstant.PUMP_SIGNAL_SIX).append(",-1)");
        SBZT_sql.append(") as SBZT");
        return  SBZT_sql.toString();
    }

    @Override
    @TargetDataSource(name=DataSourceNameConstant.WOGIA_DATASOURCE)
    public Map<String, Object> readRealTimeDeviceData(Map<String, String> map) {
        return woGiaWpAttrMapper.readRealTimeDeviceData(map);
    }

    public Map<String, Object> generatePumpState(Integer pumpNum,Object pumpStateObj) {
        Double pumpState = Double.parseDouble(pumpStateObj.toString());
        Map<String, Object> pumpStateMap = new HashMap<String, Object>();
        pumpStateMap.put("pumpName",pumpNum+"号泵");
//        0-停机 1工频 2-变频 3故障 4检修 5-现场手动
//        0-变频运行,1-工频运行,2-未运行(停机),3-电磁阀,4-未使用,5-故障,6-检修,7-现场手动
        if(pumpState.equals(0.0)){
            pumpStateMap.put("pumpStateName","未运行");
            pumpStateMap.put("pumpState",2);
        }
        if(pumpState.equals(1.0)){
            pumpStateMap.put("pumpStateName","工频运行");
            pumpStateMap.put("pumpState",1);
        }
        if(pumpState.equals(2.0)){
            pumpStateMap.put("pumpStateName","变频运行");
            pumpStateMap.put("pumpState",0);
        }
        if(pumpState.equals(3.0)){
            pumpStateMap.put("pumpStateName","故障");
            pumpStateMap.put("pumpState",5);
        }
        if(pumpState.equals(4.0)){
            pumpStateMap.put("pumpStateName","检修");
            pumpStateMap.put("pumpState",6);
        }
        if(pumpState.equals(5.0)){
            pumpStateMap.put("pumpStateName","手动运行");
            pumpStateMap.put("pumpState",7);
        }
//        pumpStateMap.put("pumpFrequency",0);
        return  pumpStateMap;
    }

    @Override
    @TargetDataSource(name=DataSourceNameConstant.WOGIA_DATASOURCE)
    public List<String> readWogia2TableNameList() {
        return woGiaWpAttrMapper.readWogia2TableNameList();
    }


    /**
     * 查询 每个设备的  最大 最小 平均 压力
     * @param paramList
     * @return
     */
    @TargetDataSource(name=DataSourceNameConstant.WOGIA_DATASOURCE)
    public  List<Map<String,Object>>  readWogia2DataAnalysisCount( List<Map<String,String>> paramList){
        return  woGiaWpAttrMapper.readWogia2DataAnalysisCount(paramList);
    }

    /**
     * 查询表中 数据条数
     * @param tablename
     * @return
     */
    @TargetDataSource(name=DataSourceNameConstant.WOGIA_DATASOURCE)
    public Integer selectCountTableDataNum(String tablename){
        return  woGiaWpAttrMapper.selectCountTableDataNum(tablename);
    }

    /**
     * 查询每个设备的  上报时间序列
     * @param paramList
     * @return
     */
    @TargetDataSource(name=DataSourceNameConstant.WOGIA_DATASOURCE)
   public LinkedHashSet<String> readWogia2DataAnalysisCurveTime(List<Map<String,String>> paramList){
        return  woGiaWpAttrMapper.readWogia2DataAnalysisCurveTime(paramList);
    }
}
