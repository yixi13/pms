package com.pms.service.impl;

import com.baomidou.mybatisplus.mapper.SqlHelper;
import com.baomidou.mybatisplus.mapper.Wrapper;
import com.pms.entity.WaterPumpGroupAttr;
import com.pms.mapper.WaterPumpGroupAttrMapper;
import com.pms.service.IWaterPumpGroupAttrService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.apache.ibatis.session.RowBounds;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author asus
 * @since 2018-01-10
 */
@Service
public class WaterPumpGroupAttrServiceImpl extends ServiceImpl<WaterPumpGroupAttrMapper, WaterPumpGroupAttr> implements IWaterPumpGroupAttrService {
    @Autowired
    protected WaterPumpGroupAttrMapper waterPumpAttrMapper;

    public int insertKafkaData(String sql){
       return waterPumpAttrMapper.insertKafkaData(sql);
    }
    public int insertKafkaDataList(List<String> sqlList){
        return waterPumpAttrMapper.insertKafkaDataList(sqlList);
    }

    @Override
    public List<Map<String, Object>> selectByDeviceNameAndReportTimeFormat(String deviceName, String startTimeStr, String endTimeStr) {
        return waterPumpAttrMapper.selectByDeviceNameAndReportTimeFormat(deviceName,startTimeStr,endTimeStr);
    }
    public WaterPumpGroupAttr selectListByAttr(String deviceName){
        return  waterPumpAttrMapper.selectListByAttr(deviceName);
    }

    /**
     * 查询 水泵组 时间段内的数据  20180401
     * @param deviceName 水泵组名称
     * @param startTime unix起始时间
     * @param endTime   unix截止时间
     * @return
     */
    public List<Map<String,Object>> selectByDeviceNameAndReportTime(String deviceName, Long startTime, Long endTime){
        return waterPumpAttrMapper.selectByDeviceNameAndReportTime(deviceName,startTime,endTime );
    }

    /**
     * 调用存储过程 查询 水泵组 时间段内的数据  20180408
     * @param deviceName 水泵组名称
     * @param startTime unix起始时间
     * @param endTime   unix截止时间
     * @return
     */
   public List<Map<String,Object>> selectAttrRecordByFuction(String deviceName, Long startTime, Long endTime){
       List< List<Map<String,Object>>> reuseltList = waterPumpAttrMapper.selectAttrRecordByFuction(deviceName,startTime,endTime );
       // 引返回数据 结果集 有2个,需要的数据为第二个结果集
       if(reuseltList==null||reuseltList.size()<2){
           return new ArrayList<Map<String,Object>>();
       }
       return reuseltList.get(1);
   }

    /**
     * 查询 固定时间采集的数据 20180408 16:30
     * @param deviceName 水泵组名称
     * @param startTime unix起始时间
     * @param endTime   unix截止时间
     * @return
     */
    public List<Map<String,Object>> readWpGroupRecordByNameAndTime (String deviceName, Long startTime, Long endTime){
       return waterPumpAttrMapper.readWpGroupRecordByNameAndTime(deviceName,startTime,endTime );
    }

    public WaterPumpGroupAttr selectOneRecord(Wrapper<WaterPumpGroupAttr> wrapper) {
        return SqlHelper.getObject( waterPumpAttrMapper.selectPage(new RowBounds(0,1),wrapper) );
    }

}
