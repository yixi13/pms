package com.pms.service.impl;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.pms.cache.annotation.BaseCacheClear;
import com.pms.cache.annotation.CacheClear;
import com.pms.entity.Company;
import com.pms.entity.WaterPumpGroup;
import com.pms.mapper.CompanyMapper;
import com.pms.mapper.WaterPumpGroupMapper;
import com.pms.service.ICompanyService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * <p>
 * 水泵所属公司表 服务实现类
 * </p>
 *
 * @author lk
 * @since 2017-12-25
 */
@Service
public class CompanyServiceImpl extends BaseServiceImpl<CompanyMapper, Company> implements ICompanyService {
    @Autowired
    CompanyMapper companyMapper;
    @Autowired
    WaterPumpGroupMapper waterPumpMapper;

    @Override
    @Transactional
    @BaseCacheClear(pre=":")
    public boolean deleteCompanyAndWaterPumps(Long companyId) {
        companyMapper.deleteById(companyId);
        waterPumpMapper.delete(new EntityWrapper<WaterPumpGroup>().eq("company_id",companyId));
        return true;
    }

}
