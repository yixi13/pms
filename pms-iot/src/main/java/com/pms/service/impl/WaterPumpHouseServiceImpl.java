package com.pms.service.impl;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.pms.entity.WaterPumpGroup;
import com.pms.entity.WaterPumpHouse;
import com.pms.mapper.WaterPumpGroupMapper;
import com.pms.mapper.WaterPumpHouseMapper;
import com.pms.service.IWaterPumpHouseService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author lk
 * @since 2017-12-27
 */
@Service
public class WaterPumpHouseServiceImpl extends BaseServiceImpl<com.pms.mapper.WaterPumpHouseMapper, WaterPumpHouse> implements IWaterPumpHouseService {

    @Autowired
    WaterPumpHouseMapper waterPumpHouseMapper;
    @Autowired
    WaterPumpGroupMapper waterPumpMapper;

    @Override
    @Transactional
    public boolean deleteWaterPumpHouseAndWaterPump(Long houseId) {
        try{
            waterPumpHouseMapper.deleteById(houseId);
            waterPumpMapper.delete(new EntityWrapper<WaterPumpGroup>().eq("house_id",houseId));
            return true;
        }catch (Exception e){
            throw new RuntimeException(e);
        }
    }

    public Integer selectMaxWaterPumpHouseSort(Long communityId){
        Integer maxWaterPumpHouseSort = waterPumpHouseMapper.selectMaxWaterPumpHouseSort(communityId);
        if(maxWaterPumpHouseSort==null||maxWaterPumpHouseSort<1){maxWaterPumpHouseSort = 0;}
        return maxWaterPumpHouseSort;
    }

    /**
     * 查询水泵房 基础信息 asus 20180329
     * @param wpHouseId  水泵房户室id
     * @param wpHouseLbsId  水泵房户室lbsid
     * @param communityId  社区id
     * @return 空参返回null
     */
    public  List<Map<String,Object>> readWaterPumpHouseoutline(Long wpHouseId,String wpHouseLbsId,Long communityId){
        if(wpHouseId==null&&communityId==null&& StringUtils.isBlank(wpHouseLbsId)){
            return null;
        }
        Map<String,Object> paramMap = new HashMap<String,Object>();
        if(wpHouseId!=null){
            paramMap.put("waterPumpHouseId",wpHouseId);
        }
        if(StringUtils.isNotBlank(wpHouseLbsId)){
            paramMap.put("lbsPoiId",wpHouseLbsId);
        }
        if(communityId!=null){
            paramMap.put("communityId",communityId);
        }
        List<Map<String,Object>> result = waterPumpHouseMapper.readWaterPumpHouseOutline(paramMap);
        if(result==null||result.isEmpty()){
            result = null;
        }
        return  result;
    }

    /**
     * 查询水泵房 基础信息 asus 20180329
     * @param communityId  社区id
     * @return
     */
    public  List<Map<String,Object>> readWaterPumpHouseoutline(Long communityId){
        return readWaterPumpHouseoutline(null,null,communityId);
    }

    /**
     * 查询水泵房 基础信息 asus 20180329
     * @param wpHouseId  水泵房户室id
     * @param wpHouseLbsId  水泵房户室lbsid
     * @return
     */
    public Map<String,Object> readWaterPumpHouseoutline(Long wpHouseId, String wpHouseLbsId){
        List<Map<String,Object>> resultMap = readWaterPumpHouseoutline(wpHouseId,wpHouseLbsId,null);
        if(resultMap == null||resultMap.isEmpty()){
            return null;
        }
        return resultMap.get(0);
    }
    public  Integer testSetVAR(){
        return  waterPumpHouseMapper.testSetVAR();
    }

    /**
     * 查询水泵房 基础信息 asus 20180401
     * @param wpHouseId      水泵房户室id
     * @param wpHouseLbsId  水泵房户室lbsid
     * @param communityId   社区id
     * @return
     */
    public List<Map<String,Object>> readWaterPumpHouseOutlineHouseList(Long wpHouseId, String wpHouseLbsId, Long communityId){
        if(wpHouseId==null&&communityId==null&& StringUtils.isBlank(wpHouseLbsId)){
            return null;
        }
        Map<String,Object> paramMap = new HashMap<String,Object>();
        if(wpHouseId!=null){
            paramMap.put("waterPumpHouseId",wpHouseId);
        }
        if(StringUtils.isNotBlank(wpHouseLbsId)){
            paramMap.put("lbsPoiId",wpHouseLbsId);
        }
        if(communityId!=null){
            paramMap.put("communityId",communityId);
        }
        List<Map<String,Object>> result = waterPumpHouseMapper.readWaterPumpHouseOutlineHouseList(paramMap);
        if(result==null||result.isEmpty()){
            result = null;
        }
        return  result;
    }
}
