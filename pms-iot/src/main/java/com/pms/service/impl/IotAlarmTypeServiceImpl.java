package com.pms.service.impl;

import com.pms.entity.IotAlarmType;
import com.pms.mapper.IotAlarmTypeMapper;
import com.pms.service.IIotAlarmTypeService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 报警类型表 服务实现类
 * </p>
 *
 * @author ljb
 * @since 2018-08-20
 */
@Service
public class IotAlarmTypeServiceImpl extends ServiceImpl<IotAlarmTypeMapper, IotAlarmType> implements IIotAlarmTypeService {
	
}
