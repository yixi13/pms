package com.pms.service.impl;

import org.springframework.stereotype.Service;
import com.pms.entity.EquipmentClass;
import com.pms.mapper.EquipmentClassMapper;
import com.pms.service.IEquipmentClassService;
/**
 * 
 *
 * @author lk
 * @email
 * @date 2018-04-25 14:26:06
 */
@Service
public class EquipmentClassServiceImpl extends BaseServiceImpl<EquipmentClassMapper,EquipmentClass> implements IEquipmentClassService {

}