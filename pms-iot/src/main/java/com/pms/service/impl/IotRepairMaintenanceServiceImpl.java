package com.pms.service.impl;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.plugins.Page;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.pms.entity.IotRepairMaintenance;
import com.pms.mapper.IotRepairMaintenanceMapper;
import com.pms.service.IIotRepairMaintenanceService;

import java.util.List;
import java.util.Map;

/**
 * 
 *
 * @author lk
 * @email 307169223@qq.com
 * @date 2018-08-20 09:57:14
 */
@Service
public class IotRepairMaintenanceServiceImpl extends BaseServiceImpl<IotRepairMaintenanceMapper,IotRepairMaintenance> implements IIotRepairMaintenanceService {
    @Autowired
    IotRepairMaintenanceMapper iotRepairMaintenanceMapper;

    public  IotRepairMaintenance seteleByMaintenance(Long repairMaintenanceId){
        return  iotRepairMaintenanceMapper.seteleByMaintenance(repairMaintenanceId);
    }


    public Page<IotRepairMaintenance> selectByPageList(Page<IotRepairMaintenance> page, Map<String,Object> map){
        int count=iotRepairMaintenanceMapper.selectCount(new EntityWrapper<IotRepairMaintenance>().eq("repair_declare_id",map.get("repairDeclareId")));
        List<IotRepairMaintenance> list= iotRepairMaintenanceMapper.selectByPageList(map);
        page.setRecords(list);
        page.setSize(Integer.parseInt(map.get("limit").toString()));
        page.setTotal(count);
        return  page;
    }




    public Page<IotRepairMaintenance> selectByPageMap(Page<IotRepairMaintenance> page, Map<String,Object> map){
        int count=0;
        if (map.get("ids").equals("-1")){
            page.setTotal(count);
        }else {
            count=iotRepairMaintenanceMapper.countByPageMap(map);
            page.setTotal(count);
        }
        List<IotRepairMaintenance> list= iotRepairMaintenanceMapper.selectByPageMap(map);
        page.setRecords(list);
        return  page;
    }
}