package com.pms.service;
import com.baomidou.mybatisplus.service.IService;
import com.pms.entity.GroupIotAgency;
/**
 * 
 *
 * @author ljb
 * @email 517770986@qq.com
 * @date 2018-07-31 10:28:59
 */

public interface IGroupIotAgencyService extends  IBaseService<GroupIotAgency> {

    /**
     *  根据角色id 查询角色 拥有的机构权限
     * @param groupId 角色id
     * @return
     */
    String getAgencyIdsStrByGroupId(String groupId);
}