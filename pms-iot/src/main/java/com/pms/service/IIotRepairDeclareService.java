package com.pms.service;
import com.baomidou.mybatisplus.mapper.Wrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.service.IService;
import com.pms.entity.IotRepairDeclare;

import java.util.List;
import java.util.Map;

/**
 * 
 *
 * @author lk
 * @email 307169223@qq.com
 * @date 2018-08-20 09:57:14
 */

public interface IIotRepairDeclareService extends  IBaseService<IotRepairDeclare> {

    Page<Map<String, Object>> getByIotRepairDeclarePage( Page<Map<String,Object>> page,Map<String,Object> map);
    public Map<String,Object> getByIotRepairDeclare(Long repairMaintenanceId);
    Page<IotRepairDeclare> selectByIotRepairDeclarePage(Page<IotRepairDeclare> page, Wrapper<IotRepairDeclare> wrapper);

    public List<IotRepairDeclare> selectByIotRepairIstrue(Wrapper<IotRepairDeclare> wrapper);
}