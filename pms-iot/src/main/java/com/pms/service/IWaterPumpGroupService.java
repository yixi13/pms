package com.pms.service;

import com.alibaba.fastjson.JSONObject;
import com.pms.entity.WaterPumpGroup;
import com.baomidou.mybatisplus.service.IService;

/**
 * <p>
 * 水泵表 服务类
 * </p>
 *
 * @author lk
 * @since 2017-12-25
 */
public interface IWaterPumpGroupService extends IService<WaterPumpGroup> {
    /**
     * 根据设备名称获取消息格式化规则
     * @param name
     * @return
     */
    public JSONObject getMessageFormatRuleJson(String name);
}
