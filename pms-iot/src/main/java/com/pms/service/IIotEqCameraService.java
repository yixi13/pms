package com.pms.service;
import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.service.IService;
import com.pms.entity.IotEqCamera;
import com.pms.exception.R;
import org.apache.ibatis.annotations.Param;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

/**
 * 
 *
 * @author lk
 * @email 307169223@qq.com
 * @date 2018-08-21 13:52:06
 */

public interface IIotEqCameraService extends  IBaseService<IotEqCamera> {
    /**
     * 根据失效时间获取摄像头，用于定时器更新摄像头sessionKey
     * @param nowTime 当前时间
     * @param pageSize 每页容量
     * @return
     */
    List<IotEqCamera> findEqCameraByExpireTime(String nowTime, int pageSize);

    Map<String,Object> readHouseCamera(Long houseId);

    R deleteByIotEqCameraId(String agencyIds);

    List<IotEqCamera> selectByList(@Param("agencyId") Long agencyId);

    Page<IotEqCamera> selectByEqCameraAndCompanyCodeAll(Page<IotEqCamera> page, Map<String,Object> map);

}