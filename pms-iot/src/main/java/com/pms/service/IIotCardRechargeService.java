package com.pms.service;

import com.baomidou.mybatisplus.plugins.Page;
import com.pms.entity.IotCardRecharge;

import java.util.Map;

/**
 * 
 *
 * @author lk
 * @email 307169223@qq.com
 * @date 2018-08-20 09:57:14
 */

public interface IIotCardRechargeService extends  IBaseService<IotCardRecharge> {
    IotCardRecharge findById(Long cardId);
    Page<IotCardRecharge> selectByListPage(Page<IotCardRecharge> pages, Map<String,Object> map);
}