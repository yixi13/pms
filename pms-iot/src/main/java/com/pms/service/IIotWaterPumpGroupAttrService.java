package com.pms.service;

import com.baomidou.mybatisplus.mapper.Wrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.pms.entity.IotWaterPumpGroupAttr;
import com.baomidou.mybatisplus.service.IService;

import java.util.List;
import java.util.Map;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author asus
 * @since 2018-07-23
 */
public interface IIotWaterPumpGroupAttrService extends IService<IotWaterPumpGroupAttr> {

    Page<Map<String,Object>> readPumpGroupHistory(Page<Map<String,Object>> page,Map<String,Object> sqlMap);
}
