package com.pms.service;

import com.pms.entity.Community;
import com.baomidou.mybatisplus.service.IService;

/**
 * <p>
 * 社区（小区）表 服务类
 * </p>
 *
 * @author lk
 * @since 2017-12-25
 */
public interface ICommunityService extends IService<Community> {
	boolean deleteCommunityAndWaterPumps(Long communityId);

	/**
	 * 查询最大的排序
	 * @return
	 */
	Long selectMaxCommunitySort();
}
