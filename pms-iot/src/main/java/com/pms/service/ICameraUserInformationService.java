package com.pms.service;
import com.baomidou.mybatisplus.service.IService;
import com.pms.entity.CameraUserInformation;

import java.util.List;

/**
 * 
 *
 * @author zyl
 * @email 517770986@qq.com
 * @date 2018-03-15 11:21:02
 */

public interface ICameraUserInformationService extends  IBaseService<CameraUserInformation> {
    public List<CameraUserInformation> seleteBySum();
}