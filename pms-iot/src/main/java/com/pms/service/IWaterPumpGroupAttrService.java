package com.pms.service;

import com.baomidou.mybatisplus.mapper.Wrapper;
import com.pms.entity.WaterPumpGroupAttr;
import com.baomidou.mybatisplus.service.IService;

import java.util.List;
import java.util.Map;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author asus
 * @since 2018-01-10
 */
public interface IWaterPumpGroupAttrService extends IService<WaterPumpGroupAttr> {

    public int insertKafkaData(String sql);
    public int insertKafkaDataList(List<String> sqlList);
    List<Map<String,Object>> selectByDeviceNameAndReportTimeFormat(String deviceName, String startTimeStr, String endTimeStr);
    WaterPumpGroupAttr selectListByAttr(String deviceName);

    /**
     * 查询 水泵组 时间段内的数据  20180401
     * @param deviceName 水泵组名称
     * @param startTime unix起始时间
     * @param endTime   unix截止时间
     * @return
     */
    List<Map<String,Object>> selectByDeviceNameAndReportTime(String deviceName, Long startTime, Long endTime);
    /**
     * 查询 水泵组 时间段内的数据  20180408
     * @param deviceName 水泵组名称
     * @param startTime unix起始时间
     * @param endTime   unix截止时间
     * @return
     */
    List<Map<String,Object>> selectAttrRecordByFuction(String deviceName, Long startTime, Long endTime);

    /**
     * 查询 固定时间采集的数据 20180408 16:30
     * @param deviceName 水泵组名称
     * @param startTime unix起始时间
     * @param endTime   unix截止时间
     * @return
     */
    List<Map<String,Object>> readWpGroupRecordByNameAndTime (String deviceName, Long startTime, Long endTime);

    public WaterPumpGroupAttr selectOneRecord(Wrapper<WaterPumpGroupAttr> wrapper);
}
