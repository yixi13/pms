package com.pms.service;
import com.baomidou.mybatisplus.mapper.Wrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.pms.entity.EqCamera;

import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;

/**
 * 
 *
 * @author asus
 * @date 2018-04-20 11:21:02
 */
public interface IWoGiaWpAttrService {

    /**
     * 根据表名查询数据
     * tablename-表名,columnStr-查询列名,whereStr-weher条件语句需带上where
     * @return
     */
    List<Map<String,Object>> selectAttrByTablenameAndColumn(Map<String,String> map);
    /**
     * 查询表名是否存在
     * @return
     */
    Integer selectTableIsExist(String tablename);

    /**
     * 验证 设备是否故障
     * 	@FiledMark(comment="最新故障码",desc="0-正常,1-水位低,2-变频器故障,3-超压报警,4-传感器故障,5-自检故障,6/9/12/15/18/21-1/2/3/4/5/6号泵故障,7/10/13/16/19/22-1/2/3/4/5/6号泵过流,8/11/14/17/20/23-1/2/3/4/5/6号泵缺相,"
    ,unit="")
     * @param map
     * @param columnPrifix
     * @return isFault-是否故障,ZXGZ-(2-变频器故障，1-稳流罐异常，3-超压报警)
     */
    public Map<String,Object> getWoGiaIsFault(Map<String,Object> map,String columnPrifix);

    /**
     *  沃佳================0-停机 1工频 2-变频 3故障 4检修 5-现场手动
     * 本地================0-变频运行,1-工频运行,2-未运行(停机),3-电磁阀,4-未使用,5-故障,6-检修,7-现场手动
     * 根据mao结果  获取 SBZT 水泵状态
     * @param map 设备属性mysql结果集Map
     * @param columnPrifix column前缀
     * @return map(count-泵运行数量,SBZT-水泵状态)
     */
    public Map<String,Object> getWoGiaSBZT(Map<String,Object> map,String columnPrifix);


    /**
     *  沃佳================0-停机 1工频 2-变频 3故障 4检修 5-现场手动
     * 本地================0-变频运行,1-工频运行,2-未运行(停机),3-电磁阀,4-未使用,5-故障,6-检修,7-现场手动
     * 根据mao结果  获取 SBZT 水泵状态
     * @param map 设备属性mysql结果集Map
     * @param columnPrifix column前缀
     * @return map(count-泵运行数量,SBZT-水泵状态)
     */
    public Map<String,Object> convertWoGiaData(Map<String,Object> map,String columnPrifix);

    public Map<String,Object> defaultQueryByDeviceNameReturnMap(String deviceName);

    /**
     * 查询沃佳 最新的一条数据
     * @param deviceName
     * @return
     */
   Map<String,Object> selectNewOneAttrByTablenameAndColumn(String deviceName);

    /**
     * 查询列名
     * @param map
     * @return
     */
    List<String> selectAllColumnByTableName(Map<String,String> map);

    /**
     * 生成 水泵运行状态 column
     * @param columnprefix 查询列 前缀
     * @return
     */
    public String generate_SBZT_sqlStr(String columnprefix);

    /**
     * 根据表名查询数据
     * @param map 表 查询参数
     * @return
     */
    Map<String,Object> readRealTimeDeviceData(Map<String,String> map);

    public Map<String, Object> generatePumpState(Integer pumpNum,Object pumpState);

    /**
     * 查询所有表名
     * @return
     */
    List<String>  readWogia2TableNameList();

    /**
     * 查询 每个设备的  最大 最小 平均 压力
     * @param paramList
     * @return
     */
    List<Map<String,Object>>  readWogia2DataAnalysisCount( List<Map<String,String>> paramList);

    /**
     * 查询表中 数据条数
     * @param tablename
     * @return
     */
    Integer selectCountTableDataNum(String tablename);

    /**
     * 查询每个设备的  上报时间序列
     * @param paramList
     * @return
     */
    LinkedHashSet<String> readWogia2DataAnalysisCurveTime(List<Map<String,String>> paramList);
}