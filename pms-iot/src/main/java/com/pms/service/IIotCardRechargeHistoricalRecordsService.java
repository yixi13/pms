package com.pms.service;
import com.baomidou.mybatisplus.service.IService;
import com.pms.entity.IotCardRechargeHistoricalRecords;

import java.util.List;

/**
 * 
 *
 * @author lk
 * @email 307169223@qq.com
 * @date 2018-08-20 09:57:14
 */

public interface IIotCardRechargeHistoricalRecordsService extends  IBaseService<IotCardRechargeHistoricalRecords> {
    List<IotCardRechargeHistoricalRecords> selectListByData(Long groupId);
}