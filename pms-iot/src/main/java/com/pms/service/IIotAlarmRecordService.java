package com.pms.service;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.pms.entity.IotAlarmRecord;
import com.baomidou.mybatisplus.service.IService;

import java.util.Map;

/**
 * <p>
 * 设备上传数据 服务类
 * </p>
 *
 * @author ljb
 * @since 2018-08-20
 */
public interface IIotAlarmRecordService extends IService<IotAlarmRecord> {

    Page<Map<String,Object>> selectAlarmRecordRecordMapsPage(Page<Map<String, Object>> page, EntityWrapper<Map<String, Object>> ew);

    void deleteBatch(String alarmIds);

    Map<String,Object> selectApiAlarmRecord(String alarmId);

   public int selectNoReadAlarmRecordCount( EntityWrapper<Map<String, Object>> ew);
}
