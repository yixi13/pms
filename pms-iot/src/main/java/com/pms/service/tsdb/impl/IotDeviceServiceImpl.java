package com.pms.service.tsdb.impl;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.baidubce.services.iothub.model.ListResponse;
import com.pms.service.tsdb.IIotDeviceService;
import com.pms.utils.IotAccessHttpClientUtil;
import com.pms.utils.IotHttpClientUtil;
import com.pms.utils.TsdbUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @author zyl
 * @email 517770986@qq.com
 * @date 2017-11-20 14:28:50
 */
@Service
public class IotDeviceServiceImpl implements IIotDeviceService {

    @Autowired
    TsdbUtil tsdbUtil;
    @Autowired
    IotHttpClientUtil httpReq;// 物管理
    @Autowired
    IotAccessHttpClientUtil iotAccessHttpReq;// 物接入
    @Override
    public JSONObject readRules(Integer address,String myDeviceName) {
        return httpReq.iotDoPost_GET("/v3/iot/rules/device/" + myDeviceName, new JSONObject(),address);
    }

    @Override
    public JSONObject createRules(Integer address,String myDeviceName, JSONObject obj) {
        obj.put("destinations", tsdbUtil.returnRuleDestinationArray(address));//数据上传目的地
        return httpReq.iotDoPost_POST("/v3/iot/rules/device/" + myDeviceName, obj,address);
    }

    @Override
    public JSONObject delRules(Integer address,String myDeviceName) {
        return httpReq.iotDoPost_DELETE("/v3/iot/rules/device/"+myDeviceName,new JSONObject(),address);
    }

    @Override
    public JSONObject editRules(Integer address,String myDeviceName, JSONObject paramJson) {
        JSONArray destinationsArray = new JSONArray();
        destinationsArray.add(tsdbUtil.returnRuleDestination(address));
        paramJson.put("destinations", destinationsArray);//数据上传目的地
        return httpReq.iotDoPost_PUT("/v3/iot/rules/device/"+myDeviceName,paramJson,address);
    }

    public JSONObject disableRules(Integer address,String myDeviceName) {
        return httpReq.iotDoPost_PUT("/v3/iot/rules/device/" + myDeviceName + "?disable", new JSONObject(),address);
    }

    @Override
    public JSONObject enableRules(Integer address,String myDeviceName) {
        return httpReq.iotDoPost_PUT("/v3/iot/rules/device/"+myDeviceName+"?enable",new JSONObject(),address);
    }

    /**
     * 创建设备
     *
     * @param paramJson 设备信息
     * @return
     */
    public JSONObject createMydevice(Integer address,JSONObject paramJson) {
        return httpReq.iotDoPost_POST("/v3/iot/management/device", paramJson,address);
    }

    /**
     * 删除设备
     *
     * @param paramJson
     * @return
     */
    @Override
    public JSONObject delDevice(Integer address,JSONObject paramJson) {
        return httpReq.iotDoPost_PUT("/v3/iot/management/device?remove", paramJson,address);
    }

    /**
     * 查询设备简况(属性)
     *
     * @param deviceName 设备名称
     * @return
     */
    @Override
    public JSONObject readDeviceProfile(Integer address,String deviceName) {
        return httpReq.iotDoPost_GET("/v3/iot/management/device/" + deviceName, new JSONObject(),address);
    }

    /**
     * 查询设备视图
     *
     * @param deviceName 设备名称
     * @return
     */
    @Override
    public JSONObject readDeviceView(Integer address,String deviceName) {
        return httpReq.iotDoPost_GET("/v3/iot/management/deviceView/" + deviceName, new JSONObject(),address);
    }

    /**
     * 查询设备影子列表
     *
     * @param paramJson
     * @return
     */
    @Override
    public JSONObject readDevicePage(Integer address,JSONObject paramJson) {
        return httpReq.iotDoPost_GET("/v3/iot/management/device", paramJson,address);
    }

    /**
     * 查询设备接入详情
     *
     * @param deviceName 设备名称
     * @return
     */
    @Override
    public JSONObject readDeviceAccessDetail(Integer address,String deviceName) {
        return httpReq.iotDoPost_GET("/v3/iot/management/device/" + deviceName + "/accessDetail", null,address);
    }

    /**
     * 更新设备密钥
     *
     * @param deviceName 设备名称
     * @return
     */
    @Override
    public JSONObject updateDeviceSecretKey(Integer address,String deviceName) {
        return httpReq.iotDoPost_PUT("/v3/iot/management/device/" + deviceName + "?updateSecretKey", new JSONObject(),address);
    }

    /**
     * 更新设备属性
     *
     * @param deviceName 设备名称
     * @param paramJson
     * @return
     */
    @Override
    public JSONObject updateDeviceProfile(Integer address,String deviceName, JSONObject paramJson) {
        return httpReq.iotDoPost_PUT("/v3/iot/management/device/" + deviceName + "?updateProfile", paramJson,address);
    }

    /**
     * 更新单个设备视图
     *
     * @param deviceName 设备名称
     * @param paramJson  设
     * @return
     */
    @Override
    public JSONObject updateDeviceSingleView(Integer address,String deviceName, JSONObject paramJson) {
        return httpReq.iotDoPost_PUT("/v3/iot/management/deviceView/" + deviceName + "?updateView", paramJson,address);
    }

    /**
     * 更新单个设备注册信息
     *
     * @param deviceName 设备名称
     * @param paramJson
     * @return
     */
    @Override
    public JSONObject updateDeviceSingleRegistry(Integer address,String deviceName, JSONObject paramJson) {
        return httpReq.iotDoPost_PUT("/v3/iot/management/device/" + deviceName + "?updateRegistry", paramJson,address);
    }

    /**
     * 重置（清空）设备影子
     *
     * @param paramJson
     * @return
     */
    @Override
    public JSONObject resetDevice(Integer address,JSONObject paramJson) {
        return httpReq.iotDoPost_PUT("/v3/iot/management/device?reset", paramJson,address);
    }


    /**
     * 查询设备模板
     *
     * @param paramJson
     * @return
     */
    public JSONObject readSchema(Integer address,JSONObject paramJson) {
        return httpReq.iotDoPost_GET("/v3/iot/management/schema", paramJson,address);
    }

    /**
     * 查询设备模板详情
     *
     * @param schemaId
     * @return
     */
    @Override
    public JSONObject readSchemaDetail(Integer address,String schemaId) {
        return httpReq.iotDoPost_GET("/v3/iot/management/schema/" + schemaId, new JSONObject(),address);
    }

    /**
     * 创建设备模板
     *
     * @param paramJson
     * @return
     */
    @Override
    public JSONObject createSchema(Integer address,JSONObject paramJson) {
        return httpReq.iotDoPost_POST("/v3/iot/management/schema", paramJson,address);
    }

    /**
     * 删除设备模板
     *
     * @param schemaId 设备模板id
     * @return
     */
    @Override
    public JSONObject delSchema(Integer address,String schemaId) {
        return httpReq.iotDoPost_DELETE("/v3/iot/management/schema/" + schemaId, new JSONObject(),address);
    }

    /**
     * 修改设备模板
     *
     * @param schemaId
     * @param paramJson
     * @return
     */
    @Override
    public JSONObject editSchema(Integer address,String schemaId, JSONObject paramJson) {
        return httpReq.iotDoPost_PUT("/v3/iot/management/schema/" + schemaId, paramJson,address);
    }
    /**
     * 获取实列 列表
     * @return
     */
    public JSONObject getEndpointList(Integer address, JSONObject paramJson){
        return iotAccessHttpReq.iotDoPost_GET("/v1/endpoint", paramJson ,address);
    }
    /**
     * 获取指定实列信息
     * @return
     */
    public JSONObject getEndpointDetail(Integer address, String endpointName){
        return iotAccessHttpReq.iotDoPost_GET("/v1/endpoint/"+endpointName, new JSONObject() ,address);
    }
    /**
     * 创建实列
     * @return
     */
    public JSONObject createEndpoint(Integer address, String endpointName){
        JSONObject paramJson= new JSONObject();
        paramJson.put("endpointName",endpointName);
        return iotAccessHttpReq.iotDoPost_POST("/v1/endpoint",paramJson ,address);
    }
    /**
     * 删除实列
     * @return
     */
    public JSONObject delEndpoint(Integer address, String endpointName){
        return iotAccessHttpReq.iotDoPost_DELETE("/v1/endpoint/"+endpointName,new JSONObject() ,address);
    }
    /**
     * 获取thing设备列表
     * @return
     */
    public JSONObject getThingDeviceList(Integer address, String endpointName, JSONObject paramJson){
        return iotAccessHttpReq.iotDoPost_GET("/v1/endpoint/"+endpointName+"/thing", paramJson ,address);
    }

    /**
     * 获取指定thing设备信息
     * @return
     */
    public JSONObject getThingDeviceDetail(Integer address,String endpointName,String thingName){
        return iotAccessHttpReq.iotDoPost_GET("/v1/endpoint/"+endpointName+"/thing/"+thingName, new JSONObject() ,address);
    }

    /**
     * 创建thing设备
     * @return
     */
    public JSONObject createThingDevice(Integer address, String endpointName,String thingName){
        JSONObject paramJson= new JSONObject();
        paramJson.put("thingName",thingName);
        return iotAccessHttpReq.iotDoPost_POST("/v1/endpoint/"+endpointName+"/thing", paramJson ,address);
    }
    /**
     * 删除thing设备
     * @return
     */
    public JSONObject delThingDevice(Integer address, String endpointName,String thingName){
        return iotAccessHttpReq.iotDoPost_DELETE("/v1/endpoint/"+endpointName+"/thing/"+thingName,new JSONObject() ,address);
    }

    /**
     * 获取身份列表
     * @return
     */
    public JSONObject getPrincipalList(Integer address, String endpointName, JSONObject paramJson){
        return iotAccessHttpReq.iotDoPost_GET("/v1/endpoint/"+endpointName+"/principal", paramJson ,address);
    }

    /**
     * 获取指定身份信息
     * @return
     */
    public JSONObject getPrincipalDetail(Integer address,String endpointName,String principalName){
        return iotAccessHttpReq.iotDoPost_GET("/v1/endpoint/"+endpointName+"/principal/"+principalName, new JSONObject() ,address);
    }

    /**
     * 创建身份
     * @return
     */
    public JSONObject createPrincipal(Integer address, String endpointName,String principalName){
        JSONObject paramJson= new JSONObject();
        paramJson.put("principalName",principalName);
        return iotAccessHttpReq.iotDoPost_POST("/v1/endpoint/"+endpointName+"/principal", paramJson ,address);
    }
    /**
     * 删除身份
     * @return
     */
    public JSONObject delPrincipal(Integer address, String endpointName,String principalName){
        return iotAccessHttpReq.iotDoPost_DELETE("/v1/endpoint/"+endpointName+"/principal/"+principalName,new JSONObject() ,address);
    }
    /**
     * 重新生成身份密钥
     * @return
     */
    public JSONObject editPrincipal(Integer address, String endpointName,String principalName){
        return iotAccessHttpReq.iotDoPost_POST("/v1/endpoint/"+endpointName+"/principal/"+principalName,new JSONObject(),address);
    }

    /**
     * 查询策略列表
     * @return
     */
    public JSONObject readPolicyList(Integer address, String endpointName, JSONObject paramJson){
        return iotAccessHttpReq.iotDoPost_GET("/v1/endpoint/"+endpointName+"/policy", paramJson ,address);
    }
    /**
     * 获取指定策略信息
     * @return
     */
    public JSONObject readPolicyDetail(Integer address,String endpointName,String policyName){
        return iotAccessHttpReq.iotDoPost_GET("/v1/endpoint/"+endpointName+"/policy/"+policyName, new JSONObject() ,address);
    }
    /**
     * 创建策略
     * @return
     */
    public JSONObject createPolicy(Integer address, String endpointName,String policyName){
        JSONObject paramJson= new JSONObject();
        paramJson.put("policyName",policyName);
        return iotAccessHttpReq.iotDoPost_POST("/v1/endpoint/"+endpointName+"/policy", paramJson ,address);

    }
    /**
     * 删除策略
     * @return
     */
    public JSONObject delPolicy(Integer address, String endpointName,String policyName){
        return iotAccessHttpReq.iotDoPost_DELETE("/v1/endpoint/"+endpointName+"/policy/"+policyName,new JSONObject() ,address);
    }
    /**
     * 身份绑定策略
     * @param address
     * @param endpointName 实列名称
     * @param policyName 策略名称
     * @param principalName 身份名称
     * @return
     */
    public JSONObject principalBindPolicy(Integer address, String endpointName,String principalName,String policyName){
        JSONObject paramJson = new JSONObject();
        paramJson.put("endpointName",endpointName);
        paramJson.put("principalName",principalName);
        paramJson.put("policyName",policyName);
        return iotAccessHttpReq.iotDoPost_POST("/v1/action/attach-principal-policy",paramJson,address);
    }
    /**
     * 身份解绑策略
     * @param address
     * @param endpointName 实列名称
     * @param policyName 策略名称
     * @param principalName 身份名称
     * @return
     */
    public JSONObject principalRemovePolicy(Integer address, String endpointName,String principalName,String policyName){
        JSONObject paramJson = new JSONObject();
        paramJson.put("endpointName",endpointName);
        paramJson.put("principalName",principalName);
        paramJson.put("policyName",policyName);
        return iotAccessHttpReq.iotDoPost_POST("/v1/action/remove-principal-policy",paramJson,address);
    }

    /**
     * 设备绑定身份
     * @param address
     * @param endpointName 实列名称
     * @param thingName 设备名称
     * @param principalName 身份名称
     * @return
     */
    public JSONObject thingBindPrincipal(Integer address, String endpointName,String principalName,String thingName){
        JSONObject paramJson = new JSONObject();
        paramJson.put("endpointName",endpointName);
        paramJson.put("principalName",principalName);
        paramJson.put("thingName",thingName);
        return iotAccessHttpReq.iotDoPost_POST("/v1/action/attach-thing-principal",paramJson,address);
    }
    /**
     * 设备解绑身份
     * @param address
     * @param endpointName 实列名称
     * @param thingName 设备名称
     * @param principalName 身份名称
     * @return
     */
    public JSONObject thingRemovePrincipal(Integer address, String endpointName,String principalName,String thingName){
        JSONObject paramJson = new JSONObject();
        paramJson.put("endpointName",endpointName);
        paramJson.put("principalName",principalName);
        paramJson.put("thingName",thingName);
        return iotAccessHttpReq.iotDoPost_POST("/v1/action/remove-thing-principal",paramJson,address);
    }

    /**
     * 查询策略下的主题列表
     * @return
     */
    public JSONObject readTopicList(Integer address, String endpointName, JSONObject paramJson){
        return iotAccessHttpReq.iotDoPost_GET("/v1/endpoint/"+endpointName+"/permission", paramJson ,address);
    }
    /**
     * 获取指定主题信息
     * @return
     */
    public JSONObject readTopicDetail(Integer address,String endpointName,String permissionUuid){
        return iotAccessHttpReq.iotDoPost_GET("/v1/endpoint/"+endpointName+"/permission/"+permissionUuid, new JSONObject() ,address);
    }
    /**
     * 策略设置主题
     * @return
     */
    public JSONObject addTopicToPolicy(Integer address, String endpointName,JSONObject paramJson){
        return iotAccessHttpReq.iotDoPost_POST("/v1/endpoint/"+endpointName+"/permission",paramJson,address);
    }
    /**
     * 更新已有的topic设置
     * @return
     */
    public JSONObject updateTopic(Integer address, String endpointName,String permissionUuid,JSONObject paramJson){
        return iotAccessHttpReq.iotDoPost_PUT("/v1/endpoint/"+endpointName+"/permission/"+permissionUuid,paramJson,address);
    }
    /**
     * 删除topic设置
     * @return
     */
    public JSONObject delTopic(Integer address, String endpointName,String permissionUuid){
        return iotAccessHttpReq.iotDoPost_DELETE("/v1/endpoint/"+endpointName+"/permission/"+permissionUuid,new JSONObject(),address);
    }

    /**
     *物接入 创建接入实列====》创建设备===》设置身份====》设置策略(策略绑定主题)===>创建数据储存
     * @param address 1-广州,2-北京(默认)
     * @param waterPumpName 水泵设备名称
     */
    public void createWaterPumpConfig(Integer address,String waterPumpName) {
        // 获取实列名称
        String endpointName = iotAccessHttpReq.readDefaultEndpointName(address);
        // 根据实列名称创建thing设备
        JSONObject createThingJson = createThingDevice(address, endpointName, waterPumpName);
        // 获取身份名称
        String principalName = iotAccessHttpReq.readDefaultPrincipalName(address);
        //设备绑定身份
        JSONObject thingBindPrincipalJson = thingBindPrincipal(address,endpointName,principalName, waterPumpName);
        //创建设备存储
        //创建规则引擎订阅所有数据
    }

}


