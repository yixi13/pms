package com.pms.service.tsdb;

import com.alibaba.fastjson.JSONObject;

public interface IIotDeviceService {
    /*===================规则管理==================*/

    /**
     * 查询规则
     *
     * @param myDeviceName 设备名称
     * @return
     */
    public JSONObject readRules(Integer address,String myDeviceName);

    /**
     * 创建规则
     *
     * @param myDeviceName 设备名称
     * @param paramJson
     * @return
     */
    public JSONObject createRules(Integer address,String myDeviceName, JSONObject paramJson);
    /**
     * 删除规则
     *
     * @param myDeviceName
     * @return
     */
    public JSONObject delRules(Integer address,String myDeviceName);

    /**
     * 修改规则
     *
     * @param myDeviceName
     * @return
     */
    public JSONObject editRules(Integer address,String myDeviceName,JSONObject paramJson);
    /**
     * 禁用规则
     *
     * @param myDeviceName
     * @return
     */
    public JSONObject disableRules(Integer address,String myDeviceName);

    /**
     * 启用规则
     *
     * @param myDeviceName
     * @return
     */

    public JSONObject enableRules(Integer address,String myDeviceName);

  /*===================设备管理==================*/

    /**
     * 创建设备
     *
     * @param paramJson 设备信息
     * @return
     */
    public JSONObject createMydevice(Integer address,JSONObject paramJson);

    /**
     * 删除设备
     *
     * @param paramJson
     * @return
     */
    public JSONObject delDevice(Integer address,JSONObject paramJson);

    /**
     * 查询设备简况(属性)
     *
     * @param deviceName 设备名称
     * @return
     */
    public JSONObject readDeviceProfile(Integer address,String deviceName);

    /**
     * 查询设备视图
     *
     * @param deviceName 设备名称
     * @return
     */
    public JSONObject readDeviceView(Integer address,String deviceName);

    /**
     * 查询设备影子列表
     *
     * @param paramJson
     * @return
     */
    public JSONObject readDevicePage(Integer address,JSONObject paramJson);

    /**
     * 查询设备接入详情
     *
     * @param deviceName 设备名称
     * @return
     */
    public JSONObject readDeviceAccessDetail(Integer address,String deviceName);

    /**
     * 更新设备密钥
     *
     * @param deviceName 设备名称
     * @return
     */
    public JSONObject updateDeviceSecretKey(Integer address,String deviceName);

    /**
     * 更新设备属性
     *
     * @param deviceName 设备名称
     * @param paramJson
     * @return
     */
    public JSONObject updateDeviceProfile(Integer address,String deviceName, JSONObject paramJson);

    /**
     * 更新单个设备视图
     *
     * @param deviceName 设备名称
     * @param paramJson  设
     * @return
     */
    public JSONObject updateDeviceSingleView(Integer address,String deviceName, JSONObject paramJson);

    /**
     * 更新单个设备注册信息
     *
     * @param deviceName 设备名称
     * @param paramJson
     * @return
     */
    public JSONObject updateDeviceSingleRegistry(Integer address,String deviceName, JSONObject paramJson);

    /**
     * 重置（清空）设备影子
     *
     * @param paramJson
     * @return
     */
    public JSONObject resetDevice(Integer address,JSONObject paramJson);
      /*===================设备模板管理==================*/

    /**
     * 查询设备模板列表
     *
     * @param paramJson
     * @return
     */
    public JSONObject readSchema(Integer address,JSONObject paramJson);

    /**
     * 查询设备模板详情
     *
     * @param schemaId 模板id
     * @return
     */
    public JSONObject readSchemaDetail(Integer address,String schemaId);

    /**
     * 创建设备模板
     *
     * @param paramJson
     * @return
     */
    public JSONObject createSchema(Integer address,JSONObject paramJson);

    /**
     * 删除设备模板
     *
     * @param schemaId
     * @return
     */
    public JSONObject delSchema(Integer address,String schemaId);

    /**
     * 修改设备模板
     *
     * @param schemaId
     * @param paramJson
     * @return
     */
    public JSONObject editSchema(Integer address,String schemaId, JSONObject paramJson);



    /*=====================================物接入 start===============================*/

    /**
     * 获取实列 列表
     * @return
     */
    public JSONObject getEndpointList(Integer address, JSONObject paramJson);
    /**
     * 获取指定实列信息
     * @return
     */
    public JSONObject getEndpointDetail(Integer address, String endpointName);

    /**
     * 创建实列
     * @return
     */
    public JSONObject createEndpoint(Integer address, String endpointName);
    /**
     * 删除实列
     * @return
     */
    public JSONObject delEndpoint(Integer address, String endpointName);

    /**
     * 获取thing设备列表
     * @return
     */
    public JSONObject getThingDeviceList(Integer address, String endpointName, JSONObject paramJson);

    /**
     * 获取指定thing设备信息
     * @return
     */
    public JSONObject getThingDeviceDetail(Integer address,String endpointName,String thingName);

    /**
     * 创建thing设备
     * @return
     */
    public JSONObject createThingDevice(Integer address, String endpointName,String thingName);
    /**
     * 删除thing设备
     * @return
     */
    public JSONObject delThingDevice(Integer address, String endpointName,String thingName);

    /**
     * 获取身份列表
     * @return
     */
    public JSONObject getPrincipalList(Integer address, String endpointName, JSONObject paramJson);

    /**
     * 获取指定身份信息
     * @return
     */
    public JSONObject getPrincipalDetail(Integer address,String endpointName,String principalName);

    /**
     * 创建身份
     * @return
     */
    public JSONObject createPrincipal(Integer address, String endpointName,String principalName);
    /**
     * 删除身份
     * @return
     */
    public JSONObject delPrincipal(Integer address, String endpointName,String principalName);
    /**
     * 重新生成身份密钥
     * @return
     */
    public JSONObject editPrincipal(Integer address, String endpointName,String principalName);

    /**
     * 查询策略列表
     * @return
     */
    public JSONObject readPolicyList(Integer address, String endpointName, JSONObject paramJson);
    /**
     * 获取指定策略信息
     * @return
     */
    public JSONObject readPolicyDetail(Integer address,String endpointName,String policyName);
    /**
     * 创建策略
     * @return
     */
    public JSONObject createPolicy(Integer address, String endpointName,String policyName);
    /**
     * 删除策略
     * @return
     */
    public JSONObject delPolicy(Integer address, String endpointName,String policyName);

    /**
     * 身份绑定策略
     * @param address
     * @param endpointName 实列名称
     * @param policyName 策略名称
     * @param principalName 身份名称
     * @return
     */
    public JSONObject principalBindPolicy(Integer address, String endpointName,String principalName,String policyName);
    /**
     * 身份解绑策略
     * @param address
     * @param endpointName 实列名称
     * @param policyName 策略名称
     * @param principalName 身份名称
     * @return
     */
    public JSONObject principalRemovePolicy(Integer address, String endpointName,String principalName,String policyName);

    /**
     * 设备绑定身份
     * @param address
     * @param endpointName 实列名称
     * @param thingName 设备名称
     * @param principalName 身份名称
     * @return
     */
    public JSONObject thingBindPrincipal(Integer address, String endpointName,String principalName,String thingName);
    /**
     * 设备解绑身份
     * @param address
     * @param endpointName 实列名称
     * @param thingName 设备名称
     * @param principalName 身份名称
     * @return
     */
    public JSONObject thingRemovePrincipal(Integer address, String endpointName,String principalName,String thingName);


    /**
     * 查询策略下的主题列表
     * @return
     */
    public JSONObject readTopicList(Integer address, String endpointName, JSONObject paramJson);
    /**
     * 获取指定主题信息
     * @return
     */
    public JSONObject readTopicDetail(Integer address,String endpointName,String permissionUuid);
    /**
     * 策略设置主题
     * @return
     */
    public JSONObject addTopicToPolicy(Integer address, String endpointName,JSONObject paramJson);
    /**
     * 更新已有的topic设置
     * @return
     */
    public JSONObject updateTopic(Integer address, String endpointName,String permissionUuid,JSONObject paramJson);
    /**
     * 删除topic设置
     * @return
     */
    public JSONObject delTopic(Integer address, String endpointName,String permissionUuid);
    /*=====================================物接入 stop===============================*/

    /**
     * @param address 1-广州,2-北京(默认)
     * @param waterPumpName 水泵设备名称
     */
    public void createWaterPumpConfig(Integer address,String waterPumpName);
}
