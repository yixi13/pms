package com.pms.service.tsdb.impl;

import com.baidubce.services.tsdb.model.*;
import com.pms.service.tsdb.ITsdbService;
import com.pms.utils.TsdbUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;

/**
 * @author zyl
 * @email 517770986@qq.com
 * @date 2017-11-20 14:28:50
 */
@Service
public class TsdbServiceImpl implements ITsdbService {

    @Autowired
    TsdbUtil tsdbUtil;

    /**
     * 查询 度量响应头
     * @param HttpType 请求方式，2-HTTPS请求，默认HTTP请求
     * @return
     */
    @Override
    public GetMetricsResponse readMetricResponse(Integer address,Integer HttpType) {
        if (HttpType == null || HttpType != 2) {
            return tsdbUtil.getHttpTsdbClient(address).getMetrics();
        }
        return tsdbUtil.getHttpTsdbClient(address).getMetrics();
    }

    /**
     * 查询 度量集合
     * @param HttpType 请求方式，2-HTTPS请求，默认HTTP请求
     * @return
     */
    @Override
    public List<String> readMetricList(Integer address,Integer HttpType) {
        return readMetricResponse(address,HttpType).getMetrics();
    }

    /**
     * 根据 度量 查询 域响应头
     * @param HttpType 请求方式，2-HTTPS请求，默认HTTP请求
     * @param metric 度量
     * @return GetFieldsResponse 域响应头
     */
    @Override
    public GetFieldsResponse readFieldResponse(Integer address,Integer HttpType, String metric) {
        if (HttpType == null || HttpType != 2) {
            return tsdbUtil.getHttpTsdbClient(address).getFields(metric);
        }
        return tsdbUtil.getHttpsTsdbClient(address).getFields(metric);
    }

    /**
     * 根据 度量 查询 域Map集合
     * @param HttpType 请求方式，2-HTTPS请求，默认HTTP请求
     * @param metric 度量
     * @return Map<String,Object> 域Map集合(key值为域名称，value为域值的数据类型)
     */
    @Override
    public List<Map<String, Object>> readFieldMap(Integer address,Integer HttpType, String metric) {
        List<Map<String, Object>> fieldList = new ArrayList<Map<String, Object>>();
        GetFieldsResponse fieldResponse = readFieldResponse(address,HttpType, metric);
        for (Map.Entry<String, GetFieldsResponse.FieldInfo> entry : fieldResponse.getFields().entrySet()) {
            Map<String, Object> fieldMap = new HashMap<String, Object>();
            fieldMap.put("fieldName", entry.getKey());
            fieldMap.put("fieldValueType", entry.getValue().getType());
            fieldList.add(fieldMap);
        }
        return fieldList;
    }

    /**
     *
     * @param HttpType 请求方式，2-HTTPS请求，默认HTTP请求
     * @param metric 度量
     * @return
     */
    @Override
    public GetTagsResponse readTagResponse(Integer address,Integer HttpType, String metric) {
        if (HttpType == null || HttpType != 2) {
            return tsdbUtil.getHttpTsdbClient(address).getTags(metric);
        }
        return tsdbUtil.getHttpsTsdbClient(address).getTags(metric);
    }

    @Override
    public List<Map<String, Object>> readTagMap(Integer address,Integer HttpType, String metric) {
        List<Map<String, Object>> tagList = new ArrayList<Map<String, Object>>();
        Map<String,List<String>> tagMap= readTagResponse(address,HttpType,metric).getTags();
        for (Map.Entry<String, List<String>> entry : tagMap.entrySet() ) {
            Map<String, Object> map = new HashMap<String, Object>();
            map.put("tagKey",entry.getKey());
            map.put("tagValue",entry.getValue());
            tagList.add(map);
        }
        return tagList;
    }

    @Override
    public QueryDatapointsResponse readDataPointResponse(Integer address,Integer HttpType, List<Query> queries){
        if (HttpType == null || HttpType != 2) {
            return tsdbUtil.getHttpTsdbClient(address).queryDatapoints(queries);
        }
        return tsdbUtil.getHttpsTsdbClient(address).queryDatapoints(queries);
    }

    @Override
    public List<Result> readDataPointResult(Integer address,Integer HttpType, List<Query> queries) {
        return readDataPointResponse(address,HttpType,queries).getResults();
    }

    @Override
    public QueryDatapointsResponse readDataPointResponse(Integer address,List<Query> queries) {
        return readDataPointResponse(address,null,queries);
    }

    @Override
    public List<Result> readDataPointResult(Integer address,List<Query> queries) {
        return readDataPointResult(address,null,queries);
    }

    @Override
    public Object writeDatapoints(Integer address,List<Datapoint> datapoints) {
        return tsdbUtil.getHttpTsdbClient(address).writeDatapoints(datapoints);
    }
}