package com.pms.service.tsdb;

import com.baidubce.services.tsdb.model.*;

import java.util.List;
import java.util.Map;

public interface ITsdbService {
    /**
     * 查询 度量(Metric)响应头
     * @param HttpType 请求方式，2-HTTPS请求，默认HTTP请求
     * @return GetMetricsResponse 度量响应头
     */
    public GetMetricsResponse readMetricResponse(Integer address,Integer HttpType);
    /**
     * 查询 度量(Metric)集合
     * @param HttpType 请求方式，2-HTTPS请求，默认HTTP请求
     * @return List<String> 度量集合
     */
    public List<String> readMetricList(Integer address,Integer HttpType);

    /**
     * 根据 度量 查询 域响应头
     * @param HttpType 请求方式，2-HTTPS请求，默认HTTP请求
     * @param metric 度量
     * @return GetFieldsResponse 域响应头
     */
    public GetFieldsResponse readFieldResponse(Integer address,Integer HttpType,String metric);

    /**
     * 根据 度量 查询 域Map集合
     * @param HttpType 请求方式，2-HTTPS请求，默认HTTP请求
     * @param metric 度量
     * @return Map<String,Object> 域集合(fieldName,fieldValueType)
     */
    public List<Map<String,Object>> readFieldMap(Integer address,Integer HttpType,String metric);

    /**
     * 根据 度量 查询 标签响应头
     * @param HttpType 请求方式，2-HTTPS请求，默认HTTP请求
     * @param metric 度量
     * @return GetFieldsResponse 域响应头
     */
    public GetTagsResponse readTagResponse(Integer address,Integer HttpType,String metric);

    /**
     * 根据 度量 查询 标签集合
     * @param HttpType 请求方式，2-HTTPS请求，默认HTTP请求
     * @param metric 度量
     * @return Map<String,List<String>> 域Map集合(tagKey,tagValue)
     */
    public List<Map<String, Object>> readTagMap(Integer address,Integer HttpType,String metric);

    /**
     * 查询数据点响应头
     * @param HttpType 请求方式，2-HTTPS请求，默认HTTP请求
     * @param queries 查询参数对象集合
     */
    public QueryDatapointsResponse readDataPointResponse(Integer address,Integer HttpType, List<Query> queries);
    /**
     * 查询数据点
     * @param HttpType 请求方式，2-HTTPS请求，默认HTTP请求
     * @param queries 查询参数对象集合
     */
    public List<Result> readDataPointResult(Integer address,Integer HttpType, List<Query> queries);

    /**
     * 查询数据点响应头
     * @param queries 查询参数对象集合
     */
    public QueryDatapointsResponse readDataPointResponse(Integer address,List<Query> queries);
    /**
     * 查询数据点
     * @param queries 查询参数对象集合
     */
    public List<Result> readDataPointResult( Integer address,List<Query> queries);

    /**
     * 写入数据点
     * @param datapoints 数据点集合
     */
    public Object writeDatapoints(Integer address,List<Datapoint> datapoints);

}
