package com.pms.service;
import com.baomidou.mybatisplus.mapper.Wrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.service.IService;
import com.pms.entity.EqCamera;

import java.util.List;
import java.util.Map;

/**
 * 
 *
 * @author zyl
 * @email 517770986@qq.com
 * @date 2018-03-15 11:21:02
 */

public interface IEqCameraService extends  IBaseService<EqCamera> {
    /**
     * 根据失效时间获取摄像头，用于定时器更新摄像头sessionKey
     * @param nowTime 当前时间
     * @param pageSize 每页容量
     * @return
     */
    List<EqCamera> findEqCameraByExpireTime(String nowTime, int pageSize);

    Page<Map<String, Object>> selectByISMapsPage(Page page, Wrapper<EqCamera> wrapper);

}