package com.pms.service;

import com.pms.entity.IotAlarmType;
import com.baomidou.mybatisplus.service.IService;

/**
 * <p>
 * 报警类型表 服务类
 * </p>
 *
 * @author ljb
 * @since 2018-08-20
 */
public interface IIotAlarmTypeService extends IService<IotAlarmType> {
	
}
