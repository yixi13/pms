package com.pms.service;

import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.mapper.Wrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.pms.entity.IotCardRecharge;
import com.pms.entity.IotWaterPumpGroup;
import com.baomidou.mybatisplus.service.IService;

import java.util.List;
import java.util.Map;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author asus
 * @since 2018-07-18
 */
public interface IIotWaterPumpGroupService extends IService<IotWaterPumpGroup> {

    /**
     * 查询 根据 水泵房id字符串 查询 水泵组id字符串
     * @param orgIds 水泵房id字符串
     * @return 水泵组id字符串
     */
    String getGroupIdsStrByHouseId( String orgIds);

    /**
     * 查询数据用于 机构数展示
     * @param wrapper
     * @return
     */
    List<Map<String,Object>> selectMapList(Wrapper<IotWaterPumpGroup> wrapper);

    /**
     * 查询 水泵组 实时(最新的一条)数据
     * @param page
     * @param wrapper
     * @return
     */
    Page<Map<String,Object>> readRealTimeDataPage(Page<Map<String,Object>> page , Wrapper<Map<String,Object>> wrapper);

    /**
     * 查询水泵房中的水泵组
     * @param paramMap（houseId-水泵房id，communityId-小区id）
     * @return
     */
    public List<Map<String,Object>> readHousePumpGroupDetail( Map<String,Object> paramMap);

    /**
     * 查询 根据 水泵房id字符串 查询 水泵组id集合
     * @param orgIds 水泵房id字符串
     * @return 水泵组id字符串
     */
    List<Long> getGroupIdListByHouseId( String orgIds);

    /**
     * 查询社区下的小泵数量统计
     * @param paramMap
     * @return Map(communityId-社区id,totalPumpNum-总小泵数量,onlinePumpNum-在线泵数量,nolinePumpNum-离线泵数量)
     */
    Map<String,Map<String,Object>> getPumpNumByCommunityId(Map<String,Object> paramMap);
    /**
     * 查询水泵房下的小泵数量
     * @param paramMap
     * @return Map(houseId-水泵房id,totalPumpNum-总小泵数量,onlinePumpNum-在线泵数量,nolinePumpNum-离线泵数量)
     */
    Map<String,Map<String,Object>> getPumpNumByHouseId(Map<String,Object> paramMap);

    /**
     * 0-变频运行,1-工频运行,2-未运行(停机),3-电磁阀,4-未使用,5-故障,6-检修,7-现场手动
     * @param pumpState
     * @return
     */
    String formatPumpStateStr(String pumpState);

    /**
     * 查询水泵房中的水泵组
     * @param groupId  水泵组id
     * @return
     */
    List<Map<String,Object>> readPumpGroupDetail(Long groupId);

    Page<Map<String,Object>> readDataAnalysisReportFormPage(Page<Map<String,Object>> page , Wrapper<Map<String,Object>> wrapper,Map<String,Object> paramMap);


    boolean  insert_insertWaterPumpGroup_cardRecharge(IotWaterPumpGroup insertWaterPumpGroup,IotCardRecharge iotCardRecharge);
    /**
     * 查询 水泵组 实时(最新的一条)数据
     * @param wrapper
     * @return
     */
    List<Map<String,Object>> readRealTimeDataList(Wrapper<Map<String,Object>> wrapper);

    /**
     * 根据设备名称获取消息格式化规则
     * @param name
     * @return
     */
    public JSONObject getMessageFormatRuleJson(String name);


    public Long getGroupIdByCode(String code);

    /**
     * 查询 水泵组 不同设备状态个数
     * @param map
     * @return
     */
    public Integer selectWaterPumpStatusCount(Wrapper<Map<String,Object>> wrapper);

    /*==========曲线分析==============*/
    public List<Map<String,Object>> selectReportTimesCurveMap(Map<String,Object> map);
    public List<String> selectReportTimes(Map<String,Object> map);
    public List<Map<String,Object>> selectGroupMaps(Map<String,Object> map);
    public Map<String,Object> readDataAnalysisCurve_New(Map<String,Object> paramMap);
    public Map<String,Object> readDataAnalysisCurve_New(Map<String,Object> paramMap,int valueType);
    public List<Map<String,Object>> readDataAnalysisCurve(Map<String,Object> paramMap,Integer valueType);


    /**
     * 生成 WoGia 设备水泵个数记录
     */
    public void generateWoGiaPumpNumRecord();
    /**
     * 根据沃佳 表参数 查询设备数据
     */
    public Map<String,Map<String,Object>> getResolveWoGiaDataByTable(List<Map<String,String>> tableList);
}
