package com.pms.utils;

import com.pms.util.Description;

import java.lang.reflect.Field;

/**
 * Created by Administrator on 2017/10/23.
 */
public class FiledMarkUtil {
    /**
     * 获取 cl 的注解注释内容
     * @param cl
     * @return
     */
    public static String getFiledMarkValue(Class<?> cl){
        StringBuffer str = new StringBuffer("");
        Field[] fields = cl.getDeclaredFields();
        for(Field f : fields){
            FiledMark desc = f.getAnnotation(FiledMark.class);
            if(null!=desc){
                str.append("{").append(f.getName()).append(":").append(desc.comment()).append("；")
                .append("desc详细描述:").append(desc.desc()).append("；").append("单位为:").append(desc.unit())
                .append("}").append(",");
            }
        }
        return str.toString();
    }
}
