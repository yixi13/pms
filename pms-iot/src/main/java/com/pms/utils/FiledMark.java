package com.pms.utils;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

/**
 * Created by Administrator on 2018/1/18.
 */
@Retention(RetentionPolicy.RUNTIME)
public @interface FiledMark {
    String comment() default "";//注释
    String desc() default "";//描述
    String unit() default "";//单位
}
