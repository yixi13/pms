package com.pms.utils;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.baidubce.BceClientConfiguration;
import com.baidubce.Protocol;
import com.baidubce.auth.DefaultBceCredentials;
import com.baidubce.services.tsdb.TsdbClient;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

/**
 * 时序数据库单例模式
 *
 * @author zyl
 * @create 2017-12-20 14:23
 **/
@Component
public class TsdbUtil {
    @Value("${baidu.AccessKey}")
    private String ACCESS_KEY_ID;
    @Value("${baidu.SecretKey}")
    private String SECRET_ACCESS_KEY;
    @Value("${baidu.tsdb_Endpoint_bj}")
    private String TSDB_ENDPOINT_BJ;
    @Value("${baidu.tsdb_Endpoint_gz}")
    private String TSDB_ENDPOINT_GZ;

    //单利模式 --懒汉式(双重锁定)保证线程的安全性
    public static TsdbUtil tsdbUtil = null;

    private TsdbUtil() {

    }

    public static TsdbUtil getInstance() {
        if (tsdbUtil == null) {
            synchronized (TsdbUtil.class) {
                if (tsdbUtil == null) {
                    tsdbUtil = new TsdbUtil();
                }
            }
        }
        return tsdbUtil;
    }

    //httpTsdbClient
    public TsdbClient getHttpTsdbClient() {
        // 创建配置
        BceClientConfiguration config = new BceClientConfiguration()
                .withCredentials(new DefaultBceCredentials(ACCESS_KEY_ID, SECRET_ACCESS_KEY))
                .withEndpoint(TSDB_ENDPOINT_GZ);
        TsdbClient tsdbClient = new TsdbClient(config);
        return tsdbClient;
    }

    //httpsTsdbClient
    public TsdbClient getHttpsTsdbClient() {
        // 创建配置
        BceClientConfiguration config = new BceClientConfiguration()
                .withProtocol(Protocol.HTTPS)   //使用HTTPS协议;不设置，默认使用HTTP协议
                .withCredentials(new DefaultBceCredentials(ACCESS_KEY_ID, SECRET_ACCESS_KEY))
                .withEndpoint(TSDB_ENDPOINT_GZ);
        TsdbClient tsdbClient = new TsdbClient(config);
        return tsdbClient;
    }

    /**
     * @param address 1-广州(默认)，2-北京
     * @return
     */
    public TsdbClient getHttpTsdbClient(Integer address) {
        // 创建配置
        BceClientConfiguration config = new BceClientConfiguration()
                .withCredentials(new DefaultBceCredentials(ACCESS_KEY_ID, SECRET_ACCESS_KEY));
        if (address == null) {
            address = 1;
        }
        switch (address) {
            case 1:
                config.withEndpoint(TSDB_ENDPOINT_GZ);
                break;
            case 2:
                config.withEndpoint(TSDB_ENDPOINT_BJ);
                break;
            default:
                config.withEndpoint(TSDB_ENDPOINT_GZ);
                break;
        }
        TsdbClient tsdbClient = new TsdbClient(config);
        return tsdbClient;
    }

    //httpsTsdbClient
    public TsdbClient getHttpsTsdbClient(Integer address) {
        // 创建配置
        BceClientConfiguration config = new BceClientConfiguration()
                .withProtocol(Protocol.HTTPS)   //使用HTTPS协议;不设置，默认使用HTTP协议
                .withCredentials(new DefaultBceCredentials(ACCESS_KEY_ID, SECRET_ACCESS_KEY));
        if (address == null) {
            address = 1;
        }
        switch (address) {
            case 1:
                config.withEndpoint(TSDB_ENDPOINT_GZ);
                break;
            case 2:
                config.withEndpoint(TSDB_ENDPOINT_BJ);
                break;
            default:
                config.withEndpoint(TSDB_ENDPOINT_GZ);
                break;
        }
        TsdbClient tsdbClient = new TsdbClient(config);
        return tsdbClient;
    }


    public String returnEndPoint() {
        return this.TSDB_ENDPOINT_GZ;
    }

    public String returnEndPoint_GZ() {
        return this.TSDB_ENDPOINT_GZ;
    }

    public String returnEndPoint_BJ() {
        return this.TSDB_ENDPOINT_BJ;
    }

    public JSONObject returnRuleDestination(Integer address){
        if (address == null) {
            address = 1;
        }
        switch (address) {
            case 1:
                return returnRuleDestination_GZ();
            case 2:
                return returnRuleDestination_BJ();
            default:
                return returnRuleDestination_GZ();
        }
    }
    public JSONObject returnRuleDestination_GZ() {
        JSONObject destinationsObj = new JSONObject();
        destinationsObj.put("value", TSDB_ENDPOINT_GZ);
        destinationsObj.put("kind", "TSDB");
        return destinationsObj;
    }

    public JSONObject returnRuleDestination_BJ() {
        JSONObject destinationsObj = new JSONObject();
        destinationsObj.put("value", TSDB_ENDPOINT_BJ);
        destinationsObj.put("kind", "TSDB");
        return destinationsObj;
    }

    public JSONArray returnRuleDestinationArray(Integer address) {
        if (address == null) {
            address = 1;
        }
        switch (address) {
            case 1:
                return returnRuleDestinationArray_GZ();
            case 2:
                return returnRuleDestinationArray_BJ();
            default:
                return returnRuleDestinationArray_GZ();
        }
    }

    public JSONArray returnRuleDestinationArray_GZ() {
        JSONArray destinationsArray = new JSONArray();
        JSONObject destinationsObj = new JSONObject();
        destinationsObj.put("value", TSDB_ENDPOINT_GZ);
        destinationsObj.put("kind", "TSDB");
        destinationsArray.add(destinationsObj);
        return destinationsArray;
    }

    public JSONArray returnRuleDestinationArray_BJ() {
        JSONArray destinationsArray = new JSONArray();
        JSONObject destinationsObj = new JSONObject();
        destinationsObj.put("value", TSDB_ENDPOINT_BJ);
        destinationsObj.put("kind", "TSDB");
        destinationsArray.add(destinationsObj);
        return destinationsArray;
    }
}
