package com.pms.utils;

import com.alibaba.fastjson.JSONObject;
import com.pms.util.StringFormatUtil;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;
import java.util.StringTokenizer;

/**
 * Created by As on 2018/3/14.
 */
@Component
public class KafkaMessageFormatUtil {
    private Logger logger = LoggerFactory.getLogger(KafkaMessageToMysql.class);
/** 规则Json 示例格式
    {
        "waterFrequency": {// 规则为值加上200
            "way": "add",
            "val": "200",
        },
        "SBZT": {//进行2进制装换
            "way": "hexadecimal",
            "val": "2",
            "isReversal": "1",//是否进行二进制值字符串反转(1-是,2-否)
            "readWay":"index",
            "readVal":"1",
            "indexJson":{//0-变频运行，1-工频运行,2-未使用,3-电磁阀
                "0":"0","1":"1",
                "2":"0","3":"1",
                "4":"0","5":"1",
                "6":"0","7":"1",
                "8":"0","9":"1",
                "10":"0","11":"1",
                "12":"0","13":"1",
                "14":"2","15":"3",
            }
        },
        "GZZT": {
            "way": "hexadecimal",
            "val": "2",
            "isReversal": "1",//是否进行二进制值字符串反转(1-是,2-否)
            "readWay":"index"; //读取方式 下标
            "readVal":"1"; //读取值 为 1，读取1的下标
        }
    }
 */
    /**
     *
     * 转换消息值
     * @param key
     * @param value
     * @param rulesJson  规则Json
     * @return
     */
    public Object kafkaMessageFormat(String key, Object value,JSONObject rulesJson){
        try {
            if (value == null) {
                return value;
            }
            JSONObject keyRuleJson = rulesJson.getJSONObject(key);
            if (keyRuleJson == null || keyRuleJson.isEmpty()) {
                return value;
            }
            String keyRuleWay = keyRuleJson.getString("way");
            if (StringUtils.isBlank(keyRuleWay) || keyRuleWay.equals("*")) {
                return value;
            }
            if (keyRuleWay.equals("add")) {//加
                return (Integer) value + keyRuleJson.getDoubleValue("val");
            }
            if (keyRuleWay.equals("sub")) {//减
                return (Integer) value -keyRuleJson.getDoubleValue("val");
            }
            if (keyRuleWay.equals("ride")) {//乘
                return (Integer) value * keyRuleJson.getDoubleValue("val");
            }
            if (keyRuleWay.equals("divide")) {//除
                return (Integer) value / keyRuleJson.getDoubleValue("val");
            }
            if (keyRuleWay.equals("hexadecimal")) {//进制转换
                String hexadecimalValueStr = null;
                if ("2".equals( keyRuleJson.getString("val") )) {//转换为2进制
                    hexadecimalValueStr = Integer.toBinaryString((Integer) value);
                }else if ("8".equals( keyRuleJson.getString("val") )) {//转换为8进制
                    hexadecimalValueStr = Integer.toOctalString ((Integer) value);
                }else if ("16".equals( keyRuleJson.getString("val") )) {//转换为16进制
                    hexadecimalValueStr = Integer.toHexString((Integer) value);
                }
                //字符串反转
                String isReversal = keyRuleJson.getString("isReversal");//是否进行字符串反转
                if("1".equals(isReversal)){
                    hexadecimalValueStr =  new StringBuffer(hexadecimalValueStr).reverse().toString();//反转字符串
                }
                StringBuffer ValueStrBuffer =new StringBuffer("");
                String readWay = keyRuleJson.getString("readWay");
                if("index".equals(readWay)){//如果读取方式为下标读取
                    String readVal = keyRuleJson.getString("readVal");
                    JSONObject indexJson = keyRuleJson.getJSONObject("indexJson");
                    int lastIndex= hexadecimalValueStr.lastIndexOf(readVal);
                    if(indexJson==null||indexJson.isEmpty()){
                        int index= hexadecimalValueStr.indexOf(readVal);
                        ValueStrBuffer.append(index);
                        while (index<lastIndex){
                            index = hexadecimalValueStr.indexOf(readVal,index+1);
                            ValueStrBuffer.append(",").append(index);
                        }
                    }else{
                        int index= hexadecimalValueStr.indexOf(readVal);
                        ValueStrBuffer.append(indexJson.get( index+""));
                        while (index<lastIndex){
                            index = hexadecimalValueStr.indexOf(readVal,index+1);
                            ValueStrBuffer.append(",").append(indexJson.get(index+""));
                        }
                    }
                }
                if(ValueStrBuffer.toString().equals("null")){
                    logger.error("转换出来的消息为null,打印数据为:");
                    logger.error(key+"="+value);
                    return null;
                }
                return  "'" +ValueStrBuffer.toString()+"'";
            }
        }catch (Exception ex){
            System.out.println("此处有异常:"+ex.getMessage());
            return null;
        }
        return value;//均不匹配规则，返回原值
    }
}
