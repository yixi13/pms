package com.pms.utils;

import com.baidubce.BceClientConfiguration;
import com.baidubce.Protocol;
import com.baidubce.auth.DefaultBceCredentials;
import com.baidubce.services.tsdb.TsdbClient;
import com.baidubce.services.tsdb.model.*;
import com.pms.util.DateUtil;
import com.pms.util.StringFormatUtil;
import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * Tsdb 时间转换
 *
 * @author ASUS_B
 * @create 2017-12-20 14:23
 **/
public class TsdbReqParamUtil {

    /**
     * 获取百度请求时间
     *
     * @param timeNum  数值，默认为1
     * @param timeType 类型,默认为6(1年,2月,3日,4时,5分,6秒)
     * @return
     */
    public static String convertRequestRelativeTimeStr_BaiDu(Integer timeNum, Integer timeType) {
        return convertRequestRelativeTimeStr_BaiDu(timeNum, timeType, 1);
    }

    /**
     * 获取百度请求时间
     *
     * @param timeNum  数值，默认为1
     * @param timeType 类型,默认为6(1年,2月,3日,4时,5分,6秒)
     * @param postfix  1-ago(前)，2-after(后)
     * @return
     */
    public static String convertRequestRelativeTimeStr_BaiDu(Integer timeNum, Integer timeType, int postfix) {
        if (timeNum == null || timeNum < 1) {
            timeNum = 1;
        }
        if (timeType == null || timeType < 1 || timeType > 6) {
            timeType = 6;
        }
        StringBuilder timeStr = new StringBuilder("");
        timeStr.append(timeNum);
        switch (timeType) {
            case 1:
                timeStr.append(" year ");
                break;
            case 2:
                timeStr.append(" month ");
                break;
            case 3:
                timeStr.append(" day ");
                break;
            case 4:
                timeStr.append(" hour ");
                break;
            case 5:
                timeStr.append(" minute ");
                break;
            case 6:
                timeStr.append(" seconds ");
                break;
            default:
                timeStr.append(" second ");
                break;
        }
        switch (timeType) {
            case 1:
                timeStr.append("ago");
                break;
            default:
                timeStr.append("after");
                break;
        }
        return timeStr.toString();
    }

    /**
     * 创建Filters中的过滤值-value
     *
     * @param filterType  过滤方式，默认为1(1-等于,2不等于,3大于,4小于,5大于等于,6小于等于)
     * @param filterValue 过滤值
     * @return value
     */
    public static String createFiltersValue(Integer filterType, Object filterValue) {
        if (filterValue == null) {
            return null;
        }
        StringBuilder value = new StringBuilder("");
        if (filterType == null) {
            filterType = 1;
        }
        switch (filterType) {
            case 1:
                value.append("=");
                break;
            case 2:
                value.append("!=");
                break;
            case 3:
                value.append(">");
                break;
            case 4:
                value.append("<");
                break;
            case 5:
                value.append(">=");
                break;
            case 6:
                value.append("<=");
                break;
            default:
                value.append("=");
                break;
        }
        value.append(" ");
        value.append(filterValue);
        return value.toString();
    }

    /**
     * 创建Filters中的过滤值-value
     *
     * @param filterType  过滤方式，默认为1(1-等于,2不等于,3大于,4小于,5大于等于,6小于等于)
     * @param filterValue 过滤值
     * @return value
     */
    public static String createFiltersValue(String filterType, Object filterValue) {
        if (filterValue == null) {
            return null;
        }
        try {
            Integer filterTypeInt = Integer.parseInt(filterType);
            return createFiltersValue(filterTypeInt, filterValue);
        } catch (NumberFormatException ex) {
            return null;
        }

    }
/*=================================生成TagFilter start======================*/

    /**
     * 创建Filters中的TagFilter
     *
     * @param tag 标签key值
     * @param in  可以包含的tag的value列表；不可与notIn和like同时存在 * @return
     */
    public static TagFilter createFiltersTagFilter_in(String tag, List<String> in) {
        TagFilter tagFilter = new TagFilter();
        tagFilter.setTag(tag);
        tagFilter.setIn(in);
        return tagFilter;
    }

    /**
     * 创建Filters中的TagFilter
     *
     * @param tag   标签key值
     * @param notIn 需要过滤的tag的value列表；不可与in和like同时存在
     * @return
     */
    public static TagFilter createFiltersTagFilter_notIn(String tag, List<String> notIn) {
        TagFilter tagFilter = new TagFilter();
        tagFilter.setTag(tag);
        tagFilter.setNotIn(notIn);
        return tagFilter;
    }

    /**
     * 创建Filters中的TagFilter
     *
     * @param tag  标签key值
     * @param like 需要匹配的规则，“%”匹配任意数量的字符，“_”匹配单个字符，转义字符为“/”；不可与in和notIn同时存在
     * @return
     */
    public static TagFilter createFiltersTagFilter_like(String tag, String like) {
        TagFilter tagFilter = new TagFilter();
        tagFilter.setTag(tag);
        tagFilter.setLike(like);
        return tagFilter;
    }
     /*=================================生成TagFilter end======================*/

    /*=================================生成Filters start======================*/
    public static Filters createFilters() {
        return createFilters(null, null, null, null, null, null);
    }

    public static Filters createFilters(String startTime) {
        return createFilters(startTime, null, null, null, null, null);
    }

    public static Filters createFilters(String startTime, String endTime) {
        return createFilters(startTime, endTime, null, null, null, null);
    }

    public static Filters createFilters(String startTime, String endTime, String value) {
        return createFilters(startTime, endTime, null, value, null, null);
    }

    public static Filters createFilters(String startTime, String endTime, List<FieldFilter> fields) {
        return createFilters(startTime, endTime, null, null, fields, null);
    }

    public static Filters createFilters(String startTime, String endTime, Map<String, List<String>> tags) {
        return createFilters(startTime, endTime, tags, null, null, null);
    }

    public static Filters createFilters_TagFilter(String startTime, String endTime, List<TagFilter> tagFilters) {
        return createFilters(startTime, endTime, null, null, null, tagFilters);
    }

    public static Filters createFilters(String startTime, String endTime, String value, List<TagFilter> tagFilters) {
        return createFilters(startTime, endTime, null, value, null, tagFilters);
    }

    public static Filters createFilters(String startTime, String endTime, List<FieldFilter> fields, List<TagFilter> tagFilters) {
        return createFilters(startTime, endTime, null, null, fields, tagFilters);
    }

    /**
     * 组建Filters对象
     *
     * @param startTime  起始时间 默认1天前
     * @param endTime    截止时间
     * @param tags       标签,模糊查询
     * @param value      单Field条件
     * @param fields     多Field条件
     * @param tagFilters 标签值过滤
     * @return Filters对象
     */
    public static Filters createFilters(String startTime, String endTime, Map<String, List<String>> tags
            , String value, List<FieldFilter> fields, List<TagFilter> tagFilters) {
        Filters filters = new Filters();
        //起始时间
        if (StringUtils.isBlank(startTime)) {
            startTime = "1 day ago";
        }
        filters.withRelativeStart(startTime);
        //截止时间
        if (StringUtils.isNotBlank(endTime)) {
            filters.withRelativeEnd(endTime);
        }
        //单Field条件
        if (StringUtils.isNotBlank(value)) {
            filters.withValue(value);
        }
        //多Field条件
        if (fields != null && !fields.isEmpty()) {
            filters.withFields(fields);
        }
        //标签模糊查询
        if (tags != null && !tags.isEmpty()) {
            filters.withTags(tags);
        }
        if (tagFilters != null && !tagFilters.isEmpty()) {
            filters.withTagFilters(tagFilters);
        }
        return filters;
    }

    public static Filters createAbsoFilters_SignFiled(Long absoStartTime, Long absoEndTime,String value,List<TagFilter> tagFilters) {
        Filters filters = new Filters();
        //起始时间 -默认当前时间往前1天
        if (absoStartTime==null) {
            absoStartTime = DateUtil.addDate(new Date(),5,-1).getTime();
        }
        filters.withAbsoluteStart(absoStartTime);
        //截止时间
        if (absoEndTime!=null&& absoEndTime>absoStartTime) {
            filters.withAbsoluteEnd(absoEndTime);
        }
        //单Field条件
        if (StringUtils.isNotBlank(value)) {
            filters.withValue(value);
        }
        if (tagFilters != null && !tagFilters.isEmpty()) {
            filters.withTagFilters(tagFilters);
        }
        return filters;
    }
/*=================================生成Filters end======================*/

/*=================================生成Query start=====================*/

    /**
     * 单域 查询
     *
     * @param metric  度量
     * @param filters Filters对象
     * @return
     */
    public static Query createSingleFieldQuery(String metric, Filters filters) {
        return createQuery(metric, filters, null, null, null, null, null, null, null, null, null);
    }

    /**
     * 单域 查询
     *
     * @param metric  度量
     * @param filters Filters对象
     * @param limit   返回的data point数目限制，不填时默认为1,000,000
     * @return
     */
    public static Query createSingleFieldQuery(String metric, Filters filters, Integer limit) {
        return createQuery(metric, filters, null, null, null, null, null, null, limit, null, null);
    }

    /**
     * 单域 查询
     *
     * @param metric  度量
     * @param filters Filters对象
     * @param field   单域查询，field的名称
     */
    public static Query createSingleFieldQuery(String metric, Filters filters, String field) {
        return createQuery(metric, filters, field, null, null, null, null, null, null, null, null);
    }

    /**
     * 单域 查询
     *
     * @param metric  度量
     * @param filters Filters对象
     * @param field   单域查询，field的名称
     * @param limit   返回的data point数目限制，不填时默认为1,000,000
     */
    public static Query createSingleFieldQuery(String metric, Filters filters, String field, Integer limit) {
        return createQuery(metric, filters, field, null, null, null, null, null, limit, null, null);
    }

    /**
     * 单域 查询
     *
     * @param metric      度量
     * @param filters     Filters对象
     * @param field       单域查询，field的名称
     * @param fill        插值选项，类型为Fill，插值只作用于原始数据，每个时间序列单独进行插值
     * @param marker      用于分页查询，从marker开始返回，应使用上一次查询返回的nextMarker值
     * @param tags        需要查询的metric的tag的key列表
     * @param groupBy     分组条件，由GroupBy对象组成的数组
     * @param limit       返回的data point数目限制，不填时默认为1,000,000
     * @param aggregators 聚合条件，由Aggregator对象组成的数组
     * @param order       支持Asc和Desc，默认是Asc
     */
    public static Query createSingleFieldQuery(String metric, Filters filters, String field, Fill fill, String marker
            , List<String> tags, List<GroupBy> groupBy, Integer limit, List<Aggregator> aggregators, String order) {
        return createQuery(metric, filters, field, null, fill, marker, tags, groupBy, limit, aggregators, order);
    }

    /**
     * 多域 查询
     */
    public static Query createFieldsQuery(String metric, Filters filters, List<String> fields) {
        return createQuery(metric, filters, null, fields, null, null, null, null, null, null, null);
    }

    /**
     * 多域 查询
     */
    public static Query createFieldsQuery(String metric, Filters filters, List<String> fields, Integer limit) {
        return createQuery(metric, filters, null, fields, null, null, null, null, limit, null, null);
    }

    /**
     * 多域 查询
     */
    public static Query createFieldsQuery(String metric, Filters filters, List<String> fields, Fill fill) {
        return createQuery(metric, filters, null, fields, fill, null, null, null, null, null, null);
    }

    public static Query createFieldsQuery(String metric, Filters filters, List<String> fields, String marker) {
        return createQuery(metric, filters, null, fields, null, marker, null, null, null, null, null);
    }

    /**
     * 多域 查询
     */
    public static Query createFieldsQuery(String metric, Filters filters, List<String> fields, Fill fill, String marker
            , List<String> tags, List<GroupBy> groupBy, Integer limit, List<Aggregator> aggregators, String order) {
        return createQuery(metric, filters, null, fields, fill, marker, tags, groupBy, limit, aggregators, order);
    }

    /**
     * 生成Query对象
     *
     * @param metric      度量
     * @param filters     Filters对象
     * @param field       单域查询，field的名称
     * @param fields      多域查询，field的名称的集合
     * @param fill        插值选项，类型为Fill，插值只作用于原始数据，每个时间序列单独进行插值
     * @param marker      用于分页查询，从marker开始返回，应使用上一次查询返回的nextMarker值
     * @param tags        需要查询的metric的tag的key列表
     * @param groupBy     分组条件，由GroupBy对象组成的数组
     * @param limit       返回的data point数目限制，不填时默认为1,000,000
     * @param aggregators 聚合条件，由Aggregator对象组成的数组
     * @param order       支持Asc和Desc，默认是Asc
     * @return
     */
    public static Query createQuery(String metric, Filters filters, String field, List<String> fields, Fill fill, String marker
            , List<String> tags, List<GroupBy> groupBy, Integer limit, List<Aggregator> aggregators, String order) {
        if (StringUtils.isBlank(metric)) {
            return null;
        }
        Query query = new Query();
        //度量
        query.withMetric(metric);
        //设置 过滤条件filters
        if (filters == null) {
            filters = createFilters();
        }
        query.withFilters(filters);
        // 设置域
        if (StringUtils.isBlank(field)) {
            if (fields != null && !fields.isEmpty()) {
                query.withFields(fields);
            }
        }
        if (StringUtils.isNotBlank(field)) {
            query.withField(field);
        }
        //设置查询的标签
        if (tags != null && !tags.isEmpty()) {
            query.withTags(tags);
        }
        //设置分组条件
        if (groupBy != null && !groupBy.isEmpty()) {
            query.withGroupBy(groupBy);
        }
        //设置 聚合条件
        if (aggregators != null && !aggregators.isEmpty()) {
            query.withAggregators(aggregators);
        }
        //设置排序
        if (StringUtils.isNotBlank(order)) {
            if (order.equals("desc")) {
                order = "Desc";
            }
            if (!order.equals("Desc")) {
                order = "Asc";
            }
            query.withOrder(order);
        }
        //设置分页查询
        if (StringUtils.isNotBlank(marker)) {
            query.withMarker(marker);
        }
        //设置插值
        if (fill != null) {
            query.withFill(fill);
        }
        //设置 查询数量
        if (limit != null && limit > 0) {
            query.withLimit(limit);
        }
        return query;
    }
/*=================================生成Query end=====================*/

/*=================================生成Fill插值对象 start=====================*/

    /**
     * 创建 插值对象
     *
     * @param type             插值类型 Linear（线性插值）、Previous（按前一个值插值）、Fixed（固定值插值）
     * @param interval         插值间隔，一个时间序列在此间隔内没有值则进行插值
     * @param maxWriteInterval 最大写入间隔，一个时间序列的数据最大写入间隔（在此间隔内必然有值），默认为0。
     * @param value            固定值插值所插入的固定值，type为Fixed时必填
     * @return
     */
    public static Fill createFill(String type, String interval, String maxWriteInterval, Object value) {
        if (StringUtils.isBlank(type) || StringUtils.isBlank(interval)) {
            return null;
        }
        Fill fill = new Fill();
        fill.setInterval(interval);
        if (type.equals("Fixed") || type.equals("fixed")) {
            if (value == null) {
                return null;
            }
            fill.setType("Fixed");
            if (value instanceof Long) {
                fill.setValue((Long) value);
            }
            if (value instanceof Double) {
                fill.setValue((Double) value);
            }
            if (value instanceof String) {
                fill.setValue(value.toString());
            }
            return fill;
        }
        if (type.equals("Linear") || type.equals("linear")) {
            fill.setType("Linear");
        }
        if (type.equals("Previous") || type.equals("previous")) {
            fill.setType("Previous");
        }
        if (StringUtils.isNotBlank(maxWriteInterval)) {
            fill.setMaxWriteInterval(maxWriteInterval);
        }
        return fill;
    }
 /*=================================生成Fill插值对象 end=====================*/


  /*=================================生成聚合函数对象 start=====================*/

    /**
     * 生成聚合函数的采样的时间长度
     *
     * @param value 数值，默认为1
     * @param type  类型,默认为6(1年,2月,3日,4时,5分,6秒)
     * @return
     */
    public static String createAggregatorSampling(Integer type, Integer value) {
        StringBuilder sampling = new StringBuilder("");
        if (value == null || value < 1) {
            value = 1;
        }
        if (type == null || type < 1 || type > 6) {
            type = 6;
        }
        StringBuilder timeStr = new StringBuilder("");
        timeStr.append(value);
        switch (type) {
            case 1:
                timeStr.append(" years");
                break;
            case 2:
                timeStr.append(" months");
                break;
            case 3:
                timeStr.append(" days");
                break;
            case 4:
                timeStr.append(" hours");
                break;
            case 5:
                timeStr.append(" minutes");
                break;
            case 6:
                timeStr.append(" seconds");
                break;
            default:
                timeStr.append(" seconds");
                break;
        }
        return sampling.toString();
    }

    /**
     * 求平均值
     *
     * @param sampling 采样的时间长度，如"10 minutes"
     * @return
     */
    public static Aggregator createAggregator_Avg(String sampling) {
        return createAggregator("Avg", sampling);
    }

    /**
     * 求数目统计
     *
     * @param sampling 采样的时间长度，如"10 minutes"
     * @return
     */
    public static Aggregator createAggregator_Count(String sampling) {
        return createAggregator("Count", sampling);
    }

    /**
     * 求第一个值
     *
     * @param sampling 采样的时间长度，如"10 minutes"
     * @return
     */
    public static Aggregator createAggregator_First(String sampling) {
        return createAggregator("First", sampling);
    }

    /**
     * 求最后一个值
     *
     * @param sampling 采样的时间长度，如"10 minutes"
     * @return
     */
    public static Aggregator createAggregator_Last(String sampling) {
        return createAggregator("Last", sampling);
    }

    /**
     * 求最大值
     *
     * @param sampling 采样的时间长度，如"10 minutes"
     * @return
     */
    public static Aggregator createAggregator_Max(String sampling) {
        return createAggregator("Max", sampling);
    }

    /**
     * 求最小值
     *
     * @param sampling 采样的时间长度，如"10 minutes"
     * @return
     */
    public static Aggregator createAggregator_Min(String sampling) {
        return createAggregator("Min", sampling);
    }

    /**
     * 求总和
     *
     * @param sampling 采样的时间长度，如"10 minutes"
     * @return
     */
    public static Aggregator createAggregator_Sum(String sampling) {
        return createAggregator("Sum", sampling);
    }

    /**
     * 求差 ,以每两个相邻的value的差值作为结果
     *
     * @return
     */
    public static Aggregator createAggregator_Diff() {
        return createAggregator("Diff", null);
    }

    /**
     * 求除
     *
     * @return
     * @Param factor 除数
     */
    public static Aggregator createAggregator_Div(Double divisor) {
        return createAggregator("Diff", null, null, divisor, null, null);
    }

    /**
     * 求倍数 乘法
     *
     * @return
     * @Param factor 倍数
     */
    public static Aggregator createAggregator_Scale(Double factor) {
        return createAggregator("Scale", null, null, null, factor, null);
    }

    /**
     * 变化率 以每两个相邻的value在单位时间（见参数timeUnit）的变化率作为结果
     *
     * @return
     * @Param timeUnit 时间单位
     */
    public static Aggregator createAggregator_Rate(String timeUnit) {
        return createAggregator("Rate", null, null, null, null, timeUnit);
    }

    /**
     * 百分位数，以每个采样时间范围内的value的第p（见请求参数percentile）百分位数作为结果
     *
     * @return
     * @Param timeUnit 时间单位
     */
    public static Aggregator createAggregator_Percentile(String sampling, Double percentile) {
        return createAggregator("Percentile", sampling, percentile, null, null, null);
    }

    /**
     * 相邻值去重
     *
     * @return
     */
    public static Aggregator createAggregator_AdjacentUnique(String sampling) {
        return createAggregator("AdjacentUnique", sampling, null, null, null, null);
    }

    public static Aggregator createAggregator(String name, String sampling) {
        return createAggregator(name, sampling, null, null, null, null);
    }

    /**
     * Aggregator对象
     *
     * @param name       聚合的方式，目前支持Avg、Dev、Count、First、Last、LeastSquares、Max、Min、Percentile、Sum、Diff、Div、Scale、Rate、AdjacentUnique
     * @param sampling   采样的时间长度，如"10 minutes"，name为Avg、Dev、Count、First、Last、LeastSquares、Max、Min、Percentile、Sum时才需填此项，若不填写则sampling为整个查询时间范围
     * @param percentile 百分数，取值范围为(0,1]，如0.1表示10%，name为Percentile时必填
     * @param divisor    除数，name为Div时必填
     * @param factor     倍数，name为Scale时必填
     * @param timeUnit   时间单位，name为Rate时必填
     * @return
     */
    public static Aggregator createAggregator(String name, String sampling, Double percentile, Double divisor, Double factor, String timeUnit) {
        if (StringUtils.isBlank(name)) {
            return null;
        }
        Aggregator aggregator = new Aggregator();
        name = StringFormatUtil.toUpperCaseFirstOne(name);//首字母大写
        if (name.equals("Avg") || name.equals("Dev") || name.equals("Count") || name.equals("Max")
                || name.equals("First") || name.equals("Last") || name.equals("LeastSquares")
                || name.equals("Min") || name.equals("Sum")) {
            aggregator.setName(name);
            if (StringUtils.isNotBlank(sampling)) {
                aggregator.setSampling(sampling);
            }
        }
        if (name.equals("Percentile")) {
            if (null == percentile || percentile < 0 || percentile > 1) {
                return null;
            }
            if (StringUtils.isNotBlank(sampling)) {
                aggregator.setSampling(sampling);
            }
            aggregator.setName(name);

            aggregator.withPercentile(percentile);
        }
        if (name.equals("Div")) {
            if (null == divisor) {
                return null;
            }
            aggregator.setName(name);
            aggregator.withDivisor(divisor);
        }
        if (name.equals("Scale")) {
            if (null == factor) {
                return null;
            }
            aggregator.setName(name);
            aggregator.withFactor(factor);
        }
        if (name.equals("Rate")) {
            if (null == timeUnit) {
                return null;
            }
            aggregator.setName(name);
            aggregator.withTimeUnit(timeUnit);
        }
        if (name.equals("AdjacentUnique")) {
            aggregator.setName(name);
        }
        if (name.equals("Diff")) {//相邻差值
            aggregator.setName(name);
        }
        return aggregator;
    }

    /*=================================生成聚合函数对象 end=====================*/
    public static GroupBy createGroupBy_Tag(List<String> tags) {
        return createGroupBy("Tag", tags);
    }

    public static GroupBy createGroupBy(String name, List<String> tags) {
        if (StringUtils.isBlank(name)) {
            return null;
        }
        GroupBy groupBy = new GroupBy();
        name = StringFormatUtil.toUpperCaseFirstOne(name);//首字母大写
        groupBy.setName(name);
        if (name.equals("Tag")) {
            if (tags == null || tags.isEmpty()) {
                return null;
            }
            groupBy.setTags(tags);
        }
        return groupBy;
    }

    public static FieldFilter createFieldFilter(String field, String value) {
        return new FieldFilter(field, value);
    }

    /**
     * 默认返回  秒 seconds
     *
     * @param num
     * @return
     */
    public static String returnTimeStrByNum(Integer num) {
        if (num == null) {
            num = 6;
        }
        switch (num) {
            case 1:
                return "years";
            case 2:
                return "months";
            case 3:
                return "days";
            case 4:
                return "hours";
            case 5:
                return "minutes";
            case 6:
                return "seconds";
            default:
                return "seconds";
        }
    }

    /**
     * 默认返回  秒 seconds
     *
     * @param num
     * @return
     */
    public static String returnTimeStrByNum(String num) {
        if (StringUtils.isBlank(num)) {
            num = "6";
        }
        if (num.equals("1")) {
            return "years";
        }
        if (num.equals("2")) {
            return "months";
        }
        if (num.equals("3")) {
            return "days";
        }
        if (num.equals("4")) {
            return "hours";
        }
        if (num.equals("5")) {
            return "minutes";
        }
        if (num.equals("6")) {
            return "seconds";
        }
        return "seconds";
    }

    /**
     * 聚合函数类型{1-求平均数，2-总记录数，3-求总和,4-求相邻值的差,5/6-求最大/小值,7/8-求倍数/除数,9/10-求第一个/最后一个值}
     * 返回构造函数名称 默认求平均值
     * @param num Avg、Dev、Count、First、Last、LeastSquares、Max、Min、Percentile、Sum、Diff、Div、Scale、Rate、AdjacentUnique
     * @return
     */
    public static String returnAggregatorNameStrByNum(String num) {
        if (StringUtils.isBlank(num)) {
            num = "1";
        }
        if (num.equals("1")) {//平均数
            return "Avg";
        }
        if (num.equals("2")) {//总条数
            return "Count";
        }
        if (num.equals("3")) {//求总和
            return "Sum";
        }
        if (num.equals("4")) {//求相邻值的差
            return "Diff";
        }
        if (num.equals("5")) {//求最大值
            return "Max";
        }
        if (num.equals("6")) {//求最小值
            return "Min";
        }
        if (num.equals("7")) {//求乘法
            return "Scale";
        }
        if (num.equals("8")) {//求除法
            return "Div";
        }
        if(num.equals("9")){//第一个值
            return "First";
        }
        if(num.equals("10")){//第一个值
            return "Last";
        }
        return "Avg";
    }

    /**
     * 返回构造函数名称 默认求平均值
     * @param num Avg、Dev、Count、First、Last、LeastSquares、Max、Min、Percentile、Sum、Diff、Div、Scale、Rate、AdjacentUnique
     * @return
     */
    public static String returnAggregatorNameStrByNum(Integer num) {
        if (num==null) {num = 1;}
       return returnAggregatorNameStrByNum(num.toString());
    }
}
