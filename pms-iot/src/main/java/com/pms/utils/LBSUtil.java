package com.pms.utils;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.pms.util.httpUtil.HttpClientUtil;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * LBS 请求工具类
 * Created by Administrator on 2017/8/22.
 */
@Component
public class LBSUtil {
    /**
     * 百度LBS提供的AK值
     */
    @Value("${baidu.lbs.ak}")
    private String AK ;
    /**
     * LBS社区表id
     */
    @Value("${baidu.lbs.tableId.community}")
    private String communityGeotableId;
    /**
     * LBS社区表id
     */
    @Value("${baidu.lbs.tableId.waterPumpHouse}")
    private String waterPumpHouseGeotableId;
    /**
     * 百度LBS创建数据请求
     */
    private static final String poiCreateUrl="http://api.map.baidu.com/geodata/v3/poi/create";
    /**
     * 百度LBS修改数据请求
     */
    private static final String poiUpdateUrl="http://api.map.baidu.com/geodata/v3/poi/update";

    /**
     * 百度LBS删除数据请求
     */
    private static final String poiDeleteUrl="http://api.map.baidu.com/geodata/v3/poi/delete";
    /**
     * 百度LBS查询数据请求
     */
    private static final String poiSelectUrl="http://api.map.baidu.com/geodata/v3/poi/list";
    /**
     * 百度LBS周边搜索数据请求
     */
    private static final String poiGeoSearchUrl="http://api.map.baidu.com/geosearch/v3/nearby";

    /**
     * 百度LBS 云逆地理编码接口
     */
    private static final String cloudrgcUrl="http://api.map.baidu.com/cloudrgc/v1";


    /**
     *
     * @param paramMap
     * @return
     */
    public static JSONObject poiUpdateByMap(Map<String, Object> paramMap){
        return poiOperationByMap(poiUpdateUrl,paramMap);
    }

    public static JSONObject poiCreateByMap(Map<String, Object> paramMap){
        return poiOperationByMap(poiCreateUrl,paramMap);
    }

    public static JSONObject poiDeleteByMap(Map<String, Object> paramMap){
        return poiOperationByMap(poiDeleteUrl,paramMap);
    }
    public static JSONObject poiGeoSearchByMap(Map<String, Object> paramMap){
        return poiGetOperationByMap(poiGeoSearchUrl,paramMap);
    }
    /**
     *
     * @param paramMap
     * @return
     */
    public static JSONObject poiOperationByMap(String url,Map<String, Object> paramMap){
        JSONObject ResponseJSON = null;
        ResponseJSON =  HttpClientUtil.doPost(url,getParamStr(paramMap));
        return ResponseJSON;
    }
    /**
     *
     * @param paramMap
     * @return
     */
    public static JSONObject poiGetOperationByMap(String url,Map<String, Object> paramMap){
        JSONObject ResponseJSON = null;
       String result =  HttpClientUtil.doGet(url+"?"+getParamStr(paramMap),"");
        ResponseJSON = JSON.parseObject(result);
        return ResponseJSON;
    }
    /**
     * 根据map生成请求参数字符串
     * @param paramMap
     * @return
     */
    public static String getParamStr(Map<String, Object> paramMap){
        StringBuilder paramStr = new StringBuilder("");
        List<String> paramKeys = new ArrayList<String>(paramMap.size());
        paramKeys.addAll(paramMap.keySet());
        for (int x=0;x<paramKeys.size();x++){
            if(x==0){
                paramStr.append(paramKeys.get(x)).append("=").append(paramMap.get(paramKeys.get(x)));
            }
            if(x>0){
                paramStr.append("&").append(paramKeys.get(x)).append("=").append(paramMap.get(paramKeys.get(x)));
            }
        }
        return paramStr.toString();
    }

    public String getAK() {
        return AK;
    }

    public void setAK(String AK) {
        this.AK = AK;
    }

    public String getCommunityGeotableId() {
        return communityGeotableId;
    }

    public void setCommunityGeotableId(String communityGeotableId) {
        this.communityGeotableId = communityGeotableId;
    }

    public String getWaterPumpHouseGeotableId() {
        return waterPumpHouseGeotableId;
    }

    public void setWaterPumpHouseGeotableId(String waterPumpHouseGeotableId) {
        this.waterPumpHouseGeotableId = waterPumpHouseGeotableId;
    }
     /**
     * 周边收缩店铺
     * @param tableId 表id
     * @param longitude 经度值
     * @param latitude  纬度值
     * @param radius    搜索范围
     * @param pageNum   搜索页数
     * @param pageSize  搜索条数
     * @param filterStr 过滤条件( key:valeu|key:value 格式（以|区分）)
     * @param tags       搜索标签（ tag1 tag2 tag3 格式（以空格区分） ）
     * @param q           关键字（ 商品名称）
     * @return
     */
    public String poiGeoSearch(String tableId,double longitude,double latitude,int radius,int pageNum,int pageSize,String filterStr,String tags,String sortBy,String q){
        String responseJSONstr = null;
        StringBuilder url = new StringBuilder(poiGeoSearchUrl).append("?ak=").append(getAK()).append("&geotable_id=").append(tableId);
        url.append("&location=").append(longitude).append(",").append(latitude);
        url.append("&coord_type=").append(3).append("&radius=").append(radius);
        if(StringUtils.isNotBlank(tags)){
            url.append("&tags=").append(tags);
        }
        if(StringUtils.isNotBlank(sortBy)){
            url.append("&sortby=").append(sortBy);
        }
        if(StringUtils.isNotBlank(q)){
            url.append("&q=").append(q);
        }
        url.append("&page_index=").append(pageNum-1);
        url.append("&page_size=").append(pageSize);
        url.append("&filter=").append("state:2");//过滤（查询启用的店铺）
        if(StringUtils.isNotBlank(filterStr)){
            url.append("|").append(filterStr);
        }
        responseJSONstr =  HttpClientUtil.doGet(url.toString(),"");
        return responseJSONstr;
    }



    /**
     * 查询数据详情
     * @param poiId
     * @return
     */
    public JSONObject poiDetDetailByPoiId(String tableId,String poiId){
        StringBuilder url = new StringBuilder("http://api.map.baidu.com/geosearch/v3/detail/").append(poiId);
        url.append("?ak=").append(getAK()).append("&geotable_id=").append(tableId).append("&coord_type=").append(3);
        JSONObject ResponseJSON =  HttpClientUtil.doPost(url.toString(),"");
//        String status = ResponseJSON.getString("status");
        return ResponseJSON;
    }
    /**
     * 请求云逆地理编码
     * @param longitude  经度
     * @param latitude  纬度
     * @return
     */
    public JSONObject cloudrgcUrl1(Double longitude,Double latitude,String tableId){
        StringBuilder url = new StringBuilder(cloudrgcUrl);
        url.append("?ak=").append(getAK()).append("&geotable_id=").append(tableId);
        url.append("&location=").append(latitude).append(",").append(longitude);
        JSONObject ResponseJSON =  HttpClientUtil.doPost(url.toString(),"");
        return ResponseJSON;
    }
}
