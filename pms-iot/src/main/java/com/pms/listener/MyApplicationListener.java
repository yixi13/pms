package com.pms.listener;

//import com.pms.utils.ApplicationInitAfterOperate;

import com.pms.utils.KafkaMessageToMysql;
import com.pms.utils.KafkaUtil;
import org.apache.kafka.clients.consumer.KafkaConsumer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.event.ApplicationEnvironmentPreparedEvent;
import org.springframework.boot.context.event.ApplicationFailedEvent;
import org.springframework.boot.context.event.ApplicationPreparedEvent;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.ApplicationEvent;
import org.springframework.context.ApplicationListener;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.event.*;


/**
 * <p>
 * 监听器
 * 执行顺序 ：
 * ContextRefreshedEvent
 * ObjectMapperConfigured
 * EnvironmentChangeEvent
 * ApplicationPreparedEvent
 * ContextRefreshedEvent
 * ApplicationReadyEvent
 * </p>
 *
 * @author asus
 * @since 2017-12-25
 */
//@Configuration
public class MyApplicationListener implements ApplicationListener<ApplicationEvent> {
    private KafkaConsumer<String, String> kafkaConsumer;
    @Autowired
    private KafkaMessageToMysql kafkaMessageToMysql;

    private boolean triggerApplicationReadyEvent = false;
    //	@Autowired
//	private ApplicationInitAfterOperate applicationInitAfterOperate;
    private Logger logger = LoggerFactory.getLogger(MyApplicationListener.class);

    @Override
    public void onApplicationEvent(ApplicationEvent event) {

        // 在这里可以监听到Spring Boot的生命周期
        if (event instanceof ApplicationEnvironmentPreparedEvent) { // 初始化环境变量
            logger.info("========================================");
            logger.info("初始化环境变量");
            logger.info("========================================");
        } else if (event instanceof ApplicationPreparedEvent) { // 初始化完成
            logger.info("========================================");
            logger.info("初始化环境变量完成");
            logger.info("========================================");
        } else if (event instanceof ContextRefreshedEvent) { // 应用刷新
            logger.info("========================================");
            logger.info("应用刷新");
            logger.info("========================================");
        } else if (event instanceof ApplicationReadyEvent) {// 应用已启动完成
            logger.info("========================================");
            logger.info("应用已启动完成");
            logger.info("========================================");
           if (triggerApplicationReadyEvent) {
                kafkaConsumer = KafkaUtil.getConsumerInstance();
                kafkaMessageToMysql.saveWaterPumpAttrData(kafkaConsumer);
            }
            if( !triggerApplicationReadyEvent){
                triggerApplicationReadyEvent = true;
            }
        } else if (event instanceof ContextStartedEvent) { // 应用启动，需要在代码动态添加监听器才可捕获 }

        } else if (event instanceof ContextStoppedEvent) { // 应用停止

        } else if (event instanceof ContextClosedEvent) { // 应用关闭

        } else if (event instanceof ApplicationFailedEvent) {
            logger.info("========================================");
            logger.info("启动失败");
            logger.info("========================================");
            if (kafkaConsumer != null) {
                kafkaConsumer.close();
            }
        }
    }

    public KafkaConsumer<String, String> getKafkaConsumer() {
        return kafkaConsumer;
    }

    public void setKafkaConsumer(KafkaConsumer<String, String> kafkaConsumer) {
        this.kafkaConsumer = kafkaConsumer;
    }

    public boolean isTriggerApplicationReadyEvent() {
        return triggerApplicationReadyEvent;
    }

    public void setTriggerApplicationReadyEvent(boolean triggerApplicationReadyEvent) {
        this.triggerApplicationReadyEvent = triggerApplicationReadyEvent;
    }
}
