package com.pms.rpc;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.pms.entity.Community;
import com.pms.service.ICommunityService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Map;

/**
 * Created by Administrator on 2018/1/3 0003.
 */
@RestController
@RequestMapping("api")
public class communityService {
    @Autowired
    ICommunityService communityService;

    @RequestMapping(value = "/rpcCommunity/selectByAreaId", method = RequestMethod.GET)
    public List<Community> selectByAreaId(Integer areaGb){
        return communityService.selectList(new EntityWrapper<Community>().eq("area_gb",areaGb));
    }

    @RequestMapping(value = "/selectMap/{communityId}", method = RequestMethod.GET)
    public Map<String, Object> selectMap(@PathVariable Long communityId){
        return communityService.selectMap(new EntityWrapper<Community>().eq("community_id",communityId));
    }

    @RequestMapping(value = "/rpcCommunity/selectAll", method = RequestMethod.GET)
    public List<Community> selectAll(){
        return communityService.selectList(new EntityWrapper<Community>());
    }

    @RequestMapping(value = "/rpcCommunity/selectMapAll", method = RequestMethod.GET)
    public List<Map<String, Object>> selectMapAll(Integer areaGb){
        List<Map<String, Object>>  community=communityService.selectMaps(new EntityWrapper<Community>().eq("area_gb",areaGb));
        return community;
       }

    @RequestMapping(value = "/selectByCommunityId/{communityId}", method = RequestMethod.POST)
    public Map<String, Object> selectByCommunityId(@PathVariable("communityId") Long communityId){
        return communityService.selectMap(new EntityWrapper<Community>().eq("community_id",communityId));
    }


}
