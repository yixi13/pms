package com.pms.rpc;
import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.pms.entity.Company;
import com.pms.service.ICompanyService;
import com.pms.service.IRelationTableService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import java.util.List;
import java.util.Map;

/**
 * Created by Administrator on 2018/1/3 0003.
 */
@RestController
@RequestMapping("api")
public class CompanyService {
    @Autowired
    ICompanyService companyService;
    @Autowired
    IRelationTableService relationTableService;

    @RequestMapping(value = "/rpcCompany/selectAll", method = RequestMethod.GET)
    public List<Company> selectAll(){
        return  companyService.selectList(new EntityWrapper<Company>());
    }

    @RequestMapping(value = "/selectMap/{companyId}", method = RequestMethod.GET)
    public Map<String, Object> selectMap(Long companyId){
        return companyService.selectMap(new EntityWrapper<Company>().eq("company_id",companyId));
    }

    /**
     * 清除 公司数据
     * @param companyCode 公司编号
     * @return -1 删除失败,有异常
     */
    @RequestMapping(value = "/company/cleanUpCompanyData", method = RequestMethod.POST, produces="application/json")
    public Integer cleanUpCompanyData(@RequestParam("companyCode") String companyCode){
        if(companyCode==null||companyCode.length()<1){
            return -1;
        }
        return relationTableService.cleanUpCompanyData(companyCode);
    }

}
