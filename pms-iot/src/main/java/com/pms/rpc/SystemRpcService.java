package com.pms.rpc;

import com.pms.entity.UserInfo;
import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.Map;


/**
 * Created by hm on 2018/1/15.
 */

@FeignClient("pms-system")
@RequestMapping
public interface SystemRpcService {

    @RequestMapping(value = "/api/user/username/{username}/{companyCode}", method = RequestMethod.GET, produces="application/json")
    public UserInfo getUserByUsername(@PathVariable("username") String username, @PathVariable("companyCode")String companyCode);
}
