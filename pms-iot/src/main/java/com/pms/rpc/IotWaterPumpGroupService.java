package com.pms.rpc;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.pms.service.IIotWaterPumpGroupService;
import com.pms.service.IWoGiaWpAttrService;
import com.pms.utils.WoGiaFiledConstant;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author asus
 * @since 2018-07-18
 */
@RestController
@RequestMapping("api")
public class IotWaterPumpGroupService  {
    Logger logger = LoggerFactory.getLogger(getClass());
    @Autowired
    IIotWaterPumpGroupService waterPumpGroupService;
    @Autowired
    IWoGiaWpAttrService woGiaWpAttrService;

    @RequestMapping(value = "/iot/readDataAnalysisCurve",method = RequestMethod.POST)
    public List<Map<String,Object>> readDataAnalysisCurve(@RequestParam Map<String,Object> paramMap){
       return waterPumpGroupService.readDataAnalysisCurve(paramMap,null);
    }
    @RequestMapping(value = "/iot/readDataAnalysisCurve_new_xToy",method = RequestMethod.POST,produces="application/json")
    public String readDataAnalysisCurve_new_xToy(@RequestParam Map<String,Object> paramMap){
        List<Map<String,Object>> data = null;
        String valueType = paramMap.get("valueType")+"";
        paramMap.remove("valueType");
        if("2".equals(valueType)){
            data = waterPumpGroupService.readDataAnalysisCurve(paramMap,2);
        }
        if("3".equals(valueType)){
            data = waterPumpGroupService.readDataAnalysisCurve(paramMap,3);
        }
        data = waterPumpGroupService.readDataAnalysisCurve(paramMap,null);
        return JSONArray.toJSONString(data);
    }


    @RequestMapping(value = "/iot/selectGroupMaps",method = RequestMethod.POST,produces="application/json")
    public List<Map<String,Object>> selectGroupMaps(@RequestParam Map<String,Object> paramMap){
        return waterPumpGroupService.selectGroupMaps(paramMap);
    }
    @RequestMapping(value = "/iot/selectReportTimes",method = RequestMethod.POST,produces="application/json")
    public List<String> selectReportTimes(@RequestParam Map<String,Object> paramMap){
        return waterPumpGroupService.selectReportTimes(paramMap);
    }
    @RequestMapping(value = "/iot/selectReportTimesCurveMap",method = RequestMethod.POST,produces="application/json")
    public List<Map<String,Object>> selectReportTimesCurveMap(@RequestParam Map<String,Object> paramMap){
        return waterPumpGroupService.selectReportTimesCurveMap(paramMap);
    }

    @RequestMapping(value = "/iot/readWoGiaCurveColumn",method = RequestMethod.POST,produces="application/json")
    public String readWoGiaCurveColumn(@RequestParam("monitorParam") Integer monitorParam){
        if(monitorParam!= null){
            if(monitorParam == 3){
                return WoGiaFiledConstant.INLET_PRESSURE;
            }
            if(monitorParam == 4){
                return WoGiaFiledConstant.OUT_PRESSURE;
            }
        }
        return  null;
    }

    @RequestMapping(value = "/iot/selectWoGiaAllColumnByTableName",method = RequestMethod.POST,produces="application/json")
    public  List<String> selectWoGiaAllColumnByTableName(@RequestParam Map<String,String> paramMap){
        return woGiaWpAttrService.selectAllColumnByTableName(paramMap);
    }

    /**
     * 查询每个设备的  上报时间序列
     * @param paramList
     * @return
     */
    @RequestMapping(value = "/iot/readWogia2DataAnalysisCurveTime",method = RequestMethod.POST,produces="application/json")
    public  List<String> readWogia2DataAnalysisCurveTime(@RequestParam("paramList") List<String> paramList){
        List<Map<String,String>> param = new ArrayList<Map<String,String>>();
        for(String str:paramList){
            Map<String,String> strMap = ( Map<String,String>)JSONObject.parse(str);
            param.add(strMap);
        }
        LinkedHashSet<String>  list =woGiaWpAttrService.readWogia2DataAnalysisCurveTime(param);
        List<String> returnList =  new ArrayList<String>();
        returnList.addAll(list==null?new LinkedHashSet<>():list);
        return returnList;
    }

    /**
     * 查询 每个设备的  最大 最小 平均 压力
     * @param paramList
     * @return
     */
    @RequestMapping(value = "/iot/readWogia2DataAnalysisCount",method = RequestMethod.POST,produces="application/json")
    public List<Map<String,Object>>  readWogia2DataAnalysisCount(@RequestParam("paramList") List<String> paramList){
        List<Map<String,String>> param = new ArrayList<Map<String,String>>();
        for(String str:paramList){
            Map<String,String> strMap = ( Map<String,String>)JSONObject.parse(str);
            param.add(strMap);
        }
        return woGiaWpAttrService.readWogia2DataAnalysisCount(param);
    }

    /**
     * 查询每个设备的  上报时间序列
     * @param paramStr 单个
     * @return
     */
    @RequestMapping(value = "/iot/readWogia2DataAnalysisCurveTime_single",method = RequestMethod.POST,produces="application/json")
    public  List<String> readWogia2DataAnalysisCurveTime_single(@RequestParam("paramStr") String paramStr){
        List<Map<String,String>> param = new ArrayList<Map<String,String>>();
        Map<String,String> strMap = ( Map<String,String>)JSONObject.parse(paramStr);
        param.add(strMap);
        LinkedHashSet<String>  list =woGiaWpAttrService.readWogia2DataAnalysisCurveTime(param);
        List<String> returnList =  new ArrayList<String>();
        returnList.addAll(list==null?new LinkedHashSet<>():list);
        return returnList;
    }

    /**
     * 查询 每个设备的  最大 最小 平均 压力
     * @param paramList
     * @return
     */
    @RequestMapping(value = "/iot/readWogia2DataAnalysisCount_single",method = RequestMethod.POST,produces="application/json")
    public List<Map<String,Object>>  readWogia2DataAnalysisCount_single(@RequestParam("paramStr")String paramList){
        List<Map<String,String>> param = new ArrayList<Map<String,String>>();
            Map<String,String> strMap = ( Map<String,String>)JSONObject.parse(paramList);
            param.add(strMap);
        return woGiaWpAttrService.readWogia2DataAnalysisCount(param);
    }
}
