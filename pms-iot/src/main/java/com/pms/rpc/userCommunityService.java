package com.pms.rpc;

import com.pms.entity.BaseUserInfo;
import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.Map;

/**
 * Created by Administrator on 2017/12/28 0028.
 */
@FeignClient("pms-system")
@RequestMapping("api")
public interface userCommunityService {


    @RequestMapping(value = "/user/getUserCommunity/{userId}/{userCommunityId}",method = RequestMethod.POST, produces="application/json")
    public @ResponseBody
    Map<String, Object> getUserCommunity(@PathVariable("userId")Long userId, @PathVariable("userCommunityId")Long userCommunityId);

    }
