package com.pms.rpc;

import com.pms.authority.PermissionInfo;
import com.pms.entity.BaseUserInfo;
import com.pms.entity.UserInfo;
import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

/**
 * Created by Administrator on 2017/12/28 0028.
 */
@FeignClient("pms-system")
@RequestMapping("api")
public interface UserService {
    @RequestMapping(value = "/user/seleteById/{id}",method = RequestMethod.GET, produces="application/json")
    public  BaseUserInfo getUserByUserId(@PathVariable("id")Long id);

    @RequestMapping(value = "/user/selectbyPipUser/{id}",method = RequestMethod.GET, produces="application/json")
    public @ResponseBody
    UserInfo selectbyPipUser(@PathVariable("id")Long id);

    @RequestMapping(value = "/user/un/getApiPermission/{userId}/{menuType}/api", method = RequestMethod.GET)
    Map<String,Object> getApiPermission(@PathVariable("userId") Long userId, @PathVariable("menuType")Integer menuType,@RequestParam("title")String title,@RequestParam("permissionsType") Integer permissionsType,@RequestParam("method")String method);

}
