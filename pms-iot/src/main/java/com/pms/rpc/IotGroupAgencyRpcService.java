package com.pms.rpc;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.pms.entity.Company;
import com.pms.entity.GroupIotAgency;
import com.pms.service.ICompanyService;
import com.pms.service.IGroupIotAgencyService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Map;

/**
 * Created by Administrator on 2018/1/3 0003.
 */
@RestController
@RequestMapping("api")
public class IotGroupAgencyRpcService {
    @Autowired
    IGroupIotAgencyService iotGroupAgencyService;

    @RequestMapping(value = "/del/delGroupAgencyByRoleId/{roleId}", method = RequestMethod.GET)
    public void delGroupAgencyByRoleId(@PathVariable("roleId") Long roleId){
        iotGroupAgencyService.delete(new EntityWrapper<GroupIotAgency>().eq("group_id",roleId));
    }


    @RequestMapping(value = "/getGroupAgencyByRoleIdList/{groupId}",method = RequestMethod.POST, produces="application/json")
    public List<Map<String,Object>> getGroupAgencyByRoleIdList(@PathVariable("groupId")Long groupId){
        List<Map<String,Object>> list= iotGroupAgencyService.selectMaps(new EntityWrapper<GroupIotAgency>().eq("group_id",groupId));
        return  list;
    }


}
