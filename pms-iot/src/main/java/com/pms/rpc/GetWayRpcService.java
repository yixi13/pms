package com.pms.rpc;

import com.pms.entity.UserInfo;
import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.Map;


/**
 * Created by hm on 2018/1/15.
 */

@FeignClient("pms-gateway")
@RequestMapping("gateway")
public interface GetWayRpcService {
    /**
     * 验证用户名 token 是否匹配
     * @return
     */
    @RequestMapping(value = "/user/getValidateUserToken", method = RequestMethod.POST, produces="application/json")
    public Map<String,Boolean> validateUserToken(@RequestParam Map<String,String> map);
}
