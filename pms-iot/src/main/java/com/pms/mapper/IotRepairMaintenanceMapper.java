package com.pms.mapper;
import com.pms.entity.IotRepairDeclare;
import com.pms.entity.IotRepairMaintenance;
import com.baomidou.mybatisplus.mapper.BaseMapper;

import java.util.List;
import java.util.Map;

/**
 * 
 * 
 * @author lk
 * @email 307169223@qq.com
 * @date 2018-08-20 09:57:14
 */
public interface IotRepairMaintenanceMapper extends BaseMapper<IotRepairMaintenance> {
    public IotRepairMaintenance seteleByMaintenance(Long repairMaintenanceId);
    List<IotRepairMaintenance> selectByPageList(Map<String,Object> map);

    int   countByPageMap(Map<String,Object> map);

    List<IotRepairMaintenance> selectByPageMap(Map<String,Object> map);
}
