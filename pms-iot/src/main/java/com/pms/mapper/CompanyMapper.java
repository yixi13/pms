package com.pms.mapper;

import com.pms.entity.Company;
import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
  * 水泵所属公司表 Mapper 接口
 * </p>
 *
 * @author lk
 * @since 2017-12-25
 */
public interface CompanyMapper extends BaseMapper<Company> {

}