package com.pms.mapper;
import com.baomidou.mybatisplus.mapper.Wrapper;
import com.pms.entity.EqCamera;
import com.baomidou.mybatisplus.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.session.RowBounds;

import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * 
 * 
 * @author zyl
 * @email 517770986@qq.com
 * @date 2018-03-15 11:21:02
 */
public interface EqCameraMapper extends BaseMapper<EqCamera> {
    //查询当前过期的摄像头
    List<EqCamera> findEqCameraByExpireTime(@Param("nowTime")Date nowTime, @Param("pageSize")int pageSize);

    List<Map<String, Object>> selectByISMapsPage(RowBounds rowBounds, @Param("ew") Wrapper<EqCamera> wrapper);

}
