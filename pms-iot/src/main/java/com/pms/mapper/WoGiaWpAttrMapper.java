package com.pms.mapper;

import org.apache.ibatis.annotations.Param;

import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;

/**
 * <p>
  * 沃佳水泵数据 mapper类
 * </p>
 *
 * @author asus
 * @since 2018-04-18
 */
public interface WoGiaWpAttrMapper{
    /**
     * 查询最大的排序
     * @return
     */
    List<Map<String,Object>> selectAttrByTablenameAndColumn(Map<String,String> map);
    /**
     * 查询表名是否存在
     * @return
     */
    Integer selectTableIsExist(String tablename);

    /**
     * 查询列名
     * @param map
     * @return
     */
    List<String> selectAllColumnByTableName(Map<String,String> map);


    Map<String,Object> readRealTimeDeviceData(Map<String,String> paramMap);

    /**
     * 查询所有表名
     * @return
     */
    List<String>  readWogia2TableNameList();

    /**
     * 查询 每个设备的  最大 最小 平均 压力
     * @param paramList
     * @return
     */
    List<Map<String,Object>>  readWogia2DataAnalysisCount( List<Map<String,String>> paramList);

    /**
     * 查询表中 数据条数
     * @param tablename
     * @return
     */
    Integer selectCountTableDataNum(@Param("tablename") String tablename);

    /**
     * 查询每个设备的  上报时间序列
     * @param paramList
     * @return
     */
    LinkedHashSet<String> readWogia2DataAnalysisCurveTime(List<Map<String,String>> paramList);

}