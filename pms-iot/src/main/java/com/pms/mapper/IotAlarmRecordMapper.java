package com.pms.mapper;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.pms.entity.IotAlarmRecord;
import com.baomidou.mybatisplus.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;

import javax.websocket.server.PathParam;
import java.util.List;
import java.util.Map;

/**
 * <p>
  * 设备上传数据 Mapper 接口
 * </p>
 *
 * @author ljb
 * @since 2018-08-20
 */
public interface IotAlarmRecordMapper extends BaseMapper<IotAlarmRecord> {

    List<Map<String,Object>> selectAlarmRecordRecordMapsPage(
            Page<Map<String, Object>> page,@Param("ew") EntityWrapper<Map<String, Object>> ew);


    void deleteBatch(@Param("alarmIds") String alarmIds);

    Map<String,Object> selectApiAlarmRecord(@Param("alarmId")String alarmId);

    int selectNoReadAlarmRecordCount(@Param("ew") EntityWrapper<Map<String, Object>> ew);
}