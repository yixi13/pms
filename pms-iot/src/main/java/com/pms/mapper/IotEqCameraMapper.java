package com.pms.mapper;
import com.pms.entity.IotEqCamera;
import com.baomidou.mybatisplus.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;

import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * 
 * 
 * @author lk
 * @email 307169223@qq.com
 * @date 2018-08-21 13:52:06
 */
public interface IotEqCameraMapper extends BaseMapper<IotEqCamera> {
    //查询当前过期的摄像头
    List<IotEqCamera> findEqCameraByExpireTime(@Param("nowTime") Date nowTime, @Param("pageSize") int pageSize);

    Map<String,Object> readHouseCamera(@Param("houseId") Long houseId);

    public  List<IotEqCamera> selectByList(@Param("agencyId") Long agencyId);

    List<IotEqCamera> selectByEqCameraAndCompanyCodeAll(Map<String,Object> map );

    int countByEqCameraAndCompanyCodeAll (Map<String,Object> map );
}
