package com.pms.mapper;
import com.pms.entity.EquipmentRepairRecord;
import com.baomidou.mybatisplus.mapper.BaseMapper;
/**
 * 
 *
 * @author lk
 * @email
 * @date 2018-04-25 14:26:06
 */
public interface EquipmentRepairRecordMapper extends BaseMapper<EquipmentRepairRecord> {
	
}
