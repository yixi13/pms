package com.pms.mapper;
import com.pms.entity.IotCardRechargeHistoricalRecords;
import com.baomidou.mybatisplus.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 
 * 
 * @author lk
 * @email 307169223@qq.com
 * @date 2018-08-20 09:57:14
 */
public interface IotCardRechargeHistoricalRecordsMapper extends BaseMapper<IotCardRechargeHistoricalRecords> {

    List<IotCardRechargeHistoricalRecords> getListByData(@Param("groupId")Long groupId);
}
