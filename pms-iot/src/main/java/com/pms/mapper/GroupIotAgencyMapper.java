package com.pms.mapper;
import com.pms.entity.GroupIotAgency;
import com.baomidou.mybatisplus.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;

/**
 * 
 * 
 * @author ljb
 * @email 517770986@qq.com
 * @date 2018-07-31 10:28:59
 */
public interface GroupIotAgencyMapper extends BaseMapper<GroupIotAgency> {
    /**
     *  根据角色id 查询角色 拥有的机构权限
     * @param groupId 角色id
     * @return
     */
   String getAgencyIdsStrByGroupId(@Param("groupId")String groupId);
}
