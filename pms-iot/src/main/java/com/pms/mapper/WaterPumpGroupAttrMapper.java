package com.pms.mapper;

import com.pms.entity.WaterPumpGroupAttr;
import com.baomidou.mybatisplus.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * <p>
  *  Mapper 接口
 * </p>
 *
 * @author asus
 * @since 2018-01-10
 */
public interface WaterPumpGroupAttrMapper extends BaseMapper<WaterPumpGroupAttr> {
    public int insertKafkaData(String sql);
    public int insertKafkaDataList(List<String> sqlList);
    List<Map<String,Object>> selectByDeviceNameAndReportTimeFormat(@Param("deviceName") String deviceName, @Param("startTimeStr")String startTimeStr,@Param("endTimeStr")String endTimeStr);
    public WaterPumpGroupAttr selectListByAttr(String deviceName);

    List<Map<String,Object>> selectByDeviceNameAndReportTime(@Param("deviceName") String deviceName, @Param("startTime")Long startTime,@Param("endTime")Long endTime);
    /**  查询随时变换的数据*/
    List< List<Map<String,Object>>> selectAttrRecordByFuction(@Param("deviceName")String deviceName, @Param("startTime")Long startTime,@Param("endTime")Long endTime);
    /**  查询固定时间采集的数据*/
    List<Map<String,Object>> readWpGroupRecordByNameAndTime(@Param("deviceName") String deviceName, @Param("startTime")Long startTime,@Param("endTime")Long endTime);
}