package com.pms.mapper;
import com.baomidou.mybatisplus.mapper.Wrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.pms.entity.IotRepairDeclare;
import com.baomidou.mybatisplus.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.session.RowBounds;

import java.util.List;
import java.util.Map;

/**
 * 
 * 
 * @author lk
 * @email 307169223@qq.com
 * @date 2018-08-20 09:57:14
 */
public interface IotRepairDeclareMapper extends BaseMapper<IotRepairDeclare> {

    List<Map<String,Object>> getByIotRepairDeclarePage(Map<String,Object> map);
   int countByIotRepairDeclarePage(Map<String,Object> map);
    Map<String,Object> getByIotRepairDeclare(Long repairMaintenanceId);
    List<IotRepairDeclare> selectByIotRepairDeclarePage(RowBounds rowBounds, @Param("ew") Wrapper<IotRepairDeclare> wrapper);

    public  List<IotRepairDeclare> selectByIotRepairDeclarePage(@Param("ew") Wrapper<IotRepairDeclare> wrapper);
}
