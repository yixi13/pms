package com.pms.mapper;

import com.pms.entity.WaterPumpGroup;
import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
  * 水泵表 Mapper 接口
 * </p>
 *
 * @author lk
 * @since 2017-12-25
 */
public interface WaterPumpGroupMapper extends BaseMapper<WaterPumpGroup> {

}