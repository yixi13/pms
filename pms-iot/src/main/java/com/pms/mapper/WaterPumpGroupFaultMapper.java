package com.pms.mapper;

import com.pms.entity.WaterPumpGroupFault;
import com.baomidou.mybatisplus.mapper.BaseMapper;

import java.util.List;

/**
 * <p>
  *  Mapper 接口
 * </p>
 *
 * @author asus
 * @since 2018-03-19
 */
public interface WaterPumpGroupFaultMapper extends BaseMapper<WaterPumpGroupFault> {
    List<WaterPumpGroupFault> selectByCommunityIdAndDate(long communityId,String dateStr);
}