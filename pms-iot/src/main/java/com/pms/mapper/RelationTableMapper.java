package com.pms.mapper;


import org.apache.ibatis.annotations.Param;

public interface RelationTableMapper {

    public void cleanUpCompanyAgencyData(@Param("companyCode")String companyCode);

    public void cleanUpCompanyWaterGroupData(@Param("companyCode") String companyCode);
}