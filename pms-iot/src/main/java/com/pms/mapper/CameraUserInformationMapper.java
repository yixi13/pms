package com.pms.mapper;
import com.pms.entity.CameraUserInformation;
import com.baomidou.mybatisplus.mapper.BaseMapper;

import java.util.List;

/**
 * 
 * 
 * @author zyl
 * @email 517770986@qq.com
 * @date 2018-03-15 11:21:02
 */
public interface CameraUserInformationMapper extends BaseMapper<CameraUserInformation> {
    List<CameraUserInformation> seleteBySum();
}
