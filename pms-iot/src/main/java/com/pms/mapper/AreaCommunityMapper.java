package com.pms.mapper;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.baomidou.mybatisplus.mapper.Wrapper;
import com.pms.entity.AreaCommunity;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * 
 * 
 * @author zyl
 * @email 517770986@qq.com
 * @date 2018-03-23 16:39:08
 */
public interface AreaCommunityMapper extends BaseMapper<AreaCommunity> {
    public List<Map<String,Object>> selectByAreaCommunity();

    List<Map<String, Object>> selectAreaGbAndCommunity(@Param("ew") Wrapper<AreaCommunity> wrapper);
}
