package com.pms.mapper;

import com.pms.entity.Community;
import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
  * 社区（小区）表 Mapper 接口
 * </p>
 *
 * @author lk
 * @since 2017-12-25
 */
public interface CommunityMapper extends BaseMapper<Community> {
    /**
     * 查询最大的排序
     * @return
     */
   Long selectMaxCommunitySort();
}