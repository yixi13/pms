package com.pms.mapper;
import com.pms.entity.IotCardRecharge;
import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.pms.entity.IotRepairMaintenance;

import java.util.List;
import java.util.Map;

/**
 * 
 * 
 * @author lk
 * @email 307169223@qq.com
 * @date 2018-08-20 09:57:14
 */
public interface IotCardRechargeMapper extends BaseMapper<IotCardRecharge> {
    public  IotCardRecharge findById(Long cardId);
    List<IotCardRecharge> selectByPageList(Map<String,Object> map);
    int countByListPage(Map<String,Object> map);
}
