package com.pms.mapper;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.pms.entity.WaterPumpHouse;

import java.util.List;
import java.util.Map;

/**
 * <p>
  *  Mapper 接口
 * </p>
 *
 * @author lk
 * @since 2017-12-27
 */
public interface WaterPumpHouseMapper extends BaseMapper<WaterPumpHouse> {
   public Integer selectMaxWaterPumpHouseSort(Long communityId);


   public List<Map<String,Object>> readWaterPumpHouseOutline(Map<String,Object> paramMap);

   public  Integer testSetVAR();

   public List<Map<String,Object>> readWaterPumpHouseOutlineHouseList(Map<String,Object> paramMap);
}