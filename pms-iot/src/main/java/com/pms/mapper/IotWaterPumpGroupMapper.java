package com.pms.mapper;

import com.baomidou.mybatisplus.mapper.Wrapper;
import com.pms.entity.IotWaterPumpGroup;
import com.baomidou.mybatisplus.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.session.RowBounds;

import java.util.List;
import java.util.Map;

/**
 * <p>
  *  Mapper 接口
 * </p>
 *
 * @author asus
 * @since 2018-07-18
 */
public interface IotWaterPumpGroupMapper extends BaseMapper<IotWaterPumpGroup> {
    /**
     * 查询 根据 水泵房id字符串 查询 水泵组id字符串
     * @param orgIds 水泵房id字符串
     * @return 水泵组id字符串
     */
    String getGroupIdsStrByHouseId(@Param("orgIds") String orgIds);


    /**
     * 查询数据用于 机构数展示
     * @param wrapper
     * @return
     */
    List<Map<String,Object>> selectMapList(@Param("ew") Wrapper<IotWaterPumpGroup> wrapper);

    /**
     * 删除 水泵组 通过水泵房id
     * @param orgIds 水泵房id
     */
    void delWaterPumpGroupByHouseIds(@Param("houseIds")String orgIds);

    /**
     * 查询 水泵组 实时(最新的一条)数据
     * @param rowBounds
     * @param wrapper
     * @return
     */
    List<Map<String,Object>> readRealTimeData(RowBounds rowBounds, @Param("ew") Wrapper<Map<String,Object>> wrapper);

    /**
     * 查询水泵房中的水泵组
     * @param map（houseId-水泵房id，communityId-小区id）
     * @return
     */
    List<Map<String,Object>> readHousePumpGroupDetail(Map<String,Object> map);

    /**
     * 查询 根据 水泵房id字符串 查询 水泵组id集合
     * @param orgIds 水泵房id字符串
     * @return 水泵组id字符串
     */
     List<Long> getGroupIdListByHouseId(@Param("orgIds") String orgIds);

    /**
     * 查询社区下的小泵数量统计
     * @param paramMap
     * @return Map(communityId-社区id,totalPumpNum-总小泵数量,onlinePumpNum-在线泵数量,nolinePumpNum-离线泵数量)
     */
    List<Map<String,Object>> getPumpNumByCommunityId(Map<String,Object> paramMap);
    /**
     * 查询水泵房下的小泵数量
     * @param paramMap
     * @return Map(houseId-水泵房id,totalPumpNum-总小泵数量,onlinePumpNum-在线泵数量,nolinePumpNum-离线泵数量)
     */
    List<Map<String,Object>> getPumpNumByHouseId(Map<String,Object> paramMap);

    /**
     * 查询水泵房中的水泵组
     * @param groupId  水泵组id
     * @return
     */
    List<Map<String,Object>> readPumpGroupDetail(@Param("groupId")Long groupId);

    /**
     * 查询水泵组的 压力统计
     * @param paramMap
     * @return
     */
    List<Map<String,Object>> readPumpGroupAttrCountPressure(RowBounds rowBounds ,@Param("ew")Wrapper<Map<String,Object>> wrapper,@Param("param")Map<String,Object> paramMap);

    List<Map<String,Object>> readDataAnalysisCurve(Map<String,Object> paramMap);
    /**
     * 查询 水泵组 实时(最新的一条)数据
     * @param wrapper
     * @return
     */
    List<Map<String,Object>> readRealTimeData(@Param("ew") Wrapper<Map<String,Object>> wrapper);

    /**
     * 关联删除 水泵组 通过水泵房id
     * @param orgIds 水泵房id
     */
    void joinDelWaterPumpGroupByHouseIds(@Param("houseIds")String orgIds);
    /**
     * 关联删除 水泵组 通过水泵房id
     * @param groupId 水泵组id
     */
    void joinDelWaterPumpGroupById(@Param("groupId")String groupId);


    public Long getGroupIdByCode(@Param("groupCode")String code);


    public List<Map<String,Object>> selectReportTimesCurveMap();

    /**
     * 查询 水泵组 不同设备状态个数
     * @param wrapper
     * @return
     */
    public Integer selectWaterPumpStatusCount(@Param("ew") Wrapper<Map<String,Object>> wrapper);

    /*==========曲线分析==============*/
    public List<Map<String,Object>> selectReportTimesCurveMap(Map<String,Object> map);
    public List<String> selectReportTimes(Map<String,Object> map);
    public List<Map<String,Object>> selectGroupMaps(Map<String,Object> map);
}