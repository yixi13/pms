package com.pms.mapper;

import com.pms.entity.IotAlarmType;
import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
  * 报警类型表 Mapper 接口
 * </p>
 *
 * @author ljb
 * @since 2018-08-20
 */
public interface IotAlarmTypeMapper extends BaseMapper<IotAlarmType> {

}