package com.pms.mapper;

import com.baomidou.mybatisplus.mapper.Wrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.pms.entity.IotAgency;
import com.baomidou.mybatisplus.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.session.RowBounds;

import java.util.List;
import java.util.Map;

/**
 * <p>
  *  Mapper 接口
 * </p>
 *
 * @author asus
 * @since 2018-07-18
 */
public interface IotAgencyMapper extends BaseMapper<IotAgency> {

    /**
     * 根据一级分区id查询 下级  二/三级分区id
     * @param agencyId
     * @return
     */
    List<Long> getSubordinateAgencyIds(@Param("agencyId") Long agencyId);

    /**
     * 查询 二供机构 管理模块 分页栏数据
     * @param rowBounds
     * @param wrapper
     * @param map
     * @return
     */
    List<Map<String,Object>> getAgencyManagePageData(RowBounds rowBounds, @Param("ew")Wrapper<Map<String,Object>> wrapper, @Param("map")Map<String,String> map);
    List<Map<String,Object>> getAgencyManagePageData1(RowBounds rowBounds, @Param("ew") Wrapper<Map<String,Object>> wrapper);

    /**
     * 查询机构栏展示数据
     * @param wrapper
     * @return
     */
    List<Map<String,Object>> selectMapList(@Param("ew") Wrapper<IotAgency> wrapper);

    /**
     * 删除 机构
     * @param orgIds 机构id
     */
    void delAgencyByIds(@Param("orgIds")String orgIds);

    /**
     * 查询电子地图 小区,水泵房  信息展示
     * @param param {level-(1小区,1水泵房) communityId-小区id  houseId-水泵房id}
     */
    List<Map<String,Object>> selectAgencyMapInfoByLevel(Map<String,Object> param);

    public   List<Long> selectByAgencyIdS(Map<String,Object> param);
}