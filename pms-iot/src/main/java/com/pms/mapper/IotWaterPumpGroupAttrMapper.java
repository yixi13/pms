package com.pms.mapper;

import com.baomidou.mybatisplus.mapper.Wrapper;
import com.pms.entity.IotWaterPumpGroupAttr;
import com.baomidou.mybatisplus.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.session.RowBounds;

import java.util.List;
import java.util.Map;

/**
 * <p>
  *  Mapper 接口
 * </p>
 *
 * @author asus
 * @since 2018-07-23
 */
public interface IotWaterPumpGroupAttrMapper extends BaseMapper<IotWaterPumpGroupAttr> {


    List<Map<String,Object>> readPumpGroupHistory(RowBounds rowBounds, @Param("ew") Wrapper<Map<String,Object>> wrapper);

    List<Map<String,Object>> readPumpGroupHistory(@Param("ew") Wrapper<Map<String,Object>> wrapper);

    public int insertKafkaData(String sql);
}