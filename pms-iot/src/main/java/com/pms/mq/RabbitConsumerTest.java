package com.pms.mq;

import com.pms.constant.MQConstant;
import com.pms.utils.KafkaMessageToMysql;
import com.pms.web.WaterPumpWebSocketUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.rabbit.annotation.RabbitHandler;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.amqp.RabbitProperties;
import org.springframework.stereotype.Component;
import com.rabbitmq.client.Channel;

import java.io.IOException;

/**
 * rabbit消息消费测试类
 * Created by Administrator on 2018/3/16.
 */
@Component
public class RabbitConsumerTest {
    @Autowired
    WaterPumpWebSocketUtil waterPumpWebSocketUtil;
    private Logger logger = LoggerFactory.getLogger(RabbitConsumerTest.class);
    /**
     * 订阅消息
     */
    @RabbitListener(queues = MQConstant.QUEUE_NAME_IOT_FAULT_SOCKET_PUSH)
    public void queue_process(String message,Channel channel){
        try {
            waterPumpWebSocketUtil.sendInfo("socket队列接收到消息为:" + message);
//            channel.basicAck(message.getMessageProperties().getDeliveryTag(), false);
        }catch (Exception ex){
//            channel.basicNack(false,false);
        }
        logger.info("socket队列接收到消息为:"+message);
    }
    /**
     * 订阅消息
     */
    @RabbitListener(queues = MQConstant.QUEUE_NAME_IOT_FAULT_SMS_PUSH)
    public void SMSQueuePushMessageSubscribe(String message){
        waterPumpWebSocketUtil.sendInfo("短信队列接收到消息为:"+message);
        logger.info("短信队列接收到消息为: "+message);
    }
}
