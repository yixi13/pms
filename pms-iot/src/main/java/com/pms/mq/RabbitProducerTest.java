package com.pms.mq;

import com.pms.constant.MQConstant;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.amqp.core.Message;

/**
 * rabbit消息发送测试类
 * Created by Administrator on 2018/3/16.
 */
@Component
public class RabbitProducerTest {
    @Autowired
    private RabbitTemplate rabbitTemplate;

    /**
     * 发送消息
     * @param message
     */
    public void SocketQueuePushMessage(Object message){
        this.rabbitTemplate.convertAndSend( MQConstant.QUEUE_NAME_IOT_FAULT_SOCKET_PUSH,message);
    }

    /**
     * 发送消息
     * @param message
     */
    public void SMSQueuePushMessage(Object message){
        this.rabbitTemplate.convertAndSend( MQConstant.QUEUE_NAME_IOT_FAULT_SMS_PUSH,message);
    }
}
