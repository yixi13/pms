package com.pms.service;

import com.pms.rpc.client.BillTableService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by Administrator on 2017/11/10 0010.
 */
@Service
public class ScheduledTaskService {

    @Autowired
    public BillTableService billTableService;

    private final Logger logger = LoggerFactory.getLogger(this.getClass());
    private static final SimpleDateFormat dateFormat = new SimpleDateFormat("HH:mm:ss");

    @Scheduled(fixedRate = 8000) //通过@Scheduled声明该方法是计划任务，使用fixedRate属性每隔固定时间执行
    private void reportCurrentTime(){
        System.out.println("每隔8秒执行一次 "+dateFormat.format(new Date()));
//        billTableService.addBillTableByTimer();
    }

    @Scheduled(cron = "0 07 20 ? * *" ) //使用cron属性可按照指定时间执行，本例指的是每天20点07分执行；
    //cron是UNIX和类UNIX(Linux)系统下的定时任务
    public void fixTimeExecution(){
        System.out.println("在指定时间 "+dateFormat.format(new Date())+" 执行");
    }
}
