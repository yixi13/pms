package com.pms.rpc.client;

import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * Created by Administrator on 2017/11/14 0014.
 */
@FeignClient("pms-estate")
@RequestMapping("api")
public interface BillTableService {
    @RequestMapping(value = "/billTable/addBillTables",method = RequestMethod.GET, produces="application/json")
    public void addBillTableByTimer();
}
