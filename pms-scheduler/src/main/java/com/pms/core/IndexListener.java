package com.pms.core;

import com.baomidou.mybatisplus.spring.MybatisSqlSessionFactoryBean;
import org.springframework.beans.factory.DisposableBean;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.config.ConfigurableListableBeanFactory;
import org.springframework.beans.factory.support.AbstractBeanFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.AbstractApplicationContext;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.annotation.WebListener;

/**
 * Created by Administrator on 2017/11/24 0024.
 */
@WebListener
public class IndexListener implements ServletContextListener ,InitializingBean, DisposableBean,Runnable {

    private static ApplicationContext applicationContext;
    private ConfigurableListableBeanFactory beanfactory;

    @Override
    public void contextInitialized(ServletContextEvent servletContextEvent) {
        System.out.print("启动开始了..........");
    }

    @Override
    public void contextDestroyed(ServletContextEvent servletContextEvent) {
//        Object msfb = SpringUtil.getBean("mybatisPlus");
        System.out.print("----------------");
//        SpringUtil su = new SpringUtil();
//        su.destroyBean("mybatisPlus");
//        Object mb = SpringUtil.getBean("mybatisPlus");
//        this.run();
//        try{
//            this.destroy();
//        }catch (Exception e){
//
//        }


    }

    @Override
    public void destroy() throws Exception {

    }

    @Override
    public void afterPropertiesSet() throws Exception {

    }

    @Override
    public void run() {
        this.beanfactory.destroySingletons();
    }
}
