package com.pms.rpc;


import com.pms.authority.PermissionInfo;
import com.pms.entity.BaseUserInfo;
import com.pms.entity.UserInfo;
import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;
import java.util.Map;

@FeignClient("pms-system")
@RequestMapping("api")
public interface IUserCommunityService {

    @RequestMapping(value = "/user/selectByuserCommunity/{userId}",method = RequestMethod.GET, produces="application/json")
    public  Map<String, Object> selectByuserCommunity(@PathVariable("userId")Long userId);
}
