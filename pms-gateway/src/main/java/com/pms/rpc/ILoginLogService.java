package com.pms.rpc;

import org.springframework.cloud.netflix.feign.FeignClient;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

@FeignClient("pms-system")
@RequestMapping("api")
public interface ILoginLogService {
    @RequestMapping(value="/loginLog/addLoginLog",method = RequestMethod.POST)
    void addLoginLog(@RequestParam("userId")Long userId, @RequestParam("nickName")String nickName,@RequestParam("explain")String explain);
}
