package com.pms.rpc;

import com.pms.entity.UserInfo;
import com.pms.jwt.JwtTokenUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by Administrator on 2018/1/17 0017.
 */
@RestController
@RequestMapping("gateway")
public class UserTokenService {
    Logger log = LoggerFactory.getLogger(getClass());
    @Autowired
    private JwtTokenUtil jwtTokenUtil;
/**
     * 验证 用户名 和 token
     * @return
     */
    @RequestMapping(value = "/user/getValidateUserToken",method = RequestMethod.POST, produces="application/json")
    public Map<String,Boolean> validateUserToken(@RequestParam Map<String, String> paramMap) {
        UserInfo currentUser = new UserInfo();currentUser.setUsername(paramMap.get("userName"));
        Boolean tag = jwtTokenUtil.validateToken(paramMap.get("token"),currentUser);
//        UserInfo currentUser = new UserInfo();currentUser.setUsername(userName);
//        return  jwtTokenUtil.validateToken(token,currentUser);
        Map<String,Boolean> returnMap = new HashMap<String,Boolean>();
        returnMap.put("value",tag);
        return returnMap;
    }
}
