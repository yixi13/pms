package com.pms.rpc;

import com.pms.entity.LogInfo;
import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@FeignClient("pms-system")
@RequestMapping("api")
public interface ILogService {
    @RequestMapping(value="/log/save",method = RequestMethod.POST)
     void saveLog(LogInfo info);
}
