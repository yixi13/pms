package com.pms.rpc;
import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.web.bind.annotation.*;

import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Set;

@FeignClient("pms-pip")
@RequestMapping("api")
public interface OrgService {

    @RequestMapping(value = "/kyOrganization/getOrgMeasure", method = RequestMethod.POST)
    List<Map<String,Object>> getOrgMeasure(@RequestParam("orgMaxId")Integer orgMaxId, @RequestParam("orgId")Integer orgId, @RequestParam("startDate")Date startDate, @RequestParam("endDate")Date endDate);
    @RequestMapping(value = "/kyOrganization/getEquipmentMeasure", method = RequestMethod.POST)
    Set<Map<String,Object>> getEquipmentMeasure(@RequestParam("orgMaxId")Integer orgMaxId, @RequestParam("equIds")String equIds, @RequestParam("startDate")Date startDate, @RequestParam("endDate")Date endDate, @RequestParam("param")String param);

}
