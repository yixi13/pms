package com.pms.rpc;
import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.util.Map;

@FeignClient("pms-iot")
@RequestMapping("api")
public interface IComService {

    @RequestMapping(value = "/selectByCommunityId/{communityId}", method = RequestMethod.POST)
    public Map<String, Object> selectByCommunityId(@PathVariable("communityId") Long communityId);

}
