package com.pms.rpc;
import com.pms.entity.MemberInfo;
import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;


@FeignClient("pms-system")
@RequestMapping("api")
public interface IMemberService {
    @RequestMapping(value = "/member/memberId/{memberId}", method = RequestMethod.GET)
    MemberInfo queryBymember(@PathVariable("memberId") Long memberId);

    @RequestMapping(value = "/member/{phone}", method = RequestMethod.GET)
    MemberInfo queryByphone(@PathVariable("phone") String phone);


}
