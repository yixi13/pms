//package com.pms.rpc;
//
//import com.netflix.zuul.context.RequestContext;
//import com.pms.entity.UserInfo;
//import com.pms.jwt.JwtTokenUtil;
//import org.slf4j.Logger;
//import org.slf4j.LoggerFactory;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.beans.factory.annotation.Value;
//import org.springframework.web.bind.annotation.*;
//
//import javax.servlet.http.HttpServletRequest;
//import javax.servlet.http.HttpSession;
//
///**
// * Created by Administrator on 2018/1/17 0017.
// */
//@RestController
//@RequestMapping("gateway")
//public class UserInfoService {
//    Logger log = LoggerFactory.getLogger(getClass());
//    @Autowired
//    private IUserService userService;
//    @Autowired
//    private ILogService logService;
//
//    @Value("${gate.ignore.startWith}")
//    private String startWith;
//    @Value("${gate.ignore.contain}")
//    private String contain;
//    @Value("${gate.oauth.prefix}")
//    private String oauthPrefix;
//    @Value("${gate.jwt.header}")
//    private String tokenHeader;
//    @Value("${zuul.prefix}")
//    private String zuulPrefix;
//    @Autowired
//    private JwtTokenUtil jwtTokenUtil;
//
//    @RequestMapping(value = "/user/getCurrentUser",method = RequestMethod.GET, produces="application/json")
//    public  UserInfo getCurrentUser() {
//        RequestContext ctx = RequestContext.getCurrentContext();
//        HttpSession httpSession = ctx.getRequest().getSession();
//        HttpServletRequest request = ctx.getRequest();
//        String authToken = request.getHeader(this.tokenHeader);
//        if (authToken != null) {
//            String username = jwtTokenUtil.getUsernameFromToken(authToken);
//            log.info("checking authentication " + username);
//            if (username != null ) {
//                UserInfo userDetails = userService.getUserByUsername(username);
//                if (jwtTokenUtil.validateToken(authToken, userDetails)) {
//                    return userDetails;
//                }
//            }
//            return null;
//        }else{
//            return null;
//        }
//    }
//}
