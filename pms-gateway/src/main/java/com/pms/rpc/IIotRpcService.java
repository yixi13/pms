package com.pms.rpc;

import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.web.bind.annotation.*;

import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;

@FeignClient("pms-iot")
@RequestMapping("api")
public interface IIotRpcService {
    @RequestMapping(value = "/iot/readDataAnalysisCurve",method = RequestMethod.POST)
    public List<Map<String,Object>> readDataAnalysisCurve(@RequestParam Map<String,Object> paramMap);


    @RequestMapping(value = "/iot/selectGroupMaps",method = RequestMethod.POST,produces="application/json")
    public List<Map<String,Object>> selectGroupMaps(@RequestParam Map<String,Object> paramMap);
    @RequestMapping(value = "/iot/selectReportTimes",method = RequestMethod.POST,produces="application/json")
    public List<String> selectReportTimes(@RequestParam Map<String,Object> paramMap);
    @RequestMapping(value = "/iot/selectReportTimesCurveMap",method = RequestMethod.POST,produces="application/json")
    public List<Map<String,Object>> selectReportTimesCurveMap(@RequestParam Map<String,Object> paramMap);

    @RequestMapping(value = "/iot/readWoGiaCurveColumn",method = RequestMethod.POST,produces="application/json")
    public String readWoGiaCurveColumn(@RequestParam("monitorParam") Integer monitorParam);

    @RequestMapping(value = "/iot/selectWoGiaAllColumnByTableName",method = RequestMethod.POST,produces="application/json")
    public  List<String> selectWoGiaAllColumnByTableName(@RequestParam Map<String,String> paramMap);

    @RequestMapping(value = "/iot/readWogia2DataAnalysisCurveTime",method = RequestMethod.POST,produces="application/json")
    public List<String> readWogia2DataAnalysisCurveTime(@RequestParam("paramList")  List<String> mapList);
    /**
     * 查询 每个设备的  最大 最小 平均 压力
     * @param paramList
     * @return
     */
    @RequestMapping(value = "/iot/readWogia2DataAnalysisCount",method = RequestMethod.POST,produces="application/json")
    public List<Map<String,Object>>  readWogia2DataAnalysisCount(@RequestParam("paramList") List<String> paramList);

    @RequestMapping(value = "/iot/readWogia2DataAnalysisCurveTime_single",method = RequestMethod.POST,produces="application/json")
    public  List<String> readWogia2DataAnalysisCurveTime_single(@RequestParam("paramStr")String paramStr);
    /**
     * 查询 每个设备的  最大 最小 平均 压力
     * @param paramList
     * @return
     */
    @RequestMapping(value = "/iot/readWogia2DataAnalysisCount_single",method = RequestMethod.POST,produces="application/json")
    public List<Map<String,Object>>  readWogia2DataAnalysisCount_single(@RequestParam("paramStr")String paramList);



    }
