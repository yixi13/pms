package com.pms.rpc;
import com.pms.authority.PermissionInfo;
import com.pms.entity.BaseUserInfo;
import com.pms.entity.UserInfo;
import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

@FeignClient("pms-system")
@RequestMapping("api")
public interface IUserService {
    @RequestMapping(value = "/user/username/{username}/{companyCode}", method = RequestMethod.GET)
    UserInfo getUserByUsername(@PathVariable("username") String username,@PathVariable("companyCode")String companyCode);

    @RequestMapping(value = "/user/getListByPhone/{phone}",method = RequestMethod.POST, produces="application/json")
    public @ResponseBody
    int getListByPhone(@PathVariable("phone")String phone);

    @RequestMapping(value = "/user/un/{username}/permissions/{menuType}/{companyCode}", method = RequestMethod.GET)
    List<PermissionInfo> getPermissionByUsername(@PathVariable("username") String username,@PathVariable("menuType")Integer menuType,@PathVariable("companyCode")String companyCode);

    @RequestMapping(value = "/permissions", method = RequestMethod.GET)
    List<PermissionInfo> getAllPermissionInfo();

    @RequestMapping(value = "/user/seleteByuser/{username}",method = RequestMethod.GET, produces="application/json")
    public @ResponseBody
    BaseUserInfo seleteByuser(@PathVariable("username")String username);

    @RequestMapping(value = "/user/getByuseraccount/{username}/{companyCode}",method = RequestMethod.POST, produces="application/json")
    public @ResponseBody
    BaseUserInfo getByuseraccount(@PathVariable("username")String username,@PathVariable("companyCode")String companyCode);

    /**
     * 查询所有
     * @return
     */
    @RequestMapping(value = "/getAll",method = RequestMethod.GET)
    @ResponseBody
    public List<Map<String, Object>>getAll();


    @RequestMapping(value = "/user/getCurrentUserInformation", method = RequestMethod.GET, produces = "application/json")
    public Map<String,Object> getCurrentUserInformation();



    @RequestMapping(value = "/user/selectByCompanyCode/{phone}",method = RequestMethod.POST, produces="application/json")
    public @ResponseBody
    List<Map<String,Object>> selectByCompanyCode(@PathVariable("phone")String phone);


    @RequestMapping(value = "/user/updataPhoneCompanyCodePassWord/{phone}/{companyCode}/{passWord}", method = RequestMethod.POST, produces = "application/json")
    public
    @ResponseBody boolean updataPhoneCompanyCodePassWord(@PathVariable("phone")String  phone,@PathVariable("companyCode")String  companyCode,@PathVariable("passWord")String passWord);


    @RequestMapping(value = "/user/un/{username}/permissions/{companyCode}/api", method = RequestMethod.GET)
    List<PermissionInfo> getApiPermissionByUsername(@PathVariable("username") String username,@PathVariable("companyCode")String companyCode);

    @RequestMapping(value = "/user/un/getApiPermission/{userId}/{menuType}/api", method = RequestMethod.GET)
    Map<String,Object> getApiPermission(@PathVariable("userId") Long userId, @PathVariable("menuType")Integer menuType, @RequestParam("title")String title, @RequestParam("permissionsType") Integer permissionsType, @RequestParam("method")String method);

    /**
     * 提供给超级管理员所用
     * @param username
     * @return
     */
    @RequestMapping(value = "/user/getBySuperUser/{username}",method = RequestMethod.POST, produces="application/json")
    public @ResponseBody
    BaseUserInfo getBySuperUser(@PathVariable("username")String username);
    /**
     * 提供给超级管理员所用
     * @param username
     * @return
     */
    @RequestMapping(value = "/user/getSuperUserByUsername/{username}", method = RequestMethod.GET)
    UserInfo getSuperUserByUsername(@PathVariable("username") String username);

    /**
     * 提供给超级管理员所用
     * @param username
     * @return
     */
    @RequestMapping(value = "/user/un/{username}/getPermissionBysuperAdmin", method = RequestMethod.GET)
    List<PermissionInfo> getPermissionBysuperAdmin(@PathVariable("username") String username);


    @RequestMapping(value = "/user/getByCompany/{companyCode}",method = RequestMethod.POST, produces="application/json")
    public @ResponseBody
    Map<String,Object> getByCompany(@PathVariable("companyCode")String companyCode);

}
