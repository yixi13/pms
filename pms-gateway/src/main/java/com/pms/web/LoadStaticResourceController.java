package com.pms.web;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.pms.controller.BaseController;
import com.pms.entity.UserInfo;
import com.pms.exception.R;
import com.pms.exception.RRException;
import com.pms.rpc.IIotRpcService;
import com.pms.rpc.IUserService;
import com.pms.rpc.OrgService;
import com.pms.util.DateUtil;
import com.pms.validator.Assert;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.servlet.http.HttpServletRequest;
import java.util.*;

/**
 * Created by Administrator on 2018/4/9.
 */
@Controller
public class LoadStaticResourceController extends BaseController {

    @Autowired
    OrgService orgService;
    @Autowired
    IIotRpcService iotRpcService;

    @RequestMapping(value ="/getResourceCurveView",method = RequestMethod.GET)
    public String getUsers(HttpServletRequest request, Model model, String deviceName) {
        if(StringUtils.isBlank(deviceName)){
            return "";
        }
        String url = request.getRequestURL().toString();
        url=url.substring(0,url.indexOf("/getResourceCurveView"));
        model.addAttribute("reqHost",url );
        model.addAttribute("deviceName", deviceName);
        return "curveView";
    }

    /**
     * 分区曲线
     * @param model
     * @param type
     * @param orgMaxId
     * @param orgId
     * @param startTime
     * @param endTime
     * @return
     */
    @RequestMapping(value ="pip/orgMeasure",method = RequestMethod.GET)
    public String orgMeasure(Model model,Integer type,Integer orgMaxId,Integer orgId,String startTime,String endTime,Long userId) {
//        Assert.isNull(type,"数据类型不能为空");
//        Assert.isNull(orgId,"分区id不能为空");
//        Assert.isNull(orgMaxId,"系统id不能为空");
//        Assert.isNull(startTime,"开始时间不能为空");
//        Assert.isNull(endTime,"结束时间不能为空");
        Map<String,Object> paramMap = new HashMap<String,Object>();
        paramMap.put("code",400);
        if (type==null){
            paramMap.put("msg","数据类型不能为空");
            model.addAttribute("data",paramMap);
            return "measure";
        }
        if (orgMaxId==null){
            paramMap.put("msg","系统id不能为空");
            model.addAttribute("data",paramMap);
            return "measure";
        }
        if (orgId==null){
            paramMap.put("msg","分区id不能为空");
            model.addAttribute("data",paramMap);
            return "measure";
        }
        if (StringUtils.isEmpty(startTime)){
            paramMap.put("msg","开始时间不能为空");
            model.addAttribute("data",paramMap);
            return "measure";
        }
        if (StringUtils.isEmpty(endTime)){
            paramMap.put("msg","结束时间不能为空");
            model.addAttribute("data",paramMap);
            return "measure";
        }
        if (userId==null){
            paramMap.put("msg","用户ID不能为空");
            model.addAttribute("data",paramMap);
            return "part";
        }
        Date startDate = DateUtil.stringToDate(startTime);
        Date endDate = DateUtil.stringToDate(endTime);
        List<Map<String,Object>> list = orgService.getOrgMeasure(orgMaxId,orgId,startDate,endDate);
        model.addAttribute("list",list);
        model.addAttribute("type",type);
        model.addAttribute("startTime",startTime);
        model.addAttribute("endTime",endTime);
        return "part";
    }

    /**
     * 测点曲线
     * @param model
     * @param type
     * @param orgMaxId
     * @param equIds
     * @param startTime
     * @param endTime
     * @return
     */
    @RequestMapping(value ="pip/equipmentMeasure",method = RequestMethod.GET)
    public String equipmentMeasure(Model model,Integer type,Integer orgMaxId,String equIds,String startTime,String endTime,Long userId) {
//        Assert.isNull(type,"数据类型不能为空");
//        Assert.isNull(equIds,"测点拼接字符串不能为空");
//        Assert.isNull(orgMaxId,"系统id不能为空");
//        Assert.isNull(startTime,"开始时间不能为空");
//        Assert.isNull(endTime,"结束时间不能为空");
        String param = null;
        Map<String,Object> paramMap = new HashMap<String,Object>();
        paramMap.put("code",400);
        if (type==null){
            paramMap.put("msg","数据类型不能为空");
            model.addAttribute("data",paramMap);
            return "measure";
        }
        if (orgMaxId==null){
            paramMap.put("msg","系统id不能为空");
            model.addAttribute("data",paramMap);
            return "measure";
        }
        if (StringUtils.isEmpty(equIds)){
            paramMap.put("msg","测点拼接字符串不能为空");
            model.addAttribute("data",paramMap);
            return "measure";
        }
        if (StringUtils.isEmpty(startTime)){
            paramMap.put("msg","开始时间不能为空");
            model.addAttribute("data",paramMap);
            return "measure";
        }
        if (StringUtils.isEmpty(endTime)){
            paramMap.put("msg","结束时间不能为空");
            model.addAttribute("data",paramMap);
            return "measure";
        }
        if (userId==null){
            paramMap.put("msg","用户ID不能为空");
            model.addAttribute("data",paramMap);
            return "measure";
        }
        if(type<1||type>5){
            paramMap.put("msg","数据类型不对");
            model.addAttribute("data",paramMap);
            return "measure";
        }
        switch (type) {
            case 1:param = "instantaneousFlow";break;
            case 2:param = "supplyVoltage";break;
            case 3:param = "gprsSignalIntensity";break;
            case 4:param = "pressure";break;
            case 5:param = "waterLevel";break;
        }
        Date startDate = DateUtil.stringToDate(startTime);
        Date endDate = DateUtil.stringToDate(endTime);
        Set<Map<String,Object>> set = orgService.getEquipmentMeasure(orgMaxId,equIds,startDate,endDate,param);
        Map<String,Object> map = new HashMap<>();
        map.put("type",type);
        map.put("set",set);
        map.put("startTime",startTime);
        map.put("endTime",endTime);
        model.addAttribute("data",map);
        return "measure";
    }

    @RequestMapping(value ="iot/readDataAnalysisCurveResource_old",method = {RequestMethod.POST,RequestMethod.GET})
    public String readDataAnalysisCurveResource(Model model,String waterPumpGroupIds,String startTime,String endTime,Integer monitorParam) {
        Assert.parameterIsNull(waterPumpGroupIds,"参数waterPumpGroupIds不能为空");
        Assert.parameterIsBlank(startTime,"起始时间不能为空");
        Assert.parameterIsBlank(startTime,"截止时间不能为空");
        Assert.parameterIsNull(monitorParam,"请选择监控参数");
        String readColumn = getReadColumnName(monitorParam);
        Assert.parameterIsBlank(readColumn,"该监控参数暂无数据");
        Date startDate = DateUtil.stringToDate(startTime);
        if(startDate==null){
            throw new RRException("起始时间格式有误",400);
        }
        Date endDate = DateUtil.stringToDate(endTime);
        if(endDate==null){
            throw new RRException("截止时间格式有误",400);
        }
        Map<String,Object> paramMap = new HashMap<String,Object>();
        paramMap.put("column",readColumn);
        paramMap.put("startTime",DateUtil.format(startDate,DateUtil.DATE_PATTERN.YYYY_MM_DD_HH_MM_SS));
        paramMap.put("endTime",DateUtil.format(endDate,DateUtil.DATE_PATTERN.YYYY_MM_DD_HH_MM_SS));
        if(waterPumpGroupIds!=null&&!waterPumpGroupIds.equals("-1")){
            paramMap.put("waterPumpGroupIds",waterPumpGroupIds);
        }
        paramMap.put("companyCode",getCurrentCompanyCode()==null?"20180101":getCurrentCompanyCode());
        List<Map<String,Object>> dataList = iotRpcService.readDataAnalysisCurve(paramMap);
        if(dataList==null){dataList= new ArrayList<Map<String,Object>>();}
        for(int x=0,lenth=dataList.size();x<lenth;x++){
            List<String> retimeFormatList = (List<String>) dataList.get(x).get("timeArr");
            if(retimeFormatList==null||retimeFormatList.isEmpty()){
                dataList.get(x).put("valueArr",new ArrayList<>());
            }
        }
        Map<String,Object> dataMap = new HashMap<String,Object>();
        dataMap.put("list",dataList);
        dataMap.put("startTime",startTime);
        dataMap.put("endTime",endTime);
//        dataMap.put("type",formatReadColumnName(monitorParam));
        dataMap.put("type",monitorParam);
        model.addAttribute("data",dataMap);
        return "waterPumpCurve";
    }
    private static String getReadColumnName(Integer monitorParam){
        if(monitorParam==null){return null;}
        switch (monitorParam){
            case 1: return null;// 电流
            case 2: return null;// 电压
            case 3: return "FORMAT(IFNULL(attr.SZGWYL,0.0),2) as value_";// 进水压力
            case 4: return "FORMAT(IFNULL(attr.pressureFeedback,0.0),2) as value_";// 出水压力
            case 5: return null;// 进水瞬时流量
            case 6: return null;// 出水瞬时流量
            case 7: return null;// 出水瞬时流量
            default: return null;
        }
    }

    private static String formatReadColumnName(Integer monitorParam){
        if(monitorParam==null){return "";}
        switch (monitorParam){
            case 1: return "电流";// 电流
            case 2: return "电压";// 电压
            case 3: return "进水压力";// 进水压力
            case 4: return "出水压力";// 出水压力
            case 5: return "进水瞬时流量";// 进水瞬时流量
            case 6: return "出水瞬时流量";// 出水瞬时流量
            case 7: return "频率";// 出水瞬时流量
            default: return "";
        }
    }

    /*
     * 查询数据分析曲线-新接口(多组x轴对多组y轴)
     * @param
     * @return
     */
    @RequestMapping(value = "iot/readDataAnalysisCurveResource",method ={ RequestMethod.POST,RequestMethod.GET})
    public String readDataAnalysisCurve_new_xToy(Model model,String waterPumpGroupIds, String startTime, String endTime, Integer monitorParam, Integer dataIsZero,String companyCode,Long userId){
        Map<String,Object> paramMap = new HashMap<String,Object>();
        if(userId==null){
            paramMap.put("code",400);
            paramMap.put("msg","用户ID不能为空");
            model.addAttribute("data",paramMap);
            return "waterPumpCurve";
        }
        paramMap.put("list",new ArrayList<>());
//        Assert.parameterIsNull(waterPumpGroupIds,"参数waterPumpGroupIds不能为空");
        if(StringUtils.isBlank(waterPumpGroupIds)){
            paramMap.put("code",400);
            paramMap.put("msg","请选择设备组");
            model.addAttribute("data",paramMap);
            return "waterPumpCurve";
        }
//        Assert.parameterIsBlank(startTime,"起始时间不能为空");
        if(StringUtils.isBlank(startTime)){
            paramMap.put("code",400);
            paramMap.put("msg","起始时间不能为空");
            model.addAttribute("data",paramMap);
            return "waterPumpCurve";
        }
//        Assert.parameterIsBlank(endTime,"截止时间不能为空");
        if(StringUtils.isBlank(endTime)){
            paramMap.put("code",400);
            paramMap.put("msg","截止时间不能为空");
            model.addAttribute("data",paramMap);
            return "waterPumpCurve";
        }
//        Assert.parameterIsNull(monitorParam,"请选择监控参数");
        if(monitorParam==null){
            paramMap.put("code",400);
            paramMap.put("msg","请选择监控参数");
            model.addAttribute("data",paramMap);
            return "waterPumpCurve";
        }
        String readColumn = getReadColumnName(monitorParam);
//        Assert.parameterIsBlank(readColumn,"该监控参数暂无数据");
        if(StringUtils.isBlank(readColumn)){
            paramMap.put("code",400);
            paramMap.put("msg","该监控参数暂无数据");
            model.addAttribute("data",paramMap);
            return "waterPumpCurve";
        }
        Date startDate = DateUtil.stringToDate(startTime);
        if(startDate==null){
//            throw new RRException("起始时间格式有误",400);
            paramMap.put("code",400);
            paramMap.put("msg","起始时间格式有误");
            model.addAttribute("data",paramMap);
            return "waterPumpCurve";
        }
        Date endDate = DateUtil.stringToDate(endTime);
        if(endDate==null){
//            throw new RRException("截止时间格式有误",400);
            paramMap.put("code",400);
            paramMap.put("msg","截止时间格式有误");
            model.addAttribute("data",paramMap);
            return "waterPumpCurve";
        }

        paramMap.put("column",readColumn);
        String startDateStr =DateUtil.format(startDate,DateUtil.DATE_PATTERN.YYYY_MM_DD_HH_MM_SS);
        String endDateStr =DateUtil.format(endDate,DateUtil.DATE_PATTERN.YYYY_MM_DD_HH_MM_SS);
        paramMap.put("startTime",startDateStr);
        paramMap.put("endTime",endDateStr);
        if(waterPumpGroupIds!=null&&!waterPumpGroupIds.equals("-1")){
            paramMap.put("waterPumpGroupIds",waterPumpGroupIds);
        }
        if(StringUtils.isBlank(companyCode)){
            companyCode  =getCurrentCompanyCode();
        }
        if(StringUtils.isBlank(companyCode)){
            paramMap.put("code",400);
            paramMap.put("msg","参数companyCode不能为空");
            model.addAttribute("data",paramMap);
            return "waterPumpCurve";
        }
        paramMap.put("companyCode",companyCode);
//        paramMap.put("valueType",3); //  远程调用 必传参数
//        List<WaterPumpCurveResult>  dataList = iotRpcService.readDataAnalysisCurve_rpc(paramMap);
        List<Map<String,Object>>  dataList = new ArrayList<Map<String,Object>>();
  /*======= 查询 设备组 数据上报时间序列  ==*/
        List<Map<String,Object>> groupNameList = iotRpcService.selectGroupMaps(paramMap);// 查询呢设备组
        if(groupNameList==null||groupNameList.isEmpty()){
//            throw new RRException("请选择设备组",400);
            paramMap.put("code",400);
            paramMap.put("msg","请选择设备组");
            model.addAttribute("data",paramMap);
            return "waterPumpCurve";
        }
        Map<String,String > groupMap = new HashMap<String,String>();// 设备组对应的数据
        List<String> groupCodeList = new ArrayList<String>();
        //沃佳 设备编号 集合
        List<String> woGiaDeviceNameList = new ArrayList<String>();
        for(int x=0,len=groupNameList.size();x<len;x++){
            String groupCode = (String)groupNameList.get(x).get("groupCode");
            groupMap.put(groupCode,(String) groupNameList.get(x).get("groupName"));
            groupCodeList.add(groupCode);
            Integer dataSource = (Integer) groupNameList.get(x).get("dataSource");
            if(dataSource!=null&&dataSource==2){
                woGiaDeviceNameList.add(groupCode);
            }
        }

//        String startDateStr =DateUtil.format(paramMap.get("startTime"),DateUtil.DATE_PATTERN.YYYY_MM_DD_HH_MM_SS);
//        String endDateStr =DateUtil.format(paramMap.get("endTime"),DateUtil.DATE_PATTERN.YYYY_MM_DD_HH_MM_SS);
        String readWoGiaCurveColumn = iotRpcService.readWoGiaCurveColumn(monitorParam);// 生成 监控参数 查询字段
        // 返回字符串 会变成 ""w1""
        if(readWoGiaCurveColumn!=null&&readWoGiaCurveColumn.startsWith("\"")){
            readWoGiaCurveColumn = readWoGiaCurveColumn.substring(1,readWoGiaCurveColumn.length()-1);
        }
        List<String> woGiaSelectMapList = new ArrayList<String>();
        if(!woGiaDeviceNameList.isEmpty()){
            Map<String,List<String>> wogiaDeviceAttrTableNameMap = new HashMap<String,List<String>>();
            for(String woGiaDeviceName : woGiaDeviceNameList){
                String[] woGiaDeviceNameSplit = woGiaDeviceName.split("\\|");
                List<String> columnNameList=wogiaDeviceAttrTableNameMap.get(woGiaDeviceNameSplit[0]);
                if(columnNameList==null){
                    Map<String,String> cloumnParamMap = new HashMap<String,String>();
                    cloumnParamMap.put("tablename",woGiaDeviceNameSplit[0]);
                    cloumnParamMap.put("columnPrifix",readWoGiaCurveColumn);
                    columnNameList=iotRpcService.selectWoGiaAllColumnByTableName(cloumnParamMap);
                    wogiaDeviceAttrTableNameMap.put(woGiaDeviceNameSplit[0],columnNameList);
                    if(columnNameList.contains(woGiaDeviceNameSplit[2]+readWoGiaCurveColumn)){
                        JSONObject woGiaSelectMap = new JSONObject();
                        woGiaSelectMap.put("tableName","`"+woGiaDeviceNameSplit[0]+"`");
                        woGiaSelectMap.put("column","'"+woGiaDeviceName+"' as deviceName,Time as reportTimeFormat,FORMAT("+woGiaDeviceNameSplit[2]+readWoGiaCurveColumn+",2) as value_");
                        woGiaSelectMap.put("startTime","'"+startDateStr+"'");
                        woGiaSelectMap.put("endTime","'"+endDateStr+"'");
                        woGiaSelectMapList.add(woGiaSelectMap.toJSONString());
                    }
                }else{
                    if(columnNameList.contains(woGiaDeviceNameSplit[2]+readWoGiaCurveColumn)){
                        JSONObject woGiaSelectMap = new JSONObject();
                        woGiaSelectMap.put("tableName","`"+woGiaDeviceNameSplit[0]+"`");
                        woGiaSelectMap.put("column","'"+woGiaDeviceName+"' as deviceName,Time as reportTimeFormat,FORMAT("+woGiaDeviceNameSplit[2]+readWoGiaCurveColumn+",2) as value_");
                        woGiaSelectMap.put("startTime","'"+startDateStr+"'");
                        woGiaSelectMap.put("endTime","'"+endDateStr+"'");
                        woGiaSelectMapList.add(woGiaSelectMap.toJSONString());
                    }
                }
            }
        }
            /*======= 查询 设备组 数据上报时间序列  ==*/
        List<String> reportTimeList =iotRpcService.selectReportTimes(paramMap);
        if(reportTimeList==null){reportTimeList = new ArrayList<String>();}
        if(!woGiaSelectMapList.isEmpty()){
//            LinkedHashSet<String> woGiaReportTimeList =iotRpcService.readWogia2DataAnalysisCurveTime(woGiaSelectMapList);
//            if(woGiaReportTimeList==null){woGiaReportTimeList = new LinkedHashSet<String>();}
//            reportTimeList.addAll(woGiaReportTimeList);
            List<String> woGiaReportTimeList = null;
            if(woGiaSelectMapList.size()>1){
                woGiaReportTimeList =iotRpcService.readWogia2DataAnalysisCurveTime(woGiaSelectMapList);
            }else{
                woGiaReportTimeList =iotRpcService.readWogia2DataAnalysisCurveTime_single(woGiaSelectMapList.get(0));
            }
            reportTimeList.addAll(woGiaReportTimeList);
            Collections.sort(reportTimeList);//默认排序(从小到大)
        }
            /*======= 查询 设备组 数据  ==*/
        Map<String,Map<String,Object>> dataMap = new HashMap<String,Map<String,Object>>();
            if(!reportTimeList.isEmpty()){
                List<Map<String,Object>> groupDataList = iotRpcService.selectReportTimesCurveMap(paramMap);
                if(groupDataList==null){
                    groupDataList = new ArrayList<Map<String,Object>>();
                }
                if(!woGiaSelectMapList.isEmpty()){
                    List<Map<String,Object>> woGiaGroupDataList = null;
                    if(woGiaSelectMapList.size()>1){
                        woGiaGroupDataList = iotRpcService.readWogia2DataAnalysisCount(woGiaSelectMapList);
                    }else{
                        woGiaGroupDataList = iotRpcService.readWogia2DataAnalysisCount_single(woGiaSelectMapList.get(0));
                    }
                    groupDataList.addAll(woGiaGroupDataList==null?new ArrayList<>():woGiaGroupDataList);
                }
                for(int x=0,len=groupDataList.size();x<len;x++){
                    // 根据 设备编号+ 时间  存储数据
                    dataMap.put(groupDataList.get(x).get("deviceName")+"|"+formatTime(groupDataList.get(x).get("reportTimeFormat")),groupDataList.get(x));
                }
            }
        Map<String,List<JSONArray>> groupDataMap = new HashMap<String,List<JSONArray>>();// 设备组对应的数据
        for(int y=0,len = groupCodeList.size();y<len;y++){// 遍历 设备编号
            Map<String,Object> returnDataMap =new HashMap<String,Object>();
            returnDataMap.put("groupName", groupMap.get(groupCodeList.get(y)) );
            List<JSONArray> dataArr = new ArrayList<JSONArray>();
            for(int x=0,reportTimeLen = reportTimeList.size();x<reportTimeLen;x++){// 遍历时间轴
                Map<String,Object> data = dataMap.get( groupCodeList.get(y)+"|"+ formatTime(reportTimeList.get(x)));
                if(data==null||data.isEmpty()){
                    StringBuilder jsonArrayBuilder = new StringBuilder("[");
                    jsonArrayBuilder.append("'").append( reportTimeList.get(x) ).append("'");// 拼接时间
                    jsonArrayBuilder.append(",").append("null");
                    jsonArrayBuilder.append("]");
                    dataArr.add(JSONArray.parseArray(jsonArrayBuilder.toString()));
                }else{
                    StringBuilder jsonArrayBuilder = new StringBuilder("[");
                    jsonArrayBuilder.append("'").append( reportTimeList.get(x) ).append("'");// 拼接时间
                    jsonArrayBuilder.append(",").append(data.get("value_"));
                    jsonArrayBuilder.append("]");
                    dataArr.add(JSONArray.parseArray(jsonArrayBuilder.toString()));
                }
            }
            returnDataMap.put("valueArr",dataArr);
            dataList.add(returnDataMap);
        }

        paramMap.put("list",dataList);
        paramMap.put("type",monitorParam);
        paramMap.remove("column");
        paramMap.remove("valueType");
        paramMap.put("code",200);
        paramMap.put("msg","ok");
        model.addAttribute("data",paramMap);
        return "waterPumpCurve";
    }

    public Long formatTime(Object time){
        if(time instanceof String){
            return DateUtil.stringToDate((String) time).getTime();
        }
        if(time instanceof Date){
            return ((Date) time).getTime();
        }
        return  (Long)time;
    }

    /*
    * 查询数据分析曲线-跳转页面不处理数据
    * @param
    * @return
    */
    @RequestMapping(value = "/iot/readDataAnalysisCurve_toHtml",method ={ RequestMethod.POST,RequestMethod.GET})
    public String readDataAnalysisCurve_toHtml(Model model,String waterPumpGroupIds, String startTime, String endTime, Integer monitorParam, Integer dataIsZero,String companyCode,Long userId){
        Map<String,Object> paramMap = new HashMap<String,Object>();
        if(userId==null){
            paramMap.put("code",400);
            paramMap.put("msg","用户ID不能为空");
            model.addAttribute("data",paramMap);
            return "waterPumpCurve";
        }
        paramMap.put("list",new ArrayList<>());
//        Assert.parameterIsNull(waterPumpGroupIds,"参数waterPumpGroupIds不能为空");
        if(StringUtils.isBlank(waterPumpGroupIds)){
            paramMap.put("code",400);
            paramMap.put("msg","请选择设备组");
            model.addAttribute("data",paramMap);
            return "waterPumpCurve";
        }
//        Assert.parameterIsBlank(startTime,"起始时间不能为空");
        if(StringUtils.isBlank(startTime)){
            paramMap.put("code",400);
            paramMap.put("msg","起始时间不能为空");
            model.addAttribute("data",paramMap);
            return "waterPumpCurve";
        }
//        Assert.parameterIsBlank(endTime,"截止时间不能为空");
        if(StringUtils.isBlank(endTime)){
            paramMap.put("code",400);
            paramMap.put("msg","截止时间不能为空");
            model.addAttribute("data",paramMap);
            return "waterPumpCurve";
        }
//        Assert.parameterIsNull(monitorParam,"请选择监控参数");
        if(monitorParam==null){
            paramMap.put("code",400);
            paramMap.put("msg","请选择监控参数");
            model.addAttribute("data",paramMap);
            return "waterPumpCurve";
        }
        String readColumn = getReadColumnName(monitorParam);
//        Assert.parameterIsBlank(readColumn,"该监控参数暂无数据");
        if(StringUtils.isBlank(readColumn)){
            paramMap.put("code",400);
            paramMap.put("msg","该监控参数暂无数据");
            model.addAttribute("data",paramMap);
            return "waterPumpCurve";
        }
        Date startDate = DateUtil.stringToDate(startTime);
        if(startDate==null){
//            throw new RRException("起始时间格式有误",400);
            paramMap.put("code",400);
            paramMap.put("msg","起始时间格式有误");
            model.addAttribute("data",paramMap);
            return "waterPumpCurve";
        }
        Date endDate = DateUtil.stringToDate(endTime);
        if(endDate==null){
//            throw new RRException("截止时间格式有误",400);
            paramMap.put("code",400);
            paramMap.put("msg","截止时间格式有误");
            model.addAttribute("data",paramMap);
            return "waterPumpCurve";
        }

        paramMap.put("column",readColumn);
        String startDateStr =DateUtil.format(startDate,DateUtil.DATE_PATTERN.YYYY_MM_DD_HH_MM_SS);
        String endDateStr =DateUtil.format(endDate,DateUtil.DATE_PATTERN.YYYY_MM_DD_HH_MM_SS);
        paramMap.put("startTime",startDateStr);
        paramMap.put("endTime",endDateStr);
//        if(waterPumpGroupIds!=null&&!waterPumpGroupIds.equals("-1")){
//            paramMap.put("waterPumpGroupIds",waterPumpGroupIds);
//        }
        if(StringUtils.isBlank(companyCode)){
            companyCode  =getCurrentCompanyCode();
        }
        if(StringUtils.isBlank(companyCode)){
            paramMap.put("code",400);
            paramMap.put("msg","参数companyCode不能为空");
            model.addAttribute("data",paramMap);
            return "waterPumpCurve";
        }
        String url = request.getRequestURL().toString();
        url=url.substring(0,url.indexOf("/iot/readDataAnalysisCurve_toHtml"));
        paramMap.put("reqHost",url );
        paramMap.put("waterPumpGroupIds",waterPumpGroupIds);
        paramMap.put("userId",userId);
        paramMap.put("companyCode",companyCode);
        paramMap.put("type",monitorParam);
        paramMap.put("monitorParam",monitorParam);
        paramMap.remove("column");
        paramMap.remove("valueType");
//        paramMap.remove("list");
        paramMap.put("code",200);
        paramMap.put("msg","ok");
        model.addAttribute("data",paramMap);
        return "waterPumpCurve";
    }
}
