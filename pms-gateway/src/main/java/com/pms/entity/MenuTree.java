package com.pms.entity;
/**
 * @author zyl
 * @create 2017-07-06 15:12
 **/
public class MenuTree extends TreeNode{
    String icon;
    String title;
    String href;
    boolean spread = false;


    private String code;
    private String type;
    private String uri;
    private String method;
    private String name;
    private Integer menuType;
    private String menu;
    private String id;

    private String parentId;
//    @TableField(exist = true)
//    private List<BaseElement> baseElement;
//
//    public List<BaseElement> getBaseElement() {
//        return baseElement;
//    }
//
//    public void setBaseElement(List<BaseElement> baseElement) {
//        this.baseElement = baseElement;
//    }


    public void setCode(String code) {
        this.code = code;
    }

    public void setType(String type) {
        this.type = type;
    }

    public void setUri(String uri) {
        this.uri = uri;
    }

    public void setMethod(String method) {
        this.method = method;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public void setMenuType(Integer menuType) {
        this.menuType = menuType;
    }

    public void setMenu(String menu) {
        this.menu = menu;
    }

    public void setId(String id) {
        this.id = id;
    }

    public void setParentId(String parentId) {
        this.parentId = parentId;
    }

    public String getCode() {
        return code;
    }

    public String getType() {
        return type;
    }

    public String getUri() {
        return uri;
    }

    public String getMethod() {
        return method;
    }

    public String getName() {
        return name;
    }

    @Override
    public Integer getMenuType() {
        return menuType;
    }

    public String getMenu() {
        return menu;
    }


    public MenuTree() {
    }

    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getHref() {
        return href;
    }

    public void setHref(String href) {
        this.href = href;
    }

    public boolean isSpread() {
        return spread;
    }

    public void setSpread(boolean spread) {
        this.spread = spread;
    }
}
