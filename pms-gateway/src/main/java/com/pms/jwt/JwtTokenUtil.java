package com.pms.jwt;
import com.pms.entity.BaseUserInfo;
import com.pms.entity.UserInfo;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Component;

import java.io.Serializable;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

@Component
public class JwtTokenUtil implements Serializable {

    private static final long serialVersionUID = -3301605591108950415L;

    private static final String CLAIM_KEY_USERNAME = "sub";
    private static final String CLAIM_KEY_CREATED = "created";

    @Value("${gate.jwt.secret}")
    private String secret;

    @Value("${gate.jwt.expiration}")
    private Long expiration;

    @Autowired
    private RedisTemplate<String, Object> redisTemplate;
//
//    @Value("${gate.jwt.prefix}")
//    private String prefix;

    public String getUsernameFromToken(String token) {

        String username;
        try {
            final Claims claims = getClaimsFromToken(token);
            username = claims.getSubject();
        } catch (Exception e) {
            username = null;
        }
        return username;
    }

    public Date getCreatedDateFromToken(String token) {
        Date created;
        try {
            final Claims claims = getClaimsFromToken(token);
            created = new Date((Long) claims.get(CLAIM_KEY_CREATED));
        } catch (Exception e) {
            created = null;
        }
        return created;
    }

    public Date getExpirationDateFromToken(String token) {
        Date expiration;
        try {
            final Claims claims = getClaimsFromToken(token);
            expiration = claims.getExpiration();//过期时间
        } catch (Exception e) {
            expiration = null;
        }
        return expiration;
    }

    /**
     * 退出清空token
     * @param token
     * @return
     */
    public Boolean invalid(String token){
        String username = this.getUsernameFromToken(token);//查询信息是否存在
        if (username==null){return false;}//为空，返回false
        Object old = redisTemplate.opsForValue().get(username);
        if (old!=null){
            redisTemplate.opsForValue().set(username,null);
        }
        return true;
    }

    /**
     * 通过token查询token对象
     * @param token
     * @return
     */
    private Claims getClaimsFromToken(String token) {
        Claims claims;
        try {
            claims = Jwts.parser()
                    .setSigningKey(secret)
                    .parseClaimsJws(token)
                    .getBody();
        } catch (Exception e) {
            claims = null;
        }
        return claims;
    }

    private Date generateExpirationDate() {
        return new Date(System.currentTimeMillis() + expiration * 1000);
    }

    /**
     * 判断token是否过⑦
     * @param token
     * @return
     */
    public Boolean isTokenExpired(String token) {
        final Date expiration = getExpirationDateFromToken(token);//取到token过期时间，过期时间为2小时
        return expiration.before(new Date());//比较token结束时间是否小于当前时间，对返回true，相反false
    }

    /**
     *
     * @param created    当前token获取的时间
     * @param lastPasswordReset  用户更新的时间
     * @return
     */
    private Boolean isCreatedBeforeLastPasswordReset(Date created, Date lastPasswordReset) {
        return (lastPasswordReset != null && created.before(lastPasswordReset));
    }

    public String generateToken(BaseUserInfo info) {
//        Object old = redisTemplate.opsForValue().get(info.getUsername());
        String token = "";
//        if(old==null) {
            Map<String, Object> claims = new HashMap<String, Object>();
            claims.put(CLAIM_KEY_USERNAME, info.getUsername());
            claims.put(CLAIM_KEY_CREATED, new Date());
            token = generateToken(claims);
            redisTemplate.opsForValue().set(info.getUsername(), token,expiration, TimeUnit.SECONDS);
//        }else {
//            token = old.toString();
//        }
        return token;
    }

    String generateToken(Map<String, Object> claims) {
        return Jwts.builder()
                .setClaims(claims)
                .setExpiration(generateExpirationDate())
                .signWith(SignatureAlgorithm.HS512, secret)
                .compact();
    }

    /**
     * token 时间比较
     * @param token
     * @param lastPasswordReset   用户更新的时间
     * @return
     */
    public Boolean canTokenBeRefreshed(String token, Date lastPasswordReset) {
        final Date created = getCreatedDateFromToken(token);//取到token注册的时间
        return !isCreatedBeforeLastPasswordReset(created, lastPasswordReset)
                &&  !isTokenExpired(token);
    }

    /**
     * 刷新token
     * @param token
     * @return
     */
    public String refreshToken(String token,BaseUserInfo info) {
        String refreshedToken;
        try {
            Map<String, Object> claims = new HashMap<String, Object>();
            claims.put(CLAIM_KEY_USERNAME,info.getUsername());
            claims.put(CLAIM_KEY_CREATED, new Date());
            refreshedToken = generateToken(claims);
        } catch (Exception e) {
            refreshedToken = null;
        }
        return refreshedToken;
    }

    public Boolean validateToken(String token, UserInfo info) {
        if(StringUtils.isBlank(token)){
            return false;
        }
//        Object existToken = redisTemplate.opsForValue().get(info.getUsername());
//        if(token.equals(existToken)){
            final String username = getUsernameFromToken(token);
            final Date created = getCreatedDateFromToken(token);
            return (
                    username.equals(info.getUsername())
                            && !isTokenExpired(token));
//     }
// else{
//            return false;
//        }
//        if(!token.startsWith(prefix)){
//            return false;
//        }else {
//            token = token.substring(prefix.length() + 1);
//        }
    }
}

