package com.pms.service;

import com.pms.entity.BaseUserInfo;
import com.pms.entity.FrontUser;
import com.pms.entity.MenuTree;

import java.util.List;

public interface AuthService {
    String  login(BaseUserInfo info);
    String refresh(String oldToken,BaseUserInfo info);
    Boolean validate(String token, String resource,Integer menuType,String companyCode);
    FrontUser getUserInfo(String token,Integer menuType,String companyCode);
    Boolean invalid(String token);
    FrontUser getApiUserInfo(String token,String companyCode);

    FrontUser getSuperUser(String token);

}
