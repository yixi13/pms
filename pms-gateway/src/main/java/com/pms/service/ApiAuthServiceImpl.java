//package com.pms.service;
//
//import com.google.common.base.Predicate;
//import com.google.common.collect.Collections2;
//import com.pms.authority.PermissionInfo;
//import com.pms.constant.BaseConstants;
//import com.pms.entity.BaseUserInfo;
//import com.pms.entity.FrontUser;
//import com.pms.entity.MemberInfo;
//import com.pms.entity.UserInfo;
//import com.pms.exception.R;
//import com.pms.exception.RRException;
//import com.pms.jwt.ApiJwtTokenUtil;
//import com.pms.jwt.JwtTokenUtil;
//import com.pms.rpc.IMemberService;
//import com.pms.rpc.IUserService;
//import org.springframework.beans.BeanUtils;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
//import org.springframework.stereotype.Service;
//import org.w3c.dom.ranges.RangeException;
//
//import java.util.Collection;
//import java.util.HashMap;
//import java.util.List;
//import java.util.Map;
//import java.util.regex.Pattern;
//import java.util.stream.Collectors;
//import java.util.stream.Stream;
//
///**
// * @author zyl
// * @create 2017-07-20 14:12
// **/
//@Service
//public class ApiAuthServiceImpl implements ApiAuthService {
//    private ApiJwtTokenUtil apijwtTokenUtil;
//    private IMemberService memberService;
//    private BCryptPasswordEncoder encoder = new BCryptPasswordEncoder(12);
//
//    @Autowired
//    public ApiAuthServiceImpl(
//            ApiJwtTokenUtil jwtTokenUtil,
//            IMemberService memberService) {
//        this.apijwtTokenUtil = jwtTokenUtil;
//        this.memberService = memberService;
//    }
//
//    /**
//     * 会员登录
//     * @param phone
//     * @param passWord
//     * @return
//     */
//    @Override
//    public Map<String, Object> login(String phone, String passWord) {
//        MemberInfo info = memberService.queryByphone(phone);
//        if (info==null){  throw  new RRException("账号不存在",400);}
//        if(encoder.matches(passWord,info.getPassWord())){
//            String token = apijwtTokenUtil.generateToken(info);
//            Map<String,Object> map=new HashMap<>();
//            map.put("member",info);
//            map.put("token",token);
//             return map;
//        }else{
//            throw  new RRException("账号密码错误",400);
//        }
//    }
//
//    @Override
//    public String refresh(String token, BaseUserInfo info) {
//        String phone = apijwtTokenUtil.getPhoneFromToken(token);
//        if (phone==null){//token已过期
//            return apijwtTokenUtil.refreshToken(token,info);
//        }else {
//            return phone;
//        }
////        if (apijwtTokenUtil.canTokenBeRefreshed(token,info.getUpdateTime())){//info.getUpdTime()用户的修改时间
////            return apijwtTokenUtil.refreshToken(token);
////        }
//    }
//
//    @Override
//    public Boolean invalid(String token) {
//        return apijwtTokenUtil.invalid(token);
//    }
//
//    @Override
//    public Boolean validate(String token) {
//        String phone = apijwtTokenUtil.getPhoneFromToken(token);
//        if (phone==null){return false;}
//        MemberInfo info = memberService.queryByphone(phone);
//        return info.getPhone().equals(phone)//账号是否相同
//                && !apijwtTokenUtil.isTokenExpired(token);//过期时间小于当前时间验证token时间未过
//   }
//
//
//}
